
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  74]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/KiwiThomas417' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 08:59:37 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 09:02:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701269977838 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01465E+00  1.03794E+00  9.87908E-01  9.86332E-01  9.87086E-01  1.01655E+00  9.94314E-01  9.75222E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.41723E-01 0.00081  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.58277E-01 0.00064  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54652E-01 0.00034  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.12311E-01 0.00047  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14807E+01 0.00144  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40344E-01 1.5E-05  5.92821E-02 0.00023  3.74326E-04 0.00272  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.10565E+01 0.00082  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.08841E+01 0.00082  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.12044E+02 0.00083  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.05741E+02 0.00169  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 600122 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00020E+04 0.00220 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00020E+04 0.00220 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30220E+01 ;
RUNNING_TIME              (idx, 1)        =  3.06952E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.09283E-01  1.09283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.53333E-03  4.53333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.95568E+00  2.95568E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.06947E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.50022 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.69379E+00 0.00401 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.56258E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1926.92 ;
MEMSIZE                   (idx, 1)        = 1809.21 ;
XS_MEMSIZE                (idx, 1)        = 1250.85 ;
MAT_MEMSIZE               (idx, 1)        = 490.03 ;
RES_MEMSIZE               (idx, 1)        = 1.07 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.27 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 117.71 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 641913 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.93522E-05 0.00115  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.49122E-02 0.00988 ];
U235_FISS                 (idx, [1:   4]) = [  4.17883E-01 0.00183  9.99193E-01 5.7E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.37591E-04 0.07010  8.07441E-04 0.07026 ];
U235_CAPT                 (idx, [1:   4]) = [  1.58582E-01 0.00394  3.88854E-01 0.00279 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43640E-02 0.00992  3.52320E-02 0.01002 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 600122 6.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 4.36720E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 600122 6.04367E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 244681 2.46261E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 251951 2.52569E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 103490 1.05537E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 600122 6.04367E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.28057E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35389E-11 0.00108 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02149E+00 0.00108 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.17767E-01 0.00108 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.07487E-01 0.00115 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.25253E-01 0.00066 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.93522E-01 0.00115 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.41433E+02 0.00112 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.74747E-01 0.00312 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.08999E+01 0.00118 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06639E+00 0.00148 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.51329E-01 0.00202 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.44476E-01 0.00270 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.79841E+00 0.00348 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43647E-01 0.00061 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76835E-01 0.00023 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.24896E+00 0.00143 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02926E+00 0.00145 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44513E+00 9.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 4.9E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02944E+00 0.00146  1.02155E+00 0.00146  7.71100E-03 0.02052 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02902E+00 0.00107 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02825E+00 0.00176 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02902E+00 0.00107 ];
ABS_KINF                  (idx, [1:   2]) = [  1.24887E+00 0.00090 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31333E+01 0.00083 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31288E+01 0.00060 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.97005E-05 0.01076 ];
IMP_EALF                  (idx, [1:   2]) = [  3.98147E-05 0.00787 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01726E-01 0.00873 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.01763E-01 0.00243 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.58756E-03 0.01552  2.36683E-04 0.07507  9.65172E-04 0.03631  5.98330E-04 0.04756  1.30822E-03 0.03190  2.10008E-03 0.03098  6.39599E-04 0.04883  5.76822E-04 0.05245  1.62656E-04 0.09227 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.23245E-01 0.02430  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.28135E-03 0.02157  2.68932E-04 0.11632  1.06114E-03 0.05040  6.99026E-04 0.08085  1.45371E-03 0.05194  2.27474E-03 0.04724  7.62442E-04 0.08555  5.92654E-04 0.06770  1.68698E-04 0.15417 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.09347E-01 0.03553  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.31198E-05 0.00784  2.31487E-05 0.00794  1.96341E-05 0.10046 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.37987E-05 0.00781  2.38284E-05 0.00791  2.02157E-05 0.10071 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.53130E-03 0.02216  2.84315E-04 0.11792  1.12787E-03 0.06112  7.13028E-04 0.07999  1.48332E-03 0.04696  2.43876E-03 0.03965  6.89549E-04 0.08825  6.24553E-04 0.07176  1.69906E-04 0.12781 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.07025E-01 0.03140  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 1.9E-09  1.63478E+00 0.0E+00  3.55460E+00 7.1E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.01194E-05 0.04347  2.01343E-05 0.04371  1.32163E-05 0.23831 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.07057E-05 0.04352  2.07207E-05 0.04376  1.36189E-05 0.23927 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  5.86323E-03 0.09732  2.01737E-04 0.40743  1.03146E-03 0.16936  6.16243E-04 0.27444  1.22923E-03 0.17090  1.85163E-03 0.17648  2.38606E-04 0.27626  6.67179E-04 0.26382  2.71413E-05 0.70942 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.42498E-01 0.11255  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 6.0E-09  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  5.93190E-03 0.08823  2.13427E-04 0.35725  1.05981E-03 0.17353  6.77255E-04 0.29539  1.12902E-03 0.15608  1.82420E-03 0.16031  2.97457E-04 0.27877  6.88886E-04 0.25101  4.18433E-05 0.72613 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.66006E-01 0.11924  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 6.0E-09  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.02341E+02 0.10273 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.24551E-05 0.00352 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.31128E-05 0.00306 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.27128E-03 0.01145 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.24018E+02 0.01185 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.00849E-07 0.00355 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60789E-05 0.00163  1.60830E-05 0.00162  1.55017E-05 0.01711 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.03884E-04 0.00406  1.03938E-04 0.00406  9.66299E-05 0.05097 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.21538E-01 0.00255  2.21490E-01 0.00256  2.32017E-01 0.03360 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.33896E+01 0.03919 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.08841E+01 0.00082  4.99632E+01 0.00125 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.80125E+04 0.01594  4.63237E+05 0.00301  1.03963E+06 0.00163  1.88373E+06 0.00123  1.99551E+06 0.00214  1.85036E+06 0.00269  1.70069E+06 0.00068  1.50871E+06 0.00086  1.33808E+06 0.00080  1.20916E+06 0.00131  1.10796E+06 0.00139  1.02747E+06 0.00198  9.45995E+05 0.00270  9.05847E+05 0.00097  8.66410E+05 0.00220  7.34954E+05 0.00073  7.28034E+05 0.00269  6.94723E+05 0.00367  6.60915E+05 0.00210  1.21872E+06 0.00255  1.05803E+06 0.00418  6.90154E+05 0.00132  4.14836E+05 0.00165  4.40201E+05 0.00141  3.87485E+05 0.00489  3.07351E+05 0.00409  5.01320E+05 0.00268  1.00787E+05 0.00312  1.23742E+05 0.00274  1.11327E+05 0.01123  6.31288E+04 0.00734  1.08838E+05 0.00450  7.31176E+04 0.00346  6.00348E+04 0.00168  1.13315E+04 0.01273  1.09930E+04 0.00529  1.07706E+04 0.00779  1.13138E+04 0.01377  1.12303E+04 0.01157  1.11368E+04 0.03480  1.12468E+04 0.01038  1.07597E+04 0.00673  2.00194E+04 0.00622  3.13221E+04 0.01202  3.94145E+04 0.00956  9.98018E+04 0.00688  9.74754E+04 0.00205  9.39030E+04 0.00289  5.44222E+04 0.00531  3.53871E+04 0.00174  2.54509E+04 0.00932  2.79729E+04 0.00841  4.74756E+04 0.00393  5.67499E+04 0.00693  9.91213E+04 0.00122  1.45049E+05 0.00341  2.25864E+05 0.00527  1.57223E+05 0.00333  1.19142E+05 0.00326  9.05237E+04 0.00688  8.40407E+04 0.00578  8.67666E+04 0.01276  7.56797E+04 0.00724  5.28528E+04 0.00536  5.02122E+04 0.01230  4.56718E+04 0.00351  4.04339E+04 0.00761  3.21912E+04 0.00283  2.18643E+04 0.01023  8.21934E+03 0.00431 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.24746E+00 0.00345 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32624E+02 0.00219  8.81102E+00 0.00367 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43659E-01 0.00061  6.04164E-01 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  2.39707E-03 0.00163  1.01699E-02 0.00839 ];
INF_ABS                   (idx, [1:   4]) = [  4.71686E-03 0.00129  2.26714E-02 0.00987 ];
INF_FISS                  (idx, [1:   4]) = [  2.31979E-03 0.00099  1.25016E-02 0.01158 ];
INF_NSF                   (idx, [1:   4]) = [  5.67963E-03 0.00099  3.04556E-02 0.01158 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44834E+00 1.1E-05  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 4.4E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.19870E-08 0.00281  2.59217E-06 0.00131 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38939E-01 0.00065  5.81441E-01 0.00065 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89746E-02 0.00057  4.05736E-02 0.00661 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05022E-02 0.00550  4.95810E-03 0.01171 ];
INF_SCATT3                (idx, [1:   4]) = [  1.10384E-03 0.02262  1.69873E-03 0.04632 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.93521E-04 0.04115  5.99534E-04 0.10704 ];
INF_SCATT5                (idx, [1:   4]) = [  7.58907E-05 0.41120  2.36461E-04 0.23299 ];
INF_SCATT6                (idx, [1:   4]) = [  4.09806E-04 0.01614  1.07971E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  4.36714E-05 0.26075 -7.07102E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38994E-01 0.00065  5.81441E-01 0.00065 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.89873E-02 0.00058  4.05736E-02 0.00661 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05051E-02 0.00555  4.95810E-03 0.01171 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.10445E-03 0.02288  1.69873E-03 0.04632 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.92552E-04 0.04112  5.99534E-04 0.10704 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.65310E-05 0.40648  2.36461E-04 0.23299 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.10134E-04 0.01625  1.07971E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.37702E-05 0.26401 -7.07102E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49372E-01 0.00075  5.59445E-01 0.00081 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33669E+00 0.00075  5.95830E-01 0.00081 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.66233E-03 0.00126  2.26714E-02 0.00987 ];
INF_REMXS                 (idx, [1:   4]) = [  6.45289E-03 0.00157  2.38874E-02 0.00613 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.37206E-01 0.00064  1.73306E-03 0.00352  1.16447E-03 0.00521  5.80277E-01 0.00066 ];
INF_S1                    (idx, [1:   8]) = [  3.91371E-02 0.00052 -1.62521E-04 0.02185  2.30419E-04 0.01321  4.03432E-02 0.00657 ];
INF_S2                    (idx, [1:   8]) = [  1.05844E-02 0.00518 -8.22443E-05 0.03673 -8.80263E-06 1.00000  4.96691E-03 0.00981 ];
INF_S3                    (idx, [1:   8]) = [  1.18786E-03 0.02424 -8.40199E-05 0.04940 -3.98135E-05 0.22873  1.73855E-03 0.05034 ];
INF_S4                    (idx, [1:   8]) = [ -5.64101E-04 0.04400 -2.94191E-05 0.03807 -1.95164E-05 0.38868  6.19050E-04 0.10221 ];
INF_S5                    (idx, [1:   8]) = [  7.05730E-05 0.44509  5.31771E-06 0.18953 -1.13902E-05 0.57565  2.47851E-04 0.19584 ];
INF_S6                    (idx, [1:   8]) = [  4.02518E-04 0.01702  7.28848E-06 0.03343 -3.18153E-06 0.79910  1.11153E-04 0.94925 ];
INF_S7                    (idx, [1:   8]) = [  4.56930E-05 0.27338 -2.02162E-06 0.56402 -4.67966E-06 0.98753 -6.60305E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37261E-01 0.00064  1.73306E-03 0.00352  1.16447E-03 0.00521  5.80277E-01 0.00066 ];
INF_SP1                   (idx, [1:   8]) = [  3.91498E-02 0.00053 -1.62521E-04 0.02185  2.30419E-04 0.01321  4.03432E-02 0.00657 ];
INF_SP2                   (idx, [1:   8]) = [  1.05873E-02 0.00524 -8.22443E-05 0.03673 -8.80263E-06 1.00000  4.96691E-03 0.00981 ];
INF_SP3                   (idx, [1:   8]) = [  1.18847E-03 0.02448 -8.40199E-05 0.04940 -3.98135E-05 0.22873  1.73855E-03 0.05034 ];
INF_SP4                   (idx, [1:   8]) = [ -5.63133E-04 0.04398 -2.94191E-05 0.03807 -1.95164E-05 0.38868  6.19050E-04 0.10221 ];
INF_SP5                   (idx, [1:   8]) = [  7.12132E-05 0.43969  5.31771E-06 0.18953 -1.13902E-05 0.57565  2.47851E-04 0.19584 ];
INF_SP6                   (idx, [1:   8]) = [  4.02846E-04 0.01712  7.28848E-06 0.03343 -3.18153E-06 0.79910  1.11153E-04 0.94925 ];
INF_SP7                   (idx, [1:   8]) = [  4.57918E-05 0.27654 -2.02162E-06 0.56402 -4.67966E-06 0.98753 -6.60305E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70508E-01 0.00124  8.33350E-01 0.01271 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.76602E-01 0.00262  1.11358E+00 0.02638 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.74465E-01 0.00253  1.16308E+00 0.02931 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61006E-01 0.00373  5.44043E-01 0.02615 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23225E+00 0.00124  4.00120E-01 0.01258 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.20512E+00 0.00262  2.99753E-01 0.02644 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21450E+00 0.00253  2.87089E-01 0.02929 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27714E+00 0.00374  6.13517E-01 0.02556 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.28135E-03 0.02157  2.68932E-04 0.11632  1.06114E-03 0.05040  6.99026E-04 0.08085  1.45371E-03 0.05194  2.27474E-03 0.04724  7.62442E-04 0.08555  5.92654E-04 0.06770  1.68698E-04 0.15417 ];
LAMBDA                    (idx, [1:  18]) = [  4.09347E-01 0.03553  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];

