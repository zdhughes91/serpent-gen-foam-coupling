
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  75]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/KiwiThomas1548' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 08:59:37 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 09:02:40 2023' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701269977838 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.00178E+00  9.85464E-01  1.00441E+00  1.00718E+00  1.02167E+00  1.01117E+00  9.76907E-01  9.91417E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.46575E-01 0.00090  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.53425E-01 0.00073  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54595E-01 0.00044  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.15141E-01 0.00052  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.16208E+01 0.00177  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40466E-01 1.5E-05  5.91644E-02 0.00023  3.69612E-04 0.00302  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.16564E+01 0.00080  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.14817E+01 0.00081  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11880E+02 0.00089  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07243E+02 0.00191  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 599976 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99960E+03 0.00212 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99960E+03 0.00212 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28796E+01 ;
RUNNING_TIME              (idx, 1)        =  3.04607E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.19500E-02  7.19500E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.88333E-03  2.88333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.97122E+00  2.97122E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.04585E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.51121 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.64853E+00 0.00437 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.68599E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1605.39 ;
MEMSIZE                   (idx, 1)        = 1502.79 ;
XS_MEMSIZE                (idx, 1)        = 1041.65 ;
MAT_MEMSIZE               (idx, 1)        = 392.80 ;
RES_MEMSIZE               (idx, 1)        = 1.07 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.27 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 102.60 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 514474 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 1 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.91507E-05 0.00111  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.70961E-02 0.01088 ];
U235_FISS                 (idx, [1:   4]) = [  4.11704E-01 0.00189  9.99144E-01 5.1E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.52169E-04 0.05918  8.55627E-04 0.05932 ];
U235_CAPT                 (idx, [1:   4]) = [  1.59345E-01 0.00284  3.88456E-01 0.00263 ];
U238_CAPT                 (idx, [1:   4]) = [  1.54747E-02 0.01066  3.77311E-02 0.01086 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 599976 6.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 4.55946E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 599976 6.04559E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 246557 2.48241E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 248627 2.49350E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 104792 1.06968E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 599976 6.04559E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 3.60887E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.33470E-11 0.00125 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.00709E+00 0.00125 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.11846E-01 0.00125 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.11398E-01 0.00110 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.23244E-01 0.00066 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.91507E-01 0.00111 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.42119E+02 0.00124 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.76756E-01 0.00305 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.14188E+01 0.00128 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05773E+00 0.00154 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.62711E-01 0.00239 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.18018E-01 0.00274 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  4.16278E+00 0.00341 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.42062E-01 0.00067 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.75843E-01 0.00019 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.23669E+00 0.00122 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01622E+00 0.00147 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44530E+00 1.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 5.5E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01535E+00 0.00146  1.00839E+00 0.00147  7.82662E-03 0.02283 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01465E+00 0.00122 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01582E+00 0.00198 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01465E+00 0.00122 ];
ABS_KINF                  (idx, [1:   2]) = [  1.23449E+00 0.00095 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.28781E+01 0.00083 ];
IMP_ALF                   (idx, [1:   2]) = [  1.28698E+01 0.00057 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.12394E-05 0.01059 ];
IMP_EALF                  (idx, [1:   2]) = [  5.15757E-05 0.00738 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.03256E-01 0.00933 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.03686E-01 0.00262 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.74428E-03 0.01903  2.23489E-04 0.09928  1.04884E-03 0.04327  6.29999E-04 0.05370  1.49208E-03 0.02737  2.05590E-03 0.02771  5.94108E-04 0.05530  5.46143E-04 0.05674  1.53723E-04 0.10722 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.00202E-01 0.02252  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.49027E-03 0.02559  2.48567E-04 0.13437  1.15848E-03 0.05504  7.23247E-04 0.08229  1.58128E-03 0.05082  2.26336E-03 0.04362  7.01632E-04 0.07826  6.59571E-04 0.09562  1.54141E-04 0.15497 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.05319E-01 0.03114  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  1.93348E-05 0.00721  1.93381E-05 0.00725  1.85714E-05 0.06847 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  1.96302E-05 0.00721  1.96334E-05 0.00722  1.88803E-05 0.06883 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.65999E-03 0.02276  2.39129E-04 0.15500  1.17636E-03 0.05449  7.99669E-04 0.07194  1.62858E-03 0.04296  2.34626E-03 0.04232  6.92793E-04 0.07565  6.01745E-04 0.08281  1.75460E-04 0.17841 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  3.98110E-01 0.03733  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 1.9E-09  3.55460E+00 6.6E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  1.70829E-05 0.04290  1.70485E-05 0.04272  2.26868E-05 0.21411 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  1.73516E-05 0.04302  1.73169E-05 0.04284  2.30426E-05 0.21480 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.80918E-03 0.08745  1.87794E-04 0.50804  1.03845E-03 0.23721  6.85400E-04 0.27923  1.42547E-03 0.18850  2.31650E-03 0.13595  5.48280E-04 0.31177  5.83745E-04 0.28800  2.35335E-05 0.75177 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.44151E-01 0.10970  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 3.3E-09  2.92467E-01 0.0E+00  6.66488E-01 7.6E-09  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.66990E-03 0.08504  1.76013E-04 0.51669  1.04925E-03 0.23933  6.55247E-04 0.27024  1.41615E-03 0.18083  2.31513E-03 0.12463  4.89736E-04 0.27123  5.47243E-04 0.28357  2.11310E-05 0.70282 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.38099E-01 0.09457  1.24667E-02 5.6E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 5.4E-09  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -4.03302E+02 0.07832 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  1.91872E-05 0.00442 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  1.94791E-05 0.00414 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.53211E-03 0.01386 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.93379E+02 0.01546 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.51173E-07 0.00264 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.67682E-05 0.00157  1.67678E-05 0.00157  1.67205E-05 0.02012 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  7.66920E-05 0.00355  7.67010E-05 0.00352  7.68527E-05 0.04860 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.09559E-01 0.00237  2.09531E-01 0.00235  2.18679E-01 0.04459 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.36528E+01 0.03857 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.14817E+01 0.00081  5.05696E+01 0.00125 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.87227E+04 0.01183  4.62339E+05 0.00733  1.04062E+06 0.00150  1.88420E+06 0.00178  1.99412E+06 0.00148  1.85477E+06 0.00116  1.70537E+06 0.00186  1.50251E+06 0.00139  1.33889E+06 0.00054  1.20874E+06 0.00298  1.10872E+06 0.00171  1.02518E+06 9.6E-05  9.44415E+05 0.00048  9.04754E+05 0.00182  8.69457E+05 0.00092  7.35912E+05 0.00193  7.25225E+05 0.00180  6.94186E+05 0.00121  6.59422E+05 0.00074  1.21553E+06 0.00278  1.05356E+06 0.00298  6.84557E+05 0.00065  4.08434E+05 0.00358  4.32471E+05 0.00375  3.80285E+05 0.00396  2.98649E+05 0.00370  4.94006E+05 0.00366  1.00645E+05 0.00705  1.26320E+05 0.00157  1.15859E+05 0.00324  6.72946E+04 0.00471  1.18464E+05 0.00462  8.14159E+04 0.00166  6.89129E+04 0.00546  1.30458E+04 0.00972  1.31489E+04 0.00735  1.37028E+04 0.00896  1.44075E+04 0.01254  1.40590E+04 0.01013  1.46031E+04 0.00257  1.45113E+04 0.00152  1.42860E+04 0.00330  2.68949E+04 0.00924  4.46553E+04 0.00678  5.93746E+04 0.00876  1.71697E+05 0.00321  2.04038E+05 0.00528  2.29355E+05 0.00642  1.42377E+05 0.00596  9.60878E+04 0.00620  6.86735E+04 0.00462  7.22457E+04 0.00898  1.15649E+05 0.00394  1.24318E+05 0.00585  1.74890E+05 0.00405  1.82341E+05 0.00478  1.76537E+05 0.00282  8.12372E+04 0.00124  4.71670E+04 0.00356  2.98629E+04 0.00603  2.43889E+04 0.00303  2.18351E+04 0.00735  1.65362E+04 0.01158  1.05220E+04 0.00946  9.05671E+03 0.00275  7.69454E+03 0.01084  6.10350E+03 0.02070  4.52542E+03 0.00506  2.80807E+03 0.01489  9.81151E+02 0.04696 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.23581E+00 0.00078 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32953E+02 0.00102  9.16762E+00 0.00400 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.44861E-01 0.00022  6.07472E-01 0.00027 ];
INF_CAPT                  (idx, [1:   4]) = [  2.50324E-03 0.00071  8.57429E-03 0.00307 ];
INF_ABS                   (idx, [1:   4]) = [  4.85498E-03 0.00091  1.93972E-02 0.00187 ];
INF_FISS                  (idx, [1:   4]) = [  2.35175E-03 0.00113  1.08229E-02 0.00314 ];
INF_NSF                   (idx, [1:   4]) = [  5.75755E-03 0.00112  2.63662E-02 0.00314 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44820E+00 1.7E-05  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 8.0E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.64464E-08 0.00086  1.67003E-06 0.00125 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.40011E-01 0.00023  5.88088E-01 0.00026 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89528E-02 0.00094  3.96916E-02 0.00316 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05626E-02 0.00191  5.06985E-03 0.03129 ];
INF_SCATT3                (idx, [1:   4]) = [  1.15826E-03 0.02647  1.40606E-03 0.08366 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.34411E-04 0.06589  6.76203E-04 0.14655 ];
INF_SCATT5                (idx, [1:   4]) = [  7.31094E-05 0.25646  3.89733E-04 0.18138 ];
INF_SCATT6                (idx, [1:   4]) = [  4.25744E-04 0.02219  9.33473E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  8.32970E-05 0.12843  2.42424E-04 0.22481 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.40068E-01 0.00023  5.88088E-01 0.00026 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.89655E-02 0.00095  3.96916E-02 0.00316 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05654E-02 0.00191  5.06985E-03 0.03129 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.15827E-03 0.02668  1.40606E-03 0.08366 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.34434E-04 0.06569  6.76203E-04 0.14655 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.33549E-05 0.25222  3.89733E-04 0.18138 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.25492E-04 0.02244  9.33473E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  8.32395E-05 0.13033  2.42424E-04 0.22481 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49826E-01 0.00049  5.64250E-01 0.00050 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33426E+00 0.00049  5.90755E-01 0.00050 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.79831E-03 0.00093  1.93972E-02 0.00187 ];
INF_REMXS                 (idx, [1:   4]) = [  7.53879E-03 0.00092  3.68003E-02 0.00225 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.37322E-01 0.00024  2.68865E-03 0.00434  1.74166E-02 0.00325  5.70672E-01 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  3.92247E-02 0.00086 -2.71884E-04 0.02057 -1.28354E-03 0.00821  4.09752E-02 0.00319 ];
INF_S2                    (idx, [1:   8]) = [  1.06653E-02 0.00186 -1.02711E-04 0.02053 -6.83894E-04 0.02931  5.75374E-03 0.02473 ];
INF_S3                    (idx, [1:   8]) = [  1.21458E-03 0.02331 -5.63163E-05 0.05754 -2.95191E-04 0.14564  1.70125E-03 0.05209 ];
INF_S4                    (idx, [1:   8]) = [ -5.12892E-04 0.06871 -2.15190E-05 0.03245 -1.52753E-04 0.11691  8.28956E-04 0.09857 ];
INF_S5                    (idx, [1:   8]) = [  8.53236E-05 0.22284 -1.22142E-05 0.10896 -9.99477E-05 0.07100  4.89681E-04 0.14770 ];
INF_S6                    (idx, [1:   8]) = [  4.30828E-04 0.02576 -5.08397E-06 0.38849 -5.65488E-05 0.11550  1.49896E-04 0.65131 ];
INF_S7                    (idx, [1:   8]) = [  8.50723E-05 0.12717 -1.77528E-06 0.92925 -2.16843E-05 0.24004  2.64108E-04 0.22455 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37379E-01 0.00024  2.68865E-03 0.00434  1.74166E-02 0.00325  5.70672E-01 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  3.92374E-02 0.00087 -2.71884E-04 0.02057 -1.28354E-03 0.00821  4.09752E-02 0.00319 ];
INF_SP2                   (idx, [1:   8]) = [  1.06681E-02 0.00186 -1.02711E-04 0.02053 -6.83894E-04 0.02931  5.75374E-03 0.02473 ];
INF_SP3                   (idx, [1:   8]) = [  1.21459E-03 0.02351 -5.63163E-05 0.05754 -2.95191E-04 0.14564  1.70125E-03 0.05209 ];
INF_SP4                   (idx, [1:   8]) = [ -5.12915E-04 0.06850 -2.15190E-05 0.03245 -1.52753E-04 0.11691  8.28956E-04 0.09857 ];
INF_SP5                   (idx, [1:   8]) = [  8.55691E-05 0.21906 -1.22142E-05 0.10896 -9.99477E-05 0.07100  4.89681E-04 0.14770 ];
INF_SP6                   (idx, [1:   8]) = [  4.30576E-04 0.02591 -5.08397E-06 0.38849 -5.65488E-05 0.11550  1.49896E-04 0.65131 ];
INF_SP7                   (idx, [1:   8]) = [  8.50148E-05 0.12942 -1.77528E-06 0.92925 -2.16843E-05 0.24004  2.64108E-04 0.22455 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.71478E-01 0.00145  8.61205E-01 0.00888 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.76354E-01 0.00255  1.16508E+00 0.03073 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.75481E-01 0.00068  1.19470E+00 0.00758 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.63017E-01 0.00156  5.59779E-01 0.01202 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.22785E+00 0.00145  3.87116E-01 0.00889 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.20620E+00 0.00255  2.86659E-01 0.03149 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21001E+00 0.00068  2.79042E-01 0.00763 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.26735E+00 0.00156  5.95647E-01 0.01212 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.49027E-03 0.02559  2.48567E-04 0.13437  1.15848E-03 0.05504  7.23247E-04 0.08229  1.58128E-03 0.05082  2.26336E-03 0.04362  7.01632E-04 0.07826  6.59571E-04 0.09562  1.54141E-04 0.15497 ];
LAMBDA                    (idx, [1:  18]) = [  4.05319E-01 0.03114  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];

