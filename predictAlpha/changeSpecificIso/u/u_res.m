
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   1]) = 'u' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/changeSpecificIso/u' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 14:37:49 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:57:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701290269096 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 4 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   4]) = [  1.21531E+00  9.16107E-01  9.38750E-01  9.29836E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.37090E-01 0.00018  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.62910E-01 0.00014  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54711E-01 9.0E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11247E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15413E+01 0.00041  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40196E-01 3.6E-06  5.94267E-02 5.7E-05  3.77331E-04 0.00063  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.07299E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.05583E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11879E+02 0.00020  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.03651E+02 0.00037  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 10000876 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00009E+05 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00009E+05 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.82119E+02 ;
RUNNING_TIME              (idx, 1)        =  7.94674E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.90217E-01  2.90217E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.38333E-03  8.38333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.91688E+01  7.91688E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.94666E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.55012 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.94878E+00 0.00912 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.93023E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 2247.22 ;
MEMSIZE                   (idx, 1)        = 2179.00 ;
XS_MEMSIZE                (idx, 1)        = 1076.75 ;
MAT_MEMSIZE               (idx, 1)        = 433.37 ;
RES_MEMSIZE               (idx, 1)        = 0.93 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 667.95 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 68.22 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 567710 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92864E-06 0.00024  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.69003E-02 0.00264 ];
U235_FISS                 (idx, [1:   4]) = [  4.17366E-01 0.00044  9.99191E-01 1.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.38072E-04 0.01682  8.09329E-04 0.01680 ];
U235_CAPT                 (idx, [1:   4]) = [  1.61169E-01 0.00079  3.94410E-01 0.00062 ];
U238_CAPT                 (idx, [1:   4]) = [  1.55671E-02 0.00256  3.80968E-02 0.00263 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 10000876 1.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.28885E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 10000876 1.00729E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 4088556 4.11569E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 4196128 4.20706E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1716192 1.75015E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 10000876 1.00729E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.74530E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35415E-11 0.00028 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02169E+00 0.00028 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.17846E-01 0.00028 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.08389E-01 0.00024 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.26235E-01 0.00015 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92864E-01 0.00024 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.40734E+02 0.00024 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.73765E-01 0.00073 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.05447E+01 0.00026 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06699E+00 0.00038 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.44427E-01 0.00052 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.42478E-01 0.00060 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.86083E+00 0.00084 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43678E-01 0.00016 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77845E-01 5.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.24691E+00 0.00031 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02868E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44513E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02863E+00 0.00038  1.02114E+00 0.00036  7.54652E-03 0.00567 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02922E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02904E+00 0.00040 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02922E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.24760E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31253E+01 0.00017 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31233E+01 0.00012 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.98948E-05 0.00227 ];
IMP_EALF                  (idx, [1:   2]) = [  3.99694E-05 0.00158 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01923E-01 0.00253 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.01841E-01 0.00051 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.47745E-03 0.00442  2.17292E-04 0.01916  9.71989E-04 0.00973  6.26314E-04 0.01164  1.28815E-03 0.00882  2.05067E-03 0.00727  6.02713E-04 0.01258  5.65716E-04 0.01375  1.54601E-04 0.02699 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.17211E-01 0.00621  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 4.4E-09  2.92467E-01 0.0E+00  6.66488E-01 3.7E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.41255E-03 0.00635  2.61892E-04 0.03368  1.09566E-03 0.01698  7.30095E-04 0.01763  1.52150E-03 0.01482  2.31492E-03 0.01018  6.77348E-04 0.01985  6.38629E-04 0.01910  1.72509E-04 0.04116 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.11664E-01 0.00931  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.5E-09  1.33042E-01 4.4E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.39790E-05 0.00220  2.39795E-05 0.00222  2.39390E-05 0.02411 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.46651E-05 0.00216  2.46657E-05 0.00219  2.46216E-05 0.02409 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.32193E-03 0.00565  2.61473E-04 0.02883  1.07832E-03 0.01408  7.34479E-04 0.01853  1.45492E-03 0.01311  2.30518E-03 0.00987  6.83139E-04 0.01818  6.31510E-04 0.01953  1.72905E-04 0.03918 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.14282E-01 0.00901  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 5.1E-09  2.92467E-01 0.0E+00  6.66488E-01 3.7E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.21197E-05 0.02381  2.21162E-05 0.02381  2.22308E-05 0.06675 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.27539E-05 0.02381  2.27503E-05 0.02382  2.28645E-05 0.06667 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.95067E-03 0.03087  2.52835E-04 0.10785  1.00399E-03 0.05418  7.08279E-04 0.07011  1.34901E-03 0.05255  2.15216E-03 0.04371  6.95163E-04 0.06579  6.14082E-04 0.07040  1.75153E-04 0.11893 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.25905E-01 0.02687  1.24667E-02 2.3E-09  2.82917E-02 0.0E+00  4.25244E-02 1.9E-09  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.1E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.91128E-03 0.03098  2.49928E-04 0.10683  1.01905E-03 0.05375  6.95443E-04 0.06871  1.34632E-03 0.05231  2.13424E-03 0.04228  6.92208E-04 0.06550  5.99886E-04 0.06790  1.74209E-04 0.11776 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.23052E-01 0.02586  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 4.6E-09  2.92467E-01 0.0E+00  6.66488E-01 4.8E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.15006E+02 0.02141 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.36473E-05 0.00107 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.43240E-05 0.00100 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35176E-03 0.00273 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.10910E+02 0.00275 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.13645E-07 0.00091 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59474E-05 0.00034  1.59482E-05 0.00035  1.58331E-05 0.00443 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13248E-04 0.00097  1.13279E-04 0.00098  1.08916E-04 0.01212 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19035E-01 0.00064  2.18936E-01 0.00064  2.34879E-01 0.00975 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29095E+01 0.00812 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.05583E+01 0.00023  4.95701E+01 0.00029 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.81911E+05 0.00086  4.61326E+06 0.00143  1.04001E+07 0.00083  1.88700E+07 0.00053  1.99287E+07 0.00031  1.85006E+07 0.00014  1.69974E+07 0.00024  1.50635E+07 0.00036  1.33950E+07 0.00043  1.20650E+07 0.00017  1.10779E+07 0.00026  1.02650E+07 0.00014  9.46255E+06 0.00019  9.04436E+06 0.00053  8.67474E+06 0.00068  7.34599E+06 0.00040  7.25932E+06 0.00027  6.93965E+06 0.00065  6.60774E+06 0.00037  1.21921E+07 0.00013  1.06025E+07 0.00020  6.90232E+06 0.00038  4.12689E+06 0.00042  4.36021E+06 0.00097  3.81746E+06 0.00077  2.99774E+06 0.00079  4.90321E+06 0.00107  9.77269E+05 0.00117  1.20974E+06 0.00211  1.09151E+06 0.00073  6.21397E+05 0.00098  1.07019E+06 0.00111  7.15138E+05 0.00146  5.84339E+05 0.00156  1.07877E+05 0.00342  1.05581E+05 0.00211  1.08478E+05 0.00447  1.11018E+05 0.00275  1.09544E+05 0.00161  1.07510E+05 0.00398  1.10175E+05 0.00447  1.03205E+05 0.00303  1.92750E+05 0.00256  3.03851E+05 0.00163  3.78930E+05 0.00165  9.55755E+05 0.00191  9.28851E+05 0.00178  8.84970E+05 0.00215  5.04691E+05 0.00309  3.26971E+05 0.00251  2.31940E+05 0.00194  2.45555E+05 0.00112  3.99554E+05 0.00112  4.56626E+05 0.00220  7.45281E+05 0.00152  1.04703E+06 0.00113  1.70225E+06 0.00151  1.31782E+06 0.00110  1.10435E+06 0.00073  9.00805E+05 0.00134  8.95821E+05 0.00155  9.81423E+05 0.00121  9.12278E+05 0.00176  6.68672E+05 0.00131  6.64799E+05 0.00200  6.40285E+05 0.00145  5.89085E+05 0.00240  4.95628E+05 0.00186  3.51095E+05 0.00178  1.35549E+05 0.00206 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.24712E+00 0.00029 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32229E+02 0.00020  8.50453E+00 0.00109 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43398E-01 7.0E-05  6.04101E-01 0.00015 ];
INF_CAPT                  (idx, [1:   4]) = [  2.40770E-03 0.00029  1.05852E-02 0.00068 ];
INF_ABS                   (idx, [1:   4]) = [  4.74669E-03 0.00029  2.33509E-02 0.00049 ];
INF_FISS                  (idx, [1:   4]) = [  2.33899E-03 0.00031  1.27657E-02 0.00063 ];
INF_NSF                   (idx, [1:   4]) = [  5.72653E-03 0.00031  3.10990E-02 0.00063 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44829E+00 2.3E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.4E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.12921E-08 0.00065  2.89344E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38650E-01 7.3E-05  5.80743E-01 0.00014 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89939E-02 0.00021  4.06104E-02 0.00157 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05615E-02 0.00072  4.88743E-03 0.00601 ];
INF_SCATT3                (idx, [1:   4]) = [  1.06832E-03 0.00423  1.18520E-03 0.02966 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.82717E-04 0.00786  3.49736E-04 0.15371 ];
INF_SCATT5                (idx, [1:   4]) = [  8.75625E-05 0.04114  1.39515E-04 0.26120 ];
INF_SCATT6                (idx, [1:   4]) = [  4.21249E-04 0.01135  5.48549E-05 0.32272 ];
INF_SCATT7                (idx, [1:   4]) = [  4.36907E-05 0.12132  4.61651E-05 0.61655 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38704E-01 7.3E-05  5.80743E-01 0.00014 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.90066E-02 0.00021  4.06104E-02 0.00157 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05640E-02 0.00072  4.88743E-03 0.00601 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.06893E-03 0.00424  1.18520E-03 0.02966 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.82505E-04 0.00780  3.49736E-04 0.15371 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.76471E-05 0.04132  1.39515E-04 0.26120 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.21267E-04 0.01125  5.48549E-05 0.32272 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.36760E-05 0.12147  4.61651E-05 0.61655 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49173E-01 0.00017  5.59366E-01 0.00017 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33776E+00 0.00017  5.95913E-01 0.00017 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.69196E-03 0.00029  2.33509E-02 0.00049 ];
INF_REMXS                 (idx, [1:   4]) = [  6.43411E-03 0.00018  2.40374E-02 0.00029 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.36964E-01 6.8E-05  1.68607E-03 0.00103  6.79133E-04 0.00511  5.80064E-01 0.00015 ];
INF_S1                    (idx, [1:   8]) = [  3.91575E-02 0.00020 -1.63556E-04 0.00496  1.79863E-04 0.00631  4.04305E-02 0.00158 ];
INF_S2                    (idx, [1:   8]) = [  1.06424E-02 0.00071 -8.08716E-05 0.00623  1.26150E-05 0.11380  4.87481E-03 0.00607 ];
INF_S3                    (idx, [1:   8]) = [  1.15742E-03 0.00382 -8.90979E-05 0.00469 -1.48840E-05 0.12661  1.20009E-03 0.02895 ];
INF_S4                    (idx, [1:   8]) = [ -5.53715E-04 0.00741 -2.90026E-05 0.01944 -1.47499E-05 0.07498  3.64486E-04 0.14651 ];
INF_S5                    (idx, [1:   8]) = [  7.99558E-05 0.04548  7.60665E-06 0.04268 -9.91455E-06 0.05950  1.49430E-04 0.24295 ];
INF_S6                    (idx, [1:   8]) = [  4.14066E-04 0.01137  7.18292E-06 0.06766 -5.81114E-06 0.26508  6.06660E-05 0.28917 ];
INF_S7                    (idx, [1:   8]) = [  4.40778E-05 0.12010 -3.87066E-07 0.42802 -4.12820E-06 0.16394  5.02933E-05 0.56866 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37018E-01 6.8E-05  1.68607E-03 0.00103  6.79133E-04 0.00511  5.80064E-01 0.00015 ];
INF_SP1                   (idx, [1:   8]) = [  3.91702E-02 0.00020 -1.63556E-04 0.00496  1.79863E-04 0.00631  4.04305E-02 0.00158 ];
INF_SP2                   (idx, [1:   8]) = [  1.06448E-02 0.00071 -8.08716E-05 0.00623  1.26150E-05 0.11380  4.87481E-03 0.00607 ];
INF_SP3                   (idx, [1:   8]) = [  1.15803E-03 0.00384 -8.90979E-05 0.00469 -1.48840E-05 0.12661  1.20009E-03 0.02895 ];
INF_SP4                   (idx, [1:   8]) = [ -5.53502E-04 0.00734 -2.90026E-05 0.01944 -1.47499E-05 0.07498  3.64486E-04 0.14651 ];
INF_SP5                   (idx, [1:   8]) = [  8.00405E-05 0.04568  7.60665E-06 0.04268 -9.91455E-06 0.05950  1.49430E-04 0.24295 ];
INF_SP6                   (idx, [1:   8]) = [  4.14084E-04 0.01127  7.18292E-06 0.06766 -5.81114E-06 0.26508  6.06660E-05 0.28917 ];
INF_SP7                   (idx, [1:   8]) = [  4.40631E-05 0.12026 -3.87066E-07 0.42802 -4.12820E-06 0.16394  5.02933E-05 0.56866 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70324E-01 0.00039  8.47353E-01 0.00393 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.74753E-01 0.00045  1.13887E+00 0.00381 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.74819E-01 0.00049  1.15332E+00 0.00728 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61821E-01 0.00062  5.57064E-01 0.00488 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23309E+00 0.00039  3.93406E-01 0.00392 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21321E+00 0.00045  2.92704E-01 0.00379 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21292E+00 0.00049  2.89082E-01 0.00725 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27314E+00 0.00062  5.98432E-01 0.00484 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.41255E-03 0.00635  2.61892E-04 0.03368  1.09566E-03 0.01698  7.30095E-04 0.01763  1.52150E-03 0.01482  2.31492E-03 0.01018  6.77348E-04 0.01985  6.38629E-04 0.01910  1.72509E-04 0.04116 ];
LAMBDA                    (idx, [1:  18]) = [  4.11664E-01 0.00931  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.5E-09  1.33042E-01 4.4E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

