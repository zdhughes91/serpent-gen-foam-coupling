/**************************************************************
******************* Control Drum Geometry *********************
***************************************************************/
% ------------- Control Drum is Universe # 37 ---------------- %
% ------------- transformation card for u37 is at bottom ----- %

% include materials.txt



% - surfaces
surf outerDiam      cylz 0 0 4.92 0 132.08
surf boronInnerDiam cylz 0 0 4.7  0 132.08 
surf xplane         px   0
surf yplane         py   0
surf tieTubeOR      cylz 0 0 2.46 0 132.08 %- outer edge of tie tube is 1/2 that of control drum

% - transforming y plane to get to 120 degrees
trans S yplane rot 0 0 0 0 0 1 -30

% - lattices
pin 34 % - propellant rings
propellant 0.23749
beryllium

pin 35 % - tie rods
inconel718 0.23749
beryllium

lat 36 4 0 0 3
12 1.64 0 34 34 34 34 34 34 34 34 34 34 34 34
4  2.46 0 35 35 35 35
12 3.28 0 34 34 34 34 34 34 34 34 34 34 34 34

% - cells
cell contrDrum  37 fill 36    -boronInnerDiam
cell contrDrum2 37 beryllium  -outerDiam boronInnerDiam xplane yplane
cell contrDrum3 37 beryllium  -outerDiam boronInnerDiam -xplane yplane
cell contrDrum4 37 beryllium  -outerDiam boronInnerDiam  xplane -yplane
cell boronPlate 37 absorber   -outerDiam boronInnerDiam -xplane -yplane
cell fillBeryll 37 beryllium   outerDiam
cell tieTube    38 inconel718 -tieTubeOR
cell fillBeryl2 38 beryllium   tieTubeOR

% - transformation card for u37 - %
trans U 37 rot 0 0 0 0 0 1 90
% ------------------------------- %

% set pop 10000 100 50
% plot 3 5000 5000 1 -5 5 -5 5 % xy plot