
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   4]) = 'u238' ;
WORKING_DIRECTORY         (idx, [1:  83]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/changeSpecificIso/u238' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 14:37:22 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 16:00:51 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701290242899 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 4 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   4]) = [  9.52943E-01  9.25626E-01  1.08726E+00  1.03417E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.41293E-01 0.00017  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.58707E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54535E-01 8.8E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11531E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14330E+01 0.00044  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40316E-01 3.5E-06  5.93111E-02 5.6E-05  3.73127E-04 0.00064  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.08386E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.06665E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11971E+02 0.00019  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.05604E+02 0.00039  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 9999973 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99997E+04 0.00058 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99997E+04 0.00058 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.98021E+02 ;
RUNNING_TIME              (idx, 1)        =  8.34819E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.38683E-01  2.38683E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.01667E-03  7.01667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  8.32362E+01  8.32362E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  8.34815E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.56989 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.94345E+00 0.00960 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.93752E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 2307.00 ;
MEMSIZE                   (idx, 1)        = 2241.51 ;
XS_MEMSIZE                (idx, 1)        = 1117.92 ;
MAT_MEMSIZE               (idx, 1)        = 454.71 ;
RES_MEMSIZE               (idx, 1)        = 0.93 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 667.95 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 65.49 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 595679 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92910E-06 0.00026  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.77377E-02 0.00270 ];
U235_FISS                 (idx, [1:   4]) = [  4.18573E-01 0.00044  9.99222E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.26052E-04 0.01655  7.78375E-04 0.01657 ];
U235_CAPT                 (idx, [1:   4]) = [  1.58048E-01 0.00075  3.88272E-01 0.00056 ];
U238_CAPT                 (idx, [1:   4]) = [  1.59991E-02 0.00270  3.93043E-02 0.00263 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 9999973 1.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.35294E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 9999973 1.00735E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 4071904 4.09962E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 4207319 4.21891E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1720750 1.75500E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 9999973 1.00735E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.80887E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35724E-11 0.00030 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02401E+00 0.00030 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.18800E-01 0.00030 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.06945E-01 0.00029 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.25745E-01 0.00016 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92910E-01 0.00026 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.41024E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.74255E-01 0.00077 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.06571E+01 0.00025 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06757E+00 0.00037 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.48473E-01 0.00055 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.46809E-01 0.00066 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.78116E+00 0.00083 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43309E-01 0.00014 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77696E-01 6.1E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25115E+00 0.00037 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03157E+00 0.00041 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44510E+00 2.3E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03153E+00 0.00041  1.02400E+00 0.00041  7.56686E-03 0.00553 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03154E+00 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03133E+00 0.00045 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03154E+00 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  1.25116E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31726E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31714E+01 0.00014 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.80525E-05 0.00250 ];
IMP_EALF                  (idx, [1:   2]) = [  3.80936E-05 0.00184 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01550E-01 0.00235 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.01467E-01 0.00057 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.50046E-03 0.00412  2.15333E-04 0.02421  9.75278E-04 0.01108  6.47725E-04 0.01236  1.28927E-03 0.00887  2.05068E-03 0.00681  5.96886E-04 0.01182  5.67026E-04 0.01169  1.58272E-04 0.02495 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.17808E-01 0.00596  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.40627E-03 0.00613  2.39903E-04 0.03185  1.10153E-03 0.01616  7.47040E-04 0.01999  1.48086E-03 0.01310  2.33682E-03 0.01036  6.70156E-04 0.02068  6.44422E-04 0.02092  1.85531E-04 0.03681 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.19072E-01 0.00885  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 4.6E-09  2.92467E-01 0.0E+00  6.66488E-01 3.7E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.40530E-05 0.00205  2.40627E-05 0.00208  2.27543E-05 0.02349 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48113E-05 0.00209  2.48213E-05 0.00211  2.34711E-05 0.02349 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.33746E-03 0.00539  2.47924E-04 0.03294  1.09701E-03 0.01466  7.57699E-04 0.01753  1.44035E-03 0.01259  2.30858E-03 0.00979  6.80754E-04 0.01836  6.19604E-04 0.01877  1.85540E-04 0.03908 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.16514E-01 0.00868  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.7E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.20124E-05 0.02365  2.20221E-05 0.02367  2.02122E-05 0.07059 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.27079E-05 0.02365  2.27179E-05 0.02367  2.08486E-05 0.07061 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.85529E-03 0.03192  2.45603E-04 0.09138  9.17243E-04 0.05246  7.15792E-04 0.07235  1.42140E-03 0.04977  2.23392E-03 0.04504  6.11608E-04 0.07307  5.71078E-04 0.07550  1.38643E-04 0.13908 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.98367E-01 0.02933  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 1.9E-09  1.33042E-01 5.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.8E-09  1.63478E+00 0.0E+00  3.55460E+00 5.8E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.81017E-03 0.03143  2.49157E-04 0.09320  9.53953E-04 0.05201  7.04299E-04 0.07150  1.41321E-03 0.04885  2.20841E-03 0.04377  5.95177E-04 0.07053  5.44972E-04 0.07300  1.40986E-04 0.13072 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.94256E-01 0.02869  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 1.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.1E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.12282E+02 0.02351 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.37316E-05 0.00099 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.44795E-05 0.00096 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.34592E-03 0.00342 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.09564E+02 0.00349 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.15009E-07 0.00103 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59797E-05 0.00039  1.59810E-05 0.00039  1.57920E-05 0.00371 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.12175E-04 0.00112  1.12217E-04 0.00111  1.06327E-04 0.01326 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.22606E-01 0.00066  2.22507E-01 0.00066  2.38130E-01 0.00872 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30027E+01 0.00939 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.06665E+01 0.00023  4.97361E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.77000E+05 0.00330  4.60290E+06 0.00130  1.03885E+07 0.00029  1.88383E+07 0.00039  1.99246E+07 0.00022  1.84863E+07 0.00037  1.70003E+07 0.00033  1.50689E+07 0.00053  1.33887E+07 0.00042  1.20629E+07 0.00027  1.10783E+07 0.00027  1.02623E+07 0.00031  9.45910E+06 0.00044  9.03971E+06 0.00036  8.66470E+06 0.00046  7.34713E+06 0.00048  7.25815E+06 0.00081  6.94495E+06 0.00074  6.60112E+06 0.00073  1.21933E+07 0.00034  1.06253E+07 0.00044  6.92565E+06 0.00086  4.15407E+06 0.00068  4.40922E+06 0.00049  3.87415E+06 0.00073  3.06809E+06 0.00090  5.02017E+06 0.00113  9.99959E+05 0.00152  1.23416E+06 0.00229  1.11235E+06 0.00092  6.31231E+05 0.00168  1.08942E+06 0.00181  7.27324E+05 0.00075  5.94174E+05 0.00112  1.09215E+05 0.00293  1.07305E+05 0.00156  1.09122E+05 0.00119  1.12599E+05 0.00279  1.11492E+05 0.00214  1.09514E+05 0.00272  1.11735E+05 0.00165  1.04706E+05 0.00224  1.95891E+05 0.00281  3.07974E+05 0.00282  3.84477E+05 0.00211  9.71926E+05 0.00164  9.43167E+05 0.00104  8.99437E+05 0.00181  5.10992E+05 0.00096  3.31350E+05 0.00130  2.33709E+05 0.00251  2.46742E+05 0.00194  4.04164E+05 0.00271  4.61908E+05 0.00160  7.56553E+05 0.00247  1.05663E+06 0.00198  1.71611E+06 0.00184  1.32614E+06 0.00227  1.11168E+06 0.00164  9.05046E+05 0.00199  9.01298E+05 0.00197  9.87443E+05 0.00137  9.15575E+05 0.00211  6.72465E+05 0.00195  6.68820E+05 0.00142  6.45397E+05 0.00150  5.92116E+05 0.00181  4.98505E+05 0.00164  3.53344E+05 0.00252  1.36349E+05 0.00175 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.25085E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32448E+02 0.00014  8.57623E+00 0.00131 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43393E-01 7.7E-05  6.03456E-01 0.00014 ];
INF_CAPT                  (idx, [1:   4]) = [  2.38710E-03 0.00026  1.05849E-02 0.00111 ];
INF_ABS                   (idx, [1:   4]) = [  4.70973E-03 0.00024  2.35482E-02 0.00071 ];
INF_FISS                  (idx, [1:   4]) = [  2.32263E-03 0.00036  1.29632E-02 0.00090 ];
INF_NSF                   (idx, [1:   4]) = [  5.68658E-03 0.00036  3.15802E-02 0.00090 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44834E+00 5.1E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 2.5E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.18241E-08 0.00068  2.88965E-06 0.00026 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38683E-01 8.2E-05  5.79895E-01 0.00016 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89881E-02 0.00030  4.06170E-02 0.00121 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05663E-02 0.00033  4.95238E-03 0.00470 ];
INF_SCATT3                (idx, [1:   4]) = [  1.06674E-03 0.00291  1.27560E-03 0.01763 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.77625E-04 0.00871  3.63985E-04 0.02848 ];
INF_SCATT5                (idx, [1:   4]) = [  8.54266E-05 0.04522  1.27963E-04 0.34343 ];
INF_SCATT6                (idx, [1:   4]) = [  4.28477E-04 0.01148  3.48921E-05 0.59704 ];
INF_SCATT7                (idx, [1:   4]) = [  4.46478E-05 0.06667  8.15468E-05 0.35274 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38738E-01 8.2E-05  5.79895E-01 0.00016 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.90008E-02 0.00030  4.06170E-02 0.00121 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05690E-02 0.00033  4.95238E-03 0.00470 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.06736E-03 0.00286  1.27560E-03 0.01763 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.77522E-04 0.00874  3.63985E-04 0.02848 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.55546E-05 0.04550  1.27963E-04 0.34343 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.28534E-04 0.01156  3.48921E-05 0.59704 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.47176E-05 0.06710  8.15468E-05 0.35274 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49369E-01 0.00021  5.58660E-01 0.00022 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33671E+00 0.00021  5.96666E-01 0.00022 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.65461E-03 0.00027  2.35482E-02 0.00071 ];
INF_REMXS                 (idx, [1:   4]) = [  6.42097E-03 0.00010  2.42403E-02 0.00085 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.36972E-01 7.8E-05  1.71050E-03 0.00083  6.79332E-04 0.00208  5.79216E-01 0.00016 ];
INF_S1                    (idx, [1:   8]) = [  3.91476E-02 0.00031 -1.59420E-04 0.00653  1.86617E-04 0.00498  4.04304E-02 0.00119 ];
INF_S2                    (idx, [1:   8]) = [  1.06504E-02 0.00028 -8.40560E-05 0.00698  1.62733E-05 0.03904  4.93611E-03 0.00461 ];
INF_S3                    (idx, [1:   8]) = [  1.15803E-03 0.00276 -9.12925E-05 0.00361 -1.47947E-05 0.06788  1.29040E-03 0.01800 ];
INF_S4                    (idx, [1:   8]) = [ -5.47052E-04 0.00901 -3.05737E-05 0.01208 -1.47757E-05 0.04047  3.78761E-04 0.02719 ];
INF_S5                    (idx, [1:   8]) = [  7.76750E-05 0.05152  7.75161E-06 0.02826 -8.90975E-06 0.08489  1.36872E-04 0.31893 ];
INF_S6                    (idx, [1:   8]) = [  4.20929E-04 0.01191  7.54784E-06 0.04563 -7.46465E-06 0.08573  4.23567E-05 0.50119 ];
INF_S7                    (idx, [1:   8]) = [  4.49102E-05 0.06645 -2.62391E-07 1.00000 -5.13419E-06 0.18418  8.66810E-05 0.32997 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37027E-01 7.8E-05  1.71050E-03 0.00083  6.79332E-04 0.00208  5.79216E-01 0.00016 ];
INF_SP1                   (idx, [1:   8]) = [  3.91602E-02 0.00031 -1.59420E-04 0.00653  1.86617E-04 0.00498  4.04304E-02 0.00119 ];
INF_SP2                   (idx, [1:   8]) = [  1.06531E-02 0.00027 -8.40560E-05 0.00698  1.62733E-05 0.03904  4.93611E-03 0.00461 ];
INF_SP3                   (idx, [1:   8]) = [  1.15865E-03 0.00271 -9.12925E-05 0.00361 -1.47947E-05 0.06788  1.29040E-03 0.01800 ];
INF_SP4                   (idx, [1:   8]) = [ -5.46948E-04 0.00903 -3.05737E-05 0.01208 -1.47757E-05 0.04047  3.78761E-04 0.02719 ];
INF_SP5                   (idx, [1:   8]) = [  7.78030E-05 0.05183  7.75161E-06 0.02826 -8.90975E-06 0.08489  1.36872E-04 0.31893 ];
INF_SP6                   (idx, [1:   8]) = [  4.20986E-04 0.01201  7.54784E-06 0.04563 -7.46465E-06 0.08573  4.23567E-05 0.50119 ];
INF_SP7                   (idx, [1:   8]) = [  4.49800E-05 0.06682 -2.62391E-07 1.00000 -5.13419E-06 0.18418  8.66810E-05 0.32997 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70489E-01 0.00055  8.40922E-01 0.00186 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.74989E-01 0.00051  1.13158E+00 0.00389 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.75149E-01 0.00057  1.14357E+00 0.00519 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61773E-01 0.00065  5.52772E-01 0.00491 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23234E+00 0.00055  3.96396E-01 0.00186 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21217E+00 0.00051  2.94591E-01 0.00387 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21147E+00 0.00057  2.91516E-01 0.00519 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27337E+00 0.00065  6.03079E-01 0.00491 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.40627E-03 0.00613  2.39903E-04 0.03185  1.10153E-03 0.01616  7.47040E-04 0.01999  1.48086E-03 0.01310  2.33682E-03 0.01036  6.70156E-04 0.02068  6.44422E-04 0.02092  1.85531E-04 0.03681 ];
LAMBDA                    (idx, [1:  18]) = [  4.19072E-01 0.00885  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 4.6E-09  2.92467E-01 0.0E+00  6.66488E-01 3.7E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

