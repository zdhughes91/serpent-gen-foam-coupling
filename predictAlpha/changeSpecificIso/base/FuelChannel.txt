/******************************************
 ********** Fuel Channel Geometry **********
 ******************************************/
% --- 19 of these go in each fuel element --- 
% --- this is just hydrogen surrounded by NbC ---

%include materials.txt  % - need to fill with materials

% ===================== defining surfaces ==== surf <id> <type> x0 , y0 , r, z1 , z2
% - outermost cyliner of fuel channel
surf outCyl cyl 0 0 0.127 0 132.08 

% - 
surf inCyl cyl 0 0 0.122 0 132.08 

% ===================== defining cells ====== cell <name> <u0> <mat> <surf 1> <surf 2>
% ========== universes 10-20 as well as 100-200 are reserved for fuel types


% ----------------------------------------- type 45 ----------------- 
cell hydrogen45 10 propellant -inCyl
cell nioCar45 10 NbC -outCyl inCyl
cell structure45 10 type45 outCyl
cell pureStructure45 100 type45
% ----------------------------------------- type 39 ----------------- 
cell hydrogen39 11 propellant -inCyl
cell nioCar39 11 NbC -outCyl inCyl
cell structure39 11 type39 outCyl
cell pureStructure39 110 type39
% ----------------------------------------- type 34 ----------------- 
cell hydrogen34 12 propellant -inCyl
cell nioCar34 12 NbC -outCyl inCyl
cell structure34 12 type34 outCyl
cell pureStructure34 120 type34
% ----------------------------------------- type 30 ----------------- 
cell hydrogen30 13 propellant -inCyl
cell nioCar30 13 NbC -outCyl inCyl
cell structure30 13 type30 outCyl
cell pureStructure30 130 type30
% ----------------------------------------- type 26 ----------------- 
cell hydrogen26 14 propellant -inCyl
cell nioCar26 14 NbC -outCyl inCyl
cell structure26 14 type26 outCyl
cell pureStructure26 140 type26
% ----------------------------------------- type 22 ----------------- 
cell hydrogen22 15 propellant -inCyl
cell nioCar22 15 NbC -outCyl inCyl
cell structure22 15 type22 outCyl
cell pureStructure22 150 type22
% ----------------------------------------- type 19 -----------------
cell hydrogen19 16 propellant -inCyl
cell nioCar19 16 NbC -outCyl inCyl
cell structure19 16 type19 outCyl
cell pureStructure19 160 type19
% ----------------------------------------- type 17 ----------------- 
cell hydrogen17 17 propellant -inCyl
cell nioCar17 17 NbC -outCyl inCyl
cell structure17 17 type17 outCyl
cell pureStructure17 170 type17
% ----------------------------------------- type 15 -----------------
cell hydrogen15 18 propellant -inCyl
cell nioCar15 18 NbC -outCyl inCyl
cell structure15 18 type15 outCyl
cell pureStructure15 180 type15
% ----------------------------------------- type 13 -----------------  
cell hydrogen13 19 propellant -inCyl
cell nioCar13 19 NbC -outCyl inCyl
cell structure13 19 type13 outCyl
cell pureStructure13 190 type13

% --- for plotting
% --- need to set U#=0 for all for it to plot
% set pop 10000 100 50
% plot 1 5000 5000 0 -0.3 1.3 -10 150