
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   4]) = 'base' ;
WORKING_DIRECTORY         (idx, [1:  83]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/changeSpecificIso/base' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 13:27:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 13:52:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701286033077 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 16 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  16]) = [  1.03624E+00  9.95755E-01  9.98422E-01  1.00287E+00  9.98551E-01  9.94420E-01  9.94084E-01  9.98007E-01  9.98750E-01  9.96718E-01  9.93773E-01  9.97824E-01  9.96627E-01  9.98573E-01  1.00340E+00  9.95993E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.42375E-01 0.00020  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.57625E-01 0.00016  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54645E-01 9.0E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11770E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14217E+01 0.00040  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40353E-01 3.4E-06  5.92749E-02 5.5E-05  3.72493E-04 0.00067  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.08777E+01 0.00022  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.07060E+01 0.00022  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11933E+02 0.00020  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06070E+02 0.00041  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 10000358 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00004E+05 0.00057 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00004E+05 0.00057 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.82425E+02 ;
RUNNING_TIME              (idx, 1)        =  2.50805E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  7.63000E-02  7.63000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  3.91666E-03  3.91666E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.50003E+01  2.50003E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.50796E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 15.24785 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.53053E+01 0.00148 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76169E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 2727.75 ;
MEMSIZE                   (idx, 1)        = 2536.59 ;
XS_MEMSIZE                (idx, 1)        = 1376.75 ;
MAT_MEMSIZE               (idx, 1)        = 490.13 ;
RES_MEMSIZE               (idx, 1)        = 1.37 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.34 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 191.15 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 641913 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92626E-06 0.00027  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.42249E-02 0.00286 ];
U235_FISS                 (idx, [1:   4]) = [  4.19988E-01 0.00047  9.99216E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.29538E-04 0.01638  7.84079E-04 0.01642 ];
U235_CAPT                 (idx, [1:   4]) = [  1.58393E-01 0.00082  3.90397E-01 0.00064 ];
U238_CAPT                 (idx, [1:   4]) = [  1.40151E-02 0.00281  3.45436E-02 0.00278 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 10000358 1.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.34976E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 10000358 1.00735E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 4059870 4.08736E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 4222979 4.23440E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1717509 1.75174E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 10000358 1.00735E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.59466E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.36222E-11 0.00029 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02776E+00 0.00028 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.20336E-01 0.00029 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.05782E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.26118E-01 0.00017 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92626E-01 0.00027 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.41091E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.73882E-01 0.00079 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.06827E+01 0.00026 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06866E+00 0.00034 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.49843E-01 0.00050 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.47811E-01 0.00059 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.76825E+00 0.00077 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43560E-01 0.00014 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77792E-01 5.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25526E+00 0.00037 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03537E+00 0.00039 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44508E+00 2.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.3E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03547E+00 0.00040  1.02777E+00 0.00040  7.59566E-03 0.00565 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03533E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03540E+00 0.00043 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03533E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.25519E+00 0.00022 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31841E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31872E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.76166E-05 0.00245 ];
IMP_EALF                  (idx, [1:   2]) = [  3.74933E-05 0.00177 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01744E-01 0.00215 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.01225E-01 0.00061 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.44903E-03 0.00340  2.10387E-04 0.02033  9.71931E-04 0.01017  6.13680E-04 0.01246  1.28988E-03 0.00813  2.04410E-03 0.00747  5.95124E-04 0.01249  5.66796E-04 0.01304  1.57127E-04 0.02285 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.19820E-01 0.00608  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.32450E-03 0.00641  2.38731E-04 0.03167  1.09024E-03 0.01744  7.03950E-04 0.01718  1.45813E-03 0.01483  2.34948E-03 0.01106  6.67719E-04 0.02122  6.34017E-04 0.02151  1.82239E-04 0.04037 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.19480E-01 0.00935  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.40798E-05 0.00205  2.40969E-05 0.00208  2.17879E-05 0.02531 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49335E-05 0.00200  2.49511E-05 0.00203  2.25579E-05 0.02524 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.33644E-03 0.00586  2.47619E-04 0.03103  1.07113E-03 0.01446  7.15207E-04 0.01602  1.47450E-03 0.01249  2.34393E-03 0.01081  6.67895E-04 0.01937  6.42565E-04 0.01961  1.73598E-04 0.03714 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.16994E-01 0.00899  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.19867E-05 0.02362  2.19883E-05 0.02362  2.09080E-05 0.08011 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.27668E-05 0.02362  2.27685E-05 0.02362  2.16470E-05 0.08011 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.93682E-03 0.03256  2.50356E-04 0.10282  9.90014E-04 0.05580  6.71298E-04 0.06550  1.40289E-03 0.04560  2.27544E-03 0.04135  5.86797E-04 0.06919  5.87934E-04 0.07874  1.72088E-04 0.13037 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.10208E-01 0.03550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.4E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.96290E-03 0.03209  2.53176E-04 0.09916  1.00468E-03 0.05446  6.89782E-04 0.06359  1.39407E-03 0.04626  2.26795E-03 0.04135  5.89176E-04 0.06586  6.02117E-04 0.07674  1.61953E-04 0.12712 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.07335E-01 0.03269  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 1.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.8E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.16800E+02 0.02422 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.36396E-05 0.00107 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.44776E-05 0.00097 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.48123E-03 0.00373 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.16532E+02 0.00407 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.15210E-07 0.00087 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59830E-05 0.00029  1.59834E-05 0.00030  1.59226E-05 0.00456 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.11942E-04 0.00099  1.11981E-04 0.00100  1.06082E-04 0.01255 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23433E-01 0.00058  2.23358E-01 0.00059  2.35443E-01 0.00963 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29121E+01 0.00901 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.07060E+01 0.00022  4.97914E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.86513E+05 0.00328  4.61813E+06 0.00132  1.03848E+07 0.00098  1.88534E+07 0.00040  1.99206E+07 0.00023  1.85047E+07 0.00062  1.70029E+07 0.00027  1.50701E+07 0.00051  1.33859E+07 0.00042  1.20623E+07 0.00038  1.10807E+07 0.00053  1.02636E+07 0.00039  9.45743E+06 0.00030  9.03959E+06 0.00032  8.67180E+06 0.00079  7.34450E+06 0.00081  7.26064E+06 0.00071  6.93786E+06 0.00049  6.60560E+06 0.00051  1.21966E+07 0.00012  1.06335E+07 0.00034  6.93125E+06 0.00033  4.15792E+06 0.00089  4.42364E+06 0.00065  3.89068E+06 0.00033  3.07385E+06 0.00039  5.04556E+06 0.00060  1.00544E+06 0.00094  1.23939E+06 0.00148  1.11809E+06 0.00109  6.34534E+05 0.00184  1.09432E+06 0.00150  7.30580E+05 0.00111  5.98581E+05 0.00123  1.09918E+05 0.00072  1.07787E+05 0.00264  1.09728E+05 0.00383  1.13529E+05 0.00231  1.11721E+05 0.00295  1.10124E+05 0.00307  1.12358E+05 0.00198  1.05086E+05 0.00332  1.96967E+05 0.00448  3.10838E+05 0.00308  3.85452E+05 0.00141  9.74974E+05 0.00210  9.46360E+05 0.00126  9.00907E+05 0.00100  5.12158E+05 0.00135  3.32238E+05 0.00203  2.34354E+05 0.00125  2.47897E+05 0.00099  4.07029E+05 0.00249  4.62591E+05 0.00162  7.58435E+05 0.00154  1.05881E+06 0.00068  1.72089E+06 0.00152  1.32784E+06 0.00119  1.11316E+06 0.00256  9.06898E+05 0.00181  9.02129E+05 0.00142  9.87748E+05 0.00169  9.16679E+05 0.00159  6.71848E+05 0.00117  6.69487E+05 0.00120  6.47279E+05 0.00181  5.92574E+05 0.00078  4.99371E+05 0.00154  3.53012E+05 0.00230  1.36062E+05 0.00177 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.25537E+00 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32502E+02 0.00030  8.58906E+00 0.00088 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43385E-01 6.7E-05  6.03498E-01 0.00026 ];
INF_CAPT                  (idx, [1:   4]) = [  2.37634E-03 0.00016  1.05849E-02 0.00097 ];
INF_ABS                   (idx, [1:   4]) = [  4.70316E-03 0.00028  2.36283E-02 0.00107 ];
INF_FISS                  (idx, [1:   4]) = [  2.32682E-03 0.00058  1.30435E-02 0.00115 ];
INF_NSF                   (idx, [1:   4]) = [  5.69682E-03 0.00057  3.17756E-02 0.00115 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44833E+00 6.8E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 3.4E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.19460E-08 0.00061  2.88815E-06 0.00026 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38683E-01 7.2E-05  5.79874E-01 0.00029 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89889E-02 0.00025  4.06676E-02 0.00071 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05400E-02 0.00054  4.96517E-03 0.00397 ];
INF_SCATT3                (idx, [1:   4]) = [  1.05440E-03 0.00533  1.24340E-03 0.02540 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.89988E-04 0.00322  3.74977E-04 0.04980 ];
INF_SCATT5                (idx, [1:   4]) = [  8.77315E-05 0.03350  1.74323E-04 0.18097 ];
INF_SCATT6                (idx, [1:   4]) = [  4.20966E-04 0.00883  3.70613E-05 0.81565 ];
INF_SCATT7                (idx, [1:   4]) = [  4.45920E-05 0.03196  5.02402E-05 0.35011 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38738E-01 7.2E-05  5.79874E-01 0.00029 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.90018E-02 0.00025  4.06676E-02 0.00071 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05427E-02 0.00054  4.96517E-03 0.00397 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.05501E-03 0.00526  1.24340E-03 0.02540 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.89981E-04 0.00322  3.74977E-04 0.04980 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.78159E-05 0.03352  1.74323E-04 0.18097 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.20974E-04 0.00886  3.70613E-05 0.81565 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.45641E-05 0.03254  5.02402E-05 0.35011 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49325E-01 0.00018  5.58634E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33694E+00 0.00018  5.96693E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.64810E-03 0.00027  2.36283E-02 0.00107 ];
INF_REMXS                 (idx, [1:   4]) = [  6.41770E-03 0.00026  2.42992E-02 0.00075 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.36967E-01 7.1E-05  1.71530E-03 0.00065  6.74369E-04 0.00584  5.79199E-01 0.00029 ];
INF_S1                    (idx, [1:   8]) = [  3.91492E-02 0.00025 -1.60328E-04 0.00245  1.84575E-04 0.00922  4.04830E-02 0.00071 ];
INF_S2                    (idx, [1:   8]) = [  1.06234E-02 0.00052 -8.34079E-05 0.00725  1.78983E-05 0.05050  4.94727E-03 0.00415 ];
INF_S3                    (idx, [1:   8]) = [  1.14688E-03 0.00471 -9.24823E-05 0.00465 -1.58197E-05 0.06957  1.25922E-03 0.02564 ];
INF_S4                    (idx, [1:   8]) = [ -5.58831E-04 0.00319 -3.11573E-05 0.00596 -1.39076E-05 0.08239  3.88884E-04 0.04890 ];
INF_S5                    (idx, [1:   8]) = [  7.94297E-05 0.03402  8.30179E-06 0.04548 -8.30966E-06 0.06954  1.82633E-04 0.17041 ];
INF_S6                    (idx, [1:   8]) = [  4.13378E-04 0.00948  7.58789E-06 0.05090 -5.64419E-06 0.07801  4.27055E-05 0.69932 ];
INF_S7                    (idx, [1:   8]) = [  4.44600E-05 0.03592  1.31929E-07 1.00000 -4.38927E-06 0.07216  5.46295E-05 0.32139 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37023E-01 7.1E-05  1.71530E-03 0.00065  6.74369E-04 0.00584  5.79199E-01 0.00029 ];
INF_SP1                   (idx, [1:   8]) = [  3.91622E-02 0.00025 -1.60328E-04 0.00245  1.84575E-04 0.00922  4.04830E-02 0.00071 ];
INF_SP2                   (idx, [1:   8]) = [  1.06261E-02 0.00052 -8.34079E-05 0.00725  1.78983E-05 0.05050  4.94727E-03 0.00415 ];
INF_SP3                   (idx, [1:   8]) = [  1.14749E-03 0.00466 -9.24823E-05 0.00465 -1.58197E-05 0.06957  1.25922E-03 0.02564 ];
INF_SP4                   (idx, [1:   8]) = [ -5.58824E-04 0.00319 -3.11573E-05 0.00596 -1.39076E-05 0.08239  3.88884E-04 0.04890 ];
INF_SP5                   (idx, [1:   8]) = [  7.95142E-05 0.03399  8.30179E-06 0.04548 -8.30966E-06 0.06954  1.82633E-04 0.17041 ];
INF_SP6                   (idx, [1:   8]) = [  4.13386E-04 0.00951  7.58789E-06 0.05090 -5.64419E-06 0.07801  4.27055E-05 0.69932 ];
INF_SP7                   (idx, [1:   8]) = [  4.44321E-05 0.03653  1.31929E-07 1.00000 -4.38927E-06 0.07216  5.46295E-05 0.32139 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70343E-01 0.00021  8.46167E-01 0.00354 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.74832E-01 0.00038  1.14757E+00 0.01076 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.75022E-01 0.00021  1.13597E+00 0.01082 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61619E-01 0.00021  5.57751E-01 0.00450 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23300E+00 0.00021  3.93953E-01 0.00355 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21286E+00 0.00038  2.90602E-01 0.01064 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21202E+00 0.00021  2.93571E-01 0.01074 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27412E+00 0.00021  5.97687E-01 0.00448 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.32450E-03 0.00641  2.38731E-04 0.03167  1.09024E-03 0.01744  7.03950E-04 0.01718  1.45813E-03 0.01483  2.34948E-03 0.01106  6.67719E-04 0.02122  6.34017E-04 0.02151  1.82239E-04 0.04037 ];
LAMBDA                    (idx, [1:  18]) = [  4.19480E-01 0.00935  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

