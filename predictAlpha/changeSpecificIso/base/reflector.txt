/***********************************************************
******************* Reflector Geometry *********************
************************************************************/
% ------------- Reflector is Universe #  ---------------- %
%include materials.txt
%include fuelElementMaterials.txt
%include FuelChannel.txt
%include FuelElement.txt
%include UnloadedCentralFuelElement.txt
%include core.txt 

% - surfaces
surf innerPyro      cylz 0 0 42.6 0 132.08
surf outerPyro      cylz 0 0 42.9 0 132.08
surf IDcoolGap      cylz 0 0 45.9 0 132.08
surf innerReflector cylz 0 0 46.2 0 132.08
surf outerReflector cylz 0 0 50.8 0 132.08
surf ODcoolGap      cylz 0 0 51.1 0 132.08

% - trying coolant holes
pin 31
propellant 0.3175
inconel718 0.3675
graphite

lat 32 4 0 0 1
108 48.51 0 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31 31

% - cells
%cell inVoid    0 outside    -innerPyro
cell coreIN    33 fill 30    -innerPyro
cell pyroFill  33 pyrocarbon -outerPyro       innerPyro
cell perimFill 33 graphite   -IDcoolGap       outerPyro
cell IDcool    33 propellant -innerReflector  IDcoolGap
cell reflector 33 fill 32    -outerReflector  innerReflector
cell ODcool    33 propellant -ODcoolGap       outerReflector
cell void      33 outside     ODcoolGap

% set pop 10000 100 50
% plot 3 5000 5000 1 -52 52 -52 52 % xy plot