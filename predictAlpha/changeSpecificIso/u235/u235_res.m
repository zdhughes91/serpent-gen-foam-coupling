
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   4]) = 'u235' ;
WORKING_DIRECTORY         (idx, [1:  83]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/changeSpecificIso/u235' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 14:37:17 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 16:01:48 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701290237449 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 4 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   4]) = [  9.80847E-01  1.09437E+00  9.78407E-01  9.46378E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.38167E-01 0.00019  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.61833E-01 0.00015  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54798E-01 9.0E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11531E-01 0.00013  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15314E+01 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40222E-01 3.9E-06  5.94016E-02 6.2E-05  3.76918E-04 0.00063  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.07610E+01 0.00022  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.05890E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11800E+02 0.00020  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.04050E+02 0.00041  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 10000235 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00002E+05 0.00049 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00002E+05 0.00049 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.02616E+02 ;
RUNNING_TIME              (idx, 1)        =  8.45218E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  7.26833E-02  7.26833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90000E-03  1.90000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  8.44472E+01  8.44472E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  8.45214E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.58033 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.94374E+00 0.00943 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.95971E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 2367.99 ;
MEMSIZE                   (idx, 1)        = 2284.29 ;
XS_MEMSIZE                (idx, 1)        = 1146.75 ;
MAT_MEMSIZE               (idx, 1)        = 468.66 ;
RES_MEMSIZE               (idx, 1)        = 0.93 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 667.95 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.70 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 613970 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92413E-06 0.00028  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.32929E-02 0.00235 ];
U235_FISS                 (idx, [1:   4]) = [  4.18530E-01 0.00046  9.99210E-01 1.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.30897E-04 0.01816  7.89926E-04 0.01812 ];
U235_CAPT                 (idx, [1:   4]) = [  1.61384E-01 0.00071  3.96719E-01 0.00057 ];
U238_CAPT                 (idx, [1:   4]) = [  1.35119E-02 0.00235  3.32155E-02 0.00235 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 10000235 1.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.42423E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 10000235 1.00742E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 4071262 4.09909E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 4209094 4.22063E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1719879 1.75453E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 10000235 1.00742E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 8.58679E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35760E-11 0.00029 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02428E+00 0.00029 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.18911E-01 0.00029 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.06968E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.25879E-01 0.00017 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92413E-01 0.00028 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.40741E+02 0.00025 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.74121E-01 0.00079 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.05576E+01 0.00026 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06733E+00 0.00033 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.45464E-01 0.00052 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.43539E-01 0.00066 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.85160E+00 0.00087 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43302E-01 0.00015 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77760E-01 5.0E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25158E+00 0.00031 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03199E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44510E+00 2.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03200E+00 0.00036  1.02439E+00 0.00035  7.60062E-03 0.00555 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03186E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03212E+00 0.00045 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03186E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.25136E+00 0.00022 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31403E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31363E+01 0.00014 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.93016E-05 0.00247 ];
IMP_EALF                  (idx, [1:   2]) = [  3.94532E-05 0.00189 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01471E-01 0.00224 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.01430E-01 0.00051 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.46648E-03 0.00367  2.19308E-04 0.01958  9.81659E-04 0.00988  6.20178E-04 0.01054  1.30391E-03 0.00915  2.04900E-03 0.00650  5.93871E-04 0.01251  5.48071E-04 0.01304  1.50486E-04 0.02415 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.10806E-01 0.00586  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34940E-03 0.00590  2.44952E-04 0.03049  1.10130E-03 0.01512  7.06523E-04 0.01951  1.49672E-03 0.01312  2.30894E-03 0.01033  6.88825E-04 0.02080  6.31698E-04 0.01889  1.70446E-04 0.04158 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13069E-01 0.00936  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 4.4E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.39089E-05 0.00222  2.39161E-05 0.00221  2.29456E-05 0.02340 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.46740E-05 0.00225  2.46814E-05 0.00223  2.36820E-05 0.02344 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.36938E-03 0.00578  2.59711E-04 0.02871  1.10964E-03 0.01489  7.12893E-04 0.01778  1.49101E-03 0.01243  2.32996E-03 0.01049  6.75629E-04 0.01985  6.24698E-04 0.01925  1.65841E-04 0.03714 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08094E-01 0.00894  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.2E-09  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 3.7E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.18988E-05 0.02397  2.18943E-05 0.02397  2.22627E-05 0.06937 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.25963E-05 0.02398  2.25916E-05 0.02397  2.29733E-05 0.06939 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.87932E-03 0.03204  2.24704E-04 0.09840  1.02518E-03 0.05882  6.54695E-04 0.06844  1.36616E-03 0.04962  2.20095E-03 0.04493  6.73309E-04 0.05919  5.87592E-04 0.06777  1.46729E-04 0.12063 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.11384E-01 0.02745  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 1.9E-09  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 5.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.4E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.89462E-03 0.03115  2.25388E-04 0.09632  1.00962E-03 0.05854  6.58419E-04 0.06590  1.37738E-03 0.04880  2.21652E-03 0.04366  6.73658E-04 0.05907  5.91029E-04 0.06444  1.42608E-04 0.11556 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.11054E-01 0.02517  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 4.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.1E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.15197E+02 0.02318 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.35889E-05 0.00096 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.43433E-05 0.00087 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.41485E-03 0.00289 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.14370E+02 0.00307 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.14433E-07 0.00103 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59610E-05 0.00036  1.59620E-05 0.00035  1.58078E-05 0.00441 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.13331E-04 0.00101  1.13381E-04 0.00102  1.05883E-04 0.01388 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19864E-01 0.00065  2.19762E-01 0.00064  2.35976E-01 0.00897 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31469E+01 0.00830 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.05890E+01 0.00023  4.96245E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.81297E+05 0.00314  4.61608E+06 0.00108  1.03925E+07 0.00048  1.88519E+07 0.00040  1.99275E+07 0.00021  1.84956E+07 0.00053  1.69923E+07 0.00070  1.50711E+07 0.00015  1.33932E+07 0.00036  1.20649E+07 0.00028  1.10829E+07 0.00023  1.02623E+07 0.00050  9.45517E+06 0.00024  9.03853E+06 0.00040  8.67294E+06 0.00022  7.35243E+06 0.00053  7.26484E+06 0.00047  6.94404E+06 0.00060  6.60619E+06 0.00045  1.21944E+07 0.00064  1.06053E+07 0.00058  6.89827E+06 0.00039  4.12591E+06 0.00095  4.37074E+06 0.00106  3.82904E+06 0.00091  3.00866E+06 0.00067  4.92725E+06 0.00078  9.83402E+05 0.00094  1.21447E+06 0.00147  1.09716E+06 0.00171  6.23941E+05 0.00161  1.07487E+06 0.00154  7.16468E+05 0.00119  5.86297E+05 0.00149  1.08070E+05 0.00244  1.05657E+05 0.00311  1.08596E+05 0.00315  1.11456E+05 0.00222  1.09754E+05 0.00208  1.07771E+05 0.00293  1.10905E+05 0.00483  1.03513E+05 0.00484  1.93229E+05 0.00202  3.04947E+05 0.00167  3.80550E+05 0.00165  9.59842E+05 0.00094  9.33919E+05 0.00137  8.87824E+05 0.00084  5.06566E+05 0.00123  3.28898E+05 0.00186  2.32230E+05 0.00231  2.45084E+05 0.00343  4.02699E+05 0.00245  4.57851E+05 0.00183  7.50453E+05 0.00107  1.05478E+06 0.00174  1.71119E+06 0.00134  1.32222E+06 0.00084  1.11019E+06 0.00096  9.03940E+05 0.00148  8.99880E+05 0.00104  9.84535E+05 0.00135  9.14481E+05 0.00113  6.69698E+05 0.00184  6.68814E+05 0.00182  6.44564E+05 0.00209  5.90940E+05 0.00119  4.98945E+05 0.00076  3.52416E+05 0.00067  1.36199E+05 0.00127 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.25155E+00 0.00019 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32202E+02 0.00023  8.53888E+00 0.00086 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43423E-01 0.00010  6.03879E-01 0.00010 ];
INF_CAPT                  (idx, [1:   4]) = [  2.39591E-03 0.00054  1.05665E-02 0.00084 ];
INF_ABS                   (idx, [1:   4]) = [  4.73856E-03 0.00045  2.33564E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  2.34265E-03 0.00040  1.27900E-02 0.00068 ];
INF_NSF                   (idx, [1:   4]) = [  5.73540E-03 0.00039  3.11581E-02 0.00068 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44826E+00 2.7E-06  2.43614E+00 5.9E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.4E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.14065E-08 0.00045  2.89334E-06 0.00025 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38684E-01 0.00010  5.80539E-01 0.00013 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89840E-02 0.00019  4.06786E-02 0.00136 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05475E-02 0.00110  4.88340E-03 0.00581 ];
INF_SCATT3                (idx, [1:   4]) = [  1.05282E-03 0.00709  1.22786E-03 0.02346 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.78726E-04 0.01694  3.63586E-04 0.07393 ];
INF_SCATT5                (idx, [1:   4]) = [  7.96416E-05 0.04127  1.00167E-04 0.19101 ];
INF_SCATT6                (idx, [1:   4]) = [  4.24386E-04 0.00994  6.11894E-05 0.14528 ];
INF_SCATT7                (idx, [1:   4]) = [  4.91523E-05 0.11447  5.00394E-05 0.21380 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38740E-01 0.00010  5.80539E-01 0.00013 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.89970E-02 0.00019  4.06786E-02 0.00136 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05500E-02 0.00110  4.88340E-03 0.00581 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.05342E-03 0.00710  1.22786E-03 0.02346 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.78572E-04 0.01693  3.63586E-04 0.07393 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.97362E-05 0.04073  1.00167E-04 0.19101 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.24455E-04 0.01006  6.11894E-05 0.14528 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.91914E-05 0.11409  5.00394E-05 0.21380 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49215E-01 0.00023  5.59065E-01 0.00019 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33753E+00 0.00023  5.96234E-01 0.00019 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.68282E-03 0.00044  2.33564E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  6.43089E-03 0.00018  2.40185E-02 0.00098 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.36992E-01 1.0E-04  1.69203E-03 0.00054  6.78338E-04 0.00397  5.79861E-01 0.00013 ];
INF_S1                    (idx, [1:   8]) = [  3.91458E-02 0.00018 -1.61745E-04 0.00582  1.83358E-04 0.01517  4.04953E-02 0.00131 ];
INF_S2                    (idx, [1:   8]) = [  1.06297E-02 0.00108 -8.22132E-05 0.00897  1.33071E-05 0.11990  4.87010E-03 0.00610 ];
INF_S3                    (idx, [1:   8]) = [  1.14287E-03 0.00690 -9.00584E-05 0.00507 -1.70225E-05 0.07378  1.24488E-03 0.02337 ];
INF_S4                    (idx, [1:   8]) = [ -5.49282E-04 0.01805 -2.94434E-05 0.01377 -1.67726E-05 0.04220  3.80358E-04 0.07060 ];
INF_S5                    (idx, [1:   8]) = [  7.20600E-05 0.04781  7.58166E-06 0.02892 -1.10941E-05 0.07609  1.11261E-04 0.17495 ];
INF_S6                    (idx, [1:   8]) = [  4.17036E-04 0.00930  7.35011E-06 0.06903 -6.09773E-06 0.07005  6.72871E-05 0.13242 ];
INF_S7                    (idx, [1:   8]) = [  4.89081E-05 0.11588  2.44240E-07 1.00000 -4.45220E-06 0.04888  5.44916E-05 0.19826 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37048E-01 1.0E-04  1.69203E-03 0.00054  6.78338E-04 0.00397  5.79861E-01 0.00013 ];
INF_SP1                   (idx, [1:   8]) = [  3.91587E-02 0.00018 -1.61745E-04 0.00582  1.83358E-04 0.01517  4.04953E-02 0.00131 ];
INF_SP2                   (idx, [1:   8]) = [  1.06323E-02 0.00109 -8.22132E-05 0.00897  1.33071E-05 0.11990  4.87010E-03 0.00610 ];
INF_SP3                   (idx, [1:   8]) = [  1.14348E-03 0.00691 -9.00584E-05 0.00507 -1.70225E-05 0.07378  1.24488E-03 0.02337 ];
INF_SP4                   (idx, [1:   8]) = [ -5.49129E-04 0.01804 -2.94434E-05 0.01377 -1.67726E-05 0.04220  3.80358E-04 0.07060 ];
INF_SP5                   (idx, [1:   8]) = [  7.21546E-05 0.04723  7.58166E-06 0.02892 -1.10941E-05 0.07609  1.11261E-04 0.17495 ];
INF_SP6                   (idx, [1:   8]) = [  4.17105E-04 0.00943  7.35011E-06 0.06903 -6.09773E-06 0.07005  6.72871E-05 0.13242 ];
INF_SP7                   (idx, [1:   8]) = [  4.89471E-05 0.11551  2.44240E-07 1.00000 -4.45220E-06 0.04888  5.44916E-05 0.19826 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70328E-01 0.00036  8.42547E-01 0.00445 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.74825E-01 0.00071  1.13920E+00 0.00724 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.74844E-01 0.00040  1.14512E+00 0.00984 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61745E-01 0.00031  5.52711E-01 0.00244 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23307E+00 0.00036  3.95657E-01 0.00448 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21290E+00 0.00071  2.92664E-01 0.00735 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21281E+00 0.00040  2.91205E-01 0.00993 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27351E+00 0.00031  6.03102E-01 0.00244 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.34940E-03 0.00590  2.44952E-04 0.03049  1.10130E-03 0.01512  7.06523E-04 0.01951  1.49672E-03 0.01312  2.30894E-03 0.01033  6.88825E-04 0.02080  6.31698E-04 0.01889  1.70446E-04 0.04158 ];
LAMBDA                    (idx, [1:  18]) = [  4.13069E-01 0.00936  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 4.4E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

