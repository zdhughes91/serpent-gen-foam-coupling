
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   4]) = 'u235' ;
WORKING_DIRECTORY         (idx, [1:  92]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/changeSpecificIso/u235twelveLib' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 16:40:40 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 17:04:10 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701297640720 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 16 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:  16]) = [  1.03684E+00  9.95292E-01  1.00400E+00  9.91215E-01  9.96240E-01  9.93416E-01  9.92580E-01  9.96714E-01  1.00223E+00  1.00106E+00  1.00421E+00  9.91746E-01  1.00024E+00  9.93860E-01  1.00410E+00  9.96261E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.38812E-01 0.00020  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.61188E-01 0.00015  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54796E-01 9.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11494E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15162E+01 0.00041  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40239E-01 4.1E-06  5.93843E-02 6.5E-05  3.76597E-04 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.07526E+01 0.00024  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.05810E+01 0.00024  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11801E+02 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.04365E+02 0.00041  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 9999033 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99903E+04 0.00058 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99903E+04 0.00058 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.50794E+02 ;
RUNNING_TIME              (idx, 1)        =  2.34914E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.30667E-02  6.30667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  3.06667E-03  3.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.34253E+01  2.34253E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.34905E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 14.93287 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.49767E+01 0.00242 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76141E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 2673.41 ;
MEMSIZE                   (idx, 1)        = 2472.32 ;
XS_MEMSIZE                (idx, 1)        = 1331.89 ;
MAT_MEMSIZE               (idx, 1)        = 470.73 ;
RES_MEMSIZE               (idx, 1)        = 1.37 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.34 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 201.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 616479 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92411E-06 0.00024  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.33406E-02 0.00268 ];
U235_FISS                 (idx, [1:   4]) = [  4.18586E-01 0.00041  9.99222E-01 1.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.25938E-04 0.01830  7.78102E-04 0.01833 ];
U235_CAPT                 (idx, [1:   4]) = [  1.61309E-01 0.00085  3.96216E-01 0.00073 ];
U238_CAPT                 (idx, [1:   4]) = [  1.35390E-02 0.00263  3.32548E-02 0.00256 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 9999033 1.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.40049E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 9999033 1.00740E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 4074042 4.10238E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 4209156 4.22116E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1715835 1.75046E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 9999033 1.00740E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -6.55651E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35873E-11 0.00028 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02513E+00 0.00028 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.19258E-01 0.00028 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.07024E-01 0.00025 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.26283E-01 0.00015 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92411E-01 0.00024 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.40746E+02 0.00024 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.73717E-01 0.00071 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.05484E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06849E+00 0.00039 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.46451E-01 0.00058 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.43934E-01 0.00059 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.83585E+00 0.00083 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43599E-01 0.00014 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77899E-01 5.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25110E+00 0.00033 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03210E+00 0.00035 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44510E+00 2.2E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03210E+00 0.00037  1.02457E+00 0.00036  7.52561E-03 0.00521 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03271E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03298E+00 0.00042 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03271E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.25177E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31450E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31444E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.91169E-05 0.00237 ];
IMP_EALF                  (idx, [1:   2]) = [  3.91330E-05 0.00172 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01004E-01 0.00236 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.01435E-01 0.00053 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.43941E-03 0.00339  2.11687E-04 0.02193  9.94900E-04 0.00991  6.39079E-04 0.01401  1.26808E-03 0.00815  2.01434E-03 0.00686  6.05153E-04 0.01205  5.44573E-04 0.01413  1.61603E-04 0.02427 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.16800E-01 0.00600  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 3.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.26953E-03 0.00586  2.33455E-04 0.03092  1.10842E-03 0.01503  7.46204E-04 0.01854  1.41376E-03 0.01403  2.26514E-03 0.01064  6.89979E-04 0.01947  6.28830E-04 0.02157  1.83746E-04 0.03743 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.20648E-01 0.00917  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.39061E-05 0.00205  2.39148E-05 0.00206  2.27363E-05 0.02539 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.46732E-05 0.00204  2.46822E-05 0.00205  2.34637E-05 0.02536 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.28971E-03 0.00540  2.47307E-04 0.03170  1.09173E-03 0.01326  7.57289E-04 0.02016  1.43075E-03 0.01370  2.27776E-03 0.01023  6.91635E-04 0.02019  6.12463E-04 0.01892  1.80771E-04 0.03236 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15400E-01 0.00808  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 3.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.16543E-05 0.02366  2.16590E-05 0.02369  2.07823E-05 0.07526 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.23516E-05 0.02367  2.23565E-05 0.02370  2.14443E-05 0.07522 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.83769E-03 0.03157  2.47852E-04 0.09366  1.00203E-03 0.05148  7.41387E-04 0.06000  1.38804E-03 0.05192  2.17028E-03 0.04743  5.51982E-04 0.07419  5.78472E-04 0.07070  1.57643E-04 0.15586 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.03089E-01 0.03385  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 4.0E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.84880E-03 0.03165  2.53235E-04 0.09038  9.98048E-04 0.05156  7.37111E-04 0.05956  1.38070E-03 0.05098  2.17026E-03 0.04622  5.57679E-04 0.07434  5.82456E-04 0.07121  1.69304E-04 0.15205 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.09611E-01 0.03424  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 4.4E-09  1.63478E+00 0.0E+00  3.55460E+00 6.1E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.16407E+02 0.02216 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.35191E-05 0.00104 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.42737E-05 0.00097 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33865E-03 0.00381 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.12045E+02 0.00381 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.13854E-07 0.00097 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59553E-05 0.00034  1.59548E-05 0.00034  1.60196E-05 0.00419 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.12725E-04 0.00096  1.12750E-04 0.00097  1.09181E-04 0.01302 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.20185E-01 0.00058  2.20108E-01 0.00059  2.32344E-01 0.00942 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32124E+01 0.00860 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.05810E+01 0.00024  4.96383E+01 0.00031 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.78284E+05 0.00165  4.62566E+06 0.00083  1.04017E+07 0.00069  1.88624E+07 0.00043  1.99270E+07 0.00031  1.84904E+07 0.00037  1.70065E+07 0.00029  1.50749E+07 0.00025  1.33850E+07 0.00054  1.20599E+07 0.00066  1.10770E+07 0.00040  1.02568E+07 0.00037  9.45569E+06 0.00036  9.04698E+06 0.00073  8.67272E+06 0.00056  7.34352E+06 0.00044  7.26364E+06 0.00022  6.94440E+06 0.00074  6.60584E+06 0.00065  1.21909E+07 0.00060  1.06049E+07 0.00036  6.90173E+06 0.00040  4.12655E+06 0.00093  4.37821E+06 0.00143  3.83590E+06 0.00118  3.01332E+06 0.00061  4.94324E+06 0.00095  9.83709E+05 0.00164  1.21723E+06 0.00151  1.09990E+06 0.00110  6.24404E+05 0.00205  1.07919E+06 0.00113  7.17486E+05 0.00174  5.87946E+05 0.00129  1.08374E+05 0.00201  1.06521E+05 0.00416  1.08901E+05 0.00184  1.11662E+05 0.00447  1.10145E+05 0.00315  1.08198E+05 0.00263  1.10729E+05 0.00333  1.03370E+05 0.00233  1.93263E+05 0.00285  3.03283E+05 0.00276  3.80873E+05 0.00233  9.61186E+05 0.00200  9.32284E+05 0.00157  8.90149E+05 0.00090  5.05681E+05 0.00162  3.28291E+05 0.00133  2.31490E+05 0.00248  2.44693E+05 0.00183  4.00543E+05 0.00228  4.57548E+05 0.00046  7.49931E+05 0.00040  1.04885E+06 0.00069  1.70540E+06 0.00158  1.32009E+06 0.00114  1.10664E+06 0.00120  9.00848E+05 0.00110  8.94905E+05 0.00067  9.79276E+05 0.00196  9.11457E+05 0.00082  6.67787E+05 0.00136  6.66019E+05 0.00152  6.42047E+05 0.00204  5.88336E+05 0.00116  4.96167E+05 0.00156  3.50609E+05 0.00211  1.35980E+05 0.00158 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.25209E+00 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32234E+02 0.00024  8.51240E+00 0.00064 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43402E-01 6.9E-05  6.03729E-01 9.1E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  2.39595E-03 0.00042  1.05965E-02 0.00071 ];
INF_ABS                   (idx, [1:   4]) = [  4.73721E-03 0.00022  2.34798E-02 0.00070 ];
INF_FISS                  (idx, [1:   4]) = [  2.34126E-03 6.7E-05  1.28833E-02 0.00119 ];
INF_NSF                   (idx, [1:   4]) = [  5.73205E-03 6.5E-05  3.13855E-02 0.00119 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44827E+00 2.1E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.0E-08  2.02270E+02 5.9E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.14577E-08 0.00070  2.89191E-06 0.00024 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38666E-01 7.4E-05  5.80267E-01 9.4E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89680E-02 0.00024  4.06621E-02 0.00162 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05462E-02 0.00051  4.90701E-03 0.00695 ];
INF_SCATT3                (idx, [1:   4]) = [  1.04669E-03 0.00750  1.22752E-03 0.02905 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.74598E-04 0.01144  4.05388E-04 0.07115 ];
INF_SCATT5                (idx, [1:   4]) = [  9.49461E-05 0.06952  1.20011E-04 0.25572 ];
INF_SCATT6                (idx, [1:   4]) = [  4.21520E-04 0.01188  4.48371E-05 0.18114 ];
INF_SCATT7                (idx, [1:   4]) = [  5.16548E-05 0.10611  2.97347E-05 0.77509 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38721E-01 7.4E-05  5.80267E-01 9.4E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.89810E-02 0.00024  4.06621E-02 0.00162 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05489E-02 0.00051  4.90701E-03 0.00695 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.04737E-03 0.00751  1.22752E-03 0.02905 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.74457E-04 0.01138  4.05388E-04 0.07115 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.50370E-05 0.06939  1.20011E-04 0.25572 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.21557E-04 0.01190  4.48371E-05 0.18114 ];
INF_SCATTP7               (idx, [1:   4]) = [  5.17045E-05 0.10600  2.97347E-05 0.77509 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49234E-01 0.00021  5.58947E-01 0.00014 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33743E+00 0.00021  5.96360E-01 0.00014 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.68167E-03 0.00024  2.34798E-02 0.00070 ];
INF_REMXS                 (idx, [1:   4]) = [  6.43074E-03 0.00025  2.41413E-02 0.00039 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.36972E-01 7.1E-05  1.69392E-03 0.00075  6.79406E-04 0.00293  5.79587E-01 9.5E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.91293E-02 0.00023 -1.61292E-04 0.00247  1.81679E-04 0.00621  4.04804E-02 0.00163 ];
INF_S2                    (idx, [1:   8]) = [  1.06280E-02 0.00052 -8.17931E-05 0.00403  1.67987E-05 0.06495  4.89022E-03 0.00705 ];
INF_S3                    (idx, [1:   8]) = [  1.13746E-03 0.00693 -9.07657E-05 0.00356 -1.52588E-05 0.03904  1.24278E-03 0.02837 ];
INF_S4                    (idx, [1:   8]) = [ -5.44819E-04 0.01211 -2.97797E-05 0.01226 -1.29678E-05 0.04893  4.18356E-04 0.06817 ];
INF_S5                    (idx, [1:   8]) = [  8.67908E-05 0.07705  8.15531E-06 0.02586 -9.61326E-06 0.07125  1.29625E-04 0.23189 ];
INF_S6                    (idx, [1:   8]) = [  4.14566E-04 0.01214  6.95391E-06 0.05030 -7.06412E-06 0.06456  5.19012E-05 0.14985 ];
INF_S7                    (idx, [1:   8]) = [  5.21067E-05 0.10233 -4.51917E-07 0.68938 -3.86474E-06 0.13365  3.35994E-05 0.69290 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37027E-01 7.1E-05  1.69392E-03 0.00075  6.79406E-04 0.00293  5.79587E-01 9.5E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.91423E-02 0.00023 -1.61292E-04 0.00247  1.81679E-04 0.00621  4.04804E-02 0.00163 ];
INF_SP2                   (idx, [1:   8]) = [  1.06307E-02 0.00052 -8.17931E-05 0.00403  1.67987E-05 0.06495  4.89022E-03 0.00705 ];
INF_SP3                   (idx, [1:   8]) = [  1.13814E-03 0.00693 -9.07657E-05 0.00356 -1.52588E-05 0.03904  1.24278E-03 0.02837 ];
INF_SP4                   (idx, [1:   8]) = [ -5.44677E-04 0.01204 -2.97797E-05 0.01226 -1.29678E-05 0.04893  4.18356E-04 0.06817 ];
INF_SP5                   (idx, [1:   8]) = [  8.68817E-05 0.07691  8.15531E-06 0.02586 -9.61326E-06 0.07125  1.29625E-04 0.23189 ];
INF_SP6                   (idx, [1:   8]) = [  4.14603E-04 0.01215  6.95391E-06 0.05030 -7.06412E-06 0.06456  5.19012E-05 0.14985 ];
INF_SP7                   (idx, [1:   8]) = [  5.21564E-05 0.10221 -4.51917E-07 0.68938 -3.86474E-06 0.13365  3.35994E-05 0.69290 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70153E-01 0.00032  8.51859E-01 0.00242 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.74548E-01 0.00050  1.15426E+00 0.00811 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.74732E-01 0.00065  1.16636E+00 0.00504 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61607E-01 0.00077  5.56292E-01 0.00427 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23387E+00 0.00032  3.91310E-01 0.00242 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21412E+00 0.00050  2.88863E-01 0.00822 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21331E+00 0.00065  2.85818E-01 0.00505 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27418E+00 0.00077  5.99250E-01 0.00427 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.26953E-03 0.00586  2.33455E-04 0.03092  1.10842E-03 0.01503  7.46204E-04 0.01854  1.41376E-03 0.01403  2.26514E-03 0.01064  6.89979E-04 0.01947  6.28830E-04 0.02157  1.83746E-04 0.03743 ];
LAMBDA                    (idx, [1:  18]) = [  4.20648E-01 0.00917  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 3.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

