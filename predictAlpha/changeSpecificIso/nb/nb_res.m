
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:   2]) = 'nb' ;
WORKING_DIRECTORY         (idx, [1:  81]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/changeSpecificIso/nb' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 13:55:50 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 14:34:56 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701287750428 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 4 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   4]) = [  9.94766E-01  1.00579E+00  9.99027E-01  1.00041E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.41384E-01 0.00018  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.58616E-01 0.00014  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54549E-01 9.0E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11554E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14140E+01 0.00038  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40327E-01 3.7E-06  5.92995E-02 5.9E-05  3.73823E-04 0.00060  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.08351E+01 0.00022  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.06632E+01 0.00022  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11951E+02 0.00020  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.05628E+02 0.00034  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 9999654 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99965E+04 0.00062 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99965E+04 0.00062 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.54478E+02 ;
RUNNING_TIME              (idx, 1)        =  3.91028E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  7.82333E-02  7.82333E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.40000E-03  2.40000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  3.90222E+01  3.90222E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.91025E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.95056 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.98490E+00 0.00094 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.94011E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 2417.94 ;
MEMSIZE                   (idx, 1)        = 2336.53 ;
XS_MEMSIZE                (idx, 1)        = 1181.13 ;
MAT_MEMSIZE               (idx, 1)        = 486.51 ;
RES_MEMSIZE               (idx, 1)        = 0.93 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 667.95 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 81.42 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 637372 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92780E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.41743E-02 0.00264 ];
U235_FISS                 (idx, [1:   4]) = [  4.18276E-01 0.00046  9.99210E-01 1.6E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.30700E-04 0.02076  7.90040E-04 0.02080 ];
U235_CAPT                 (idx, [1:   4]) = [  1.57835E-01 0.00081  3.87461E-01 0.00064 ];
U238_CAPT                 (idx, [1:   4]) = [  1.39311E-02 0.00257  3.41993E-02 0.00259 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 9999654 1.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.39700E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 9999654 1.00740E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 4075276 4.10319E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 4204626 4.21651E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1719752 1.75428E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 9999654 1.00740E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.04891E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35579E-11 0.00029 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02292E+00 0.00029 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.18353E-01 0.00029 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.07487E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.25839E-01 0.00016 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92780E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.41003E+02 0.00026 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.74161E-01 0.00077 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.06496E+01 0.00026 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06898E+00 0.00035 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.48320E-01 0.00052 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.46741E-01 0.00062 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.77797E+00 0.00080 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43360E-01 0.00016 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77722E-01 5.1E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25031E+00 0.00033 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03097E+00 0.00038 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44513E+00 2.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03086E+00 0.00041  1.02339E+00 0.00038  7.58010E-03 0.00604 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03050E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03037E+00 0.00038 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03050E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.24978E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31758E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31740E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.79301E-05 0.00234 ];
IMP_EALF                  (idx, [1:   2]) = [  3.79915E-05 0.00168 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01298E-01 0.00223 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.01736E-01 0.00060 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.51240E-03 0.00384  2.16592E-04 0.02093  9.82093E-04 0.01030  6.31940E-04 0.01355  1.27896E-03 0.00872  2.08514E-03 0.00679  6.03994E-04 0.01264  5.61655E-04 0.01397  1.52027E-04 0.02542 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14322E-01 0.00584  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39161E-03 0.00564  2.55740E-04 0.03005  1.09856E-03 0.01463  7.22595E-04 0.01871  1.46963E-03 0.01634  2.35544E-03 0.00949  6.87524E-04 0.01848  6.26545E-04 0.02037  1.75568E-04 0.03550 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13654E-01 0.00925  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 4.8E-09  2.92467E-01 0.0E+00  6.66488E-01 4.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.41424E-05 0.00193  2.41578E-05 0.00191  2.20432E-05 0.02293 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48870E-05 0.00187  2.49028E-05 0.00185  2.27208E-05 0.02287 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.35676E-03 0.00607  2.55920E-04 0.02965  1.10770E-03 0.01460  7.28929E-04 0.01752  1.42719E-03 0.01425  2.33285E-03 0.01147  6.89202E-04 0.01914  6.46438E-04 0.01992  1.68531E-04 0.03746 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15163E-01 0.00881  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.0E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.20803E-05 0.02411  2.21021E-05 0.02412  1.93313E-05 0.06676 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.27625E-05 0.02412  2.27850E-05 0.02413  1.99303E-05 0.06678 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  7.17357E-03 0.03228  2.65788E-04 0.11650  1.05723E-03 0.05801  7.30367E-04 0.06289  1.38181E-03 0.05766  2.32733E-03 0.03949  6.40743E-04 0.06331  6.03577E-04 0.07590  1.66729E-04 0.13761 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.12349E-01 0.03504  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 1.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.8E-09  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  7.14316E-03 0.03193  2.68782E-04 0.10831  1.04911E-03 0.05444  7.19729E-04 0.06143  1.38386E-03 0.05735  2.31703E-03 0.03845  6.41474E-04 0.06122  6.00366E-04 0.07237  1.62818E-04 0.13739 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.10480E-01 0.03410  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 5.1E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.25742E+02 0.02328 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.37471E-05 0.00118 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.44794E-05 0.00103 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.46937E-03 0.00337 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.14619E+02 0.00389 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.15146E-07 0.00093 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59795E-05 0.00033  1.59805E-05 0.00033  1.58391E-05 0.00452 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.12298E-04 0.00099  1.12334E-04 0.00100  1.07134E-04 0.01237 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.22561E-01 0.00060  2.22457E-01 0.00061  2.38762E-01 0.00798 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29667E+01 0.00763 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.06632E+01 0.00022  4.97765E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.87155E+05 0.00363  4.61373E+06 0.00132  1.04046E+07 0.00071  1.88461E+07 0.00074  1.99209E+07 0.00053  1.85002E+07 0.00029  1.69992E+07 0.00022  1.50782E+07 0.00019  1.33927E+07 0.00024  1.20607E+07 0.00038  1.10839E+07 0.00027  1.02611E+07 0.00023  9.45903E+06 0.00072  9.03603E+06 0.00029  8.67173E+06 0.00028  7.34775E+06 0.00065  7.26266E+06 0.00039  6.93390E+06 0.00061  6.59400E+06 0.00017  1.21748E+07 0.00014  1.05845E+07 0.00051  6.89907E+06 0.00046  4.14545E+06 0.00106  4.40824E+06 0.00054  3.87815E+06 0.00058  3.06563E+06 0.00053  5.02749E+06 0.00090  9.98937E+05 0.00094  1.23366E+06 0.00080  1.11588E+06 0.00162  6.33588E+05 0.00125  1.09175E+06 0.00117  7.27986E+05 0.00077  5.96089E+05 0.00079  1.09611E+05 0.00316  1.07588E+05 0.00251  1.09349E+05 0.00322  1.12450E+05 0.00331  1.11396E+05 0.00320  1.09313E+05 0.00403  1.12125E+05 0.00251  1.04483E+05 0.00259  1.96158E+05 0.00182  3.09514E+05 0.00265  3.85452E+05 0.00157  9.71997E+05 0.00141  9.40492E+05 0.00168  8.97907E+05 0.00118  5.11121E+05 0.00222  3.30617E+05 0.00261  2.33619E+05 0.00178  2.47146E+05 0.00230  4.03582E+05 0.00193  4.61173E+05 0.00178  7.56225E+05 0.00169  1.05792E+06 0.00050  1.71945E+06 0.00153  1.32826E+06 0.00157  1.11435E+06 0.00102  9.07594E+05 0.00171  9.01510E+05 0.00143  9.85373E+05 0.00087  9.17024E+05 0.00101  6.72653E+05 0.00183  6.69559E+05 0.00125  6.46211E+05 0.00179  5.92595E+05 0.00204  4.99618E+05 0.00147  3.52830E+05 0.00129  1.36320E+05 0.00318 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.24939E+00 0.00029 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32424E+02 0.00027  8.57918E+00 0.00064 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43372E-01 0.00020  6.03672E-01 0.00024 ];
INF_CAPT                  (idx, [1:   4]) = [  2.39139E-03 0.00047  1.05851E-02 0.00087 ];
INF_ABS                   (idx, [1:   4]) = [  4.71148E-03 0.00031  2.35373E-02 0.00050 ];
INF_FISS                  (idx, [1:   4]) = [  2.32009E-03 0.00019  1.29522E-02 0.00035 ];
INF_NSF                   (idx, [1:   4]) = [  5.68045E-03 0.00019  3.15533E-02 0.00035 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44838E+00 3.1E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.9E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.18442E-08 0.00051  2.89017E-06 0.00039 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38659E-01 0.00020  5.80124E-01 0.00025 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89829E-02 0.00024  4.06026E-02 0.00158 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05458E-02 0.00068  4.92076E-03 0.00429 ];
INF_SCATT3                (idx, [1:   4]) = [  1.05839E-03 0.00709  1.29469E-03 0.04225 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.78713E-04 0.01471  4.08701E-04 0.07188 ];
INF_SCATT5                (idx, [1:   4]) = [  8.65072E-05 0.04088  1.18491E-04 0.18708 ];
INF_SCATT6                (idx, [1:   4]) = [  4.18556E-04 0.01564  8.51808E-05 0.10306 ];
INF_SCATT7                (idx, [1:   4]) = [  4.87285E-05 0.06805  7.45029E-05 0.25054 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38714E-01 0.00020  5.80124E-01 0.00025 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.89960E-02 0.00024  4.06026E-02 0.00158 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05484E-02 0.00069  4.92076E-03 0.00429 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.05905E-03 0.00714  1.29469E-03 0.04225 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.78556E-04 0.01467  4.08701E-04 0.07188 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.65701E-05 0.04049  1.18491E-04 0.18708 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.18570E-04 0.01568  8.51808E-05 0.10306 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.86756E-05 0.06903  7.45029E-05 0.25054 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49293E-01 0.00029  5.58911E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33711E+00 0.00029  5.96398E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.65603E-03 0.00036  2.35373E-02 0.00050 ];
INF_REMXS                 (idx, [1:   4]) = [  6.42329E-03 0.00029  2.42337E-02 0.00030 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.36948E-01 0.00020  1.71063E-03 0.00077  6.86467E-04 0.00392  5.79438E-01 0.00025 ];
INF_S1                    (idx, [1:   8]) = [  3.91439E-02 0.00023 -1.61008E-04 0.00313  1.81289E-04 0.01164  4.04213E-02 0.00160 ];
INF_S2                    (idx, [1:   8]) = [  1.06286E-02 0.00069 -8.27960E-05 0.00369  1.64982E-05 0.10520  4.90426E-03 0.00405 ];
INF_S3                    (idx, [1:   8]) = [  1.14998E-03 0.00659 -9.15880E-05 0.00362 -1.61226E-05 0.05256  1.31081E-03 0.04152 ];
INF_S4                    (idx, [1:   8]) = [ -5.48432E-04 0.01566 -3.02809E-05 0.00721 -1.32615E-05 0.05977  4.21963E-04 0.06903 ];
INF_S5                    (idx, [1:   8]) = [  7.84118E-05 0.04640  8.09540E-06 0.02358 -8.59977E-06 0.06758  1.27091E-04 0.17362 ];
INF_S6                    (idx, [1:   8]) = [  4.11074E-04 0.01610  7.48190E-06 0.03450 -5.99180E-06 0.11350  9.11726E-05 0.10167 ];
INF_S7                    (idx, [1:   8]) = [  4.90366E-05 0.06806 -3.08183E-07 0.28931 -5.26405E-06 0.09546  7.97670E-05 0.23336 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37004E-01 0.00020  1.71063E-03 0.00077  6.86467E-04 0.00392  5.79438E-01 0.00025 ];
INF_SP1                   (idx, [1:   8]) = [  3.91570E-02 0.00023 -1.61008E-04 0.00313  1.81289E-04 0.01164  4.04213E-02 0.00160 ];
INF_SP2                   (idx, [1:   8]) = [  1.06312E-02 0.00069 -8.27960E-05 0.00369  1.64982E-05 0.10520  4.90426E-03 0.00405 ];
INF_SP3                   (idx, [1:   8]) = [  1.15064E-03 0.00663 -9.15880E-05 0.00362 -1.61226E-05 0.05256  1.31081E-03 0.04152 ];
INF_SP4                   (idx, [1:   8]) = [ -5.48275E-04 0.01561 -3.02809E-05 0.00721 -1.32615E-05 0.05977  4.21963E-04 0.06903 ];
INF_SP5                   (idx, [1:   8]) = [  7.84747E-05 0.04600  8.09540E-06 0.02358 -8.59977E-06 0.06758  1.27091E-04 0.17362 ];
INF_SP6                   (idx, [1:   8]) = [  4.11088E-04 0.01614  7.48190E-06 0.03450 -5.99180E-06 0.11350  9.11726E-05 0.10167 ];
INF_SP7                   (idx, [1:   8]) = [  4.89838E-05 0.06905 -3.08183E-07 0.28931 -5.26405E-06 0.09546  7.97670E-05 0.23336 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70426E-01 0.00020  8.39366E-01 0.00367 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.75102E-01 0.00063  1.11365E+00 0.01515 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.74860E-01 0.00021  1.14805E+00 0.00707 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61756E-01 0.00035  5.54282E-01 0.00529 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23262E+00 0.00020  3.97146E-01 0.00366 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21167E+00 0.00063  2.99588E-01 0.01489 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21274E+00 0.00021  2.90406E-01 0.00710 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27345E+00 0.00035  6.01445E-01 0.00524 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.39161E-03 0.00564  2.55740E-04 0.03005  1.09856E-03 0.01463  7.22595E-04 0.01871  1.46963E-03 0.01634  2.35544E-03 0.00949  6.87524E-04 0.01848  6.26545E-04 0.02037  1.75568E-04 0.03550 ];
LAMBDA                    (idx, [1:  18]) = [  4.13654E-01 0.00925  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 3.7E-09  1.33042E-01 4.8E-09  2.92467E-01 0.0E+00  6.66488E-01 4.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

