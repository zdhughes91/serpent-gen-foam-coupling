
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/predictAlpha/changeSpecificIso/c' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 11:09:50 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 11:52:33 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701277790496 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 4 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   4]) = [  1.00386E+00  9.87620E-01  9.95253E-01  1.01327E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.49439E-01 0.00020  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.50561E-01 0.00016  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54388E-01 7.6E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.13347E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14873E+01 0.00037  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40498E-01 3.6E-06  5.91371E-02 5.6E-05  3.65015E-04 0.00059  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.12571E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.10846E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11944E+02 0.00018  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.09045E+02 0.00038  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 10000219 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00002E+05 0.00051 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00002E+05 0.00051 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.69361E+02 ;
RUNNING_TIME              (idx, 1)        =  4.27130E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.49500E-02  6.49500E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.11666E-03  2.11666E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  4.26460E+01  4.26460E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  4.27127E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.96508 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.98229E+00 0.00106 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.94766E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 2424.77 ;
MEMSIZE                   (idx, 1)        = 2343.67 ;
XS_MEMSIZE                (idx, 1)        = 1185.78 ;
MAT_MEMSIZE               (idx, 1)        = 489.01 ;
RES_MEMSIZE               (idx, 1)        = 0.93 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 667.95 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 81.10 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 640645 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92574E-06 0.00024  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.44245E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.18336E-01 0.00044  9.99226E-01 1.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23899E-04 0.01758  7.73584E-04 0.01750 ];
U235_CAPT                 (idx, [1:   4]) = [  1.59392E-01 0.00080  3.92112E-01 0.00065 ];
U238_CAPT                 (idx, [1:   4]) = [  1.41150E-02 0.00227  3.47237E-02 0.00226 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 10000219 1.00000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.34096E+04 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 10000219 1.00734E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 4067967 4.09538E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 4206524 4.21792E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 1725728 1.76011E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 10000219 1.00734E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.76579E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35722E-11 0.00027 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02402E+00 0.00027 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.18793E-01 0.00027 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.06503E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.25297E-01 0.00016 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92574E-01 0.00024 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.41964E+02 0.00025 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.74703E-01 0.00073 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.10570E+01 0.00027 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05957E+00 0.00034 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.40279E-01 0.00053 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.39387E-01 0.00064 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.96525E+00 0.00085 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43475E-01 0.00015 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76898E-01 5.0E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25166E+00 0.00032 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03135E+00 0.00035 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44518E+00 2.2E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03140E+00 0.00036  1.02381E+00 0.00035  7.53895E-03 0.00614 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03152E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03169E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03152E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.25182E+00 0.00022 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.30218E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.30220E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.42392E-05 0.00201 ];
IMP_EALF                  (idx, [1:   2]) = [  4.42304E-05 0.00172 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.02204E-01 0.00228 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.02300E-01 0.00053 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.46014E-03 0.00401  2.12907E-04 0.01987  9.86010E-04 0.01058  6.15955E-04 0.01426  1.27833E-03 0.00712  2.05096E-03 0.00792  6.10129E-04 0.01339  5.53054E-04 0.01290  1.52791E-04 0.02783 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14793E-01 0.00628  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.4E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.38685E-03 0.00605  2.58104E-04 0.02938  1.12036E-03 0.01629  7.13842E-04 0.01912  1.46268E-03 0.01318  2.30833E-03 0.01100  7.04362E-04 0.02015  6.42130E-04 0.02145  1.77047E-04 0.04164 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.17196E-01 0.00964  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 4.4E-09  2.92467E-01 0.0E+00  6.66488E-01 4.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  1.97773E-05 0.00169  1.97769E-05 0.00170  1.97640E-05 0.02147 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.03982E-05 0.00168  2.03978E-05 0.00169  2.03827E-05 0.02144 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.28523E-03 0.00631  2.53249E-04 0.03243  1.11504E-03 0.01541  7.09853E-04 0.02066  1.42078E-03 0.01179  2.29077E-03 0.01085  7.01538E-04 0.01944  6.27539E-04 0.01837  1.66457E-04 0.04252 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.12953E-01 0.01043  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.4E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.5E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  1.83282E-05 0.02359  1.83275E-05 0.02358  1.83421E-05 0.05966 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  1.89015E-05 0.02357  1.89008E-05 0.02357  1.89119E-05 0.05965 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.74292E-03 0.03330  2.58956E-04 0.09987  1.08707E-03 0.05465  6.51652E-04 0.06916  1.24628E-03 0.05118  2.11918E-03 0.04479  6.88831E-04 0.06732  5.69686E-04 0.06676  1.21269E-04 0.13746 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99694E-01 0.02903  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 2.3E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 5.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.72832E-03 0.03265  2.43317E-04 0.09753  1.06397E-03 0.05226  6.57042E-04 0.06971  1.23581E-03 0.05016  2.14212E-03 0.04344  7.03315E-04 0.06454  5.64703E-04 0.06250  1.18030E-04 0.13377 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98714E-01 0.02708  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 2.3E-09  1.33042E-01 4.8E-09  2.92467E-01 0.0E+00  6.66488E-01 4.6E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.68720E+02 0.02444 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  1.96001E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.02153E-05 0.00077 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33551E-03 0.00352 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.74316E+02 0.00384 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  1.79777E-07 0.00081 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.62992E-05 0.00041  1.62997E-05 0.00041  1.62258E-05 0.00432 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  9.09107E-05 0.00095  9.09401E-05 0.00095  8.66648E-05 0.01138 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.20503E-01 0.00062  2.20410E-01 0.00062  2.35043E-01 0.00897 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30692E+01 0.00890 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.10846E+01 0.00021  4.99492E+01 0.00026 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.84529E+05 0.00098  4.63080E+06 0.00145  1.04225E+07 0.00118  1.88744E+07 0.00061  1.99385E+07 0.00037  1.85172E+07 0.00043  1.70235E+07 0.00035  1.50844E+07 0.00035  1.33985E+07 0.00029  1.20711E+07 0.00045  1.10841E+07 0.00035  1.02632E+07 0.00093  9.46175E+06 0.00047  9.04284E+06 0.00017  8.67467E+06 0.00046  7.34780E+06 0.00054  7.26250E+06 0.00057  6.94719E+06 0.00061  6.59817E+06 0.00041  1.21968E+07 0.00033  1.06106E+07 0.00051  6.92127E+06 0.00038  4.15566E+06 0.00034  4.43607E+06 0.00079  3.89752E+06 0.00064  3.09151E+06 0.00077  5.09301E+06 0.00069  1.02776E+06 0.00127  1.28038E+06 0.00012  1.17036E+06 0.00114  6.67929E+05 0.00084  1.17295E+06 0.00093  7.87578E+05 0.00158  6.49146E+05 0.00090  1.21305E+05 0.00365  1.18672E+05 0.00223  1.23340E+05 0.00553  1.27224E+05 0.00282  1.26178E+05 0.00281  1.25067E+05 0.00216  1.28870E+05 0.00271  1.21983E+05 0.00410  2.30279E+05 0.00150  3.69047E+05 0.00192  4.74414E+05 0.00038  1.27185E+06 0.00104  1.37870E+06 0.00163  1.46930E+06 0.00080  9.12606E+05 0.00080  6.18864E+05 0.00138  4.47882E+05 0.00155  4.80827E+05 0.00159  7.92032E+05 0.00106  8.92034E+05 0.00124  1.36998E+06 0.00100  1.63029E+06 0.00118  1.98448E+06 0.00099  1.16947E+06 0.00124  8.34262E+05 0.00168  6.09568E+05 0.00096  5.60378E+05 0.00152  5.70960E+05 0.00096  4.98359E+05 0.00140  3.47902E+05 0.00179  3.34383E+05 0.00178  3.10809E+05 0.00115  2.76200E+05 0.00191  2.25357E+05 0.00083  1.54950E+05 0.00255  5.86385E+04 0.00338 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.25200E+00 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33067E+02 0.00025  8.89754E+00 0.00045 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43248E-01 9.5E-05  6.04908E-01 0.00014 ];
INF_CAPT                  (idx, [1:   4]) = [  2.37999E-03 0.00024  1.00935E-02 0.00115 ];
INF_ABS                   (idx, [1:   4]) = [  4.73038E-03 0.00026  2.20111E-02 0.00066 ];
INF_FISS                  (idx, [1:   4]) = [  2.35039E-03 0.00046  1.19176E-02 0.00073 ];
INF_NSF                   (idx, [1:   4]) = [  5.75432E-03 0.00046  2.90330E-02 0.00073 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44824E+00 2.9E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 2.2E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.42964E-08 0.00037  2.20597E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38519E-01 9.5E-05  5.82898E-01 0.00016 ];
INF_SCATT1                (idx, [1:   4]) = [  3.89875E-02 0.00013  4.09705E-02 0.00078 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05474E-02 0.00068  5.43419E-03 0.00359 ];
INF_SCATT3                (idx, [1:   4]) = [  1.05078E-03 0.00657  1.38742E-03 0.01747 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.90321E-04 0.00879  4.11855E-04 0.05959 ];
INF_SCATT5                (idx, [1:   4]) = [  8.64090E-05 0.05298  1.28725E-04 0.10015 ];
INF_SCATT6                (idx, [1:   4]) = [  4.20452E-04 0.00488  5.05957E-05 0.21890 ];
INF_SCATT7                (idx, [1:   4]) = [  4.36791E-05 0.09831  5.66900E-05 0.13056 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38573E-01 9.6E-05  5.82898E-01 0.00016 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.90002E-02 0.00013  4.09705E-02 0.00078 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05499E-02 0.00068  5.43419E-03 0.00359 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.05142E-03 0.00658  1.38742E-03 0.01747 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.90308E-04 0.00877  4.11855E-04 0.05959 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.64295E-05 0.05312  1.28725E-04 0.10015 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.20464E-04 0.00487  5.05957E-05 0.21890 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.37057E-05 0.09856  5.66900E-05 0.13056 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49206E-01 8.1E-05  5.52616E-01 0.00013 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33758E+00 8.2E-05  6.03192E-01 0.00013 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.67563E-03 0.00029  2.20111E-02 0.00066 ];
INF_REMXS                 (idx, [1:   4]) = [  6.81121E-03 0.00019  2.89605E-02 0.00073 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.36437E-01 9.5E-05  2.08146E-03 0.00053  6.95066E-03 0.00099  5.75947E-01 0.00016 ];
INF_S1                    (idx, [1:   8]) = [  3.91509E-02 0.00014 -1.63413E-04 0.00453 -5.30042E-04 0.00839  4.15005E-02 0.00075 ];
INF_S2                    (idx, [1:   8]) = [  1.06508E-02 0.00069 -1.03466E-04 0.00379 -3.01859E-04 0.01456  5.73605E-03 0.00388 ];
INF_S3                    (idx, [1:   8]) = [  1.15537E-03 0.00570 -1.04589E-04 0.00355 -1.20664E-04 0.03023  1.50808E-03 0.01390 ];
INF_S4                    (idx, [1:   8]) = [ -5.52972E-04 0.00936 -3.73490E-05 0.00916 -5.97629E-05 0.05472  4.71618E-04 0.05627 ];
INF_S5                    (idx, [1:   8]) = [  8.15847E-05 0.05350  4.82432E-06 0.05523 -3.24502E-05 0.05609  1.61175E-04 0.07529 ];
INF_S6                    (idx, [1:   8]) = [  4.13634E-04 0.00494  6.81841E-06 0.02883 -1.75642E-05 0.14173  6.81599E-05 0.18333 ];
INF_S7                    (idx, [1:   8]) = [  4.39024E-05 0.09606 -2.23223E-07 1.00000 -1.43429E-05 0.30086  7.10329E-05 0.07995 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.36492E-01 9.5E-05  2.08146E-03 0.00053  6.95066E-03 0.00099  5.75947E-01 0.00016 ];
INF_SP1                   (idx, [1:   8]) = [  3.91636E-02 0.00014 -1.63413E-04 0.00453 -5.30042E-04 0.00839  4.15005E-02 0.00075 ];
INF_SP2                   (idx, [1:   8]) = [  1.06533E-02 0.00068 -1.03466E-04 0.00379 -3.01859E-04 0.01456  5.73605E-03 0.00388 ];
INF_SP3                   (idx, [1:   8]) = [  1.15601E-03 0.00572 -1.04589E-04 0.00355 -1.20664E-04 0.03023  1.50808E-03 0.01390 ];
INF_SP4                   (idx, [1:   8]) = [ -5.52959E-04 0.00935 -3.73490E-05 0.00916 -5.97629E-05 0.05472  4.71618E-04 0.05627 ];
INF_SP5                   (idx, [1:   8]) = [  8.16052E-05 0.05367  4.82432E-06 0.05523 -3.24502E-05 0.05609  1.61175E-04 0.07529 ];
INF_SP6                   (idx, [1:   8]) = [  4.13646E-04 0.00494  6.81841E-06 0.02883 -1.75642E-05 0.14173  6.81599E-05 0.18333 ];
INF_SP7                   (idx, [1:   8]) = [  4.39289E-05 0.09627 -2.23223E-07 1.00000 -1.43429E-05 0.30086  7.10329E-05 0.07995 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70622E-01 0.00073  8.40220E-01 0.00214 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.75237E-01 0.00082  1.13940E+00 0.00432 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.75200E-01 0.00096  1.12554E+00 0.00461 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.61874E-01 0.00095  5.54247E-01 0.00271 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23173E+00 0.00073  3.96729E-01 0.00214 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21108E+00 0.00082  2.92573E-01 0.00433 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21124E+00 0.00096  2.96179E-01 0.00461 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27288E+00 0.00095  6.01435E-01 0.00273 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.38685E-03 0.00605  2.58104E-04 0.02938  1.12036E-03 0.01629  7.13842E-04 0.01912  1.46268E-03 0.01318  2.30833E-03 0.01100  7.04362E-04 0.02015  6.42130E-04 0.02145  1.77047E-04 0.04164 ];
LAMBDA                    (idx, [1:  18]) = [  4.17196E-01 0.00964  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 4.2E-09  1.33042E-01 4.4E-09  2.92467E-01 0.0E+00  6.66488E-01 4.0E-09  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];

