

import subprocess
import os

sss2path = './../../../../Serpent2/src/src/sss2'

def launch_executable(executable_path,args):
    """
    this launches the exectuable but does not wait for it to finsih so you can keep working
    """
    os.chdir(args[0])
    try:
        subprocess.Popen([executable_path]+args, shell=True)
        print(f"Executable '{executable_path}' launched successfully.")
    except Exception as e:
        print(f"Error launching executable: {e}")
        os.chdir("..")
    os.chdir("..")

def extractValue(startline,path):
    """
    extract a value from serpent output file
        start (str) - what the line starts with that you want the d for
                        ex. "ANA_KEFF" or "BETA_EFF"
        path (str) - the path to the file that was run
    """
    with open(path+'_res.m','r') as file:
        for line in file:
            if line.startswith(startline):
                parts = line.split('=')
                if len(parts) > 1:
                    stringVals = parts[1].split()[1:-1]
                    keff = [float(value) for value in stringVals ]
                    return keff

d = {
    'base':{},
    'c':{},
    'nb':{},
    'u235':{},
    'u238':{},
    'u':{}
}


# for case in d:
#     with open(case+'_res.m','w') as file:
#         d[case]['keff'] = extractValue("ANA_KEFF",case+'/'+case+'_res.m')
#         d[case]['beta'] = extractValue("BETA_EFF",case+'/'+case+'_res.m')
for case in d:
    d[case]['keff'] = extractValue("ANA_KEFF",case+'/'+case)
    d[case]['beta'] = extractValue("BETA_EFF",case+'/'+case)
    print('-'*20,case)
    print(' keff={:0.6f} +/- {:0.6f} '.format(d[case]['keff'][0],d[case]['keff'][1]))
    print(' beta={:0.6f}  '.format(d[case]['beta'][0]))

print('-'*30,'reactivity feedback','-'*10)
sum_pcm,sum_dol = 0,0
for case in d:
    if case == "base": continue
    rho_pcm = (d[case]['keff'][0] - d['base']['keff'][0]) * 1e5 # pcm
    rho_dol = rho_pcm / (1e5 * d['base']['beta'][0])
    sum_pcm += rho_pcm
    sum_dol += rho_dol
    d[case]['rho_pcm'] = rho_pcm
    d[case]['rho_dol'] = rho_dol
    print('-'*20,case)
    print(' rho_pcm= {:0.2f}  '.format(rho_pcm))
    print(' rho_$= {:0.6f}  '.format(rho_dol))
print('rhoSum_pcm = {:0.6f} pcm '.format(sum_pcm))
print('rhoSum_dol = {:0.6f} $ '.format(sum_dol))

