/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
	version     2.0;
	format      ascii;
	class       dictionary;
	location    "constant/neutroRegion";
	object      nuclearData;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

energyGroups                2; // [0.000000e+00, 6.250000e-01, 2.000000e+07] eV

precGroups                  6;

//- Point Kinetics data
fastNeutrons                false;

promptGenerationTime        4.318156e-05; // [s] (5.547209e-08)

Beta
(
	2.282190e-04  // (1.481285e-07)
	1.178546e-03  // (7.647760e-07)
	1.125457e-03  // (7.302258e-07)
	2.524596e-03  // (1.637634e-06)
	1.036341e-03  // (6.718366e-07)
	4.340755e-04  // (2.814153e-07)
);
// Beta tot = 652.723 pcm

lambda
(
	1.333624e-02  // (8.651015e-06)
	3.273719e-02  // (2.122717e-05)
	1.207840e-01  // (7.829295e-05)
	3.028230e-01  // (1.961877e-04)
	8.496762e-01  // (5.497812e-04)
	2.853625e+00  // (1.846617e-03)
);

// Alpha Doppler
feedbackCoeffFastDoppler    -7.414e-03; // [-] (+/- 6.203e-04)

//- Representative of fuel axial expansion
feedbackCoeffTFuel          5.882e-06; // [1/K] (+/- 7.391e-08)

//- Representative of in-assembly structure density change
//  (i.e. cladding AND wrapper wire)
feedbackCoeffTClad          0; // [1/K]

feedbackCoeffTCool          -9.595e-06; // [1/K] (+/- 1.288e-06)

feedbackCoeffRhoCool        0; // [1/(kg/m3)]

feedbackCoeffTStructMech	0;

//- Representative of structure radial expansion
feedbackCoeffTStruct        9.356e-06; // [1/K] (+/- 1.153e-07)

absoluteDrivelineExpansionCoeff 0; // [m/K]

//- CR insertion/driveline expansion (in m) vs reactivity
controlRodReactivityMap ();

//- Represents the reactivity instertion ramp in this case
externalReactivityTimeProfile
{
    type        table;

    startTime   10000000;

    table       table
    (
        (   0       0          )
        (   0.01    -10000e-5  )
    );
}



fuelFeedbackZones
(
	fuelElement_45
	fuelElement_39
	fuelElement_34
	fuelElement_30
	fuelElement_26
	fuelElement_22
	fuelElement_19
	fuelElement_17
	fuelElement_15
	fuelElement_13
);

coolFeedbackZones
(
	fuelElement_45
	fuelElement_39
	fuelElement_34
	fuelElement_30
	fuelElement_26
	fuelElement_22
	fuelElement_19
	fuelElement_17
	fuelElement_15
	fuelElement_13
	unloadedElementsTypeC
	unloadedElementsTypeT
);

structFeedbackZones
(
	coreSupport
	supportPlate
);

drivelineFeedbackZones ();


//- Diffusion data
zones
(
	reflectorSystemBe  // OpenMC: Reflector System Container (Beryllium)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (9.360519e-06 2.796859e-04);
		D                  nonuniform List<scalar> 2 (5.407346e-03 4.453832e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(6.680412e+01 6.860840e-01)
			(5.149208e-02 7.475955e+01)
		);
		scatteringMatrixP1 2 2 (
			(5.876457e+00 -1.871687e-01)
			(9.359444e-03 7.297407e-01)
		);
		scatteringMatrixP2 2 2 (
			(4.986128e-01 -2.117228e-02)
			(-4.871463e-03 -3.013632e+00)
		);
		scatteringMatrixP3 2 2 (
			(1.067522e-01 -2.247227e-03)
			(-5.184610e-03 -2.575577e+00)
		);
		scatteringMatrixP4 2 2 (
			(1.628593e-02 -2.798753e-03)
			(-2.407871e-03 -1.961101e+00)
		);
		scatteringMatrixP5 2 2 (
			(-1.387450e-02 1.357277e-03)
			(-1.778009e-04 -1.354676e+00)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (7.197993e-01 1.277180e-01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273721e-02 1.207840e-01 3.028226e-01 8.496746e-01 2.853620e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (1.382558e-01 4.201014e-02);
	}

	controlDrum  // OpenMC: Adapted a few values because it was crashing!!!!!!
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (5.453002e-04 5.453002e-04);
		D                  nonuniform List<scalar> 2 (1.797693e+1 3.301990e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(0.000000e+00 0.000000e+00)
			(0.000000e+00 7.667503e+01)
		);
		scatteringMatrixP1 2 2 (
			(0.000000e+00 0.000000e+00)
			(0.000000e+00 -1.307164e+01)
		);
		scatteringMatrixP2 2 2 (
			(0.000000e+00 0.000000e+00)
			(0.000000e+00 -6.408914e+00)
		);
		scatteringMatrixP3 2 2 (
			(0.000000e+00 0.000000e+00)
			(0.000000e+00 7.565098e+00)
		);
		scatteringMatrixP4 2 2 (
			(0.000000e+00 0.000000e+00)
			(0.000000e+00 -5.800154e+00)
		);
		scatteringMatrixP5 2 2 (
			(0.000000e+00 0.000000e+00)
			(0.000000e+00 -5.975245e+00)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (1.015083e+01 1.015083e+01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273721e-02 1.207840e-01 3.028226e-01 8.496746e-01 2.853620e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.662241e-06 3.662241e-06);
	}

	fuelElement_13  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.128 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (5.328004e-06 1.930669e-04);
		D                  nonuniform List<scalar> 2 (1.423802e-02 9.520696e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (4.497764e-01 1.083632e+01);
		sigmaPow           nonuniform List<scalar> 2 (5.706415e-12 1.378030e-10);
		scatteringMatrixP0 2 2 (
			(2.488686e+01 1.259846e-01)
			(6.880878e-01 3.042975e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.966164e+00 -2.195758e-02)
			(-6.264060e-02 1.755867e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.727780e-01 -4.153629e-03)
			(-3.265100e-02 2.276755e-01)
		);
		scatteringMatrixP3 2 2 (
			(6.744593e-02 -1.742614e-03)
			(-1.451979e-02 7.493032e-02)
		);
		scatteringMatrixP4 2 2 (
			(7.787326e-03 -7.947870e-04)
			(-5.018713e-03 3.507442e-02)
		);
		scatteringMatrixP5 2 2 (
			(3.740423e-03 -3.711329e-04)
			(-1.944326e-03 9.128550e-03)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (4.869399e-01 6.033986e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.278813e-04 1.176385e-03 1.123154e-03 2.518500e-03 1.032859e-03 4.326505e-04);
		lambda             nonuniform List<scalar> 6 (1.333606e-02 3.273857e-02 1.207810e-01 3.027903e-01 8.495348e-01 2.853150e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (2.085637e-02 2.386992e-02);
	}

	fuelElement_15  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.148 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (5.116260e-06 1.897540e-04);
		D                  nonuniform List<scalar> 2 (1.429248e-02 9.345848e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (5.045185e-01 1.228004e+01);
		sigmaPow           nonuniform List<scalar> 2 (6.399789e-12 1.561624e-10);
		scatteringMatrixP0 2 2 (
			(2.479395e+01 1.205710e-01)
			(7.267864e-01 3.037634e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.987551e+00 -2.091612e-02)
			(-5.889789e-02 1.792867e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.824201e-01 -3.247690e-03)
			(-4.208885e-02 2.048650e-01)
		);
		scatteringMatrixP3 2 2 (
			(6.343294e-02 -2.498577e-03)
			(-8.649472e-03 8.385931e-02)
		);
		scatteringMatrixP4 2 2 (
			(7.098174e-03 -5.910817e-04)
			(-5.014790e-03 4.397050e-02)
		);
		scatteringMatrixP5 2 2 (
			(3.021296e-03 -5.896567e-04)
			(-4.344038e-03 4.334328e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.148425e-01 6.772909e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.278923e-04 1.176459e-03 1.123235e-03 2.518721e-03 1.032992e-03 4.327045e-04);
		lambda             nonuniform List<scalar> 6 (1.333607e-02 3.273851e-02 1.207811e-01 3.027917e-01 8.495408e-01 2.853171e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (6.901671e-03 8.787511e-03);
	}

	fuelElement_17  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.17 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.773282e-06 1.794354e-04);
		D                  nonuniform List<scalar> 2 (1.439032e-02 9.246406e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (5.451866e-01 1.322953e+01);
		sigmaPow           nonuniform List<scalar> 2 (6.913144e-12 1.682370e-10);
		scatteringMatrixP0 2 2 (
			(2.461707e+01 1.089851e-01)
			(8.065315e-01 3.016402e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.981046e+00 -1.915182e-02)
			(-8.112729e-02 1.800189e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.960504e-01 -3.693005e-03)
			(-4.034021e-02 2.403617e-01)
		);
		scatteringMatrixP3 2 2 (
			(7.100628e-02 -1.614931e-03)
			(-1.318625e-02 7.166751e-02)
		);
		scatteringMatrixP4 2 2 (
			(9.618596e-03 -6.029233e-04)
			(-3.096880e-03 2.440962e-02)
		);
		scatteringMatrixP5 2 2 (
			(5.724305e-03 -3.363491e-04)
			(-1.315816e-03 1.386828e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.235626e-01 7.314531e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279190e-04 1.176641e-03 1.123434e-03 2.519263e-03 1.033317e-03 4.328371e-04);
		lambda             nonuniform List<scalar> 6 (1.333608e-02 3.273836e-02 1.207814e-01 3.027951e-01 8.495556e-01 2.853220e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (1.022157e-02 1.632848e-02);
	}

	fuelElement_19  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.195 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.600280e-06 1.758758e-04);
		D                  nonuniform List<scalar> 2 (1.441914e-02 9.052071e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (6.064216e-01 1.481184e+01);
		sigmaPow           nonuniform List<scalar> 2 (7.688122e-12 1.883588e-10);
		scatteringMatrixP0 2 2 (
			(2.455329e+01 1.057821e-01)
			(8.607450e-01 3.009179e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.993430e+00 -1.820209e-02)
			(-7.850065e-02 1.795380e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.996993e-01 -3.755085e-03)
			(-3.879084e-02 1.919767e-01)
		);
		scatteringMatrixP3 2 2 (
			(8.629386e-02 -1.218244e-03)
			(-1.367381e-02 -3.123689e-03)
		);
		scatteringMatrixP4 2 2 (
			(1.162541e-02 -1.740378e-04)
			(-8.066519e-03 -7.474430e-04)
		);
		scatteringMatrixP5 2 2 (
			(3.436828e-03 -7.804173e-04)
			(-4.053858e-03 4.735433e-03)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.570221e-01 8.137094e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279302e-04 1.176721e-03 1.123524e-03 2.519516e-03 1.033473e-03 4.329010e-04);
		lambda             nonuniform List<scalar> 6 (1.333609e-02 3.273829e-02 1.207816e-01 3.027969e-01 8.495633e-01 2.853246e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (4.176132e-03 7.393662e-03);
	}

	fuelElement_22  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.224 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.359886e-06 1.705311e-04);
		D                  nonuniform List<scalar> 2 (1.442685e-02 8.848332e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (6.685688e-01 1.641534e+01);
		sigmaPow           nonuniform List<scalar> 2 (8.473985e-12 2.087502e-10);
		scatteringMatrixP0 2 2 (
			(2.449192e+01 9.772253e-02)
			(9.121788e-01 3.003357e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.973816e+00 -1.748405e-02)
			(-7.495340e-02 1.761118e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.128846e-01 -3.400039e-03)
			(-3.757008e-02 1.581491e-01)
		);
		scatteringMatrixP3 2 2 (
			(7.810997e-02 -1.473644e-03)
			(-1.644481e-02 6.324542e-03)
		);
		scatteringMatrixP4 2 2 (
			(1.378112e-02 -6.534163e-04)
			(-4.435182e-03 1.389112e-02)
		);
		scatteringMatrixP5 2 2 (
			(6.746478e-03 -1.090844e-04)
			(-2.919454e-03 -1.108579e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.860571e-01 8.968999e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279531e-04 1.176874e-03 1.123689e-03 2.519962e-03 1.033737e-03 4.330086e-04);
		lambda             nonuniform List<scalar> 6 (1.333611e-02 3.273818e-02 1.207818e-01 3.027996e-01 8.495749e-01 2.853285e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (5.456728e-03 1.132452e-02);
	}

	fuelElement_26  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.258 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.187742e-06 1.673144e-04);
		D                  nonuniform List<scalar> 2 (1.443360e-02 8.639302e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (7.436174e-01 1.847370e+01);
		sigmaPow           nonuniform List<scalar> 2 (9.423257e-12 2.349258e-10);
		scatteringMatrixP0 2 2 (
			(2.446219e+01 9.200442e-02)
			(1.013625e+00 2.993607e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.996399e+00 -1.627802e-02)
			(-7.691065e-02 1.876106e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.259142e-01 -2.855864e-03)
			(-5.002093e-02 2.635183e-01)
		);
		scatteringMatrixP3 2 2 (
			(8.720408e-02 -1.479702e-03)
			(-1.846954e-02 1.151334e-01)
		);
		scatteringMatrixP4 2 2 (
			(1.577856e-02 -6.454361e-04)
			(-7.113985e-03 2.783592e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.101807e-02 -1.404806e-04)
			(-7.316926e-03 5.458372e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.254267e-01 1.007173e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279727e-04 1.177005e-03 1.123832e-03 2.520352e-03 1.033969e-03 4.331034e-04);
		lambda             nonuniform List<scalar> 6 (1.333612e-02 3.273807e-02 1.207821e-01 3.028020e-01 8.495853e-01 2.853320e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (7.283181e-03 1.731464e-02);
	}

	fuelElement_30  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.297 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (3.879447e-06 1.620421e-04);
		D                  nonuniform List<scalar> 2 (1.447014e-02 8.411376e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (8.091815e-01 2.045423e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.025059e-11 2.601118e-10);
		scatteringMatrixP0 2 2 (
			(2.439362e+01 8.349609e-02)
			(1.099677e+00 2.985572e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.011340e+00 -1.468257e-02)
			(-8.467514e-02 1.876427e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.332925e-01 -3.123201e-03)
			(-5.646332e-02 2.060316e-01)
		);
		scatteringMatrixP3 2 2 (
			(8.810587e-02 -1.174514e-03)
			(-1.662921e-02 8.000732e-02)
		);
		scatteringMatrixP4 2 2 (
			(1.596759e-02 -5.850481e-04)
			(-1.377930e-02 3.349982e-02)
		);
		scatteringMatrixP5 2 2 (
			(5.749847e-03 -9.863509e-05)
			(-3.827595e-03 -4.537016e-04)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.541570e-01 1.111926e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.280093e-04 1.177249e-03 1.124096e-03 2.521068e-03 1.034392e-03 4.332761e-04);
		lambda             nonuniform List<scalar> 6 (1.333615e-02 3.273789e-02 1.207825e-01 3.028063e-01 8.496040e-01 2.853383e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (7.931544e-03 2.374963e-02);
	}

	fuelElement_34  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.341 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (3.582754e-06 1.569044e-04);
		D                  nonuniform List<scalar> 2 (1.448179e-02 8.180509e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (8.787139e-01 2.261018e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.112753e-11 2.875285e-10);
		scatteringMatrixP0 2 2 (
			(2.434983e+01 7.418204e-02)
			(1.206117e+00 2.978672e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.016615e+00 -1.243610e-02)
			(-8.934830e-02 1.860721e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.456965e-01 -2.869142e-03)
			(-5.817372e-02 2.688781e-01)
		);
		scatteringMatrixP3 2 2 (
			(8.886117e-02 -9.487378e-04)
			(-2.730341e-02 6.689216e-02)
		);
		scatteringMatrixP4 2 2 (
			(1.719717e-02 -5.735578e-04)
			(-1.101929e-02 1.098415e-02)
		);
		scatteringMatrixP5 2 2 (
			(8.591821e-03 -3.820892e-04)
			(-8.539786e-03 4.913554e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.840354e-01 1.227374e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.280489e-04 1.177514e-03 1.124383e-03 2.521843e-03 1.034850e-03 4.334630e-04);
		lambda             nonuniform List<scalar> 6 (1.333617e-02 3.273770e-02 1.207829e-01 3.028110e-01 8.496241e-01 2.853450e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (6.450511e-03 2.440872e-02);
	}

	fuelElement_39  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.392 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (3.262044e-06 1.517042e-04);
		D                  nonuniform List<scalar> 2 (1.448765e-02 7.919073e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (9.483847e-01 2.490580e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.200496e-11 3.167213e-10);
		scatteringMatrixP0 2 2 (
			(2.432112e+01 6.487182e-02)
			(1.357616e+00 2.967785e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.030863e+00 -1.162054e-02)
			(-9.403444e-02 1.824427e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.556492e-01 -2.093171e-03)
			(-6.000321e-02 2.512870e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.051732e-01 -1.109652e-03)
			(-2.393774e-02 1.135411e-01)
		);
		scatteringMatrixP4 2 2 (
			(2.412700e-02 -2.981950e-04)
			(-1.479107e-02 2.020738e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.239436e-02 -1.848709e-04)
			(-7.826865e-03 2.918462e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (7.137415e-01 1.353467e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.281018e-04 1.177861e-03 1.124757e-03 2.522850e-03 1.035438e-03 4.337035e-04);
		lambda             nonuniform List<scalar> 6 (1.333621e-02 3.273745e-02 1.207834e-01 3.028168e-01 8.496494e-01 2.853535e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (5.005799e-03 2.523868e-02);
	}

	fuelElement_45  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.45 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (2.381812e-06 1.428599e-04);
		D                  nonuniform List<scalar> 2 (1.452524e-02 7.750135e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (9.260444e-01 2.641663e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.171199e-11 3.359343e-10);
		scatteringMatrixP0 2 2 (
			(2.433374e+01 3.721498e-02)
			(1.652796e+00 2.939141e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.049628e+00 -6.596501e-03)
			(-1.056494e-01 1.869406e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.601725e-01 -1.282957e-03)
			(-7.820501e-02 2.336937e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.016165e-01 -6.181198e-04)
			(-3.142050e-02 5.391947e-02)
		);
		scatteringMatrixP4 2 2 (
			(2.556874e-02 -1.879020e-04)
			(-1.757239e-02 1.648458e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.201262e-02 -6.826381e-05)
			(-7.864612e-03 4.378596e-03)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.638987e-01 1.454114e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.283173e-04 1.179162e-03 1.126107e-03 2.526299e-03 1.037297e-03 4.344669e-04);
		lambda             nonuniform List<scalar> 6 (1.333629e-02 3.273683e-02 1.207848e-01 3.028315e-01 8.497130e-01 2.853749e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.485010e-02 4.181854e-01);
	}

	fuelElement  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.45 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (2.381812e-06 1.428599e-04);
		D                  nonuniform List<scalar> 2 (1.452524e-02 7.750135e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (9.260444e-01 2.641663e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.171199e-11 3.359343e-10);
		scatteringMatrixP0 2 2 (
			(2.433374e+01 3.721498e-02)
			(1.652796e+00 2.939141e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.049628e+00 -6.596501e-03)
			(-1.056494e-01 1.869406e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.601725e-01 -1.282957e-03)
			(-7.820501e-02 2.336937e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.016165e-01 -6.181198e-04)
			(-3.142050e-02 5.391947e-02)
		);
		scatteringMatrixP4 2 2 (
			(2.556874e-02 -1.879020e-04)
			(-1.757239e-02 1.648458e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.201262e-02 -6.826381e-05)
			(-7.864612e-03 4.378596e-03)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.638987e-01 1.454114e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.283173e-04 1.179162e-03 1.126107e-03 2.526299e-03 1.037297e-03 4.344669e-04);
		lambda             nonuniform List<scalar> 6 (1.333629e-02 3.273683e-02 1.207848e-01 3.028315e-01 8.497130e-01 2.853749e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.485010e-02 4.181854e-01);
	}

	unloadedElementsTypeC  // OpenMC: uCentralUnloadedFuelElement
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (3.564359e-06 1.752889e-04);
		D                  nonuniform List<scalar> 2 (1.058089e-02 6.101174e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(4.666374e+01 5.154463e-01)
			(1.261315e+00 7.625482e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.586001e+01 1.390388e-01)
			(3.845124e-01 1.885773e+01)
		);
		scatteringMatrixP2 2 2 (
			(6.241028e+00 -3.219111e-02)
			(1.230775e-01 5.980988e+00)
		);
		scatteringMatrixP3 2 2 (
			(3.351589e-01 -4.357264e-02)
			(6.413651e-04 2.010891e+00)
		);
		scatteringMatrixP4 2 2 (
			(-6.836102e-01 -1.823374e-02)
			(-3.786511e-02 4.535115e-01)
		);
		scatteringMatrixP5 2 2 (
			(-1.425151e-02 -3.969787e-03)
			(-4.561538e-02 1.751074e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (7.027448e-01 2.298893e+00);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028227e-01 8.496749e-01 2.853621e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (2.251785e-02 6.823207e-02);
	}

	unloadedElementsTypeT  // OpenMC: uCentralUnloadedFuelElementTa
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (2.497234e-06 1.596879e-04);
		D                  nonuniform List<scalar> 2 (1.254106e-02 7.633825e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(4.079437e+01 3.013421e-01)
			(1.496035e+00 6.843235e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.524835e+01 8.428791e-02)
			(5.594949e-01 1.915690e+01)
		);
		scatteringMatrixP2 2 2 (
			(6.099744e+00 -2.259752e-02)
			(1.974896e-01 6.570808e+00)
		);
		scatteringMatrixP3 2 2 (
			(3.094265e-01 -2.706450e-02)
			(1.851179e-02 2.386297e+00)
		);
		scatteringMatrixP4 2 2 (
			(-6.576515e-01 -1.030183e-02)
			(-5.820082e-02 6.030177e-01)
		);
		scatteringMatrixP5 2 2 (
			(8.110370e-03 -1.811531e-03)
			(-6.193112e-02 3.017065e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (1.044956e+00 3.214211e+00);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028227e-01 8.496749e-01 2.853621e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.351988e-03 2.969693e-02);
	}

	coreSupport  // OpenMC: Cell cLowerCoreSupport (6061T6 Alloy)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (3.119103e-06 2.528190e-04);
		D                  nonuniform List<scalar> 2 (2.292759e-02 3.585541e-02);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(1.710533e+01 8.950150e-03)
			(3.859566e-02 8.367818e+00)
		);
		scatteringMatrixP1 2 2 (
			(2.597661e+00 -2.404189e-03)
			(-1.048457e-03 -1.056359e-02)
		);
		scatteringMatrixP2 2 2 (
			(7.914639e-01 -1.635938e-04)
			(-3.370156e-03 -1.389446e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.315906e-01 -1.762775e-05)
			(5.389325e-05 -1.252955e-01)
		);
		scatteringMatrixP4 2 2 (
			(3.634137e-02 -6.643334e-05)
			(-1.729328e-04 -1.394426e-01)
		);
		scatteringMatrixP5 2 2 (
			(1.103259e-02 -2.160667e-05)
			(-2.005109e-03 -9.205517e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (3.007928e-02 8.660114e-01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028227e-01 8.496749e-01 2.853621e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (1.753013e-03 3.148724e-03);
	}

	supportPlate  // OpenMC: Cell cUpperCoreSupport (6061T6 Alloy)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (2.953833e-06 2.314348e-04);
		D                  nonuniform List<scalar> 2 (2.379699e-02 3.640574e-02);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(1.629011e+01 7.252558e-03)
			(4.975415e-02 8.364089e+00)
		);
		scatteringMatrixP1 2 2 (
			(2.310301e+00 -1.647961e-03)
			(-3.368593e-03 7.068637e-02)
		);
		scatteringMatrixP2 2 2 (
			(6.678345e-01 -2.590444e-04)
			(-3.126650e-03 -1.489103e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.069522e-01 -5.299521e-05)
			(-2.389657e-03 -9.592232e-02)
		);
		scatteringMatrixP4 2 2 (
			(3.206566e-02 -1.341096e-04)
			(-7.025836e-04 -1.123747e-01)
		);
		scatteringMatrixP5 2 2 (
			(1.037024e-02 1.072762e-04)
			(2.584503e-04 -1.109698e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (2.561778e-02 8.071824e-01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028227e-01 8.496749e-01 2.853621e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.314679e-03 9.268299e-03);
	}

	reflector  // OpenMC: Reflector (Graphite)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (8.615836e-06 2.452122e-04);
		D                  nonuniform List<scalar> 2 (7.717328e-03 6.415892e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(4.595216e+01 3.161789e-01)
			(1.431291e-01 5.226543e+01)
		);
		scatteringMatrixP1 2 2 (
			(3.078219e+00 -8.514283e-02)
			(-5.791948e-03 8.634468e-01)
		);
		scatteringMatrixP2 2 2 (
			(2.615665e-01 -9.460348e-03)
			(-1.253189e-02 -9.253242e-01)
		);
		scatteringMatrixP3 2 2 (
			(3.996878e-02 -1.601186e-03)
			(-5.575172e-03 -7.460124e-01)
		);
		scatteringMatrixP4 2 2 (
			(-7.760223e-03 -1.290254e-03)
			(-2.129415e-03 -5.893458e-01)
		);
		scatteringMatrixP5 2 2 (
			(-3.520242e-03 -7.156558e-04)
			(-9.163141e-04 -4.761671e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (3.176483e-01 1.656989e-01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028227e-01 8.496749e-01 2.853621e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (1.987203e-01 7.544126e-02);
	}

	reflectorSystem  // OpenMC: Reflector System (Berrylium ring, control drums (102.300 deg) and tie rods)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (9.077399e-06 2.817945e-04);
		D                  nonuniform List<scalar> 2 (5.137404e-03 4.233555e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(6.861984e+01 5.886148e-01)
			(6.116274e-02 7.614976e+01)
		);
		scatteringMatrixP1 2 2 (
			(5.173552e+00 -1.587388e-01)
			(9.039465e-03 5.920319e-01)
		);
		scatteringMatrixP2 2 2 (
			(5.842534e-01 -1.781138e-02)
			(-4.549353e-03 -2.786576e+00)
		);
		scatteringMatrixP3 2 2 (
			(1.327578e-01 -2.908252e-03)
			(-4.197546e-03 -2.360165e+00)
		);
		scatteringMatrixP4 2 2 (
			(3.466004e-02 -1.512485e-03)
			(-2.072355e-03 -1.805088e+00)
		);
		scatteringMatrixP5 2 2 (
			(1.154100e-03 9.596406e-05)
			(-5.703068e-04 -1.217693e+00)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (1.440283e+00 2.570966e+00);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028227e-01 8.496749e-01 2.853621e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (2.124861e-01 6.796316e-02);
	}
);

// ************************************************************************* //
