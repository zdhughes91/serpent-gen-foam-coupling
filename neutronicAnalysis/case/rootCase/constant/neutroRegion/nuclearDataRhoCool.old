/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      nuclearDataRhoCool;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

energyGroups        2; // [0.000000e+00, 6.250000e-01, 2.000000e+07] eV

precGroups          6;

rhoCoolRef          0.91729;

rhoCoolPerturbed    0.73383;

//- Diffusion data
zones
(/*
	fuelElement_13  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.128 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (5.314582e-06 1.931605e-04);
		D                  nonuniform List<scalar> 2 (1.424811e-02 9.530841e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (4.490384e-01 1.084372e+01);
		sigmaPow           nonuniform List<scalar> 2 (5.697052e-12 1.378971e-10);
		scatteringMatrixP0 2 2 (
			(2.485107e+01 1.219432e-01)
			(6.809998e-01 3.034032e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.945140e+00 -2.213108e-02)
			(-5.878009e-02 1.701888e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.606018e-01 -4.451073e-03)
			(-2.983507e-02 2.250590e-01)
		);
		scatteringMatrixP3 2 2 (
			(6.955323e-02 -1.310220e-03)
			(-1.261275e-02 3.357075e-02)
		);
		scatteringMatrixP4 2 2 (
			(9.112544e-03 -3.750034e-04)
			(-6.958981e-03 3.078118e-02)
		);
		scatteringMatrixP5 2 2 (
			(6.899965e-03 -5.898288e-04)
			(-3.733762e-03 1.362977e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (4.821942e-01 6.030081e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.278826e-04 1.176392e-03 1.123162e-03 2.518517e-03 1.032868e-03 4.326540e-04);
		lambda             nonuniform List<scalar> 6 (1.333606e-02 3.273856e-02 1.207810e-01 3.027904e-01 8.495350e-01 2.853151e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (2.073555e-02 2.392016e-02);
	}

	fuelElement_15  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.148 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (5.109459e-06 1.894477e-04);
		D                  nonuniform List<scalar> 2 (1.428671e-02 9.363642e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (5.030653e-01 1.226367e+01);
		sigmaPow           nonuniform List<scalar> 2 (6.381372e-12 1.559543e-10);
		scatteringMatrixP0 2 2 (
			(2.476113e+01 1.175385e-01)
			(7.277116e-01 3.027297e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.940947e+00 -2.118478e-02)
			(-5.161743e-02 1.767362e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.701386e-01 -4.138760e-03)
			(-4.070965e-02 1.676479e-01)
		);
		scatteringMatrixP3 2 2 (
			(7.282375e-02 -1.065635e-03)
			(-1.615859e-02 5.223279e-02)
		);
		scatteringMatrixP4 2 2 (
			(1.689169e-02 -1.214886e-03)
			(-1.024384e-02 6.316778e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.274483e-02 -1.357938e-04)
			(-6.324661e-03 1.454242e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.108733e-01 6.765827e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.278941e-04 1.176469e-03 1.123245e-03 2.518742e-03 1.033000e-03 4.327081e-04);
		lambda             nonuniform List<scalar> 6 (1.333607e-02 3.273851e-02 1.207811e-01 3.027917e-01 8.495408e-01 2.853171e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (6.890843e-03 8.808076e-03);
	}

	fuelElement_17  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.17 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.761705e-06 1.795685e-04);
		D                  nonuniform List<scalar> 2 (1.439058e-02 9.253455e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (5.439286e-01 1.323476e+01);
		sigmaPow           nonuniform List<scalar> 2 (6.897241e-12 1.683034e-10);
		scatteringMatrixP0 2 2 (
			(2.458778e+01 1.075771e-01)
			(8.190955e-01 3.004974e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.953075e+00 -1.955184e-02)
			(-7.016111e-02 1.730699e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.825615e-01 -3.530195e-03)
			(-3.755119e-02 1.795778e-01)
		);
		scatteringMatrixP3 2 2 (
			(7.714666e-02 -1.556206e-03)
			(-2.088315e-02 4.089054e-02)
		);
		scatteringMatrixP4 2 2 (
			(1.976385e-02 -4.256332e-04)
			(-8.380471e-03 3.929811e-02)
		);
		scatteringMatrixP5 2 2 (
			(6.796241e-03 -8.575588e-05)
			(-1.312429e-03 2.785200e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.213558e-01 7.329494e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279217e-04 1.176654e-03 1.123447e-03 2.519291e-03 1.033327e-03 4.328417e-04);
		lambda             nonuniform List<scalar> 6 (1.333608e-02 3.273836e-02 1.207814e-01 3.027951e-01 8.495555e-01 2.853220e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (1.018692e-02 1.634044e-02);
	}

	fuelElement_19  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.195 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.585721e-06 1.756390e-04);
		D                  nonuniform List<scalar> 2 (1.442523e-02 9.085941e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (6.030306e-01 1.480962e+01);
		sigmaPow           nonuniform List<scalar> 2 (7.645073e-12 1.883306e-10);
		scatteringMatrixP0 2 2 (
			(2.451771e+01 1.031243e-01)
			(8.808322e-01 2.998786e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.959690e+00 -1.879708e-02)
			(-8.917586e-02 1.844492e+00)
		);
		scatteringMatrixP2 2 2 (
			(3.956749e-01 -3.658210e-03)
			(-4.322881e-02 2.474718e-01)
		);
		scatteringMatrixP3 2 2 (
			(7.734934e-02 -8.936017e-04)
			(-1.210428e-02 6.860729e-02)
		);
		scatteringMatrixP4 2 2 (
			(1.611624e-02 -6.893443e-04)
			(-8.853760e-03 5.060264e-02)
		);
		scatteringMatrixP5 2 2 (
			(5.926806e-03 7.492800e-05)
			(2.443112e-03 -1.170799e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.522143e-01 8.156635e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279334e-04 1.176738e-03 1.123540e-03 2.519553e-03 1.033490e-03 4.329078e-04);
		lambda             nonuniform List<scalar> 6 (1.333610e-02 3.273829e-02 1.207816e-01 3.027969e-01 8.495634e-01 2.853247e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (4.174033e-03 7.404649e-03);
	}

	fuelElement_22  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.224 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.349619e-06 1.709877e-04);
		D                  nonuniform List<scalar> 2 (1.443840e-02 8.868011e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (6.669167e-01 1.647555e+01);
		sigmaPow           nonuniform List<scalar> 2 (8.453108e-12 2.095158e-10);
		scatteringMatrixP0 2 2 (
			(2.445972e+01 9.599269e-02)
			(9.451218e-01 2.992456e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.958970e+00 -1.738528e-02)
			(-8.860144e-02 1.806187e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.077153e-01 -3.447715e-03)
			(-3.084179e-02 2.345504e-01)
		);
		scatteringMatrixP3 2 2 (
			(8.022846e-02 -1.277527e-03)
			(-1.217974e-02 9.149190e-02)
		);
		scatteringMatrixP4 2 2 (
			(1.746317e-02 -2.023309e-04)
			(-1.300641e-02 4.789097e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.112105e-02 -6.247050e-04)
			(-1.985766e-03 6.630689e-03)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (5.829550e-01 9.031158e+00);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279558e-04 1.176886e-03 1.123700e-03 2.519986e-03 1.033744e-03 4.330117e-04);
		lambda             nonuniform List<scalar> 6 (1.333611e-02 3.273818e-02 1.207818e-01 3.027995e-01 8.495746e-01 2.853284e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (5.461184e-03 1.134042e-02);
	}

	fuelElement_26  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.258 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (4.175840e-06 1.673531e-04);
		D                  nonuniform List<scalar> 2 (1.443869e-02 8.631745e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (7.430035e-01 1.848362e+01);
		sigmaPow           nonuniform List<scalar> 2 (9.415577e-12 2.350520e-10);
		scatteringMatrixP0 2 2 (
			(2.442945e+01 9.007818e-02)
			(9.702124e-01 2.989282e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.971366e+00 -1.665785e-02)
			(-7.469472e-02 1.778816e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.102127e-01 -3.070914e-03)
			(-3.990001e-02 2.238200e-01)
		);
		scatteringMatrixP3 2 2 (
			(9.283589e-02 -1.007215e-03)
			(-1.678628e-02 6.444839e-02)
		);
		scatteringMatrixP4 2 2 (
			(1.812308e-02 -4.473639e-04)
			(-9.788227e-03 2.430181e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.389931e-02 -4.415677e-06)
			(-8.434376e-03 2.547811e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.232580e-01 1.003257e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.279764e-04 1.177025e-03 1.123851e-03 2.520395e-03 1.033987e-03 4.331111e-04);
		lambda             nonuniform List<scalar> 6 (1.333612e-02 3.273807e-02 1.207821e-01 3.028020e-01 8.495854e-01 2.853320e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (7.231558e-03 1.733288e-02);
	}

	fuelElement_30  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.297 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (3.867717e-06 1.617698e-04);
		D                  nonuniform List<scalar> 2 (1.446831e-02 8.411787e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (8.081982e-01 2.042784e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.023819e-11 2.597762e-10);
		scatteringMatrixP0 2 2 (
			(2.435727e+01 8.170413e-02)
			(1.102474e+00 2.976060e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.975097e+00 -1.486303e-02)
			(-9.274476e-02 1.774732e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.279076e-01 -2.714948e-03)
			(-4.284697e-02 2.067803e-01)
		);
		scatteringMatrixP3 2 2 (
			(9.628545e-02 -1.053377e-03)
			(-2.567890e-02 2.491186e-02)
		);
		scatteringMatrixP4 2 2 (
			(2.187250e-02 -5.705175e-04)
			(-1.086839e-02 2.354525e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.066938e-02 -1.788207e-04)
			(-6.693205e-03 -8.405311e-03)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.512184e-01 1.110907e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.280122e-04 1.177264e-03 1.124110e-03 2.521099e-03 1.034404e-03 4.332812e-04);
		lambda             nonuniform List<scalar> 6 (1.333615e-02 3.273789e-02 1.207825e-01 3.028063e-01 8.496039e-01 2.853382e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (7.915422e-03 2.376335e-02);
	}

	fuelElement_34  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.341 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (3.578814e-06 1.566984e-04);
		D                  nonuniform List<scalar> 2 (1.448350e-02 8.177701e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (8.767639e-01 2.255389e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.110284e-11 2.868127e-10);
		scatteringMatrixP0 2 2 (
			(2.431627e+01 7.200676e-02)
			(1.191540e+00 2.968801e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.984510e+00 -1.310332e-02)
			(-7.543616e-02 1.757408e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.275182e-01 -2.301280e-03)
			(-4.621187e-02 2.863663e-01)
		);
		scatteringMatrixP3 2 2 (
			(9.495029e-02 -6.891765e-04)
			(-2.730570e-02 1.116815e-01)
		);
		scatteringMatrixP4 2 2 (
			(2.330405e-02 -7.020413e-04)
			(-1.203034e-02 3.079379e-02)
		);
		scatteringMatrixP5 2 2 (
			(9.354729e-03 -2.976848e-04)
			(-1.224552e-02 2.653457e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.804353e-01 1.223097e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.280531e-04 1.177536e-03 1.124405e-03 2.521895e-03 1.034873e-03 4.334730e-04);
		lambda             nonuniform List<scalar> 6 (1.333617e-02 3.273769e-02 1.207829e-01 3.028110e-01 8.496245e-01 2.853452e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (6.406378e-03 2.443498e-02);
	}

	fuelElement_39  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.392 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (3.263162e-06 1.514284e-04);
		D                  nonuniform List<scalar> 2 (1.448995e-02 7.943557e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (9.468426e-01 2.484552e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.198560e-11 3.159548e-10);
		scatteringMatrixP0 2 2 (
			(2.429460e+01 6.321056e-02)
			(1.349962e+00 2.958220e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.001931e+00 -1.177116e-02)
			(-9.540259e-02 1.839271e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.453011e-01 -2.095284e-03)
			(-6.784503e-02 1.908180e-01)
		);
		scatteringMatrixP3 2 2 (
			(9.765995e-02 -9.531408e-04)
			(-2.872935e-02 9.588064e-02)
		);
		scatteringMatrixP4 2 2 (
			(2.444276e-02 -2.015141e-04)
			(-3.055801e-03 5.117912e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.702553e-02 -2.857091e-04)
			(-5.053484e-03 1.163926e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (7.105562e-01 1.349770e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.281074e-04 1.177890e-03 1.124785e-03 2.522911e-03 1.035463e-03 4.337141e-04);
		lambda             nonuniform List<scalar> 6 (1.333621e-02 3.273745e-02 1.207834e-01 3.028168e-01 8.496494e-01 2.853535e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (4.962042e-03 2.526782e-02);
	}

	fuelElement_45  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.45 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (2.372028e-06 1.427308e-04);
		D                  nonuniform List<scalar> 2 (1.453343e-02 7.753986e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (9.235691e-01 2.639206e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.168063e-11 3.356218e-10);
		scatteringMatrixP0 2 2 (
			(2.429909e+01 3.641467e-02)
			(1.666184e+00 2.930044e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.025949e+00 -6.750834e-03)
			(-9.846079e-02 1.839323e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.519589e-01 -1.219966e-03)
			(-8.193705e-02 2.216703e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.020313e-01 -4.693182e-04)
			(-3.334596e-02 7.579407e-02)
		);
		scatteringMatrixP4 2 2 (
			(2.623937e-02 -2.450390e-04)
			(-1.522462e-02 2.788422e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.267571e-02 -8.021176e-05)
			(-6.219225e-03 1.910671e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.613274e-01 1.454199e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.283239e-04 1.179199e-03 1.126143e-03 2.526385e-03 1.037337e-03 4.344835e-04);
		lambda             nonuniform List<scalar> 6 (1.333629e-02 3.273683e-02 1.207848e-01 3.028317e-01 8.497138e-01 2.853751e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.428508e-02 4.179656e-01);
	}

	fuelElement  // OpenMC: Universe Fuel (UC2) e=93.000 %; rhoU = 0.45 g/cm3
	{
		fuelFraction       6.721789e-01;
		IV                 nonuniform List<scalar> 2 (2.372028e-06 1.427308e-04);
		D                  nonuniform List<scalar> 2 (1.453343e-02 7.753986e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (9.235691e-01 2.639206e+01);
		sigmaPow           nonuniform List<scalar> 2 (1.168063e-11 3.356218e-10);
		scatteringMatrixP0 2 2 (
			(2.429909e+01 3.641467e-02)
			(1.666184e+00 2.930044e+01)
		);
		scatteringMatrixP1 2 2 (
			(2.025949e+00 -6.750834e-03)
			(-9.846079e-02 1.839323e+00)
		);
		scatteringMatrixP2 2 2 (
			(4.519589e-01 -1.219966e-03)
			(-8.193705e-02 2.216703e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.020313e-01 -4.693182e-04)
			(-3.334596e-02 7.579407e-02)
		);
		scatteringMatrixP4 2 2 (
			(2.623937e-02 -2.450390e-04)
			(-1.522462e-02 2.788422e-02)
		);
		scatteringMatrixP5 2 2 (
			(1.267571e-02 -8.021176e-05)
			(-6.219225e-03 1.910671e-02)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (6.613274e-01 1.454199e+01);
		chiPrompt          nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (1.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (2.283239e-04 1.179199e-03 1.126143e-03 2.526385e-03 1.037337e-03 4.344835e-04);
		lambda             nonuniform List<scalar> 6 (1.333629e-02 3.273683e-02 1.207848e-01 3.028317e-01 8.497138e-01 2.853751e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.428508e-02 4.179656e-01);
	}

	unloadedElementsTypeC  // OpenMC: uCentralUnloadedFuelElement
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (3.559164e-06 1.751312e-04);
		D                  nonuniform List<scalar> 2 (1.058347e-02 6.088418e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(4.664672e+01 5.133130e-01)
			(1.279430e+00 7.617437e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.583988e+01 1.374833e-01)
			(3.957094e-01 1.881205e+01)
		);
		scatteringMatrixP2 2 2 (
			(6.232477e+00 -3.239255e-02)
			(1.210935e-01 6.013535e+00)
		);
		scatteringMatrixP3 2 2 (
			(3.348423e-01 -4.336464e-02)
			(1.950026e-03 2.017043e+00)
		);
		scatteringMatrixP4 2 2 (
			(-6.815853e-01 -1.812350e-02)
			(-4.705265e-02 4.388178e-01)
		);
		scatteringMatrixP5 2 2 (
			(-1.647600e-02 -3.734761e-03)
			(-4.542729e-02 1.824475e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (7.006660e-01 2.313598e+00);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028228e-01 8.496753e-01 2.853622e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (2.235409e-02 6.824635e-02);
	}

	unloadedElementsTypeT  // OpenMC: uCentralUnloadedFuelElementTa
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (2.480442e-06 1.598784e-04);
		D                  nonuniform List<scalar> 2 (1.252944e-02 7.620878e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(4.077717e+01 2.982373e-01)
			(1.498564e+00 6.844387e+01)
		);
		scatteringMatrixP1 2 2 (
			(1.523520e+01 8.198214e-02)
			(5.433276e-01 1.918378e+01)
		);
		scatteringMatrixP2 2 2 (
			(6.093224e+00 -2.217421e-02)
			(1.902012e-01 6.470496e+00)
		);
		scatteringMatrixP3 2 2 (
			(3.114057e-01 -2.624058e-02)
			(1.487136e-02 2.204855e+00)
		);
		scatteringMatrixP4 2 2 (
			(-6.498715e-01 -9.970918e-03)
			(-5.704538e-02 5.343943e-01)
		);
		scatteringMatrixP5 2 2 (
			(8.692714e-03 -2.089580e-03)
			(-6.907453e-02 2.007325e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (1.039455e+00 3.215509e+00);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028228e-01 8.496753e-01 2.853622e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.315818e-03 2.968102e-02);
	}

	coreSupport  // OpenMC: Cell cLowerCoreSupport (6061T6 Alloy)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (3.076678e-06 2.515145e-04);
		D                  nonuniform List<scalar> 2 (2.294270e-02 3.591012e-02);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(1.708408e+01 8.305317e-03)
			(3.515164e-02 8.376267e+00)
		);
		scatteringMatrixP1 2 2 (
			(2.584946e+00 -2.178593e-03)
			(-5.486756e-03 -9.856530e-04)
		);
		scatteringMatrixP2 2 2 (
			(7.953128e-01 -3.057946e-04)
			(-3.927076e-03 -1.948195e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.315853e-01 1.674829e-04)
			(1.766766e-03 -1.400037e-01)
		);
		scatteringMatrixP4 2 2 (
			(4.194050e-02 -1.500169e-04)
			(1.947785e-04 -1.252614e-01)
		);
		scatteringMatrixP5 2 2 (
			(6.002621e-03 1.205722e-04)
			(-1.977977e-03 -1.233234e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (2.925120e-02 8.582979e-01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028228e-01 8.496753e-01 2.853622e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (1.736418e-03 3.135866e-03);
	}

	supportPlate  // OpenMC: Cell cUpperCoreSupport (6061T6 Alloy)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (2.942756e-06 2.309128e-04);
		D                  nonuniform List<scalar> 2 (2.385576e-02 3.629358e-02);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(1.627450e+01 6.841985e-03)
			(5.021244e-02 8.359280e+00)
		);
		scatteringMatrixP1 2 2 (
			(2.325638e+00 -1.870272e-03)
			(-2.725083e-03 4.280151e-02)
		);
		scatteringMatrixP2 2 2 (
			(6.767115e-01 -2.363862e-04)
			(-3.768148e-03 -1.256324e-01)
		);
		scatteringMatrixP3 2 2 (
			(1.073392e-01 1.946156e-05)
			(-5.925280e-04 -9.691657e-02)
		);
		scatteringMatrixP4 2 2 (
			(2.615429e-02 -7.628128e-05)
			(-5.880917e-04 -1.378215e-01)
		);
		scatteringMatrixP5 2 2 (
			(1.041349e-02 8.916716e-05)
			(-1.276331e-03 -1.045797e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (2.520821e-02 8.059325e-01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028228e-01 8.496753e-01 2.853622e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (3.297870e-03 9.196373e-03);
	}

	reflector  // OpenMC: Reflector (Graphite)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (8.619720e-06 2.456472e-04);
		D                  nonuniform List<scalar> 2 (7.716706e-03 6.417460e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(4.595980e+01 3.157849e-01)
			(1.416340e-01 5.226317e+01)
		);
		scatteringMatrixP1 2 2 (
			(3.081922e+00 -8.499702e-02)
			(-5.580166e-03 8.707344e-01)
		);
		scatteringMatrixP2 2 2 (
			(2.594786e-01 -9.145790e-03)
			(-1.264317e-02 -9.120145e-01)
		);
		scatteringMatrixP3 2 2 (
			(3.612998e-02 -2.124196e-03)
			(-4.969153e-03 -7.472572e-01)
		);
		scatteringMatrixP4 2 2 (
			(-8.473514e-03 -9.858197e-04)
			(-2.584267e-03 -6.033498e-01)
		);
		scatteringMatrixP5 2 2 (
			(-3.616260e-03 -1.028032e-03)
			(-9.099573e-04 -4.907813e-01)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (3.172577e-01 1.642438e-01);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028228e-01 8.496753e-01 2.853622e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (1.993865e-01 7.551152e-02);
	}

	reflectorSystem  // OpenMC: Reflector System (Berrylium ring, control drums (102.300 deg) and tie rods)
	{
		fuelFraction       0.000000e+00;
		IV                 nonuniform List<scalar> 2 (9.086338e-06 2.817959e-04);
		D                  nonuniform List<scalar> 2 (5.135334e-03 4.235674e-03);
		nuSigmaEff         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		sigmaPow           nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		scatteringMatrixP0 2 2 (
			(6.863870e+01 5.904796e-01)
			(6.123022e-02 7.614035e+01)
		);
		scatteringMatrixP1 2 2 (
			(5.170406e+00 -1.599797e-01)
			(8.899331e-03 6.165841e-01)
		);
		scatteringMatrixP2 2 2 (
			(5.827490e-01 -1.747798e-02)
			(-4.180976e-03 -2.782076e+00)
		);
		scatteringMatrixP3 2 2 (
			(1.297769e-01 -2.631058e-03)
			(-3.865594e-03 -2.362997e+00)
		);
		scatteringMatrixP4 2 2 (
			(4.348852e-02 -1.825815e-03)
			(-2.284514e-03 -1.780911e+00)
		);
		scatteringMatrixP5 2 2 (
			(7.055763e-03 6.900215e-04)
			(-5.998842e-04 -1.233271e+00)
		);
		sigmaDisapp        nonuniform List<scalar> 2 (1.443992e+00 2.561823e+00);
		chiPrompt          nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		chiDelayed         nonuniform List<scalar> 2 (0.000000e+00 0.000000e+00);
		Beta               nonuniform List<scalar> 6 (0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00 0.000000e+00);
		lambda             nonuniform List<scalar> 6 (1.333624e-02 3.273720e-02 1.207840e-01 3.028228e-01 8.496753e-01 2.853622e+00);
		discFactor         nonuniform List<scalar> 2 (1.000000e+00 1.000000e+00);
		integralFlux       nonuniform List<scalar> 2 (2.130467e-01 6.808400e-02);
	}
*/);

// ************************************************************************* //
