/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant/fluidRegion";
    object      turbulenceProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

simulationType 	RAS;

RAS
{
    RASModel        porousKEpsilon; // laminar; // kEpsilon;

    turbulence      on;

    printCoeffs     on;
}

porousKEpsilonProperties
{
    "fuelElements:unloadedElements" //:coreSupport:supportPlate"
    {
        convergenceLength           0.1;// k and epsilon will exponentially
					 					// converge to equilibrium according
					 					// to this exponent
		// k and epsilon are determined based on correlations for turbulent
		// intensity (I) and lengh scale (L)
		// Turbulent intensity correlation in the form 0.16*Reynolds^-0.125
		// Reynolds number calculated by the thermal-hydraulic class, according
		// to the input data in phaseProperties
        turbulenceIntensityCoeff    0.16;
        turbulenceIntensityExp      -0.125;
        turbulenceLengthScaleCoeff  0.07;   // L = 0.07*Dh (Dh is the hydraulic
                                            // diameter secificied in
                                            // phaseProperties)
        DhStruct 					1; // 0.00254;
    }
    // unloadedElements //:coreSupport:supportPlate"
    // {
    //     convergenceLength           0.1;// k and epsilon will exponentially
	// 				 					// converge to equilibrium according
	// 				 					// to this exponent
	// 	// k and epsilon are determined based on correlations for turbulent
	// 	// intensity (I) and lengh scale (L)
	// 	// Turbulent intensity correlation in the form 0.16*Reynolds^-0.125
	// 	// Reynolds number calculated by the thermal-hydraulic class, according
	// 	// to the input data in phaseProperties
    //     turbulenceIntensityCoeff    0.16;
    //     turbulenceIntensityExp      -0.125;
    //     turbulenceLengthScaleCoeff  0.07;   // L = 0.07*Dh (Dh is the hydraulic
    //                                         // diameter secificied in
    //                                         // phaseProperties)
    //     DhStruct 					${{0.0095758/100}};
    // }
}

// ************************************************************************* //
