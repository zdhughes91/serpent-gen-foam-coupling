/*--------------------------------*- C++ -*----------------------------------*\
|       ______          _   __           ______                               |
|      / ____/  ___    / | / /          / ____/  ____   ____ _   ____ ___     |
|     / / __   / _ \  /  |/ /  ______  / /_     / __ \ / __ `/  / __ `__ \    |
|    / /_/ /  /  __/ / /|  /  /_____/ / __/    / /_/ // /_/ /  / / / / / /    |
|    \____/   \___/ /_/ |_/          /_/       \____/ \__,_/  /_/ /_/ /_/     |
|    Copyright (C) 2015 - 2022 EPFL                                           |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volScalarField;
    location    "0";
    object      defaultFlux;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// References : [1] GeN-Foam, https://foam-for-nuclear.gitlab.io/GeN-Foam/NEUTRONICS.html 
//              [2] Fiorina, Hursin, Pautz : Extension of the GeN-Foam neutronic solver 
//              to SP3 analysis and application to the CROCUS experimental reactor
//              https://www.sciencedirect.com/science/article/pii/S0306454916305126
//
// The albedo condition is set up according to the results of the OpenMC calculation using a full core model.
// This file is the first moment flux used in the equtions of SP3 and Diffusion [1][2],
// hence forSeconMoment flag is set to false 

dimensions      [ 0 -2 -1 0 0 0 0 ];

internalField   uniform 1;

boundaryField
{
    inlet
    {
        type    fixedValue;
        value   uniform 0;
    }
    outlet
    {
        type    fixedValue;
        value   uniform 0;
    }
    defaultFaces
    {
        /*
        type            albedoSP3;
        gamma           0.035; // defined as (1-alpha)/(1+alpha)/2,                                     
        diffCoeffName   Dalbedo;  // alpha being the albedo coefficient calculated with OpenMC
        fluxStarAlbedo  fluxStarAlbedo;
        forSecondMoment false;
        value           uniform 1;
        */
        type    fixedValue;
        value   uniform 0;
    }
}

// ************************************************************************* //
