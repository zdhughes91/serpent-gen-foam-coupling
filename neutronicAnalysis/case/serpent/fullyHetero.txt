/******************************************************************
********************* Core Support  Geometry **********************
*******************************************************************/

include materials.txt
include fuelElementMaterials.txt
include FuelChannel.txt
include FuelElement.txt
include UnloadedCentralFuelElement.txt
include core.txt 
include reflector.txt
include controlDrum.txt
include reflectorSystem.txt

% - surfaces
surf fullCore cylz 0 0 62.9 0       132.08
surf topSupp  cylz 0 0 42.6 137.95  149.38
surf botSupp  cylz 0 0 42.6 -3.81   0

% - cells
cell cFullCore 0 fill 40      -fullCore
cell cTopSupp  0 aluminum6061 -topSupp
cell cBotSupp  0 aluminum6061 -botSupp
cell voidedLast 0 outside fullCore topSupp botSupp

% - plotting
set pop 10000 60 10
%plot 3 5000 5000 1 -65 65 -65 65 % xy plot
%plot 2 5000 5000 2 -65 65 -5 155
