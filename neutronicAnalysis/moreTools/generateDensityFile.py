"""

"""

nominalDensity = 2 # what is nominal density defined as in type9.ifc

cellZoneOrder = ["unloadedElements","fuelElement_34", 'fuelElement_45', 'coreSupport',
                'fuelElement_22', 'supportPlate', 'fuelElement_19', 'fuelElement_13',
                'fuelElement_39', 'fuelElement_15', 'fuelElement_26', 'fuelElement_30', 
                'fuelElement_17' ] # i have checked and this appears to be the correct order ...


import numpy as np

celltoregion = np.genfromtxt('cellToRegion',skip_header=21,skip_footer=25223-25221)

densityList = [2.7,1.62651 ,1.70247,2.7,
               1.54497 ,2.7,1.52476,1.47807,
               1.66205,1.49201 ,1.56867,1.59584,
               1.50734]

nominalDensity = 2.7

with open('density','w') as file:
    for i,region in enumerate(celltoregion):
        if i == 0: file.write(f"{len(celltoregion)} "+'\n')

        file.write(f"{densityList[int(region)]/nominalDensity} "+'\n')