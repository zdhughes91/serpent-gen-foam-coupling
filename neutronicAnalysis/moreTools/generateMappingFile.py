"""
    This is a python script to generate a mapping file for the Serpent2 
    type 9 multi-physics interface. The goal is to set an identical bin to all
    non power producing cells, and then give each cell in the power producing 
    cells (the fuel region) its own bin in order to have a high fidelity power
    distribution.

    Author: Zach Hughes - zhughes@tamu.edu
"""
import numpy as np

czOrder = ["unloadedElements","fuelElement_34", 'fuelElement_45', 'coreSupport',
            'fuelElement_22', 'supportPlate', 'fuelElement_19', 'fuelElement_13',
            'fuelElement_39', 'fuelElement_15', 'fuelElement_26', 'fuelElement_30', 
            'fuelElement_17' ]

celltoregion = np.genfromtxt('cellToRegion',skip_header=21,skip_footer=25223-25221)

c = 1
with open('map','w') as file:
    for i,region in enumerate(celltoregion):
        if i == 0: file.write(f"{len(celltoregion)} "+'\n')

        if region == 2: # fuel occurs in index [2] in cellZoneOrder in generateMaterialsFile.py
            c+=1
            file.write(f"{c}"+'\n')
        else:
            file.write(f"{1}"+'\n')