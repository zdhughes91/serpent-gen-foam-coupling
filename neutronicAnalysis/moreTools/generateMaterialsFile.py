"""
    This is a script for generating a materials file from an OpenFOAM polyMesh folder. 
    The materials file relates each cell to a specific material. This is necessary for the
    Serpent2 coupling because Serpent needs to know whih material to put in each cell
    when reading the unstructured mesh.

    Author: Zach Hughes - zhughes@tamu.edu

    To prepare for this script:
        1. create the polyMesh folder with all cellZones representing different materials
        2. type: splitMeshRegions -overwrite -cellZones
        3. This code will need the 'cellToRegion' file as well as the order of cellZones, 
           which can be found at the top of the regions' polyMesh folder in the cellZones 
           file

Link to definition of materials file:
    https://serpent.vtt.fi/mediawiki/index.php/Unstructured_mesh_based_input#Material_file

"""
import numpy as np

# --- user entered information

cellZoneOrder = ["unloadedElements","fuelElement_34", 'fuelElement_45', 'coreSupport',
                'fuelElement_22', 'supportPlate', 'fuelElement_19', 'fuelElement_13',
                'fuelElement_39', 'fuelElement_15', 'fuelElement_26', 'fuelElement_30', 
                'fuelElement_17' ] # i have checked and this appears to be the correct order ...



# ---


celltoregion = np.genfromtxt('cellToRegion',skip_header=21,skip_footer=129767-129765)
print(celltoregion,len(celltoregion))


# with open('materials','w') as file:
#     for i,region in enumerate(celltoregion):
#         if i == 0: file.write(f"{len(celltoregion)} "+'\n')

#         if cellZoneOrder[int(region)] == "unloadedElements" or cellZoneOrder[int(region)] == "supportPlate":
#             file.write(f"aluminum6061"+'\n')
#         elif cellZoneOrder[int(region)] == "coreSupport":
#             file.write(f"aluminum6061"+'\n')
#         else:
#             file.write(f"type{cellZoneOrder[int(region)][-2:]}"+'\n')

