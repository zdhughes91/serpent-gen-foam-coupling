
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:46:03 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.23660E+00  7.44223E-01  7.33873E-01  7.57067E-01  1.25146E+00  9.49466E-01  1.24913E+00  1.07817E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.52556E-01 0.00164  2.43728E-01 0.00091 ];
DT_FRAC                   (idx, [1:   4]) = [  5.47444E-01 0.00135  7.56272E-01 0.00029 ];
DT_EFF                    (idx, [1:   4]) = [  2.59042E-01 0.00085  2.40876E-01 0.00072 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24049E-01 0.00079  2.91869E-01 0.00082 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15852E+01 0.00498  4.54779E+00 0.00096  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41577E-01 2.4E-05  5.72815E-02 0.00038  1.14176E-03 0.00295  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.28666E+01 0.00159  2.81540E+01 0.00349 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.26960E+01 0.00161  2.78173E+01 0.00354 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09922E+02 0.00162  6.74906E+01 0.00365 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07755E+02 0.00417  2.23060E+01 0.00402 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100381 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00381E+04 0.00660 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00381E+04 0.00660 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.46816E+00 ;
RUNNING_TIME              (idx, 1)        =  1.01915E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  5.75000E-03  5.75000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.62117E-01  7.62117E-01  0.00000E+00 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.01910E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 6.34662 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99619E+00 0.00014 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  7.61582E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.28192E+10 ;
TOT_DECAY_HEAT            (idx, 1)        =  9.47885E-03 ;
TOT_SF_RATE               (idx, 1)        =  8.19005E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.28192E+10 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  9.47885E-03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.08889E+05 ;
INGESTION_TOXICITY        (idx, 1)        =  6.02207E+02 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.08889E+05 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.02207E+02 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  1.63153E+10  3.33310E-04 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26653E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.22928E+10 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.89743E+15 0.00217  6.89743E+15 0.00217 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 144639 1.44643E+05 8.54089E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20070 2.00777E+04 1.02597E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 644327 6.43730E+05 2.83900E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 465067 4.64932E+05 5.39232E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1274103 1.27338E+06 5.86423E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1240305 1.23945E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33798 3.39296E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1274103 1.27338E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 4.42378E-09 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  2.34034E+19 0.00756 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  8.47931E+20 0.00232 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  6.92332E+18 0.00970 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  8.78258E+20 0.00226 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.92625E+21 0.00230 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  6.03056E+21 0.00222 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  5.57311E+07 0.00214 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36059E-10 0.00173 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
FIMA                      (idx, [1:   3]) = [  0.00000E+00  0.00000E+00  4.36181E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.53725E-02 0.02094 ];
U235_FISS                 (idx, [1:   4]) = [  2.96148E+19 0.00044  9.99142E-01 0.00013 ];
U238_FISS                 (idx, [1:   4]) = [  2.54422E+16 0.15564  8.58422E-04 0.15566 ];
U235_CAPT                 (idx, [1:   4]) = [  1.11479E+19 0.00768  4.00166E-01 0.00680 ];
U238_CAPT                 (idx, [1:   4]) = [  1.03428E+18 0.02027  3.71430E-02 0.02244 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100381 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.52050E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100381 1.00752E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40246 4.03900E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43016 4.29749E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17119 1.73871E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100381 1.00752E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.57569E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  9.37000E+08 0.0E+00  8.89945E+08 0.00022  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  5.49899E+00 5.6E-09  5.22284E+00 0.00022  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  7.24908E+19 0.00050  7.24908E+19 0.00050  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  2.96403E+19 0.00041  2.96403E+19 0.00041  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.78596E+19 0.00486  1.22448E+19 0.00695  1.56148E+19 0.00825 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.74999E+19 0.00233  4.18851E+19 0.00200  1.56148E+19 0.00825 ];
TOT_SRCRATE               (idx, [1:   6]) = [  6.89743E+19 0.00217  6.89743E+19 0.00217  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.00252E+22 0.00212  4.02612E+21 0.00203  5.99910E+21 0.00263 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  3.22139E+20 0.00225  3.20723E+20 0.00186 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.19931E+19 0.00872 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.94930E+19 0.00218 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.66294E+21 0.00313 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70395E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06791E+00 0.00278 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.38080E-01 0.00576 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73073E-01 0.00534 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53275E+00 0.00714 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.45016E-01 0.00176 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77650E-01 0.00030 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27226E+00 0.00265 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05103E+00 0.00239 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44568E+00 0.00016 ];
FISSE                     (idx, [1:   2]) = [  1.97309E+02 0.00041 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04840E+00 0.00526  1.04319E+00 0.00242  7.84234E-03 0.02489 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05103E+00 0.00239 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33659E+01 0.00194 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14463E-05 0.02537 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.72104E-02 0.03342 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.26626E-03 0.03460  2.27341E-04 0.21607  1.12955E-03 0.04618  9.66002E-04 0.08733  2.40837E-03 0.05170  1.06045E-03 0.11825  4.74550E-04 0.10421 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.99201E-01 0.03740  1.33360E-02 0.0E+00  3.27282E-02 0.00033  1.20780E-01 0.0E+00  3.02858E-01 0.00026  8.49961E-01 0.00055  2.85580E+00 0.00098 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.00671E-05 0.02465  3.00365E-05 0.02465  3.44318E-05 0.19736 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.15106E-05 0.02361  3.14788E-05 0.02364  3.60639E-05 0.19729 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.15243E-03 0.05011  2.36100E-04 0.25930  1.57105E-03 0.13283  1.20995E-03 0.11746  2.44976E-03 0.09584  1.19260E-03 0.16753  4.92972E-04 0.16012 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.77139E-01 0.09892  1.33360E-02 0.0E+00  3.27066E-02 0.00099  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.73042E-05 0.05305 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.86730E-05 0.05581 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.40834E-03 0.08466 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.95212E+02 0.04309 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.63657E-07 0.00707 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60289E-05 0.00265  1.60304E-05 0.00274  1.55336E-05 0.04503 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.32633E-04 0.00727  1.32404E-04 0.00763  1.76830E-04 0.13955 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44743E-01 0.00569  2.44785E-01 0.00561  2.43454E-01 0.10479 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.17005E+01 0.12207 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.78308E+00 0.00163  5.17463E+01 0.00372 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:47:28 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21220E+00  7.71636E-01  1.22217E+00  1.21294E+00  7.67741E-01  8.08878E-01  1.22730E+00  7.77132E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51310E-01 0.00159  2.44534E-01 0.00206 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48690E-01 0.00131  7.55466E-01 0.00067 ];
DT_EFF                    (idx, [1:   4]) = [  2.59694E-01 0.00079  2.40845E-01 0.00086 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.25081E-01 0.00088  2.91904E-01 0.00105 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14197E+01 0.00346  4.54938E+00 0.00115  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41590E-01 2.8E-05  5.72597E-02 0.00043  1.15018E-03 0.00297  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.31175E+01 0.00225  2.80508E+01 0.00378 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29457E+01 0.00228  2.77154E+01 0.00380 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09924E+02 0.00219  6.72316E+01 0.00371 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07124E+02 0.00323  2.23356E+01 0.00491 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99798 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.97980E+03 0.00712 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.97980E+03 0.00712 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.77169E+01 ;
RUNNING_TIME              (idx, 1)        =  2.42798E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.11833E-02  7.41667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.15482E+00  6.90133E-01  7.02567E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  3.33309E-05  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.42793E+00  4.53349E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.29695 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99941E+00 0.00014 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.96560E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  2.49895E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.84272E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19045E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.49895E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.84272E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  6.36855E+06 ;
INGESTION_TOXICITY        (idx, 1)        =  6.95494E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.36855E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.95494E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  1.78548E+17  2.04173E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26712E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  5.24956E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.91549E+15 0.00416  6.91549E+15 0.00416 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142882 1.43623E+05 8.63471E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20008 2.01179E+04 1.02802E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 636457 6.39691E+05 2.81455E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 459488 4.62140E+05 5.36650E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1258835 1.26557E+06 5.83711E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1225366 1.23176E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33469 3.38120E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1258835 1.26557E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -3.56231E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  2.33817E+19 0.00827 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  8.44784E+20 0.00252 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  6.95631E+18 0.00985 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  8.75122E+20 0.00258 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.92426E+21 0.00286 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  6.03672E+21 0.00294 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  5.57792E+07 0.00273 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.38915E-10 0.00073 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  3.81875E-02  3.92739E-02 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  6.94444E-03  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  4.07160E-05  1.77596E+22  4.36163E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.29259E-02 0.02600 ];
U235_FISS                 (idx, [1:   4]) = [  2.96141E+19 0.00035  9.99113E-01 0.00015 ];
U238_FISS                 (idx, [1:   4]) = [  2.63121E+16 0.16648  8.87478E-04 0.16633 ];
U235_CAPT                 (idx, [1:   4]) = [  1.10815E+19 0.00721  3.96995E-01 0.00382 ];
U238_CAPT                 (idx, [1:   4]) = [  9.33236E+17 0.02653  3.34399E-02 0.02650 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99798 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.61678E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99798 1.00762E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40017 4.03641E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42643 4.28680E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17138 1.75296E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99798 1.00762E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -3.66708E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  9.37000E+08 0.0E+00  8.89662E+08 0.00022  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  5.49899E+00 5.6E-09  5.22118E+00 0.00022  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  7.24989E+19 0.00041  7.24989E+19 0.00041  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  2.96404E+19 0.00039  2.96404E+19 0.00039  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.79168E+19 0.00798  1.20922E+19 0.00702  1.58245E+19 0.01018 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.75572E+19 0.00375  4.17327E+19 0.00191  1.58245E+19 0.01018 ];
TOT_SRCRATE               (idx, [1:   6]) = [  6.91549E+19 0.00416  6.91549E+19 0.00416  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.00774E+22 0.00402  4.02094E+21 0.00527  6.05643E+21 0.00378 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  3.20620E+20 0.00337  3.19627E+20 0.00284 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.21245E+19 0.01175 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.96817E+19 0.00420 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.69042E+21 0.00421 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70388E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70388E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07390E+00 0.00409 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.34026E-01 0.00525 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.75302E-01 0.00541 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.51334E+00 0.00384 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43661E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77530E-01 0.00062 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27140E+00 0.00397 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04853E+00 0.00442 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44595E+00 0.00012 ];
FISSE                     (idx, [1:   2]) = [  1.97308E+02 0.00039 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04872E+00 0.00457  1.04071E+00 0.00446  7.81800E-03 0.04367 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04853E+00 0.00442 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33533E+01 0.00136 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.17990E-05 0.01812 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.93594E-02 0.02654 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.70924E-03 0.03547  1.90695E-04 0.22479  1.29280E-03 0.07772  1.05161E-03 0.09832  2.49411E-03 0.08126  1.08805E-03 0.06174  5.91967E-04 0.08512 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  5.29647E-01 0.04895  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 6.8E-09  3.02880E-01 0.00033  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.94881E-05 0.01912  2.95108E-05 0.01869  2.70699E-05 0.20887 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.09175E-05 0.01835  3.09408E-05 0.01779  2.84432E-05 0.21139 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.59069E-03 0.05902  2.59350E-04 0.21589  1.52939E-03 0.12828  1.26453E-03 0.17918  2.85022E-03 0.08615  1.22840E-03 0.13001  4.58803E-04 0.21798 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.48279E-01 0.06943  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02887E-01 0.00035  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.59138E-05 0.05564 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.71770E-05 0.05593 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.78347E-03 0.07250 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.21312E+02 0.02362 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.65776E-07 0.00718 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60681E-05 0.00267  1.60632E-05 0.00260  1.68184E-05 0.02981 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.32970E-04 0.00794  1.32775E-04 0.00807  1.57084E-04 0.11658 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.46288E-01 0.00446  2.46215E-01 0.00420  2.60011E-01 0.06072 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09987E+01 0.09701 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.83265E+00 0.00115  5.19140E+01 0.00228 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:48:55 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.20910E+00  8.03495E-01  9.93088E-01  7.80641E-01  7.89218E-01  1.00827E+00  1.20495E+00  1.21123E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 3.9E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.49714E-01 0.00145  2.43745E-01 0.00142 ];
DT_FRAC                   (idx, [1:   4]) = [  5.50286E-01 0.00118  7.56255E-01 0.00046 ];
DT_EFF                    (idx, [1:   4]) = [  2.59010E-01 0.00071  2.40624E-01 0.00052 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.23837E-01 0.00124  2.91515E-01 0.00059 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14264E+01 0.00299  4.55506E+00 0.00098  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41501E-01 2.9E-05  5.73534E-02 0.00050  1.14529E-03 0.00270  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.28498E+01 0.00167  2.79654E+01 0.00296 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.26784E+01 0.00168  2.76285E+01 0.00299 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09991E+02 0.00138  6.71475E+01 0.00308 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06464E+02 0.00211  2.22036E+01 0.00292 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99825 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.98250E+03 0.00558 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.98250E+03 0.00558 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.93441E+01 ;
RUNNING_TIME              (idx, 1)        =  3.88388E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  3.71667E-02  7.80000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  3.59407E+00  6.99133E-01  7.40117E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  6.66618E-05  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  1.66655E-05 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  3.88382E+00  4.43686E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.55534 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99943E+00 0.00015 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.33327E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  4.34036E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  3.20019E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19092E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  4.34036E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  3.20019E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.14781E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.24727E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.14781E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.24727E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  3.10624E+17  3.55675E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26774E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.13653E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.91932E+15 0.00361  6.91932E+15 0.00361 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142921 1.43693E+05 8.64416E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 19824 1.99582E+04 1.01986E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 635890 6.38935E+05 2.81696E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 458892 4.61356E+05 5.36553E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1257527 1.26394E+06 5.83565E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1223895 1.23000E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33632 3.39426E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1257527 1.26394E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -1.34110E-07 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  2.34871E+19 0.00724 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  8.44152E+20 0.00428 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  6.90514E+18 0.01057 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  8.74544E+20 0.00430 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.91915E+21 0.00438 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  6.02740E+21 0.00452 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  5.56638E+07 0.00565 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37643E-10 0.00206 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  7.63749E-02  7.84173E-02 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.38889E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  8.13805E-05  3.54967E+22  4.36146E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.40813E-02 0.02922 ];
U235_FISS                 (idx, [1:   4]) = [  2.96160E+19 0.00067  9.99108E-01 0.00016 ];
U238_FISS                 (idx, [1:   4]) = [  2.64449E+16 0.18219  8.92376E-04 0.18261 ];
U235_CAPT                 (idx, [1:   4]) = [  1.10938E+19 0.01067  3.96202E-01 0.00642 ];
U238_CAPT                 (idx, [1:   4]) = [  9.80788E+17 0.02999  3.49872E-02 0.02392 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99825 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.66381E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99825 1.00766E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40092 4.04625E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42629 4.28456E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17104 1.74582E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99825 1.00766E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.92085E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  9.37000E+08 0.0E+00  8.90000E+08 0.00030  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  5.49899E+00 5.6E-09  5.22316E+00 0.00030  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  7.24965E+19 0.00068  7.24965E+19 0.00068  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  2.96424E+19 0.00061  2.96424E+19 0.00061  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.80013E+19 0.00886  1.21510E+19 0.01115  1.58503E+19 0.00931 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.76438E+19 0.00412  4.17934E+19 0.00314  1.58503E+19 0.00931 ];
TOT_SRCRATE               (idx, [1:   6]) = [  6.91932E+19 0.00361  6.91932E+19 0.00361  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.00523E+22 0.00373  4.03704E+21 0.00336  6.01527E+21 0.00433 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  3.20007E+20 0.00475  3.19272E+20 0.00421 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.20799E+19 0.00933 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.97237E+19 0.00368 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.67336E+21 0.00424 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70381E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70381E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06284E+00 0.00315 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.29515E-01 0.00398 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.71569E-01 0.00610 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.60118E+00 0.00573 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.45017E-01 0.00186 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76808E-01 0.00054 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26955E+00 0.00449 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04788E+00 0.00406 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44570E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97295E+02 0.00061 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05000E+00 0.00406  1.04073E+00 0.00417  7.14695E-03 0.03582 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04788E+00 0.00406 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32949E+01 0.00105 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.36899E-05 0.01402 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73215E-02 0.02356 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.28422E-03 0.03608  1.63381E-04 0.23133  1.18322E-03 0.10035  1.10120E-03 0.09347  2.35495E-03 0.04494  1.18591E-03 0.07929  2.95554E-04 0.12880 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.35210E-01 0.04226  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20800E-01 0.00016  3.02780E-01 5.6E-09  8.50431E-01 0.00111  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.87326E-05 0.02328  2.87472E-05 0.02435  2.61948E-05 0.27807 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.01656E-05 0.02321  3.01807E-05 0.02424  2.75353E-05 0.27843 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.79560E-03 0.03950  1.28494E-04 0.46223  1.33504E-03 0.14564  1.14325E-03 0.12282  2.87614E-03 0.06524  1.02324E-03 0.20043  2.89433E-04 0.21065 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  3.99388E-01 0.07118  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20812E-01 0.00026  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.57563E-05 0.04828 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.70588E-05 0.04959 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.10269E-03 0.07351 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.34016E+02 0.03350 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.63074E-07 0.00576 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60478E-05 0.00395  1.60460E-05 0.00381  1.64039E-05 0.03588 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33025E-04 0.00775  1.33035E-04 0.00746  1.28459E-04 0.11921 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44023E-01 0.00612  2.43962E-01 0.00650  2.56401E-01 0.09299 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  9.17742E+00 0.08762 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81045E+00 0.00188  5.12847E+01 0.00266 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:50:29 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.09122E+00  1.15397E+00  9.73605E-01  7.83687E-01  7.83260E-01  7.81552E-01  1.22265E+00  1.21006E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.49564E-01 0.00077  2.43607E-01 0.00105 ];
DT_FRAC                   (idx, [1:   4]) = [  5.50436E-01 0.00063  7.56393E-01 0.00034 ];
DT_EFF                    (idx, [1:   4]) = [  2.60238E-01 0.00062  2.40880E-01 0.00085 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.25146E-01 0.00080  2.91673E-01 0.00069 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.13923E+01 0.00293  4.55113E+00 0.00091  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41581E-01 1.8E-05  5.72710E-02 0.00028  1.14756E-03 0.00320  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.30181E+01 0.00123  2.80048E+01 0.00228 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.28462E+01 0.00122  2.76699E+01 0.00228 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09685E+02 0.00114  6.71974E+01 0.00291 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06227E+02 0.00184  2.22187E+01 0.00285 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100084 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00084E+04 0.00304 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00084E+04 0.00304 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.18559E+01 ;
RUNNING_TIME              (idx, 1)        =  5.45117E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  5.47000E-02  9.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.14315E+00  7.80333E-01  7.68750E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  1.16662E-04  3.33349E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.45108E+00  4.55753E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.67833 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99759E+00 0.00014 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.50781E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  5.68720E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  4.19273E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19154E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  5.68720E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  4.19273E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.57129E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.69023E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.57129E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.69023E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  4.07736E+17  4.67562E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26842E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19983E+18 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.94230E+15 0.00276  6.94230E+15 0.00276 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143410 1.43724E+05 8.48484E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20126 2.01935E+04 1.03188E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 639697 6.40888E+05 2.82024E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 461052 4.62263E+05 5.36164E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1264285 1.26707E+06 5.83170E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1230765 1.23332E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33520 3.37464E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1264285 1.26707E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -2.00234E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  2.34282E+19 0.00920 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  8.49151E+20 0.00118 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  7.00838E+18 0.01060 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  8.79588E+20 0.00122 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.92803E+21 0.00151 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  6.03266E+21 0.00260 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  5.58041E+07 0.00269 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.35307E-10 0.00166 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  1.14562E-01  1.17427E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  2.08333E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  1.22066E-04  5.32430E+22  4.36128E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.38113E-02 0.03205 ];
U235_FISS                 (idx, [1:   4]) = [  2.96130E+19 0.00026  9.99204E-01 0.00011 ];
U238_FISS                 (idx, [1:   4]) = [  2.35914E+16 0.14095  7.96085E-04 0.14096 ];
U235_CAPT                 (idx, [1:   4]) = [  1.11682E+19 0.00753  3.96885E-01 0.00743 ];
U238_CAPT                 (idx, [1:   4]) = [  9.70780E+17 0.03065  3.44947E-02 0.03012 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100084 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.58289E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100084 1.00758E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40277 4.05373E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42598 4.26929E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17209 1.75281E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100084 1.00758E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -5.71890E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  9.37000E+08 0.0E+00  8.89700E+08 0.00013  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  5.49899E+00 5.6E-09  5.22140E+00 0.00013  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  7.24893E+19 0.00022  7.24893E+19 0.00022  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  2.96366E+19 0.00020  2.96366E+19 0.00020  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.81435E+19 0.00564  1.22131E+19 0.00583  1.59304E+19 0.00841 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.77801E+19 0.00272  4.18497E+19 0.00171  1.59304E+19 0.00841 ];
TOT_SRCRATE               (idx, [1:   6]) = [  6.94230E+19 0.00276  6.94230E+19 0.00276  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.01037E+22 0.00291  4.03012E+21 0.00264  6.07358E+21 0.00362 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  3.20162E+20 0.00255  3.20956E+20 0.00171 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.21696E+19 0.00916 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.99497E+19 0.00290 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.69847E+21 0.00344 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70374E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70374E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07309E+00 0.00448 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.30318E-01 0.00433 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73518E-01 0.00379 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54415E+00 0.00669 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43897E-01 0.00138 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77272E-01 0.00050 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26619E+00 0.00259 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04424E+00 0.00285 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44594E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97334E+02 0.00020 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04424E+00 0.00312  1.03667E+00 0.00285  7.57115E-03 0.03505 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04424E+00 0.00285 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33484E+01 0.00122 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.19461E-05 0.01653 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.88680E-02 0.02363 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.71189E-03 0.02574  1.24448E-04 0.25893  1.16607E-03 0.07558  1.28209E-03 0.08880  2.63870E-03 0.07966  9.64948E-04 0.11634  5.35626E-04 0.14373 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.97258E-01 0.04517  1.33360E-02 5.7E-09  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02832E-01 0.00017  8.49490E-01 0.0E+00  2.85545E+00 0.00086 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.86514E-05 0.01634  2.86068E-05 0.01677  3.65152E-05 0.19771 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.99085E-05 0.01411  2.98615E-05 0.01450  3.81897E-05 0.19923 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.41805E-03 0.03529  2.57858E-04 0.26398  1.51015E-03 0.10687  1.29819E-03 0.06809  2.43960E-03 0.09211  1.29768E-03 0.16644  6.14580E-04 0.22636 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.08086E-01 0.09027  1.33360E-02 5.7E-09  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.66551E-05 0.04539 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.78578E-05 0.04723 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.66624E-03 0.08709 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.09620E+02 0.05555 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.62137E-07 0.00569 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60381E-05 0.00237  1.60381E-05 0.00225  1.61273E-05 0.03084 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.31666E-04 0.00586  1.31669E-04 0.00604  1.33106E-04 0.13608 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45141E-01 0.00319  2.44998E-01 0.00305  2.67545E-01 0.05925 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  8.24220E+00 0.06755 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81156E+00 0.00134  5.15905E+01 0.00172 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:51:52 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21182E+00  1.21641E+00  7.61592E-01  1.23088E+00  7.71896E-01  8.19572E-01  7.68266E-01  1.21956E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.49677E-01 0.00203  2.44043E-01 0.00162 ];
DT_FRAC                   (idx, [1:   4]) = [  5.50323E-01 0.00166  7.55957E-01 0.00052 ];
DT_EFF                    (idx, [1:   4]) = [  2.59647E-01 0.00064  2.40540E-01 0.00067 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24301E-01 0.00115  2.91576E-01 0.00069 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.13931E+01 0.00318  4.55166E+00 0.00080  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41532E-01 2.8E-05  5.73100E-02 0.00041  1.15803E-03 0.00393  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.26007E+01 0.00152  2.79496E+01 0.00235 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.24262E+01 0.00154  2.76101E+01 0.00240 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09234E+02 0.00181  6.70838E+01 0.00300 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.05838E+02 0.00282  2.22134E+01 0.00227 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99858 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.98580E+03 0.00490 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.98580E+03 0.00490 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.29692E+01 ;
RUNNING_TIME              (idx, 1)        =  6.84343E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.25000E-02  7.51667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  6.51672E+00  6.90167E-01  6.83400E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  1.83328E-04  3.33349E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.84338E+00  4.52931E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.74015 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99887E+00 0.00013 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.59640E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  6.71909E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  4.95287E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19242E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  6.71909E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  4.95287E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.94233E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.06645E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.94233E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  2.06645E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  4.82620E+17  5.54309E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26916E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.42085E+18 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.94776E+15 0.00282  6.94776E+15 0.00282 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142748 1.43530E+05 8.49966E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20098 2.02363E+04 1.03407E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 634558 6.37389E+05 2.80711E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 458122 4.60485E+05 5.34974E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1255526 1.26164E+06 5.81886E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1221627 1.22742E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33899 3.42180E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1255526 1.26164E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -6.56582E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  2.37737E+19 0.00631 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  8.45702E+20 0.00191 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  7.02956E+18 0.01170 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  8.76505E+20 0.00173 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.92607E+21 0.00170 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  6.04622E+21 0.00210 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  5.58241E+07 0.00233 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.38151E-10 0.00199 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  1.52750E-01  1.56452E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  2.77778E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  1.62761E-04  7.09931E+22  4.36110E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.41752E-02 0.02024 ];
U235_FISS                 (idx, [1:   4]) = [  2.96314E+19 0.00026  9.99409E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  1.75238E+16 0.29744  5.90796E-04 0.29739 ];
U235_CAPT                 (idx, [1:   4]) = [  1.10427E+19 0.00785  3.94464E-01 0.00585 ];
U238_CAPT                 (idx, [1:   4]) = [  9.83521E+17 0.01993  3.51396E-02 0.02041 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99858 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.96588E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99858 1.00797E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 39955 4.02904E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42475 4.26773E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17428 1.78289E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99858 1.00797E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.20550E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  9.37000E+08 0.0E+00  8.89879E+08 0.00018  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  5.49899E+00 7.9E-09  5.22245E+00 0.00018  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  7.25078E+19 0.00036  7.25078E+19 0.00036  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  2.96489E+19 0.00030  2.96489E+19 0.00030  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.79935E+19 0.00459  1.21074E+19 0.00685  1.58861E+19 0.00588 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.76424E+19 0.00215  4.17564E+19 0.00197  1.58861E+19 0.00588 ];
TOT_SRCRATE               (idx, [1:   6]) = [  6.94776E+19 0.00282  6.94776E+19 0.00282  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.00395E+22 0.00292  4.01962E+21 0.00296  6.01986E+21 0.00351 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  3.18278E+20 0.00234  3.19975E+20 0.00165 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.23884E+19 0.00969 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  7.00308E+19 0.00268 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.67114E+21 0.00253 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70367E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70367E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07630E+00 0.00306 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.36687E-01 0.00398 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.70693E-01 0.00455 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.55039E+00 0.00446 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.40920E-01 0.00161 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77157E-01 0.00056 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27015E+00 0.00260 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04369E+00 0.00296 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44554E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97252E+02 0.00030 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04301E+00 0.00325  1.03639E+00 0.00304  7.29997E-03 0.04507 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04369E+00 0.00296 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33435E+01 0.00129 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.21075E-05 0.01719 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.55719E-02 0.02378 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.37133E-03 0.02591  2.19935E-04 0.22931  1.19073E-03 0.05824  9.89176E-04 0.06390  2.42034E-03 0.07725  1.01666E-03 0.10798  5.34496E-04 0.18840 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  5.19093E-01 0.08578  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 5.6E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.98088E-05 0.01614  2.97745E-05 0.01591  3.55289E-05 0.26686 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.10893E-05 0.01623  3.10535E-05 0.01597  3.70629E-05 0.26667 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.18095E-03 0.04868  2.77425E-04 0.24363  1.38927E-03 0.11477  9.15675E-04 0.15431  2.89449E-03 0.11722  1.06424E-03 0.12139  6.39851E-04 0.28484 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.26180E-01 0.12320  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.65199E-05 0.04973 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.76452E-05 0.04873 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.40439E-03 0.07032 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.39668E+02 0.03371 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.62991E-07 0.00801 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60927E-05 0.00157  1.60839E-05 0.00154  1.73282E-05 0.04821 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.32904E-04 0.00784  1.32728E-04 0.00755  1.63600E-04 0.12672 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.41897E-01 0.00508  2.41774E-01 0.00517  2.64013E-01 0.07339 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  9.95989E+00 0.08106 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80303E+00 0.00181  5.14541E+01 0.00261 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:53:21 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.26491E+00  1.25895E+00  1.26284E+00  7.37204E-01  7.36725E-01  7.37737E-01  1.26896E+00  7.32677E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.49848E-01 0.00217  2.44005E-01 0.00126 ];
DT_FRAC                   (idx, [1:   4]) = [  5.50152E-01 0.00178  7.55995E-01 0.00041 ];
DT_EFF                    (idx, [1:   4]) = [  2.59735E-01 0.00089  2.40623E-01 0.00072 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.25306E-01 0.00162  2.91543E-01 0.00070 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.13378E+01 0.00540  4.55565E+00 0.00122  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41570E-01 2.9E-05  5.72752E-02 0.00050  1.15469E-03 0.00243  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.31917E+01 0.00234  2.80582E+01 0.00281 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.30182E+01 0.00236  2.77183E+01 0.00282 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09961E+02 0.00164  6.73563E+01 0.00275 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06410E+02 0.00341  2.23185E+01 0.00364 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99847 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.98470E+03 0.00491 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.98470E+03 0.00491 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.47496E+01 ;
RUNNING_TIME              (idx, 1)        =  8.31875E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.84333E-02  7.83333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.97540E+00  7.34517E-01  7.24167E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  2.16663E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  8.31870E+00  4.40695E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.78357 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99891E+00 9.2E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.65822E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.49575E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  5.52471E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19366E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  7.49575E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  5.52471E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  2.26951E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.38751E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.26951E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  2.38751E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  5.39476E+17  6.20665E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26998E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.58902E+18 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  6.93032E+15 0.00286  6.93032E+15 0.00286 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142857 1.43606E+05 8.54882E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20404 2.05247E+04 1.04881E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 638725 6.41674E+05 2.82657E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460052 4.62482E+05 5.39945E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1262038 1.26829E+06 5.87248E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1228100 1.23404E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33938 3.42508E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1262038 1.26829E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -1.22935E-07 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  2.37366E+19 0.00767 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  8.48119E+20 0.00395 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  7.11195E+18 0.01040 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  8.78968E+20 0.00388 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  1.92846E+21 0.00422 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  6.04352E+21 0.00440 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  5.60726E+07 0.00442 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37367E-10 0.00164 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  1.90937E-01  1.95592E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  3.47222E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.03615E-04  8.88132E+22  4.36092E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37848E-02 0.03408 ];
U235_FISS                 (idx, [1:   4]) = [  2.96049E+19 0.00053  9.99184E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  2.41914E+16 0.20360  8.16497E-04 0.20392 ];
U235_CAPT                 (idx, [1:   4]) = [  1.10348E+19 0.00692  3.95048E-01 0.00283 ];
U238_CAPT                 (idx, [1:   4]) = [  9.66872E+17 0.03477  3.46093E-02 0.03366 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99847 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.26935E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99847 1.00727E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 39965 4.03032E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42554 4.27565E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17328 1.76672E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99847 1.00727E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -5.41331E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  9.37000E+08 0.0E+00  8.89619E+08 0.00024  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  5.49899E+00 5.6E-09  5.22093E+00 0.00024  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  7.24695E+19 0.00051  7.24695E+19 0.00051  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  2.96291E+19 0.00050  2.96291E+19 0.00050  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.79341E+19 0.00702  1.20738E+19 0.00733  1.58603E+19 0.00822 ];
TOT_ABSRATE               (idx, [1:   6]) = [  5.75633E+19 0.00321  4.17030E+19 0.00199  1.58603E+19 0.00822 ];
TOT_SRCRATE               (idx, [1:   6]) = [  6.93032E+19 0.00286  6.93032E+19 0.00286  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.01044E+22 0.00387  4.03126E+21 0.00369  6.07314E+21 0.00478 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  3.21088E+20 0.00481  3.20565E+20 0.00401 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.22434E+19 0.00455 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  6.98067E+19 0.00269 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.70054E+21 0.00420 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70360E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70360E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07019E+00 0.00324 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.30717E-01 0.00483 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.74397E-01 0.00535 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54706E+00 0.00789 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43216E-01 0.00096 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76415E-01 0.00062 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27021E+00 0.00383 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04578E+00 0.00334 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44589E+00 0.00012 ];
FISSE                     (idx, [1:   2]) = [  1.97384E+02 0.00050 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04628E+00 0.00284  1.03894E+00 0.00315  6.83608E-03 0.06495 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04578E+00 0.00334 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33627E+01 0.00148 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.15092E-05 0.02005 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.82062E-02 0.02553 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.05830E-03 0.05073  2.01188E-04 0.22862  8.70201E-04 0.11299  1.21518E-03 0.08057  2.42339E-03 0.06516  9.46716E-04 0.08349  4.01617E-04 0.14105 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68741E-01 0.05478  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.97994E-05 0.01516  2.98337E-05 0.01560  2.52375E-05 0.22874 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.11783E-05 0.01532  3.12141E-05 0.01575  2.64078E-05 0.22839 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.66650E-03 0.05770  2.48052E-04 0.37767  1.00566E-03 0.14594  1.31234E-03 0.13728  2.66165E-03 0.07346  9.93304E-04 0.11039  4.45499E-04 0.24566 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.63575E-01 0.08286  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.62222E-05 0.05478 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.74202E-05 0.05389 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.25130E-03 0.09553 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.33758E+02 0.05200 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.66234E-07 0.01013 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60152E-05 0.00277  1.60194E-05 0.00262  1.51789E-05 0.04765 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33573E-04 0.00823  1.33586E-04 0.00845  1.16283E-04 0.14684 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.46154E-01 0.00528  2.46121E-01 0.00531  2.60594E-01 0.13297 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02628E+01 0.08365 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.82658E+00 0.00103  5.17056E+01 0.00282 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:54:48 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.20850E+00  1.21890E+00  1.20696E+00  7.80741E-01  7.68481E-01  7.72533E-01  8.33086E-01  1.21080E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50644E-01 0.00227  2.44363E-01 0.00178 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49356E-01 0.00186  7.55637E-01 0.00057 ];
DT_EFF                    (idx, [1:   4]) = [  2.59273E-01 0.00104  2.40672E-01 0.00077 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24270E-01 0.00094  2.91744E-01 0.00092 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14670E+01 0.00474  4.55314E+00 0.00090  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41533E-01 2.3E-05  5.73200E-02 0.00042  1.14739E-03 0.00321  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29584E+01 0.00190  2.81924E+01 0.00274 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.27875E+01 0.00192  2.78583E+01 0.00280 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10001E+02 0.00138  6.76308E+01 0.00280 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06932E+02 0.00509  2.24338E+01 0.00319 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100101 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00101E+04 0.00341 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00101E+04 0.00341 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.62849E+01 ;
RUNNING_TIME              (idx, 1)        =  9.76320E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.04750E-01  8.28333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.40283E+00  7.13067E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  2.66663E-04  3.33349E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  9.76315E+00  4.49356E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.81352 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99899E+00 0.00014 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70085E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.08471E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  5.95806E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19533E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.08471E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  5.95806E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  2.56461E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.66814E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.56461E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  2.66814E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  5.83076E+17  6.72034E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27089E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.71832E+18 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143536 1.43875E+05 8.44154E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20134 2.01934E+04 1.03188E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 642556 6.43745E+05 2.83736E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 463528 4.64685E+05 5.37259E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1269754 1.27250E+06 5.84393E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1236309 1.23883E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33445 3.36651E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1269754 1.27250E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 2.49129E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37903E-10 0.00197 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  4.16667E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.25650E-02 0.02066 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100101 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.67679E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100101 1.00768E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40151 4.04028E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42841 4.29273E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17109 1.74376E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100101 1.00768E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -4.14730E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06051E+00 0.00315 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.32896E-01 0.00499 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72277E-01 0.00606 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.58341E+00 0.00830 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44964E-01 0.00198 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77110E-01 0.00055 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27164E+00 0.00356 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04985E+00 0.00238 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44563E+00 9.0E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97294E+02 0.00039 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04869E+00 0.00261  1.04249E+00 0.00256  7.35726E-03 0.05710 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04985E+00 0.00238 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33436E+01 0.00230 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.21985E-05 0.03064 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.64867E-02 0.01945 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.36723E-03 0.05868  2.09498E-04 0.21237  1.24712E-03 0.08790  1.06542E-03 0.11821  2.48407E-03 0.06990  9.42187E-04 0.11642  4.18926E-04 0.15953 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.50317E-01 0.06539  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02845E-01 0.00022  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.86701E-05 0.02019  2.86613E-05 0.02067  3.21059E-05 0.17484 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.00628E-05 0.01977  3.00532E-05 0.02021  3.37220E-05 0.17613 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.90445E-03 0.05656  2.07674E-04 0.25901  1.11329E-03 0.21533  1.20956E-03 0.11271  2.78566E-03 0.07143  1.17078E-03 0.10771  4.17491E-04 0.21082 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.60738E-01 0.06290  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.49150E-05 0.04975 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.61336E-05 0.04999 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.47705E-03 0.06690 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.17991E+02 0.02347 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.65753E-07 0.00871 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60141E-05 0.00456  1.60132E-05 0.00456  1.62532E-05 0.03975 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33642E-04 0.00852  1.33611E-04 0.00834  1.42656E-04 0.06770 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44370E-01 0.00566  2.44302E-01 0.00541  2.65768E-01 0.15268 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.22470E+01 0.10902 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80290E+00 0.00123  5.14633E+01 0.00335 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:55:35 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21894E+00  7.75506E-01  9.02263E-01  1.22246E+00  7.69488E-01  7.69222E-01  1.11945E+00  1.22267E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.9E-09  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51667E-01 0.00183  2.43827E-01 0.00176 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48333E-01 0.00151  7.56173E-01 0.00057 ];
DT_EFF                    (idx, [1:   4]) = [  2.59225E-01 0.00082  2.40573E-01 0.00053 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24389E-01 0.00117  2.91505E-01 0.00065 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15282E+01 0.00521  4.55166E+00 0.00076  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41542E-01 3.0E-05  5.73159E-02 0.00048  1.14175E-03 0.00352  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.28698E+01 0.00228  2.79397E+01 0.00513 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.26982E+01 0.00229  2.76080E+01 0.00511 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09754E+02 0.00130  6.71011E+01 0.00529 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07131E+02 0.00358  2.21942E+01 0.00482 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99964 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99640E+03 0.00457 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99640E+03 0.00457 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  8.25639E+01 ;
RUNNING_TIME              (idx, 1)        =  1.05497E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.13333E-01  8.58333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.01802E+01  7.77333E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  2.83333E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.05497E+01  4.44992E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.82619 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99863E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.71928E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  6.04554E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  4.45236E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19637E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  6.04554E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  4.45236E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  2.21247E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.22629E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.21247E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  2.22629E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  4.38978E+17  5.08323E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27129E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29582E+18 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142205 1.42615E+05 8.36818E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 19912 1.99750E+04 1.02072E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 638430 6.40122E+05 2.81834E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 459168 4.60661E+05 5.34491E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1259715 1.26337E+06 5.81250E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1226551 1.22998E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33164 3.33927E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1259715 1.26337E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -5.93718E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37207E-10 0.00169 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  4.86111E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.40110E-02 0.02855 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99964 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.25314E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99964 1.00725E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40121 4.04102E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42691 4.28176E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17152 1.74975E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99964 1.00725E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -5.00586E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06584E+00 0.00315 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.35555E-01 0.00416 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72615E-01 0.00675 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54816E+00 0.00688 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44547E-01 0.00164 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76884E-01 0.00037 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26940E+00 0.00376 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04729E+00 0.00421 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44594E+00 0.00012 ];
FISSE                     (idx, [1:   2]) = [  1.97220E+02 0.00035 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04758E+00 0.00455  1.03971E+00 0.00410  7.58310E-03 0.04841 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04729E+00 0.00421 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33363E+01 0.00180 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.23807E-05 0.02390 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.90953E-02 0.02413 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.11692E-03 0.02669  2.20649E-04 0.18501  1.06166E-03 0.06051  1.12793E-03 0.10005  2.24296E-03 0.04972  1.01324E-03 0.07757  4.50486E-04 0.10413 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.89115E-01 0.03965  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02875E-01 0.00031  8.49490E-01 0.0E+00  2.85691E+00 0.00137 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.89608E-05 0.01360  2.88401E-05 0.01266  4.48792E-05 0.19396 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.03306E-05 0.01196  3.02056E-05 0.01139  4.68166E-05 0.19065 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.11041E-03 0.05139  2.37304E-04 0.36757  1.23789E-03 0.11481  1.27122E-03 0.07363  2.82932E-03 0.10157  9.90685E-04 0.14120  5.44000E-04 0.24306 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.78528E-01 0.09413  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 5.6E-09  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.50271E-05 0.05391 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.61735E-05 0.05119 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.88602E-03 0.08478 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.31481E+02 0.03851 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.64799E-07 0.00954 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60096E-05 0.00330  1.60113E-05 0.00335  1.57517E-05 0.02529 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33072E-04 0.01132  1.32953E-04 0.01130  1.53857E-04 0.12150 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44732E-01 0.00557  2.44586E-01 0.00529  2.72801E-01 0.08722 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05668E+01 0.07506 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80928E+00 0.00214  5.15702E+01 0.00280 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:56:18 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21160E+00  7.77810E-01  7.65230E-01  7.85912E-01  1.19630E+00  8.37563E-01  1.22082E+00  1.20477E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.9E-09  5.00000E-02 6.8E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51272E-01 0.00142  2.43426E-01 0.00165 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48728E-01 0.00117  7.56574E-01 0.00053 ];
DT_EFF                    (idx, [1:   4]) = [  2.59444E-01 0.00120  2.40887E-01 0.00074 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24197E-01 0.00145  2.91669E-01 0.00103 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15468E+01 0.00314  4.54513E+00 0.00086  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41543E-01 2.3E-05  5.73129E-02 0.00035  1.14371E-03 0.00251  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.27811E+01 0.00180  2.80913E+01 0.00469 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.26111E+01 0.00181  2.77595E+01 0.00471 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09671E+02 0.00209  6.74140E+01 0.00422 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06978E+02 0.00307  2.22616E+01 0.00454 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100106 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00106E+04 0.00962 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00106E+04 0.00962 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  8.82914E+01 ;
RUNNING_TIME              (idx, 1)        =  1.12672E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.22050E-01  8.71667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.08884E+01  7.08183E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  2.99998E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.12672E+01  4.59150E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.83612 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99857E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73361E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  4.52839E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  3.33232E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19688E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  4.52839E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  3.33232E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.94985E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.89703E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.94985E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.89703E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  3.31765E+17  3.86611E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27175E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.81476E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 144022 1.44401E+05 8.50565E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 19974 2.00349E+04 1.02378E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 641184 6.42414E+05 2.83142E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 463015 4.64222E+05 5.39143E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1268195 1.27107E+06 5.86201E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1234984 1.23764E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33211 3.34289E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1268195 1.27107E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -7.14790E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36664E-10 0.00149 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  5.55556E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.30172E-02 0.02778 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100106 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.46731E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100106 1.00747E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40265 4.05031E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42827 4.29180E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17014 1.73257E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100106 1.00747E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -5.63159E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06659E+00 0.00383 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.32645E-01 0.00479 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72272E-01 0.00516 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.56881E+00 0.00774 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.45878E-01 0.00150 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77379E-01 0.00060 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26978E+00 0.00391 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04979E+00 0.00452 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44604E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97305E+02 0.00039 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04553E+00 0.00650  1.04271E+00 0.00446  7.07782E-03 0.05304 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04979E+00 0.00452 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33129E+01 0.00190 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.31573E-05 0.02549 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.00091E-01 0.02458 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.28754E-03 0.05566  1.43368E-04 0.24885  1.09075E-03 0.07828  1.02811E-03 0.07132  2.64426E-03 0.09135  9.30688E-04 0.09501  4.50368E-04 0.15716 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.75197E-01 0.04671  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 5.6E-09  8.50196E-01 0.00083  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.88878E-05 0.01844  2.88574E-05 0.01842  3.43295E-05 0.21051 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.01763E-05 0.01334  3.01453E-05 0.01345  3.57488E-05 0.21092 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.85337E-03 0.04458  5.94433E-05 0.50962  1.20334E-03 0.15216  1.09927E-03 0.10843  2.95984E-03 0.06301  1.08298E-03 0.13606  4.48495E-04 0.22813 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.68234E-01 0.08503  1.33360E-02 1.3E-08  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.57910E-05 0.04970 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.69864E-05 0.05114 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.56021E-03 0.06722 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.14547E+02 0.03511 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.61181E-07 0.00583 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59946E-05 0.00142  1.59880E-05 0.00153  1.68219E-05 0.04198 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.31211E-04 0.00504  1.31212E-04 0.00562  1.30554E-04 0.12714 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44467E-01 0.00504  2.44264E-01 0.00475  2.85548E-01 0.08350 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.14377E+01 0.15198 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.78398E+00 0.00175  5.14066E+01 0.00291 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:57:03 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.19668E+00  7.87165E-01  1.20542E+00  7.77414E-01  1.21234E+00  1.22380E+00  7.77414E-01  8.19772E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51268E-01 0.00205  2.43792E-01 0.00216 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48732E-01 0.00169  7.56208E-01 0.00070 ];
DT_EFF                    (idx, [1:   4]) = [  2.59350E-01 0.00106  2.40652E-01 0.00067 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24914E-01 0.00084  2.91386E-01 0.00104 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14110E+01 0.00427  4.55193E+00 0.00114  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41561E-01 2.4E-05  5.72949E-02 0.00039  1.14384E-03 0.00274  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.30994E+01 0.00130  2.81019E+01 0.00279 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29280E+01 0.00131  2.77680E+01 0.00279 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09971E+02 0.00142  6.75277E+01 0.00222 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07069E+02 0.00408  2.23565E+01 0.00398 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100017 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00017E+04 0.00803 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00017E+04 0.00803 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  9.43481E+01 ;
RUNNING_TIME              (idx, 1)        =  1.20261E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.30900E-01  8.85000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.16377E+01  7.49383E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  3.16664E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.20260E+01  4.44417E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.84530 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99885E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.74659E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  3.39951E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.49900E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19718E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  3.39951E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  2.49900E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.75384E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.65153E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.75384E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.65153E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.51985E+17  2.96073E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27226E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  7.47561E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 144088 1.44598E+05 8.41750E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 19820 1.98990E+04 1.01684E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 641267 6.43198E+05 2.83125E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 462311 4.63968E+05 5.36997E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1267486 1.27166E+06 5.83895E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1234085 1.23801E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33401 3.36578E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1267486 1.27166E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 3.77186E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37495E-10 0.00134 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  6.25000E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.26247E-02 0.01541 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100017 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.77936E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100017 1.00778E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40086 4.03640E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42797 4.29337E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17134 1.74803E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100017 1.00778E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.82307E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06263E+00 0.00367 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.35826E-01 0.00600 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73200E-01 0.00485 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.55232E+00 0.00502 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44648E-01 0.00134 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76971E-01 0.00033 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27233E+00 0.00455 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04993E+00 0.00500 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44548E+00 9.0E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97340E+02 0.00067 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05214E+00 0.00574  1.04237E+00 0.00477  7.56090E-03 0.06968 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04993E+00 0.00500 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33352E+01 0.00143 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.23856E-05 0.01920 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.52306E-02 0.01995 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.41140E-03 0.05090  2.98595E-04 0.19134  1.15705E-03 0.10230  9.05758E-04 0.07768  2.54063E-03 0.07351  1.08958E-03 0.12070  4.19778E-04 0.18322 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.67605E-01 0.05516  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.04124E-05 0.02586  3.04734E-05 0.02616  2.36210E-05 0.28152 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.20018E-05 0.02697  3.20667E-05 0.02734  2.47983E-05 0.27962 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.40375E-03 0.07038  2.54848E-04 0.37830  1.30822E-03 0.17860  9.89310E-04 0.17161  3.01755E-03 0.08207  1.33949E-03 0.16523  4.94330E-04 0.22648 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.82235E-01 0.06368  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 6.8E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.67895E-05 0.05389 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.81555E-05 0.05246 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.94202E-03 0.08989 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.17735E+02 0.04652 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.68480E-07 0.00746 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59961E-05 0.00394  1.59971E-05 0.00387  1.59204E-05 0.02746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.35746E-04 0.00762  1.35768E-04 0.00767  1.33912E-04 0.07799 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45189E-01 0.00543  2.44911E-01 0.00570  2.97008E-01 0.09221 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09078E+01 0.04872 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81014E+00 0.00074  5.17176E+01 0.00219 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:57:51 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.19330E+00  7.86116E-01  1.09131E+00  7.82011E-01  1.21362E+00  1.20909E+00  7.79451E-01  9.45109E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.9E-09  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50800E-01 0.00168  2.43632E-01 0.00171 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49200E-01 0.00138  7.56368E-01 0.00055 ];
DT_EFF                    (idx, [1:   4]) = [  2.59171E-01 0.00094  2.40661E-01 0.00079 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.23987E-01 0.00097  2.91500E-01 0.00123 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14207E+01 0.00411  4.55604E+00 0.00104  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41544E-01 2.3E-05  5.73092E-02 0.00038  1.14696E-03 0.00240  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.28866E+01 0.00222  2.80502E+01 0.00358 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.27148E+01 0.00223  2.77146E+01 0.00358 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09993E+02 0.00264  6.73633E+01 0.00434 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07028E+02 0.00288  2.22646E+01 0.00418 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100075 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00075E+04 0.00458 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00075E+04 0.00458 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.00728E+02 ;
RUNNING_TIME              (idx, 1)        =  1.28254E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.39767E-01  8.86667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.24275E+01  7.89800E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  3.33329E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.28254E+01  4.53487E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.85375 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99683E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75907E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  2.55949E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.87894E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19739E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.55949E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.87894E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.60740E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.46836E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.60740E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.46836E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  1.92612E+17  2.28701E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27279E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  5.73479E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143021 1.43425E+05 8.60181E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20464 2.05484E+04 1.05002E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 640745 6.42059E+05 2.82990E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460904 4.62191E+05 5.37817E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1265134 1.26822E+06 5.85218E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1231551 1.23440E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33583 3.38251E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1265134 1.26822E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -3.86499E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37593E-10 0.00185 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  6.94444E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.38705E-02 0.03931 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100075 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.31686E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100075 1.00732E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40138 4.03593E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42746 4.28471E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17191 1.75253E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100075 1.00732E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.01863E-10 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06537E+00 0.00298 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.31408E-01 0.00433 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73497E-01 0.00300 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.56340E+00 0.00577 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44469E-01 0.00142 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76648E-01 0.00058 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27059E+00 0.00271 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04793E+00 0.00353 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44574E+00 0.00010 ];
FISSE                     (idx, [1:   2]) = [  1.97334E+02 0.00048 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05298E+00 0.00296  1.04033E+00 0.00360  7.59858E-03 0.04894 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04793E+00 0.00353 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33182E+01 0.00168 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.29595E-05 0.02249 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.75216E-02 0.02018 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.46695E-03 0.02795  1.52246E-04 0.28307  1.08608E-03 0.10283  9.70833E-04 0.10814  2.65353E-03 0.04939  1.13695E-03 0.07436  4.67313E-04 0.20951 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  5.00315E-01 0.08365  1.33500E-02 0.00105  3.27390E-02 3.9E-09  1.20780E-01 5.6E-09  3.02780E-01 7.9E-09  8.49490E-01 3.9E-09  2.85580E+00 0.00098 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.86831E-05 0.01753  2.87414E-05 0.01730  2.02561E-05 0.27470 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.02005E-05 0.01741  3.02621E-05 0.01725  2.13178E-05 0.27325 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.36990E-03 0.04320  2.18654E-04 0.30264  1.22402E-03 0.17788  1.12068E-03 0.11473  2.86221E-03 0.06603  1.32940E-03 0.12036  6.14934E-04 0.32662 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.18433E-01 0.10902  1.33605E-02 0.00183  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.54077E-05 0.05397 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67264E-05 0.05227 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.47318E-03 0.09497 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.50062E+02 0.05428 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.62030E-07 0.00819 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59418E-05 0.00418  1.59429E-05 0.00415  1.57735E-05 0.03234 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.31275E-04 0.00824  1.31396E-04 0.00808  1.15839E-04 0.15999 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45544E-01 0.00266  2.45604E-01 0.00270  2.37320E-01 0.03931 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.13263E+01 0.08352 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80482E+00 0.00179  5.13949E+01 0.00321 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:58:37 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.20061E+00  7.80481E-01  8.23507E-01  7.81228E-01  1.21495E+00  1.20210E+00  7.76536E-01  1.22060E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50560E-01 0.00146  2.43810E-01 0.00210 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49440E-01 0.00120  7.56190E-01 0.00068 ];
DT_EFF                    (idx, [1:   4]) = [  2.59519E-01 0.00072  2.40809E-01 0.00115 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24973E-01 0.00118  2.91582E-01 0.00147 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14959E+01 0.00385  4.54705E+00 0.00132  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41578E-01 1.7E-05  5.72767E-02 0.00027  1.14536E-03 0.00347  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29888E+01 0.00265  2.78774E+01 0.00324 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.28169E+01 0.00266  2.75470E+01 0.00329 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09708E+02 0.00177  6.69266E+01 0.00253 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06513E+02 0.00340  2.21602E+01 0.00324 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100054 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00054E+04 0.00393 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00054E+04 0.00393 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.06731E+02 ;
RUNNING_TIME              (idx, 1)        =  1.35772E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.48067E-01  8.30000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.31703E+01  7.42783E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  3.49998E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.35771E+01  4.60857E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.86104 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99890E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76943E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.93439E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.41753E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19755E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.93439E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.41753E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.49784E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.33158E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.49784E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.33158E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  1.48423E+17  1.78556E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27336E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  4.43913E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143129 1.43525E+05 8.60476E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20318 2.03847E+04 1.04165E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 636517 6.37920E+05 2.80655E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 457688 4.58933E+05 5.33777E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1257652 1.26076E+06 5.80864E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1224593 1.22750E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33059 3.32650E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1257652 1.26076E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -7.75326E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.38094E-10 0.00208 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  7.63889E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.42513E-02 0.01287 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100054 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.28063E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100054 1.00728E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40306 4.05318E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42544 4.26521E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17204 1.75442E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100054 1.00728E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -4.71482E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07389E+00 0.00288 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.27579E-01 0.00367 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.74249E-01 0.00596 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54622E+00 0.00639 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43454E-01 0.00188 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77598E-01 0.00053 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26530E+00 0.00323 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04330E+00 0.00335 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44607E+00 0.00015 ];
FISSE                     (idx, [1:   2]) = [  1.97230E+02 0.00046 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04407E+00 0.00321  1.03558E+00 0.00347  7.72370E-03 0.05010 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04330E+00 0.00335 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33238E+01 0.00194 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.28023E-05 0.02654 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.00103E-01 0.03044 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.48258E-03 0.04920  2.02437E-04 0.26124  1.07387E-03 0.08713  1.16119E-03 0.11076  2.48061E-03 0.04838  1.19971E-03 0.09594  3.64770E-04 0.19473 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.61252E-01 0.05639  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02850E-01 0.00023  8.50003E-01 0.00060  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.97291E-05 0.01927  2.96527E-05 0.01852  3.61187E-05 0.29529 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.10421E-05 0.02009  3.09630E-05 0.01949  3.76386E-05 0.29452 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.57584E-03 0.04505  2.37734E-04 0.29935  1.34210E-03 0.10295  1.51106E-03 0.10642  2.81844E-03 0.06825  1.34009E-03 0.12266  3.26412E-04 0.22070 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.16920E-01 0.07126  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02930E-01 0.00049  8.50620E-01 0.00133  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55099E-05 0.05727 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.66407E-05 0.05779 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.88408E-03 0.09585 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.63759E+02 0.04859 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.66397E-07 0.01050 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59808E-05 0.00298  1.59750E-05 0.00295  1.68261E-05 0.03080 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33894E-04 0.00925  1.34018E-04 0.00870  1.21377E-04 0.11270 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45279E-01 0.00646  2.45162E-01 0.00663  2.76456E-01 0.12226 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32504E+01 0.10705 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.83425E+00 0.00137  5.15400E+01 0.00377 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 11:59:25 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21549E+00  7.77970E-01  1.22012E+00  7.75944E-01  1.13431E+00  8.66827E-01  7.90443E-01  1.21890E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50054E-01 0.00166  2.44327E-01 0.00104 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49946E-01 0.00135  7.55673E-01 0.00034 ];
DT_EFF                    (idx, [1:   4]) = [  2.59457E-01 0.00087  2.40875E-01 0.00086 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24977E-01 0.00173  2.91936E-01 0.00067 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14122E+01 0.00347  4.54901E+00 0.00116  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41556E-01 3.2E-05  5.72932E-02 0.00054  1.15059E-03 0.00214  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.30809E+01 0.00223  2.80755E+01 0.00307 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29090E+01 0.00224  2.77417E+01 0.00311 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09900E+02 0.00199  6.72849E+01 0.00302 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06428E+02 0.00307  2.23221E+01 0.00276 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100050 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00050E+04 0.00610 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00050E+04 0.00610 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.13200E+02 ;
RUNNING_TIME              (idx, 1)        =  1.43872E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.56517E-01  8.45000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.39712E+01  8.00833E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  3.66664E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.43871E+01  4.52576E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.86812 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99880E+00 0.00012 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77903E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.46919E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.07417E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.19769E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.46919E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.07417E+04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.41572E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.22931E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.41572E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.22931E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  1.15532E+17  1.41226E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27394E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.47469E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143610 1.44069E+05 8.56872E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20050 2.01327E+04 1.02878E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 640140 6.41726E+05 2.82572E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 461126 4.62519E+05 5.37583E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1264926 1.26845E+06 5.84697E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1231526 1.23483E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33400 3.36141E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1264926 1.26845E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 6.65896E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36717E-10 0.00162 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  8.33333E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.43494E-02 0.02779 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100050 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.28938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100050 1.00729E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40468 4.07281E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42380 4.24831E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17202 1.75178E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100050 1.00729E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.19326E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06379E+00 0.00624 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.32339E-01 0.00277 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73930E-01 0.00724 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.52590E+00 0.00600 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44037E-01 0.00117 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77233E-01 0.00047 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25975E+00 0.00364 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03907E+00 0.00397 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44585E+00 0.00012 ];
FISSE                     (idx, [1:   2]) = [  1.97398E+02 0.00046 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03881E+00 0.00385  1.03102E+00 0.00386  8.04569E-03 0.05204 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03907E+00 0.00397 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33617E+01 0.00082 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.15033E-05 0.01099 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.81842E-02 0.02408 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.61099E-03 0.03244  2.12274E-04 0.18962  1.24020E-03 0.05579  1.39132E-03 0.14161  2.56647E-03 0.05942  8.35156E-04 0.08334  3.65566E-04 0.16544 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.15674E-01 0.06204  1.33360E-02 0.0E+00  3.27290E-02 0.00031  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.10094E-05 0.01575  3.10009E-05 0.01617  3.12636E-05 0.19103 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.22117E-05 0.01596  3.22020E-05 0.01621  3.25774E-05 0.19255 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.92314E-03 0.05301  1.08624E-04 0.34296  1.62301E-03 0.09291  1.79764E-03 0.16278  2.83759E-03 0.07179  1.08055E-03 0.14527  4.75720E-04 0.21808 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.35377E-01 0.11490  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.82535E-05 0.05311 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.93478E-05 0.05328 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.25243E-03 0.08493 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.18687E+02 0.04713 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.68615E-07 0.00807 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60149E-05 0.00323  1.60279E-05 0.00306  1.42544E-05 0.05755 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.35006E-04 0.00606  1.35025E-04 0.00611  1.29886E-04 0.08329 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45404E-01 0.00679  2.45350E-01 0.00697  2.56413E-01 0.07247 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.24290E+01 0.08709 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81709E+00 0.00187  5.18443E+01 0.00180 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:00:15 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  8.11770E-01  8.12783E-01  1.20422E+00  1.17856E+00  8.11076E-01  1.18224E+00  1.19558E+00  8.03770E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50387E-01 0.00221  2.44491E-01 0.00105 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49613E-01 0.00181  7.55509E-01 0.00034 ];
DT_EFF                    (idx, [1:   4]) = [  2.59308E-01 0.00058  2.40915E-01 0.00048 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24429E-01 0.00104  2.92002E-01 0.00070 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14917E+01 0.00352  4.55174E+00 0.00080  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41556E-01 3.3E-05  5.72976E-02 0.00055  1.14646E-03 0.00394  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.31533E+01 0.00210  2.82015E+01 0.00314 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29817E+01 0.00212  2.78639E+01 0.00318 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10325E+02 0.00182  6.75593E+01 0.00292 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07089E+02 0.00427  2.24377E+01 0.00261 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100055 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00055E+04 0.00497 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00055E+04 0.00497 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.19827E+02 ;
RUNNING_TIME              (idx, 1)        =  1.52173E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.65100E-01  8.58334E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.47921E+01  8.20900E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  3.83329E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.52173E+01  4.63288E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.87436 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99872E+00 0.00014 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78762E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.12298E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  8.18630E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19782E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.12298E+17 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.18630E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.35403E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.15272E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.35403E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.15272E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  9.10471E+16  1.13430E+03 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27454E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  2.75669E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 144558 1.44892E+05 8.70193E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20046 2.00924E+04 1.02672E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 643020 6.44400E+05 2.84033E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 462558 4.63784E+05 5.39815E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1270182 1.27317E+06 5.87188E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1236410 1.23922E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33772 3.39451E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1270182 1.27317E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 3.46918E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.38546E-10 0.00142 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  9.02778E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.48549E-02 0.03666 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100055 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.40892E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100055 1.00741E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40297 4.05189E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42581 4.27054E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17177 1.75165E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100055 1.00741E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -4.56930E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06580E+00 0.00398 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.33648E-01 0.00528 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72733E-01 0.00579 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54919E+00 0.00774 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44580E-01 0.00112 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76621E-01 0.00033 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26633E+00 0.00251 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04451E+00 0.00281 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44585E+00 7.9E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97413E+02 0.00048 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04119E+00 0.00308  1.03743E+00 0.00266  7.08281E-03 0.06355 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04451E+00 0.00281 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33644E+01 0.00152 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14604E-05 0.02051 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.81838E-02 0.01663 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.23220E-03 0.05185  2.38931E-04 0.17038  1.10888E-03 0.09359  1.09212E-03 0.12951  2.36470E-03 0.07735  1.01590E-03 0.13373  4.11666E-04 0.14816 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.67148E-01 0.06203  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 6.8E-09  8.49843E-01 0.00042  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.11064E-05 0.01716  3.12062E-05 0.01697  1.71572E-05 0.13178 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.23754E-05 0.01484  3.24795E-05 0.01467  1.78363E-05 0.13036 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.91971E-03 0.06712  2.66118E-04 0.27202  1.08196E-03 0.15814  1.12798E-03 0.10391  3.06719E-03 0.10181  1.12849E-03 0.14482  2.47966E-04 0.25438 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  3.98623E-01 0.07913  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 7.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.70950E-05 0.04977 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.82165E-05 0.05015 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.48957E-03 0.07382 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.73821E+02 0.03484 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.65718E-07 0.00661 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59953E-05 0.00372  1.59975E-05 0.00380  1.55378E-05 0.04197 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.34346E-04 0.00677  1.34474E-04 0.00691  1.10772E-04 0.16167 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44929E-01 0.00507  2.44978E-01 0.00542  2.41290E-01 0.07817 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  8.93116E+00 0.05978 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81305E+00 0.00155  5.19745E+01 0.00383 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:01:03 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.18944E+00  8.81230E-01  1.11689E+00  8.09104E-01  8.05641E-01  1.18763E+00  1.21789E+00  7.92164E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.9E-09  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.49871E-01 0.00204  2.44209E-01 0.00142 ];
DT_FRAC                   (idx, [1:   4]) = [  5.50129E-01 0.00167  7.55791E-01 0.00046 ];
DT_EFF                    (idx, [1:   4]) = [  2.59119E-01 0.00080  2.40921E-01 0.00077 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24301E-01 0.00060  2.91866E-01 0.00110 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15050E+01 0.00286  4.54782E+00 0.00103  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41548E-01 2.3E-05  5.73043E-02 0.00037  1.14775E-03 0.00289  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.30862E+01 0.00291  2.80112E+01 0.00398 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29139E+01 0.00295  2.76772E+01 0.00398 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10248E+02 0.00262  6.71497E+01 0.00303 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06726E+02 0.00513  2.22762E+01 0.00395 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100093 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00093E+04 0.00482 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00093E+04 0.00482 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.26204E+02 ;
RUNNING_TIME              (idx, 1)        =  1.60164E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.74367E-01  9.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.55808E+01  7.88783E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  3.99995E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.60163E+01  4.66669E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.87968 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99855E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79493E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.65310E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.28443E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19793E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.65310E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.28443E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.30753E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.09525E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.30753E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.09525E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  7.28171E+16  9.27278E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27515E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  2.22207E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143698 1.44052E+05 8.66226E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20170 2.02496E+04 1.03475E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 637936 6.39152E+05 2.81112E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460582 4.61763E+05 5.36606E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1262386 1.26522E+06 5.83727E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1228952 1.23158E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33434 3.36375E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1262386 1.26522E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 5.19212E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37998E-10 0.00161 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  9.72222E-02  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.56643E-02 0.02811 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100093 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.36705E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100093 1.00737E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40407 4.06500E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42441 4.25269E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17245 1.75598E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100093 1.00737E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -6.56291E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07245E+00 0.00507 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.28653E-01 0.00651 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72736E-01 0.00370 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.55315E+00 0.00856 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44341E-01 0.00238 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76392E-01 0.00054 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26174E+00 0.00326 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04018E+00 0.00380 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44593E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97367E+02 0.00052 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04147E+00 0.00337  1.03339E+00 0.00387  6.79147E-03 0.05754 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04018E+00 0.00380 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33561E+01 0.00194 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.17614E-05 0.02717 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.88454E-02 0.02246 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.05650E-03 0.03074  2.12058E-04 0.24356  1.25870E-03 0.10447  9.99965E-04 0.10949  2.16329E-03 0.07236  1.03833E-03 0.08041  3.84158E-04 0.15773 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.59701E-01 0.07255  1.33360E-02 0.0E+00  3.27302E-02 0.00027  1.20780E-01 0.0E+00  3.02780E-01 7.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.02003E-05 0.01776  3.02202E-05 0.01773  2.69935E-05 0.15052 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.14448E-05 0.01644  3.14657E-05 0.01642  2.80863E-05 0.15018 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.57442E-03 0.06377  1.97780E-04 0.32402  1.20542E-03 0.12778  1.12096E-03 0.16896  2.51256E-03 0.09362  1.11035E-03 0.14850  4.27344E-04 0.27482 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.74879E-01 0.11430  1.33360E-02 8.2E-09  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.57315E-05 0.05737 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67987E-05 0.05755 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.38579E-03 0.07266 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.08100E+02 0.02817 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.66169E-07 0.00732 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60237E-05 0.00337  1.60209E-05 0.00334  1.63863E-05 0.03592 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.34321E-04 0.00603  1.34267E-04 0.00612  1.42255E-04 0.14152 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45108E-01 0.00543  2.44946E-01 0.00560  2.76291E-01 0.08953 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06993E+01 0.09924 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.82623E+00 0.00069  5.18123E+01 0.00281 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:01:50 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21980E+00  1.26367E+00  7.48671E-01  7.66210E-01  7.55121E-01  1.25690E+00  1.24997E+00  7.39661E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 6.8E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50246E-01 0.00251  2.44187E-01 0.00069 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49754E-01 0.00205  7.55813E-01 0.00022 ];
DT_EFF                    (idx, [1:   4]) = [  2.59265E-01 0.00073  2.40720E-01 0.00095 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24595E-01 0.00107  2.91799E-01 0.00076 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14713E+01 0.00455  4.54863E+00 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41545E-01 2.1E-05  5.73098E-02 0.00033  1.14534E-03 0.00279  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29753E+01 0.00163  2.80589E+01 0.00316 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.28042E+01 0.00164  2.77231E+01 0.00320 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09873E+02 0.00152  6.72851E+01 0.00350 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06517E+02 0.00454  2.22969E+01 0.00371 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99992 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99920E+03 0.00514 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99920E+03 0.00514 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.32523E+02 ;
RUNNING_TIME              (idx, 1)        =  1.68081E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.82500E-01  8.13334E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.63637E+01  7.82850E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  4.16660E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.68080E+01  4.61024E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.88453 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99843E+00 0.00015 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80206E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  6.73514E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  4.86884E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19804E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  6.73514E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  4.86884E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.27235E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.05199E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.27235E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.05199E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  5.92415E+16  7.73042E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27576E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.82391E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143887 1.44337E+05 8.55286E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20566 2.06457E+04 1.05500E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 640160 6.41868E+05 2.82459E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460627 4.62103E+05 5.37301E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1265240 1.26895E+06 5.84650E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1231668 1.23517E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33572 3.37834E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1265240 1.26895E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -3.51574E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36305E-10 0.00168 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.04167E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.38468E-02 0.03006 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99992 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.23783E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99992 1.00724E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40019 4.03076E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42874 4.29972E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17099 1.74189E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99992 1.00724E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -3.20142E-10 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07000E+00 0.00455 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.32768E-01 0.00421 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73758E-01 0.00569 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.55298E+00 0.00719 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.46166E-01 0.00140 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.75943E-01 0.00056 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27338E+00 0.00337 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05156E+00 0.00356 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44565E+00 7.7E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97273E+02 0.00038 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05317E+00 0.00395  1.04434E+00 0.00364  7.22009E-03 0.04382 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05156E+00 0.00356 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33566E+01 0.00136 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16945E-05 0.01851 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.65314E-02 0.01566 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.18355E-03 0.02187  1.99483E-04 0.10913  1.07623E-03 0.06878  9.80058E-04 0.09413  2.50111E-03 0.06530  1.02661E-03 0.10214  4.00067E-04 0.11033 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.71977E-01 0.04285  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02896E-01 0.00038  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.87450E-05 0.01727  2.88215E-05 0.01720  1.92161E-05 0.18227 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.02691E-05 0.01687  3.03496E-05 0.01680  2.02358E-05 0.18345 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.43834E-03 0.05653  2.17714E-04 0.32377  1.16210E-03 0.09303  1.22073E-03 0.14275  3.23619E-03 0.07815  1.22264E-03 0.13564  3.78969E-04 0.28025 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.38173E-01 0.08927  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.03022E-01 0.00080  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.56390E-05 0.04602 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.70171E-05 0.04748 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.56737E-03 0.05638 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.94678E+02 0.02694 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.66261E-07 0.00502 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59545E-05 0.00260  1.59589E-05 0.00266  1.53751E-05 0.03289 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.32973E-04 0.00795  1.33012E-04 0.00818  1.25526E-04 0.08912 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.46745E-01 0.00555  2.46397E-01 0.00574  3.03257E-01 0.05341 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07213E+01 0.06912 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80612E+00 0.00183  5.16235E+01 0.00189 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:02:39 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  7.81181E-01  7.77443E-01  1.22176E+00  7.72156E-01  7.60567E-01  1.23457E+00  1.24376E+00  1.20857E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50510E-01 0.00171  2.44135E-01 0.00132 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49490E-01 0.00140  7.55865E-01 0.00043 ];
DT_EFF                    (idx, [1:   4]) = [  2.59188E-01 0.00102  2.40404E-01 0.00124 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24425E-01 0.00127  2.91438E-01 0.00112 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14798E+01 0.00416  4.55622E+00 0.00119  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41556E-01 2.1E-05  5.72978E-02 0.00033  1.14667E-03 0.00307  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29476E+01 0.00184  2.80097E+01 0.00236 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.27754E+01 0.00185  2.76730E+01 0.00241 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09899E+02 0.00186  6.72803E+01 0.00222 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06684E+02 0.00394  2.22883E+01 0.00257 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99957 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99570E+03 0.00403 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99570E+03 0.00403 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.38952E+02 ;
RUNNING_TIME              (idx, 1)        =  1.76139E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.92300E-01  9.80000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.71591E+01  7.95383E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  4.33330E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.76138E+01  4.60915E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.88880 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99906E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80817E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  5.30737E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  3.81508E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19813E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  5.30737E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  3.81508E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.24558E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.01932E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.24558E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.01932E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  4.91293E+16  6.58086E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27638E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.52728E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142665 1.43226E+05 8.51708E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20242 2.03306E+04 1.03889E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 638198 6.40588E+05 2.82087E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 459818 4.61701E+05 5.36039E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1260923 1.26584E+06 5.83154E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1227263 1.23194E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33660 3.39023E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1260923 1.26584E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -2.77068E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37764E-10 0.00141 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.11111E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.52848E-02 0.03021 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99957 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.53191E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99957 1.00753E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40036 4.03301E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42706 4.28616E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17215 1.75615E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99957 1.00753E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.28465E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05817E+00 0.00674 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.36972E-01 0.00622 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72478E-01 0.00364 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.56176E+00 0.00545 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44818E-01 0.00139 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.75814E-01 0.00040 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27169E+00 0.00397 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04834E+00 0.00370 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44587E+00 5.8E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97252E+02 0.00035 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04760E+00 0.00357  1.04129E+00 0.00373  7.04472E-03 0.05082 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04834E+00 0.00370 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33114E+01 0.00121 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.31499E-05 0.01593 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.92739E-02 0.01136 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.10533E-03 0.02726  2.10250E-04 0.26194  1.10710E-03 0.08287  1.12314E-03 0.10554  2.52922E-03 0.03618  7.14171E-04 0.15423  4.21454E-04 0.12791 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.50645E-01 0.04804  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02948E-01 0.00055  8.49490E-01 0.0E+00  2.85626E+00 0.00114 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.86703E-05 0.01729  2.87050E-05 0.01726  2.37412E-05 0.19565 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.00351E-05 0.01767  3.00718E-05 0.01770  2.48239E-05 0.19475 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.66955E-03 0.05180  2.46335E-04 0.39858  1.16149E-03 0.09054  1.21057E-03 0.13110  2.74170E-03 0.04803  8.52400E-04 0.23065  4.57064E-04 0.17223 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.53933E-01 0.07381  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 5.6E-09  8.49490E-01 0.0E+00  2.86387E+00 0.00380 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.59322E-05 0.05402 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.71766E-05 0.05465 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.54444E-03 0.07669 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.11480E+02 0.03401 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.64058E-07 0.00542 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.61401E-05 0.00388  1.61363E-05 0.00408  1.63799E-05 0.04630 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.32405E-04 0.00600  1.32468E-04 0.00585  1.22776E-04 0.06024 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45419E-01 0.00451  2.45288E-01 0.00445  2.66914E-01 0.06144 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.11991E+01 0.07971 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81554E+00 0.00102  5.15632E+01 0.00262 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:03:24 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.15888E+00  7.96662E-01  1.21607E+00  8.04156E-01  9.53241E-01  1.21745E+00  8.01020E-01  1.05253E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 5.6E-09  5.00000E-02 5.6E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50808E-01 0.00216  2.44162E-01 0.00196 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49192E-01 0.00178  7.55838E-01 0.00063 ];
DT_EFF                    (idx, [1:   4]) = [  2.59730E-01 0.00059  2.41013E-01 0.00081 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24700E-01 0.00090  2.91855E-01 0.00118 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14705E+01 0.00462  4.55195E+00 0.00090  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41539E-01 2.5E-05  5.73186E-02 0.00036  1.14195E-03 0.00458  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.28027E+01 0.00150  2.79616E+01 0.00298 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.26313E+01 0.00152  2.76269E+01 0.00301 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09461E+02 0.00141  6.70342E+01 0.00353 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06551E+02 0.00514  2.22443E+01 0.00405 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100316 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00316E+04 0.00573 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00316E+04 0.00573 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.45028E+02 ;
RUNNING_TIME              (idx, 1)        =  1.83752E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.00500E-01  8.20000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.79112E+01  7.52183E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  4.49995E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.83751E+01  4.61639E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.89262 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99929E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81343E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  4.24436E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  3.03056E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19822E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  4.24436E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  3.03056E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.22508E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.94531E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.22508E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.94531E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  4.15942E+16  5.72360E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27700E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.30620E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143517 1.43602E+05 8.49675E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20206 2.02478E+04 1.03466E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 639544 6.39576E+05 2.82003E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 461606 4.61891E+05 5.36044E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1264873 1.26532E+06 5.83088E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1231297 1.23157E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33576 3.37423E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1264873 1.26532E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -6.40284E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.35817E-10 0.00124 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.18056E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.20369E-02 0.01327 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100316 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.53218E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100316 1.00753E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40191 4.03522E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42931 4.29168E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17194 1.74842E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100316 1.00753E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -6.54836E-10 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07255E+00 0.00494 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.35639E-01 0.00496 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.71462E-01 0.00348 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.55811E+00 0.00433 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44449E-01 0.00126 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77158E-01 0.00033 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27207E+00 0.00333 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04968E+00 0.00405 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44584E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97238E+02 0.00043 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04985E+00 0.00424  1.04201E+00 0.00402  7.66691E-03 0.03289 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04968E+00 0.00405 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33364E+01 0.00125 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.23323E-05 0.01676 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.82671E-02 0.02226 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.11853E-03 0.02769  2.76409E-04 0.19199  1.11721E-03 0.09850  8.28568E-04 0.06864  2.51213E-03 0.06792  9.94807E-04 0.09608  3.89406E-04 0.16565 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.70081E-01 0.06321  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 6.8E-09  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.01684E-05 0.01385  3.01697E-05 0.01435  3.05366E-05 0.17679 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.16585E-05 0.01067  3.16594E-05 0.01116  3.21016E-05 0.17820 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.55832E-03 0.04020  3.17791E-04 0.34951  1.38747E-03 0.14149  1.23818E-03 0.11733  3.03086E-03 0.08557  1.17768E-03 0.10749  4.06344E-04 0.28770 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.28153E-01 0.08839  1.33360E-02 8.2E-09  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.64162E-05 0.05227 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.77176E-05 0.05160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.69058E-03 0.07516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.13052E+02 0.03251 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.64417E-07 0.00720 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60003E-05 0.00472  1.60108E-05 0.00486  1.45934E-05 0.03544 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33586E-04 0.00782  1.33588E-04 0.00738  1.32736E-04 0.12377 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.43619E-01 0.00286  2.43264E-01 0.00284  3.04595E-01 0.07604 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.18601E+01 0.11294 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80161E+00 0.00139  5.14960E+01 0.00199 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:04:08 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21275E+00  7.83619E-01  1.20970E+00  7.81429E-01  1.21862E+00  1.22947E+00  8.02158E-01  7.62248E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50563E-01 0.00158  2.44228E-01 0.00205 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49437E-01 0.00130  7.55772E-01 0.00066 ];
DT_EFF                    (idx, [1:   4]) = [  2.59296E-01 0.00093  2.40794E-01 0.00056 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24686E-01 0.00147  2.91741E-01 0.00077 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.13906E+01 0.00391  4.55209E+00 0.00080  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41549E-01 2.6E-05  5.73061E-02 0.00043  1.14474E-03 0.00212  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.31362E+01 0.00195  2.78386E+01 0.00320 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29632E+01 0.00195  2.75055E+01 0.00319 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10158E+02 0.00131  6.67756E+01 0.00344 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06939E+02 0.00323  2.21515E+01 0.00282 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99842 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.98420E+03 0.00791 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.98420E+03 0.00791 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.50765E+02 ;
RUNNING_TIME              (idx, 1)        =  1.90939E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.08983E-01  8.48333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.86208E+01  7.09583E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  4.66661E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.90939E+01  4.55709E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.89597 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99939E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81835E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  3.45277E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.44639E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19830E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  3.45277E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  2.44639E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.20924E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.75600E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.20924E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.75600E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  3.59768E+16  5.08387E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27762E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.14134E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 141807 1.42579E+05 8.54012E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 19598 1.97080E+04 1.00708E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 634974 6.37982E+05 2.81026E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 458118 4.60549E+05 5.33368E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1254497 1.26082E+06 5.80082E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1221234 1.22724E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33263 3.35816E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1254497 1.26082E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 6.98492E-09 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36234E-10 0.00111 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.25000E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.41416E-02 0.02931 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99842 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.18110E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99842 1.00818E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40089 4.04529E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42483 4.26820E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17270 1.76831E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99842 1.00818E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 5.67525E-10 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.08124E+00 0.00373 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.32917E-01 0.00428 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72858E-01 0.00582 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.52925E+00 0.00789 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43198E-01 0.00115 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76247E-01 0.00067 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26785E+00 0.00416 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04367E+00 0.00481 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44523E+00 7.9E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97203E+02 0.00059 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04186E+00 0.00537  1.03596E+00 0.00473  7.71285E-03 0.05800 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04367E+00 0.00481 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33929E+01 0.00131 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.05597E-05 0.01715 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.27184E-02 0.01841 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.24713E-03 0.02315  1.72799E-04 0.13915  1.20532E-03 0.09408  9.90941E-04 0.08705  2.42515E-03 0.07671  1.12527E-03 0.08152  3.27651E-04 0.18933 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.47924E-01 0.06435  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20798E-01 0.00015  3.02890E-01 0.00036  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.03574E-05 0.01823  3.04262E-05 0.01863  2.10693E-05 0.15204 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.16178E-05 0.01688  3.16896E-05 0.01732  2.19475E-05 0.15085 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.29760E-03 0.04733  3.09963E-04 0.26884  1.50025E-03 0.07133  1.19886E-03 0.14114  2.66003E-03 0.09744  1.32079E-03 0.14688  3.07701E-04 0.23064 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.10425E-01 0.07268  1.33360E-02 0.0E+00  3.27390E-02 6.8E-09  1.20807E-01 0.00022  3.02780E-01 7.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.63785E-05 0.05843 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.75054E-05 0.06001 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.04377E-03 0.08226 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.26119E+02 0.03098 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.68602E-07 0.00931 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60450E-05 0.00252  1.60427E-05 0.00242  1.63565E-05 0.03303 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.35607E-04 0.01031  1.35777E-04 0.01022  1.18519E-04 0.12693 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44898E-01 0.00575  2.44749E-01 0.00605  2.69653E-01 0.08157 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.19735E+01 0.05383 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.83682E+00 0.00231  5.20387E+01 0.00305 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:04:51 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.20285E+00  7.77796E-01  8.25326E-01  7.80567E-01  1.21382E+00  1.20839E+00  1.22011E+00  7.71136E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 5.6E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50752E-01 0.00102  2.44122E-01 0.00203 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49248E-01 0.00084  7.55878E-01 0.00066 ];
DT_EFF                    (idx, [1:   4]) = [  2.59206E-01 0.00130  2.40488E-01 0.00087 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24375E-01 0.00175  2.91425E-01 0.00100 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14357E+01 0.00425  4.55611E+00 0.00117  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41571E-01 2.7E-05  5.72830E-02 0.00045  1.14638E-03 0.00222  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29978E+01 0.00246  2.80595E+01 0.00376 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.28234E+01 0.00246  2.77247E+01 0.00377 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10023E+02 0.00175  6.74131E+01 0.00475 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06943E+02 0.00311  2.23430E+01 0.00511 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99825 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.98250E+03 0.00645 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.98250E+03 0.00645 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.56523E+02 ;
RUNNING_TIME              (idx, 1)        =  1.98151E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.16700E-01  7.71666E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.93336E+01  7.12717E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  4.83330E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  1.66655E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.98151E+01  4.50359E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.89919 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99877E+00 0.00012 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82266E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  2.86314E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.01130E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19837E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.86314E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  2.01130E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.19688E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.61033E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.19688E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.61033E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  3.17864E+16  4.60602E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27825E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.01832E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142976 1.43714E+05 8.46417E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20248 2.03604E+04 1.04041E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 638933 6.41953E+05 2.82850E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460587 4.62949E+05 5.38059E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1262744 1.26898E+06 5.85213E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1229315 1.23527E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33429 3.37079E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1262744 1.26898E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 5.23869E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37842E-10 0.00207 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.31944E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.40648E-02 0.01768 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99825 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.48028E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99825 1.00748E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 39860 4.02316E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42558 4.27450E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17407 1.77715E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99825 1.00748E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -3.66708E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07144E+00 0.00379 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.34928E-01 0.00482 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73550E-01 0.00532 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53529E+00 0.00708 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.41532E-01 0.00173 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77126E-01 0.00029 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27130E+00 0.00281 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04540E+00 0.00424 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44567E+00 9.9E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97368E+02 0.00036 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04535E+00 0.00437  1.03815E+00 0.00425  7.24707E-03 0.03566 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04540E+00 0.00424 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33660E+01 0.00115 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.13837E-05 0.01539 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67548E-02 0.01969 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.29409E-03 0.05035  2.85407E-04 0.15601  1.22583E-03 0.14026  1.14240E-03 0.10220  2.13254E-03 0.06624  1.05824E-03 0.11284  4.49669E-04 0.11231 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.82570E-01 0.06373  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02880E-01 0.00033  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.92611E-05 0.01553  2.92538E-05 0.01569  3.16118E-05 0.16391 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.05869E-05 0.01583  3.05797E-05 0.01611  3.29438E-05 0.16171 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.82927E-03 0.04904  2.65852E-04 0.25713  1.44778E-03 0.15792  1.12326E-03 0.13001  2.34947E-03 0.09136  1.14646E-03 0.15535  4.96449E-04 0.16531 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.88589E-01 0.06542  1.33570E-02 0.00157  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02935E-01 0.00051  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.59597E-05 0.05065 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.71215E-05 0.04960 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.34301E-03 0.04848 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06238E+02 0.01847 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.66777E-07 0.00817 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60242E-05 0.00241  1.60274E-05 0.00243  1.53538E-05 0.04776 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.34328E-04 0.00879  1.34666E-04 0.00868  8.63046E-05 0.05929 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44513E-01 0.00366  2.44383E-01 0.00376  2.71190E-01 0.07818 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.27584E+01 0.09075 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80733E+00 0.00257  5.18655E+01 0.00161 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:05:34 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.19717E+00  7.85884E-01  9.85883E-01  7.87002E-01  1.03434E+00  1.22294E+00  1.21794E+00  7.68845E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51284E-01 0.00160  2.43769E-01 0.00193 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48716E-01 0.00132  7.56231E-01 0.00062 ];
DT_EFF                    (idx, [1:   4]) = [  2.59549E-01 0.00082  2.40721E-01 0.00056 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.25020E-01 0.00096  2.91741E-01 0.00075 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14915E+01 0.00426  4.54918E+00 0.00091  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41575E-01 4.2E-05  5.72812E-02 0.00065  1.14421E-03 0.00349  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29885E+01 0.00204  2.80053E+01 0.00275 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.28162E+01 0.00205  2.76690E+01 0.00277 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09685E+02 0.00171  6.71729E+01 0.00318 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06851E+02 0.00449  2.22050E+01 0.00373 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100244 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00244E+04 0.00306 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00244E+04 0.00306 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.62288E+02 ;
RUNNING_TIME              (idx, 1)        =  2.05372E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.24933E-01  8.23333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.00467E+01  7.13133E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  4.99996E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.05371E+01  4.50896E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.90214 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99891E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82661E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  2.42379E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.68714E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19844E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.42379E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.68714E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.18709E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.49712E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.18709E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.49712E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.86578E+16  4.24863E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27888E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.26431E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143458 1.43665E+05 8.45415E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20254 2.02822E+04 1.03642E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 638566 6.38876E+05 2.81214E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460794 4.61278E+05 5.36057E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1263072 1.26410E+06 5.82997E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1229354 1.23024E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33718 3.38653E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1263072 1.26410E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -7.40401E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37807E-10 0.00194 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.38889E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.38376E-02 0.02324 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100244 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.29325E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100244 1.00729E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40243 4.04080E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42731 4.27592E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17270 1.75622E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100244 1.00729E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.39698E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06586E+00 0.00341 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.33085E-01 0.00432 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72690E-01 0.00487 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.55870E+00 0.00709 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43855E-01 0.00165 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76916E-01 0.00054 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26864E+00 0.00321 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04584E+00 0.00367 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44588E+00 0.00013 ];
FISSE                     (idx, [1:   2]) = [  1.97267E+02 0.00034 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04320E+00 0.00301  1.03864E+00 0.00371  7.19839E-03 0.07461 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04584E+00 0.00367 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33413E+01 0.00183 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.22209E-05 0.02384 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.85484E-02 0.02553 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.22023E-03 0.05201  2.21003E-04 0.22713  1.15170E-03 0.10205  1.13295E-03 0.12392  2.18021E-03 0.07797  1.10693E-03 0.10151  4.27441E-04 0.22112 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.83712E-01 0.08120  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 7.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.97027E-05 0.01349  2.96258E-05 0.01436  3.82604E-05 0.19703 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.09880E-05 0.01444  3.09086E-05 0.01544  3.98077E-05 0.19569 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.69912E-03 0.06473  1.58411E-04 0.40016  1.45747E-03 0.15046  9.34153E-04 0.24703  2.19759E-03 0.12771  1.46557E-03 0.13113  4.85936E-04 0.32521 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.15652E-01 0.11674  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 4.0E-09  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.57345E-05 0.05053 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.68674E-05 0.05203 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.35496E-03 0.06141 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.46308E+02 0.02479 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.66532E-07 0.00423 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60323E-05 0.00211  1.60260E-05 0.00204  1.70258E-05 0.02660 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.34196E-04 0.00521  1.34078E-04 0.00493  1.48942E-04 0.15111 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44556E-01 0.00551  2.44603E-01 0.00585  2.45575E-01 0.09405 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.13094E+01 0.08757 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.82367E+00 0.00108  5.18482E+01 0.00256 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:06:17 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21292E+00  7.74151E-01  1.21207E+00  7.71538E-01  8.30136E-01  1.21340E+00  1.22182E+00  7.63966E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50181E-01 0.00175  2.44445E-01 0.00113 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49819E-01 0.00144  7.55555E-01 0.00037 ];
DT_EFF                    (idx, [1:   4]) = [  2.59686E-01 0.00113  2.40391E-01 0.00067 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.25422E-01 0.00100  2.91562E-01 0.00077 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14652E+01 0.00369  4.56128E+00 0.00084  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41619E-01 2.5E-05  5.72314E-02 0.00041  1.14914E-03 0.00272  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.30979E+01 0.00177  2.81608E+01 0.00384 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29270E+01 0.00177  2.78249E+01 0.00387 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09715E+02 0.00205  6.76097E+01 0.00410 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06291E+02 0.00392  2.24194E+01 0.00468 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100070 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00070E+04 0.00554 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00070E+04 0.00554 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.68050E+02 ;
RUNNING_TIME              (idx, 1)        =  2.12588E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.32883E-01  7.95000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.07596E+01  7.12883E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  5.16661E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.12587E+01  4.50846E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.90498 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99857E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.83046E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  2.09627E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.44553E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19850E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.09627E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.44553E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.17923E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.40810E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.17923E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.40810E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.63194E+16  3.98091E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.27950E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  8.57706E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143273 1.43602E+05 8.42983E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20020 2.00658E+04 1.02536E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 641555 6.42734E+05 2.83421E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 463014 4.64234E+05 5.37854E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1267862 1.27063E+06 5.84879E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1234244 1.23679E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33618 3.38426E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1267862 1.27063E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 7.68341E-09 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36888E-10 0.00225 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 21 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.45833E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.35609E-02 0.02092 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100070 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.73824E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100070 1.00774E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 39969 4.02019E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43000 4.30997E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17101 1.74723E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100070 1.00774E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -8.60018E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07511E+00 0.00333 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.34799E-01 0.00366 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73709E-01 0.00507 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54423E+00 0.00733 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44960E-01 0.00120 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76707E-01 0.00064 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27732E+00 0.00256 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05413E+00 0.00248 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44580E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97292E+02 0.00047 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05756E+00 0.00316  1.04689E+00 0.00270  7.24669E-03 0.05119 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05413E+00 0.00248 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33479E+01 0.00184 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.20099E-05 0.02407 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.78865E-02 0.02407 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.35279E-03 0.03870  1.70727E-04 0.13882  1.25283E-03 0.08817  1.16675E-03 0.07778  2.29474E-03 0.06187  1.05954E-03 0.07063  4.08206E-04 0.13619 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.68940E-01 0.05252  1.33360E-02 3.9E-09  3.27189E-02 0.00061  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.00014E-05 0.01536  3.00399E-05 0.01527  2.38954E-05 0.20994 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.17314E-05 0.01633  3.17719E-05 0.01619  2.52968E-05 0.21027 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.86549E-03 0.06217  1.77825E-04 0.42310  1.32134E-03 0.14606  1.27421E-03 0.15380  2.50863E-03 0.07183  1.14596E-03 0.16773  4.37522E-04 0.21023 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.61839E-01 0.07130  1.33360E-02 5.9E-09  3.26688E-02 0.00215  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.61964E-05 0.05275 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.76786E-05 0.05128 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.60663E-03 0.08230 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.10912E+02 0.03878 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.71044E-07 0.00742 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60358E-05 0.00417  1.60346E-05 0.00427  1.64978E-05 0.05940 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.36080E-04 0.00864  1.36077E-04 0.00876  1.47055E-04 0.12543 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45895E-01 0.00413  2.46022E-01 0.00429  2.29597E-01 0.09644 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10152E+01 0.07522 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81371E+00 0.00182  5.18025E+01 0.00346 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:07:01 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  7.64745E-01  7.47903E-01  1.26365E+00  7.41508E-01  1.23823E+00  1.25949E+00  1.24771E+00  7.36764E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 6.8E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.52572E-01 0.00136  2.43711E-01 0.00156 ];
DT_FRAC                   (idx, [1:   4]) = [  5.47428E-01 0.00113  7.56289E-01 0.00050 ];
DT_EFF                    (idx, [1:   4]) = [  2.59434E-01 0.00065  2.40917E-01 0.00109 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.25041E-01 0.00126  2.91688E-01 0.00115 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14607E+01 0.00452  4.54955E+00 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41583E-01 3.3E-05  5.72692E-02 0.00055  1.14733E-03 0.00197  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.31266E+01 0.00105  2.81902E+01 0.00217 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29526E+01 0.00106  2.78522E+01 0.00220 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09959E+02 0.00172  6.76353E+01 0.00277 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07706E+02 0.00200  2.23855E+01 0.00220 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100290 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00290E+04 0.00482 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00290E+04 0.00482 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.73861E+02 ;
RUNNING_TIME              (idx, 1)        =  2.19867E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.41033E-01  8.15000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.14786E+01  7.19050E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  5.33330E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.19867E+01  4.50959E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.90755 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99960E+00 0.00014 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.83374E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.85196E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.26534E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19856E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.85196E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.26534E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.17280E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.33707E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.17280E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.33707E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.45691E+16  3.77992E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28013E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  8.06220E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 144607 1.44659E+05 8.62645E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20432 2.04574E+04 1.04537E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 645280 6.44988E+05 2.83979E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 464161 4.64299E+05 5.40512E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1274480 1.27440E+06 5.87990E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1240585 1.24036E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33895 3.40489E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1274480 1.27440E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 3.46918E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.38231E-10 0.00151 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 22 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.52778E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.49651E-02 0.02978 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100290 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.07566E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100290 1.00708E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40245 4.04055E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42596 4.25816E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17449 1.77205E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100290 1.00708E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.25555E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06122E+00 0.00378 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.29033E-01 0.00521 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.75920E-01 0.00404 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53967E+00 0.00599 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.42626E-01 0.00103 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76463E-01 0.00062 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26584E+00 0.00313 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04151E+00 0.00294 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44593E+00 7.4E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97474E+02 0.00047 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04204E+00 0.00309  1.03354E+00 0.00313  7.96964E-03 0.04531 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04151E+00 0.00294 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33803E+01 0.00114 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.09391E-05 0.01543 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.85151E-02 0.01536 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.53881E-03 0.02259  2.29130E-04 0.24563  1.15780E-03 0.08177  1.13169E-03 0.09713  2.61308E-03 0.06040  1.03448E-03 0.08414  3.72621E-04 0.13331 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.46000E-01 0.04473  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.95680E-05 0.01702  2.95630E-05 0.01706  3.09607E-05 0.13008 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.08161E-05 0.01822  3.08105E-05 0.01817  3.23234E-05 0.13210 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.69646E-03 0.04854  2.89513E-04 0.40121  1.09086E-03 0.17809  1.70699E-03 0.10763  2.80269E-03 0.06271  1.38853E-03 0.10749  4.17870E-04 0.21640 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.47036E-01 0.08727  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 6.8E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.72445E-05 0.05651 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.83951E-05 0.05683 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.98073E-03 0.07600 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.17702E+02 0.03227 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.66768E-07 0.00658 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60777E-05 0.00277  1.60707E-05 0.00281  1.70796E-05 0.02678 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33305E-04 0.00728  1.33225E-04 0.00731  1.51118E-04 0.12137 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.47092E-01 0.00386  2.47011E-01 0.00373  2.60490E-01 0.06126 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.26967E+01 0.11528 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.80796E+00 0.00150  5.17501E+01 0.00184 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:07:44 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  8.04556E-01  8.04503E-01  1.20404E+00  7.98641E-01  1.18699E+00  1.20073E+00  1.20878E+00  7.91767E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50675E-01 0.00156  2.44813E-01 0.00121 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49325E-01 0.00128  7.55187E-01 0.00039 ];
DT_EFF                    (idx, [1:   4]) = [  2.59306E-01 0.00074  2.40921E-01 0.00102 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24596E-01 0.00124  2.92070E-01 0.00104 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14294E+01 0.00391  4.54780E+00 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41519E-01 3.3E-05  5.73294E-02 0.00051  1.15148E-03 0.00406  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29693E+01 0.00186  2.80313E+01 0.00315 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.27980E+01 0.00187  2.76949E+01 0.00312 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09861E+02 0.00199  6.71295E+01 0.00376 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06733E+02 0.00322  2.23347E+01 0.00402 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100126 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00126E+04 0.00423 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00126E+04 0.00423 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.79571E+02 ;
RUNNING_TIME              (idx, 1)        =  2.27026E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.49250E-01  8.21667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.21853E+01  7.06650E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  5.50000E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.27026E+01  4.51360E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.90972 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99882E+00 0.00012 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.83668E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.66956E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.13086E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19862E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.66956E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13086E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.16744E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.27944E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.16744E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.27944E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.32563E+16  3.62861E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28075E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  7.67562E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143194 1.43500E+05 8.52516E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20250 2.03203E+04 1.03836E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 637922 6.38843E+05 2.81267E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 461166 4.62175E+05 5.37018E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1262532 1.26484E+06 5.84053E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1228842 1.23092E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33690 3.39164E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1262532 1.26484E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 5.89062E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.38426E-10 0.00176 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 23 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.59722E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.45329E-02 0.01720 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100126 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.24935E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100126 1.00725E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40239 4.04616E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42734 4.28034E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17153 1.74600E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100126 1.00725E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -6.89761E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06527E+00 0.00226 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.36061E-01 0.00372 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72311E-01 0.00488 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54668E+00 0.00609 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44217E-01 0.00119 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77706E-01 0.00067 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26829E+00 0.00260 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04685E+00 0.00320 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44572E+00 0.00010 ];
FISSE                     (idx, [1:   2]) = [  1.97299E+02 0.00040 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04687E+00 0.00314  1.03901E+00 0.00322  7.84663E-03 0.05905 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04685E+00 0.00320 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33677E+01 0.00103 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.13239E-05 0.01375 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.70319E-02 0.02079 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.96785E-03 0.03219  2.20011E-04 0.21610  1.03167E-03 0.10583  1.18342E-03 0.07474  2.30340E-03 0.05885  9.14276E-04 0.13978  3.15071E-04 0.16304 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.29209E-01 0.05587  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 5.6E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.02733E-05 0.02094  3.02025E-05 0.01988  4.17095E-05 0.19480 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.16818E-05 0.01927  3.16080E-05 0.01818  4.36267E-05 0.19410 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.77275E-03 0.05609  3.25948E-04 0.26514  1.30621E-03 0.18760  1.52700E-03 0.09467  3.10718E-03 0.07226  1.19939E-03 0.10281  3.07024E-04 0.25112 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.01796E-01 0.08028  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.67923E-05 0.04992 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.80708E-05 0.05146 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.40086E-03 0.06982 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.74182E+02 0.03217 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.67922E-07 0.00632 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60831E-05 0.00263  1.60847E-05 0.00263  1.58428E-05 0.02248 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.35337E-04 0.00760  1.35297E-04 0.00790  1.48915E-04 0.12613 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.43891E-01 0.00486  2.43806E-01 0.00512  2.60768E-01 0.09548 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06991E+01 0.10714 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.82092E+00 0.00207  5.17965E+01 0.00317 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:08:27 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  7.80783E-01  7.79396E-01  1.22384E+00  8.21372E-01  1.19995E+00  1.20891E+00  1.21931E+00  7.66436E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.9E-09  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50915E-01 0.00207  2.44757E-01 0.00192 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49085E-01 0.00170  7.55243E-01 0.00062 ];
DT_EFF                    (idx, [1:   4]) = [  2.59366E-01 0.00131  2.40573E-01 0.00083 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24861E-01 0.00123  2.91733E-01 0.00091 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14845E+01 0.00343  4.55013E+00 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41548E-01 2.4E-05  5.73057E-02 0.00041  1.14581E-03 0.00326  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.30919E+01 0.00235  2.80624E+01 0.00328 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29209E+01 0.00237  2.77281E+01 0.00326 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09983E+02 0.00222  6.73190E+01 0.00366 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06926E+02 0.00531  2.23803E+01 0.00422 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100209 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00209E+04 0.00885 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00209E+04 0.00885 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.85326E+02 ;
RUNNING_TIME              (idx, 1)        =  2.34236E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.57150E-01  7.90000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.28973E+01  7.12067E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  5.66669E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.34236E+01  4.50458E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.91192 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99899E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.83946E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.53324E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  1.03040E+03 ;
TOT_SF_RATE               (idx, 1)        =  8.19867E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.53324E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.03040E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.16288E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.23180E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.16288E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.23180E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.22692E+16  3.51426E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28138E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  7.38451E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 144425 1.44628E+05 8.53895E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20412 2.04345E+04 1.04420E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 640612 6.41219E+05 2.82378E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460747 4.61466E+05 5.38502E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1266196 1.26775E+06 5.85721E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1232691 1.23407E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33505 3.36772E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1266196 1.26775E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -1.39466E-07 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.39737E-10 0.00199 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 24 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.66667E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.43803E-02 0.02035 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100209 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.54628E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100209 1.00755E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40359 4.05492E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42708 4.27663E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17142 1.74392E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100209 1.00755E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.99361E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07580E+00 0.00418 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.34582E-01 0.00584 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73278E-01 0.00511 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.52128E+00 0.00938 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.45039E-01 0.00210 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77008E-01 0.00038 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26678E+00 0.00485 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04589E+00 0.00585 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44561E+00 9.6E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97321E+02 0.00081 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04224E+00 0.00544  1.03868E+00 0.00606  7.21084E-03 0.05941 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04589E+00 0.00585 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33637E+01 0.00244 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.15702E-05 0.03201 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.63580E-02 0.02147 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14405E-03 0.03376  1.72898E-04 0.25031  1.07413E-03 0.12617  9.60392E-04 0.11666  2.49186E-03 0.07217  1.06431E-03 0.11145  3.80455E-04 0.08652 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.75312E-01 0.02904  1.33360E-02 0.0E+00  3.27307E-02 0.00025  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.50620E-01 0.00133  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.02373E-05 0.01858  3.03073E-05 0.01868  1.98507E-05 0.21699 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.15110E-05 0.01887  3.15838E-05 0.01894  2.06911E-05 0.21795 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.86302E-03 0.05776  2.16045E-04 0.30853  1.10418E-03 0.21380  1.27211E-03 0.13303  2.64352E-03 0.11831  1.12105E-03 0.14668  5.06123E-04 0.22053 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.99672E-01 0.08217  1.33360E-02 0.0E+00  3.27296E-02 0.00029  1.20780E-01 4.0E-09  3.02780E-01 3.9E-09  8.53177E-01 0.00292  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.66717E-05 0.05092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.78093E-05 0.05198 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.42391E-03 0.07992 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.37575E+02 0.04162 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.68691E-07 0.00777 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60902E-05 0.00398  1.60940E-05 0.00399  1.54792E-05 0.03499 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.35679E-04 0.00922  1.35925E-04 0.00944  9.99453E-05 0.09446 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45276E-01 0.00561  2.45075E-01 0.00545  2.81921E-01 0.07410 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09142E+01 0.05293 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.82564E+00 0.00175  5.17986E+01 0.00344 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:09:11 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.19579E+00  7.81067E-01  1.20475E+00  7.71147E-01  8.28693E-01  1.22288E+00  1.23296E+00  7.62720E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.9E-09  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51434E-01 0.00232  2.43588E-01 0.00172 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48566E-01 0.00191  7.56412E-01 0.00055 ];
DT_EFF                    (idx, [1:   4]) = [  2.59331E-01 0.00075  2.40796E-01 0.00079 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24578E-01 0.00119  2.91571E-01 0.00091 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14784E+01 0.00360  4.54935E+00 0.00093  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41582E-01 2.4E-05  5.72755E-02 0.00039  1.14235E-03 0.00280  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.30740E+01 0.00294  2.81891E+01 0.00328 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29047E+01 0.00296  2.78531E+01 0.00329 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10089E+02 0.00204  6.76752E+01 0.00353 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07337E+02 0.00494  2.23728E+01 0.00353 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99692 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.96920E+03 0.00401 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.96920E+03 0.00401 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.91112E+02 ;
RUNNING_TIME              (idx, 1)        =  2.41485E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.64850E-01  7.70000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.36135E+01  7.16117E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  5.83335E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.41484E+01  4.50773E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.91407 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99998E+00 0.00013 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84229E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.43121E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  9.55249E+02 ;
TOT_SF_RATE               (idx, 1)        =  8.19872E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.43121E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  9.55249E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.15891E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.19159E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.15891E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.19159E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.15244E+16  3.42744E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28200E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  7.16445E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143580 1.44451E+05 8.65425E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20552 2.06861E+04 1.05706E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 642125 6.45840E+05 2.84638E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 461903 4.64864E+05 5.39025E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1268160 1.27584E+06 5.86713E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1234666 1.24201E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33494 3.38340E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1268160 1.27584E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -1.50641E-07 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36205E-10 0.00127 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 25 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.73611E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.38262E-02 0.02549 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99692 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.09362E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99692 1.00709E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40168 4.05432E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42648 4.28970E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 16876 1.72692E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99692 1.00709E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 7.59610E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07526E+00 0.00361 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.31828E-01 0.00687 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73717E-01 0.00581 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53572E+00 0.00754 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.46336E-01 0.00177 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77521E-01 0.00060 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26816E+00 0.00302 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04916E+00 0.00353 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44578E+00 9.2E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97363E+02 0.00039 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04999E+00 0.00345  1.04170E+00 0.00374  7.46167E-03 0.08088 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04916E+00 0.00353 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33767E+01 0.00197 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11169E-05 0.02673 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.74403E-02 0.02075 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.53764E-03 0.02954  2.29144E-04 0.25701  1.16879E-03 0.10552  1.04715E-03 0.12268  2.60966E-03 0.04272  9.84781E-04 0.11074  4.98115E-04 0.15777 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.91976E-01 0.05968  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02864E-01 0.00028  8.49893E-01 0.00047  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.89412E-05 0.01486  2.89674E-05 0.01514  2.59005E-05 0.20668 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.03886E-05 0.01541  3.04156E-05 0.01555  2.72615E-05 0.20760 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.23688E-03 0.08692  2.49853E-04 0.39813  1.19349E-03 0.14362  1.41130E-03 0.24046  2.74962E-03 0.08768  1.20494E-03 0.12433  4.27682E-04 0.24184 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.56489E-01 0.06394  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.50243E-01 0.00089  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.63162E-05 0.04783 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.76112E-05 0.04637 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.96496E-03 0.06868 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.24406E+02 0.02903 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.65451E-07 0.01000 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60173E-05 0.00312  1.60181E-05 0.00307  1.61160E-05 0.04854 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33149E-04 0.00821  1.33217E-04 0.00791  1.25126E-04 0.14991 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.45762E-01 0.00709  2.45804E-01 0.00729  2.40166E-01 0.10081 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  9.93137E+00 0.12164 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.79194E+00 0.00148  5.17475E+01 0.00345 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:09:54 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.20568E+00  7.80054E-01  1.21116E+00  8.11293E-01  7.78937E-01  1.22505E+00  1.21909E+00  7.68719E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 3.9E-09 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51020E-01 0.00091  2.44427E-01 0.00192 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48980E-01 0.00075  7.55573E-01 0.00062 ];
DT_EFF                    (idx, [1:   4]) = [  2.59483E-01 0.00092  2.40896E-01 0.00046 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.25217E-01 0.00087  2.92193E-01 0.00090 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.13597E+01 0.00351  4.54460E+00 0.00077  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41588E-01 3.0E-05  5.72666E-02 0.00050  1.14559E-03 0.00170  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.31376E+01 0.00247  2.80765E+01 0.00434 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.29652E+01 0.00249  2.77413E+01 0.00440 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09894E+02 0.00148  6.71987E+01 0.00365 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06839E+02 0.00263  2.22814E+01 0.00528 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100180 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00180E+04 0.00552 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00180E+04 0.00552 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.96908E+02 ;
RUNNING_TIME              (idx, 1)        =  2.48747E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.73217E-01  8.36667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.43306E+01  7.17117E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  6.00000E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.48746E+01  4.51186E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.91601 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99832E+00 0.00012 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84516E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.35469E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  8.98935E+02 ;
TOT_SF_RATE               (idx, 1)        =  8.19877E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.35469E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.98935E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.15538E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.15693E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.15538E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.15693E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.09599E+16  3.36111E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28262E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  6.99726E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 143794 1.44027E+05 8.52580E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20154 2.02174E+04 1.03311E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 640878 6.41348E+05 2.82242E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 461646 4.62352E+05 5.35996E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1266472 1.26794E+06 5.83077E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1232902 1.23419E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33570 3.37561E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1266472 1.26794E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -1.76951E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37132E-10 0.00140 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 26 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.80556E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.40237E-02 0.03050 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100180 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.43523E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100180 1.00744E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 39970 4.01792E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42933 4.29679E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17277 1.75964E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100180 1.00744E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -3.91447E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05226E+00 0.00262 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.33860E-01 0.00541 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.75042E-01 0.00372 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.56536E+00 0.00473 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44035E-01 0.00129 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76306E-01 0.00043 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27531E+00 0.00373 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05093E+00 0.00461 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44584E+00 0.00014 ];
FISSE                     (idx, [1:   2]) = [  1.97195E+02 0.00040 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04778E+00 0.00469  1.04314E+00 0.00463  7.78435E-03 0.04307 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05093E+00 0.00461 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33190E+01 0.00171 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.29361E-05 0.02197 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.84127E-02 0.02899 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.38392E-03 0.03110  2.38825E-04 0.26884  1.07053E-03 0.05758  1.10232E-03 0.08367  2.43341E-03 0.04851  9.82191E-04 0.09444  5.56643E-04 0.13359 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  5.23568E-01 0.05138  1.33360E-02 0.0E+00  3.27273E-02 0.00036  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85545E+00 0.00086 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.94522E-05 0.01762  2.94673E-05 0.01646  2.73274E-05 0.26427 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.08552E-05 0.01735  3.08713E-05 0.01621  2.86386E-05 0.26659 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.77920E-03 0.06466  2.35812E-04 0.41568  1.18259E-03 0.13646  1.37595E-03 0.09101  2.99455E-03 0.09598  1.30622E-03 0.12839  6.84076E-04 0.29640 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.27412E-01 0.09917  1.33360E-02 5.7E-09  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85844E+00 0.00190 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.63359E-05 0.04888 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.76146E-05 0.05046 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.94149E-03 0.05882 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.62551E+02 0.01621 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.70215E-07 0.01103 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60219E-05 0.00276  1.60243E-05 0.00281  1.56467E-05 0.04430 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.35083E-04 0.01097  1.35101E-04 0.01086  1.36007E-04 0.15513 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.47027E-01 0.00483  2.46885E-01 0.00470  2.72785E-01 0.08562 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.23741E+01 0.12105 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.82281E+00 0.00119  5.16660E+01 0.00393 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:10:38 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.25314E+00  7.40870E-01  7.36174E-01  1.14412E+00  7.51276E-01  1.26488E+00  1.26301E+00  8.46526E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.9E-09  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50723E-01 0.00205  2.43788E-01 0.00171 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49277E-01 0.00168  7.56212E-01 0.00055 ];
DT_EFF                    (idx, [1:   4]) = [  2.59158E-01 0.00102  2.40180E-01 0.00092 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24194E-01 0.00099  2.90950E-01 0.00074 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14902E+01 0.00268  4.56510E+00 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41528E-01 3.2E-05  5.73270E-02 0.00053  1.14502E-03 0.00272  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.27928E+01 0.00308  2.80433E+01 0.00459 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.26205E+01 0.00310  2.77096E+01 0.00462 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09691E+02 0.00283  6.75295E+01 0.00489 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06636E+02 0.00517  2.23444E+01 0.00584 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99982 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99820E+03 0.00675 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99820E+03 0.00675 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.02719E+02 ;
RUNNING_TIME              (idx, 1)        =  2.56030E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.81850E-01  8.63334E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.50494E+01  7.18850E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  6.16666E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  3.33309E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.56030E+01  4.51327E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.91777 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99889E+00 0.00013 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84779E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.29716E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  8.56639E+02 ;
TOT_SF_RATE               (idx, 1)        =  8.19881E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.29716E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.56639E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.15218E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.12641E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.15218E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.12641E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.05297E+16  3.31003E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28325E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  6.86943E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142635 1.43099E+05 8.39995E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20182 2.02564E+04 1.03510E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 640054 6.41881E+05 2.83013E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 461124 4.62747E+05 5.35867E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1263995 1.26798E+06 5.82919E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1230631 1.23438E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33364 3.36011E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1263995 1.26798E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 2.42144E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37219E-10 0.00210 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 27 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.87500E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.50150E-02 0.02506 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99982 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.09574E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99982 1.00710E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40271 4.05329E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42484 4.26023E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17227 1.75744E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99982 1.00710E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.00408E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06480E+00 0.00298 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.34356E-01 0.00415 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.73388E-01 0.00495 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53152E+00 0.00601 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43310E-01 0.00169 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77410E-01 0.00060 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26414E+00 0.00252 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04200E+00 0.00374 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44588E+00 0.00012 ];
FISSE                     (idx, [1:   2]) = [  1.97312E+02 0.00039 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03963E+00 0.00442  1.03485E+00 0.00393  7.15674E-03 0.05933 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04200E+00 0.00374 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33534E+01 0.00162 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.18126E-05 0.02072 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.84472E-02 0.02697 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.61787E-03 0.02672  2.03330E-04 0.19549  1.21676E-03 0.06654  1.10917E-03 0.14379  2.44794E-03 0.05724  1.15050E-03 0.08824  4.90160E-04 0.09963 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.99791E-01 0.04024  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 6.8E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.03109E-05 0.01986  3.02315E-05 0.01941  3.73564E-05 0.25238 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.15165E-05 0.02107  3.14338E-05 0.02061  3.88460E-05 0.25192 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.24074E-03 0.05179  2.00353E-04 0.36406  1.08273E-03 0.14538  1.28308E-03 0.12471  2.81551E-03 0.09052  1.27450E-03 0.12683  5.84565E-04 0.16485 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.29379E-01 0.06052  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02780E-01 5.6E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.67495E-05 0.05245 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.78162E-05 0.05273 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.00316E-03 0.08178 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.20980E+02 0.03819 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.64866E-07 0.01059 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.59304E-05 0.00263  1.59297E-05 0.00271  1.64212E-05 0.05058 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33179E-04 0.01042  1.33187E-04 0.01013  1.31890E-04 0.09722 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44603E-01 0.00592  2.44714E-01 0.00608  2.28891E-01 0.07639 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08314E+01 0.09491 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.79466E+00 0.00145  5.17252E+01 0.00313 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:11:21 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.21178E+00  7.73467E-01  7.67386E-01  7.67653E-01  1.20720E+00  1.21898E+00  1.23781E+00  8.15715E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.50133E-01 0.00134  2.44686E-01 0.00122 ];
DT_FRAC                   (idx, [1:   4]) = [  5.49867E-01 0.00110  7.55314E-01 0.00039 ];
DT_EFF                    (idx, [1:   4]) = [  2.59531E-01 0.00081  2.40802E-01 0.00076 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24565E-01 0.00087  2.92099E-01 0.00079 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14518E+01 0.00433  4.54593E+00 0.00109  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41544E-01 1.6E-05  5.73107E-02 0.00028  1.14553E-03 0.00326  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.28346E+01 0.00212  2.79034E+01 0.00362 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.26610E+01 0.00213  2.75714E+01 0.00365 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09590E+02 0.00173  6.68190E+01 0.00347 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06296E+02 0.00366  2.21914E+01 0.00344 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 99820 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.98200E+03 0.00483 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.98200E+03 0.00483 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.08480E+02 ;
RUNNING_TIME              (idx, 1)        =  2.63247E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.89900E-01  8.05000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.57622E+01  7.12800E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  6.33335E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  5.00003E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.63246E+01  4.51168E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.91956 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  8.00036E+00 0.00012 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.85022E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.25376E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  8.24775E+02 ;
TOT_SF_RATE               (idx, 1)        =  8.19885E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.25376E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.24775E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.14923E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.09898E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.14923E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.09898E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  2.01995E+16  3.27033E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28387E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  6.77089E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142531 1.43304E+05 8.38048E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20056 2.01792E+04 1.03116E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 634245 6.37219E+05 2.80112E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 456524 4.58930E+05 5.34886E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1253356 1.25963E+06 5.81589E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1220215 1.22617E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33141 3.34628E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1253356 1.25963E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 5.82077E-09 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37392E-10 0.00110 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 28 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  1.94444E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.36219E-02 0.02588 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 99820 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.06302E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 99820 1.00706E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40105 4.04372E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42383 4.25822E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17332 1.76870E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 99820 1.00706E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -5.36966E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.04884E+00 0.00286 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.35129E-01 0.00236 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.71053E-01 0.00435 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.58860E+00 0.00456 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.43017E-01 0.00155 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76409E-01 0.00059 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.26549E+00 0.00267 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04166E+00 0.00299 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44623E+00 9.9E-05 ];
FISSE                     (idx, [1:   2]) = [  1.97314E+02 0.00039 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04580E+00 0.00353  1.03390E+00 0.00293  7.75958E-03 0.05462 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04166E+00 0.00299 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33164E+01 0.00106 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.29732E-05 0.01405 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.01549E-01 0.02193 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.68510E-03 0.04552  2.60284E-04 0.18378  1.12116E-03 0.11306  1.02995E-03 0.08021  2.71454E-03 0.07653  1.04745E-03 0.06778  5.11725E-04 0.13855 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.98450E-01 0.05640  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02885E-01 0.00035  8.49961E-01 0.00055  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.98599E-05 0.01907  2.98453E-05 0.01966  3.26509E-05 0.17662 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.12289E-05 0.01971  3.12142E-05 0.02036  3.41024E-05 0.17579 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.58718E-03 0.05109  3.06882E-04 0.25115  1.35509E-03 0.11459  1.07422E-03 0.11440  2.98202E-03 0.07760  1.28210E-03 0.15828  5.86875E-04 0.26888 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.10071E-01 0.10653  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20780E-01 0.0E+00  3.02941E-01 0.00053  8.50085E-01 0.00070  2.85300E+00 5.6E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55449E-05 0.05031 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67045E-05 0.04970 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.92253E-03 0.06572 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.30559E+02 0.02482 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.64968E-07 0.00894 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60552E-05 0.00243  1.60502E-05 0.00253  1.67231E-05 0.03728 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33609E-04 0.00925  1.33713E-04 0.00965  1.17169E-04 0.08729 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.43315E-01 0.00560  2.43156E-01 0.00552  2.68256E-01 0.03016 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02874E+01 0.11508 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.82574E+00 0.00203  5.15682E+01 0.00320 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:12:05 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  7.84090E-01  8.16676E-01  7.63697E-01  7.75624E-01  1.20920E+00  1.22150E+00  1.22970E+00  1.19951E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.49885E-01 0.00106  2.44401E-01 0.00161 ];
DT_FRAC                   (idx, [1:   4]) = [  5.50115E-01 0.00086  7.55599E-01 0.00052 ];
DT_EFF                    (idx, [1:   4]) = [  2.59229E-01 0.00092  2.40753E-01 0.00116 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24292E-01 0.00129  2.91792E-01 0.00127 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15028E+01 0.00125  4.55618E+00 0.00188  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41534E-01 2.8E-05  5.73218E-02 0.00044  1.14435E-03 0.00312  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.29691E+01 0.00216  2.79955E+01 0.00409 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.27978E+01 0.00217  2.76611E+01 0.00405 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10011E+02 0.00149  6.71376E+01 0.00446 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06531E+02 0.00198  2.22865E+01 0.00494 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100060 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00060E+04 0.00369 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00060E+04 0.00369 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.14257E+02 ;
RUNNING_TIME              (idx, 1)        =  2.70486E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.98083E-01  8.18333E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.64772E+01  7.14933E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  6.50001E-04  1.66655E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  5.00003E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.70485E+01  4.51111E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.92121 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99933E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.85233E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.22087E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  8.00676E+02 ;
TOT_SF_RATE               (idx, 1)        =  8.19889E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.22087E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.00676E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.14646E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.07385E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.14646E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.07385E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  1.99436E+16  3.23908E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28449E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  6.69416E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 142410 1.42833E+05 8.36580E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 19882 1.99342E+04 1.01864E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 639355 6.40916E+05 2.82158E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 460539 4.61950E+05 5.34429E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1262186 1.26563E+06 5.81197E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1228729 1.23198E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33457 3.36566E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1262186 1.26563E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 -5.96046E-08 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.36496E-10 0.00128 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 29 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  2.01389E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.43284E-02 0.03718 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100060 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.36440E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100060 1.00736E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40028 4.02839E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42888 4.30029E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17144 1.74497E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100060 1.00736E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -1.13505E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06892E+00 0.00329 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.35271E-01 0.00373 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.72470E-01 0.00494 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.55918E+00 0.00550 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.45261E-01 0.00161 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76624E-01 0.00058 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27420E+00 0.00322 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05182E+00 0.00271 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44593E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97190E+02 0.00048 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05241E+00 0.00261  1.04405E+00 0.00245  7.77555E-03 0.05561 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05182E+00 0.00271 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33219E+01 0.00182 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.28485E-05 0.02370 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.92021E-02 0.02510 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.07330E-03 0.02792  2.28675E-04 0.15724  9.03336E-04 0.07489  1.02367E-03 0.11451  2.46203E-03 0.06900  1.03644E-03 0.08006  4.19146E-04 0.16129 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  4.92299E-01 0.05763  1.33360E-02 0.0E+00  3.27390E-02 0.0E+00  1.20812E-01 0.00026  3.02780E-01 0.0E+00  8.49490E-01 3.9E-09  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.97257E-05 0.02392  2.97849E-05 0.02375  2.23430E-05 0.26762 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.12744E-05 0.02253  3.13364E-05 0.02230  2.35434E-05 0.26919 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.87091E-03 0.05610  2.96559E-04 0.17068  1.10191E-03 0.14435  1.33843E-03 0.08959  3.14924E-03 0.11359  1.49895E-03 0.11172  4.85820E-04 0.19729 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  4.89689E-01 0.06897  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 3.9E-09  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.61309E-05 0.04394 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.75122E-05 0.04476 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.49090E-03 0.07870 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.44974E+02 0.04379 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.65610E-07 0.00795 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60552E-05 0.00215  1.60535E-05 0.00220  1.61456E-05 0.04383 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.33704E-04 0.00720  1.33834E-04 0.00678  1.13129E-04 0.16635 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.44932E-01 0.00445  2.44820E-01 0.00470  2.66280E-01 0.08176 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07103E+01 0.08497 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81550E+00 0.00148  5.15819E+01 0.00310 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Feb 16 2024 16:38:49' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'coreSupport.txt' ;
WORKING_DIRECTORY         (idx, [1:  63]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/coupled/serpent' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Mar 21 11:45:02 2024' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Mar 21 12:12:40 2024' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 10 ;
SKIP                      (idx, 1)        = 5 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1711039502394 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 1 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 0 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 0 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 1 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 0 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 0 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  7.79305E-01  7.65768E-01  7.55268E-01  8.12615E-01  1.21623E+00  1.21815E+00  1.23595E+00  1.21671E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  51]) = '/home/grads/z/zhughes/Serpent2/src/s2v0_endfb71.dec' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
PHOTON_PHYS_DIRECTORY     (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/xsdata/photon_data/' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  5.00000E-02 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.51120E-01 0.00145  2.44238E-01 0.00160 ];
DT_FRAC                   (idx, [1:   4]) = [  5.48880E-01 0.00119  7.55762E-01 0.00052 ];
DT_EFF                    (idx, [1:   4]) = [  2.59316E-01 0.00092  2.40557E-01 0.00093 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.24885E-01 0.00128  2.91629E-01 0.00088 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.15119E+01 0.00431  4.56134E+00 0.00104  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.41575E-01 3.2E-05  5.72759E-02 0.00052  1.14952E-03 0.00223  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.31766E+01 0.00124  2.82478E+01 0.00327 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.30056E+01 0.00125  2.79089E+01 0.00326 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.10148E+02 0.00191  6.77913E+01 0.00327 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.07158E+02 0.00437  2.24709E+01 0.00314 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 10 ;
SIMULATED_HISTORIES       (idx, 1)        = 100155 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00155E+04 0.00422 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00155E+04 0.00422 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.18914E+02 ;
RUNNING_TIME              (idx, 1)        =  2.76327E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  2.51283E-01  2.51283E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  3.06383E-01  8.30000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.70519E+01  5.74717E-01  7.14367E-01 ] ;
BURNUP_CYCLE_TIME         (idx, [1:   2]) = [  6.66670E-04  1.66694E-05 ] ;
BATEMAN_SOLUTION_TIME     (idx, [1:   2]) = [  5.00003E-05  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.76326E+01  4.49770E+01 ] ;
CPU_USAGE                 (idx, 1)        = 7.92230 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.99852E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.85355E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.10 ;
ALLOC_MEMSIZE             (idx, 1)        = 1547.95 ;
MEMSIZE                   (idx, 1)        = 1464.84 ;
XS_MEMSIZE                (idx, 1)        = 1365.95 ;
MAT_MEMSIZE               (idx, 1)        = 25.37 ;
RES_MEMSIZE               (idx, 1)        = 2.90 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 70.62 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 83.11 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 120 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 1137900 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Photon energy grid:

PHOTON_ERG_NE             (idx, 1)        = 12954 ;
PHOTON_EMIN               (idx, 1)        =  1.00000E-03 ;
PHOTON_EMAX               (idx, 1)        =  1.00000E+02 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 48 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 230 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 84 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 108 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 38 ;
TOT_REA_CHANNELS          (idx, 1)        = 2423 ;
TOT_TRANSMU_REA           (idx, 1)        = 292 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Photon physics options:

COMPTON_EKN               (idx, 1)        =  1.00000E+37 ;
COMPTON_DOPPLER           (idx, 1)        = 1 ;
COMPTON_EANG              (idx, 1)        = 0 ;
PHOTON_TTB                (idx, 1)        = 1 ;

% Photon production:

PHOTON_SAMPLING_MODE      (idx, 1)        = 1 ;
PHOTON_SAMPLING_FAIL      (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 3 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 1 0 0 0 0 1 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.19581E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  7.82355E+02 ;
TOT_SF_RATE               (idx, 1)        =  8.19893E+01 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.19581E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  7.82355E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  1.14384E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  9.05045E+06 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.14384E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.05045E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  1.97431E+16  3.21415E+02 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28510E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  6.63367E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Photon balance (particles/weight):

BALA_SRC_PHOTON_SRC       (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TTB       (idx, [1:   3]) = [ 144900 1.45099E+05 8.55511E+03 ] ;
BALA_SRC_PHOTON_ANNIH     (idx, [1:   3]) = [ 20068 2.01348E+04 1.02889E+04 ] ;
BALA_SRC_PHOTON_FLUOR     (idx, [1:   3]) = [ 643761 6.44606E+05 2.83893E+04 ] ;
BALA_SRC_PHOTON_NREA      (idx, [1:   3]) = [ 464604 4.65511E+05 5.40236E+05 ] ;
BALA_SRC_PHOTON_VR        (idx, [1:   3]) = [ 0 0.00000E+00 0.00000E+00 ] ;
BALA_SRC_PHOTON_TOT       (idx, [1:   3]) = [ 1273333 1.27535E+06 5.87470E+05 ] ;

BALA_LOSS_PHOTON_CAPT     (idx, [1:   2]) = [ 1239391 1.24123E+06 ] ;
BALA_LOSS_PHOTON_LEAK     (idx, [1:   2]) = [ 33942 3.41235E+04 ] ;
BALA_LOSS_PHOTON_CUT      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_ERR      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_PHOTON_TOT      (idx, [1:   2]) = [ 1273333 1.27535E+06 ] ;

BALA_PHOTON_DIFF          (idx, [1:   2]) = [ 0 1.73459E-07 ] ;

% Normalized total reaction rates (photons):

TOT_PHOTON_LEAKRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_CUTRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PHOTOELE_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PAIRPROD_CAPT_RATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_LOSSRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_SRCRATE        (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_RR             (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_FLUX           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_PHOTON_HEATRATE       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Analog mean photon lifetime:

ANA_LIFETIME              (idx, [1:   2]) = [  5.37513E-10 0.00186 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 1 ;
BURN_STEP                 (idx, 1)        = 30 ;
BURN_RANDOMIZE_DATA       (idx, [1:   3]) = [ 0 0 0 ] ;
BURNUP                    (idx, [1:   2]) = [  2.29125E-01  2.34829E-01 ] ;
BURN_DAYS                 (idx, [1:   2]) = [  2.08333E-01  6.94444E-03 ] ;
FIMA                      (idx, [1:   3]) = [  2.44478E-04  1.06637E+23  4.36075E+26 ] ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39143E-02 0.02250 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100155 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.28063E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100155 1.00728E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 40042 4.02406E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 42991 4.30545E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 17122 1.74330E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100155 1.00728E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.61934E-10 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_ABSRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_SRCRATE               (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
INI_FMASS                 (idx, 1)        =  1.70395E+02 ;
TOT_FMASS                 (idx, 1)        =  1.70353E+02 ;
INI_BURN_FMASS            (idx, 1)        =  1.70395E+02 ;
TOT_BURN_FMASS            (idx, 1)        =  1.70353E+02 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07900E+00 0.00403 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.36318E-01 0.00507 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.74618E-01 0.00414 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.51183E+00 0.00624 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.45464E-01 0.00134 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.76590E-01 0.00054 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.27533E+00 0.00327 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05298E+00 0.00300 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44570E+00 0.00011 ];
FISSE                     (idx, [1:   2]) = [  1.97351E+02 0.00034 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05340E+00 0.00366  1.04641E+00 0.00300  6.57082E-03 0.07421 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05298E+00 0.00300 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33493E+01 0.00157 ];
IMP_ALF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.19430E-05 0.02174 ];
IMP_EALF                  (idx, [1:   2]) = [  2.00000E+01 0.0E+00 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73032E-02 0.02438 ];
IMP_AFGE                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 6 ;
FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14827E-03 0.03593  2.56447E-04 0.28454  1.17605E-03 0.07835  1.06484E-03 0.11493  2.13217E-03 0.06438  1.00635E-03 0.06166  5.12412E-04 0.11765 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  5.14700E-01 0.05809  1.33360E-02 0.0E+00  3.27390E-02 3.9E-09  1.20780E-01 0.0E+00  3.02780E-01 0.0E+00  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.96149E-05 0.01334  2.96465E-05 0.01343  2.80372E-05 0.34851 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.11930E-05 0.01297  3.12269E-05 0.01325  2.93772E-05 0.34551 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.21864E-03 0.07590  2.77235E-04 0.42210  1.04438E-03 0.18779  9.98536E-04 0.21088  2.47875E-03 0.11470  9.43887E-04 0.11351  4.75853E-04 0.15483 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  5.05268E-01 0.07924  1.33360E-02 8.2E-09  3.27390E-02 6.8E-09  1.20780E-01 6.8E-09  3.02780E-01 3.9E-09  8.49490E-01 0.0E+00  2.85300E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55807E-05 0.05564 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.69756E-05 0.05738 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.32985E-03 0.07775 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.44272E+02 0.02899 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.67907E-07 0.00587 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.61083E-05 0.00336  1.61097E-05 0.00342  1.60763E-05 0.04182 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.34435E-04 0.00782  1.34682E-04 0.00758  9.85289E-05 0.12964 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.46865E-01 0.00378  2.46823E-01 0.00377  2.54243E-01 0.06196 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.16344E+01 0.07420 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  7.81010E+00 0.00162  5.18234E+01 0.00164 ];

