#!/usr/bin/env python
"""
Script to generate the mesh for the neutronics region of GeN-Foam.
Use the Salome API to generate UNV file.
The script relies on a core map file with 10 different fuel enrichement 
loadings. 

Author: Eymeric Simonnot, EPFL, First version, 04/2023
        Thomas Guilbaud, EPFL, Refactoring, 29/06/2023
"""

#=============================================================================*
# Imports
#=============================================================================*

import sys
import salome
import numpy as np

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()

from SketchAPI import *
from salome.shaper import model

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder

#=============================================================================*
# Main
#=============================================================================*

# Units
inch = 2.54 # m


coreDiameter = 35*inch

# Dimensions of the hexagonal face of the element
flatToFlat = 0.75 * inch # 1.905 cm
summitToSummit = flatToFlat * 2/np.sqrt(3)
side = summitToSummit/2

### Number of meshes in z-direction for each par of the model
nzFuelElement   = 10
nzNozzleChamber = 2
nzcoreSupport   = 1
nzsupportPlate  = 1
nzupperPlenum   = 2
nzGap           = 1

### Dimensions found in : 1965 - Survey description of the design and testing of Kiwi-B-4E-301
lengthFuelElement   = 52.0 * inch
lengthNozzleChamber =  8.0 * inch  ## arbitrary length to study what happens in this zone
lengthcoreSupport   =  1.2 * inch
lengthsupportPlate  =  4.5 * inch 
lengthupperPlenum   =  3.0 * inch  ## arbitrary length to study what happens in this zone
lengthGap           =  1.2 * inch


# Absolute path of the lattice map file
# This file contains the pattern of the elements, make sure the path to coreMap.txt is correct
latticeMapFilename = '/home/grads/z/zhughes/kiwiWork/serpentFiles/coreMapLoadings.txt'


#=============================================================================*
# Useful functions
#=============================================================================*

def removeFacesByName(mesh, name: str) -> None:
  """
  Remove all groups corresponding to a name
  """
  for group in mesh.GetGroups():
    if (group.GetName() == name):
      mesh.RemoveGroup(group)


def meshElement(mesh, lengthElement: float, nzElement: int) -> None:
  """
  mesh: face of the mesh to be extruded
  lengthElement: length of the part of the model in the z-direction
  nzElement: number of meshes in the z-direction

  return an extruded mesh along the Z-axis
  """
  NETGEN_1D_2D = mesh.Triangle(algo=smeshBuilder.NETGEN_1D2D)
  NETGEN_2D_Parameters_1 = NETGEN_1D_2D.Parameters()
  NETGEN_2D_Parameters_1.SetMaxSize( 0.6 )                         
  NETGEN_2D_Parameters_1.SetMinSize( 0.3 )                        
  NETGEN_2D_Parameters_1.SetSecondOrder( 0 )
  NETGEN_2D_Parameters_1.SetOptimize( 1 )
  NETGEN_2D_Parameters_1.SetFineness( 1 )
  NETGEN_2D_Parameters_1.SetChordalError( -1 )
  NETGEN_2D_Parameters_1.SetChordalErrorEnabled( 0 )
  NETGEN_2D_Parameters_1.SetUseSurfaceCurvature( 1 )
  NETGEN_2D_Parameters_1.SetFuseEdges( 1 )
  NETGEN_2D_Parameters_1.SetWorstElemMeasure( 0 )

  outlet = mesh.GroupOnGeom(Group_1_1, 'outlet', SMESH.FACE)
  edges_outlet = mesh.GroupOnGeom(Group_2_1, 'edges_outlet', SMESH.EDGE)
  Regular_1D = mesh.Segment(geom=Group_2_1)
  Number_of_Segments_1 = Regular_1D.NumberOfSegments(1) # Number of meshes on a segment of the hexagon
  isDone = mesh.Compute()

  [ outlet, edges_outlet ] = mesh.GetGroups()

  [ extrusion, extruded_Inlet, inlet, edges_inlet ] = mesh.ExtrusionSweepObjects( 
    [ mesh ], 
    [ mesh ], 
    [ mesh ], 
    [ 0, 0, lengthElement/nzElement ], 
    nzElement, True, [  ], False, [  ], [  ], False
  )

  [ outlet, edges_outlet, extrusion, inlet, extruded_Inlet, edges_inlet ] = mesh.GetGroups()
  Sub_mesh_1 = Regular_1D.GetSubMesh()

  ## Set names of Mesh objects
  extruded_Inlet.SetName('baffle')
  inlet.SetName('inlet')

  return(mesh)


def placeMesh(mesh, z: float=0, key='f', isAllNonZero: bool=False, name: str="mesh"):
  """
  mesh: part of the model one want to mesh
  z: z-coordinate of the bottom of the mesh
  key : element in coreMap.txt
  isAllNonZero : true --> places a mesh on each element in coreMap.txt 
                 false --> places a mesh on the specified key
  return concatenated mesh
  """
  # First position in coreMap.txt corresponds to (x,y,z) in Salome
  x, y = 0, flatToFlat/2
  n = 1     # Variable n used to store the number of the line of coreMap.txt 
  num = 0   # Variable to count the number of fuelElements
  meshElements=[]  # Creating list to store each type of element

  # Opening of coreMap.txt
  with open(latticeMapFilename, 'r') as file:
    for line in file.readlines():
      for element in line.split():
        isPlaced = False
        if (element in key and element != "0" or (isAllNonZero and element != "0")):
          meshTranslated = mesh.TranslateObjectMakeMesh(
            mesh, [x, y, z], 1, name
          )
          isPlaced = True
          num += 1

        # Special name to remove the baffles
        if (element in ["f", "c"] and isPlaced):
          # Removing boundary groups to make it work with ideasUNVToFoam
          removeFacesByName(meshTranslated, "baffle")

        if isPlaced:
          meshElements.append(meshTranslated.GetMesh())
        
        # Updating y to go through the lines of coreMap.txt  
        y += flatToFlat

      # Updating x to go through the columns of coreMap.txt
      x += summitToSummit/2 + side/2
      n += 1

      # To place the first element of a line according to its location in coreMap.txt
      if (n%2)==0:
        y = 0
      else:
        y = flatToFlat/2
    

  # Merging of all the groups stored in the meshElements list
  meshConcat = smesh.Concatenate(
    meshElements, True, True, 1e-05, False, name=name
  )

  # Rename cellZone
  for group in meshConcat.GetGroups():
    if (group.GetName() == "outlet_extruded"):
      group.SetName(name)

  # Remove translated meshes
  for part in meshElements:
    smesh.RemoveMesh(part)
  
  print(f"Number of element place for key {key}: {num}")
  
  return(meshConcat.GetMesh())


#=============================================================================*
# Create fuel element hexagonal patterns

model.begin()
partSet = model.moduleDocument()

### Create Part
Part_1 = model.addPart(partSet)
Part_1_doc = Part_1.document()

### Create Sketch
Sketch_1 = model.addSketch(Part_1_doc, model.defaultPlane("XOY"))

SketchProjection_1 = Sketch_1.addProjection(model.selection("VERTEX", "PartSet/Origin"), False)
SketchPoint_1 = SketchProjection_1.createdFeature()

### Create SketchCircle
SketchCircle_1 = Sketch_1.addCircle(0, 0, 71) # X, Y, Radius
SketchCircle_1.setAuxiliary(True)
Sketch_1.setCoincident(SketchCircle_1.center(), SketchAPI_Point(SketchPoint_1).coordinates())

### Create polyline representing the hexagon
SketchLine_1 = Sketch_1.addLine(35.63867741653722, 61.72799999999999, -35.63867741653722, 61.72799999999999)
Sketch_1.setCoincident(SketchLine_1.startPoint(), SketchCircle_1.results()[1])
Sketch_1.setCoincident(SketchLine_1.endPoint(), SketchCircle_1.results()[1])
SketchLine_2 = Sketch_1.addLine(-35.63867741653722, 61.72799999999999, -71.27735483307444, -1.346712824871863e-15)
Sketch_1.setCoincident(SketchLine_1.endPoint(), SketchLine_2.startPoint())
Sketch_1.setCoincident(SketchLine_2.endPoint(), SketchCircle_1.results()[1])
SketchLine_3 = Sketch_1.addLine(-71.27735483307444, -1.346712824871863e-15, -35.63867741653722, -61.72800000000001)
Sketch_1.setCoincident(SketchLine_2.endPoint(), SketchLine_3.startPoint())
Sketch_1.setCoincident(SketchLine_3.endPoint(), SketchCircle_1.results()[1])
SketchLine_4 = Sketch_1.addLine(-35.63867741653722, -61.72800000000001, 35.63867741653723, -61.72799999999999)
Sketch_1.setCoincident(SketchLine_3.endPoint(), SketchLine_4.startPoint())
Sketch_1.setCoincident(SketchLine_4.endPoint(), SketchCircle_1.results()[1])
SketchLine_5 = Sketch_1.addLine(35.63867741653723, -61.72799999999999, 71.27735483307444, -2.33036364153576e-15)
Sketch_1.setCoincident(SketchLine_4.endPoint(), SketchLine_5.startPoint())
Sketch_1.setCoincident(SketchLine_5.endPoint(), SketchCircle_1.results()[1])
SketchLine_6 = Sketch_1.addLine(71.27735483307444, -2.33036364153576e-15, 35.63867741653722, 61.72799999999999)
Sketch_1.setCoincident(SketchLine_5.endPoint(), SketchLine_6.startPoint())
Sketch_1.setCoincident(SketchLine_1.startPoint(), SketchLine_6.endPoint())

### Set constraints

# Horizontal
Sketch_1.setHorizontal(SketchLine_1.result())

# Set equal distance of the edges
Sketch_1.setEqual(SketchLine_1.result(), SketchLine_6.result())
Sketch_1.setEqual(SketchLine_1.result(), SketchLine_5.result())
Sketch_1.setEqual(SketchLine_1.result(), SketchLine_4.result())
Sketch_1.setEqual(SketchLine_1.result(), SketchLine_3.result())
Sketch_1.setEqual(SketchLine_1.result(), SketchLine_2.result())

# Set flat to flat distance
Sketch_1.setDistance(SketchLine_1.result(), SketchLine_3.endPoint(), flatToFlat, True)

model.do()


#=============================================================================*

### Create Face
Face_1 = model.addFace(Part_1_doc, [model.selection("FACE", "Sketch_1/Face-SketchLine_1f-SketchLine_2f-SketchLine_3f-SketchLine_4f-SketchLine_5f-SketchLine_6f")])

### Create Group
Group_1 = model.addGroup(Part_1_doc, "Faces", [model.selection("FACE", "Face_1_1")])

### Create Group
Group_2_objects = [
  model.selection("EDGE", "Face_1_1/Modified_Edge&Sketch_1/SketchLine_6"),
  model.selection("EDGE", "Face_1_1/Modified_Edge&Sketch_1/SketchLine_5"),
  model.selection("EDGE", "Face_1_1/Modified_Edge&Sketch_1/SketchLine_1"),
  model.selection("EDGE", "Face_1_1/Modified_Edge&Sketch_1/SketchLine_4"),
  model.selection("EDGE", "Face_1_1/Modified_Edge&Sketch_1/SketchLine_2"),
  model.selection("EDGE", "Face_1_1/Modified_Edge&Sketch_1/SketchLine_3")
]
Group_2 = model.addGroup(Part_1_doc, "Edges", Group_2_objects)

model.end()

model.publishToShaperStudy()
import SHAPERSTUDY
Face_1_1, Group_1_1, Group_2_1, = SHAPERSTUDY.shape(model.featureStringId(Face_1))


#=============================================================================* 
# CREATION OF THE BASIS OF EACH PART
#=============================================================================*
smesh = smeshBuilder.New()

# upperPlenumMesh = smesh.Mesh(Face_1_1, 'upperPlenumMesh')
coreSupportMesh = smesh.Mesh(Face_1_1, 'coreSupportMesh')
# gapMesh = smesh.Mesh(Face_1_1, 'gapMesh')
fuelElementMesh = smesh.Mesh(Face_1_1, 'fuelElementMesh')
supportPlateMesh = smesh.Mesh(Face_1_1, 'supportPlateMesh')
# nozzleChamberMesh = smesh.Mesh(Face_1_1, 'nozzleChamberMesh')


#=============================================================================*
# MESHES CREATION
#=============================================================================*

# upperPlenumMesh = meshElement(upperPlenumMesh, lengthupperPlenum, nzupperPlenum)
coreSupportMesh = meshElement(coreSupportMesh, lengthcoreSupport, nzcoreSupport)
# gapMesh = meshElement(gapMesh, lengthGap, nzGap)
fuelElementMesh = meshElement(fuelElementMesh, lengthFuelElement, nzFuelElement)
supportPlateMesh = meshElement(supportPlateMesh, lengthsupportPlate, nzsupportPlate)
# nozzleChamberMesh = meshElement(nozzleChamberMesh, lengthNozzleChamber, nzNozzleChamber)


# Remove double faces to avoid overlaping in UNV
removeFacesByName(supportPlateMesh, "outlet")
removeFacesByName(fuelElementMesh, "inlet")
removeFacesByName(fuelElementMesh, "outlet")
removeFacesByName(coreSupportMesh, "inlet")


#=============================================================================*
# ASSEMBLIES CREATION OF THE DIFFERENT PARTS OF MODEL
#=============================================================================*

listOfElements = [
  placeMesh(fuelElementMesh, z=0, key=["F45", "f"], name="fuelElement_45"),
  placeMesh(fuelElementMesh, z=0, key="F39", name="fuelElement_39"),
  placeMesh(fuelElementMesh, z=0, key="F34", name="fuelElement_34"),
  placeMesh(fuelElementMesh, z=0, key="F30", name="fuelElement_30"),
  placeMesh(fuelElementMesh, z=0, key="F26", name="fuelElement_26"),
  placeMesh(fuelElementMesh, z=0, key="F22", name="fuelElement_22"),
  placeMesh(fuelElementMesh, z=0, key="F19", name="fuelElement_19"),
  placeMesh(fuelElementMesh, z=0, key="F17", name="fuelElement_17"),
  placeMesh(fuelElementMesh, z=0, key="F15", name="fuelElement_15"),
  placeMesh(fuelElementMesh, z=0, key="F13", name="fuelElement_13"),
  placeMesh(fuelElementMesh, z=0, key=['C', 'c'], name="unloadedElements"),
  placeMesh(coreSupportMesh, z=-lengthcoreSupport, isAllNonZero=True, name="coreSupport"),
  placeMesh(supportPlateMesh, z=lengthFuelElement, isAllNonZero=True, name="supportPlate")
]

# gp=placeMesh(gap,lengthFuelElement)
# listOfElements.append(gp)

# nozzleCh=placeMesh(nozzleChamber,-lengthNozzleChamber, isAllNonZero=True)
# listOfElements.append(nozzleCh)

# upperPl=placeMesh(upperPlenum, lengthFuelElement, isAllNonZero=True)
# listOfElements.append(upperPl)

# Create the final mesh
coreMesh = smesh.Concatenate(
  listOfElements, True, 1, 1e-05, False, name='coreMesh'
)

isDone = coreMesh.Compute()

# Remove temporary meshes
for part in listOfElements:
  smesh.RemoveMesh(part)

#=============================================================================*

if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()

#=============================================================================*
