/******************************************
********** Material definitions **********
******************************************/
% --- fuel material (UC2) --- From PNNL-15870Rev. 1, considering 90pct U-235


% --- niobium carbide film
mat NbC  -7.82
41093.03c     0.5
6000.03c      0.5

% --- Inconel 718 stainless steel, from wiki ( roughly exact)
mat inconel718  -8.17
28000.03c     0.525
24000.03c     0.19
26000.03c     0.13138
42000.03c     0.03
41093.03c     0.05
73181.03c     0.05
29000.03c     0.001
13027.03c     0.005
22000.03c     0.01

% --- Aluminum alloy 6061, from PNNL
mat aluminum6061  -2.7
12000.03c     -0.01
13027.03c     -0.972
14000.03c     -0.006
22000.03c     -0.00088
24000.03c     -0.00195
25055.03c     -0.00088
26000.03c     -0.00409
29000.03c     -0.00275
30000.03c     -0.00146

% --- pyrolytic carbon, from thomas
mat pyrocarbon -1.40
6000.03c   1

% --- graphite, from PNNL
mat graphite -2.16
6000.03c   1

% --- graphite, from PNNL
mat graphiteTa -2.16
6000.03c   1
73181.03c  1

% --- beryllium
mat beryllium -1.848
4009.03c   1

% --- boral absorbant, from thomas
mat absorber -2.5
13027.03c  1
6000.03c   3
B-10.03c   12
22000.03c  2
H-1.03c    4

% --- gadolinium, from PNNL
mat gadolinium -7.901
64000.03c  1

% ---- zirconium hydride
mat zircHydride -5.61
1001.03c 1
40000.03c 1

% ---- hydrogen propellant.(liq.H has rho=0.071g/cc, gas.H has rho=0.000084g/cc)
mat propellant -0.0001
1001.03c 1


