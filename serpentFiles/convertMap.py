"""
Script to convert a lattice map in MCNP format to OpenMC list of list

Author: Thomas Guilbaud, EPFL, 2023/06/29
"""

lattice = """0 0 f F F
 0 g c C F
  F d H C F
   F C C F 0
    F F F 0 0"""


# Compute the number of rows and columns, suppose square map
nRow = int(len(lattice.split('\n')))
nCol = nRow

# Compute center indices of the lattice, suppose odd number of rows
centerX = int(nRow/2)
centerY = int(nCol/2)

print(nRow, nCol, centerX, centerY)

# Convert the text into a list of list
latticeMCNP = []
for line in lattice.split('\n'):
    latticeMCNP.append(line.split())

latticeOpenMC = []
# Set initial indices at the center 
idxX, idxY = centerX, centerY
for i in range(0, centerX+1):
    temp = []
    # Special case for the centered element
    if (i == 0):
        temp.append(latticeMCNP[idxX][idxY])
    # Loop over the element of a ring
    for j in range(6*i):
        temp.append(latticeMCNP[idxX][idxY])
        if (j < i):
            idxY += 1
        elif (i <= j and j < 2*i):
            idxX += 1
        elif (2*i <= j and j < 3*i):
            idxX += 1
            idxY -= 1
        elif (3*i <= j and j < 4*i):
            idxY -= 1
        elif (4*i <= j and j < 5*i):
            idxX -= 1
        elif (5*i <= j and j < 6*i):
            idxX -= 1
            idxY += 1
    latticeOpenMC.append(temp)
    print(f"Length ring {i}: {len(temp)}")

    # Set position of the first element of a ring
    idxX = centerX-i-1
    idxY = centerY

print(latticeOpenMC)
