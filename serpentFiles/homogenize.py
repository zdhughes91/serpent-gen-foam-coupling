"""
Zach Hughes - zhughes@tamu.edu
"""

import numpy as np

pathToMatFile = 'homogenizedMaterials'
hexagonalArea = 3.143 # cm, based of a=1.905cm
coolantChannelRadius = 0.122 # cm, hydrogen
claddingRadius = 0.127 # cm, niobium carbide
numCoolantChannels = 19
enrichment = 0.93 # from Thomas' core/OpenMC/model.py line 138

def getCircularArea(innerRadius,outerRadius):
    return (np.pi*outerRadius**2 - np.pi*innerRadius**2)

coolantFraction = numCoolantChannels * getCircularArea(0,coolantChannelRadius) / hexagonalArea
claddingFraction = numCoolantChannels * getCircularArea(coolantChannelRadius,claddingRadius) / hexagonalArea
fuelFraction = 1 - coolantFraction - claddingFraction

print('-'*15+'fractions'+'-'*15) # *********** 10% coolant fraction seems like it must be incorrect...
print('coolantFraction:'+str(coolantFraction))
print('claddingFraction:'+str(claddingFraction))
print('fuelFraction :'+str(fuelFraction ))
print('-'*45)
# % --- niobium carbide film
# mat NbC  -7.82
# 41093.03c     0.5
# 6000.03c      0.5
#
# below both from Thomas' core/OpenMC/model.py line 143
densityUList = [
            0.128, 0.148, 0.170, 0.195, 0.224, 0.258, 0.297, 0.341, 0.392, 0.450
        ]
densityC = 1.73

densityUCList = [rhoi+densityC for rhoi in densityUList]
fuelTypeList = ['13','15','17','19','22','26','30','34','39','45']

UC = {}
for i, fuelType in enumerate(fuelTypeList):  # building UC
    UC[fuelType] = {}
    UC[fuelType]['rho'] = densityUCList[i]
    UC[fuelType]['u235'] = (densityUList[i]/densityUCList[i]) * enrichment
    UC[fuelType]['u238'] = (densityUList[i]/densityUCList[i]) * (1-enrichment)
    UC[fuelType]['C'] = densityC/densityUCList[i]


molarMassNbC = 12.011 + 92.90638
NbC = {
    'rho':7.82,
    'Nb':92.90638/molarMassNbC,   #im builiding weight fraction here...
    'C':12.011/molarMassNbC
}

H = {
    'rho':0.0001
}

fuelElement = {}
for i, Type in enumerate(fuelTypeList):  # building density, wFrac for each 
    fuelElement[Type] = {}
    fuelElement[Type]['rho'] = (UC[Type]['rho']*fuelFraction) +\
                                (NbC['rho']*claddingFraction) +\
                                (H['rho']*coolantFraction)
    
    fuelElement[Type]['u235'] = (UC[fuelType]['u235']*fuelFraction)
    fuelElement[Type]['u238'] = (UC[fuelType]['u238']*fuelFraction)
    fuelElement[Type]['C'] = (UC[fuelType]['C']*fuelFraction) + \
                              (NbC['C']*claddingFraction)
    fuelElement[Type]['Nb'] = (NbC['Nb']*claddingFraction)
    fuelElement[Type]['H'] = coolantFraction
        

with open('homogenizedMaterials', 'w') as file:  #writing fuel element homogenized data
    # Write a string to the file
    file.write('/***************************************************************\n')
    file.write('********** Fuel Element Homogenized Material definitions *******\n')
    file.write('***************************************************************/\n')
    file.write('\n')
    
    for t in fuelElement:
        file.write('mat type{:0.6s}  -{:0.4f} \n'.format(t,fuelElement[t]['rho']))
        file.write('6000.03c  -{:0.6f} \n'.format(fuelElement[t]['C']))
        file.write('92235.03c  -{:0.6f} \n'.format(fuelElement[t]['u235']))
        file.write('92238.03c  -{:0.6f} \n'.format(fuelElement[t]['u238']))
        file.write('41093.03c  -{:0.6f} \n'.format(fuelElement[t]['Nb']))
        file.write('1001.03c  -{:0.6f} \n'.format(fuelElement[t]['H']))
        file.write('\n')
file.close()

        
# now homogenizing unloaded element.  --------------------------------------------------------
ulRadii = [0,0.47879,0.48387,0.53467,0.83947,0.86487]
ulMats = ['H','NbC','inconel718','ZrH','pyroC','graph']
ulFracs = {}
s = 0
for i, mat in enumerate(ulMats):
    if i == 5: break
    ulFracs[mat] = getCircularArea(ulRadii[i],ulRadii[i+1]) / hexagonalArea

ulFracs['graph'] = 1 - ulFracs['H'] - ulFracs['NbC'] - ulFracs['inconel718'] - ulFracs['ZrH'] - ulFracs['pyroC']
#print(ulFracs)

molarMassCTa = 12.011 + 180.94788 
graphTa = {   
    'rho':2.16,
    'C':12.011/molarMassCTa,     #im builiding weight fraction here...
    'Ta':180.94788 /molarMassCTa
}

molarMassZrH = 91.224 + 1.00784
ZrH = {
    'rho':5.61,
    'H':1.00784/molarMassZrH,
    'Zr':91.224/molarMassZrH
}

# building inconel steel
inconMats = ['Ni','Cr','Fe','Mo','Nb','Ta','Co','Mn','Cu',
             'Al','Ti','Si','C','S','P','B']
inconPcts = [.525,.19,.13138,.03,.05,.05,.005,.001,.001,
             .005,.01,.001,.0005,.00005,.00005,.00002]

incon = {}
incon['rho'] = 2.7
for i,mat in enumerate(inconMats):
    incon[mat] = inconPcts[i] # stupid way of doign this...

pyroC = {'rho':1.41} # is just graphite with different density
# unloaded
# for this it should be weightFrac of element times volFrac of mat
# and should be done for each specific element
unloaded = {}
unloaded['H'] = (ZrH['H']*ulFracs['ZrH'] + (ulFracs['H'])) 
unloaded['Nb'] = (NbC['Nb'] * ulFracs['NbC']) + (incon['Nb']*ulFracs['inconel718'])
unloaded['C'] = (NbC['C'] * ulFracs['NbC']) + (incon['C']*ulFracs['inconel718']) + \
                (ulFracs['pyroC']) + (ulFracs['graph'])
unloaded['Zr'] = ZrH['Zr']*ulFracs['ZrH']
unloaded['Ni'] = incon['Ni'] * ulFracs['inconel718']
unloaded['Cr'] = incon['Cr'] * ulFracs['inconel718']
unloaded['Fe'] = incon['Fe'] * ulFracs['inconel718']
unloaded['Mo'] = incon['Mo'] * ulFracs['inconel718']
unloaded['Ta'] = incon['Ta'] * ulFracs['inconel718']
unloaded['Co'] = incon['Co'] * ulFracs['inconel718']

unloaded['Mn'] = incon['Mn'] * ulFracs['inconel718']
unloaded['Cu'] = incon['Cu'] * ulFracs['inconel718']
unloaded['Al'] = incon['Al'] * ulFracs['inconel718']
unloaded['Ti'] = incon['Ti'] * ulFracs['inconel718']
unloaded['Si'] = incon['Si'] * ulFracs['inconel718']
unloaded['S'] = incon['S'] * ulFracs['inconel718']
unloaded['P'] = incon['P'] * ulFracs['inconel718']
unloaded['B'] = incon['B'] * ulFracs['inconel718']

unloaded['rho'] = (H['rho']*ulFracs['H'])

with open('homogenizedMaterials', 'a') as file:
    file.write('\n')
    file.write('/***************************************************************\n')
    file.write('********** Unloaded Element Homogenized Material definitions *******\n')
    file.write('***************************************************************/\n')
    file.write('\n')
    
    file.write('mat {:0.6s}  -{:0.4f} \n'.format('ul',fuelElement[t]['rho']))
    file.write('1001.03c  -{:0.6f} \n'.format(unloaded['H']))
    file.write('6000.03c  -{:0.6f} \n'.format(unloaded['C']))
    file.write('28000.03c  -{:0.6f} \n'.format(unloaded['Ni']))
    file.write('24000.03c  -{:0.6f} \n'.format(unloaded['Cr']))
    file.write('26000.03c  -{:0.6f} \n'.format(unloaded['Fe']))
    file.write('41093.03c  -{:0.6f} \n'.format(unloaded['Nb']))
    file.write('73181.03c  -{:0.6f} \n'.format(unloaded['Ta']))
    file.write('29000.03c  -{:0.6f} \n'.format(unloaded['Cu']))
    file.write('13027.03c  -{:0.6f} \n'.format(unloaded['Al']))
    file.write('22000.03c  -{:0.6f} \n'.format(unloaded['Ti']))
    file.write('14000.03c  -{:0.6f} \n'.format(unloaded['Si']))
    file.write('16000.03c  -{:0.6f} \n'.format(unloaded['S']))
    
    file.write('42000.03c  -{:0.6f} \n'.format(unloaded['Mo']))
    file.write('40000.03c  -{:0.6f} \n'.format(unloaded['Zr']))
    #file.write('15030.03c  -{:0.6f} \n'.format(unloaded['P']))
    file.write('5010.03c  -{:0.6f} \n'.format(unloaded['B']))
    #file.write('25000.03c  -{:0.6f} \n'.format(unloaded['Mn']))
    
# --------------------- now building unloaded TA ---------------------------
unloadedTa = {}
unloadedTa['H'] = (ZrH['H']*ulFracs['ZrH'] + (ulFracs['H'])) 
unloadedTa['Nb'] = (NbC['Nb'] * ulFracs['NbC']) + (incon['Nb']*ulFracs['inconel718'])
unloadedTa['C'] = (NbC['C'] * ulFracs['NbC']) + (incon['C']*ulFracs['inconel718']) + \
                (ulFracs['pyroC']) + (graphTa['C'] * ulFracs['graph'])
unloadedTa['Zr'] = ZrH['Zr']*ulFracs['ZrH']
unloadedTa['Ni'] = incon['Ni'] * ulFracs['inconel718']
unloadedTa['Cr'] = incon['Cr'] * ulFracs['inconel718']
unloadedTa['Fe'] = incon['Fe'] * ulFracs['inconel718']
unloadedTa['Mo'] = incon['Mo'] * ulFracs['inconel718']
unloadedTa['Ta'] = (incon['Ta'] * ulFracs['inconel718']) + \
                    (graphTa['Ta'] * ulFracs['graph'])
unloadedTa['Co'] = incon['Co'] * ulFracs['inconel718']

unloadedTa['Mn'] = incon['Mn'] * ulFracs['inconel718']
unloadedTa['Cu'] = incon['Cu'] * ulFracs['inconel718']
unloadedTa['Al'] = incon['Al'] * ulFracs['inconel718']
unloadedTa['Ti'] = incon['Ti'] * ulFracs['inconel718']
unloadedTa['Si'] = incon['Si'] * ulFracs['inconel718']
unloadedTa['S'] = incon['S'] * ulFracs['inconel718']
unloadedTa['P'] = incon['P'] * ulFracs['inconel718']
unloadedTa['B'] = incon['B'] * ulFracs['inconel718']

unloadedTa['rho'] = (H['rho']*ulFracs['H'])

with open('homogenizedMaterials', 'a') as file:
    file.write('\n')
    file.write('/***************************************************************\n')
    file.write('********** Unloaded Element (with graphTa) Homogenized Material definitions *******\n')
    file.write('***************************************************************/\n')
    file.write('\n')
    
    file.write('mat {:0.6s}  -{:0.4f} \n'.format('ulTa',fuelElement[t]['rho']))
    file.write('1001.03c  -{:0.6f} \n'.format(unloadedTa['H']))
    file.write('6000.03c  -{:0.6f} \n'.format(unloadedTa['C']))
    file.write('28000.03c  -{:0.6f} \n'.format(unloadedTa['Ni']))
    file.write('24000.03c  -{:0.6f} \n'.format(unloadedTa['Cr']))
    file.write('26000.03c  -{:0.6f} \n'.format(unloadedTa['Fe']))
    file.write('41093.03c  -{:0.6f} \n'.format(unloadedTa['Nb']))
    file.write('73181.03c  -{:0.6f} \n'.format(unloadedTa['Ta']))
    file.write('29000.03c  -{:0.6f} \n'.format(unloadedTa['Cu']))
    file.write('13027.03c  -{:0.6f} \n'.format(unloadedTa['Al']))
    file.write('22000.03c  -{:0.6f} \n'.format(unloadedTa['Ti']))
    file.write('14000.03c  -{:0.6f} \n'.format(unloadedTa['Si']))
    file.write('16000.03c  -{:0.6f} \n'.format(unloadedTa['S']))
    
    file.write('42000.03c  -{:0.6f} \n'.format(unloadedTa['Mo']))
    file.write('40000.03c  -{:0.6f} \n'.format(unloadedTa['Zr']))
    #file.write('15030.03c  -{:0.6f} \n'.format(unloadedTa['P']))
    file.write('5010.03c  -{:0.6f} \n'.format(unloadedTa['B']))
    #file.write('25000.03c  -{:0.6f} \n'.format(unloadedTa['Mn']))
    