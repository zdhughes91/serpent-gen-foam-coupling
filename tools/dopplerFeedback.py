"""
Author: Zach Hughes - zhughes@tamu.edu

 - The python file should be run from the top directory, however it runs
    Serpent from the lower level directories. 
"""
# 
import shutil
import os
import fileinput
import subprocess

class dopplerFeedback:
    def __init__(self,folderName,execName,materialFiles,initT,dopplerT,numCores):
        """
            folderName (str) - name of the folder you want to run the case in
            execFile (str) - executable file (ie, "sss2 execFile)
            materialFiles (list) - list of strings of files that contain isotopes you want to change
            numCores (int) - for parallel processing using -omp
        """
        # import shutil
        # import os
        # import fileinput
        # import subprocess
        self.folderName = folderName
        self.execName = execName
        self.materialFiles = materialFiles
        self.initT = initT
        self.dopplerT = dopplerT
        self.numCores = numCores

    def setupCases(self):
        """
        this is to copy original folder, and create two identical folders that can be
        edited so doppler results can be found
        """
        baseFoldername = self.folderName+str(self.initT)
        doppFoldername = self.folderName+str(self.dopplerT)
        self.copy_folder(baseFoldername,self.folderName)
        self.copy_folder(doppFoldername,self.folderName)

        return baseFoldername, doppFoldername

    def copy_file(self,new_filename,old_filename):
        try:
            shutil.copy2(old_filename, new_filename)
            print(f"File '{old_filename}' copied to '{new_filename}' successfully.")
        except FileNotFoundError:
            print(f"Error: File '{old_filename}' not found.")
        except Exception as e:
            print(f"An error occurred: {e}")

    def copy_folder(self,new_folder,old_folder):
        try:
            shutil.copytree(old_folder, new_folder)
            print(f"Folder '{old_folder}' copied to '{new_folder}' successfully.")
        except FileNotFoundError:
            print(f"Error: Folder '{old_folder}' not found.")
        except FileExistsError:
            print(f"Error: Folder '{new_folder}' already exists.")
        except Exception as e:
            print(f"An error occurred: {e}")

    def rename_file(self,new_file_name, old_file_name):
        try:
            # Rename the old file to the new file name
            os.rename(old_file_name, new_file_name)
            print(f'Successfully renamed {old_file_name} to {new_file_name}')
        except FileNotFoundError:
            print(f'Error: File {old_file_name} not found.')
        except Exception as e:
            print(f'Error: {e}')

    def delete_folder(self,folder_name):
        try:
            shutil.rmtree(folder_name)
            print(f"Folder '{folder_name}' deleted successfully.")
        except FileNotFoundError:
            print(f"Error: Folder '{folder_name}' not found.")
        except Exception as e:
            print(f"An error occurred: {e}")

    def controlHIdentifiers(self,filepath,oldID,newID):
        """
        replaces the old XS ID with the new
        - this is for changing cross section librareis explicitly
        - should not be called directly

        filepath (str) - the name of the file path
        oldID (str) - the string that when found will be replaced with newID
        newID (str) - the string you want to include in the place of oldID
        """
        with fileinput.FileInput(filepath, inplace=True, backup='.bak') as file:
            for line in file:
                print(line.replace(oldID, newID), end='')

    def addDopplerFlags(self,filepath,dopplerTemp):
        """
        add doppler temperature flags to all materials in filepath
        """
        with open(filepath, 'r') as file:
            lines = file.readlines()

        with open(filepath, 'w') as output:
            for line in lines:
                # Check if the line starts with 'mat'
                if line.startswith('mat'):
                    modified_line = line.strip() + ' tmp {} \n'.format(dopplerTemp)
                else:
                    modified_line = line

                output.write(modified_line)

    def removeDopplerFlags(self,filepath,dopplerTemp):
        """
        removes doppler temperature flags from all materials in filepath
        """
        with open(filepath, 'r') as file:
            lines = file.readlines()

        with open(filepath, 'w') as output:
            for line in lines:
                if line.strip().endswith('tmp 1200'):
                    modified_line = line.rsplit(' ', 2)[0] + '\n'
                else:
                    modified_line = line

                output.write(modified_line)
        file.close()

    def changeFuelIDs(self,filepath,isoList,initID,finalID):
        """
        changing the fuel XS IDs from .09c to .12c
        - this really just calls controlHIdentifiers each time
            filepath (str) - 
            isoList (list) - list of strings for each isotope
        """
        for iso in isoList:
            self.controlHIdentifiers(filepath,iso+'.'+initID,iso+'.'+finalID) 

    def extractValue(self,startline,path):
        """
        extract a value from serpent output file
            start (str) - what the line starts with that you want the data for
                            ex. "ANA_KEFF" or "BETA_EFF"
            path (str) - the path to the file that was run
        """
        with open(path+'_res.m','r') as file:
            for line in file:
                if line.startswith(startline):
                    parts = line.split('=')
                    if len(parts) > 1:
                        stringVals = parts[1].split()[1:-1]
                        keff = [float(value) for value in stringVals ]
                        return keff

    def getAlpha(self,keff_before,keff_after,deltaT):
        """
        getting reactivity coef based on two keffs
        - returns value in pcm/K
        """
        return 1e5*(keff_after-keff_before)/deltaT

    def getAlphaUncert(self,k1,k1_uncert,k2,k2_uncert,dT):
        """
        calculates uncertainty in the reactivity coefficient using statistical uncertainty
            from serpent. 
        k1 (float) - keff of run base run
        k1_uncert (float) - statistical uncertainty associated with k1
        k2 (float) - keff of doppler broadened run
        k2_uncert (float) - statistical uncertainty associated with k2
        dT (float) - temperature change as a result of doppler broadening

        alpha = 1e5* (k2-k1) / dT
        - dk1 & dk2 are partial derivatives
        """
        dk2 = (1e5/dT)*(1-k1)
        dk1 = (1e5/dT)*(k2-1)
        uncert = ((dk2*k2_uncert)**2 + (dk1*k1_uncert)**2 )**(1/2)
        return uncert

    def startProcesses(self,sss2path: str):
        """
        This function starts the serpent calculation for both the initT case and the dopplerT case
        this function
        - creates two new folders, 
        - starts the case, 
        - logs the output to .log files
        - returns the process objects so the .wait() command can be put in a more appropriate location.
        """

        basefoldername, doppfoldername = self.folderName+str(self.initT),self.folderName+str(self.dopplerT)
        args = [self.execName,'-omp',str(self.numCores)]

        os.chdir(basefoldername)
        with open(self.execName+str(self.initT)+'.log','w') as logfile1: # start base case w/ log file
            baseprocess = subprocess.Popen([sss2path] + args, stdout=logfile1, stderr=logfile1)
        print('- Base process started \n')

        os.chdir('../'+doppfoldername) # change into doppler folder

        with open(self.execName+str(self.dopplerT)+'.log','w') as logfile2: # start dopp case w/ log files
            doppprocess = subprocess.Popen([sss2path] + args, stdout=logfile2, stderr=logfile2)
        print('- Doppler process started \n')
        os.chdir('..')

        return baseprocess, doppprocess

    def clean(self,casename=''):
        """
        this function
        - copies the log file from both created folders and brings it to main directory
        - copies the _res.m file form both created folders and ""
        - deletes the two created folders
        """
        baseFoldername = self.folderName+str(self.initT)
        doppFoldername = self.folderName+str(self.dopplerT)

        # copying the log files to main folder
        try:
            self.copy_file(casename+self.execName+str(self.initT)+'.log',baseFoldername+'/'+self.execName+str(self.initT)+'.log')
            self.copy_file(casename+self.execName+str(self.dopplerT)+'.log',doppFoldername+'/'+self.execName+str(self.dopplerT)+'.log')

            # copying the _res.m files to main folder
            self.copy_file(casename+self.execName+str(self.initT)+'_res.m',baseFoldername+'/'+self.execName+'_res.m')
            self.copy_file(casename+self.execName+str(self.dopplerT)+'_res.m',doppFoldername+'/'+self.execName+'_res.m')
            # deleting created folders
            self.delete_folder(baseFoldername)
            self.delete_folder(doppFoldername)
        except:
            print('An error occurred. Created folders have been left intact')




# ---------------------------------------------------------------------
# --------------- fuel temperature reactivity feedbacks ---------------
# ---------------------------------------------------------------------
if __name__ == "__main__":
                                                                
    sss2path = './../../Serpent2/src/src/sss2'
    serpentFile = 'coreSupport.txt'

    # import os
    # import fileinput
    # import subprocess
    # #import time
    # import shutil

    inletPropDensity = 10 # kg/m^3
    outletPropDensity = 0.2 # kg/m^3
    outputFile = serpentFile + '_res.m'   
    initT, dopplerT = 417, 1548

    het = dopplerFeedback(folderName='heteroKiwi',
                        execName=serpentFile,
                        materialFiles=['fuelElementMaterials.txt','materials.txt'],
                        initT=initT,
                        dopplerT=dopplerT,
                        numCores=3
                        )
    
    basefolder, doppfolder = het.setupCases() # building two cases for runs
    #doppfolder = 'KiwiThomas1548'
    for mfile in ['fuelElementMaterials.txt','materials.txt']:
        het.controlHIdentifiers(doppfolder+'/'+mfile,'.03','.15') # changing all ids to .15
        het.addDopplerFlags(doppfolder+'/'+mfile,dopplerT)        # broaden dopp to 1548
        het.addDopplerFlags(basefolder+'/'+mfile,initT)           # broaded base to 417

    hetbaseP, hetdoppP = het.startProcesses(sss2path) # start serpent for het cases

    hom = dopplerFeedback(folderName='homoKiwi',
                    execName=serpentFile,
                    materialFiles=['homogenizedFuels','homogenizedUnloaded'],
                    initT=initT,
                    dopplerT=dopplerT,
                    numCores=3
                    )

    hombasefolder, homdoppfolder = hom.setupCases(sss2path)

    for mfile in ['homogenizedFuels','homogenizedUnloaded']:
        hom.controlHIdentifiers(homdoppfolder+'/'+mfile,'.03','.15') # changing all ids to .15
        hom.addDopplerFlags(homdoppfolder+'/'+mfile,dopplerT)        # broaden dopp to 1548
        hom.addDopplerFlags(hombasefolder+'/'+mfile,initT)           # broaded base to 417

    hombaseP, homdoppP = hom.startProcesses()

    hetbaseP.wait()
    hetdoppP.wait()
    hombaseP.wait()
    homdoppP.wait()

    het.clean(casename='het')
    hom.clean(casename='hom')

    dataHet = {'base':{'keff':het.extractValue("ANA_KEFF",'het'+het.execName+str(het.initT)),
                       'beta':het.extractValue("BETA_EFF",'het'+het.execName+str(het.initT))
                    },
               'dopp':{'keff':het.extractValue("ANA_KEFF",'het'+het.execName+str(het.dopplerT)),
                       'beta':het.extractValue("BETA_EFF",'het'+het.execName+str(het.dopplerT))
                    }
              }
    dataHom = {'base':{'keff':hom.extractValue("ANA_KEFF",'hom'+hom.execName+str(hom.initT)),
                       'beta':hom.extractValue("BETA_EFF",'hom'+hom.execName+str(hom.initT))
                },
               'dopp':{'keff':hom.extractValue("ANA_KEFF",'hom'+hom.execName+str(hom.dopplerT)),
                       'beta':hom.extractValue("BETA_EFF",'hom'+hom.execName+str(hom.dopplerT))
                }
              }
    
    print('-'*20+'Heterogenous'+'-'*20)
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f} '.format(initT,dataHet['base']['keff'][0],dataHet['base']['keff'][1]))
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f}'.format(dopplerT,dataHet['dopp']['keff'][0],dataHet['dopp']['keff'][1]))
    #print('| alphaFuelDoppler = {:0.4e} +/- {:0.4e} pcm/K '.format(dataHet['alpha'][0],dataHet['alpha'][1]))
    print('Reactivity change = {:0.4e} $ '.format((dataHet['dopp']['keff'][0]-dataHet['base']['keff'][0])/dataHet['base']['beta'][0]))
    print('-'*54)

    print('-'*22+'Homogenous'+'-'*22)
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f} '.format(initT,dataHom['base']['keff'][0],dataHom['base']['keff'][1]))
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f}'.format(dopplerT,dataHom['dopp']['keff'][0],dataHom['dopp']['keff'][1]))
    #print('| alphaFuelDoppler = {:0.4e} +/- {:0.4e} pcm/K '.format(dataHom['alpha'][0],dataHom['alpha'][1]))
    print('Reactivity change = {:0.4e} $ '.format((dataHom['dopp']['keff'][0]-dataHom['base']['keff'][0])/dataHom['base']['beta'][0]))
    print('-'*54)
    # -- 
    # - first for homogenous case                              
    # keff_homo900, keff_homo1200, alphaFuelDopplerHomo = analyheteCase(folderName='homoKiwi',
    #                                                                 materialFile='homogenihetedFuels',
    #                                                                 numCores=16,
    #                                                                 dopplerT=1200,
    #                                                                 dT=300
    #                                                                 )

    # print('-'*20+'Homogenous'+'-'*20)
    # print('| Keff_900K fuel = {:0.6f} +/- {:0.6f} '.format(keff_homo900[0],keff_homo900[1]))
    # print('| Keff_1200K fuel = {:0.6f} +/- {:0.6f}'.format(keff_homo1200[0],keff_homo1200[1]))
    # print('| alphaFuelDoppler = {:0.4e} +/- {:0.4e} pcm/K '.format(alphaFuelDopplerHomo[0],alphaFuelDopplerHomo[1]))
    # print('-'*54)

    # - now for heterogenous case
    # baseprocess,doppprocess = startProcesses(folderName='KiwiThomas',
    #                                         execFile=serpentFile,
    #                                         materialFiles=['fuelElementMaterials.txt','materials.txt'],
    #                                         numCores=4,
    #                                         initT=initT,
    #                                         dopplerT=dopplerT
    #                                         )
    
    # baseprocess.wait()
    # doppprocess.wait()
    # keffbase = extractValue('ANA_KEFF','KiwiThomas/'+serpentFile+str(initT))
    # keffdopp = extractValue('ANA_KEFF','KiwiThomas/'+serpentFile+str(dopplerT))    

    # print('-'*20+'Heterogenous'+'-'*20)
    # print(keffbase)
    # print(keffdopp)
    # print(rundata['deltaK$'])
    # print('-'*20+'Heterogenous'+'-'*20)
    # print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f} '.format(initT,rundata['1']['keff'][0],rundata['1']['keff'][1]))
    # print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f}'.format(dopplerT,rundata['2']['keff'][0],rundata['2']['keff'][1]))
    # print('| alphaFuelDoppler = {:0.4e} +/- {:0.4e} pcm/K '.format(rundata['alpha'][0],rundata['alpha'][1]))
    # print('-'*54)

    # with open('writeoutfile','w') as output:
    #     output.write('-'*20+'Heterogenous'+'-'*20+'initT={:0.0f}K, dopT={:0.0f}K \n'.format(dopplerT,initT))
    #     output.write('| Keff_900K fuel = {:0.6f} +/- {:0.6f} \n'.format(keff_hete900[0],keff_hete900[1]))
    #     output.write('| Keff_1200K fuel = {:0.6f} +/- {:0.6f}\n'.format(keff_hete1200[0],keff_hete1200[1]))
    #     output.write('| alphaFuelDoppler = {:0.4e} +/- {:0.4e} pcm/K \n'.format(alphaFuelDopplerHete[0],alphaFuelDopplerHete[1]))
    #     output.write('-'*54+'\n')


