"""
    Author: Zach Hughes - zhughes@tamu.edu 

This code defines a class that can assist in the homogenization of heterogenous universes. 
The class allows for the universe density, and weight percentages of all isotopes to be calculated,
and at the bottom is an example of the input dictionary
"""


class homogenizeUni:
    def __init__(self,d):
        """
        d is the user dictionary containing all universe info
        
        """
        self.d = d # the main dictionary containing all universe information
        self.isotopes = {} # a dictionary containing isotope names followed by weight pct


    def homogenizeDensity(self):
        """ this function assigns the density of the entire universe + mass fraction of each material
           - it is possible this can be solved only using densities?..
        """
        totMass = 0
        for mat in self.d: # loop thru materials
            if not isinstance(self.d[mat],dict): continue
            # calculating mass of each material and assigning to dict
            self.d[mat]['mass'] = self.d['totalVolume'] * \
                                  self.d[mat]['rho'] * \
                                  self.d[mat]['volFrac']
            
            totMass += self.d[mat]['mass']
        
        self.d['totalMass'] = totMass # save total mass to main dict
        for mat in self.d: # loop thru materials
            if not isinstance(self.d[mat],dict): continue
            self.d[mat]['massFrac'] = self.d[mat]['mass'] / totMass
        
        self.d['homogenizedDensity'] = totMass/self.d['totalVolume']
    
    def getMaterialWeightPercent(self):
        """
        calculated the weight percent of each isotope in the material it is
        found in.
            - note this is the weight percent in each material and not in the
              entire universe
        """
        wAvgMolarMass = 0
        for mat in self.d: # looping thru materials
            if not isinstance(self.d[mat],dict): continue
            wAvgMolarMass = 0
            for iso in self.d[mat]: # looping thru elements/isotops
                if not isinstance(self.d[mat][iso],dict): continue  
                wAvgMolarMass += self.d[mat][iso]['molarMass'] * \
                                 self.d[mat][iso]['enrichment']
                self.isotopes[iso] = {'wPct':0.0,'id':self.d[mat][iso]['id']} # intializing isotopes dictionary
                                 
            self.d[mat]['wAvgMolarMass'] = wAvgMolarMass
            
            for iso in self.d[mat]: # looping thru elements/isotops again
                if not isinstance(self.d[mat][iso],dict): continue  
                self.d[mat][iso]['wPct'] = (self.d[mat][iso]['molarMass'] * \
                                           self.d[mat][iso]['enrichment']) / \
                                           wAvgMolarMass
                self.d[mat][iso]['mass'] = self.d[mat][iso]['wPct'] * self.d[mat]['mass']
            
    def getUniverseWeightPercent(self):
        """
        assigns the weight percentage of each isotope to self.isotopes
        - these are the weight percentages needed in the serpent material definition
        """
        for mat in self.d: # looping thru materials
            if not isinstance(self.d[mat],dict): continue
            for iso in self.d[mat]: # looping thru elements/isotops
                if not isinstance(self.d[mat][iso],dict): continue 
                self.isotopes[iso]['wPct'] += self.d[mat][iso]['mass'] / \
                                              self.d['totalMass']                          
    
    def solve(self):
        """
        a single command to solve for all values of interest
        """
        self.homogenizeDensity()
        self.getMaterialWeightPercent()
        self.getUniverseWeightPercent()
        
        
    def writeOut(self,filename: str,homoUniName: str) -> None:
        """
        a function for writing a materials file readable by Serpent2
        
        filename (str) - name of the output file it should write to
        homoUniName (str) - name of the universe. this will be the name of the
                            material when it writes out to serpent
        """
        #self.solve() # so only one command is needed by user
        
        # going to check if ids exist before writing anything
        for mat in self.d: # looping thru materials
            if not isinstance(self.d[mat],dict): continue
            for iso in self.d[mat]: # looping thru elements/isotops
                if not isinstance(self.d[mat][iso],dict): continue 
                try:
                    x = self.d[mat][iso]['id']
                except:
                    raise KeyError('You need to input Serpent id for the <{}> isotope'.format(iso))
        
        with open(filename, 'a') as file:
            file.write('\n')
            file.write('mat {}  -{:0.5f} \n'.format(homoUniName,self.d['homogenizedDensity']))
            for iso in self.isotopes:
                file.write('{}   -{:0.6f} \n'.format(self.isotopes[iso]['id'],self.isotopes[iso]['wPct']))
            file.close()
                    
# =============================================================================================

if __name__ == "__main__": # this is just to check functionality and provide dictionary example

    uDict = {
            # fuel material
            'UC':{'volFrac':0.6937,
                   'rho':1.858,
                   'u235':{'molarMass':235.0,
                           'enrichment':0.9,
                           'id':'92235.03c'
                           },
                   'u238':{'molarMass':238.0,
                           'enrichment':0.1,
                           'id':'92238.03c'
                           },
                   'C':{'molarMass':12.011,
                        'enrichment':1.0,
                        'id':'6000.03c'
                           }
                   },
             # propellant material
             'Prop':{'volFrac':0.28267,
                     'rho':0.0001,
                     'H':{'molarMass':1.0,
                           'enrichment':1.0,
                           'id':'1001.03c'
                           }
                     },
             # cladding material
             'Clad':{'volFrac':0.02364,
                     'rho':7.82,
                     'Nb':{'molarMass':92.906,
                           'enrichment':1.0,
                           'id':'41093.03c'
                           },
                     'C':{'molarMass':12.011,
                           'enrichment':1.0,
                           'id':'6000.03c'
                           }
                     },
             
             'totalVolume':415.127 # cc
             }
    
    
    fuelTypesList = ['13','15','17','19','22','26','30','34','39','45']
    uniNames = ['type'+num for num in fuelTypesList]
    densityUList = [ # the densities of uranium in each of the fuel types
            0.128, 0.148, 0.170, 0.195, 0.224, 0.258, 0.297, 0.341, 0.392, 0.450
        ]
    densityC = 1.73
    
    for fuelType in range(len(uniNames)):
        uni = homogenizeUni(uDict) # initialize universe object
        uni.d['UC']['rho'] = densityC + densityUList[fuelType] # adjust density to one specified in paper
        uni.solve()
        uni.writeOut('homogenizedMaterials',uniNames[fuelType])
        
