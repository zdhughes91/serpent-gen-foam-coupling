"""
Author: Zach Hughes - zhughes@tamu.edu

- A script to extract mean free path values from Serpent2 output
"""
# ================= inputs =================================
filename = 'heteroKiwi/coreSupport.txt_res.m'
    

# ===========================================================
uniKeys = []
totXSs = []
with open(filename,'r') as file:
    uniFound = False
    
    for line in file:
        if line.startswith("GC_UNIVERSE_NAME"):
            parts = line.split('=')
            if len(parts) > 1:
                uniKeys.append(parts[1].strip().strip("';"))

        if line.startswith("INF_TOT"):
            parts = line.split('=')
            if len(parts) > 1:
                # Extract the numbers in brackets to the right of the equals sign,and remove brackets
                stringVals = parts[1].split()[1:-1] # right 
                totXSs.append([float(value) for value in stringVals ])

Sigma_tot = {}
for i, key in enumerate(uniKeys):
    Sigma_tot[key] = totXSs[i]


# sData = {'F':[  2.47312E-01, 0.00058,  4.52865E-01, 0.00568 ],
#          'T': [  3.92201E-01, 0.00111  ,5.77801E-01, 0.01281 ],
#          'C':[  4.74427E-01, 0.00040,  6.82838E-01, 0.00468 ],
#          '9':[  2.47938E-01, 0.00153 , 4.63133E-01, 0.00993 ],
#          '8':[  2.47305E-01, 0.00132 , 4.66359E-01, 0.00982 ],
#          '7':[  2.48357E-01, 0.00198,  4.53179E-01, 0.00352 ],
#          '6':[  2.47568E-01, 0.00172 , 4.31862E-01, 0.02714 ],
#          '5':[  2.48707E-01, 0.00297,  4.28876E-01, 0.01288 ],
#          '4':[  2.49366E-01, 0.00123,  4.22894E-01, 0.00745 ],
#          '3':[  2.49897E-01, 0.00597,  4.07452E-01, 0.00498 ],
#          '2':[  2.65465E-01, 0.00138,  3.30618E-01, 0.00058 ],
#          'g':[  4.60930E-01, 0.00014,  5.29436E-01, 4.3E-05 ],
#          '_':[0,0,0,0]
#          }

for uni in Sigma_tot:
    if uni == '_': continue
    Sigma_tot_thermal = Sigma_tot[uni][0] 
    if Sigma_tot_thermal == 0: 
        print('xs=0 for {},continuing...'.format(uni))
        continue
    print('mfp of {} = {:0.4f} cm'.format(uni,1/Sigma_tot_thermal))