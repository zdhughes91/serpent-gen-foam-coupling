
"""
	Author: Zach Hughes - zhughes@tamu.edu
A simple python script for running multiple Serpent2 cases simulataneously and extracting the keff. 
"""

# het_keff = 1.074190 +/- 0.000660
# hom_keff = 1.062510 +/- 0.000660
# ----- input
run = True  # set to true to run the Serpent case, to false if it should just read existing _res.m file
# ----- 

import os
import subprocess

def startProcesses():
    """
    This function starts the serpent calculation for both the initT case and the dopplerT case
    this function
    - creates two new folders, 
    - starts the case, 
    - logs the output to .log files
    - returns the process objects so the .wait() command can be put in a more appropriate location.
    """
    global sss2path

    basefoldername, doppfoldername = folderName+str(initT),folderName+str(dopplerT)
    args = [execName,'-omp',str(numCores)]

    os.chdir(basefoldername)
    with open(execName+str(initT)+'.log','w') as logfile1: # start base case w/ log file
        baseprocess = subprocess.Popen([sss2path] + args, stdout=logfile1, stderr=logfile1)
    print('- Base process started \n')

    os.chdir('../'+doppfoldername) # change into doppler folder

    with open(execName+str(dopplerT)+'.log','w') as logfile2: # start dopp case w/ log files
        doppprocess = subprocess.Popen([sss2path] + args, stdout=logfile2, stderr=logfile2)
    print('- Doppler process started \n')
    os.chdir('..')

    return baseprocess, doppprocess

def extractValue(startline,path):
    """
    extract a value from serpent output file
        start (str) - what the line starts with that you want the d for
                        ex. "ANA_KEFF" or "BETA_EFF"
        path (str) - the path to the file that was run
    """
    with open(path+'_res.m','r') as file:
        for line in file:
            if line.startswith(startline):
                parts = line.split('=')
                if len(parts) > 1:
                    stringVals = parts[1].split()[1:-1]
                    keff = [float(value) for value in stringVals ]
                    return keff
                

sss2path = './../../Serpent2/src/src/sss2'
execList = ['hetKiwi.txt','homKiwi.txt']
hetArgs = ['hetKiwi.txt','-omp','2']
homArgs = ['homKiwi.txt','-omp','2']

if run:
    os.chdir('hetKiwi')
    with open('hetKiwi.log','w') as logHet:
        hetProc = subprocess.Popen([sss2path] + hetArgs, stdout=logHet, stderr=logHet)

    os.chdir('../homKiwi')

    with open('homKiwi.log','w') as logHom:
        homProc = subprocess.Popen([sss2path] + homArgs, stdout=logHom, stderr=logHom)

    os.chdir('..')

    hetProc.wait()
    homProc.wait()

d = {
    'het':{},
    'hom':{}
}

d['het']['keff'] = extractValue("ANA_KEFF",'hetKiwi/hetKiwi.txt')
d['hom']['keff'] = extractValue("ANA_KEFF",'homKiwi/homKiwi.txt')

print('het_keff = {:0.6f} +/- {:0.6f}'.format(d['het']['keff'][0],d['het']['keff'][1]))
print('hom_keff = {:0.6f} +/- {:0.6f}'.format(d['hom']['keff'][0],d['het']['keff'][1]))

