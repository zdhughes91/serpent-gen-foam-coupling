"""
Author: Zach Hughes - zhughes@tamu.edu

 - The python file should be run from the top directory, however it runs
    Serpent from the lower level directories. 
"""

# --------------------- fuel expansion feedbacks ----------------------
# ---------------------------------------------------------------------
# the only things that need to change are fuel universe geometry and fuel density
def getCircularArea(innerRadius,outerRadius):
    return (3.14159*(outerRadius**2 - innerRadius**2))

def getExpandedVolume(initVolume,volumeCoef,dT):
    """
    calculates the new volume as a result of thermal expansion
        initVolume (float) - initial volume before temperature change
        volumeCoef (float) - volume coefficient of thermal expansion
        dT (float) - temperature change

    """
    V = (initVolume * volumeCoef * dT) + initVolume
    return V

def fromVolumeGetChannelRadii(volume):
    """
    the idea here is to input thermally expanded volume, and to receive
    updated cladding and prop radii
    
    """
    global numChannels,propTocladRatio, assemblyHeight, totHexArea
    area = volume / assemblyHeight
    rClad = ( (1/(numChannels*3.14159)) * (totHexArea - area) )**(1/2)
    rProp = rClad * propToCladRatio
    return rClad, rProp

def readFuelDensity(filepath):
    """
    reads the fuel density from the Serpent input deck, also calculates mass
        filepath (str) - file path from reactivityFeedbacks.py directory to the fuel mat file

    """
    global fuelVolume, volumeExpansionCoef
    rhoList = []
    with open(filepath,'r') as file:
        lines = file.readlines()
        for line in lines:
            if line.startswith('mat'):
                rhoList.append(-1 * float(line.split()[-1]))
    file.close()
    massList = [rho*fuelVolume for rho in rhoList]
    return rhoList, massList

def getUpdatedDensity(massList,tempUpdatedVolume):
    """
    takes the mass of fuel in each zone with the temperature expanded
    volume and calculates the updated density in each material
        massList (list) - list of floats of mass of fuel in each region
        tempUpdatedVolume (float) - volume of fuel region after temperature expansion
    
    """
    return [m/tempUpdatedVolume for m in massList]

def changeMaterialDensity(filepath,replacementDensity):
    """
    changes the density in filepath to replacementDensity
        filepath (str) - path to serpent file that contains only fuel materials
        replacementDensity (list) - list of floats of densities in each fuel
    """
    with open(filepath, 'r') as file:
        lines = file.readlines()

    with open(filepath, 'w') as output:
        for line in lines:
            if line.startswith('mat'):
                words = line.split()
                words[-1] = '-{:0.6f}'.format(replacementDensity.pop(0))
                modified_line = ' '.join(words) + '\n'
            else:
                modified_line = line

            output.write(modified_line)

def changeChannelRadii(filepath,rClad,rProp):
    """
    changes the radii of the coolant channels in the heterogenous geometry
        filepath (str) - filepath to the file where the coolant channel radii are defined. this
                        file should only contain two cylinder surfaces, the first describing the cladding
                        outer diameter and the second describing the propellant outer diameter
        rClad (float) - the radius of the cladding
        rProp (float) - the radius of the propellant
    """
    with open(filepath, 'r') as file:
        lines = file.readlines()

    rCladChanged = False
    with open(filepath, 'w') as output:
        for line in lines:
            if line.startswith('surf') and rCladChanged == False:
                words = line.split()
                words[-3] = '{:0.5f}'.format(rClad)
                modified_line = ' '.join(words) + '\n'
                rCladChanged = True
            elif line.startswith('surf') and rCladChanged == True:
                words = line.split()
                words[-3] = '{:0.5f}'.format(rProp)
                modified_line = ' '.join(words) + '\n'
            else:
                modified_line = line
            output.write(modified_line)

if __name__ == "__main__":

    # 
    sss2path = './../../Serpent2/src/src/sss2'
    serpentFile = 'coreSupport.txt'

    import os
    import fileinput
    import subprocess
    import time


    inletPropDensity = 10 # kg/m^3
    outletPropDensity = 0.2 # kg/m^3
    propDensity = 0.0009245 # g/cc
    outputFile = serpentFile + '_res.m'
    # the below paper claims thermal expansion coef of 6-7 um/(mK), in Table II
    #Performance of (U,Zr)C-graphite (composite) and of (U,Zr)C (carbide) fuel elements in the Nuclear Furnace 1 test reactor
    linearExpansionCoef = 7e-6 # m/(mK)
    volumeExpansionCoef = 3 * linearExpansionCoef
    propRadius = 0.122 #cm, 
    cladRadius = 0.127 #cm, 
    propToCladRatio = propRadius / cladRadius
    numChannels = 19 
    assemblyHeight = 132.08 #cm 
    hexRadius = 0.95758
    totHexArea = 3.1764 #cm^2, solved using hexRadius in online hexagon solver

    channelArea = getCircularArea(0,cladRadius)
    fuelArea = totHexArea - (numChannels * channelArea)

    fuelVolume = fuelArea * assemblyHeight 

    # now starting calculations
    expandedVolume = getExpandedVolume(fuelVolume,
                                    volumeExpansionCoef,
                                    300
                                    )
    rClad,rProp = fromVolumeGetChannelRadii(expandedVolume)

    # grabs the original from a backup file so I dont lose it
    origFuelDensities, origFuelMasses = readFuelDensity('heteroKiwi/fuelElementMaterials.bak')
    updatedFuelDensities = getUpdatedDensity(origFuelMasses,expandedVolume) # gets updated based on dT

    # -- changing the heterogenous input 
    #changeMaterialDensity('heteroKiwi/fuelElementMaterials.txt',updatedFuelDensities)
    #changeMaterialDensity('heteroKiwi/fuelElementMaterials.txt',origFuelDensities)

    #changeChannelRadii('heteroKiwi/FuelChannel.txt',rClad,rProp)
    #changeChannelRadii('heteroKiwi/FuelChannel.txt',cladRadius,propRadius)
    # -- ^ changing the heterogenous input ^