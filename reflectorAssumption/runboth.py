

import os
import subprocess

def startProcesses():
    """
    This function starts the serpent calculation for both the initT case and the dopplerT case
    this function
    - creates two new folders, 
    - starts the case, 
    - logs the output to .log files
    - returns the process objects so the .wait() command can be put in a more appropriate location.
    """
    global sss2path

    basefoldername, doppfoldername = folderName+str(initT),folderName+str(dopplerT)
    args = [execName,'-omp',str(numCores)]

    os.chdir(basefoldername)
    with open(execName+str(initT)+'.log','w') as logfile1: # start base case w/ log file
        baseprocess = subprocess.Popen([sss2path] + args, stdout=logfile1, stderr=logfile1)
    print('- Base process started \n')

    os.chdir('../'+doppfoldername) # change into doppler folder

    with open(execName+str(dopplerT)+'.log','w') as logfile2: # start dopp case w/ log files
        doppprocess = subprocess.Popen([sss2path] + args, stdout=logfile2, stderr=logfile2)
    print('- Doppler process started \n')
    os.chdir('..')

    return baseprocess, doppprocess

def extractValue(startline,path):
    """
    extract a value from serpent output file
        start (str) - what the line starts with that you want the d for
                        ex. "ANA_KEFF" or "BETA_EFF"
        path (str) - the path to the file that was run
    """
    with open(path+'_res.m','r') as file:
        for line in file:
            if line.startswith(startline):
                parts = line.split('=')
                if len(parts) > 1:
                    stringVals = parts[1].split()[1:-1]
                    keff = [float(value) for value in stringVals ]
                    return keff
                

sss2path = './../../../Serpent2/src/src/sss2'
execList = ['fullyHetero.txt','semiHetero.txt']
fullArgs = ['fullyHetero.txt','-omp','2']
semiArgs = ['semiHetero.txt','-omp','2']

os.chdir('fullyHetero')
with open('fullyHetero.log','w') as logFull:
    fullProc = subprocess.Popen([sss2path] + fullArgs, stdout=logFull, stderr=logFull)

os.chdir('../semiHetero')

with open('semiHetero.log','w') as logSemi:
    semiProc = subprocess.Popen([sss2path] + semiArgs, stdout=logSemi, stderr=logSemi)

os.chdir('..')

fullProc.wait()
semiProc.wait()

d = {
    'full':{},
    'semi':{}
}

d['full']['keff'] = extractValue("ANA_KEFF",'fullyHetero/fullyHetero.txt')
d['semi']['keff'] = extractValue("ANA_KEFF",'semiHetero/semiHetero.txt')

print('full_keff = {:0.6f} '.format(d['full']['keff']))
print('semi_keff = {:0.6f} '.format(d['semi']['keff']))

