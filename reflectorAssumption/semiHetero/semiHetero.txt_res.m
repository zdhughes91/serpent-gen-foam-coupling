
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79537E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64633E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   2]) = '40' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.46965E+03 0.02661  4.60711E+04 0.01710  1.03888E+05 0.00293  1.87506E+05 0.00756  2.00304E+05 0.00285  1.86530E+05 0.00620  1.68727E+05 0.00289  1.49359E+05 0.00522  1.33436E+05 0.00314  1.20653E+05 0.00411  1.10972E+05 0.00489  1.03239E+05 0.00325  9.69150E+04 0.00519  9.24397E+04 0.00367  8.94280E+04 0.00508  7.62691E+04 0.00230  7.41703E+04 0.00736  7.16973E+04 0.00267  6.84704E+04 0.00408  1.25450E+05 0.00319  1.10175E+05 0.00527  7.31452E+04 0.00411  4.42146E+04 0.00679  4.74734E+04 0.00505  4.26806E+04 0.00676  3.38299E+04 0.00789  5.70551E+04 0.00788  1.14705E+04 0.01439  1.40814E+04 0.00856  1.26275E+04 0.00970  7.36272E+03 0.00691  1.25295E+04 0.00449  8.35174E+03 0.01139  6.91664E+03 0.01522  1.23565E+03 0.02640  1.32408E+03 0.02283  1.28330E+03 0.02165  1.28676E+03 0.02379  1.33755E+03 0.03152  1.30319E+03 0.00656  1.29251E+03 0.01869  1.25361E+03 0.01517  2.28098E+03 0.01744  3.55299E+03 0.00880  4.47862E+03 0.01422  1.15143E+04 0.00801  1.11837E+04 0.01966  1.07221E+04 0.01235  6.23184E+03 0.01672  4.03524E+03 0.01572  2.84304E+03 0.02443  3.03215E+03 0.01498  5.12782E+03 0.01779  5.79766E+03 0.01025  9.95471E+03 0.01393  1.48432E+04 0.01105  2.57920E+04 0.02145  2.14640E+04 0.02268  1.86047E+04 0.01414  1.54840E+04 0.02068  1.56491E+04 0.02079  1.73830E+04 0.01695  1.62337E+04 0.02227  1.20027E+04 0.02292  1.21446E+04 0.01862  1.17322E+04 0.02342  1.07600E+04 0.02708  8.96627E+03 0.01986  6.40799E+03 0.03111  2.53047E+03 0.04123 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.49779E+00 0.00376 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36850E+02 0.00161  1.34850E+01 0.01702 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.36552E-01 0.00109  5.25256E-01 0.00061 ];
INF_CAPT                  (idx, [1:   4]) = [  1.78422E-03 0.00406  2.34071E-03 0.00946 ];
INF_ABS                   (idx, [1:   4]) = [  4.05974E-03 0.00379  1.17829E-02 0.01038 ];
INF_FISS                  (idx, [1:   4]) = [  2.27552E-03 0.00375  9.44215E-03 0.01079 ];
INF_NSF                   (idx, [1:   4]) = [  5.57062E-03 0.00375  2.30024E-02 0.01079 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44807E+00 2.9E-05  2.43614E+00 5.9E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.2E-07  2.02270E+02 5.9E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.57395E-08 0.00314  3.09791E-06 0.00263 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.32489E-01 0.00114  5.13365E-01 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  3.74932E-02 0.00294  3.15178E-02 0.00593 ];
INF_SCATT2                (idx, [1:   4]) = [  1.01223E-02 0.00642  3.42847E-03 0.09650 ];
INF_SCATT3                (idx, [1:   4]) = [  8.74166E-04 0.08165  1.05582E-03 0.12430 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.35680E-04 0.08675  5.58643E-04 0.46981 ];
INF_SCATT5                (idx, [1:   4]) = [  3.28287E-05 0.89001  5.08021E-04 0.40316 ];
INF_SCATT6                (idx, [1:   4]) = [  4.49101E-04 0.06913  1.16226E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.83030E-05 0.80032  6.75854E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.32491E-01 0.00114  5.13365E-01 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.74933E-02 0.00294  3.15178E-02 0.00593 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.01224E-02 0.00641  3.42847E-03 0.09650 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.74023E-04 0.08155  1.05582E-03 0.12430 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.35714E-04 0.08690  5.58643E-04 0.46981 ];
INF_SCATTP5               (idx, [1:   4]) = [  3.27458E-05 0.89529  5.08021E-04 0.40316 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.49013E-04 0.06925  1.16226E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.83819E-05 0.79892  6.75854E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.50277E-01 0.00154  4.91886E-01 0.00088 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33187E+00 0.00154  6.77666E-01 0.00088 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.05798E-03 0.00377  1.17829E-02 0.01038 ];
INF_REMXS                 (idx, [1:   4]) = [  5.77968E-03 0.00353  1.23815E-02 0.00851 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.30772E-01 0.00111  1.71728E-03 0.00752  4.90725E-04 0.05706  5.12874E-01 0.00062 ];
INF_S1                    (idx, [1:   8]) = [  3.76608E-02 0.00311 -1.67572E-04 0.04653  1.33263E-04 0.13225  3.13846E-02 0.00621 ];
INF_S2                    (idx, [1:   8]) = [  1.02013E-02 0.00657 -7.89849E-05 0.03629  2.11636E-05 0.24493  3.40730E-03 0.09850 ];
INF_S3                    (idx, [1:   8]) = [  9.67552E-04 0.07263 -9.33856E-05 0.05356 -1.24199E-05 0.28972  1.06824E-03 0.12045 ];
INF_S4                    (idx, [1:   8]) = [ -6.03827E-04 0.09207 -3.18526E-05 0.08505 -1.11758E-05 0.75356  5.69819E-04 0.45228 ];
INF_S5                    (idx, [1:   8]) = [  2.36170E-05 1.00000  9.21175E-06 0.35366 -7.32651E-06 0.88758  5.15347E-04 0.38951 ];
INF_S6                    (idx, [1:   8]) = [  4.38796E-04 0.07145  1.03047E-05 0.25956 -2.49393E-06 0.66671  1.18720E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.63429E-05 0.83297  1.96009E-06 1.00000  1.80612E-06 1.00000  6.57792E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.30774E-01 0.00111  1.71728E-03 0.00752  4.90725E-04 0.05706  5.12874E-01 0.00062 ];
INF_SP1                   (idx, [1:   8]) = [  3.76609E-02 0.00311 -1.67572E-04 0.04653  1.33263E-04 0.13225  3.13846E-02 0.00621 ];
INF_SP2                   (idx, [1:   8]) = [  1.02013E-02 0.00657 -7.89849E-05 0.03629  2.11636E-05 0.24493  3.40730E-03 0.09850 ];
INF_SP3                   (idx, [1:   8]) = [  9.67408E-04 0.07254 -9.33856E-05 0.05356 -1.24199E-05 0.28972  1.06824E-03 0.12045 ];
INF_SP4                   (idx, [1:   8]) = [ -6.03862E-04 0.09223 -3.18526E-05 0.08505 -1.11758E-05 0.75356  5.69819E-04 0.45228 ];
INF_SP5                   (idx, [1:   8]) = [  2.35341E-05 1.00000  9.21175E-06 0.35366 -7.32651E-06 0.88758  5.15347E-04 0.38951 ];
INF_SP6                   (idx, [1:   8]) = [  4.38708E-04 0.07157  1.03047E-05 0.25956 -2.49393E-06 0.66671  1.18720E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.64218E-05 0.83138  1.96009E-06 1.00000  1.80612E-06 1.00000  6.57792E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.78706E-01 0.00178  7.93362E-01 0.03972 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.82683E-01 0.00573  1.05145E+00 0.07351 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.81974E-01 0.00564  1.01796E+00 0.04772 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.71795E-01 0.00212  5.46538E-01 0.03847 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.19602E+00 0.00178  4.22665E-01 0.03749 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.17933E+00 0.00576  3.24281E-01 0.07627 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.18229E+00 0.00557  3.30089E-01 0.04187 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.22643E+00 0.00212  6.13625E-01 0.03951 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
LAMBDA                    (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79538E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64632E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'F' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.44824E+03 0.02999  2.54565E+04 0.02656  5.87359E+04 0.00442  1.03762E+05 0.00948  1.08908E+05 0.00454  1.01427E+05 0.00843  9.05591E+04 0.00727  7.92449E+04 0.00898  6.90652E+04 0.00826  6.06847E+04 0.00723  5.47282E+04 0.00505  4.97915E+04 0.00787  4.56423E+04 0.00991  4.26515E+04 0.01110  4.08129E+04 0.00787  3.42796E+04 0.00745  3.27190E+04 0.00882  3.14118E+04 0.00365  2.90695E+04 0.01191  5.06011E+04 0.00418  4.11653E+04 0.00714  2.49213E+04 0.00518  1.38748E+04 0.00834  1.34081E+04 0.01054  1.06168E+04 0.01439  7.73327E+03 0.02612  1.15920E+04 0.01401  2.21339E+03 0.03648  2.71545E+03 0.04405  2.61516E+03 0.03563  1.56147E+03 0.03839  2.63120E+03 0.01923  1.71568E+03 0.02769  1.37481E+03 0.02273  2.35128E+02 0.09367  2.25067E+02 0.07123  2.18019E+02 0.07567  2.06470E+02 0.07502  2.33686E+02 0.11493  2.34316E+02 0.07642  2.17288E+02 0.07486  2.42470E+02 0.04733  4.30667E+02 0.05714  5.70148E+02 0.06052  7.41824E+02 0.06642  1.96293E+03 0.02913  1.70948E+03 0.03018  1.40923E+03 0.03164  6.69304E+02 0.03220  3.45170E+02 0.05147  2.49200E+02 0.12516  1.93286E+02 0.06044  3.76084E+02 0.06725  3.79338E+02 0.03961  5.24010E+02 0.04939  5.91935E+02 0.02292  6.38353E+02 0.08264  3.38598E+02 0.09921  2.13073E+02 0.02474  1.31324E+02 0.07271  1.27249E+02 0.11736  1.25160E+02 0.10702  1.02727E+02 0.16269  6.12012E+01 0.19162  5.60313E+01 0.12871  3.47785E+01 0.17280  3.35233E+01 0.27572  3.31100E+01 0.20202  1.25617E+01 0.09143  2.20262E+00 0.11596 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.61472E+00 0.00355 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  6.31168E+01 0.00555  4.19059E-01 0.01341 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55420E-01 0.00135  4.71002E-01 0.00490 ];
INF_CAPT                  (idx, [1:   4]) = [  2.34647E-03 0.00540  2.38869E-02 0.00364 ];
INF_ABS                   (idx, [1:   4]) = [  6.30913E-03 0.00323  1.54704E-01 0.00252 ];
INF_FISS                  (idx, [1:   4]) = [  3.96266E-03 0.00228  1.30817E-01 0.00235 ];
INF_NSF                   (idx, [1:   4]) = [  9.70373E-03 0.00224  3.18688E-01 0.00235 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44879E+00 4.1E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.9E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.42359E-08 0.00368  1.61281E-06 0.01237 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49132E-01 0.00138  3.15042E-01 0.01077 ];
INF_SCATT1                (idx, [1:   4]) = [  2.05591E-02 0.00668  1.70442E-02 0.08202 ];
INF_SCATT2                (idx, [1:   4]) = [  4.28058E-03 0.00825  3.35375E-03 0.50778 ];
INF_SCATT3                (idx, [1:   4]) = [  9.56428E-04 0.08659 -5.60594E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  2.04625E-04 0.29244 -3.49352E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  7.42942E-05 0.79619  1.41971E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.01747E-04 0.60276  1.26055E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.06185E-04 0.29989  3.16971E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49135E-01 0.00138  3.15042E-01 0.01077 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.05595E-02 0.00669  1.70442E-02 0.08202 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.28037E-03 0.00822  3.35375E-03 0.50778 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.56176E-04 0.08659 -5.60594E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.04459E-04 0.29240 -3.49352E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.43017E-05 0.79403  1.41971E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.01545E-04 0.60422  1.26055E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.06261E-04 0.30070  3.16971E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.01830E-01 0.00199  4.33695E-01 0.00835 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.65158E+00 0.00199  7.68804E-01 0.00836 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.30624E-03 0.00324  1.54704E-01 0.00252 ];
INF_REMXS                 (idx, [1:   4]) = [  6.62561E-03 0.00335  1.57626E-01 0.01277 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48794E-01 0.00136  3.37691E-04 0.02586  1.66577E-03 0.09719  3.13376E-01 0.01103 ];
INF_S1                    (idx, [1:   8]) = [  2.06328E-02 0.00662 -7.36611E-05 0.03430  2.77218E-04 0.15721  1.67669E-02 0.08312 ];
INF_S2                    (idx, [1:   8]) = [  4.29381E-03 0.00851 -1.32344E-05 0.11242  5.84343E-05 1.00000  3.29531E-03 0.51015 ];
INF_S3                    (idx, [1:   8]) = [  9.61768E-04 0.08467 -5.34028E-06 0.40307 -5.25518E-05 1.00000 -5.08042E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  2.09733E-04 0.29274 -5.10838E-06 0.32060 -5.11286E-05 1.00000 -2.98223E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  7.41665E-05 0.80112  1.27626E-07 1.00000 -7.52898E-05 0.63114  2.17261E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  9.94886E-05 0.61488  2.25863E-06 1.00000 -1.50963E-05 1.00000  1.41151E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.07059E-04 0.29393 -8.74559E-07 1.00000  3.33903E-05 1.00000 -1.69321E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48797E-01 0.00136  3.37691E-04 0.02586  1.66577E-03 0.09719  3.13376E-01 0.01103 ];
INF_SP1                   (idx, [1:   8]) = [  2.06331E-02 0.00663 -7.36611E-05 0.03430  2.77218E-04 0.15721  1.67669E-02 0.08312 ];
INF_SP2                   (idx, [1:   8]) = [  4.29360E-03 0.00847 -1.32344E-05 0.11242  5.84343E-05 1.00000  3.29531E-03 0.51015 ];
INF_SP3                   (idx, [1:   8]) = [  9.61517E-04 0.08468 -5.34028E-06 0.40307 -5.25518E-05 1.00000 -5.08042E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  2.09567E-04 0.29271 -5.10838E-06 0.32060 -5.11286E-05 1.00000 -2.98223E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  7.41740E-05 0.79892  1.27626E-07 1.00000 -7.52898E-05 0.63114  2.17261E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  9.92861E-05 0.61636  2.25863E-06 1.00000 -1.50963E-05 1.00000  1.41151E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.07136E-04 0.29462 -8.74559E-07 1.00000  3.33903E-05 1.00000 -1.69321E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.43287E-01 0.00445 -1.84188E+00 1.00000 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  3.98458E-01 0.01365 -9.15481E-01 0.30850 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  3.97524E-01 0.00961 -1.20739E+00 0.37754 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.69441E-01 0.00448  4.21532E-01 0.08961 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  9.71082E-01 0.00444 -4.19129E-02 1.00000 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  8.37179E-01 0.01359 -5.11865E-01 0.25387 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  8.38840E-01 0.00979 -4.28650E-01 0.26417 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.23723E+00 0.00445  8.14776E-01 0.08275 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.23503E-03 0.06269  2.85079E-04 0.29210  9.79122E-04 0.17267  8.48122E-04 0.21106  1.35942E-03 0.14720  2.43654E-03 0.11654  5.75994E-04 0.19772  5.85865E-04 0.21302  1.64893E-04 0.38437 ];
LAMBDA                    (idx, [1:  18]) = [  4.47993E-01 0.12741  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79539E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64632E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'T' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.74818E+02 0.07290  1.68135E+03 0.02583  3.67399E+03 0.03233  6.76909E+03 0.01960  7.10168E+03 0.02239  6.61439E+03 0.01551  5.81879E+03 0.01332  5.15889E+03 0.03250  4.56919E+03 0.01158  4.13435E+03 0.01806  3.67482E+03 0.01033  3.37706E+03 0.02351  3.04563E+03 0.03287  3.00806E+03 0.02426  2.68003E+03 0.01051  2.37666E+03 0.03432  2.21599E+03 0.01794  2.19530E+03 0.02852  1.97998E+03 0.01522  3.59902E+03 0.02717  2.91430E+03 0.01621  1.73842E+03 0.05054  1.04806E+03 0.05313  9.89342E+02 0.02292  8.77700E+02 0.04427  5.81002E+02 0.07488  8.06744E+02 0.05429  1.35353E+02 0.14296  2.14032E+02 0.11373  1.61495E+02 0.07563  1.04142E+02 0.08314  1.79170E+02 0.08874  1.24211E+02 0.08030  8.98974E+01 0.15601  1.38704E+01 0.29707  1.01486E+01 0.21820  1.26944E+01 0.38464  1.60677E+01 0.41480  1.81356E+01 0.30382  2.29784E+01 0.42113  1.03890E+01 0.12334  2.02931E+01 0.20566  3.11878E+01 0.17401  4.43936E+01 0.17424  5.02111E+01 0.10608  1.45715E+02 0.06312  1.33940E+02 0.16492  1.01895E+02 0.10873  5.92004E+01 0.23010  3.19341E+01 0.08669  2.27732E+01 0.26681  1.79189E+01 0.27531  4.26750E+01 0.10401  4.06821E+01 0.24281  4.64979E+01 0.09608  5.15557E+01 0.16865  6.27119E+01 0.14577  2.36777E+01 0.33170  1.83066E+01 0.15095  1.46010E+01 0.23737  1.45539E+01 0.24781  2.18118E+01 0.22195  1.41881E+01 0.25294  9.18242E+00 0.17131  5.30502E+00 0.17396  6.84752E+00 0.22929  9.74223E+00 0.40010  1.12455E+01 0.26022  4.81480E+00 0.14446  6.63747E+00 0.72737 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.22777E+00 0.00583  3.87550E-02 0.07149 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.24412E-01 0.00507  6.47250E-01 0.04583 ];
INF_CAPT                  (idx, [1:   4]) = [  8.67752E-03 0.01287  2.45535E-02 0.09001 ];
INF_ABS                   (idx, [1:   4]) = [  8.67752E-03 0.01287  2.45535E-02 0.09001 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.56534E-08 0.01425  1.92260E-06 0.02365 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.15720E-01 0.00513  6.22549E-01 0.04818 ];
INF_SCATT1                (idx, [1:   4]) = [  1.54202E-01 0.00665  2.24002E-01 0.06586 ];
INF_SCATT2                (idx, [1:   4]) = [  6.37497E-02 0.00473  8.53422E-02 0.08996 ];
INF_SCATT3                (idx, [1:   4]) = [  2.04537E-03 0.24676  2.25542E-02 0.24725 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.81643E-03 0.08079  3.50964E-03 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  2.26835E-04 1.00000  5.84354E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  3.53776E-03 0.07124  4.34778E-03 0.76104 ];
INF_SCATT7                (idx, [1:   4]) = [  3.46349E-04 1.00000 -3.11714E-03 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.15725E-01 0.00514  6.22549E-01 0.04818 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.54202E-01 0.00665  2.24002E-01 0.06586 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.37492E-02 0.00473  8.53422E-02 0.08996 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.04561E-03 0.24671  2.25542E-02 0.24725 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.81763E-03 0.08080  3.50964E-03 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.25800E-04 1.00000  5.84354E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.53879E-03 0.07121  4.34778E-03 0.76104 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.47128E-04 1.00000 -3.11714E-03 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.01572E-01 0.00856  3.56757E-01 0.06879 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.65415E+00 0.00848  9.52692E-01 0.07043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.67289E-03 0.01283  2.45535E-02 0.09001 ];
INF_REMXS                 (idx, [1:   4]) = [  1.18974E-02 0.00881  2.64287E-02 0.13157 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.12515E-01 0.00522  3.20569E-03 0.03163  1.72774E-03 0.49030  6.20822E-01 0.04848 ];
INF_S1                    (idx, [1:   8]) = [  1.53093E-01 0.00664  1.10843E-03 0.05426  1.30450E-03 0.48912  2.22697E-01 0.06576 ];
INF_S2                    (idx, [1:   8]) = [  6.41411E-02 0.00453 -3.91406E-04 0.03942  8.42688E-04 0.67451  8.44995E-02 0.09067 ];
INF_S3                    (idx, [1:   8]) = [  2.60776E-03 0.19503 -5.62386E-04 0.04687  6.52322E-04 0.84871  2.19018E-02 0.25807 ];
INF_S4                    (idx, [1:   8]) = [ -6.68398E-03 0.08455 -1.32451E-04 0.17652  6.46879E-04 0.81878  2.86276E-03 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.22904E-04 1.00000  1.03931E-04 0.15349  5.47587E-04 0.90284  3.67670E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  3.47186E-03 0.07751  6.59053E-05 0.41490  2.93632E-04 1.00000  4.05415E-03 0.74759 ];
INF_S7                    (idx, [1:   8]) = [  3.59007E-04 1.00000 -1.26582E-05 1.00000  9.97349E-05 1.00000 -3.21688E-03 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.12519E-01 0.00523  3.20569E-03 0.03163  1.72774E-03 0.49030  6.20822E-01 0.04848 ];
INF_SP1                   (idx, [1:   8]) = [  1.53094E-01 0.00664  1.10843E-03 0.05426  1.30450E-03 0.48912  2.22697E-01 0.06576 ];
INF_SP2                   (idx, [1:   8]) = [  6.41406E-02 0.00453 -3.91406E-04 0.03942  8.42688E-04 0.67451  8.44995E-02 0.09067 ];
INF_SP3                   (idx, [1:   8]) = [  2.60799E-03 0.19499 -5.62386E-04 0.04687  6.52322E-04 0.84871  2.19018E-02 0.25807 ];
INF_SP4                   (idx, [1:   8]) = [ -6.68518E-03 0.08456 -1.32451E-04 0.17652  6.46879E-04 0.81878  2.86276E-03 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.21869E-04 1.00000  1.03931E-04 0.15349  5.47587E-04 0.90284  3.67670E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  3.47289E-03 0.07747  6.59053E-05 0.41490  2.93632E-04 1.00000  4.05415E-03 0.74759 ];
INF_SP7                   (idx, [1:   8]) = [  3.59786E-04 1.00000 -1.26582E-05 1.00000  9.97349E-05 1.00000 -3.21688E-03 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.23793E-01 0.01207 -7.13720E-01 0.70033 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.59747E-01 0.02478 -3.16772E-01 1.00000 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.51719E-01 0.04420 -2.31597E-01 0.89329 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.80429E-01 0.01772 -5.42100E-01 1.00000 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.49033E+00 0.01194 -2.74887E-01 0.72369 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.28639E+00 0.02427 -3.77742E-01 0.48110 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.33483E+00 0.04508 -6.19582E-01 0.55560 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.84976E+00 0.01762  1.72662E-01 1.00000 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79540E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64630E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'C' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  6.94656E+02 0.03460  3.89253E+03 0.02440  8.43992E+03 0.01218  1.51715E+04 0.01098  1.61651E+04 0.00729  1.49186E+04 0.01699  1.34356E+04 0.01462  1.14796E+04 0.01251  1.02205E+04 0.01511  9.27374E+03 0.01186  7.94783E+03 0.01238  7.64720E+03 0.01170  6.81854E+03 0.01123  6.59254E+03 0.00883  6.30986E+03 0.01331  5.24216E+03 0.01667  5.16014E+03 0.00704  4.90726E+03 0.01489  4.88550E+03 0.00387  8.79835E+03 0.01184  7.39184E+03 0.00571  4.72620E+03 0.01680  2.80421E+03 0.02351  2.86029E+03 0.01902  2.55483E+03 0.03081  1.90679E+03 0.03895  2.95167E+03 0.03287  5.82614E+02 0.04022  7.02212E+02 0.05414  6.32518E+02 0.04185  3.59682E+02 0.04461  6.28685E+02 0.03750  4.00068E+02 0.05303  3.39098E+02 0.02355  7.74208E+01 0.05826  6.39063E+01 0.12306  7.24941E+01 0.18605  5.05504E+01 0.11255  7.02797E+01 0.24499  8.96514E+01 0.03070  6.50518E+01 0.07613  7.85510E+01 0.08801  9.47359E+01 0.05394  1.61209E+02 0.07105  2.04887E+02 0.06448  5.19069E+02 0.05905  5.34995E+02 0.07271  4.28040E+02 0.05627  2.45303E+02 0.03975  1.60723E+02 0.13278  8.94260E+01 0.08713  9.62031E+01 0.07751  1.50808E+02 0.05487  1.90667E+02 0.08180  2.84962E+02 0.07322  4.32441E+02 0.03399  4.86519E+02 0.04077  3.40939E+02 0.08315  2.35558E+02 0.07495  1.84152E+02 0.15367  1.90025E+02 0.07302  2.07660E+02 0.12464  1.67191E+02 0.10642  1.20156E+02 0.08033  1.25178E+02 0.04428  1.16658E+02 0.07298  1.05885E+02 0.06093  8.17119E+01 0.07703  6.42523E+01 0.07282  1.66978E+01 0.11446 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.94812E+00 0.00309  2.53492E-01 0.03131 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.99191E-01 0.00335  7.35913E-01 0.01421 ];
INF_CAPT                  (idx, [1:   4]) = [  1.77135E-03 0.00938  1.49880E-02 0.02240 ];
INF_ABS                   (idx, [1:   4]) = [  1.77135E-03 0.00938  1.49880E-02 0.02240 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.44841E-08 0.00394  2.34523E-06 0.00472 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.97388E-01 0.00325  7.20678E-01 0.01505 ];
INF_SCATT1                (idx, [1:   4]) = [  1.60391E-01 0.00171  2.16662E-01 0.01523 ];
INF_SCATT2                (idx, [1:   4]) = [  6.40467E-02 0.00400  8.13199E-02 0.03257 ];
INF_SCATT3                (idx, [1:   4]) = [  2.43823E-03 0.10098  2.91113E-02 0.06769 ];
INF_SCATT4                (idx, [1:   4]) = [ -7.20905E-03 0.05326  1.13640E-02 0.15378 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.49121E-04 1.00000  3.00133E-03 0.60845 ];
INF_SCATT6                (idx, [1:   4]) = [  3.47429E-03 0.06460  1.29610E-03 0.93751 ];
INF_SCATT7                (idx, [1:   4]) = [  7.05035E-05 1.00000  6.68933E-04 0.28736 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.97389E-01 0.00324  7.20678E-01 0.01505 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.60391E-01 0.00171  2.16662E-01 0.01523 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.40470E-02 0.00399  8.13199E-02 0.03257 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.43895E-03 0.10084  2.91113E-02 0.06769 ];
INF_SCATTP4               (idx, [1:   4]) = [ -7.20872E-03 0.05327  1.13640E-02 0.15378 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.49523E-04 1.00000  3.00133E-03 0.60845 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.47398E-03 0.06466  1.29610E-03 0.93751 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.03770E-05 1.00000  6.68933E-04 0.28736 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.70093E-01 0.00140  4.90239E-01 0.01858 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.23415E+00 0.00141  6.80871E-01 0.01839 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.76943E-03 0.00993  1.49880E-02 0.02240 ];
INF_REMXS                 (idx, [1:   4]) = [  6.99127E-03 0.01651  1.67271E-02 0.04460 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.92200E-01 0.00317  5.18767E-03 0.01393  1.49233E-03 0.13736  7.19186E-01 0.01513 ];
INF_S1                    (idx, [1:   8]) = [  1.58662E-01 0.00172  1.72899E-03 0.01734  4.60941E-04 0.36137  2.16201E-01 0.01560 ];
INF_S2                    (idx, [1:   8]) = [  6.46553E-02 0.00389 -6.08599E-04 0.01267  4.31656E-04 0.28344  8.08882E-02 0.03240 ];
INF_S3                    (idx, [1:   8]) = [  3.38207E-03 0.06395 -9.43843E-04 0.03278  1.54623E-04 0.73441  2.89566E-02 0.07116 ];
INF_S4                    (idx, [1:   8]) = [ -6.89327E-03 0.05510 -3.15781E-04 0.03521  3.22633E-05 1.00000  1.13317E-02 0.15497 ];
INF_S5                    (idx, [1:   8]) = [ -2.30674E-04 1.00000  8.15524E-05 0.23809  3.13977E-05 1.00000  2.96994E-03 0.60888 ];
INF_S6                    (idx, [1:   8]) = [  3.37343E-03 0.06671  1.00864E-04 0.17506 -5.36240E-05 1.00000  1.34973E-03 0.87469 ];
INF_S7                    (idx, [1:   8]) = [  2.17834E-05 1.00000  4.87201E-05 0.21623 -2.48719E-05 1.00000  6.93805E-04 0.25838 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.92202E-01 0.00317  5.18767E-03 0.01393  1.49233E-03 0.13736  7.19186E-01 0.01513 ];
INF_SP1                   (idx, [1:   8]) = [  1.58662E-01 0.00171  1.72899E-03 0.01734  4.60941E-04 0.36137  2.16201E-01 0.01560 ];
INF_SP2                   (idx, [1:   8]) = [  6.46556E-02 0.00388 -6.08599E-04 0.01267  4.31656E-04 0.28344  8.08882E-02 0.03240 ];
INF_SP3                   (idx, [1:   8]) = [  3.38279E-03 0.06386 -9.43843E-04 0.03278  1.54623E-04 0.73441  2.89566E-02 0.07116 ];
INF_SP4                   (idx, [1:   8]) = [ -6.89294E-03 0.05511 -3.15781E-04 0.03521  3.22633E-05 1.00000  1.13317E-02 0.15497 ];
INF_SP5                   (idx, [1:   8]) = [ -2.31076E-04 1.00000  8.15524E-05 0.23809  3.13977E-05 1.00000  2.96994E-03 0.60888 ];
INF_SP6                   (idx, [1:   8]) = [  3.37312E-03 0.06678  1.00864E-04 0.17506 -5.36240E-05 1.00000  1.34973E-03 0.87469 ];
INF_SP7                   (idx, [1:   8]) = [  2.16569E-05 1.00000  4.87201E-05 0.21623 -2.48719E-05 1.00000  6.93805E-04 0.25838 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.19889E-01 0.00898 -3.03599E-01 0.05792 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.61386E-01 0.02619 -1.62243E-01 0.06986 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.57412E-01 0.04039 -1.62837E-01 0.07379 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.69590E-01 0.00912  6.01074E-01 0.33708 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.51640E+00 0.00888 -1.11087E+00 0.05035 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.27884E+00 0.02682 -2.10093E+00 0.07974 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.30419E+00 0.04398 -2.09433E+00 0.07695 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.96618E+00 0.00912  8.62636E-01 0.28444 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79543E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64630E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '9' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.61884E+02 0.07360  1.21772E+03 0.09296  2.87285E+03 0.01837  4.88898E+03 0.01463  5.34025E+03 0.01893  4.64414E+03 0.01615  4.21902E+03 0.01480  3.52484E+03 0.02558  3.10407E+03 0.02307  2.76558E+03 0.03764  2.58780E+03 0.02763  2.33571E+03 0.04019  2.11325E+03 0.03917  2.00815E+03 0.02142  1.90772E+03 0.03222  1.63394E+03 0.03914  1.49619E+03 0.04666  1.39958E+03 0.03302  1.40426E+03 0.01845  2.53874E+03 0.02197  2.21438E+03 0.00697  1.37816E+03 0.03125  8.71116E+02 0.04193  8.51314E+02 0.02124  6.94183E+02 0.05782  4.75610E+02 0.09224  8.55300E+02 0.02062  1.98224E+02 0.02647  2.20898E+02 0.07060  1.92149E+02 0.05402  1.12688E+02 0.13521  2.21084E+02 0.10620  1.71493E+02 0.10050  1.14778E+02 0.12777  1.45581E+01 0.19231  1.68900E+01 0.47503  1.10835E+01 0.14284  1.47680E+01 0.37165  2.06181E+01 0.30169  2.07885E+01 0.31415  2.28848E+01 0.35742  2.18991E+01 0.26093  2.18162E+01 0.11741  5.43694E+01 0.19509  7.08289E+01 0.08825  1.55205E+02 0.13635  1.41684E+02 0.13022  1.20513E+02 0.07864  6.53679E+01 0.06956  5.06743E+01 0.16179  2.69993E+01 0.16719  2.06741E+01 0.13999  4.53305E+01 0.17208  3.37354E+01 0.09857  7.61378E+01 0.20155  9.43476E+01 0.17315  1.06646E+02 0.13502  5.64403E+01 0.16389  3.70305E+01 0.21813  2.15037E+01 0.18046  2.93214E+01 0.20498  2.03194E+01 0.16608  2.52027E+01 0.35504  1.59381E+01 0.31285  6.44885E+00 0.13763  1.07254E+01 0.39731  8.67036E+00 0.44113  8.45997E+00 0.46501  2.36578E+00 0.24520  5.02344E-01 0.18642 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.67229E+00 0.02306 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.07288E+00 0.00698  5.14018E-02 0.05201 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.54453E-01 0.00558  4.66426E-01 0.02989 ];
INF_CAPT                  (idx, [1:   4]) = [  2.44523E-03 0.01035  2.43591E-02 0.03122 ];
INF_ABS                   (idx, [1:   4]) = [  6.46369E-03 0.00675  1.57891E-01 0.02930 ];
INF_FISS                  (idx, [1:   4]) = [  4.01845E-03 0.00589  1.33532E-01 0.02901 ];
INF_NSF                   (idx, [1:   4]) = [  9.83225E-03 0.00582  3.25303E-01 0.02901 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44678E+00 7.0E-05  2.43614E+00 5.9E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 4.3E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.38879E-08 0.01868  1.87629E-06 0.01053 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.48029E-01 0.00524  3.03398E-01 0.02864 ];
INF_SCATT1                (idx, [1:   4]) = [  2.02191E-02 0.02853  2.16171E-02 0.17474 ];
INF_SCATT2                (idx, [1:   4]) = [  5.03749E-03 0.08968  8.88341E-03 0.43712 ];
INF_SCATT3                (idx, [1:   4]) = [  7.06028E-04 0.42658  4.16434E-03 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  3.69337E-04 0.76502 -8.51006E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.30863E-04 1.00000 -5.56672E-03 0.35939 ];
INF_SCATT6                (idx, [1:   4]) = [  1.05582E-04 1.00000 -2.01780E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -1.60924E-04 1.00000 -1.71696E-03 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.48032E-01 0.00524  3.03398E-01 0.02864 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.02160E-02 0.02844  2.16171E-02 0.17474 ];
INF_SCATTP2               (idx, [1:   4]) = [  5.04017E-03 0.08973  8.88341E-03 0.43712 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.03808E-04 0.42898  4.16434E-03 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.70998E-04 0.76412 -8.51006E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.31913E-04 1.00000 -5.56672E-03 0.35939 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.06019E-04 1.00000 -2.01780E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -1.60791E-04 1.00000 -1.71696E-03 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02196E-01 0.00932  3.75103E-01 0.02111 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64915E+00 0.00953  8.90216E-01 0.02089 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.46049E-03 0.00706  1.57891E-01 0.02930 ];
INF_REMXS                 (idx, [1:   4]) = [  6.92621E-03 0.01764  1.63994E-01 0.04346 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.47527E-01 0.00533  5.02002E-04 0.06000  9.65234E-04 0.50473  3.02433E-01 0.02936 ];
INF_S1                    (idx, [1:   8]) = [  2.03260E-02 0.02754 -1.06891E-04 0.22752  5.07665E-04 0.56241  2.11095E-02 0.18727 ];
INF_S2                    (idx, [1:   8]) = [  5.03712E-03 0.09067  3.71418E-07 1.00000  1.16698E-04 1.00000  8.76671E-03 0.42330 ];
INF_S3                    (idx, [1:   8]) = [  7.40896E-04 0.41517 -3.48677E-05 0.24753  1.34526E-04 0.97019  4.02981E-03 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  3.57029E-04 0.80165  1.23086E-05 1.00000  3.21100E-04 0.68317 -1.17211E-03 1.00000 ];
INF_S5                    (idx, [1:   8]) = [ -1.46669E-04 1.00000  1.58062E-05 0.93299  2.82308E-04 0.95957 -5.84903E-03 0.33755 ];
INF_S6                    (idx, [1:   8]) = [  1.11865E-04 1.00000 -6.28302E-06 1.00000  3.68502E-05 1.00000 -2.38630E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -1.63064E-04 1.00000  2.13993E-06 1.00000 -8.62827E-05 1.00000 -1.63068E-03 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.47530E-01 0.00533  5.02002E-04 0.06000  9.65234E-04 0.50473  3.02433E-01 0.02936 ];
INF_SP1                   (idx, [1:   8]) = [  2.03229E-02 0.02745 -1.06891E-04 0.22752  5.07665E-04 0.56241  2.11095E-02 0.18727 ];
INF_SP2                   (idx, [1:   8]) = [  5.03980E-03 0.09073  3.71418E-07 1.00000  1.16698E-04 1.00000  8.76671E-03 0.42330 ];
INF_SP3                   (idx, [1:   8]) = [  7.38675E-04 0.41744 -3.48677E-05 0.24753  1.34526E-04 0.97019  4.02981E-03 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  3.58689E-04 0.80036  1.23086E-05 1.00000  3.21100E-04 0.68317 -1.17211E-03 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [ -1.47719E-04 1.00000  1.58062E-05 0.93299  2.82308E-04 0.95957 -5.84903E-03 0.33755 ];
INF_SP6                   (idx, [1:   8]) = [  1.12302E-04 1.00000 -6.28302E-06 1.00000  3.68502E-05 1.00000 -2.38630E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -1.62931E-04 1.00000  2.13993E-06 1.00000 -8.62827E-05 1.00000 -1.63068E-03 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.99639E-01 0.01401 -2.39430E-01 0.05241 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.03508E-01 0.03319 -1.25833E-01 0.04347 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.15689E-01 0.03985 -1.22103E-01 0.08331 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.81843E-01 0.03436  2.82650E-01 0.12371 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  8.34749E-01 0.01417 -1.40710E+00 0.05055 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.64984E-01 0.03359 -2.66873E+00 0.04258 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.50391E-01 0.03876 -2.81021E+00 0.08654 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.18887E+00 0.03787  1.25764E+00 0.12728 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.49374E-03 0.16023  1.69164E-04 0.53536  1.32730E-03 0.43070  5.00687E-04 0.38731  6.74061E-04 0.34469  2.30040E-03 0.24540  8.55998E-04 0.42431  1.48118E-03 0.47678  1.84946E-04 0.95078 ];
LAMBDA                    (idx, [1:  18]) = [  4.83334E-01 0.14214  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 1.3E-08 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79545E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64628E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '8' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.51567E+02 0.11853  1.54596E+03 0.01543  3.81469E+03 0.01560  6.72188E+03 0.01812  6.81885E+03 0.01473  6.37054E+03 0.02393  5.68304E+03 0.01831  4.80662E+03 0.02211  4.27892E+03 0.01514  3.87538E+03 0.02932  3.44302E+03 0.01592  3.06540E+03 0.03673  2.79951E+03 0.01885  2.65534E+03 0.02310  2.54706E+03 0.03577  2.21919E+03 0.01990  2.15185E+03 0.02300  2.09419E+03 0.01786  1.84457E+03 0.03214  3.50014E+03 0.01484  2.91708E+03 0.02808  1.88608E+03 0.03288  1.10781E+03 0.05611  1.20268E+03 0.03003  1.03579E+03 0.04597  7.62963E+02 0.02656  1.20927E+03 0.04435  2.47332E+02 0.05045  3.45111E+02 0.07557  3.13339E+02 0.07630  1.94054E+02 0.11999  2.56916E+02 0.06546  2.22479E+02 0.05413  1.28197E+02 0.07664  2.03689E+01 0.28243  2.47233E+01 0.27167  2.47178E+01 0.22019  3.14351E+01 0.28264  3.54154E+01 0.32093  1.81261E+01 0.32817  2.43935E+01 0.15964  2.58341E+01 0.23033  4.97643E+01 0.09900  7.44624E+01 0.10919  9.72727E+01 0.15043  2.63570E+02 0.05613  2.35093E+02 0.10570  1.79958E+02 0.09927  1.07783E+02 0.11416  5.38794E+01 0.11669  3.38548E+01 0.18575  4.13748E+01 0.19900  7.66167E+01 0.14471  6.63079E+01 0.01723  1.09030E+02 0.13606  1.36806E+02 0.13868  1.90738E+02 0.07825  1.08333E+02 0.12985  8.63742E+01 0.14403  5.54423E+01 0.08264  6.21108E+01 0.11226  4.75344E+01 0.08769  3.71943E+01 0.12058  3.24696E+01 0.14303  2.33098E+01 0.19457  1.53891E+01 0.22060  2.39041E+01 0.23563  1.33957E+01 0.26783  6.17161E+00 0.10878  1.10624E+00 0.27384 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.65709E+00 0.01484 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.16730E+00 0.00568  8.74544E-02 0.01312 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.48940E-01 0.00619  4.59542E-01 0.01396 ];
INF_CAPT                  (idx, [1:   4]) = [  2.29494E-03 0.01755  2.29254E-02 0.02225 ];
INF_ABS                   (idx, [1:   4]) = [  5.94355E-03 0.01765  1.48506E-01 0.02316 ];
INF_FISS                  (idx, [1:   4]) = [  3.64861E-03 0.02133  1.25581E-01 0.02334 ];
INF_NSF                   (idx, [1:   4]) = [  8.92363E-03 0.02125  3.05933E-01 0.02334 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44578E+00 9.7E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 4.9E-07  2.02270E+02 5.9E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.54631E-08 0.02225  1.99404E-06 0.01511 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.42843E-01 0.00635  3.08817E-01 0.01415 ];
INF_SCATT1                (idx, [1:   4]) = [  1.88617E-02 0.01936  1.53535E-02 0.28073 ];
INF_SCATT2                (idx, [1:   4]) = [  4.24911E-03 0.05256  7.33844E-04 1.00000 ];
INF_SCATT3                (idx, [1:   4]) = [  9.82805E-04 0.36735 -1.75508E-03 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.14585E-04 1.00000 -3.50974E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [ -7.92763E-05 1.00000 -9.95417E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  2.07065E-04 1.00000 -4.11561E-03 0.30426 ];
INF_SCATT7                (idx, [1:   4]) = [  1.55435E-04 1.00000 -8.31284E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.42845E-01 0.00635  3.08817E-01 0.01415 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.88596E-02 0.01946  1.53535E-02 0.28073 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.25084E-03 0.05217  7.33844E-04 1.00000 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.81609E-04 0.36686 -1.75508E-03 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.15189E-04 1.00000 -3.50974E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [ -7.93069E-05 1.00000 -9.95417E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.06613E-04 1.00000 -4.11561E-03 0.30426 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.56225E-04 1.00000 -8.31284E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.97954E-01 0.00486  4.01496E-01 0.01732 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.68405E+00 0.00484  8.31221E-01 0.01725 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.94120E-03 0.01785  1.48506E-01 0.02316 ];
INF_REMXS                 (idx, [1:   4]) = [  6.79482E-03 0.00883  1.51641E-01 0.01745 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.42145E-01 0.00640  6.97687E-04 0.02336  9.16370E-04 0.49960  3.07901E-01 0.01454 ];
INF_S1                    (idx, [1:   8]) = [  1.90311E-02 0.01890 -1.69354E-04 0.09339  2.91992E-04 0.58568  1.50615E-02 0.28760 ];
INF_S2                    (idx, [1:   8]) = [  4.24248E-03 0.05241  6.62943E-06 1.00000 -1.53420E-04 0.70925  8.87264E-04 1.00000 ];
INF_S3                    (idx, [1:   8]) = [  9.70714E-04 0.36973  1.20914E-05 0.56345 -1.16358E-04 1.00000 -1.63872E-03 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.37706E-04 1.00000 -2.31211E-05 0.51417  6.33040E-05 0.85785 -4.14278E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [ -9.01130E-05 1.00000  1.08367E-05 0.96255  1.67116E-04 0.53958 -1.16253E-03 0.91915 ];
INF_S6                    (idx, [1:   8]) = [  2.12029E-04 1.00000 -4.96324E-06 1.00000  1.43435E-04 0.90414 -4.25904E-03 0.27987 ];
INF_S7                    (idx, [1:   8]) = [  1.50828E-04 1.00000  4.60774E-06 1.00000 -1.59447E-05 1.00000 -8.15339E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.42147E-01 0.00640  6.97687E-04 0.02336  9.16370E-04 0.49960  3.07901E-01 0.01454 ];
INF_SP1                   (idx, [1:   8]) = [  1.90290E-02 0.01900 -1.69354E-04 0.09339  2.91992E-04 0.58568  1.50615E-02 0.28760 ];
INF_SP2                   (idx, [1:   8]) = [  4.24421E-03 0.05201  6.62943E-06 1.00000 -1.53420E-04 0.70925  8.87264E-04 1.00000 ];
INF_SP3                   (idx, [1:   8]) = [  9.69517E-04 0.36923  1.20914E-05 0.56345 -1.16358E-04 1.00000 -1.63872E-03 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.38311E-04 1.00000 -2.31211E-05 0.51417  6.33040E-05 0.85785 -4.14278E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [ -9.01436E-05 1.00000  1.08367E-05 0.96255  1.67116E-04 0.53958 -1.16253E-03 0.91915 ];
INF_SP6                   (idx, [1:   8]) = [  2.11576E-04 1.00000 -4.96324E-06 1.00000  1.43435E-04 0.90414 -4.25904E-03 0.27987 ];
INF_SP7                   (idx, [1:   8]) = [  1.51618E-04 1.00000  4.60774E-06 1.00000 -1.59447E-05 1.00000 -8.15339E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.18673E-01 0.02939 -1.87932E-01 0.09490 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.46837E-01 0.06669 -1.14652E-01 0.09590 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.00403E-01 0.03649 -1.05642E-01 0.17228 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.02370E-01 0.02900 -6.15470E-02 1.00000 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.98761E-01 0.02767 -1.83384E+00 0.08735 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.20303E-01 0.06510 -3.02481E+00 0.10189 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.69832E-01 0.03792 -3.52648E+00 0.16375 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.10615E+00 0.02923  1.04978E+00 0.40653 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  5.88551E-03 0.16827  6.83091E-05 0.65329  7.99759E-04 0.27171  2.54042E-04 0.42003  1.56548E-03 0.35428  2.02684E-03 0.28334  2.34191E-04 0.43173  5.03244E-04 0.40055  4.33641E-04 0.86937 ];
LAMBDA                    (idx, [1:  18]) = [  5.08215E-01 0.14823  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 7.2E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79546E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64628E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '7' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.94451E+02 0.18636  1.05921E+03 0.06361  2.42739E+03 0.05585  4.14703E+03 0.03757  4.33371E+03 0.01949  3.87020E+03 0.03516  3.37347E+03 0.04560  2.81470E+03 0.04611  2.56635E+03 0.03687  2.18144E+03 0.04356  1.99319E+03 0.03556  1.92335E+03 0.04231  1.88807E+03 0.01435  1.72423E+03 0.01387  1.62785E+03 0.04728  1.34041E+03 0.02894  1.29042E+03 0.07252  1.27533E+03 0.05306  1.22763E+03 0.03666  2.17898E+03 0.02542  1.97827E+03 0.00984  1.39463E+03 0.02150  7.87206E+02 0.07267  8.03379E+02 0.04594  7.25733E+02 0.03872  5.80141E+02 0.03222  8.73016E+02 0.02136  1.73182E+02 0.10115  2.16541E+02 0.05725  1.78558E+02 0.02449  1.29839E+02 0.16683  1.91140E+02 0.12007  1.47177E+02 0.05196  1.47895E+02 0.08423  2.09484E+01 0.19875  1.11879E+01 0.37298  1.83214E+01 0.19666  2.07672E+01 0.13832  2.37344E+01 0.11904  1.65691E+01 0.27708  1.28293E+01 0.31183  1.49608E+01 0.25997  2.05987E+01 0.19300  5.02964E+01 0.21638  7.13708E+01 0.12459  1.64361E+02 0.14364  1.52279E+02 0.09456  1.50965E+02 0.06265  6.96068E+01 0.03173  4.29765E+01 0.27760  2.77578E+01 0.15115  3.31534E+01 0.15972  6.78430E+01 0.20780  6.36536E+01 0.28276  8.69615E+01 0.10161  1.01134E+02 0.12699  1.56592E+02 0.10208  9.64391E+01 0.14643  7.09499E+01 0.17143  5.25637E+01 0.15667  5.30835E+01 0.15490  5.15664E+01 0.18851  4.37924E+01 0.18546  2.45449E+01 0.20445  2.56439E+01 0.26261  2.27323E+01 0.29159  1.37503E+01 0.35864  1.17736E+01 0.17261  3.41111E+00 0.31937  1.82225E+00 0.12634 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.71128E+00 0.01698 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.61799E+00 0.01225  7.14693E-02 0.03394 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.49610E-01 0.00632  4.46120E-01 0.02166 ];
INF_CAPT                  (idx, [1:   4]) = [  2.20322E-03 0.01033  2.12555E-02 0.02511 ];
INF_ABS                   (idx, [1:   4]) = [  5.63527E-03 0.01022  1.38027E-01 0.02540 ];
INF_FISS                  (idx, [1:   4]) = [  3.43205E-03 0.01190  1.16772E-01 0.02554 ];
INF_NSF                   (idx, [1:   4]) = [  8.39195E-03 0.01184  2.84472E-01 0.02554 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44518E+00 7.9E-05  2.43614E+00 5.9E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 3.8E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.87485E-08 0.02696  2.08693E-06 0.01508 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.43978E-01 0.00650  3.06636E-01 0.02761 ];
INF_SCATT1                (idx, [1:   4]) = [  2.00303E-02 0.03762  1.64325E-02 0.17166 ];
INF_SCATT2                (idx, [1:   4]) = [  4.34188E-03 0.11105  2.91838E-03 0.71961 ];
INF_SCATT3                (idx, [1:   4]) = [  6.18227E-04 0.54053  3.84966E-03 0.43818 ];
INF_SCATT4                (idx, [1:   4]) = [  3.20071E-04 1.00000  4.92228E-03 0.67729 ];
INF_SCATT5                (idx, [1:   4]) = [  3.18114E-04 0.88501  4.62268E-03 0.35139 ];
INF_SCATT6                (idx, [1:   4]) = [  5.57251E-05 1.00000 -4.48300E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.21429E-04 1.00000  4.71845E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.43978E-01 0.00650  3.06636E-01 0.02761 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.00303E-02 0.03762  1.64325E-02 0.17166 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.34188E-03 0.11105  2.91838E-03 0.71961 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.18227E-04 0.54053  3.84966E-03 0.43818 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.20071E-04 1.00000  4.92228E-03 0.67729 ];
INF_SCATTP5               (idx, [1:   4]) = [  3.18114E-04 0.88501  4.62268E-03 0.35139 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.57251E-05 1.00000 -4.48300E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.21429E-04 1.00000  4.71845E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.97614E-01 0.00789  3.74013E-01 0.03501 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.68721E+00 0.00775  8.95813E-01 0.03656 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.63527E-03 0.01022  1.38027E-01 0.02540 ];
INF_REMXS                 (idx, [1:   4]) = [  6.33281E-03 0.02005  1.40752E-01 0.02606 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.43277E-01 0.00639  7.00306E-04 0.08596  1.26886E-03 0.32051  3.05367E-01 0.02693 ];
INF_S1                    (idx, [1:   8]) = [  2.02138E-02 0.03829 -1.83452E-04 0.11199 -1.26756E-05 1.00000  1.64452E-02 0.17044 ];
INF_S2                    (idx, [1:   8]) = [  4.35273E-03 0.10885 -1.08510E-05 1.00000 -1.24031E-04 0.52396  3.04241E-03 0.68463 ];
INF_S3                    (idx, [1:   8]) = [  6.26943E-04 0.57716 -8.71610E-06 1.00000  4.06772E-05 1.00000  3.80899E-03 0.43247 ];
INF_S4                    (idx, [1:   8]) = [  3.38018E-04 0.96022 -1.79463E-05 1.00000 -1.36246E-04 0.96124  5.05852E-03 0.67188 ];
INF_S5                    (idx, [1:   8]) = [  3.19083E-04 0.83440 -9.69417E-07 1.00000  5.59225E-05 0.79126  4.56676E-03 0.35129 ];
INF_S6                    (idx, [1:   8]) = [  6.85932E-05 1.00000 -1.28681E-05 0.61109 -2.94926E-05 1.00000 -4.18808E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.10895E-04 1.00000  1.05333E-05 0.99542 -8.93405E-05 1.00000  5.61185E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.43277E-01 0.00639  7.00306E-04 0.08596  1.26886E-03 0.32051  3.05367E-01 0.02693 ];
INF_SP1                   (idx, [1:   8]) = [  2.02138E-02 0.03829 -1.83452E-04 0.11199 -1.26756E-05 1.00000  1.64452E-02 0.17044 ];
INF_SP2                   (idx, [1:   8]) = [  4.35273E-03 0.10885 -1.08510E-05 1.00000 -1.24031E-04 0.52396  3.04241E-03 0.68463 ];
INF_SP3                   (idx, [1:   8]) = [  6.26943E-04 0.57716 -8.71610E-06 1.00000  4.06772E-05 1.00000  3.80899E-03 0.43247 ];
INF_SP4                   (idx, [1:   8]) = [  3.38018E-04 0.96022 -1.79463E-05 1.00000 -1.36246E-04 0.96124  5.05852E-03 0.67188 ];
INF_SP5                   (idx, [1:   8]) = [  3.19083E-04 0.83440 -9.69417E-07 1.00000  5.59225E-05 0.79126  4.56676E-03 0.35129 ];
INF_SP6                   (idx, [1:   8]) = [  6.85932E-05 1.00000 -1.28681E-05 0.61109 -2.94926E-05 1.00000 -4.18808E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.10895E-04 1.00000  1.05333E-05 0.99542 -8.93405E-05 1.00000  5.61185E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.20679E-01 0.02203 -1.97249E-01 0.10639 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.25030E-01 0.05142 -1.19133E-01 0.13142 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.80836E-01 0.05895 -1.02261E-01 0.07989 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.21756E-01 0.02410  4.11697E-01 0.25016 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.93927E-01 0.02226 -1.75330E+00 0.08563 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.41045E-01 0.04684 -2.98644E+00 0.12254 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.02433E-01 0.05557 -3.34026E+00 0.07630 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.03830E+00 0.02319  1.06681E+00 0.26091 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.67774E-03 0.21894  7.37810E-05 0.69492  1.46432E-03 0.64425  2.05570E-04 0.42342  9.73826E-04 0.38402  1.96886E-03 0.30610  1.16458E-03 0.80394  5.10057E-04 0.40628  3.16735E-04 0.68810 ];
LAMBDA                    (idx, [1:  18]) = [  5.46852E-01 0.14471  1.24667E-02 8.0E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79547E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64627E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '6' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.17542E+02 0.21620  5.79258E+02 0.04196  1.35341E+03 0.03274  2.33399E+03 0.04246  2.27019E+03 0.04487  1.95358E+03 0.02718  1.85758E+03 0.01356  1.61480E+03 0.02737  1.44527E+03 0.02980  1.29269E+03 0.03342  1.21409E+03 0.02137  1.04983E+03 0.03800  9.80825E+02 0.05166  8.90651E+02 0.04446  9.43964E+02 0.05670  6.81749E+02 0.04021  6.87417E+02 0.03787  6.64166E+02 0.05549  6.73659E+02 0.04742  1.23864E+03 0.04163  1.10875E+03 0.04388  7.82523E+02 0.02894  4.45991E+02 0.08475  5.06890E+02 0.04339  4.26346E+02 0.04085  2.97507E+02 0.03920  4.81567E+02 0.07586  1.03250E+02 0.10056  1.09371E+02 0.10361  1.29958E+02 0.23393  6.47639E+01 0.04166  1.28805E+02 0.10038  8.55266E+01 0.18063  5.16945E+01 0.17478  7.56273E+00 0.16150  1.64408E+01 0.34879  7.54749E+00 0.24060  1.69877E+01 0.51963  1.40239E+01 0.33512  1.44248E+01 0.26602  7.45521E+00 0.27376  6.26503E+00 0.28564  1.16305E+01 0.19426  4.19804E+01 0.14501  4.27254E+01 0.16436  1.01040E+02 0.18087  8.91963E+01 0.16525  8.08229E+01 0.11658  5.16067E+01 0.09110  2.96972E+01 0.32747  2.28416E+01 0.28636  2.62539E+01 0.27073  4.51899E+01 0.41871  4.05888E+01 0.14651  5.99503E+01 0.16807  8.19694E+01 0.09233  9.18390E+01 0.09984  7.04806E+01 0.08400  5.77259E+01 0.13970  4.08319E+01 0.15966  3.93907E+01 0.23314  4.79786E+01 0.21953  2.62109E+01 0.15765  3.20422E+01 0.32380  1.32306E+01 0.25182  1.56034E+01 0.25731  2.14219E+01 0.35645  1.62843E+01 0.40556  3.10579E+00 0.22086  1.43777E+00 0.41129 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.77248E+00 0.02025 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.44686E+00 0.00456  5.04287E-02 0.03256 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.48501E-01 0.00257  4.25928E-01 0.02659 ];
INF_CAPT                  (idx, [1:   4]) = [  1.99339E-03 0.01330  1.88939E-02 0.02346 ];
INF_ABS                   (idx, [1:   4]) = [  4.98967E-03 0.01096  1.22573E-01 0.02230 ];
INF_FISS                  (idx, [1:   4]) = [  2.99629E-03 0.00977  1.03679E-01 0.02212 ];
INF_NSF                   (idx, [1:   4]) = [  7.32663E-03 0.00973  2.52577E-01 0.02212 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44524E+00 0.00011  2.43614E+00 5.9E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 5.5E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.03330E-08 0.02057  2.23566E-06 0.02414 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.43539E-01 0.00293  3.06549E-01 0.02441 ];
INF_SCATT1                (idx, [1:   4]) = [  1.90038E-02 0.02929  2.70107E-02 0.15911 ];
INF_SCATT2                (idx, [1:   4]) = [  3.69789E-03 0.11490  2.64172E-03 1.00000 ];
INF_SCATT3                (idx, [1:   4]) = [  1.24851E-03 0.20390 -2.01474E-03 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.31961E-04 0.64063 -1.51378E-03 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.05143E-04 1.00000  3.23302E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  2.89818E-04 0.80260 -2.00011E-03 0.80492 ];
INF_SCATT7                (idx, [1:   4]) = [ -4.15876E-04 0.99790 -7.14500E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.43539E-01 0.00293  3.06549E-01 0.02441 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.90038E-02 0.02929  2.70107E-02 0.15911 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.69789E-03 0.11490  2.64172E-03 1.00000 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.24851E-03 0.20390 -2.01474E-03 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.31961E-04 0.64063 -1.51378E-03 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.05143E-04 1.00000  3.23302E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.89818E-04 0.80260 -2.00011E-03 0.80492 ];
INF_SCATTP7               (idx, [1:   4]) = [ -4.15876E-04 0.99790 -7.14500E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.96982E-01 0.01084  3.49298E-01 0.05574 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.69301E+00 0.01095  9.65929E-01 0.05413 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.98967E-03 0.01096  1.22573E-01 0.02230 ];
INF_REMXS                 (idx, [1:   4]) = [  5.68132E-03 0.02089  1.20542E-01 0.05026 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.42820E-01 0.00274  7.19726E-04 0.06779  1.16333E-03 0.15328  3.05386E-01 0.02403 ];
INF_S1                    (idx, [1:   8]) = [  1.91760E-02 0.02902 -1.72242E-04 0.29179  3.71692E-04 0.77378  2.66390E-02 0.16232 ];
INF_S2                    (idx, [1:   8]) = [  3.70946E-03 0.11097 -1.15630E-05 1.00000 -6.88604E-05 1.00000  2.71059E-03 1.00000 ];
INF_S3                    (idx, [1:   8]) = [  1.25754E-03 0.19953 -9.02851E-06 1.00000 -5.74052E-05 1.00000 -1.95734E-03 1.00000 ];
INF_S4                    (idx, [1:   8]) = [ -6.05620E-04 0.68128 -2.63412E-05 0.39439 -7.96930E-05 1.00000 -1.43409E-03 1.00000 ];
INF_S5                    (idx, [1:   8]) = [ -1.09530E-04 1.00000  4.38669E-06 1.00000 -6.10848E-05 1.00000  3.84387E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  2.80794E-04 0.84745  9.02436E-06 1.00000 -4.44120E-05 1.00000 -1.95569E-03 0.83721 ];
INF_S7                    (idx, [1:   8]) = [ -4.11774E-04 0.99414 -4.10196E-06 1.00000 -1.05884E-04 1.00000 -6.08615E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.42820E-01 0.00274  7.19726E-04 0.06779  1.16333E-03 0.15328  3.05386E-01 0.02403 ];
INF_SP1                   (idx, [1:   8]) = [  1.91760E-02 0.02902 -1.72242E-04 0.29179  3.71692E-04 0.77378  2.66390E-02 0.16232 ];
INF_SP2                   (idx, [1:   8]) = [  3.70946E-03 0.11097 -1.15630E-05 1.00000 -6.88604E-05 1.00000  2.71059E-03 1.00000 ];
INF_SP3                   (idx, [1:   8]) = [  1.25754E-03 0.19953 -9.02851E-06 1.00000 -5.74052E-05 1.00000 -1.95734E-03 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [ -6.05620E-04 0.68128 -2.63412E-05 0.39439 -7.96930E-05 1.00000 -1.43409E-03 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [ -1.09530E-04 1.00000  4.38669E-06 1.00000 -6.10848E-05 1.00000  3.84387E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  2.80794E-04 0.84745  9.02436E-06 1.00000 -4.44120E-05 1.00000 -1.95569E-03 0.83721 ];
INF_SP7                   (idx, [1:   8]) = [ -4.11774E-04 0.99414 -4.10196E-06 1.00000 -1.05884E-04 1.00000 -6.08615E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.19161E-01 0.03536 -3.01797E-01 0.16623 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.62431E-01 0.04241 -2.08738E-01 0.20253 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.10746E-01 0.08016 -1.30274E-01 0.17215 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.35195E-01 0.04196  4.67745E-01 0.45722 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.99468E-01 0.03748 -1.21599E+00 0.14501 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.25824E-01 0.04067 -2.10188E+00 0.31169 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.71159E-01 0.08707 -2.86790E+00 0.16185 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.00142E+00 0.04153  1.32181E+00 0.29420 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.83139E-03 0.22658  1.45887E-04 0.64223  1.05302E-03 0.49657  4.77768E-04 0.80906  1.59455E-03 0.57939  2.49147E-03 0.42731  2.64495E-04 0.40181  7.67109E-04 0.62653  3.70901E-05 0.70542 ];
LAMBDA                    (idx, [1:  18]) = [  4.12511E-01 0.17332  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 5.7E-09  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79548E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64627E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '5' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.15182E+01 0.17603  6.22825E+02 0.05805  1.27277E+03 0.04388  2.07622E+03 0.02704  2.25092E+03 0.04562  2.03148E+03 0.03632  1.73737E+03 0.05110  1.59578E+03 0.03524  1.36877E+03 0.05991  1.24896E+03 0.05367  1.07565E+03 0.04066  1.00260E+03 0.03406  9.88577E+02 0.02615  9.43496E+02 0.04293  9.38149E+02 0.08798  7.34819E+02 0.03882  7.19526E+02 0.07057  6.77838E+02 0.04291  6.73531E+02 0.06296  1.23447E+03 0.02078  1.09624E+03 0.05608  7.56287E+02 0.08981  4.14485E+02 0.05729  4.58276E+02 0.08244  4.13134E+02 0.06019  3.08236E+02 0.05695  5.04324E+02 0.04303  9.86540E+01 0.11021  1.31419E+02 0.11219  9.31360E+01 0.07537  5.87064E+01 0.20408  1.32663E+02 0.11316  6.96228E+01 0.14649  5.56702E+01 0.15203  4.95263E+00 0.20387  7.57059E+00 0.10555  1.63997E+01 0.38415  1.83217E+01 0.48491  9.74059E+00 0.26448  1.67692E+01 0.35725  9.89648E+00 0.12363  8.65336E+00 0.49870  1.63702E+01 0.14010  4.20045E+01 0.34870  2.90575E+01 0.16109  9.46239E+01 0.09595  9.52777E+01 0.10956  9.90117E+01 0.10550  4.51902E+01 0.11993  3.92756E+01 0.22556  1.87111E+01 0.15751  2.37752E+01 0.24009  4.89534E+01 0.23223  4.81583E+01 0.17892  4.79785E+01 0.17476  7.86504E+01 0.14448  1.14265E+02 0.12263  7.00209E+01 0.10116  6.77262E+01 0.18530  4.56961E+01 0.17791  4.48731E+01 0.21227  5.03912E+01 0.12935  3.43965E+01 0.18433  2.48094E+01 0.22293  3.46990E+01 0.29366  2.38671E+01 0.22927  1.21286E+01 0.12015  1.15742E+01 0.16467  2.38563E+01 0.67726  1.72431E+00 0.25874 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.70105E+00 0.03945 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.41154E+00 0.01637  5.54235E-02 0.03583 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.47762E-01 0.01077  4.23032E-01 0.03399 ];
INF_CAPT                  (idx, [1:   4]) = [  1.88166E-03 0.03591  1.82375E-02 0.05420 ];
INF_ABS                   (idx, [1:   4]) = [  4.58842E-03 0.02160  1.17379E-01 0.05283 ];
INF_FISS                  (idx, [1:   4]) = [  2.70676E-03 0.01734  9.91416E-02 0.05258 ];
INF_NSF                   (idx, [1:   4]) = [  6.61665E-03 0.01730  2.41523E-01 0.05258 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44449E+00 5.9E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 4.8E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.03413E-08 0.02079  2.34876E-06 0.03399 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.42954E-01 0.01034  3.03737E-01 0.04199 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97051E-02 0.06546  1.29169E-02 0.27458 ];
INF_SCATT2                (idx, [1:   4]) = [  4.19499E-03 0.07613  1.40774E-03 1.00000 ];
INF_SCATT3                (idx, [1:   4]) = [  2.55028E-04 0.96496 -4.40698E-03 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [ -3.32008E-04 1.00000 -4.43771E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  4.91794E-04 0.87034 -2.44190E-03 0.68240 ];
INF_SCATT6                (idx, [1:   4]) = [ -5.67548E-04 0.71623  1.51931E-03 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -1.62068E-04 1.00000 -1.59253E-03 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.42954E-01 0.01034  3.03737E-01 0.04199 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97051E-02 0.06546  1.29169E-02 0.27458 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.19499E-03 0.07613  1.40774E-03 1.00000 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.55028E-04 0.96496 -4.40698E-03 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [ -3.32008E-04 1.00000 -4.43771E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  4.91794E-04 0.87034 -2.44190E-03 0.68240 ];
INF_SCATTP6               (idx, [1:   4]) = [ -5.67548E-04 0.71623  1.51931E-03 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -1.62068E-04 1.00000 -1.59253E-03 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.96485E-01 0.01069  3.56618E-01 0.04023 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.69724E+00 0.01043  9.40494E-01 0.03828 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.58842E-03 0.02160  1.17379E-01 0.05283 ];
INF_REMXS                 (idx, [1:   4]) = [  5.57951E-03 0.04310  1.20007E-01 0.04019 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.42182E-01 0.01021  7.72324E-04 0.08142  7.12174E-04 0.61293  3.03025E-01 0.04321 ];
INF_S1                    (idx, [1:   8]) = [  1.99016E-02 0.06268 -1.96491E-04 0.24874  1.72883E-05 1.00000  1.28996E-02 0.27651 ];
INF_S2                    (idx, [1:   8]) = [  4.19035E-03 0.07690  4.64029E-06 1.00000  5.89217E-05 1.00000  1.34882E-03 1.00000 ];
INF_S3                    (idx, [1:   8]) = [  3.07694E-04 0.81639 -5.26662E-05 0.44025 -2.09363E-04 0.73261 -4.19762E-03 1.00000 ];
INF_S4                    (idx, [1:   8]) = [ -3.41985E-04 1.00000  9.97629E-06 1.00000  1.52026E-04 0.66985 -5.95798E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  5.07141E-04 0.88493 -1.53474E-05 1.00000 -1.24496E-04 1.00000 -2.31741E-03 0.71485 ];
INF_S6                    (idx, [1:   8]) = [ -5.71638E-04 0.67121  4.08922E-06 1.00000 -4.01429E-05 1.00000  1.55946E-03 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -1.62185E-04 1.00000  1.16977E-07 1.00000 -1.03283E-04 0.61245 -1.48925E-03 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.42182E-01 0.01021  7.72324E-04 0.08142  7.12174E-04 0.61293  3.03025E-01 0.04321 ];
INF_SP1                   (idx, [1:   8]) = [  1.99016E-02 0.06268 -1.96491E-04 0.24874  1.72883E-05 1.00000  1.28996E-02 0.27651 ];
INF_SP2                   (idx, [1:   8]) = [  4.19035E-03 0.07690  4.64029E-06 1.00000  5.89217E-05 1.00000  1.34882E-03 1.00000 ];
INF_SP3                   (idx, [1:   8]) = [  3.07694E-04 0.81639 -5.26662E-05 0.44025 -2.09363E-04 0.73261 -4.19762E-03 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [ -3.41985E-04 1.00000  9.97629E-06 1.00000  1.52026E-04 0.66985 -5.95798E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  5.07141E-04 0.88493 -1.53474E-05 1.00000 -1.24496E-04 1.00000 -2.31741E-03 0.71485 ];
INF_SP6                   (idx, [1:   8]) = [ -5.71638E-04 0.67121  4.08922E-06 1.00000 -4.01429E-05 1.00000  1.55946E-03 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -1.62185E-04 1.00000  1.16977E-07 1.00000 -1.03283E-04 0.61245 -1.48925E-03 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.51589E-01 0.03416 -1.93555E-01 0.12670 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.51474E-01 0.07616 -1.03967E-01 0.10305 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  6.12654E-01 0.10280 -1.11975E-01 0.06832 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.20405E-01 0.02365 -1.96277E-01 1.00000 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.41689E-01 0.03508 -1.82117E+00 0.10904 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.18029E-01 0.07243 -3.34669E+00 0.10251 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  5.64397E-01 0.08838 -3.02557E+00 0.05917 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.04264E+00 0.02324  9.08758E-01 0.37475 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.25285E-03 0.25610  1.19602E-05 0.73193  2.51776E-03 0.62192  4.38954E-04 0.84110  4.13880E-04 0.47466  2.20379E-03 0.31478  5.59716E-04 0.82176  1.05338E-03 0.62530  5.34128E-05 0.76256 ];
LAMBDA                    (idx, [1:  18]) = [  5.23764E-01 0.16536  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 5.4E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 9.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79549E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27759 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64625E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '4' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.98871E+02 0.06169  1.06419E+03 0.03847  2.61266E+03 0.04053  3.95750E+03 0.02335  4.26366E+03 0.01458  3.88673E+03 0.03030  3.41803E+03 0.01152  2.86173E+03 0.03121  2.60039E+03 0.01130  2.28744E+03 0.02711  2.08370E+03 0.01960  1.93023E+03 0.03503  1.87571E+03 0.03764  1.79966E+03 0.03099  1.67652E+03 0.01981  1.46707E+03 0.03468  1.42589E+03 0.05502  1.33664E+03 0.04561  1.28848E+03 0.02100  2.38197E+03 0.02921  2.19604E+03 0.04862  1.43906E+03 0.03483  9.13244E+02 0.03487  8.68851E+02 0.02428  8.74431E+02 0.04206  6.37797E+02 0.07323  1.07250E+03 0.03272  1.95365E+02 0.10272  2.78484E+02 0.05043  2.50922E+02 0.07728  1.42886E+02 0.08576  2.89937E+02 0.10279  1.61120E+02 0.05111  1.32843E+02 0.10719  1.78731E+01 0.25557  4.11325E+01 0.40393  3.31566E+01 0.33513  2.37789E+01 0.19994  3.05011E+01 0.23091  2.44528E+01 0.20509  2.38624E+01 0.20642  2.02030E+01 0.21565  4.12845E+01 0.16238  6.22676E+01 0.11953  8.71739E+01 0.20096  2.59308E+02 0.10562  1.91877E+02 0.05179  1.85634E+02 0.03618  1.09352E+02 0.13685  8.00948E+01 0.09846  3.97731E+01 0.18207  4.48817E+01 0.11050  7.02063E+01 0.07466  7.84887E+01 0.14595  1.94795E+02 0.05332  1.54002E+02 0.12997  2.93858E+02 0.13751  1.86115E+02 0.12963  1.70512E+02 0.11384  1.22257E+02 0.06545  1.10696E+02 0.10772  1.07250E+02 0.12257  9.14868E+01 0.17705  6.80410E+01 0.05320  6.28092E+01 0.17985  5.60749E+01 0.10754  6.21313E+01 0.18175  5.03054E+01 0.19840  2.03009E+01 0.18274  5.32292E+00 0.25337 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.76552E+00 0.01557 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.73467E+00 0.00387  1.28176E-01 0.03852 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.51540E-01 0.00261  4.10877E-01 0.02083 ];
INF_CAPT                  (idx, [1:   4]) = [  1.82280E-03 0.01133  1.66605E-02 0.02387 ];
INF_ABS                   (idx, [1:   4]) = [  4.33135E-03 0.00675  1.07429E-01 0.02159 ];
INF_FISS                  (idx, [1:   4]) = [  2.50854E-03 0.00751  9.07688E-02 0.02120 ];
INF_NSF                   (idx, [1:   4]) = [  6.13072E-03 0.00751  2.21125E-01 0.02120 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44394E+00 6.3E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 4.7E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.52231E-08 0.01128  2.41423E-06 0.01208 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.47163E-01 0.00284  3.04779E-01 0.02251 ];
INF_SCATT1                (idx, [1:   4]) = [  1.88613E-02 0.02955  1.65380E-02 0.07783 ];
INF_SCATT2                (idx, [1:   4]) = [  3.86480E-03 0.06453  3.76428E-03 0.43329 ];
INF_SCATT3                (idx, [1:   4]) = [  8.16879E-04 0.52545 -1.23406E-03 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.50216E-04 0.82382 -1.41315E-03 0.34537 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.17475E-04 1.00000  9.68022E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [ -3.69651E-04 0.30114 -9.58619E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -4.96310E-04 0.69806  2.98367E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.47163E-01 0.00284  3.04779E-01 0.02251 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.88613E-02 0.02955  1.65380E-02 0.07783 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.86480E-03 0.06453  3.76428E-03 0.43329 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.16879E-04 0.52545 -1.23406E-03 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.50216E-04 0.82382 -1.41315E-03 0.34537 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.17475E-04 1.00000  9.68022E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [ -3.69651E-04 0.30114 -9.58619E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -4.96310E-04 0.69806  2.98367E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.98085E-01 0.00790  3.60664E-01 0.03137 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.68321E+00 0.00805  9.28018E-01 0.03265 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.33135E-03 0.00675  1.07429E-01 0.02159 ];
INF_REMXS                 (idx, [1:   4]) = [  5.30996E-03 0.01853  1.06736E-01 0.02522 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.46230E-01 0.00297  9.33331E-04 0.07356  6.37911E-04 0.18774  3.04141E-01 0.02230 ];
INF_S1                    (idx, [1:   8]) = [  1.90800E-02 0.02847 -2.18689E-04 0.18861  8.89994E-05 1.00000  1.64490E-02 0.07188 ];
INF_S2                    (idx, [1:   8]) = [  3.86717E-03 0.06687 -2.36892E-06 1.00000  1.18577E-04 0.85812  3.64571E-03 0.47455 ];
INF_S3                    (idx, [1:   8]) = [  8.52209E-04 0.50188 -3.53302E-05 0.56468 -2.47539E-05 1.00000 -1.20931E-03 1.00000 ];
INF_S4                    (idx, [1:   8]) = [ -5.41004E-04 0.80986 -9.21192E-06 1.00000  9.65615E-05 0.97010 -1.50972E-03 0.35089 ];
INF_S5                    (idx, [1:   8]) = [ -1.53291E-04 1.00000  3.58165E-05 0.64979  1.87890E-05 1.00000  9.49233E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [ -3.59519E-04 0.32221 -1.01322E-05 1.00000 -2.24687E-05 1.00000 -9.36151E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -5.16795E-04 0.64783  2.04852E-05 0.68867 -1.31314E-04 0.30319  4.29682E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.46230E-01 0.00297  9.33331E-04 0.07356  6.37911E-04 0.18774  3.04141E-01 0.02230 ];
INF_SP1                   (idx, [1:   8]) = [  1.90800E-02 0.02847 -2.18689E-04 0.18861  8.89994E-05 1.00000  1.64490E-02 0.07188 ];
INF_SP2                   (idx, [1:   8]) = [  3.86717E-03 0.06687 -2.36892E-06 1.00000  1.18577E-04 0.85812  3.64571E-03 0.47455 ];
INF_SP3                   (idx, [1:   8]) = [  8.52209E-04 0.50188 -3.53302E-05 0.56468 -2.47539E-05 1.00000 -1.20931E-03 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [ -5.41004E-04 0.80986 -9.21192E-06 1.00000  9.65615E-05 0.97010 -1.50972E-03 0.35089 ];
INF_SP5                   (idx, [1:   8]) = [ -1.53291E-04 1.00000  3.58165E-05 0.64979  1.87890E-05 1.00000  9.49233E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [ -3.59519E-04 0.32221 -1.01322E-05 1.00000 -2.24687E-05 1.00000 -9.36151E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -5.16795E-04 0.64783  2.04852E-05 0.68867 -1.31314E-04 0.30319  4.29682E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.44414E-01 0.01382 -2.48045E-01 0.13215 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.68457E-01 0.08569 -1.21850E-01 0.14374 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.24055E-01 0.05240 -1.31103E-01 0.05029 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.34167E-01 0.03331  2.96153E-01 0.13769 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.50616E-01 0.01361 -1.41907E+00 0.10136 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.06847E-01 0.09989 -2.91567E+00 0.10924 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.43230E-01 0.05344 -2.56883E+00 0.05100 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.00177E+00 0.03199  1.22729E+00 0.15485 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.50161E-03 0.18383  6.76869E-05 0.55116  1.62353E-03 0.53880  1.74482E-04 0.46582  1.11866E-03 0.40340  2.18210E-03 0.26825  4.61437E-04 0.39020  7.54176E-04 0.46615  1.19533E-04 0.55791 ];
LAMBDA                    (idx, [1:  18]) = [  5.91599E-01 0.14913  1.24667E-02 5.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79550E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00707E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27759 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64625E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '3' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.16382E+01 0.25468  2.13518E+02 0.03134  4.81526E+02 0.06732  7.82246E+02 0.03014  8.62055E+02 0.08106  7.10164E+02 0.03095  6.04378E+02 0.05069  5.47331E+02 0.03312  5.71016E+02 0.03783  4.75591E+02 0.09306  4.32593E+02 0.05557  4.42247E+02 0.07212  3.66382E+02 0.05392  3.47392E+02 0.08085  3.52406E+02 0.13706  2.76069E+02 0.02470  2.88319E+02 0.05283  2.70035E+02 0.06436  2.71293E+02 0.13157  5.19215E+02 0.06866  4.53253E+02 0.05415  3.21988E+02 0.05770  1.71563E+02 0.10044  1.75062E+02 0.06023  1.92256E+02 0.10649  1.47554E+02 0.10374  2.52958E+02 0.01497  3.95015E+01 0.13616  4.97785E+01 0.17639  4.24730E+01 0.16686  5.21977E+01 0.18596  5.09488E+01 0.16825  4.42691E+01 0.17201  3.36438E+01 0.18764  7.84112E+00 0.60114  8.05691E+00 0.54233  6.09220E+00 0.65083  9.49103E+00 0.48211  2.67552E+00 0.49097  7.46644E+00 0.62370  2.24955E+00 0.32197  4.59523E+00 0.23505  1.22030E+01 0.40813  1.53080E+01 0.43600  2.21231E+01 0.08512  3.72234E+01 0.26978  4.72773E+01 0.27784  3.82296E+01 0.20915  2.11813E+01 0.30999  1.48628E+01 0.25466  1.07429E+01 0.41371  1.27756E+01 0.42641  1.91595E+01 0.33357  2.20592E+01 0.23317  2.13073E+01 0.23648  5.32384E+01 0.21773  8.21800E+01 0.14324  5.51641E+01 0.29613  3.80844E+01 0.12714  3.17676E+01 0.17903  3.20999E+01 0.19848  2.36795E+01 0.29491  3.00328E+01 0.32106  2.71093E+01 0.25849  2.69142E+01 0.33530  1.55205E+01 0.29683  1.25252E+01 0.29620  1.24842E+01 0.31757  4.78335E+00 0.17200  1.04044E+00 0.38268 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.79974E+00 0.08044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  5.51999E-01 0.01576  3.27912E-02 0.08749 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.47211E-01 0.01309  3.98374E-01 0.05248 ];
INF_CAPT                  (idx, [1:   4]) = [  1.75659E-03 0.04204  1.50506E-02 0.06588 ];
INF_ABS                   (idx, [1:   4]) = [  4.00837E-03 0.01883  9.59091E-02 0.05726 ];
INF_FISS                  (idx, [1:   4]) = [  2.25178E-03 0.02428  8.08585E-02 0.05571 ];
INF_NSF                   (idx, [1:   4]) = [  5.50223E-03 0.02426  1.96982E-01 0.05571 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44351E+00 0.00014  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 6.1E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.81590E-08 0.03978  2.50619E-06 0.01204 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.43127E-01 0.01407  3.02663E-01 0.05579 ];
INF_SCATT1                (idx, [1:   4]) = [  1.72795E-02 0.06140  2.00176E-02 0.14529 ];
INF_SCATT2                (idx, [1:   4]) = [  1.85575E-03 0.51792  2.77442E-03 1.00000 ];
INF_SCATT3                (idx, [1:   4]) = [  1.09635E-03 0.58174  1.24657E-03 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.02211E-03 0.65242 -2.37310E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  6.95335E-04 0.47714 -5.39912E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.02330E-03 0.41332  6.30425E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  5.53178E-04 1.00000  1.53457E-03 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.43127E-01 0.01407  3.02663E-01 0.05579 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.72795E-02 0.06140  2.00176E-02 0.14529 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.85575E-03 0.51792  2.77442E-03 1.00000 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.09635E-03 0.58174  1.24657E-03 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.02211E-03 0.65242 -2.37310E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  6.95335E-04 0.47714 -5.39912E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.02330E-03 0.41332  6.30425E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  5.53178E-04 1.00000  1.53457E-03 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.93572E-01 0.02533  3.21947E-01 0.06715 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.72649E+00 0.02564  1.05584E+00 0.07275 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.00837E-03 0.01883  9.59091E-02 0.05726 ];
INF_REMXS                 (idx, [1:   4]) = [  4.97885E-03 0.08678  9.72724E-02 0.08946 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.42232E-01 0.01414  8.94300E-04 0.10636  1.56142E-03 0.48006  3.01101E-01 0.05698 ];
INF_S1                    (idx, [1:   8]) = [  1.74949E-02 0.05568 -2.15327E-04 0.41842  3.89198E-04 0.61099  1.96284E-02 0.15742 ];
INF_S2                    (idx, [1:   8]) = [  1.85889E-03 0.51188 -3.14171E-06 1.00000 -4.81425E-04 0.44894  3.25584E-03 1.00000 ];
INF_S3                    (idx, [1:   8]) = [  1.14200E-03 0.55861 -4.56500E-05 0.72357 -3.40378E-04 0.71608  1.58695E-03 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.00914E-03 0.64337  1.29731E-05 1.00000  6.45168E-05 1.00000 -3.01826E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  7.11699E-04 0.47454 -1.63645E-05 1.00000  9.75858E-05 1.00000 -6.37497E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.04199E-03 0.39910 -1.86940E-05 0.68248  1.89221E-05 1.00000  6.11503E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  5.44394E-04 1.00000  8.78329E-06 1.00000  8.11012E-05 1.00000  1.45347E-03 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.42232E-01 0.01414  8.94300E-04 0.10636  1.56142E-03 0.48006  3.01101E-01 0.05698 ];
INF_SP1                   (idx, [1:   8]) = [  1.74949E-02 0.05568 -2.15327E-04 0.41842  3.89198E-04 0.61099  1.96284E-02 0.15742 ];
INF_SP2                   (idx, [1:   8]) = [  1.85889E-03 0.51188 -3.14171E-06 1.00000 -4.81425E-04 0.44894  3.25584E-03 1.00000 ];
INF_SP3                   (idx, [1:   8]) = [  1.14200E-03 0.55861 -4.56500E-05 0.72357 -3.40378E-04 0.71608  1.58695E-03 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.00914E-03 0.64337  1.29731E-05 1.00000  6.45168E-05 1.00000 -3.01826E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  7.11699E-04 0.47454 -1.63645E-05 1.00000  9.75858E-05 1.00000 -6.37497E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.04199E-03 0.39910 -1.86940E-05 0.68248  1.89221E-05 1.00000  6.11503E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  5.44394E-04 1.00000  8.78329E-06 1.00000  8.11012E-05 1.00000  1.45347E-03 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.06996E-01 0.04466  1.15113E+00 1.00000 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  3.71154E-01 0.05898 -2.04382E-01 0.31557 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.81318E-01 0.14442 -3.69866E-01 0.42588 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.73425E-01 0.12900 -3.19521E-01 1.00000 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  8.25549E-01 0.04443 -9.65982E-01 0.33961 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  9.10070E-01 0.05600 -2.44245E+00 0.29811 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.16001E-01 0.12295 -1.91263E+00 0.32981 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.50576E-01 0.11888  1.45713E+00 0.55069 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  3.65089E-03 0.32843  6.70574E-05 0.71320  1.64596E-03 0.60180  1.07075E-05 0.71495  4.74451E-04 0.63166  9.05784E-04 0.65454  2.19869E-04 0.68268  3.05319E-04 0.86230  2.17443E-05 1.00000 ];
LAMBDA                    (idx, [1:  18]) = [  3.51637E-01 0.17881  1.24667E-02 1.5E-08  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 5.5E-09  6.66488E-01 5.7E-09  1.63478E+00 8.3E-09  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79550E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00708E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27759 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64624E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '2' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.56509E+02 0.04940  1.43988E+03 0.03042  3.35576E+03 0.02517  5.83912E+03 0.01045  6.22801E+03 0.02251  5.49559E+03 0.02000  4.92873E+03 0.01606  4.28866E+03 0.02417  3.78335E+03 0.02956  3.55619E+03 0.03032  3.19979E+03 0.00885  2.92485E+03 0.02288  2.91267E+03 0.03096  2.60062E+03 0.02660  2.65258E+03 0.03017  2.39331E+03 0.01055  2.20588E+03 0.02229  2.06053E+03 0.04095  2.05931E+03 0.02569  3.72897E+03 0.03039  3.22946E+03 0.02930  2.37740E+03 0.02462  1.55902E+03 0.04943  1.51546E+03 0.02319  1.45916E+03 0.02324  1.16780E+03 0.03645  1.84057E+03 0.04519  4.42954E+02 0.09607  4.85337E+02 0.05078  3.53620E+02 0.01672  2.29275E+02 0.07906  4.25664E+02 0.10219  2.52914E+02 0.08097  2.28127E+02 0.08566  4.28584E+01 0.18837  5.04673E+01 0.11245  4.29753E+01 0.23580  4.07345E+01 0.15277  4.18647E+01 0.20711  4.35763E+01 0.21895  3.55454E+01 0.12560  3.07966E+01 0.15377  7.72838E+01 0.12882  1.06470E+02 0.08863  1.48343E+02 0.06065  3.88207E+02 0.03758  3.25904E+02 0.06283  3.27007E+02 0.06334  1.95308E+02 0.07082  1.19259E+02 0.09323  8.50737E+01 0.09474  7.86223E+01 0.17688  1.72777E+02 0.19193  1.56248E+02 0.10610  2.68037E+02 0.09345  3.72480E+02 0.04842  5.60271E+02 0.05872  4.07248E+02 0.08125  3.57589E+02 0.05171  2.93774E+02 0.08493  2.85472E+02 0.02843  2.75516E+02 0.02374  2.55738E+02 0.03052  2.08415E+02 0.09993  1.93839E+02 0.07930  1.87508E+02 0.16538  1.56962E+02 0.08212  1.36882E+02 0.05470  6.96642E+01 0.18226  1.76765E+01 0.18285 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.74820E+00 0.01476 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.13833E+00 0.00614  2.76180E-01 0.03594 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.52490E-01 0.00563  3.98935E-01 0.01005 ];
INF_CAPT                  (idx, [1:   4]) = [  1.65916E-03 0.00913  1.42815E-02 0.00941 ];
INF_ABS                   (idx, [1:   4]) = [  3.71653E-03 0.01218  9.15976E-02 0.01089 ];
INF_FISS                  (idx, [1:   4]) = [  2.05737E-03 0.02002  7.73161E-02 0.01118 ];
INF_NSF                   (idx, [1:   4]) = [  5.02562E-03 0.01996  1.88353E-01 0.01118 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44275E+00 5.6E-05  2.43614E+00 8.3E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 2.0E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.90803E-08 0.01560  2.68029E-06 0.01065 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.48725E-01 0.00589  3.04487E-01 0.00925 ];
INF_SCATT1                (idx, [1:   4]) = [  1.94577E-02 0.02784  1.82048E-02 0.10347 ];
INF_SCATT2                (idx, [1:   4]) = [  4.29062E-03 0.10353  1.32667E-03 1.00000 ];
INF_SCATT3                (idx, [1:   4]) = [  6.55710E-04 0.50000  1.90685E-03 0.34169 ];
INF_SCATT4                (idx, [1:   4]) = [ -4.50930E-05 1.00000  3.98844E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.24724E-04 1.00000  4.08970E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.16704E-05 1.00000  7.92559E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.78645E-04 1.00000  1.38248E-03 0.52674 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.48725E-01 0.00589  3.04487E-01 0.00925 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.94577E-02 0.02784  1.82048E-02 0.10347 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.29062E-03 0.10353  1.32667E-03 1.00000 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.55710E-04 0.50000  1.90685E-03 0.34169 ];
INF_SCATTP4               (idx, [1:   4]) = [ -4.50930E-05 1.00000  3.98844E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.24724E-04 1.00000  4.08970E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.16704E-05 1.00000  7.92559E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.78645E-04 1.00000  1.38248E-03 0.52674 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02348E-01 0.00870  3.64394E-01 0.01205 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64782E+00 0.00859  9.15285E-01 0.01185 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  3.71653E-03 0.01218  9.15976E-02 0.01089 ];
INF_REMXS                 (idx, [1:   4]) = [  4.66580E-03 0.02052  9.49108E-02 0.01618 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.47824E-01 0.00589  9.00945E-04 0.02749  4.62836E-04 0.29529  3.04024E-01 0.00937 ];
INF_S1                    (idx, [1:   8]) = [  1.96720E-02 0.02669 -2.14361E-04 0.12977  2.18911E-05 1.00000  1.81829E-02 0.10119 ];
INF_S2                    (idx, [1:   8]) = [  4.33413E-03 0.10279 -4.35105E-05 0.43531  7.31430E-06 1.00000  1.31935E-03 1.00000 ];
INF_S3                    (idx, [1:   8]) = [  6.54814E-04 0.51405  8.96285E-07 1.00000  2.46860E-05 1.00000  1.88216E-03 0.33296 ];
INF_S4                    (idx, [1:   8]) = [ -4.69933E-05 1.00000  1.90033E-06 1.00000 -8.74403E-05 0.22559  4.86284E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.23729E-04 1.00000  9.94905E-07 1.00000 -5.25463E-05 1.00000  4.61516E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.85544E-05 1.00000 -6.88393E-06 1.00000 -2.08952E-05 1.00000  8.13454E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.84100E-04 1.00000 -5.45485E-06 0.72395  2.08387E-05 1.00000  1.36164E-03 0.52404 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.47824E-01 0.00589  9.00945E-04 0.02749  4.62836E-04 0.29529  3.04024E-01 0.00937 ];
INF_SP1                   (idx, [1:   8]) = [  1.96720E-02 0.02669 -2.14361E-04 0.12977  2.18911E-05 1.00000  1.81829E-02 0.10119 ];
INF_SP2                   (idx, [1:   8]) = [  4.33413E-03 0.10279 -4.35105E-05 0.43531  7.31430E-06 1.00000  1.31935E-03 1.00000 ];
INF_SP3                   (idx, [1:   8]) = [  6.54814E-04 0.51405  8.96285E-07 1.00000  2.46860E-05 1.00000  1.88216E-03 0.33296 ];
INF_SP4                   (idx, [1:   8]) = [ -4.69933E-05 1.00000  1.90033E-06 1.00000 -8.74403E-05 0.22559  4.86284E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.23729E-04 1.00000  9.94905E-07 1.00000 -5.25463E-05 1.00000  4.61516E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.85544E-05 1.00000 -6.88393E-06 1.00000 -2.08952E-05 1.00000  8.13454E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.84100E-04 1.00000 -5.45485E-06 0.72395  2.08387E-05 1.00000  1.36164E-03 0.52404 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.23994E-01 0.02078 -2.33364E-01 0.04461 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.56465E-01 0.06001 -1.30288E-01 0.03918 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.77380E-01 0.02219 -1.33922E-01 0.02297 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.62922E-01 0.03109  5.55268E-01 0.19527 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.87498E-01 0.02022 -1.43957E+00 0.04366 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.40575E-01 0.05834 -2.57406E+00 0.03887 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.99663E-01 0.02268 -2.49434E+00 0.02324 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.22255E-01 0.03304  7.49679E-01 0.28196 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  5.57989E-03 0.15615  1.16798E-05 0.54226  1.42108E-03 0.41052  1.42857E-04 0.41268  9.53600E-04 0.31540  1.91609E-03 0.28368  3.74981E-04 0.48700  7.12237E-04 0.50242  4.73635E-05 0.79292 ];
LAMBDA                    (idx, [1:  18]) = [  5.05460E-01 0.15289  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 0.0E+00  6.66488E-01 6.6E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79556E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00708E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27759 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64624E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'g' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.59906E+03 0.05364  7.52513E+03 0.01263  1.52636E+04 0.02329  3.18776E+04 0.02032  3.65549E+04 0.01414  3.53161E+04 0.01149  3.37530E+04 0.00769  3.19899E+04 0.00779  3.03947E+04 0.00729  2.93593E+04 0.00430  2.89929E+04 0.00743  2.81222E+04 0.00565  2.78133E+04 0.00362  2.75238E+04 0.00505  2.72938E+04 0.00996  2.39007E+04 0.00599  2.40656E+04 0.00653  2.36321E+04 0.00682  2.32943E+04 0.00981  4.55043E+04 0.00740  4.38612E+04 0.00811  3.16317E+04 0.00789  2.03198E+04 0.00908  2.39432E+04 0.00880  2.29030E+04 0.00959  1.93035E+04 0.00939  3.47176E+04 0.00898  7.05964E+03 0.00907  8.63529E+03 0.00880  7.69163E+03 0.01053  4.36721E+03 0.00470  7.42207E+03 0.01141  4.97955E+03 0.00994  4.23768E+03 0.01480  7.72633E+02 0.01787  8.51170E+02 0.02022  8.22461E+02 0.02382  8.41183E+02 0.01270  8.39920E+02 0.02459  7.97548E+02 0.02252  8.62960E+02 0.02039  7.81038E+02 0.02359  1.48111E+03 0.01636  2.33972E+03 0.02131  2.92154E+03 0.01495  7.44465E+03 0.00849  7.54894E+03 0.02000  7.61723E+03 0.01492  4.60430E+03 0.02147  3.07503E+03 0.01966  2.22147E+03 0.02104  2.44898E+03 0.00965  4.01921E+03 0.01485  4.68474E+03 0.01757  8.24346E+03 0.01749  1.27012E+04 0.01470  2.30121E+04 0.01985  1.97170E+04 0.02071  1.72541E+04 0.01501  1.44920E+04 0.01956  1.46614E+04 0.02116  1.64073E+04 0.01654  1.54067E+04 0.02209  1.13800E+04 0.02286  1.15728E+04 0.01809  1.12265E+04 0.02357  1.02994E+04 0.02563  8.57904E+03 0.01924  6.19270E+03 0.03295  2.47430E+03 0.04118 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.98961E+01 0.00625  1.20265E+01 0.01785 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.59264E-01 0.00106  5.28949E-01 0.00017 ];
INF_CAPT                  (idx, [1:   4]) = [  1.47855E-05 0.03179  2.92384E-04 0.00237 ];
INF_ABS                   (idx, [1:   4]) = [  1.47855E-05 0.03179  2.92384E-04 0.00237 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  8.69909E-08 0.00340  3.21341E-06 0.00237 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.59245E-01 0.00106  5.28667E-01 0.00019 ];
INF_SCATT1                (idx, [1:   4]) = [  3.03436E-02 0.00592  2.83599E-02 0.00892 ];
INF_SCATT2                (idx, [1:   4]) = [  3.15481E-03 0.03814  1.59182E-03 0.26083 ];
INF_SCATT3                (idx, [1:   4]) = [  2.88814E-04 0.49890  4.89965E-04 0.46775 ];
INF_SCATT4                (idx, [1:   4]) = [ -4.59373E-06 1.00000  3.89147E-04 0.76863 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.80151E-05 1.00000  4.92656E-04 0.46084 ];
INF_SCATT6                (idx, [1:   4]) = [  1.27299E-04 0.71979  1.08967E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -8.15911E-05 0.91232  5.61005E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.59245E-01 0.00106  5.28667E-01 0.00019 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.03436E-02 0.00592  2.83599E-02 0.00892 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.15481E-03 0.03814  1.59182E-03 0.26083 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.88814E-04 0.49890  4.89965E-04 0.46775 ];
INF_SCATTP4               (idx, [1:   4]) = [ -4.59373E-06 1.00000  3.89147E-04 0.76863 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.80151E-05 1.00000  4.92656E-04 0.46084 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.27299E-04 0.71979  1.08967E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -8.15911E-05 0.91232  5.61005E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  3.90880E-01 0.00182  4.99506E-01 0.00055 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  8.52789E-01 0.00182  6.67327E-01 0.00055 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.47855E-05 0.03179  2.92384E-04 0.00237 ];
INF_REMXS                 (idx, [1:   4]) = [  3.36935E-03 0.00588  6.90770E-04 0.06124 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.55894E-01 0.00107  3.35113E-03 0.00584  4.08751E-04 0.08257  5.28258E-01 0.00021 ];
INF_S1                    (idx, [1:   8]) = [  3.12615E-02 0.00649 -9.17870E-04 0.02708  1.18871E-04 0.15026  2.82410E-02 0.00922 ];
INF_S2                    (idx, [1:   8]) = [  3.20664E-03 0.03785 -5.18345E-05 0.13574  1.18196E-05 0.61998  1.58000E-03 0.26429 ];
INF_S3                    (idx, [1:   8]) = [  2.98445E-04 0.45579 -9.63191E-06 1.00000 -1.57308E-05 0.38122  5.05696E-04 0.44608 ];
INF_S4                    (idx, [1:   8]) = [ -6.48304E-07 1.00000 -3.94542E-06 0.96380 -1.50888E-05 0.52035  4.04235E-04 0.72767 ];
INF_S5                    (idx, [1:   8]) = [ -1.33373E-05 1.00000 -4.67776E-06 1.00000 -9.59166E-06 0.61622  5.02248E-04 0.44782 ];
INF_S6                    (idx, [1:   8]) = [  1.24597E-04 0.79133  2.70199E-06 1.00000 -2.00293E-06 0.70683  1.10970E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -7.68717E-05 1.00000 -4.71936E-06 1.00000  3.90668E-06 1.00000  5.21939E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.55894E-01 0.00107  3.35113E-03 0.00584  4.08751E-04 0.08257  5.28258E-01 0.00021 ];
INF_SP1                   (idx, [1:   8]) = [  3.12615E-02 0.00649 -9.17870E-04 0.02708  1.18871E-04 0.15026  2.82410E-02 0.00922 ];
INF_SP2                   (idx, [1:   8]) = [  3.20664E-03 0.03785 -5.18345E-05 0.13574  1.18196E-05 0.61998  1.58000E-03 0.26429 ];
INF_SP3                   (idx, [1:   8]) = [  2.98445E-04 0.45579 -9.63191E-06 1.00000 -1.57308E-05 0.38122  5.05696E-04 0.44608 ];
INF_SP4                   (idx, [1:   8]) = [ -6.48304E-07 1.00000 -3.94542E-06 0.96380 -1.50888E-05 0.52035  4.04235E-04 0.72767 ];
INF_SP5                   (idx, [1:   8]) = [ -1.33373E-05 1.00000 -4.67776E-06 1.00000 -9.59166E-06 0.61622  5.02248E-04 0.44782 ];
INF_SP6                   (idx, [1:   8]) = [  1.24597E-04 0.79133  2.70199E-06 1.00000 -2.00293E-06 0.70683  1.10970E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -7.68717E-05 1.00000 -4.71936E-06 1.00000  3.90668E-06 1.00000  5.21939E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.98736E-01 0.00436  5.61292E-01 0.02672 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.71241E-01 0.00564  5.59907E-01 0.03600 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.71498E-01 0.00759  5.54297E-01 0.03801 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.92122E-01 0.00622  5.74547E-01 0.03607 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.67739E+00 0.00433  5.95470E-01 0.02518 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.94682E+00 0.00562  5.98523E-01 0.03698 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.94410E+00 0.00760  6.04504E-01 0.03419 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.14125E+00 0.00624  5.83383E-01 0.03827 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'semiHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  78]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/semiHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 15:53:17 2023' ;

% Run parameters:

POP                       (idx, 1)        = 1000 ;
CYCLES                    (idx, 1)        = 100 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193307 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  1.02011E+00  9.79891E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.91871E-01 0.00254  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.08129E-01 0.00060  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47277E-01 0.00074  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.51856E-01 0.00073  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  5.00175E+00 0.00339  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31481E-01 5.6E-05  6.77580E-02 0.00077  7.61042E-04 0.00519  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.34621E+01 0.00264  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.31769E+01 0.00265  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79540E+01 0.00250  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.45525E+01 0.00321  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 100 ;
SIMULATED_HISTORIES       (idx, 1)        = 100166 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  1.00166E+03 0.00496 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.79557E+00 ;
RUNNING_TIME              (idx, 1)        =  1.00708E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  3.31300E-01  3.31300E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  1.90500E-02  1.90500E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  9.72030E+00  9.72030E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.00704E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.27759 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.84028E+00 0.01321 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64622E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1025.15 ;
MEMSIZE                   (idx, 1)        = 989.07 ;
XS_MEMSIZE                (idx, 1)        = 605.81 ;
MAT_MEMSIZE               (idx, 1)        = 359.08 ;
RES_MEMSIZE               (idx, 1)        = 10.50 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 13.68 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 36.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.00293E-03 0.00237  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.46807E-02 0.02652 ];
U235_FISS                 (idx, [1:   4]) = [  4.39199E-01 0.00447  9.99474E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.32870E-04 0.23195  5.25866E-04 0.23679 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62635E-01 0.00844  5.85379E-01 0.00494 ];
U238_CAPT                 (idx, [1:   4]) = [  1.48477E-02 0.02659  5.34169E-02 0.02551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 100166 1.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 2.41068E+01 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 27735 2.76870E+04 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 43864 4.38074E+04 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 28567 2.85298E+04 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 100166 1.00024E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.91038E-11 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42070E-11 0.00234 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07167E+00 0.00233 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.38382E-01 0.00234 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75584E-01 0.00303 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.13966E-01 0.00209 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00293E+00 0.00237 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50799E+02 0.00250 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86034E-01 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33289E+01 0.00277 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.07298E+00 0.00297 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.44096E-01 0.00198 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.23359E-01 0.00671 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.44367E+00 0.00655 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.83145E-01 0.00185 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12609E-01 0.00113 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49849E+00 0.00298 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07084E+00 0.00331 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-07 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07170E+00 0.00345  1.06348E+00 0.00328  7.35718E-03 0.05253 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06924E+00 0.00360 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07193E+00 0.00233 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50156E+00 0.00126 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34335E+01 0.00177 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34194E+01 0.00135 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.01439E-05 0.02421 ];
IMP_EALF                  (idx, [1:   2]) = [  3.02093E-05 0.01840 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.44631E-02 0.02736 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60654E-02 0.00546 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.07965E-03 0.04015  1.96581E-04 0.23696  1.01793E-03 0.09528  6.01711E-04 0.13767  1.11660E-03 0.09060  1.91003E-03 0.06212  5.36301E-04 0.12243  5.40442E-04 0.12759  1.60058E-04 0.24004 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.53506E-01 0.08372  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  6.93534E-03 0.05630  2.21450E-04 0.27980  1.08069E-03 0.15695  6.78167E-04 0.19503  1.26387E-03 0.13507  2.32062E-03 0.10673  5.54197E-04 0.19468  6.43095E-04 0.19765  1.73254E-04 0.33820 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.51651E-01 0.10954  1.24667E-02 4.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.3E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67672E-05 0.02196  3.68381E-05 0.02220  2.06585E-05 0.24167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93251E-05 0.02126  3.94007E-05 0.02151  2.20625E-05 0.24176 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  6.81328E-03 0.05175  1.35883E-04 0.40535  1.28244E-03 0.13601  6.03943E-04 0.20762  1.12277E-03 0.15601  2.34410E-03 0.08730  4.37527E-04 0.21371  6.28693E-04 0.19382  2.57924E-04 0.33253 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.88064E-01 0.12742  1.24667E-02 5.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 2.7E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.58623E-05 0.06331  3.59061E-05 0.06328  5.98938E-06 0.37213 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.82047E-05 0.06229  3.82494E-05 0.06226  6.54196E-06 0.37896 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  4.58764E-03 0.22826  2.09799E-04 0.70412  4.26276E-04 0.62831  8.23766E-04 0.67583  1.28375E-03 0.50815  1.34711E-03 0.33716  3.32554E-04 0.91076  1.81276E-05 1.00000  1.46262E-04 0.71676 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.24988E-01 0.32151  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  4.87582E-03 0.20759  1.82745E-04 0.70679  5.17853E-04 0.56637  7.44756E-04 0.64557  1.29876E-03 0.43642  1.45681E-03 0.32227  4.34258E-04 0.86581  6.10376E-05 1.00000  1.79606E-04 0.72904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.24887E-01 0.32159  1.24667E-02 0.0E+00  2.82917E-02 8.6E-09  4.25244E-02 0.0E+00  1.33042E-01 0.0E+00  2.92467E-01 3.9E-09  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 1.5E-08 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.64992E+02 0.23252 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.68643E-05 0.01336 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94569E-05 0.01286 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82878E-03 0.03209 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85868E+02 0.03034 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.18281E-07 0.01065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83597E-05 0.00326  1.83535E-05 0.00329  1.46852E-05 0.07622 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.82786E-04 0.01179  1.82805E-04 0.01187  1.34780E-04 0.14920 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28311E-01 0.00543  2.28279E-01 0.00557  3.09039E-01 0.13858 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25374E+01 0.08025 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.31769E+01 0.00265  5.40817E+01 0.00364 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '_' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CAPT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_ABS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP1               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP2               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP3               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP4               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP5               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP6               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP7               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_REMXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP1                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP2                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP3                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP4                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP5                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP6                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP7                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

