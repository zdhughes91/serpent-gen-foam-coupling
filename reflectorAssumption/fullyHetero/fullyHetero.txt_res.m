
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  15]) = 'fullyHetero.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/reflectorAssumption/fullyHetero' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Nov 29 15:43:13 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Nov 29 16:00:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701294193326 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 2 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   2]) = [  9.34612E-01  1.06539E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  4.42330E-01 0.00074  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  5.57670E-01 0.00058  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  2.54698E-01 0.00033  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.11814E-01 0.00050  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  1.14512E+01 0.00156  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.40353E-01 1.4E-05  5.92757E-02 0.00022  3.70824E-04 0.00273  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.09150E+01 0.00080  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.07436E+01 0.00080  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11993E+02 0.00067  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.06117E+02 0.00153  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 599992 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99987E+03 0.00264 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99987E+03 0.00264 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.73370E+01 ;
RUNNING_TIME              (idx, 1)        =  1.74639E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  5.44917E-01  5.44917E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  2.22833E-02  2.22833E-02 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.68967E+01  1.68967E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.74638E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 0.99274 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  1.99416E+00 0.00137 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.68042E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.57 ;
ALLOC_MEMSIZE             (idx, 1)        = 1776.90 ;
MEMSIZE                   (idx, 1)        = 1714.28 ;
XS_MEMSIZE                (idx, 1)        = 1156.40 ;
MAT_MEMSIZE               (idx, 1)        = 489.95 ;
RES_MEMSIZE               (idx, 1)        = 0.85 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 67.07 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 62.62 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 96 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 641913 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 8 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 29 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 29 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 964 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.92838E-05 0.00102  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.40204E-02 0.01113 ];
U235_FISS                 (idx, [1:   4]) = [  4.19764E-01 0.00232  9.99310E-01 5.8E-05 ];
U238_FISS                 (idx, [1:   4]) = [  2.89872E-04 0.08420  6.89617E-04 0.08406 ];
U235_CAPT                 (idx, [1:   4]) = [  1.59668E-01 0.00364  3.92886E-01 0.00263 ];
U238_CAPT                 (idx, [1:   4]) = [  1.39213E-02 0.01116  3.42640E-02 0.01128 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 599992 6.00000E+05 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 4.38239E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 599992 6.04382E+05 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 244031 2.45585E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 253098 2.53846E+05 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 102863 1.04952E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 599992 6.04382E+05 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -8.14907E-09 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.36327E-11 0.00111 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02853E+00 0.00110 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.20660E-01 0.00111 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  4.05679E-01 0.00100 ];
TOT_ABSRATE               (idx, [1:   2]) = [  8.26339E-01 0.00060 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.92838E-01 0.00102 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.41183E+02 0.00105 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.73661E-01 0.00288 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.07281E+01 0.00114 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06324E+00 0.00168 ];
SIX_FF_F                  (idx, [1:   2]) = [  6.51388E-01 0.00218 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.47371E-01 0.00265 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.77290E+00 0.00302 ];
SIX_FF_LF                 (idx, [1:   2]) = [  8.44004E-01 0.00058 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.77579E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.25358E+00 0.00180 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03431E+00 0.00192 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44505E+00 9.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 4.7E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03476E+00 0.00191  1.02648E+00 0.00191  7.82725E-03 0.02045 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03612E+00 0.00109 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03601E+00 0.00148 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03612E+00 0.00109 ];
ABS_KINF                  (idx, [1:   2]) = [  1.25581E+00 0.00083 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.31915E+01 0.00070 ];
IMP_ALF                   (idx, [1:   2]) = [  1.31859E+01 0.00057 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.74242E-05 0.00928 ];
IMP_EALF                  (idx, [1:   2]) = [  3.75986E-05 0.00752 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.81640E-02 0.00966 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.00931E-01 0.00233 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.51388E-03 0.01590  2.41089E-04 0.09488  8.86979E-04 0.04357  6.10058E-04 0.05336  1.32489E-03 0.03447  2.14110E-03 0.02878  5.94789E-04 0.04862  5.49641E-04 0.05071  1.65328E-04 0.09516 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.21112E-01 0.02596  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.63851E-03 0.02065  3.42562E-04 0.12585  9.73586E-04 0.06662  7.05995E-04 0.08216  1.74439E-03 0.04787  2.39650E-03 0.04191  6.75147E-04 0.07413  5.96495E-04 0.07238  2.03829E-04 0.14056 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.07182E-01 0.03785  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.40034E-05 0.00947  2.40275E-05 0.00941  2.09319E-05 0.08210 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48284E-05 0.00893  2.48531E-05 0.00885  2.16781E-05 0.08231 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.62076E-03 0.02202  3.36567E-04 0.11464  1.01875E-03 0.06188  7.19694E-04 0.06754  1.62870E-03 0.05345  2.40085E-03 0.04451  6.62077E-04 0.06076  6.52127E-04 0.07380  2.01995E-04 0.13585 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15812E-01 0.03648  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 7.1E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.14071E-05 0.04801  2.14171E-05 0.04818  1.55273E-05 0.23787 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.21314E-05 0.04811  2.21426E-05 0.04828  1.59573E-05 0.23554 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.61864E-03 0.09649  2.31540E-04 0.40750  8.76484E-04 0.22530  6.08614E-04 0.29042  1.41563E-03 0.18503  2.29284E-03 0.12502  3.69065E-04 0.26781  7.07712E-04 0.30437  1.16743E-04 0.52954 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.01398E-01 0.10339  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 1.9E-09  2.92467E-01 0.0E+00  6.66488E-01 5.4E-09  1.63478E+00 2.7E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.48275E-03 0.09195  2.34684E-04 0.33809  8.57057E-04 0.22295  6.58122E-04 0.26841  1.31177E-03 0.17710  2.24817E-03 0.12436  3.74510E-04 0.26169  6.95295E-04 0.24787  1.03143E-04 0.47768 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.04335E-01 0.09414  1.24667E-02 3.9E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 1.9E-09  2.92467E-01 0.0E+00  6.66488E-01 6.1E-09  1.63478E+00 4.7E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.32734E+02 0.10277 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.38145E-05 0.00368 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46360E-05 0.00291 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.56494E-03 0.01272 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.17905E+02 0.01325 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  2.14763E-07 0.00355 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.60247E-05 0.00133  1.60273E-05 0.00132  1.56560E-05 0.01890 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.11700E-04 0.00394  1.11794E-04 0.00393  9.98648E-05 0.05310 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23335E-01 0.00243  2.23218E-01 0.00244  2.45967E-01 0.03596 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.25365E+01 0.03417 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.07436E+01 0.00080  4.98775E+01 0.00113 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.79410E+04 0.00644  4.61826E+05 0.00279  1.03711E+06 0.00105  1.88568E+06 0.00237  1.99237E+06 0.00064  1.85156E+06 0.00179  1.69827E+06 0.00055  1.50842E+06 0.00176  1.33770E+06 0.00145  1.20874E+06 0.00112  1.11094E+06 0.00149  1.02465E+06 0.00199  9.46363E+05 0.00223  9.03268E+05 0.00053  8.67573E+05 0.00078  7.35028E+05 0.00248  7.26604E+05 0.00045  6.95032E+05 0.00279  6.61069E+05 0.00186  1.22129E+06 0.00131  1.06608E+06 0.00148  6.92321E+05 0.00243  4.15369E+05 0.00085  4.44341E+05 0.00491  3.90512E+05 0.00216  3.08402E+05 0.00176  5.04900E+05 0.00312  1.00472E+05 0.00212  1.23278E+05 0.00371  1.11543E+05 0.00417  6.36111E+04 0.00635  1.09994E+05 0.00490  7.27001E+04 0.00665  5.98851E+04 0.00310  1.09051E+04 0.02340  1.07417E+04 0.00687  1.08727E+04 0.02578  1.11429E+04 0.00326  1.11931E+04 0.01489  1.10321E+04 0.01126  1.14539E+04 0.00581  1.06614E+04 0.00956  2.00133E+04 0.00283  3.12163E+04 0.00620  3.84866E+04 0.01194  9.76827E+04 0.00938  9.46096E+04 0.00412  8.97875E+04 0.00258  5.16665E+04 0.01109  3.34945E+04 0.00741  2.35870E+04 0.00078  2.50357E+04 0.00149  4.07194E+04 0.00634  4.63129E+04 0.00980  7.59252E+04 0.00972  1.06408E+05 0.00793  1.72530E+05 0.00815  1.32637E+05 0.00708  1.11286E+05 0.00651  9.06572E+04 0.00439  9.03753E+04 0.00489  9.88042E+04 0.00484  9.16949E+04 0.00089  6.68228E+04 0.00287  6.71847E+04 0.00550  6.40737E+04 0.00913  5.88044E+04 0.00753  4.94703E+04 0.00481  3.50031E+04 0.01160  1.34092E+04 0.02348 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.25542E+00 0.00099 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32595E+02 0.00067  8.58948E+00 0.00297 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.43538E-01 0.00085  6.02771E-01 0.00028 ];
INF_CAPT                  (idx, [1:   4]) = [  2.37461E-03 0.00073  1.05767E-02 0.00406 ];
INF_ABS                   (idx, [1:   4]) = [  4.70255E-03 0.00117  2.36174E-02 0.00133 ];
INF_FISS                  (idx, [1:   4]) = [  2.32794E-03 0.00179  1.30406E-02 0.00088 ];
INF_NSF                   (idx, [1:   4]) = [  5.69945E-03 0.00179  3.17688E-02 0.00088 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44828E+00 5.5E-06  2.43614E+00 1.3E-08 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 5.0E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.19657E-08 0.00130  2.88236E-06 0.00191 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.38832E-01 0.00090  5.79184E-01 0.00034 ];
INF_SCATT1                (idx, [1:   4]) = [  3.90604E-02 0.00135  4.08580E-02 0.00348 ];
INF_SCATT2                (idx, [1:   4]) = [  1.05963E-02 0.00165  4.71678E-03 0.02177 ];
INF_SCATT3                (idx, [1:   4]) = [  1.06883E-03 0.01264  1.17553E-03 0.08691 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.63152E-04 0.02614  2.76037E-04 0.47603 ];
INF_SCATT5                (idx, [1:   4]) = [  8.37063E-05 0.21875  9.37271E-05 0.78055 ];
INF_SCATT6                (idx, [1:   4]) = [  4.39195E-04 0.02796 -4.47818E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  7.22036E-05 0.56511 -1.05950E-04 0.97343 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.38887E-01 0.00090  5.79184E-01 0.00034 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.90733E-02 0.00135  4.08580E-02 0.00348 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.05992E-02 0.00165  4.71678E-03 0.02177 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.06931E-03 0.01274  1.17553E-03 0.08691 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.63373E-04 0.02631  2.76037E-04 0.47603 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.38084E-05 0.21983  9.37271E-05 0.78055 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.39196E-04 0.02739 -4.47818E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.19854E-05 0.56500 -1.05950E-04 0.97343 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.49526E-01 0.00067  5.57745E-01 0.00045 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.33586E+00 0.00067  5.97644E-01 0.00045 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.64786E-03 0.00104  2.36174E-02 0.00133 ];
INF_REMXS                 (idx, [1:   4]) = [  6.42072E-03 0.00166  2.42790E-02 0.00216 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.37117E-01 0.00089  1.71477E-03 0.00203  6.92583E-04 0.01523  5.78492E-01 0.00034 ];
INF_S1                    (idx, [1:   8]) = [  3.92228E-02 0.00135 -1.62311E-04 0.01390  1.96982E-04 0.01071  4.06610E-02 0.00354 ];
INF_S2                    (idx, [1:   8]) = [  1.06782E-02 0.00154 -8.18544E-05 0.02505  1.47518E-05 0.33595  4.70203E-03 0.02260 ];
INF_S3                    (idx, [1:   8]) = [  1.16046E-03 0.01093 -9.16347E-05 0.02049 -1.82846E-05 0.06380  1.19381E-03 0.08644 ];
INF_S4                    (idx, [1:   8]) = [ -5.29819E-04 0.02735 -3.33338E-05 0.03274 -1.60241E-05 0.17818  2.92061E-04 0.44338 ];
INF_S5                    (idx, [1:   8]) = [  7.42965E-05 0.26154  9.40976E-06 0.14201 -9.41872E-06 0.30083  1.03146E-04 0.70281 ];
INF_S6                    (idx, [1:   8]) = [  4.35442E-04 0.02612  3.75326E-06 0.27454 -4.35948E-06 0.09821 -4.04224E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  6.89676E-05 0.58023  3.23605E-06 0.30730 -7.03832E-06 0.20236 -9.89117E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.37172E-01 0.00089  1.71477E-03 0.00203  6.92583E-04 0.01523  5.78492E-01 0.00034 ];
INF_SP1                   (idx, [1:   8]) = [  3.92356E-02 0.00135 -1.62311E-04 0.01390  1.96982E-04 0.01071  4.06610E-02 0.00354 ];
INF_SP2                   (idx, [1:   8]) = [  1.06810E-02 0.00154 -8.18544E-05 0.02505  1.47518E-05 0.33595  4.70203E-03 0.02260 ];
INF_SP3                   (idx, [1:   8]) = [  1.16095E-03 0.01106 -9.16347E-05 0.02049 -1.82846E-05 0.06380  1.19381E-03 0.08644 ];
INF_SP4                   (idx, [1:   8]) = [ -5.30039E-04 0.02755 -3.33338E-05 0.03274 -1.60241E-05 0.17818  2.92061E-04 0.44338 ];
INF_SP5                   (idx, [1:   8]) = [  7.43986E-05 0.26272  9.40976E-06 0.14201 -9.41872E-06 0.30083  1.03146E-04 0.70281 ];
INF_SP6                   (idx, [1:   8]) = [  4.35443E-04 0.02553  3.75326E-06 0.27454 -4.35948E-06 0.09821 -4.04224E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  6.87494E-05 0.58016  3.23605E-06 0.30730 -7.03832E-06 0.20236 -9.89117E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.70540E-01 0.00058  8.32095E-01 0.00675 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.75026E-01 0.00250  1.09361E+00 0.01041 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.74846E-01 0.00145  1.17123E+00 0.03164 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.62162E-01 9.3E-05  5.44841E-01 0.00662 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.23210E+00 0.00058  4.00632E-01 0.00677 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.21202E+00 0.00249  3.04868E-01 0.01037 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.21281E+00 0.00145  2.85175E-01 0.03185 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.27148E+00 9.3E-05  6.11853E-01 0.00665 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.63851E-03 0.02065  3.42562E-04 0.12585  9.73586E-04 0.06662  7.05995E-04 0.08216  1.74439E-03 0.04787  2.39650E-03 0.04191  6.75147E-04 0.07413  5.96495E-04 0.07238  2.03829E-04 0.14056 ];
LAMBDA                    (idx, [1:  18]) = [  4.07182E-01 0.03785  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

