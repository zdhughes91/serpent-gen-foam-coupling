# NERVA Kiwi B4E

Author:     Zach Hughes, Texas A&M University (zhughes@tamu.edu)

### Folder Descriptions
1. 
### Personal Notes
1. neutronicAnalysis (all info shown in README is in here)
    - this folder contains all Serpent files that were used to perform neutronic analysis on the Kiwi B4E reactor. The 'case' folder within this folder contains the most updated version of Thomas' full core GeN-Foam model. The 'serpent' folder contains all the serpent files used for neutronic analysis. To run, one just needs to type "<path/to/sss2> coreSupport.txt" (should take ~40min on 8 cores). All post processing and is done using postProcess.ipynb. This file can analyze serpent results even before it is done with the simulation. The 'moreTools' folder contains scripts that can assist in serpent/openfoam coupling. 
2. homogenousAssumption
    - contains a heterogenous and a homogenized Kiwi reactor for comparison. This was made to determine if the kiwi fuel assemblies could be homogenized
3. predictAlpha
    - the file dopplerFeedback.py will run everything and return the fuel temperature feedback coefficient. In the 'changeSpecificIso' folder, each isotope temperature was changed manually to determine feedbacks of each isotope. this is where we found a positive feedback for U-235 temperature
4. reflectorAssumption
    - the file runBoth.py will run everything and return the difference in keff between the two models. The first model is normal kiwi and the second is the active core surrounded by a complete graphite reflector. This was done to determine if a complete graphite reflector is an acceptable assumption/simplification
5. serpentFiles
    - this is where manipulation of the serpent files was done. Mostly to confirm the homogenization file worked as intended among other things.
6. tools
    - a home for all useful scripts. I think the most useful is homogenize.py, which automatically homogenizes universes and prints them in serpent-readable format if you fill out a dictionary. It is pretty easy to use if you read the file. meanFreePath.py will print MFP by reading serpent output


### To do:

## Introduction

The Kiwi B4E core geometry is shown below. This core is characterized by four distinct regions:
1. Active core
2. Inner reflector
3. Outer reflector
4. Control System

<img src="images/coreGeometry.png" alt="Reactor" width="800"/>

*Fig 1: Kiwi B4E Core Diagram*

## Results
### Active Core

The core was analyzed to determine the power distribution in the active core. The Serpent model was set up by changing the energy deposition mode to "mode 3", which allows for coupled neutron-photon transport*. The paper by ([Tuominen, 2019](https://www.sciencedirect.com/science/article/pii/S0306454919300726)) describes the implementation of this feature in detail. Figure 2 below illustrates the power distribution from different energy deposition methods. 

*This energy deposition mode requires cross section data that is not found in the standard ACE-format cross section files used in Serpent, so additional data must be downloaded to use this feature. [This](https://serpent.vtt.fi/mediawiki/index.php/Input_syntax_manual#set_edepmode) link gives more information about the topic.

<img src="images/activeCoreBOLpowerDistribution.png" alt="aCore" width="1500"/>

*Fig 2: Active Core Power Distribution*

### Reflector 
Both reflector regions are primarily graphite. There is a coolant gap between the two that contains hydrogen, as well as coolant channels through the outer reflector. The axial neutron and gamma heating in these regions is shown in Figure 3. 

<img src="images/reflectorAxialNandGHeating.png" alt="axialRefl" width="1500"/>

*Fig 3: Axial Neutron and Gamma Heating in Reflector Regions*

The radial neutron and gamma heating in these regions was also found, and this is shown in Figure 4. 

<img src="images/reflectorRadialNandGHeating.png" alt="radialRefl" width="1500"/>

*Fig 4: Radial Neutron and Gamma Heating in Reflector Regions*

There is a peaking of gamma heating at two locations on either side of a decrease in neutron heating in the outer reflector region. It is believed that an (n,g) reaction is happening in the coolant channels, which is leading to this behavior. 


### Decay Heat
To determine the magnitude of decay heat in the Kiwi B4E reactor, a one hour burn was first analyzed. This is likely the upper limit of the realistic operating time of the core. In Figure 5, the total decay heat is plotted as a function of time along with the decay heat generated from the two nuclides that contribute the most to decay heat. 


<img src="images/totalDecayHeatPlusU239.png" alt="totDecayH" width="800"/>

*Fig 5: Total Decay Heat with Top Two Contributing Nuclides*

It is obvious from this plot that U-239 is the only relevant nuclide that produces decay heat. Since the active core in this reactor is comprised of several fuel types, the decay heat produced by each different fuel type was found, and is shown below  in Figure 6. Keep in mind fuel type 45 is by far the most common fuel type in the core, and it also has the highest enrichment of U-235. 

<img src="images/fuelTypeDecayHeat.png" alt="fuelTypeDecayH" width="800"/>

*Fig 6: Decay Heat per fuel type*
