
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32059E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67765E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32501 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75759E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   2]) = '40' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  4.86922E+05 0.00525  2.31021E+06 0.00195  5.19889E+06 0.00148  9.37828E+06 0.00066  9.94647E+06 0.00018  9.24748E+06 0.00075  8.43442E+06 0.00046  7.50913E+06 0.00074  6.69539E+06 0.00050  6.05472E+06 0.00030  5.58316E+06 0.00078  5.19860E+06 0.00019  4.83532E+06 0.00063  4.63996E+06 0.00084  4.46825E+06 0.00070  3.79925E+06 0.00154  3.72839E+06 0.00150  3.56861E+06 0.00098  3.41258E+06 0.00111  6.32760E+06 0.00036  5.55336E+06 0.00150  3.65654E+06 0.00099  2.20346E+06 0.00138  2.36301E+06 0.00074  2.09912E+06 0.00103  1.67071E+06 0.00172  2.78575E+06 0.00098  5.68408E+05 0.00132  6.99680E+05 0.00033  6.29848E+05 0.00126  3.59355E+05 0.00228  6.18418E+05 0.00466  4.11691E+05 0.00077  3.39486E+05 0.00037  6.36309E+04 0.00113  6.17698E+04 0.00966  6.26985E+04 0.00544  6.53296E+04 0.00329  6.38162E+04 0.00161  6.28634E+04 0.00594  6.42824E+04 0.00397  6.07240E+04 0.00478  1.13323E+05 0.00302  1.77835E+05 0.00480  2.22767E+05 0.00216  5.65343E+05 0.00099  5.54533E+05 0.00218  5.32170E+05 0.00268  3.06964E+05 0.00205  2.02511E+05 0.00220  1.44727E+05 0.00158  1.52457E+05 0.00334  2.52512E+05 0.00203  2.92455E+05 0.00388  4.94624E+05 0.00127  7.34211E+05 0.00094  1.30154E+06 0.00232  1.07311E+06 0.00280  9.30814E+05 0.00192  7.74669E+05 0.00173  7.85299E+05 0.00149  8.71802E+05 0.00109  8.21107E+05 0.00042  6.08308E+05 0.00181  6.10861E+05 0.00226  5.95022E+05 0.00161  5.50723E+05 0.00145  4.68077E+05 0.00383  3.35533E+05 0.00369  1.30702E+05 0.00308 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.49650E+00 0.00136 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36182E+02 0.00090  1.35064E+01 0.00101 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.37631E-01 0.00014  5.25567E-01 0.00015 ];
INF_CAPT                  (idx, [1:   4]) = [  1.81921E-03 0.00092  2.28216E-03 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  4.12097E-03 0.00069  1.14703E-02 0.00057 ];
INF_FISS                  (idx, [1:   4]) = [  2.30176E-03 0.00059  9.18815E-03 0.00069 ];
INF_NSF                   (idx, [1:   4]) = [  5.63478E-03 0.00059  2.23836E-02 0.00069 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44803E+00 2.2E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.3E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.52712E-08 0.00103  3.11896E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.33517E-01 0.00015  5.14106E-01 0.00015 ];
INF_SCATT1                (idx, [1:   4]) = [  3.73393E-02 0.00024  3.13894E-02 0.00125 ];
INF_SCATT2                (idx, [1:   4]) = [  1.00521E-02 0.00023  3.29430E-03 0.01530 ];
INF_SCATT3                (idx, [1:   4]) = [  9.55473E-04 0.01441  7.89851E-04 0.05965 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.87830E-04 0.01138  2.29603E-04 0.07185 ];
INF_SCATT5                (idx, [1:   4]) = [  7.61132E-05 0.18122  4.93047E-05 0.87401 ];
INF_SCATT6                (idx, [1:   4]) = [  4.10477E-04 0.01756 -3.85953E-06 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  4.37207E-05 0.27096 -4.28171E-08 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.33519E-01 0.00015  5.14106E-01 0.00015 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.73394E-02 0.00024  3.13894E-02 0.00125 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.00521E-02 0.00023  3.29430E-03 0.01530 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.55499E-04 0.01442  7.89851E-04 0.05965 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.87807E-04 0.01140  2.29603E-04 0.07185 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.60972E-05 0.18103  4.93047E-05 0.87401 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.10512E-04 0.01756 -3.85953E-06 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.36852E-05 0.27087 -4.28171E-08 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.51028E-01 0.00028  4.92329E-01 0.00023 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.32788E+00 0.00028  6.77055E-01 0.00023 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.11910E-03 0.00069  1.14703E-02 0.00057 ];
INF_REMXS                 (idx, [1:   4]) = [  5.80242E-03 0.00044  1.19449E-02 0.00077 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.31829E-01 0.00015  1.68839E-03 0.00108  4.84567E-04 0.01020  5.13622E-01 0.00016 ];
INF_S1                    (idx, [1:   8]) = [  3.75088E-02 0.00023 -1.69551E-04 0.00189  1.26945E-04 0.01370  3.12625E-02 0.00122 ];
INF_S2                    (idx, [1:   8]) = [  1.01308E-02 0.00023 -7.86887E-05 0.01489  5.80614E-06 0.08932  3.28849E-03 0.01542 ];
INF_S3                    (idx, [1:   8]) = [  1.04281E-03 0.01339 -8.73332E-05 0.00242 -1.15439E-05 0.14884  8.01395E-04 0.05891 ];
INF_S4                    (idx, [1:   8]) = [ -5.58562E-04 0.01142 -2.92682E-05 0.03036 -1.08983E-05 0.06857  2.40501E-04 0.06550 ];
INF_S5                    (idx, [1:   8]) = [  6.79772E-05 0.20231  8.13600E-06 0.02271 -6.12199E-06 0.11572  5.54267E-05 0.78809 ];
INF_S6                    (idx, [1:   8]) = [  4.03480E-04 0.01842  6.99719E-06 0.04163 -5.14786E-06 0.28191  1.28833E-06 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  4.46140E-05 0.24993 -8.93253E-07 0.82124 -5.03312E-06 0.13603  4.99030E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.31831E-01 0.00015  1.68839E-03 0.00108  4.84567E-04 0.01020  5.13622E-01 0.00016 ];
INF_SP1                   (idx, [1:   8]) = [  3.75089E-02 0.00023 -1.69551E-04 0.00189  1.26945E-04 0.01370  3.12625E-02 0.00122 ];
INF_SP2                   (idx, [1:   8]) = [  1.01308E-02 0.00023 -7.86887E-05 0.01489  5.80614E-06 0.08932  3.28849E-03 0.01542 ];
INF_SP3                   (idx, [1:   8]) = [  1.04283E-03 0.01340 -8.73332E-05 0.00242 -1.15439E-05 0.14884  8.01395E-04 0.05891 ];
INF_SP4                   (idx, [1:   8]) = [ -5.58539E-04 0.01144 -2.92682E-05 0.03036 -1.08983E-05 0.06857  2.40501E-04 0.06550 ];
INF_SP5                   (idx, [1:   8]) = [  6.79612E-05 0.20210  8.13600E-06 0.02271 -6.12199E-06 0.11572  5.54267E-05 0.78809 ];
INF_SP6                   (idx, [1:   8]) = [  4.03515E-04 0.01842  6.99719E-06 0.04163 -5.14786E-06 0.28191  1.28833E-06 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  4.45785E-05 0.24983 -8.93253E-07 0.82124 -5.03312E-06 0.13603  4.99030E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.79848E-01 0.00076  7.77042E-01 0.00221 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.84048E-01 0.00087  9.87746E-01 0.01408 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.83434E-01 0.00099  1.00088E+00 0.00808 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.72376E-01 0.00044  5.40926E-01 0.00513 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.19112E+00 0.00076  4.28981E-01 0.00221 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.17351E+00 0.00088  3.37601E-01 0.01390 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.17605E+00 0.00099  3.33084E-01 0.00814 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.22380E+00 0.00044  6.16259E-01 0.00515 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
LAMBDA                    (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32060E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67765E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32502 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75759E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'F' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.72355E+05 0.00467  1.29091E+06 0.00209  2.95309E+06 0.00107  5.18915E+06 0.00092  5.43481E+06 0.00076  5.01724E+06 0.00067  4.53420E+06 0.00142  3.97663E+06 0.00044  3.47821E+06 0.00051  3.06987E+06 0.00019  2.76409E+06 0.00200  2.51431E+06 0.00103  2.28071E+06 0.00240  2.14314E+06 0.00236  2.02745E+06 0.00099  1.69507E+06 0.00314  1.63949E+06 0.00243  1.53523E+06 0.00178  1.43422E+06 0.00233  2.55083E+06 0.00107  2.06319E+06 0.00270  1.23625E+06 0.00196  6.85082E+05 0.00325  6.44214E+05 0.00266  5.00418E+05 0.00256  3.63813E+05 0.00104  5.24144E+05 0.00465  1.06118E+05 0.00969  1.33574E+05 0.00297  1.25919E+05 0.00658  7.10783E+04 0.00776  1.26297E+05 0.00774  8.33055E+04 0.00069  6.32146E+04 0.00765  1.08641E+04 0.00894  1.01974E+04 0.03190  1.04586E+04 0.01857  1.08999E+04 0.01383  1.06544E+04 0.00961  1.08218E+04 0.01982  1.08095E+04 0.00671  9.98863E+03 0.01320  1.90440E+04 0.01398  2.97307E+04 0.00958  3.62061E+04 0.00638  8.93912E+04 0.00315  8.06058E+04 0.00099  6.67896E+04 0.00709  3.23922E+04 0.00572  1.87131E+04 0.02448  1.19660E+04 0.00088  1.14165E+04 0.01901  1.70690E+04 0.02156  1.75523E+04 0.01894  2.58794E+04 0.00858  2.74833E+04 0.01270  2.97773E+04 0.00790  1.53253E+04 0.00391  9.45115E+03 0.01251  6.32426E+03 0.00860  5.42323E+03 0.03075  5.26573E+03 0.03336  4.26939E+03 0.02453  2.58497E+03 0.01588  2.39464E+03 0.05098  2.02523E+03 0.03355  1.53872E+03 0.05451  1.21852E+03 0.00702  6.37365E+02 0.06524  1.81964E+02 0.07664 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.60099E+00 0.00124 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  6.27017E+01 0.00115  3.95749E-01 0.00204 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55492E-01 0.00015  4.68634E-01 0.00012 ];
INF_CAPT                  (idx, [1:   4]) = [  2.40647E-03 0.00058  2.38419E-02 0.00061 ];
INF_ABS                   (idx, [1:   4]) = [  6.40089E-03 0.00067  1.54156E-01 0.00065 ];
INF_FISS                  (idx, [1:   4]) = [  3.99442E-03 0.00072  1.30314E-01 0.00067 ];
INF_NSF                   (idx, [1:   4]) = [  9.78141E-03 0.00072  3.17464E-01 0.00067 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44877E+00 4.0E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 2.0E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  2.33231E-08 0.00224  1.59527E-06 0.00119 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49102E-01 0.00013  3.14381E-01 0.00087 ];
INF_SCATT1                (idx, [1:   4]) = [  2.05368E-02 0.00166  1.82818E-02 0.01364 ];
INF_SCATT2                (idx, [1:   4]) = [  4.46753E-03 0.00220  1.21782E-03 0.15903 ];
INF_SCATT3                (idx, [1:   4]) = [  9.67167E-04 0.01925  2.90983E-04 0.14239 ];
INF_SCATT4                (idx, [1:   4]) = [  2.33001E-04 0.03285  2.72670E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.31973E-04 0.10462 -1.26702E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.12783E-04 0.15575 -9.98707E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.02086E-05 0.18651  1.10213E-04 0.85709 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49105E-01 0.00013  3.14381E-01 0.00087 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.05370E-02 0.00166  1.82818E-02 0.01364 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.46747E-03 0.00220  1.21782E-03 0.15903 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.67198E-04 0.01926  2.90983E-04 0.14239 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.33018E-04 0.03289  2.72670E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.31956E-04 0.10435 -1.26702E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.12854E-04 0.15592 -9.98707E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.01604E-05 0.18635  1.10213E-04 0.85709 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.01923E-01 0.00023  4.32574E-01 0.00081 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.65079E+00 0.00023  7.70583E-01 0.00081 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.39808E-03 0.00067  1.54156E-01 0.00065 ];
INF_REMXS                 (idx, [1:   4]) = [  6.70698E-03 0.00093  1.56105E-01 0.00201 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48785E-01 0.00014  3.16605E-04 0.00607  1.85197E-03 0.02028  3.12529E-01 0.00099 ];
INF_S1                    (idx, [1:   8]) = [  2.06079E-02 0.00165 -7.10586E-05 0.00137  4.14260E-04 0.03436  1.78676E-02 0.01358 ];
INF_S2                    (idx, [1:   8]) = [  4.47666E-03 0.00229 -9.13222E-06 0.06852 -1.85441E-07 1.00000  1.21801E-03 0.17852 ];
INF_S3                    (idx, [1:   8]) = [  9.72118E-04 0.01904 -4.95120E-06 0.04286 -6.20915E-05 0.07711  3.53074E-04 0.13010 ];
INF_S4                    (idx, [1:   8]) = [  2.35208E-04 0.03129 -2.20696E-06 0.19432 -3.09853E-05 0.46482  5.82523E-05 0.84800 ];
INF_S5                    (idx, [1:   8]) = [  1.31249E-04 0.10380  7.23998E-07 0.48129 -3.29784E-05 0.13802  2.03082E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.12749E-04 0.15615  3.40037E-08 1.00000 -1.54413E-05 0.77196 -8.44294E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.01288E-05 0.19895  7.97327E-08 1.00000 -1.27435E-05 1.00000  1.22956E-04 0.89009 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48788E-01 0.00014  3.16605E-04 0.00607  1.85197E-03 0.02028  3.12529E-01 0.00099 ];
INF_SP1                   (idx, [1:   8]) = [  2.06080E-02 0.00165 -7.10586E-05 0.00137  4.14260E-04 0.03436  1.78676E-02 0.01358 ];
INF_SP2                   (idx, [1:   8]) = [  4.47661E-03 0.00229 -9.13222E-06 0.06852 -1.85441E-07 1.00000  1.21801E-03 0.17852 ];
INF_SP3                   (idx, [1:   8]) = [  9.72149E-04 0.01905 -4.95120E-06 0.04286 -6.20915E-05 0.07711  3.53074E-04 0.13010 ];
INF_SP4                   (idx, [1:   8]) = [  2.35225E-04 0.03132 -2.20696E-06 0.19432 -3.09853E-05 0.46482  5.82523E-05 0.84800 ];
INF_SP5                   (idx, [1:   8]) = [  1.31232E-04 0.10354  7.23998E-07 0.48129 -3.29784E-05 0.13802  2.03082E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.12820E-04 0.15632  3.40037E-08 1.00000 -1.54413E-05 0.77196 -8.44294E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.00807E-05 0.19880  7.97327E-08 1.00000 -1.27435E-05 1.00000  1.22956E-04 0.89009 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.44071E-01 0.00145 -6.26821E+00 0.10579 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  3.97453E-01 0.00203 -6.94006E-01 0.01401 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  3.96650E-01 0.00154 -6.54429E-01 0.05467 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.71592E-01 0.00125  4.02266E-01 0.02035 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  9.68797E-01 0.00145 -5.45189E-02 0.11640 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  8.38680E-01 0.00203 -4.80494E-01 0.01421 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  8.40375E-01 0.00154 -5.12379E-01 0.05414 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.22734E+00 0.00125  8.29316E-01 0.02004 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.48665E-03 0.01160  2.42132E-04 0.07345  1.09890E-03 0.03046  7.28534E-04 0.04106  1.50856E-03 0.02825  2.35263E-03 0.01924  7.44257E-04 0.03730  6.15199E-04 0.04643  1.96434E-04 0.07844 ];
LAMBDA                    (idx, [1:  18]) = [  4.21217E-01 0.01815  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32060E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67767E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32501 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75757E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'T' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.70718E+04 0.02860  8.24555E+04 0.00681  1.86041E+05 0.00157  3.34446E+05 0.00343  3.57523E+05 0.00142  3.33030E+05 0.00579  3.01078E+05 0.00357  2.62987E+05 0.00181  2.31755E+05 0.00377  2.01928E+05 0.00311  1.83451E+05 0.00230  1.69696E+05 0.00404  1.52025E+05 0.00537  1.48093E+05 0.00234  1.39885E+05 0.00094  1.17531E+05 0.00118  1.13313E+05 0.00507  1.07630E+05 0.00484  9.86623E+04 0.00410  1.78125E+05 0.00418  1.44979E+05 0.00343  8.70226E+04 0.00241  4.89781E+04 0.00649  4.68594E+04 0.00877  3.80624E+04 0.01394  2.61157E+04 0.00346  3.78424E+04 0.00334  7.00572E+03 0.00864  8.90812E+03 0.00133  7.96643E+03 0.02094  4.71547E+03 0.01108  8.08234E+03 0.01840  5.31986E+03 0.03048  4.27626E+03 0.02914  8.32261E+02 0.02235  6.83477E+02 0.04357  7.05485E+02 0.04915  6.83561E+02 0.00917  7.15973E+02 0.05477  7.13319E+02 0.04444  7.66667E+02 0.08152  7.06462E+02 0.03228  1.35221E+03 0.04629  1.97518E+03 0.05910  2.35209E+03 0.02575  6.22892E+03 0.01126  5.63775E+03 0.01223  4.75267E+03 0.02847  2.41007E+03 0.03084  1.51662E+03 0.03684  9.74398E+02 0.10305  9.45918E+02 0.02254  1.62092E+03 0.03659  1.70861E+03 0.00631  2.31520E+03 0.02929  2.57792E+03 0.01809  3.14520E+03 0.05822  1.61605E+03 0.01436  1.10095E+03 0.04178  7.90891E+02 0.01726  7.60916E+02 0.05695  8.00060E+02 0.04596  6.79780E+02 0.08044  4.84887E+02 0.06039  4.60024E+02 0.02625  4.67105E+02 0.09061  3.98227E+02 0.06232  3.74597E+02 0.04627  2.56951E+02 0.12114  1.06172E+02 0.15991 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.20488E+00 0.00167  3.58531E-02 0.00818 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.19644E-01 0.00070  6.29389E-01 0.00976 ];
INF_CAPT                  (idx, [1:   4]) = [  8.48699E-03 0.00225  2.52815E-02 0.01518 ];
INF_ABS                   (idx, [1:   4]) = [  8.48699E-03 0.00225  2.52815E-02 0.01518 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.40349E-08 0.00256  1.91780E-06 0.00464 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.11148E-01 0.00060  6.03327E-01 0.00918 ];
INF_SCATT1                (idx, [1:   4]) = [  1.51774E-01 0.00142  2.14822E-01 0.00636 ];
INF_SCATT2                (idx, [1:   4]) = [  6.28446E-02 0.00113  8.81058E-02 0.00195 ];
INF_SCATT3                (idx, [1:   4]) = [  2.65776E-03 0.03685  2.69938E-02 0.04325 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.67804E-03 0.00652  3.93004E-03 0.06269 ];
INF_SCATT5                (idx, [1:   4]) = [  2.53306E-04 0.05821 -1.68108E-03 0.11149 ];
INF_SCATT6                (idx, [1:   4]) = [  3.32579E-03 0.00299 -1.11862E-03 0.33145 ];
INF_SCATT7                (idx, [1:   4]) = [  1.86754E-04 0.15225 -8.18196E-04 0.39432 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.11153E-01 0.00060  6.03327E-01 0.00918 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.51773E-01 0.00143  2.14822E-01 0.00636 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.28448E-02 0.00113  8.81058E-02 0.00195 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.65741E-03 0.03679  2.69938E-02 0.04325 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.67784E-03 0.00654  3.93004E-03 0.06269 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.52967E-04 0.05761 -1.68108E-03 0.11149 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.32570E-03 0.00302 -1.11862E-03 0.33145 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.86868E-04 0.15226 -8.18196E-04 0.39432 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.00234E-01 0.00046  3.95191E-01 0.01032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.66472E+00 0.00046  8.43655E-01 0.01043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.48256E-03 0.00222  2.52815E-02 0.01518 ];
INF_REMXS                 (idx, [1:   4]) = [  1.14411E-02 0.00517  2.74636E-02 0.02300 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.08203E-01 0.00058  2.94561E-03 0.00307  1.40200E-03 0.04446  6.01925E-01 0.00915 ];
INF_S1                    (idx, [1:   8]) = [  1.50726E-01 0.00138  1.04741E-03 0.01083  2.90906E-04 0.47749  2.14532E-01 0.00605 ];
INF_S2                    (idx, [1:   8]) = [  6.32133E-02 0.00112 -3.68782E-04 0.00407  3.30531E-04 0.21625  8.77753E-02 0.00277 ];
INF_S3                    (idx, [1:   8]) = [  3.20504E-03 0.03155 -5.47278E-04 0.00930  2.35779E-04 0.42538  2.67580E-02 0.04737 ];
INF_S4                    (idx, [1:   8]) = [ -6.52401E-03 0.00576 -1.54037E-04 0.03964  1.78581E-04 0.31867  3.75146E-03 0.07915 ];
INF_S5                    (idx, [1:   8]) = [  1.91604E-04 0.07489  6.17018E-05 0.02012  1.68957E-04 0.09157 -1.85004E-03 0.10236 ];
INF_S6                    (idx, [1:   8]) = [  3.28528E-03 0.00368  4.05100E-05 0.06963  6.19635E-05 0.39288 -1.18058E-03 0.30115 ];
INF_S7                    (idx, [1:   8]) = [  1.95709E-04 0.14167 -8.95566E-06 0.08832  3.85938E-05 0.89300 -8.56789E-04 0.41532 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.08207E-01 0.00058  2.94561E-03 0.00307  1.40200E-03 0.04446  6.01925E-01 0.00915 ];
INF_SP1                   (idx, [1:   8]) = [  1.50726E-01 0.00138  1.04741E-03 0.01083  2.90906E-04 0.47749  2.14532E-01 0.00605 ];
INF_SP2                   (idx, [1:   8]) = [  6.32136E-02 0.00112 -3.68782E-04 0.00407  3.30531E-04 0.21625  8.77753E-02 0.00277 ];
INF_SP3                   (idx, [1:   8]) = [  3.20469E-03 0.03149 -5.47278E-04 0.00930  2.35779E-04 0.42538  2.67580E-02 0.04737 ];
INF_SP4                   (idx, [1:   8]) = [ -6.52380E-03 0.00578 -1.54037E-04 0.03964  1.78581E-04 0.31867  3.75146E-03 0.07915 ];
INF_SP5                   (idx, [1:   8]) = [  1.91265E-04 0.07419  6.17018E-05 0.02012  1.68957E-04 0.09157 -1.85004E-03 0.10236 ];
INF_SP6                   (idx, [1:   8]) = [  3.28519E-03 0.00370  4.05100E-05 0.06963  6.19635E-05 0.39288 -1.18058E-03 0.30115 ];
INF_SP7                   (idx, [1:   8]) = [  1.95824E-04 0.14168 -8.95566E-06 0.08832  3.85938E-05 0.89300 -8.56789E-04 0.41532 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.25276E-01 0.00233  1.83122E+00 0.09400 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.55189E-01 0.00157 -4.92931E+00 0.23289 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.56991E-01 0.00683  1.06350E+00 1.00000 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.81595E-01 0.00405  5.60672E-01 0.02334 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.47969E+00 0.00233  1.85024E-01 0.08614 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.30623E+00 0.00158 -7.53157E-02 0.22506 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.29718E+00 0.00681  3.52281E-02 1.00000 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.83565E+00 0.00404  5.95159E-01 0.02281 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32060E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67768E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32501 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75754E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'C' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.90812E+04 0.01381  1.87202E+05 0.00220  4.28658E+05 0.00154  7.60163E+05 0.00396  8.00160E+05 0.00202  7.42199E+05 0.00150  6.63512E+05 0.00151  5.79545E+05 0.00329  5.08116E+05 0.00128  4.48719E+05 0.00120  4.05863E+05 0.00251  3.74761E+05 0.00113  3.39198E+05 0.00139  3.34171E+05 0.00222  3.12188E+05 0.00184  2.64552E+05 0.00434  2.62153E+05 0.00609  2.47819E+05 0.00510  2.38309E+05 0.00267  4.35992E+05 0.00204  3.71423E+05 0.00190  2.38349E+05 0.00376  1.39416E+05 0.00317  1.44007E+05 0.00203  1.22983E+05 0.00487  9.14834E+04 0.00183  1.40909E+05 0.00321  2.84605E+04 0.01633  3.51804E+04 0.00749  3.12338E+04 0.01595  1.77250E+04 0.00080  3.02934E+04 0.00444  2.01392E+04 0.00993  1.64596E+04 0.00340  3.07523E+03 0.03132  2.97204E+03 0.00679  2.84543E+03 0.00993  3.03124E+03 0.00944  3.02798E+03 0.01304  2.94429E+03 0.01121  2.97557E+03 0.00782  2.80029E+03 0.02266  5.32187E+03 0.03424  8.06262E+03 0.01191  1.02585E+04 0.01073  2.58269E+04 0.01349  2.46465E+04 0.00928  2.21574E+04 0.01003  1.19494E+04 0.00342  7.46892E+03 0.00432  5.27088E+03 0.02589  5.20640E+03 0.01065  8.57976E+03 0.00379  9.35648E+03 0.01250  1.38135E+04 0.00418  1.77246E+04 0.00748  2.38465E+04 0.00251  1.59572E+04 0.01494  1.24488E+04 0.00172  9.39919E+03 0.00273  9.15175E+03 0.01624  9.78528E+03 0.01520  8.32093E+03 0.01174  5.87156E+03 0.03856  5.91668E+03 0.01432  5.73287E+03 0.00503  5.12854E+03 0.02438  4.09751E+03 0.00737  3.15325E+03 0.00845  1.21743E+03 0.02576 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.86021E+00 0.00155  2.45866E-01 0.00366 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.00000E-01 0.00025  7.28453E-01 0.00259 ];
INF_CAPT                  (idx, [1:   4]) = [  1.78492E-03 0.00594  1.48003E-02 0.00267 ];
INF_ABS                   (idx, [1:   4]) = [  1.78492E-03 0.00594  1.48003E-02 0.00267 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.38750E-08 0.00072  2.36403E-06 0.00098 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.98220E-01 0.00024  7.13736E-01 0.00241 ];
INF_SCATT1                (idx, [1:   4]) = [  1.60356E-01 0.00110  2.11295E-01 0.00200 ];
INF_SCATT2                (idx, [1:   4]) = [  6.41074E-02 0.00100  8.14613E-02 0.00283 ];
INF_SCATT3                (idx, [1:   4]) = [  2.41119E-03 0.00168  2.60274E-02 0.01187 ];
INF_SCATT4                (idx, [1:   4]) = [ -7.24694E-03 0.00541  6.87246E-03 0.07318 ];
INF_SCATT5                (idx, [1:   4]) = [ -2.97466E-05 1.00000  1.50090E-03 0.07608 ];
INF_SCATT6                (idx, [1:   4]) = [  3.35161E-03 0.01557  7.40031E-04 0.35301 ];
INF_SCATT7                (idx, [1:   4]) = [  3.12818E-04 0.12765  7.75748E-04 0.37571 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.98223E-01 0.00024  7.13736E-01 0.00241 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.60356E-01 0.00110  2.11295E-01 0.00200 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.41078E-02 0.00100  8.14613E-02 0.00283 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.41134E-03 0.00167  2.60274E-02 0.01187 ];
INF_SCATTP4               (idx, [1:   4]) = [ -7.24704E-03 0.00540  6.87246E-03 0.07318 ];
INF_SCATTP5               (idx, [1:   4]) = [ -2.97823E-05 1.00000  1.50090E-03 0.07608 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.35160E-03 0.01556  7.40031E-04 0.35301 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.12641E-04 0.12783  7.75748E-04 0.37571 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.70561E-01 0.00052  4.99325E-01 0.00302 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.23201E+00 0.00052  6.67580E-01 0.00302 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.78199E-03 0.00587  1.48003E-02 0.00267 ];
INF_REMXS                 (idx, [1:   4]) = [  6.78934E-03 0.00182  1.59850E-02 0.01437 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.93211E-01 0.00023  5.00949E-03 0.00225  1.26836E-03 0.02095  7.12468E-01 0.00241 ];
INF_S1                    (idx, [1:   8]) = [  1.58650E-01 0.00111  1.70624E-03 0.00152  3.32985E-04 0.03450  2.10962E-01 0.00195 ];
INF_S2                    (idx, [1:   8]) = [  6.46423E-02 0.00105 -5.34886E-04 0.00694  1.88670E-04 0.10888  8.12726E-02 0.00297 ];
INF_S3                    (idx, [1:   8]) = [  3.30200E-03 0.00169 -8.90808E-04 0.00586  1.20101E-04 0.13748  2.59073E-02 0.01251 ];
INF_S4                    (idx, [1:   8]) = [ -6.94316E-03 0.00628 -3.03779E-04 0.01496  8.92746E-05 0.11702  6.78318E-03 0.07567 ];
INF_S5                    (idx, [1:   8]) = [ -1.11981E-04 0.34030  8.22339E-05 0.02689  5.71887E-05 0.18904  1.44371E-03 0.07287 ];
INF_S6                    (idx, [1:   8]) = [  3.26791E-03 0.01601  8.36965E-05 0.01690  2.44745E-05 0.31584  7.15556E-04 0.37464 ];
INF_S7                    (idx, [1:   8]) = [  3.08762E-04 0.13758  4.05593E-06 0.91910  7.72665E-06 0.49354  7.68021E-04 0.37452 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.93214E-01 0.00023  5.00949E-03 0.00225  1.26836E-03 0.02095  7.12468E-01 0.00241 ];
INF_SP1                   (idx, [1:   8]) = [  1.58649E-01 0.00111  1.70624E-03 0.00152  3.32985E-04 0.03450  2.10962E-01 0.00195 ];
INF_SP2                   (idx, [1:   8]) = [  6.46427E-02 0.00105 -5.34886E-04 0.00694  1.88670E-04 0.10888  8.12726E-02 0.00297 ];
INF_SP3                   (idx, [1:   8]) = [  3.30215E-03 0.00175 -8.90808E-04 0.00586  1.20101E-04 0.13748  2.59073E-02 0.01251 ];
INF_SP4                   (idx, [1:   8]) = [ -6.94326E-03 0.00627 -3.03779E-04 0.01496  8.92746E-05 0.11702  6.78318E-03 0.07567 ];
INF_SP5                   (idx, [1:   8]) = [ -1.12016E-04 0.34008  8.22339E-05 0.02689  5.71887E-05 0.18904  1.44371E-03 0.07287 ];
INF_SP6                   (idx, [1:   8]) = [  3.26790E-03 0.01600  8.36965E-05 0.01690  2.44745E-05 0.31584  7.15556E-04 0.37464 ];
INF_SP7                   (idx, [1:   8]) = [  3.08585E-04 0.13778  4.05593E-06 0.91910  7.72665E-06 0.49354  7.68021E-04 0.37452 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.20346E-01 0.00082 -2.66633E-01 0.00841 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.63239E-01 0.00338 -1.44084E-01 0.00920 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.60213E-01 0.00470 -1.48768E-01 0.03215 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.67427E-01 0.00347  4.13347E-01 0.03306 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.51277E+00 0.00082 -1.25033E+00 0.00847 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.26630E+00 0.00337 -2.31386E+00 0.00927 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.28106E+00 0.00468 -2.24539E+00 0.03301 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.99096E+00 0.00346  8.08245E-01 0.03406 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32060E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67770E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32500 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75752E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '9' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.33114E+04 0.01635  6.27745E+04 0.01329  1.44759E+05 0.00748  2.48676E+05 0.00659  2.54604E+05 0.00611  2.31348E+05 0.00497  2.07942E+05 0.00504  1.79872E+05 0.00427  1.56910E+05 0.00070  1.37661E+05 0.00628  1.24547E+05 0.00634  1.14178E+05 0.00319  1.03625E+05 0.00725  9.81068E+04 0.00509  9.35777E+04 0.00495  7.89598E+04 0.00294  7.65552E+04 0.00522  7.24876E+04 0.00606  6.86159E+04 0.00209  1.26861E+05 0.00306  1.07758E+05 0.00754  6.83447E+04 0.01078  3.95661E+04 0.00300  3.98919E+04 0.00903  3.31475E+04 0.00538  2.54784E+04 0.01008  3.96458E+04 0.00479  8.52359E+03 0.01105  1.04168E+04 0.00513  9.70545E+03 0.02342  5.48801E+03 0.00224  9.68242E+03 0.01689  6.63415E+03 0.00695  5.28696E+03 0.01830  8.77129E+02 0.05230  7.63694E+02 0.05728  8.17664E+02 0.06365  9.77811E+02 0.03063  8.46362E+02 0.03334  9.18477E+02 0.03815  8.94888E+02 0.10596  7.92590E+02 0.01586  1.49103E+03 0.04405  2.45085E+03 0.03817  3.11815E+03 0.01398  7.95372E+03 0.02880  7.12385E+03 0.01841  6.23424E+03 0.00855  3.10206E+03 0.04333  1.96427E+03 0.03368  1.13931E+03 0.01996  1.17392E+03 0.04661  1.92835E+03 0.02621  2.15019E+03 0.03654  3.24264E+03 0.04995  3.64614E+03 0.03928  4.60192E+03 0.01259  2.57490E+03 0.02330  1.83610E+03 0.00791  1.38300E+03 0.04890  1.20203E+03 0.07469  1.22658E+03 0.03039  9.89272E+02 0.05165  5.99474E+02 0.00477  5.42521E+02 0.10595  4.32281E+02 0.04024  3.57230E+02 0.01354  2.31996E+02 0.10740  1.34554E+02 0.19654  3.48137E+01 0.36195 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.66203E+00 0.00390 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.02275E+00 0.00021  4.77859E-02 0.01085 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56027E-01 0.00019  4.80123E-01 0.00346 ];
INF_CAPT                  (idx, [1:   4]) = [  2.58967E-03 0.00115  2.50005E-02 0.00264 ];
INF_ABS                   (idx, [1:   4]) = [  6.73862E-03 0.00089  1.62533E-01 0.00227 ];
INF_FISS                  (idx, [1:   4]) = [  4.14895E-03 0.00089  1.37532E-01 0.00221 ];
INF_NSF                   (idx, [1:   4]) = [  1.01511E-02 0.00090  3.35047E-01 0.00221 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44667E+00 1.9E-05  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 7.2E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.25907E-08 0.00474  1.85369E-06 0.00401 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49305E-01 8.3E-05  3.17238E-01 0.00380 ];
INF_SCATT1                (idx, [1:   4]) = [  2.02424E-02 0.00853  1.89680E-02 0.01085 ];
INF_SCATT2                (idx, [1:   4]) = [  4.44483E-03 0.00813  1.09118E-03 0.52550 ];
INF_SCATT3                (idx, [1:   4]) = [  9.55758E-04 0.06261 -1.28403E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  2.29981E-04 0.46505  6.17320E-04 0.13803 ];
INF_SCATT5                (idx, [1:   4]) = [  1.34239E-04 0.06356 -3.84911E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  6.44963E-05 0.88623 -5.60176E-04 0.47449 ];
INF_SCATT7                (idx, [1:   4]) = [  3.35193E-05 1.00000  1.19620E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49308E-01 8.4E-05  3.17238E-01 0.00380 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.02431E-02 0.00855  1.89680E-02 0.01085 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.44514E-03 0.00816  1.09118E-03 0.52550 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.56307E-04 0.06259 -1.28403E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.30147E-04 0.46417  6.17320E-04 0.13803 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.34166E-04 0.06382 -3.84911E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  6.45652E-05 0.88700 -5.60176E-04 0.47449 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.35365E-05 1.00000  1.19620E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02317E-01 0.00070  4.39632E-01 0.00354 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64758E+00 0.00070  7.58229E-01 0.00354 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.73631E-03 0.00087  1.62533E-01 0.00227 ];
INF_REMXS                 (idx, [1:   4]) = [  7.30346E-03 0.00414  1.64208E-01 0.00413 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48724E-01 7.0E-05  5.81447E-04 0.01260  1.32370E-03 0.02148  3.15914E-01 0.00390 ];
INF_S1                    (idx, [1:   8]) = [  2.03701E-02 0.00838 -1.27740E-04 0.01935  2.84683E-04 0.05030  1.86833E-02 0.01162 ];
INF_S2                    (idx, [1:   8]) = [  4.46470E-03 0.00768 -1.98679E-05 0.15038 -4.54874E-05 0.86643  1.13667E-03 0.52606 ];
INF_S3                    (idx, [1:   8]) = [  9.62341E-04 0.06645 -6.58334E-06 0.72244 -2.42892E-05 1.00000 -1.04114E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  2.34215E-04 0.44897 -4.23388E-06 0.45022  3.77985E-06 1.00000  6.13540E-04 0.11788 ];
INF_S5                    (idx, [1:   8]) = [  1.35018E-04 0.07243 -7.78690E-07 1.00000 -3.99092E-07 1.00000 -3.84512E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  6.40460E-05 0.85778  4.50262E-07 1.00000 -4.30247E-05 0.28603 -5.17151E-04 0.53591 ];
INF_S7                    (idx, [1:   8]) = [  3.36202E-05 1.00000 -1.00836E-07 1.00000 -3.62418E-06 1.00000  1.23245E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48726E-01 7.2E-05  5.81447E-04 0.01260  1.32370E-03 0.02148  3.15914E-01 0.00390 ];
INF_SP1                   (idx, [1:   8]) = [  2.03709E-02 0.00839 -1.27740E-04 0.01935  2.84683E-04 0.05030  1.86833E-02 0.01162 ];
INF_SP2                   (idx, [1:   8]) = [  4.46501E-03 0.00772 -1.98679E-05 0.15038 -4.54874E-05 0.86643  1.13667E-03 0.52606 ];
INF_SP3                   (idx, [1:   8]) = [  9.62890E-04 0.06642 -6.58334E-06 0.72244 -2.42892E-05 1.00000 -1.04114E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  2.34381E-04 0.44813 -4.23388E-06 0.45022  3.77985E-06 1.00000  6.13540E-04 0.11788 ];
INF_SP5                   (idx, [1:   8]) = [  1.34944E-04 0.07289 -7.78690E-07 1.00000 -3.99092E-07 1.00000 -3.84512E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  6.41150E-05 0.85858  4.50262E-07 1.00000 -4.30247E-05 0.28603 -5.17151E-04 0.53591 ];
INF_SP7                   (idx, [1:   8]) = [  3.36373E-05 1.00000 -1.00836E-07 1.00000 -3.62418E-06 1.00000  1.23245E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.20053E-01 0.00397 -2.01311E-01 0.02788 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.23537E-01 0.00748 -1.10709E-01 0.04067 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.21961E-01 0.02000 -1.17098E-01 0.01704 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.01704E-01 0.00101  3.75997E-01 0.06671 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.93575E-01 0.00396 -1.65838E+00 0.02777 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.36766E-01 0.00745 -3.02089E+00 0.04076 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.39121E-01 0.01970 -2.84824E+00 0.01685 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.10484E+00 0.00101  8.93994E-01 0.06255 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.28261E-03 0.02425  2.24769E-04 0.19810  1.05036E-03 0.07882  6.79196E-04 0.07948  1.56673E-03 0.05140  2.37393E-03 0.04848  6.51046E-04 0.09395  6.05298E-04 0.10242  1.31284E-04 0.21399 ];
LAMBDA                    (idx, [1:  18]) = [  3.94210E-01 0.04791  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32061E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67770E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32500 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75752E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '8' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.83431E+04 0.00200  8.57233E+04 0.00712  1.95945E+05 0.00514  3.35506E+05 0.00389  3.41960E+05 0.00225  3.11976E+05 0.00196  2.78052E+05 0.00231  2.42597E+05 0.00350  2.11630E+05 0.00371  1.87917E+05 0.00722  1.67591E+05 0.00384  1.52875E+05 0.00316  1.39825E+05 0.00576  1.34003E+05 0.00639  1.26854E+05 0.00797  1.07509E+05 0.00725  1.05074E+05 0.00725  9.97932E+04 0.00624  9.55118E+04 0.00840  1.75534E+05 0.00525  1.50829E+05 0.00305  9.63673E+04 0.00340  5.64727E+04 0.00294  5.80205E+04 0.00284  4.90803E+04 0.01627  3.75445E+04 0.01273  6.00739E+04 0.00442  1.26081E+04 0.02184  1.60070E+04 0.00819  1.48193E+04 0.01845  8.20574E+03 0.01566  1.47190E+04 0.00744  9.62247E+03 0.01090  7.59528E+03 0.01444  1.26313E+03 0.02231  1.27881E+03 0.05354  1.23657E+03 0.02309  1.41634E+03 0.00871  1.38300E+03 0.06555  1.37172E+03 0.10775  1.31602E+03 0.03243  1.34990E+03 0.04382  2.43800E+03 0.04260  3.78309E+03 0.03019  4.94910E+03 0.01673  1.18010E+04 0.00800  1.11654E+04 0.00224  9.83403E+03 0.02163  5.34841E+03 0.01666  3.17464E+03 0.02867  2.14059E+03 0.05152  2.11372E+03 0.00292  3.17018E+03 0.03588  3.45680E+03 0.00883  5.33694E+03 0.01764  6.80266E+03 0.01686  8.75875E+03 0.01712  5.47547E+03 0.03556  4.03284E+03 0.01838  2.81304E+03 0.02908  2.65581E+03 0.00743  2.44403E+03 0.05326  2.10535E+03 0.05991  1.39083E+03 0.06231  1.20200E+03 0.02490  9.93385E+02 0.06651  8.22599E+02 0.02977  5.60674E+02 0.06284  3.21387E+02 0.05658  7.50278E+01 0.15750 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.68976E+00 0.00431 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.13417E+00 0.00176  8.60774E-02 0.00242 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56080E-01 0.00073  4.61427E-01 0.00166 ];
INF_CAPT                  (idx, [1:   4]) = [  2.43669E-03 0.00431  2.29785E-02 0.00090 ];
INF_ABS                   (idx, [1:   4]) = [  6.23052E-03 0.00294  1.49366E-01 0.00090 ];
INF_FISS                  (idx, [1:   4]) = [  3.79382E-03 0.00268  1.26388E-01 0.00093 ];
INF_NSF                   (idx, [1:   4]) = [  9.27984E-03 0.00268  3.07898E-01 0.00093 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44604E+00 1.5E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 2.7E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.50704E-08 0.00128  1.97346E-06 0.00305 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49900E-01 0.00083  3.12192E-01 0.00259 ];
INF_SCATT1                (idx, [1:   4]) = [  2.01970E-02 0.00363  1.85382E-02 0.00556 ];
INF_SCATT2                (idx, [1:   4]) = [  4.33034E-03 0.00664  1.52465E-03 0.50637 ];
INF_SCATT3                (idx, [1:   4]) = [  9.58246E-04 0.04884  2.03727E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  2.19961E-04 0.21487  3.55362E-04 0.72943 ];
INF_SCATT5                (idx, [1:   4]) = [  1.19289E-04 0.36582  2.73425E-04 0.79085 ];
INF_SCATT6                (idx, [1:   4]) = [  8.45074E-05 0.17307  2.08038E-04 0.58727 ];
INF_SCATT7                (idx, [1:   4]) = [  6.47583E-05 0.56851  3.59487E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49902E-01 0.00084  3.12192E-01 0.00259 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.01971E-02 0.00362  1.85382E-02 0.00556 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.32984E-03 0.00662  1.52465E-03 0.50637 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.58227E-04 0.04899  2.03727E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.20111E-04 0.21507  3.55362E-04 0.72943 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.19490E-04 0.36576  2.73425E-04 0.79085 ];
INF_SCATTP6               (idx, [1:   4]) = [  8.45687E-05 0.17158  2.08038E-04 0.58727 ];
INF_SCATTP7               (idx, [1:   4]) = [  6.47345E-05 0.56905  3.59487E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02501E-01 0.00068  4.24487E-01 0.00289 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64608E+00 0.00068  7.85274E-01 0.00288 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.22834E-03 0.00288  1.49366E-01 0.00090 ];
INF_REMXS                 (idx, [1:   4]) = [  6.81648E-03 0.00280  1.50449E-01 0.00235 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49264E-01 0.00081  6.35653E-04 0.01163  1.21435E-03 0.07910  3.10977E-01 0.00289 ];
INF_S1                    (idx, [1:   8]) = [  2.03385E-02 0.00339 -1.41424E-04 0.03273  2.56092E-04 0.10723  1.82821E-02 0.00598 ];
INF_S2                    (idx, [1:   8]) = [  4.34826E-03 0.00760 -1.79206E-05 0.34776  1.53401E-05 1.00000  1.50931E-03 0.52309 ];
INF_S3                    (idx, [1:   8]) = [  9.67121E-04 0.04680 -8.87598E-06 0.30900 -2.54538E-05 1.00000  2.29180E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  2.25447E-04 0.20362 -5.48644E-06 0.42355 -3.11988E-05 1.00000  3.86561E-04 0.67020 ];
INF_S5                    (idx, [1:   8]) = [  1.14386E-04 0.39032  4.90259E-06 0.37257 -2.95348E-05 0.54664  3.02960E-04 0.75478 ];
INF_S6                    (idx, [1:   8]) = [  8.67026E-05 0.16681 -2.19519E-06 1.00000 -1.44839E-05 1.00000  2.22522E-04 0.63662 ];
INF_S7                    (idx, [1:   8]) = [  6.63359E-05 0.53823 -1.57757E-06 1.00000 -1.86998E-06 1.00000  3.61357E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49266E-01 0.00081  6.35653E-04 0.01163  1.21435E-03 0.07910  3.10977E-01 0.00289 ];
INF_SP1                   (idx, [1:   8]) = [  2.03386E-02 0.00338 -1.41424E-04 0.03273  2.56092E-04 0.10723  1.82821E-02 0.00598 ];
INF_SP2                   (idx, [1:   8]) = [  4.34776E-03 0.00758 -1.79206E-05 0.34776  1.53401E-05 1.00000  1.50931E-03 0.52309 ];
INF_SP3                   (idx, [1:   8]) = [  9.67103E-04 0.04695 -8.87598E-06 0.30900 -2.54538E-05 1.00000  2.29180E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  2.25597E-04 0.20384 -5.48644E-06 0.42355 -3.11988E-05 1.00000  3.86561E-04 0.67020 ];
INF_SP5                   (idx, [1:   8]) = [  1.14587E-04 0.39019  4.90259E-06 0.37257 -2.95348E-05 0.54664  3.02960E-04 0.75478 ];
INF_SP6                   (idx, [1:   8]) = [  8.67639E-05 0.16537 -2.19519E-06 1.00000 -1.44839E-05 1.00000  2.22522E-04 0.63662 ];
INF_SP7                   (idx, [1:   8]) = [  6.63121E-05 0.53885 -1.57757E-06 1.00000 -1.86998E-06 1.00000  3.61357E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.28403E-01 0.00903 -1.89962E-01 0.03205 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.31626E-01 0.01114 -1.10175E-01 0.01947 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.34835E-01 0.00929 -1.04608E-01 0.01359 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.07518E-01 0.00886  3.58124E-01 0.07050 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.78211E-01 0.00901 -1.75830E+00 0.03168 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.27164E-01 0.01124 -3.02775E+00 0.01923 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.23351E-01 0.00921 -3.18768E+00 0.01372 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.08412E+00 0.00886  9.40531E-01 0.07367 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.12588E-03 0.02271  2.22247E-04 0.11558  1.00574E-03 0.06097  7.29128E-04 0.09121  1.42851E-03 0.05460  2.30045E-03 0.04501  7.44145E-04 0.08943  5.23539E-04 0.08665  1.72123E-04 0.17827 ];
LAMBDA                    (idx, [1:  18]) = [  4.07234E-01 0.04411  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32064E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67772E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32504 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75750E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '7' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.13729E+04 0.00712  5.26947E+04 0.00747  1.20434E+05 0.00832  2.06583E+05 0.00849  2.11807E+05 0.00414  1.91643E+05 0.00135  1.72886E+05 0.00817  1.48814E+05 0.00249  1.30826E+05 0.00375  1.15323E+05 0.00859  1.04192E+05 0.00760  9.52506E+04 0.00868  8.83342E+04 0.00838  8.29063E+04 0.00313  8.03125E+04 0.00197  6.85002E+04 0.00584  6.63034E+04 0.00884  6.27096E+04 0.01192  6.05467E+04 0.00388  1.11811E+05 0.00133  9.68373E+04 0.00819  6.36299E+04 0.00398  3.87009E+04 0.01357  3.94113E+04 0.00403  3.41942E+04 0.00493  2.66207E+04 0.01454  4.26296E+04 0.00412  8.70412E+03 0.01224  1.12933E+04 0.00565  1.01660E+04 0.02291  5.69123E+03 0.02141  1.01474E+04 0.00512  6.48095E+03 0.02398  5.34799E+03 0.01190  1.01471E+03 0.03692  8.96570E+02 0.01495  9.44811E+02 0.01004  9.96397E+02 0.04926  9.36990E+02 0.04849  9.85417E+02 0.03790  9.82828E+02 0.08790  1.01496E+03 0.02071  1.76292E+03 0.02925  2.66935E+03 0.02888  3.20669E+03 0.03168  8.52275E+03 0.01052  8.20698E+03 0.03402  7.11841E+03 0.00594  3.66388E+03 0.01043  2.35736E+03 0.02942  1.64493E+03 0.07649  1.57580E+03 0.02847  2.51435E+03 0.02449  2.75369E+03 0.01190  4.35116E+03 0.04249  5.48652E+03 0.02429  7.64201E+03 0.01411  4.86921E+03 0.01985  3.76379E+03 0.02794  2.68981E+03 0.03247  2.56633E+03 0.05309  2.44747E+03 0.03433  2.05893E+03 0.01920  1.46721E+03 0.03139  1.24552E+03 0.01064  1.14643E+03 0.02145  9.73900E+02 0.05384  6.64456E+02 0.02779  3.81496E+02 0.05819  1.08601E+02 0.00558 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.70899E+00 0.00405 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.60350E+00 0.00085  7.16005E-02 0.00546 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55456E-01 0.00097  4.55252E-01 0.00423 ];
INF_CAPT                  (idx, [1:   4]) = [  2.32807E-03 0.00478  2.19426E-02 0.00506 ];
INF_ABS                   (idx, [1:   4]) = [  5.84530E-03 0.00216  1.42800E-01 0.00534 ];
INF_FISS                  (idx, [1:   4]) = [  3.51723E-03 0.00079  1.20857E-01 0.00539 ];
INF_NSF                   (idx, [1:   4]) = [  8.60046E-03 0.00078  2.94425E-01 0.00539 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44524E+00 9.2E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 6.6E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.82258E-08 0.00366  2.12530E-06 0.00326 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49604E-01 0.00087  3.12702E-01 0.00354 ];
INF_SCATT1                (idx, [1:   4]) = [  1.99417E-02 0.00852  1.76556E-02 0.02238 ];
INF_SCATT2                (idx, [1:   4]) = [  4.21813E-03 0.01066  2.89076E-03 0.16790 ];
INF_SCATT3                (idx, [1:   4]) = [  8.77899E-04 0.03698  1.43210E-03 0.21345 ];
INF_SCATT4                (idx, [1:   4]) = [  1.27939E-04 0.25542  3.01865E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  9.46805E-05 0.18551 -2.86807E-04 0.68433 ];
INF_SCATT6                (idx, [1:   4]) = [  8.51416E-05 0.81655  1.02021E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  9.41146E-05 0.13547 -1.78023E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49606E-01 0.00087  3.12702E-01 0.00354 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.99421E-02 0.00852  1.76556E-02 0.02238 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.21794E-03 0.01067  2.89076E-03 0.16790 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.77977E-04 0.03722  1.43210E-03 0.21345 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.28124E-04 0.25588  3.01865E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.46998E-05 0.18430 -2.86807E-04 0.68433 ];
INF_SCATTP6               (idx, [1:   4]) = [  8.51996E-05 0.81602  1.02021E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  9.42172E-05 0.13520 -1.78023E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02681E-01 0.00239  4.18357E-01 0.00532 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64464E+00 0.00238  7.96813E-01 0.00532 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.84364E-03 0.00221  1.42800E-01 0.00534 ];
INF_REMXS                 (idx, [1:   4]) = [  6.55877E-03 0.00529  1.43504E-01 0.00861 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48898E-01 0.00086  7.06560E-04 0.00578  9.53799E-04 0.05364  3.11748E-01 0.00339 ];
INF_S1                    (idx, [1:   8]) = [  2.01001E-02 0.00862 -1.58396E-04 0.02764  2.22654E-04 0.23606  1.74329E-02 0.02005 ];
INF_S2                    (idx, [1:   8]) = [  4.24135E-03 0.01066 -2.32137E-05 0.06185  2.10235E-06 0.52878  2.88866E-03 0.16814 ];
INF_S3                    (idx, [1:   8]) = [  8.85379E-04 0.03964 -7.48024E-06 0.36443 -1.11157E-05 1.00000  1.44322E-03 0.22775 ];
INF_S4                    (idx, [1:   8]) = [  1.31770E-04 0.24318 -3.83113E-06 0.57701 -3.44400E-05 0.31720  6.46265E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  9.62549E-05 0.18077 -1.57445E-06 0.29315 -3.53089E-05 0.52554 -2.51498E-04 0.77431 ];
INF_S6                    (idx, [1:   8]) = [  8.22732E-05 0.82254  2.86848E-06 0.64500 -7.96849E-06 1.00000  1.09989E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  9.75832E-05 0.12157 -3.46856E-06 0.31037 -1.79642E-05 0.80221 -1.60059E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48899E-01 0.00086  7.06560E-04 0.00578  9.53799E-04 0.05364  3.11748E-01 0.00339 ];
INF_SP1                   (idx, [1:   8]) = [  2.01005E-02 0.00862 -1.58396E-04 0.02764  2.22654E-04 0.23606  1.74329E-02 0.02005 ];
INF_SP2                   (idx, [1:   8]) = [  4.24115E-03 0.01067 -2.32137E-05 0.06185  2.10235E-06 0.52878  2.88866E-03 0.16814 ];
INF_SP3                   (idx, [1:   8]) = [  8.85457E-04 0.03988 -7.48024E-06 0.36443 -1.11157E-05 1.00000  1.44322E-03 0.22775 ];
INF_SP4                   (idx, [1:   8]) = [  1.31955E-04 0.24372 -3.83113E-06 0.57701 -3.44400E-05 0.31720  6.46265E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  9.62742E-05 0.17958 -1.57445E-06 0.29315 -3.53089E-05 0.52554 -2.51498E-04 0.77431 ];
INF_SP6                   (idx, [1:   8]) = [  8.23311E-05 0.82199  2.86848E-06 0.64500 -7.96849E-06 1.00000  1.09989E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  9.76857E-05 0.12132 -3.46856E-06 0.31037 -1.79642E-05 0.80221 -1.60059E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.31902E-01 0.00673 -1.81383E-01 0.02694 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.25494E-01 0.01552 -1.03802E-01 0.00732 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.25679E-01 0.00613 -1.02591E-01 0.01927 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.18476E-01 0.00750  3.57835E-01 0.07197 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.71849E-01 0.00671 -1.84037E+00 0.02665 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.34631E-01 0.01556 -3.21159E+00 0.00737 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.34148E-01 0.00613 -3.25157E+00 0.01930 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.04677E+00 0.00752  9.42040E-01 0.07752 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.83029E-03 0.02680  2.12953E-04 0.13961  1.03391E-03 0.08449  7.00642E-04 0.09308  1.36369E-03 0.06597  2.21667E-03 0.05098  6.11514E-04 0.11095  5.02036E-04 0.10819  1.88875E-04 0.16874 ];
LAMBDA                    (idx, [1:  18]) = [  4.15408E-01 0.04954  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32064E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67773E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32503 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75748E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '6' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.99906E+03 0.00693  2.78831E+04 0.00810  6.48107E+04 0.00921  1.10499E+05 0.00168  1.14185E+05 0.00516  1.05170E+05 0.00113  9.25711E+04 0.00309  8.05159E+04 0.00412  7.01963E+04 0.00474  6.35316E+04 0.00460  5.72334E+04 0.00981  5.20038E+04 0.01337  4.79017E+04 0.00560  4.52541E+04 0.01199  4.37294E+04 0.00852  3.70248E+04 0.00457  3.65124E+04 0.01400  3.50125E+04 0.01496  3.34363E+04 0.00824  6.18108E+04 0.00775  5.46537E+04 0.00654  3.57855E+04 0.00873  2.09336E+04 0.01483  2.24964E+04 0.02243  1.95623E+04 0.01835  1.46300E+04 0.02279  2.38960E+04 0.00230  5.12307E+03 0.01076  6.17289E+03 0.00761  5.72101E+03 0.02678  3.22204E+03 0.01254  5.74903E+03 0.01902  3.69494E+03 0.02707  3.08516E+03 0.02639  5.27281E+02 0.07723  5.55807E+02 0.01519  5.45397E+02 0.05102  6.31256E+02 0.07916  5.76783E+02 0.02985  5.25740E+02 0.07783  6.17550E+02 0.03428  5.78133E+02 0.05662  9.75876E+02 0.04632  1.53335E+03 0.07752  2.02057E+03 0.06287  4.89970E+03 0.00641  4.55531E+03 0.02080  4.09559E+03 0.02041  2.43682E+03 0.02329  1.44198E+03 0.05252  9.23310E+02 0.03191  1.03193E+03 0.03461  1.55719E+03 0.04038  1.86383E+03 0.02811  2.71897E+03 0.02863  3.45723E+03 0.01290  4.82928E+03 0.00679  3.15590E+03 0.02679  2.55574E+03 0.00938  1.83934E+03 0.00864  1.70969E+03 0.01371  1.70805E+03 0.02329  1.48100E+03 0.04060  1.04833E+03 0.04918  9.61374E+02 0.04499  8.20493E+02 0.04706  8.10914E+02 0.09252  4.89992E+02 0.04985  2.99551E+02 0.06994  7.81473E+01 0.08682 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.72781E+00 0.00399 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.42206E+00 0.00021  4.58079E-02 0.00093 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55048E-01 0.00066  4.45545E-01 0.00545 ];
INF_CAPT                  (idx, [1:   4]) = [  2.14999E-03 0.00033  2.02008E-02 0.00597 ];
INF_ABS                   (idx, [1:   4]) = [  5.28626E-03 0.00207  1.31252E-01 0.00652 ];
INF_FISS                  (idx, [1:   4]) = [  3.13627E-03 0.00329  1.11052E-01 0.00662 ];
INF_NSF                   (idx, [1:   4]) = [  7.66770E-03 0.00327  2.70537E-01 0.00662 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44485E+00 1.6E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 1.1E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.95924E-08 0.00342  2.21724E-06 0.00431 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49748E-01 0.00064  3.15694E-01 0.00409 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98248E-02 0.00130  1.84717E-02 0.03232 ];
INF_SCATT2                (idx, [1:   4]) = [  4.26843E-03 0.01394  9.66901E-04 0.74478 ];
INF_SCATT3                (idx, [1:   4]) = [  8.10440E-04 0.05592  8.03671E-04 0.62504 ];
INF_SCATT4                (idx, [1:   4]) = [  2.03493E-04 0.23905  5.04187E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  5.02461E-05 0.45546  1.29288E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  9.05468E-05 0.34282  6.36342E-04 0.47472 ];
INF_SCATT7                (idx, [1:   4]) = [ -5.02102E-05 0.21640 -4.42254E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49749E-01 0.00064  3.15694E-01 0.00409 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98249E-02 0.00129  1.84717E-02 0.03232 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.26835E-03 0.01392  9.66901E-04 0.74478 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.10608E-04 0.05594  8.03671E-04 0.62504 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.03316E-04 0.23869  5.04187E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  5.02512E-05 0.45324  1.29288E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  9.05958E-05 0.34259  6.36342E-04 0.47472 ];
INF_SCATTP7               (idx, [1:   4]) = [ -5.05137E-05 0.21328 -4.42254E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02944E-01 0.00193  4.10458E-01 0.00793 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64250E+00 0.00193  8.12202E-01 0.00790 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.28463E-03 0.00223  1.31252E-01 0.00652 ];
INF_REMXS                 (idx, [1:   4]) = [  6.06006E-03 0.00222  1.30680E-01 0.01399 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48988E-01 0.00062  7.59258E-04 0.00977  8.29229E-04 0.02724  3.14865E-01 0.00404 ];
INF_S1                    (idx, [1:   8]) = [  1.99959E-02 0.00148 -1.71130E-04 0.04595  2.28778E-04 0.06175  1.82429E-02 0.03251 ];
INF_S2                    (idx, [1:   8]) = [  4.28238E-03 0.01507 -1.39542E-05 0.36253  1.05258E-04 0.32571  8.61643E-04 0.79754 ];
INF_S3                    (idx, [1:   8]) = [  8.27844E-04 0.05720 -1.74044E-05 0.13697  2.42945E-05 0.36744  7.79377E-04 0.65453 ];
INF_S4                    (idx, [1:   8]) = [  2.08616E-04 0.24534 -5.12264E-06 0.57756 -8.27036E-06 1.00000  5.12457E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  4.84110E-05 0.47319  1.83511E-06 1.00000  1.38555E-05 0.47390  1.15433E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  8.33873E-05 0.31504  7.15952E-06 0.73117 -8.00760E-06 1.00000  6.44349E-04 0.43547 ];
INF_S7                    (idx, [1:   8]) = [ -4.60693E-05 0.13741 -4.14090E-06 1.00000  1.00070E-05 1.00000 -4.52261E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48990E-01 0.00062  7.59258E-04 0.00977  8.29229E-04 0.02724  3.14865E-01 0.00404 ];
INF_SP1                   (idx, [1:   8]) = [  1.99960E-02 0.00147 -1.71130E-04 0.04595  2.28778E-04 0.06175  1.82429E-02 0.03251 ];
INF_SP2                   (idx, [1:   8]) = [  4.28230E-03 0.01505 -1.39542E-05 0.36253  1.05258E-04 0.32571  8.61643E-04 0.79754 ];
INF_SP3                   (idx, [1:   8]) = [  8.28012E-04 0.05721 -1.74044E-05 0.13697  2.42945E-05 0.36744  7.79377E-04 0.65453 ];
INF_SP4                   (idx, [1:   8]) = [  2.08438E-04 0.24499 -5.12264E-06 0.57756 -8.27036E-06 1.00000  5.12457E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  4.84161E-05 0.47068  1.83511E-06 1.00000  1.38555E-05 0.47390  1.15433E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  8.34363E-05 0.31485  7.15952E-06 0.73117 -8.00760E-06 1.00000  6.44349E-04 0.43547 ];
INF_SP7                   (idx, [1:   8]) = [ -4.63728E-05 0.13500 -4.14090E-06 1.00000  1.00070E-05 1.00000 -4.52261E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.17413E-01 0.00595 -1.87437E-01 0.01759 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.97979E-01 0.00861 -1.07717E-01 0.01400 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.97698E-01 0.01280 -1.09187E-01 0.01635 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.15525E-01 0.00111  4.17622E-01 0.09018 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.98627E-01 0.00593 -1.77948E+00 0.01765 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.69471E-01 0.00865 -3.09576E+00 0.01418 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.69967E-01 0.01267 -3.05447E+00 0.01610 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.05644E+00 0.00111  8.11802E-01 0.09344 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.27628E-03 0.04262  2.12268E-04 0.18883  1.07614E-03 0.11226  8.07112E-04 0.14586  1.55572E-03 0.08496  2.12924E-03 0.07616  8.73700E-04 0.13300  4.97861E-04 0.15804  1.24247E-04 0.24833 ];
LAMBDA                    (idx, [1:  18]) = [  3.80002E-01 0.05210  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32064E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67775E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32502 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75746E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '5' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  6.01969E+03 0.04462  2.74488E+04 0.00499  6.26739E+04 0.00929  1.06267E+05 0.00185  1.09829E+05 0.00459  1.00908E+05 0.00685  8.97987E+04 0.00742  7.81404E+04 0.01038  6.84328E+04 0.00748  6.11621E+04 0.00415  5.55720E+04 0.00357  5.13420E+04 0.00772  4.68312E+04 0.00829  4.54109E+04 0.00224  4.38102E+04 0.00419  3.68609E+04 0.01131  3.62602E+04 0.00840  3.46221E+04 0.00474  3.33575E+04 0.00766  6.19825E+04 0.00282  5.42278E+04 0.00239  3.59796E+04 0.00874  2.17443E+04 0.00804  2.31204E+04 0.00876  2.02758E+04 0.01599  1.59335E+04 0.01379  2.53604E+04 0.01747  5.54251E+03 0.02607  6.52245E+03 0.01167  6.29275E+03 0.02607  3.43799E+03 0.03986  5.82099E+03 0.03086  3.87580E+03 0.01528  3.19788E+03 0.00785  5.67298E+02 0.04662  6.17572E+02 0.03156  5.64449E+02 0.01161  6.63492E+02 0.02754  5.59118E+02 0.09035  5.79390E+02 0.05577  5.35110E+02 0.05173  5.27584E+02 0.02733  1.01071E+03 0.05487  1.66502E+03 0.04965  1.99543E+03 0.02627  5.33659E+03 0.00768  4.98669E+03 0.02990  4.49169E+03 0.02673  2.44070E+03 0.06096  1.62998E+03 0.05533  1.09514E+03 0.01955  1.10968E+03 0.05679  1.93007E+03 0.02113  1.90733E+03 0.05639  3.11484E+03 0.02265  4.08051E+03 0.01557  5.77438E+03 0.01312  4.15037E+03 0.01564  3.31037E+03 0.02526  2.49171E+03 0.01471  2.28803E+03 0.02572  2.33567E+03 0.01839  2.11522E+03 0.00762  1.45688E+03 0.02322  1.37974E+03 0.06067  1.23289E+03 0.04530  1.00936E+03 0.06654  7.12617E+02 0.07771  4.88393E+02 0.05189  1.20426E+02 0.05749 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.74607E+00 0.00630 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.40078E+00 0.00045  5.55769E-02 0.00439 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55342E-01 0.00156  4.29620E-01 0.00237 ];
INF_CAPT                  (idx, [1:   4]) = [  2.00298E-03 0.00717  1.84875E-02 0.00187 ];
INF_ABS                   (idx, [1:   4]) = [  4.84442E-03 0.00382  1.19816E-01 0.00174 ];
INF_FISS                  (idx, [1:   4]) = [  2.84144E-03 0.00169  1.01329E-01 0.00181 ];
INF_NSF                   (idx, [1:   4]) = [  6.94545E-03 0.00167  2.46850E-01 0.00181 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44434E+00 2.2E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 1.3E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.18473E-08 0.00346  2.33780E-06 0.00609 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50495E-01 0.00157  3.10081E-01 0.00289 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98675E-02 0.00155  1.84013E-02 0.07176 ];
INF_SCATT2                (idx, [1:   4]) = [  4.34068E-03 0.04708  1.50741E-03 0.34044 ];
INF_SCATT3                (idx, [1:   4]) = [  8.61541E-04 0.14521  9.09379E-04 0.33175 ];
INF_SCATT4                (idx, [1:   4]) = [  1.40010E-04 0.28596  1.32009E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.25510E-04 0.21486  6.55018E-04 0.22128 ];
INF_SCATT6                (idx, [1:   4]) = [  2.41298E-05 1.00000  7.88395E-04 0.47991 ];
INF_SCATT7                (idx, [1:   4]) = [ -8.79440E-05 0.74590  9.67876E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50497E-01 0.00157  3.10081E-01 0.00289 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98676E-02 0.00157  1.84013E-02 0.07176 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.34045E-03 0.04704  1.50741E-03 0.34044 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.61047E-04 0.14543  9.09379E-04 0.33175 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.40238E-04 0.28394  1.32009E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.25472E-04 0.21367  6.55018E-04 0.22128 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.42284E-05 1.00000  7.88395E-04 0.47991 ];
INF_SCATTP7               (idx, [1:   4]) = [ -8.79426E-05 0.74549  9.67876E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03541E-01 0.00089  3.95707E-01 0.00102 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63767E+00 0.00089  8.42376E-01 0.00102 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.84276E-03 0.00391  1.19816E-01 0.00174 ];
INF_REMXS                 (idx, [1:   4]) = [  5.63097E-03 0.00027  1.20340E-01 0.00722 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49711E-01 0.00159  7.84489E-04 0.02040  8.01610E-04 0.07107  3.09280E-01 0.00273 ];
INF_S1                    (idx, [1:   8]) = [  2.00583E-02 0.00152 -1.90781E-04 0.01148  2.67788E-04 0.17739  1.81335E-02 0.07541 ];
INF_S2                    (idx, [1:   8]) = [  4.36492E-03 0.04685 -2.42466E-05 0.23050  7.78218E-05 0.39298  1.42959E-03 0.37950 ];
INF_S3                    (idx, [1:   8]) = [  8.68024E-04 0.14309 -6.48274E-06 0.68345  1.73086E-06 1.00000  9.07648E-04 0.35634 ];
INF_S4                    (idx, [1:   8]) = [  1.46383E-04 0.28831 -6.37300E-06 0.88546 -1.55379E-05 0.98145  1.47547E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.25124E-04 0.20825  3.85418E-07 1.00000 -1.13659E-06 1.00000  6.56154E-04 0.22963 ];
INF_S6                    (idx, [1:   8]) = [  2.45106E-05 1.00000 -3.80831E-07 1.00000 -2.31887E-05 1.00000  8.11583E-04 0.46434 ];
INF_S7                    (idx, [1:   8]) = [ -9.17861E-05 0.70387  3.84212E-06 0.30487 -2.10479E-05 0.34778  3.07267E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49712E-01 0.00159  7.84489E-04 0.02040  8.01610E-04 0.07107  3.09280E-01 0.00273 ];
INF_SP1                   (idx, [1:   8]) = [  2.00584E-02 0.00154 -1.90781E-04 0.01148  2.67788E-04 0.17739  1.81335E-02 0.07541 ];
INF_SP2                   (idx, [1:   8]) = [  4.36469E-03 0.04681 -2.42466E-05 0.23050  7.78218E-05 0.39298  1.42959E-03 0.37950 ];
INF_SP3                   (idx, [1:   8]) = [  8.67530E-04 0.14330 -6.48274E-06 0.68345  1.73086E-06 1.00000  9.07648E-04 0.35634 ];
INF_SP4                   (idx, [1:   8]) = [  1.46611E-04 0.28622 -6.37300E-06 0.88546 -1.55379E-05 0.98145  1.47547E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.25086E-04 0.20693  3.85418E-07 1.00000 -1.13659E-06 1.00000  6.56154E-04 0.22963 ];
INF_SP6                   (idx, [1:   8]) = [  2.46092E-05 1.00000 -3.80831E-07 1.00000 -2.31887E-05 1.00000  8.11583E-04 0.46434 ];
INF_SP7                   (idx, [1:   8]) = [ -9.17847E-05 0.70349  3.84212E-06 0.30487 -2.10479E-05 0.34778  3.07267E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.29448E-01 0.01096 -1.85585E-01 0.02977 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.01515E-01 0.00644 -1.07354E-01 0.01489 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.01870E-01 0.03394 -1.07150E-01 0.00758 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.33780E-01 0.00509  4.19495E-01 0.11799 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.76376E-01 0.01095 -1.79924E+00 0.02907 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.64708E-01 0.00640 -3.10637E+00 0.01498 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.65707E-01 0.03376 -3.11125E+00 0.00763 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.98713E-01 0.00509  8.19908E-01 0.13106 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.14888E-03 0.04596  2.04462E-04 0.17992  1.06826E-03 0.12623  8.18953E-04 0.14187  1.48309E-03 0.10967  2.16711E-03 0.07768  6.15398E-04 0.12087  6.85681E-04 0.15714  1.05929E-04 0.25674 ];
LAMBDA                    (idx, [1:  18]) = [  3.95995E-01 0.06460  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32064E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67775E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32502 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75746E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '4' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.06768E+04 0.02179  5.18547E+04 0.01057  1.18559E+05 0.00377  2.05726E+05 0.00259  2.13722E+05 0.00150  1.93650E+05 0.00173  1.73381E+05 0.00436  1.51520E+05 0.00114  1.32749E+05 0.00057  1.19832E+05 0.00294  1.08833E+05 0.00605  1.00629E+05 0.00563  9.28612E+04 0.00544  8.91656E+04 0.00180  8.52725E+04 0.00249  7.31252E+04 0.00775  7.22452E+04 0.00139  6.92345E+04 0.01130  6.50197E+04 0.00455  1.22185E+05 0.00337  1.10067E+05 0.00565  7.27566E+04 0.00315  4.49962E+04 0.00048  4.79785E+04 0.00674  4.17085E+04 0.00846  3.30357E+04 0.01194  5.36897E+04 0.00336  1.11530E+04 0.00681  1.37378E+04 0.00860  1.23713E+04 0.00902  6.96777E+03 0.01962  1.18786E+04 0.01968  8.12415E+03 0.02393  6.43925E+03 0.01036  1.26377E+03 0.03720  1.25016E+03 0.03687  1.19044E+03 0.11332  1.25616E+03 0.07282  1.29161E+03 0.01129  1.12496E+03 0.02876  1.21891E+03 0.03881  1.10626E+03 0.03721  2.18852E+03 0.02088  3.43366E+03 0.01811  4.04981E+03 0.01502  1.07107E+04 0.00652  1.03310E+04 0.00348  9.70704E+03 0.00414  5.22710E+03 0.02723  3.28803E+03 0.02696  2.32314E+03 0.04451  2.49109E+03 0.04737  3.82238E+03 0.01613  4.21787E+03 0.03252  7.02377E+03 0.01269  9.14644E+03 0.01459  1.37278E+04 0.01683  9.67738E+03 0.01401  7.42197E+03 0.01183  5.88816E+03 0.00439  5.36818E+03 0.02154  5.69710E+03 0.02651  4.97334E+03 0.01947  3.30608E+03 0.03025  3.42029E+03 0.01566  3.01721E+03 0.03645  2.57024E+03 0.00941  1.85551E+03 0.01105  1.21663E+03 0.01003  2.73774E+02 0.06156 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.76063E+00 0.00178 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.75150E+00 0.00168  1.25820E-01 0.00446 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55595E-01 0.00042  4.16939E-01 0.00478 ];
INF_CAPT                  (idx, [1:   4]) = [  1.87900E-03 0.00652  1.66805E-02 0.00457 ];
INF_ABS                   (idx, [1:   4]) = [  4.45504E-03 0.00379  1.07777E-01 0.00396 ];
INF_FISS                  (idx, [1:   4]) = [  2.57604E-03 0.00222  9.10968E-02 0.00384 ];
INF_NSF                   (idx, [1:   4]) = [  6.29533E-03 0.00221  2.21924E-01 0.00384 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44380E+00 1.3E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 8.7E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.36360E-08 0.00372  2.40293E-06 0.00097 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.51160E-01 0.00040  3.09666E-01 0.00555 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97016E-02 0.00540  1.79646E-02 0.02891 ];
INF_SCATT2                (idx, [1:   4]) = [  3.81426E-03 0.01727  1.64782E-03 0.41691 ];
INF_SCATT3                (idx, [1:   4]) = [  6.88096E-04 0.10043  6.39512E-04 0.12914 ];
INF_SCATT4                (idx, [1:   4]) = [  5.60810E-05 0.36588 -8.27781E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  6.03314E-05 0.84600 -1.09809E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  4.58855E-05 1.00000 -5.54653E-04 0.38237 ];
INF_SCATT7                (idx, [1:   4]) = [  2.08866E-05 1.00000  9.08509E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.51161E-01 0.00040  3.09666E-01 0.00555 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97014E-02 0.00539  1.79646E-02 0.02891 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.81446E-03 0.01725  1.64782E-03 0.41691 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.88045E-04 0.10046  6.39512E-04 0.12914 ];
INF_SCATTP4               (idx, [1:   4]) = [  5.60220E-05 0.36600 -8.27781E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  6.02844E-05 0.84717 -1.09809E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.58551E-05 1.00000 -5.54653E-04 0.38237 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.07660E-05 1.00000  9.08509E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04368E-01 0.00041  3.86119E-01 0.00493 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63105E+00 0.00041  8.63333E-01 0.00492 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.45420E-03 0.00386  1.07777E-01 0.00396 ];
INF_REMXS                 (idx, [1:   4]) = [  5.30642E-03 0.00386  1.07940E-01 0.00343 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50289E-01 0.00041  8.71389E-04 0.00365  6.66974E-04 0.05482  3.08999E-01 0.00566 ];
INF_S1                    (idx, [1:   8]) = [  1.98996E-02 0.00505 -1.98027E-04 0.03020  1.60239E-04 0.24364  1.78044E-02 0.02897 ];
INF_S2                    (idx, [1:   8]) = [  3.84360E-03 0.01590 -2.93463E-05 0.17683 -6.14948E-06 1.00000  1.65397E-03 0.40771 ];
INF_S3                    (idx, [1:   8]) = [  6.94982E-04 0.09566 -6.88631E-06 0.38973  1.49706E-05 0.86804  6.24541E-04 0.14467 ];
INF_S4                    (idx, [1:   8]) = [  6.36364E-05 0.32119 -7.55543E-06 0.06382 -1.17313E-07 1.00000 -8.26608E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  6.05393E-05 0.84041 -2.07919E-07 1.00000  6.33489E-06 1.00000 -1.16144E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  4.53555E-05 1.00000  5.30012E-07 1.00000 -2.63654E-05 0.28395 -5.28287E-04 0.39138 ];
INF_S7                    (idx, [1:   8]) = [  2.01727E-05 1.00000  7.13883E-07 1.00000 -3.54450E-05 0.10595  1.26296E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50289E-01 0.00041  8.71389E-04 0.00365  6.66974E-04 0.05482  3.08999E-01 0.00566 ];
INF_SP1                   (idx, [1:   8]) = [  1.98994E-02 0.00504 -1.98027E-04 0.03020  1.60239E-04 0.24364  1.78044E-02 0.02897 ];
INF_SP2                   (idx, [1:   8]) = [  3.84380E-03 0.01588 -2.93463E-05 0.17683 -6.14948E-06 1.00000  1.65397E-03 0.40771 ];
INF_SP3                   (idx, [1:   8]) = [  6.94932E-04 0.09570 -6.88631E-06 0.38973  1.49706E-05 0.86804  6.24541E-04 0.14467 ];
INF_SP4                   (idx, [1:   8]) = [  6.35774E-05 0.32133 -7.55543E-06 0.06382 -1.17313E-07 1.00000 -8.26608E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  6.04923E-05 0.84149 -2.07919E-07 1.00000  6.33489E-06 1.00000 -1.16144E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  4.53251E-05 1.00000  5.30012E-07 1.00000 -2.63654E-05 0.28395 -5.28287E-04 0.39138 ];
INF_SP7                   (idx, [1:   8]) = [  2.00521E-05 1.00000  7.13883E-07 1.00000 -3.54450E-05 0.10595  1.26296E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.33118E-01 0.00809 -2.03226E-01 0.01596 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.15352E-01 0.01901 -1.17559E-01 0.01888 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.07110E-01 0.00498 -1.13813E-01 0.01689 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.31871E-01 0.00739  3.95016E-01 0.02375 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.69714E-01 0.00809 -1.64104E+00 0.01586 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.47272E-01 0.01889 -2.83745E+00 0.01856 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.57353E-01 0.00501 -2.93048E+00 0.01714 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.00452E+00 0.00744  8.44823E-01 0.02428 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.13894E-03 0.03161  2.55931E-04 0.19637  1.04329E-03 0.07821  7.80085E-04 0.09780  1.45797E-03 0.06632  2.14623E-03 0.05499  7.11761E-04 0.08547  5.78381E-04 0.11114  1.65293E-04 0.21793 ];
LAMBDA                    (idx, [1:  18]) = [  4.12797E-01 0.06505  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32065E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67777E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32501 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75744E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '3' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.95298E+03 0.02033  9.45171E+03 0.04382  2.26760E+04 0.02690  3.84227E+04 0.01248  3.97686E+04 0.01117  3.71542E+04 0.01191  3.31871E+04 0.00942  2.88422E+04 0.01194  2.54891E+04 0.00716  2.31298E+04 0.00868  2.12319E+04 0.01206  2.04347E+04 0.01290  1.79576E+04 0.00558  1.74601E+04 0.00250  1.68889E+04 0.01100  1.43377E+04 0.01133  1.40671E+04 0.01627  1.35643E+04 0.00928  1.32630E+04 0.02028  2.45029E+04 0.00195  2.16488E+04 0.01633  1.46646E+04 0.01267  8.99139E+03 0.01643  9.80454E+03 0.01921  8.73764E+03 0.01333  6.82740E+03 0.02880  1.10438E+04 0.01143  2.34710E+03 0.01493  2.81702E+03 0.00605  2.58733E+03 0.03116  1.46579E+03 0.01328  2.46647E+03 0.02029  1.58176E+03 0.02529  1.40220E+03 0.03209  2.65947E+02 0.09571  2.68217E+02 0.12312  2.02919E+02 0.11232  2.61361E+02 0.05642  2.93097E+02 0.02701  2.51414E+02 0.08042  2.53937E+02 0.10831  2.42123E+02 0.12919  4.07121E+02 0.06489  7.77056E+02 0.03575  9.07205E+02 0.10679  2.26648E+03 0.02876  2.20577E+03 0.01354  2.22311E+03 0.00574  1.18764E+03 0.04843  7.99642E+02 0.01772  5.51474E+02 0.09504  5.20827E+02 0.01580  9.62662E+02 0.09145  1.03622E+03 0.02527  1.65133E+03 0.01742  2.07838E+03 0.05045  3.14983E+03 0.02421  2.48772E+03 0.02220  1.85587E+03 0.04625  1.39912E+03 0.05068  1.41818E+03 0.02096  1.48692E+03 0.02520  1.35456E+03 0.03699  9.47519E+02 0.06809  9.71712E+02 0.03847  7.82496E+02 0.05740  7.93889E+02 0.00834  5.83929E+02 0.05059  3.63675E+02 0.08469  1.04751E+02 0.01808 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.80084E+00 0.01108 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  5.35841E-01 0.00371  3.08747E-02 0.01481 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.57236E-01 0.00095  4.10857E-01 0.00649 ];
INF_CAPT                  (idx, [1:   4]) = [  1.79193E-03 0.00725  1.56380E-02 0.00857 ];
INF_ABS                   (idx, [1:   4]) = [  4.13760E-03 0.00576  1.00471E-01 0.00845 ];
INF_FISS                  (idx, [1:   4]) = [  2.34567E-03 0.00464  8.48327E-02 0.00842 ];
INF_NSF                   (idx, [1:   4]) = [  5.73089E-03 0.00460  2.06664E-01 0.00842 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44318E+00 4.0E-05  2.43614E+00 1.6E-08 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 1.7E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.62155E-08 0.00739  2.53158E-06 0.00431 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.53155E-01 0.00102  3.11642E-01 0.00170 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98805E-02 0.00628  1.74232E-02 0.06612 ];
INF_SCATT2                (idx, [1:   4]) = [  3.75823E-03 0.06141  2.28085E-03 0.53896 ];
INF_SCATT3                (idx, [1:   4]) = [  2.88646E-04 0.62364  1.72700E-03 0.54944 ];
INF_SCATT4                (idx, [1:   4]) = [ -9.47681E-05 0.51432 -3.00349E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [ -4.85409E-05 1.00000  1.56283E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  2.69795E-05 1.00000  3.65046E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.48905E-05 1.00000  9.76616E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.53155E-01 0.00102  3.11642E-01 0.00170 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98805E-02 0.00628  1.74232E-02 0.06612 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.75823E-03 0.06141  2.28085E-03 0.53896 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.88646E-04 0.62364  1.72700E-03 0.54944 ];
INF_SCATTP4               (idx, [1:   4]) = [ -9.47681E-05 0.51432 -3.00349E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [ -4.85409E-05 1.00000  1.56283E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.69795E-05 1.00000  3.65046E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.48905E-05 1.00000  9.76616E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.06295E-01 0.00260  3.79445E-01 0.00561 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.61583E+00 0.00260  8.78531E-01 0.00563 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.13760E-03 0.00576  1.00471E-01 0.00845 ];
INF_REMXS                 (idx, [1:   4]) = [  4.97995E-03 0.00320  9.95603E-02 0.02337 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.52256E-01 0.00097  8.98722E-04 0.01849  3.45436E-04 0.29637  3.11297E-01 0.00171 ];
INF_S1                    (idx, [1:   8]) = [  2.00941E-02 0.00614 -2.13580E-04 0.02632  9.27772E-05 0.38737  1.73304E-02 0.06719 ];
INF_S2                    (idx, [1:   8]) = [  3.77595E-03 0.06206 -1.77147E-05 0.20772  8.47122E-06 1.00000  2.27238E-03 0.54219 ];
INF_S3                    (idx, [1:   8]) = [  2.93916E-04 0.60861 -5.27081E-06 1.00000 -3.37118E-06 1.00000  1.73037E-03 0.55047 ];
INF_S4                    (idx, [1:   8]) = [ -7.99897E-05 0.68687 -1.47784E-05 0.44367 -3.20485E-05 0.65747 -2.68300E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [ -6.41466E-05 1.00000  1.56057E-05 0.14491 -3.06228E-05 0.63092  1.86906E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  3.38190E-05 1.00000 -6.83946E-06 0.47324 -2.42639E-05 0.43324  3.89310E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.76337E-05 1.00000 -2.74322E-06 1.00000  1.22201E-05 1.00000 -2.45396E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.52256E-01 0.00097  8.98722E-04 0.01849  3.45436E-04 0.29637  3.11297E-01 0.00171 ];
INF_SP1                   (idx, [1:   8]) = [  2.00941E-02 0.00614 -2.13580E-04 0.02632  9.27772E-05 0.38737  1.73304E-02 0.06719 ];
INF_SP2                   (idx, [1:   8]) = [  3.77595E-03 0.06206 -1.77147E-05 0.20772  8.47122E-06 1.00000  2.27238E-03 0.54219 ];
INF_SP3                   (idx, [1:   8]) = [  2.93916E-04 0.60861 -5.27081E-06 1.00000 -3.37118E-06 1.00000  1.73037E-03 0.55047 ];
INF_SP4                   (idx, [1:   8]) = [ -7.99897E-05 0.68687 -1.47784E-05 0.44367 -3.20485E-05 0.65747 -2.68300E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [ -6.41466E-05 1.00000  1.56057E-05 0.14491 -3.06228E-05 0.63092  1.86906E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  3.38190E-05 1.00000 -6.83946E-06 0.47324 -2.42639E-05 0.43324  3.89310E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.76337E-05 1.00000 -2.74322E-06 1.00000  1.22201E-05 1.00000 -2.45396E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.14944E-01 0.02540 -2.23828E-01 0.05042 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.63462E-01 0.06217 -1.32710E-01 0.01326 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.74260E-01 0.02737 -1.30098E-01 0.05846 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.38620E-01 0.00988  6.37687E-01 0.23210 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  8.04334E-01 0.02480 -1.49702E+00 0.05160 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.24526E-01 0.05887 -2.51263E+00 0.01319 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.03897E-01 0.02722 -2.58034E+00 0.06028 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.84580E-01 0.00989  6.01919E-01 0.28658 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.38062E-03 0.06951  2.44251E-04 0.32181  1.10010E-03 0.14842  8.12823E-04 0.20920  1.14364E-03 0.13455  1.66554E-03 0.10008  8.82425E-04 0.24346  4.13800E-04 0.28277  1.18039E-04 0.49218 ];
LAMBDA                    (idx, [1:  18]) = [  3.90011E-01 0.12183  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 2.7E-09  1.63478E+00 0.0E+00  3.55460E+00 7.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32065E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67778E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32501 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75742E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '2' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.49935E+04 0.00947  7.14432E+04 0.01335  1.64988E+05 0.00885  2.90412E+05 0.00365  3.00003E+05 0.00158  2.76845E+05 0.00505  2.50370E+05 0.00342  2.20268E+05 0.00331  1.94860E+05 0.00276  1.77920E+05 0.00125  1.64184E+05 0.00125  1.52777E+05 0.00284  1.43733E+05 0.00098  1.36939E+05 0.00575  1.31612E+05 0.00138  1.12414E+05 0.01055  1.09790E+05 0.00241  1.06730E+05 0.00315  1.02349E+05 0.00414  1.92669E+05 0.00445  1.73027E+05 0.00198  1.18202E+05 0.00434  7.18167E+04 0.00955  7.86502E+04 0.00598  7.10937E+04 0.00427  5.72193E+04 0.01262  9.30399E+04 0.00349  1.91708E+04 0.01319  2.35542E+04 0.00792  2.10717E+04 0.01035  1.18886E+04 0.00891  1.99991E+04 0.01834  1.36249E+04 0.01311  1.14120E+04 0.00175  2.16136E+03 0.01114  2.09930E+03 0.02888  2.21607E+03 0.03657  2.24276E+03 0.01619  2.12171E+03 0.04227  2.07616E+03 0.03600  2.20692E+03 0.02599  2.18267E+03 0.05825  3.53856E+03 0.03763  5.81940E+03 0.02457  7.45425E+03 0.01697  1.82931E+04 0.01644  1.84814E+04 0.00585  1.76407E+04 0.00539  1.00068E+04 0.02428  6.27371E+03 0.02863  4.41621E+03 0.02621  4.47159E+03 0.02716  7.35030E+03 0.02477  8.49904E+03 0.02372  1.39178E+04 0.00697  1.88929E+04 0.01154  2.98601E+04 0.00213  2.19633E+04 0.01563  1.75881E+04 0.00145  1.43528E+04 0.02134  1.40283E+04 0.01708  1.48273E+04 0.00440  1.35783E+04 0.01319  9.32104E+03 0.02340  9.28760E+03 0.01448  8.49157E+03 0.01353  7.58724E+03 0.00966  6.02657E+03 0.01087  3.81496E+03 0.02865  1.16369E+03 0.01625 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.78980E+00 0.00184 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.14586E+00 0.00074  2.81458E-01 0.00344 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56541E-01 0.00052  3.99406E-01 0.00091 ];
INF_CAPT                  (idx, [1:   4]) = [  1.69135E-03 0.00158  1.42460E-02 0.00131 ];
INF_ABS                   (idx, [1:   4]) = [  3.82239E-03 0.00084  9.10769E-02 0.00103 ];
INF_FISS                  (idx, [1:   4]) = [  2.13104E-03 0.00246  7.68310E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  5.20541E-03 0.00245  1.87171E-01 0.00097 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44266E+00 1.8E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 8.4E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.88143E-08 0.00478  2.63845E-06 0.00061 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.52714E-01 0.00045  3.08346E-01 0.00091 ];
INF_SCATT1                (idx, [1:   4]) = [  1.96256E-02 0.00999  1.77131E-02 0.00915 ];
INF_SCATT2                (idx, [1:   4]) = [  3.71866E-03 0.00549  2.35682E-03 0.07702 ];
INF_SCATT3                (idx, [1:   4]) = [  6.95952E-04 0.07484  1.19585E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.15628E-04 0.28311 -1.58713E-04 0.53835 ];
INF_SCATT5                (idx, [1:   4]) = [  1.13571E-05 1.00000 -1.91498E-04 0.25937 ];
INF_SCATT6                (idx, [1:   4]) = [  8.89605E-05 0.28023  5.06361E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.67797E-05 1.00000 -2.03827E-04 0.68685 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.52715E-01 0.00045  3.08346E-01 0.00091 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.96255E-02 0.01000  1.77131E-02 0.00915 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.71856E-03 0.00544  2.35682E-03 0.07702 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.96055E-04 0.07476  1.19585E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.15773E-04 0.28211 -1.58713E-04 0.53835 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.13919E-05 1.00000 -1.91498E-04 0.25937 ];
INF_SCATTP6               (idx, [1:   4]) = [  8.89503E-05 0.28001  5.06361E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.67765E-05 1.00000 -2.03827E-04 0.68685 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.06813E-01 0.00100  3.70573E-01 0.00022 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.61177E+00 0.00100  8.99508E-01 0.00022 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  3.82159E-03 0.00089  9.10769E-02 0.00103 ];
INF_REMXS                 (idx, [1:   4]) = [  4.81614E-03 0.00621  9.16195E-02 0.00291 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51725E-01 0.00047  9.89217E-04 0.00782  5.59448E-04 0.02867  3.07787E-01 0.00087 ];
INF_S1                    (idx, [1:   8]) = [  1.98529E-02 0.00965 -2.27323E-04 0.02727  1.27970E-04 0.00559  1.75851E-02 0.00919 ];
INF_S2                    (idx, [1:   8]) = [  3.74409E-03 0.00590 -2.54337E-05 0.06754 -5.55274E-06 1.00000  2.36238E-03 0.07347 ];
INF_S3                    (idx, [1:   8]) = [  7.10789E-04 0.08004 -1.48370E-05 0.39339 -1.51997E-05 0.23817  1.34785E-04 0.99567 ];
INF_S4                    (idx, [1:   8]) = [  1.17348E-04 0.26877 -1.71973E-06 0.69530 -1.40873E-05 0.90646 -1.44625E-04 0.67800 ];
INF_S5                    (idx, [1:   8]) = [  1.33379E-05 1.00000 -1.98079E-06 0.93977 -4.56548E-06 0.70973 -1.86932E-04 0.28223 ];
INF_S6                    (idx, [1:   8]) = [  8.82075E-05 0.29275  7.53026E-07 1.00000 -4.52009E-06 1.00000  5.51562E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.20946E-05 1.00000  4.68507E-06 0.14519 -3.47136E-06 0.58100 -2.00356E-04 0.69739 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51726E-01 0.00047  9.89217E-04 0.00782  5.59448E-04 0.02867  3.07787E-01 0.00087 ];
INF_SP1                   (idx, [1:   8]) = [  1.98528E-02 0.00965 -2.27323E-04 0.02727  1.27970E-04 0.00559  1.75851E-02 0.00919 ];
INF_SP2                   (idx, [1:   8]) = [  3.74399E-03 0.00585 -2.54337E-05 0.06754 -5.55274E-06 1.00000  2.36238E-03 0.07347 ];
INF_SP3                   (idx, [1:   8]) = [  7.10892E-04 0.07995 -1.48370E-05 0.39339 -1.51997E-05 0.23817  1.34785E-04 0.99567 ];
INF_SP4                   (idx, [1:   8]) = [  1.17493E-04 0.26781 -1.71973E-06 0.69530 -1.40873E-05 0.90646 -1.44625E-04 0.67800 ];
INF_SP5                   (idx, [1:   8]) = [  1.33727E-05 1.00000 -1.98079E-06 0.93977 -4.56548E-06 0.70973 -1.86932E-04 0.28223 ];
INF_SP6                   (idx, [1:   8]) = [  8.81973E-05 0.29255  7.53026E-07 1.00000 -4.52009E-06 1.00000  5.51562E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.20914E-05 1.00000  4.68507E-06 0.14519 -3.47136E-06 0.58100 -2.00356E-04 0.69739 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.15720E-01 0.00624 -2.31205E-01 0.00688 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.54897E-01 0.00956 -1.34609E-01 0.01510 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.51470E-01 0.00953 -1.36845E-01 0.00494 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.56824E-01 0.00806  5.69272E-01 0.04302 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  8.01884E-01 0.00621 -1.44186E+00 0.00692 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.32902E-01 0.00965 -2.47742E+00 0.01493 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.38464E-01 0.00961 -2.43597E+00 0.00497 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.34287E-01 0.00800  5.87810E-01 0.04483 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.78963E-03 0.02885  2.25799E-04 0.16773  1.07643E-03 0.07116  6.86214E-04 0.08210  1.30936E-03 0.05484  2.12312E-03 0.05459  5.83667E-04 0.08976  6.14763E-04 0.12957  1.70278E-04 0.19207 ];
LAMBDA                    (idx, [1:  18]) = [  4.15241E-01 0.04642  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32068E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67778E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32505 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75742E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'g' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.79204E+04 0.01046  3.71403E+05 0.00748  7.58604E+05 0.00188  1.59551E+06 0.00087  1.81245E+06 0.00028  1.74555E+06 0.00047  1.67193E+06 0.00111  1.58970E+06 0.00107  1.51221E+06 0.00108  1.47127E+06 0.00145  1.44808E+06 0.00056  1.41994E+06 0.00075  1.40017E+06 0.00083  1.38179E+06 0.00074  1.38239E+06 0.00133  1.20651E+06 0.00139  1.20921E+06 0.00104  1.19591E+06 0.00141  1.18061E+06 0.00106  2.30562E+06 0.00211  2.22175E+06 0.00137  1.59983E+06 0.00118  1.03285E+06 0.00071  1.21494E+06 0.00025  1.16511E+06 0.00082  9.75852E+05 0.00128  1.73940E+06 0.00075  3.54733E+05 0.00108  4.32806E+05 0.00147  3.83155E+05 0.00129  2.20197E+05 0.00084  3.74404E+05 0.00327  2.50044E+05 0.00080  2.12416E+05 0.00207  4.10277E+04 0.00469  4.03010E+04 0.00306  4.11042E+04 0.00374  4.23951E+04 0.00501  4.15277E+04 0.00316  4.06616E+04 0.00425  4.18166E+04 0.00205  3.95478E+04 0.00133  7.40093E+04 0.00318  1.16228E+05 0.00478  1.46671E+05 0.00237  3.75108E+05 0.00085  3.77472E+05 0.00140  3.77977E+05 0.00225  2.27266E+05 0.00160  1.54167E+05 0.00166  1.12483E+05 0.00176  1.20607E+05 0.00197  2.02318E+05 0.00091  2.38291E+05 0.00515  4.11790E+05 0.00105  6.33397E+05 0.00047  1.16706E+06 0.00214  9.86253E+05 0.00207  8.65703E+05 0.00217  7.25497E+05 0.00184  7.38891E+05 0.00150  8.23979E+05 0.00103  7.79355E+05 0.00033  5.79936E+05 0.00067  5.83175E+05 0.00205  5.69967E+05 0.00164  5.28811E+05 0.00159  4.51322E+05 0.00396  3.24519E+05 0.00427  1.27249E+05 0.00334 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.99006E+01 0.00035  1.20911E+01 0.00104 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.59838E-01 0.00015  5.29151E-01 1.5E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.52788E-05 0.00336  2.94189E-04 0.00021 ];
INF_ABS                   (idx, [1:   4]) = [  1.52788E-05 0.00336  2.94189E-04 0.00021 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  8.71290E-08 0.00083  3.23325E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.59823E-01 0.00016  5.28855E-01 1.9E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  2.99206E-02 0.00126  2.84404E-02 0.00114 ];
INF_SCATT2                (idx, [1:   4]) = [  2.82242E-03 0.00373  1.60283E-03 0.03364 ];
INF_SCATT3                (idx, [1:   4]) = [  4.65845E-04 0.08281  2.34017E-04 0.17786 ];
INF_SCATT4                (idx, [1:   4]) = [  4.74196E-05 0.47612  1.02347E-04 0.11401 ];
INF_SCATT5                (idx, [1:   4]) = [ -4.35720E-06 1.00000  3.19003E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  7.60912E-06 1.00000 -1.33164E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -8.21770E-06 1.00000 -1.26526E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.59823E-01 0.00016  5.28855E-01 1.9E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.99206E-02 0.00126  2.84404E-02 0.00114 ];
INF_SCATTP2               (idx, [1:   4]) = [  2.82242E-03 0.00373  1.60283E-03 0.03364 ];
INF_SCATTP3               (idx, [1:   4]) = [  4.65845E-04 0.08281  2.34017E-04 0.17786 ];
INF_SCATTP4               (idx, [1:   4]) = [  4.74196E-05 0.47612  1.02347E-04 0.11401 ];
INF_SCATTP5               (idx, [1:   4]) = [ -4.35720E-06 1.00000  3.19003E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  7.60912E-06 1.00000 -1.33164E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -8.21770E-06 1.00000 -1.26526E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  3.92216E-01 0.00052  4.99635E-01 6.6E-05 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  8.49872E-01 0.00052  6.67154E-01 6.6E-05 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.52788E-05 0.00336  2.94189E-04 0.00021 ];
INF_REMXS                 (idx, [1:   4]) = [  3.35550E-03 0.00064  7.00383E-04 0.00555 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.56483E-01 0.00015  3.33995E-03 0.00073  4.04519E-04 0.00784  5.28451E-01 2.2E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.08347E-02 0.00118 -9.14094E-04 0.00232  1.09662E-04 0.01273  2.83307E-02 0.00113 ];
INF_S2                    (idx, [1:   8]) = [  2.89437E-03 0.00353 -7.19513E-05 0.02512  1.13157E-06 1.00000  1.60170E-03 0.03418 ];
INF_S3                    (idx, [1:   8]) = [  4.73571E-04 0.08142 -7.72550E-06 0.04372 -1.35116E-05 0.12531  2.47529E-04 0.16437 ];
INF_S4                    (idx, [1:   8]) = [  5.01504E-05 0.45530 -2.73076E-06 0.26696 -1.25981E-05 0.09442  1.14945E-04 0.09348 ];
INF_S5                    (idx, [1:   8]) = [ -3.68303E-06 1.00000 -6.74166E-07 0.82168 -6.98506E-06 0.12289  3.88854E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  8.87008E-06 1.00000 -1.26096E-06 0.94171 -5.01875E-06 0.22123 -8.29761E-06 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -4.76093E-06 1.00000 -3.45677E-06 0.72930 -4.82811E-06 0.15148 -7.82452E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.56483E-01 0.00015  3.33995E-03 0.00073  4.04519E-04 0.00784  5.28451E-01 2.2E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.08347E-02 0.00118 -9.14094E-04 0.00232  1.09662E-04 0.01273  2.83307E-02 0.00113 ];
INF_SP2                   (idx, [1:   8]) = [  2.89437E-03 0.00353 -7.19513E-05 0.02512  1.13157E-06 1.00000  1.60170E-03 0.03418 ];
INF_SP3                   (idx, [1:   8]) = [  4.73571E-04 0.08142 -7.72550E-06 0.04372 -1.35116E-05 0.12531  2.47529E-04 0.16437 ];
INF_SP4                   (idx, [1:   8]) = [  5.01504E-05 0.45530 -2.73076E-06 0.26696 -1.25981E-05 0.09442  1.14945E-04 0.09348 ];
INF_SP5                   (idx, [1:   8]) = [ -3.68303E-06 1.00000 -6.74166E-07 0.82168 -6.98506E-06 0.12289  3.88854E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  8.87008E-06 1.00000 -1.26096E-06 0.94171 -5.01875E-06 0.22123 -8.29761E-06 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -4.76093E-06 1.00000 -3.45677E-06 0.72930 -4.82811E-06 0.15148 -7.82452E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.99913E-01 0.00130  5.49343E-01 0.00104 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.73400E-01 0.00036  5.44619E-01 0.00780 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.73155E-01 0.00256  5.49180E-01 0.00490 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.88657E-01 0.00105  5.54452E-01 0.00628 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.66740E+00 0.00130  6.06786E-01 0.00104 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.92234E+00 0.00036  6.12123E-01 0.00778 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.92508E+00 0.00256  6.06995E-01 0.00492 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.15477E+00 0.00105  6.01241E-01 0.00629 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:18 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.10829E+00  9.51133E-01  1.00196E+00  1.00813E+00  9.27879E-01  1.00261E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89540E-01 0.00053  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10460E-01 0.00012  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47977E-01 0.00015  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52490E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.99838E+00 0.00066  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31498E-01 9.0E-06  6.77353E-02 0.00013  7.67143E-04 0.00105  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36316E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33474E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.79970E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.40878E+01 0.00059  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000321 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00053E+04 0.00112 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.32068E+01 ;
RUNNING_TIME              (idx, 1)        =  7.67780E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  8.59667E-02  8.59667E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.26667E-03  6.26667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.58538E+00  7.58538E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.67747E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.32504 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.74425E+00 0.02753 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75740E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1328.38 ;
MEMSIZE                   (idx, 1)        = 1237.29 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 309.15 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99729E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.62029E-02 0.00421 ];
U235_FISS                 (idx, [1:   4]) = [  4.36870E-01 0.00093  9.99274E-01 2.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.17567E-04 0.03068  7.26288E-04 0.03059 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65541E-01 0.00155  5.95816E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  1.57890E-02 0.00415  5.68290E-02 0.00407 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000321 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.64938E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 834476 8.34645E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1313121 1.31334E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 852724 8.52779E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000321 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.06288E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41801E-11 0.00051 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06966E+00 0.00051 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37551E-01 0.00051 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.78575E-01 0.00046 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16127E-01 0.00039 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98646E-01 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50197E+02 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83873E-01 0.00098 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.32870E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06933E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43121E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.16580E-01 0.00146 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53791E+00 0.00152 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84754E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12057E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49521E+00 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07019E+00 0.00083 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44466E+00 4.0E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.2E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07016E+00 0.00086  1.06225E+00 0.00083  7.93273E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07113E+00 0.00079 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06993E+00 0.00051 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49421E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33742E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33700E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.11123E-05 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  3.12357E-05 0.00346 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56070E-02 0.00459 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.64469E-02 0.00106 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21768E-03 0.00732  2.03463E-04 0.03566  9.24874E-04 0.01533  6.07179E-04 0.02446  1.25256E-03 0.01476  1.97526E-03 0.01208  5.80670E-04 0.01880  5.21977E-04 0.02785  1.51699E-04 0.04713 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14502E-01 0.01126  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.34440E-03 0.01035  2.37022E-04 0.06415  1.08329E-03 0.02765  7.28997E-04 0.03582  1.48204E-03 0.02129  2.30519E-03 0.01692  7.23408E-04 0.03502  6.00100E-04 0.04400  1.84349E-04 0.06555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.15494E-01 0.01719  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.64801E-05 0.00445  3.65027E-05 0.00448  3.37429E-05 0.04757 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90408E-05 0.00463  3.90651E-05 0.00465  3.61098E-05 0.04758 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.42299E-03 0.00872  2.55877E-04 0.04765  1.09239E-03 0.02312  7.38840E-04 0.03427  1.48754E-03 0.02168  2.37348E-03 0.01775  7.08720E-04 0.03279  5.80031E-04 0.04120  1.86108E-04 0.07004 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08924E-01 0.01680  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21517E-05 0.04050  3.21196E-05 0.04057  3.48805E-05 0.13175 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.43968E-05 0.04051  3.43630E-05 0.04059  3.72599E-05 0.13091 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.77120E-03 0.05706  2.02452E-04 0.21786  1.10179E-03 0.09526  6.89720E-04 0.12609  1.38198E-03 0.09359  1.99359E-03 0.07864  7.38007E-04 0.12059  5.84642E-04 0.13249  7.90242E-05 0.24825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.79250E-01 0.05135  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77455E-03 0.05553  2.06979E-04 0.19748  1.09708E-03 0.09130  6.90137E-04 0.12554  1.35311E-03 0.09410  2.01895E-03 0.07631  7.17877E-04 0.12034  6.06195E-04 0.13456  8.42167E-05 0.24429 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.84099E-01 0.05167  1.24667E-02 3.8E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 2.7E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12489E+02 0.04341 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61233E-05 0.00189 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86559E-05 0.00160 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.44875E-03 0.00571 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06222E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.21693E-07 0.00211 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.84024E-05 0.00060  1.84053E-05 0.00059  1.80142E-05 0.00708 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.88252E-04 0.00204  1.88340E-04 0.00205  1.76671E-04 0.02616 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.24306E-01 0.00129  2.24115E-01 0.00129  2.55414E-01 0.01555 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.29991E+01 0.01483 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33474E+01 0.00052  5.40465E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '_' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CAPT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_ABS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP1               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP2               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP3               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP4               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP5               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP6               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP7               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_REMXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP1                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP2                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP3                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP4                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP5                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP6                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP7                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

