
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37931E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87302E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29227 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70727E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   2]) = '40' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  4.86534E+05 0.00222  2.30895E+06 0.00068  5.21167E+06 0.00125  9.37754E+06 0.00071  9.92989E+06 0.00022  9.23659E+06 0.00069  8.43401E+06 0.00045  7.50050E+06 0.00034  6.68758E+06 0.00033  6.04409E+06 0.00115  5.57628E+06 0.00156  5.19938E+06 0.00033  4.83804E+06 0.00036  4.63555E+06 0.00041  4.46066E+06 0.00068  3.79849E+06 0.00048  3.72615E+06 0.00121  3.57166E+06 0.00140  3.40723E+06 0.00047  6.32865E+06 0.00036  5.56318E+06 0.00104  3.67139E+06 0.00110  2.21742E+06 0.00164  2.39476E+06 0.00206  2.13342E+06 0.00274  1.70580E+06 0.00172  2.85411E+06 0.00143  5.79634E+05 0.00316  7.11864E+05 0.00281  6.43281E+05 0.00135  3.65174E+05 0.00144  6.29289E+05 0.00198  4.18988E+05 0.00131  3.45954E+05 0.00246  6.43270E+04 0.00468  6.24564E+04 0.00289  6.48093E+04 0.00598  6.63217E+04 0.00508  6.53176E+04 0.00705  6.39956E+04 0.00161  6.58652E+04 0.00525  6.11511E+04 0.00540  1.15238E+05 0.00306  1.80733E+05 0.00516  2.25712E+05 0.00233  5.75355E+05 0.00087  5.60551E+05 0.00087  5.43261E+05 0.00292  3.11727E+05 0.00271  2.03354E+05 0.00629  1.45345E+05 0.00130  1.54968E+05 0.00193  2.55461E+05 0.00312  2.93918E+05 0.00134  4.98403E+05 0.00088  7.36185E+05 0.00074  1.30531E+06 0.00147  1.07973E+06 0.00142  9.37864E+05 0.00040  7.81816E+05 0.00156  7.91743E+05 0.00136  8.78675E+05 0.00192  8.27531E+05 0.00130  6.12550E+05 0.00154  6.16452E+05 0.00084  6.00476E+05 0.00083  5.55041E+05 0.00340  4.71738E+05 0.00489  3.38468E+05 0.00221  1.31804E+05 0.00443 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00070 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36541E+02 0.00030  1.36260E+01 0.00108 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.37575E-01 0.00013  5.25565E-01 9.1E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.78804E-03 0.00044  2.32090E-03 0.00162 ];
INF_ABS                   (idx, [1:   4]) = [  4.07659E-03 0.00024  1.16749E-02 0.00121 ];
INF_FISS                  (idx, [1:   4]) = [  2.28855E-03 0.00010  9.35403E-03 0.00112 ];
INF_NSF                   (idx, [1:   4]) = [  5.60251E-03 0.00010  2.27877E-02 0.00112 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44807E+00 2.1E-06  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.6E-08  2.02270E+02 1.3E-08 ];
INF_INVV                  (idx, [1:   4]) = [  4.58776E-08 0.00082  3.11852E-06 0.00058 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.33495E-01 0.00014  5.13907E-01 5.5E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  3.73554E-02 0.00060  3.13300E-02 0.00197 ];
INF_SCATT2                (idx, [1:   4]) = [  1.00490E-02 0.00087  3.51983E-03 0.00856 ];
INF_SCATT3                (idx, [1:   4]) = [  9.76527E-04 0.01111  7.89309E-04 0.01081 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.88106E-04 0.00783  2.35952E-04 0.06459 ];
INF_SCATT5                (idx, [1:   4]) = [  7.50420E-05 0.19686 -4.16719E-07 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  4.09164E-04 0.01796  3.15408E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  4.88394E-05 0.18846  5.24819E-05 0.79206 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.33497E-01 0.00014  5.13907E-01 5.5E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.73555E-02 0.00060  3.13300E-02 0.00197 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.00490E-02 0.00087  3.51983E-03 0.00856 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.76540E-04 0.01110  7.89309E-04 0.01081 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.88088E-04 0.00780  2.35952E-04 0.06459 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.50427E-05 0.19681 -4.16719E-07 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.09143E-04 0.01794  3.15408E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.88244E-05 0.18816  5.24819E-05 0.79206 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.51109E-01 0.00031  4.92360E-01 3.7E-05 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.32745E+00 0.00031  6.77012E-01 3.7E-05 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.07473E-03 0.00024  1.16749E-02 0.00121 ];
INF_REMXS                 (idx, [1:   4]) = [  5.79358E-03 0.00081  1.21392E-02 0.00224 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.31781E-01 0.00014  1.71399E-03 0.00061  4.81762E-04 0.00164  5.13426E-01 5.4E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.75245E-02 0.00059 -1.69036E-04 0.00238  1.29250E-04 0.01377  3.12007E-02 0.00198 ];
INF_S2                    (idx, [1:   8]) = [  1.01275E-02 0.00090 -7.85271E-05 0.00893  7.68546E-06 0.13071  3.51215E-03 0.00886 ];
INF_S3                    (idx, [1:   8]) = [  1.06735E-03 0.00974 -9.08274E-05 0.01189 -1.03774E-05 0.12978  7.99686E-04 0.01218 ];
INF_S4                    (idx, [1:   8]) = [ -5.56823E-04 0.00735 -3.12822E-05 0.02079 -8.70565E-06 0.05438  2.44658E-04 0.06418 ];
INF_S5                    (idx, [1:   8]) = [  6.75955E-05 0.22041  7.44653E-06 0.09874 -6.84637E-06 0.12128  6.42965E-06 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  4.01297E-04 0.01908  7.86728E-06 0.03994 -3.31792E-06 0.14622  3.48587E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  4.90568E-05 0.18863 -2.17415E-07 1.00000 -2.55760E-06 0.13813  5.50395E-05 0.74900 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.31783E-01 0.00014  1.71399E-03 0.00061  4.81762E-04 0.00164  5.13426E-01 5.4E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.75245E-02 0.00059 -1.69036E-04 0.00238  1.29250E-04 0.01377  3.12007E-02 0.00198 ];
INF_SP2                   (idx, [1:   8]) = [  1.01276E-02 0.00090 -7.85271E-05 0.00893  7.68546E-06 0.13071  3.51215E-03 0.00886 ];
INF_SP3                   (idx, [1:   8]) = [  1.06737E-03 0.00973 -9.08274E-05 0.01189 -1.03774E-05 0.12978  7.99686E-04 0.01218 ];
INF_SP4                   (idx, [1:   8]) = [ -5.56805E-04 0.00733 -3.12822E-05 0.02079 -8.70565E-06 0.05438  2.44658E-04 0.06418 ];
INF_SP5                   (idx, [1:   8]) = [  6.75962E-05 0.22033  7.44653E-06 0.09874 -6.84637E-06 0.12128  6.42965E-06 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  4.01276E-04 0.01906  7.86728E-06 0.03994 -3.31792E-06 0.14622  3.48587E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  4.90418E-05 0.18836 -2.17415E-07 1.00000 -2.55760E-06 0.13813  5.50395E-05 0.74900 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.79926E-01 0.00080  7.73562E-01 0.00363 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.83424E-01 0.00026  9.90096E-01 0.01488 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.84347E-01 0.00134  9.75468E-01 0.01346 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.72332E-01 0.00124  5.42852E-01 0.00678 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.19079E+00 0.00080  4.30918E-01 0.00363 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.17610E+00 0.00026  3.36816E-01 0.01481 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.17228E+00 0.00134  3.41841E-01 0.01360 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.22400E+00 0.00124  6.14097E-01 0.00681 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
LAMBDA                    (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37931E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87303E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29226 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70725E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'F' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.71397E+05 0.00381  1.28872E+06 0.00136  2.95499E+06 0.00158  5.18821E+06 0.00115  5.41183E+06 0.00064  5.01282E+06 0.00101  4.53397E+06 0.00037  3.96952E+06 0.00100  3.47168E+06 0.00019  3.06750E+06 0.00092  2.76050E+06 0.00148  2.51566E+06 0.00088  2.28502E+06 0.00120  2.14481E+06 0.00061  2.02392E+06 0.00076  1.69649E+06 0.00033  1.63898E+06 0.00111  1.53565E+06 0.00141  1.43097E+06 0.00093  2.55010E+06 0.00054  2.06776E+06 0.00062  1.24479E+06 0.00154  6.94735E+05 0.00033  6.65715E+05 0.00182  5.26117E+05 0.00232  3.88459E+05 0.00333  5.67772E+05 0.00385  1.13269E+05 0.00176  1.41520E+05 0.00673  1.33849E+05 0.00347  7.37420E+04 0.00246  1.32537E+05 0.00394  8.82540E+04 0.00560  6.75921E+04 0.00582  1.10590E+04 0.02008  1.07758E+04 0.01059  1.13072E+04 0.02437  1.15948E+04 0.03097  1.16783E+04 0.02901  1.12129E+04 0.01611  1.17891E+04 0.00473  1.07601E+04 0.01222  2.01317E+04 0.01147  3.11232E+04 0.00794  3.85533E+04 0.00674  9.38285E+04 0.00550  8.44251E+04 0.00992  7.13804E+04 0.00264  3.44123E+04 0.00733  1.93045E+04 0.01961  1.24899E+04 0.02704  1.17993E+04 0.00824  1.79826E+04 0.03807  1.84201E+04 0.00299  2.63844E+04 0.00701  2.89868E+04 0.00401  3.05918E+04 0.00500  1.54543E+04 0.01390  9.61568E+03 0.01501  6.73781E+03 0.00937  5.66716E+03 0.00672  5.16027E+03 0.02412  4.22147E+03 0.02586  2.79329E+03 0.02801  2.40998E+03 0.01029  2.15123E+03 0.03422  1.61889E+03 0.04284  1.09641E+03 0.01897  7.20796E+02 0.05275  1.57340E+02 0.07878 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.61173E+00 0.00093 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  6.29031E+01 0.00024  4.13787E-01 0.00158 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55551E-01 9.4E-05  4.68402E-01 0.00138 ];
INF_CAPT                  (idx, [1:   4]) = [  2.34239E-03 0.00013  2.37559E-02 0.00291 ];
INF_ABS                   (idx, [1:   4]) = [  6.31443E-03 0.00012  1.53764E-01 0.00274 ];
INF_FISS                  (idx, [1:   4]) = [  3.97203E-03 0.00015  1.30008E-01 0.00271 ];
INF_NSF                   (idx, [1:   4]) = [  9.72669E-03 0.00015  3.16718E-01 0.00271 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44879E+00 7.6E-07  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 2.0E-08  2.02270E+02 1.3E-08 ];
INF_INVV                  (idx, [1:   4]) = [  2.41918E-08 0.00132  1.58661E-06 0.00196 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49232E-01 7.3E-05  3.14680E-01 0.00105 ];
INF_SCATT1                (idx, [1:   4]) = [  2.05072E-02 0.00146  1.80602E-02 0.00543 ];
INF_SCATT2                (idx, [1:   4]) = [  4.44619E-03 0.00211  1.30945E-03 0.16174 ];
INF_SCATT3                (idx, [1:   4]) = [  1.00514E-03 0.00571  3.47234E-04 0.92495 ];
INF_SCATT4                (idx, [1:   4]) = [  2.41450E-04 0.01507 -1.45250E-04 0.97448 ];
INF_SCATT5                (idx, [1:   4]) = [  1.19549E-04 0.08363 -2.31001E-04 0.25528 ];
INF_SCATT6                (idx, [1:   4]) = [  7.67543E-05 0.04134 -2.44690E-04 0.95090 ];
INF_SCATT7                (idx, [1:   4]) = [  1.65756E-05 0.46149 -1.05705E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49235E-01 7.4E-05  3.14680E-01 0.00105 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.05072E-02 0.00146  1.80602E-02 0.00543 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.44616E-03 0.00210  1.30945E-03 0.16174 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.00519E-03 0.00573  3.47234E-04 0.92495 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.41481E-04 0.01500 -1.45250E-04 0.97448 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.19567E-04 0.08340 -2.31001E-04 0.25528 ];
INF_SCATTP6               (idx, [1:   4]) = [  7.67189E-05 0.04132 -2.44690E-04 0.95090 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.65414E-05 0.46131 -1.05705E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02066E-01 0.00022  4.32119E-01 0.00189 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64963E+00 0.00022  7.71397E-01 0.00189 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.31169E-03 0.00011  1.53764E-01 0.00274 ];
INF_REMXS                 (idx, [1:   4]) = [  6.65143E-03 0.00111  1.55400E-01 0.00306 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48900E-01 7.2E-05  3.32266E-04 0.00163  1.67716E-03 0.01748  3.13003E-01 0.00113 ];
INF_S1                    (idx, [1:   8]) = [  2.05827E-02 0.00143 -7.55287E-05 0.00644  4.12720E-04 0.00840  1.76475E-02 0.00575 ];
INF_S2                    (idx, [1:   8]) = [  4.45546E-03 0.00221 -9.27344E-06 0.07248  1.49892E-05 1.00000  1.29446E-03 0.15748 ];
INF_S3                    (idx, [1:   8]) = [  1.01044E-03 0.00546 -5.30533E-06 0.10097 -3.13080E-05 0.50608  3.78542E-04 0.83298 ];
INF_S4                    (idx, [1:   8]) = [  2.43555E-04 0.01536 -2.10476E-06 0.13170 -3.39580E-05 0.33534 -1.11293E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.19415E-04 0.08261  1.33490E-07 1.00000 -4.33501E-05 0.07732 -1.87651E-04 0.33202 ];
INF_S6                    (idx, [1:   8]) = [  7.59405E-05 0.03757  8.13841E-07 0.39552 -2.17087E-05 0.87000 -2.22981E-04 0.96854 ];
INF_S7                    (idx, [1:   8]) = [  1.69855E-05 0.44579 -4.09902E-07 0.50694 -1.38737E-05 0.12992  3.30320E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48903E-01 7.2E-05  3.32266E-04 0.00163  1.67716E-03 0.01748  3.13003E-01 0.00113 ];
INF_SP1                   (idx, [1:   8]) = [  2.05827E-02 0.00143 -7.55287E-05 0.00644  4.12720E-04 0.00840  1.76475E-02 0.00575 ];
INF_SP2                   (idx, [1:   8]) = [  4.45543E-03 0.00220 -9.27344E-06 0.07248  1.49892E-05 1.00000  1.29446E-03 0.15748 ];
INF_SP3                   (idx, [1:   8]) = [  1.01050E-03 0.00548 -5.30533E-06 0.10097 -3.13080E-05 0.50608  3.78542E-04 0.83298 ];
INF_SP4                   (idx, [1:   8]) = [  2.43586E-04 0.01529 -2.10476E-06 0.13170 -3.39580E-05 0.33534 -1.11293E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.19433E-04 0.08237  1.33490E-07 1.00000 -4.33501E-05 0.07732 -1.87651E-04 0.33202 ];
INF_SP6                   (idx, [1:   8]) = [  7.59050E-05 0.03754  8.13841E-07 0.39552 -2.17087E-05 0.87000 -2.22981E-04 0.96854 ];
INF_SP7                   (idx, [1:   8]) = [  1.69513E-05 0.44551 -4.09902E-07 0.50694 -1.38737E-05 0.12992  3.30320E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.44182E-01 0.00136 -1.78933E+01 0.53948 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  3.97566E-01 0.00152 -7.33525E-01 0.03680 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  3.97412E-01 0.00235 -7.69594E-01 0.05856 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.71392E-01 0.00085  4.16014E-01 0.01306 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  9.68482E-01 0.00136 -3.00688E-02 0.35514 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  8.38438E-01 0.00152 -4.55707E-01 0.03818 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  8.38770E-01 0.00235 -4.36028E-01 0.05685 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.22824E+00 0.00085  8.01529E-01 0.01311 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.39638E-03 0.01239  2.51993E-04 0.06325  1.10628E-03 0.03176  7.56405E-04 0.03721  1.41190E-03 0.02879  2.33083E-03 0.02315  7.14735E-04 0.03944  6.53518E-04 0.04066  1.70720E-04 0.08017 ];
LAMBDA                    (idx, [1:  18]) = [  4.17058E-01 0.01934  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37931E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87303E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29226 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70725E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'T' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.75469E+04 0.00859  8.28479E+04 0.00634  1.86434E+05 0.00411  3.33910E+05 0.00598  3.56223E+05 0.00269  3.30662E+05 0.00251  2.96961E+05 0.00179  2.62779E+05 0.00351  2.30059E+05 0.00607  2.03579E+05 0.00369  1.83052E+05 0.00815  1.69526E+05 0.00199  1.51616E+05 0.00268  1.48592E+05 0.00413  1.37943E+05 0.01080  1.16532E+05 0.00196  1.14196E+05 0.00521  1.07008E+05 0.00346  1.00511E+05 0.00263  1.80366E+05 0.00489  1.45696E+05 0.00171  8.78081E+04 0.00497  5.05725E+04 0.00857  4.82445E+04 0.01325  3.99523E+04 0.00306  2.76711E+04 0.00823  3.93527E+04 0.01241  7.49176E+03 0.00462  9.38974E+03 0.00272  8.41871E+03 0.00396  4.92560E+03 0.01939  8.45920E+03 0.01582  5.36210E+03 0.01855  4.40767E+03 0.02259  6.92778E+02 0.08783  7.10527E+02 0.02332  7.56452E+02 0.02927  7.52976E+02 0.06925  7.32473E+02 0.08204  7.20356E+02 0.04810  8.27055E+02 0.06812  7.24410E+02 0.03601  1.41493E+03 0.04392  2.06117E+03 0.06628  2.64421E+03 0.00896  6.39379E+03 0.02501  5.76110E+03 0.01368  5.11278E+03 0.00803  2.54971E+03 0.01390  1.44148E+03 0.03923  1.02520E+03 0.01517  1.10876E+03 0.09522  1.53975E+03 0.01261  1.56900E+03 0.03094  2.42552E+03 0.02978  2.69941E+03 0.01863  3.11059E+03 0.02709  1.79653E+03 0.05395  1.30428E+03 0.04780  8.65632E+02 0.02018  7.36548E+02 0.02149  7.71984E+02 0.05193  7.21053E+02 0.03153  4.50325E+02 0.02813  4.76066E+02 0.05630  4.50526E+02 0.02254  3.62963E+02 0.10062  3.93974E+02 0.03958  2.89199E+02 0.04325  1.37042E+02 0.10973 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.21455E+00 0.00172  3.70820E-02 0.00364 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.20374E-01 0.00104  6.37784E-01 0.00586 ];
INF_CAPT                  (idx, [1:   4]) = [  8.64481E-03 0.00131  2.56115E-02 0.00380 ];
INF_ABS                   (idx, [1:   4]) = [  8.64481E-03 0.00131  2.56115E-02 0.00380 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.47426E-08 0.00295  1.92400E-06 0.00307 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.11686E-01 0.00109  6.12483E-01 0.00584 ];
INF_SCATT1                (idx, [1:   4]) = [  1.52220E-01 0.00082  2.18494E-01 0.00732 ];
INF_SCATT2                (idx, [1:   4]) = [  6.29531E-02 0.00230  8.78733E-02 0.00184 ];
INF_SCATT3                (idx, [1:   4]) = [  2.71592E-03 0.04676  2.50477E-02 0.01797 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.70367E-03 0.01936  3.41093E-03 0.13406 ];
INF_SCATT5                (idx, [1:   4]) = [  2.41222E-04 0.19527 -6.44686E-04 0.18298 ];
INF_SCATT6                (idx, [1:   4]) = [  3.39054E-03 0.00606 -3.98898E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.86614E-04 0.22737 -2.73702E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.11690E-01 0.00109  6.12483E-01 0.00584 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.52220E-01 0.00081  2.18494E-01 0.00732 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.29529E-02 0.00230  8.78733E-02 0.00184 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.71573E-03 0.04678  2.50477E-02 0.01797 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.70391E-03 0.01937  3.41093E-03 0.13406 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.41071E-04 0.19625 -6.44686E-04 0.18298 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.39062E-03 0.00601 -3.98898E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.86631E-04 0.22784 -2.73702E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.00251E-01 0.00187  3.99064E-01 0.00975 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.66459E+00 0.00187  8.35445E-01 0.00966 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.64119E-03 0.00129  2.56115E-02 0.00380 ];
INF_REMXS                 (idx, [1:   4]) = [  1.17399E-02 0.00174  2.69638E-02 0.02500 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.08634E-01 0.00111  3.05190E-03 0.00179  1.66311E-03 0.05607  6.10820E-01 0.00574 ];
INF_S1                    (idx, [1:   8]) = [  1.51127E-01 0.00081  1.09268E-03 0.00168  4.40374E-04 0.40983  2.18054E-01 0.00768 ];
INF_S2                    (idx, [1:   8]) = [  6.33313E-02 0.00232 -3.78171E-04 0.00612  5.05806E-04 0.19976  8.73675E-02 0.00300 ];
INF_S3                    (idx, [1:   8]) = [  3.29138E-03 0.03800 -5.75459E-04 0.00952  3.28501E-04 0.24764  2.47192E-02 0.02021 ];
INF_S4                    (idx, [1:   8]) = [ -6.53353E-03 0.01891 -1.70138E-04 0.04290  2.18600E-04 0.32957  3.19233E-03 0.16389 ];
INF_S5                    (idx, [1:   8]) = [  1.77001E-04 0.25548  6.42211E-05 0.07992  9.71320E-05 0.69524 -7.41818E-04 0.13881 ];
INF_S6                    (idx, [1:   8]) = [  3.34443E-03 0.00546  4.61092E-05 0.06562  1.82234E-04 0.35275 -5.81131E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.88210E-04 0.21021 -1.59560E-06 1.00000  3.98328E-05 1.00000 -3.13535E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.08638E-01 0.00111  3.05190E-03 0.00179  1.66311E-03 0.05607  6.10820E-01 0.00574 ];
INF_SP1                   (idx, [1:   8]) = [  1.51127E-01 0.00081  1.09268E-03 0.00168  4.40374E-04 0.40983  2.18054E-01 0.00768 ];
INF_SP2                   (idx, [1:   8]) = [  6.33311E-02 0.00232 -3.78171E-04 0.00612  5.05806E-04 0.19976  8.73675E-02 0.00300 ];
INF_SP3                   (idx, [1:   8]) = [  3.29119E-03 0.03803 -5.75459E-04 0.00952  3.28501E-04 0.24764  2.47192E-02 0.02021 ];
INF_SP4                   (idx, [1:   8]) = [ -6.53377E-03 0.01892 -1.70138E-04 0.04290  2.18600E-04 0.32957  3.19233E-03 0.16389 ];
INF_SP5                   (idx, [1:   8]) = [  1.76850E-04 0.25680  6.42211E-05 0.07992  9.71320E-05 0.69524 -7.41818E-04 0.13881 ];
INF_SP6                   (idx, [1:   8]) = [  3.34451E-03 0.00542  4.61092E-05 0.06562  1.82234E-04 0.35275 -5.81131E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.88226E-04 0.21068 -1.59560E-06 1.00000  3.98328E-05 1.00000 -3.13535E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.25323E-01 0.00376  1.20699E+00 0.06426 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.55265E-01 0.00439  1.62789E+01 0.84363 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.58894E-01 0.00438 -7.52484E+00 1.00000 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.80698E-01 0.00369  4.99744E-01 0.10259 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.47940E+00 0.00377  2.78402E-01 0.06255 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.30588E+00 0.00441  9.05163E-02 0.46401 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.28758E+00 0.00439  6.32469E-02 0.68032 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.84474E+00 0.00369  6.81444E-01 0.10379 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37931E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87303E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29226 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70725E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'C' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.88945E+04 0.00918  1.86975E+05 0.00652  4.30749E+05 0.00326  7.61938E+05 0.00272  8.01234E+05 0.00169  7.41733E+05 0.00137  6.65152E+05 0.00126  5.78459E+05 0.00129  5.09103E+05 0.00165  4.47004E+05 0.00129  4.06231E+05 0.00307  3.76120E+05 0.00297  3.38285E+05 0.00060  3.32163E+05 0.00150  3.12517E+05 0.00148  2.67319E+05 0.00375  2.60657E+05 0.00418  2.50229E+05 0.00181  2.37300E+05 0.00116  4.34300E+05 0.00130  3.72559E+05 0.00288  2.40620E+05 0.00233  1.41081E+05 0.00166  1.46356E+05 0.00500  1.24784E+05 0.00506  9.49893E+04 0.00135  1.48503E+05 0.00091  2.96288E+04 0.01258  3.63573E+04 0.00271  3.25062E+04 0.00788  1.85873E+04 0.00972  3.24376E+04 0.01013  2.10549E+04 0.01510  1.73011E+04 0.01086  3.18893E+03 0.00580  2.90084E+03 0.01216  3.14032E+03 0.03413  3.35722E+03 0.04009  3.21753E+03 0.01986  3.12242E+03 0.01585  3.22630E+03 0.02148  2.93482E+03 0.02107  5.30337E+03 0.01841  8.26649E+03 0.00568  1.03650E+04 0.01806  2.62465E+04 0.01344  2.44589E+04 0.00551  2.33590E+04 0.00450  1.22237E+04 0.00703  7.60084E+03 0.02261  5.41320E+03 0.01266  5.57789E+03 0.01966  8.68847E+03 0.01021  9.42102E+03 0.01744  1.45231E+04 0.01198  1.80366E+04 0.01911  2.40319E+04 0.01205  1.63914E+04 0.01438  1.27219E+04 0.00723  9.46170E+03 0.01326  9.36081E+03 0.00202  9.82327E+03 0.02264  8.63937E+03 0.01869  6.13845E+03 0.01971  6.05711E+03 0.03325  5.75365E+03 0.00930  5.11148E+03 0.01231  4.28896E+03 0.01520  3.07646E+03 0.00986  1.25993E+03 0.02068 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.90374E+00 0.00023  2.51302E-01 0.00433 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.99967E-01 0.00028  7.29400E-01 0.00391 ];
INF_CAPT                  (idx, [1:   4]) = [  1.78939E-03 0.00220  1.49974E-02 0.00930 ];
INF_ABS                   (idx, [1:   4]) = [  1.78939E-03 0.00220  1.49974E-02 0.00930 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.47342E-08 0.00109  2.36227E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.98188E-01 0.00030  7.14471E-01 0.00383 ];
INF_SCATT1                (idx, [1:   4]) = [  1.60504E-01 0.00017  2.11701E-01 0.00438 ];
INF_SCATT2                (idx, [1:   4]) = [  6.40909E-02 0.00036  8.20965E-02 0.00407 ];
INF_SCATT3                (idx, [1:   4]) = [  2.44706E-03 0.03438  2.69987E-02 0.00702 ];
INF_SCATT4                (idx, [1:   4]) = [ -7.23651E-03 0.00935  7.48811E-03 0.02845 ];
INF_SCATT5                (idx, [1:   4]) = [ -4.57537E-05 1.00000  1.45059E-03 0.22800 ];
INF_SCATT6                (idx, [1:   4]) = [  3.34588E-03 0.00911  6.47272E-04 0.77876 ];
INF_SCATT7                (idx, [1:   4]) = [  2.98639E-04 0.24639  9.11150E-04 0.25235 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.98191E-01 0.00030  7.14471E-01 0.00383 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.60505E-01 0.00017  2.11701E-01 0.00438 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.40910E-02 0.00036  8.20965E-02 0.00407 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.44695E-03 0.03433  2.69987E-02 0.00702 ];
INF_SCATTP4               (idx, [1:   4]) = [ -7.23638E-03 0.00934  7.48811E-03 0.02845 ];
INF_SCATTP5               (idx, [1:   4]) = [ -4.58015E-05 1.00000  1.45059E-03 0.22800 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.34584E-03 0.00911  6.47272E-04 0.77876 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.98557E-04 0.24636  9.11150E-04 0.25235 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.70415E-01 0.00079  4.99907E-01 0.00338 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.23268E+00 0.00079  6.66806E-01 0.00337 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.78584E-03 0.00224  1.49974E-02 0.00930 ];
INF_REMXS                 (idx, [1:   4]) = [  6.92352E-03 0.00092  1.62344E-02 0.00528 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.93044E-01 0.00027  5.14384E-03 0.00298  1.30493E-03 0.03098  7.13166E-01 0.00388 ];
INF_S1                    (idx, [1:   8]) = [  1.58751E-01 0.00018  1.75291E-03 0.00347  3.61362E-04 0.07235  2.11339E-01 0.00450 ];
INF_S2                    (idx, [1:   8]) = [  6.46361E-02 0.00043 -5.45278E-04 0.00844  2.21088E-04 0.06173  8.18754E-02 0.00425 ];
INF_S3                    (idx, [1:   8]) = [  3.36835E-03 0.02283 -9.21297E-04 0.00809  1.40518E-04 0.10932  2.68582E-02 0.00761 ];
INF_S4                    (idx, [1:   8]) = [ -6.92601E-03 0.00931 -3.10508E-04 0.01110  9.14508E-05 0.15527  7.39666E-03 0.03037 ];
INF_S5                    (idx, [1:   8]) = [ -1.27953E-04 0.45583  8.21994E-05 0.05501  5.02052E-05 0.45455  1.40039E-03 0.22047 ];
INF_S6                    (idx, [1:   8]) = [  3.26200E-03 0.00921  8.38828E-05 0.00591  3.44388E-05 0.40925  6.12833E-04 0.82929 ];
INF_S7                    (idx, [1:   8]) = [  2.90936E-04 0.24957  7.70320E-06 0.25302  2.31079E-06 1.00000  9.08840E-04 0.25554 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.93047E-01 0.00027  5.14384E-03 0.00298  1.30493E-03 0.03098  7.13166E-01 0.00388 ];
INF_SP1                   (idx, [1:   8]) = [  1.58752E-01 0.00018  1.75291E-03 0.00347  3.61362E-04 0.07235  2.11339E-01 0.00450 ];
INF_SP2                   (idx, [1:   8]) = [  6.46362E-02 0.00043 -5.45278E-04 0.00844  2.21088E-04 0.06173  8.18754E-02 0.00425 ];
INF_SP3                   (idx, [1:   8]) = [  3.36824E-03 0.02279 -9.21297E-04 0.00809  1.40518E-04 0.10932  2.68582E-02 0.00761 ];
INF_SP4                   (idx, [1:   8]) = [ -6.92587E-03 0.00929 -3.10508E-04 0.01110  9.14508E-05 0.15527  7.39666E-03 0.03037 ];
INF_SP5                   (idx, [1:   8]) = [ -1.28001E-04 0.45685  8.21994E-05 0.05501  5.02052E-05 0.45455  1.40039E-03 0.22047 ];
INF_SP6                   (idx, [1:   8]) = [  3.26196E-03 0.00920  8.38828E-05 0.00591  3.44388E-05 0.40925  6.12833E-04 0.82929 ];
INF_SP7                   (idx, [1:   8]) = [  2.90854E-04 0.24952  7.70320E-06 0.25302  2.31079E-06 1.00000  9.08840E-04 0.25554 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.20194E-01 0.00198 -2.70140E-01 0.00601 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.61011E-01 0.00285 -1.46189E-01 0.01399 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.61934E-01 0.00846 -1.50428E-01 0.01440 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.67370E-01 0.00104  4.20533E-01 0.04512 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.51383E+00 0.00198 -1.23402E+00 0.00601 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.27711E+00 0.00285 -2.28105E+00 0.01413 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.27277E+00 0.00851 -2.21683E+00 0.01455 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.99160E+00 0.00104  7.95833E-01 0.04443 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37931E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87303E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29226 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70725E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '9' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.29922E+04 0.01163  6.26059E+04 0.00535  1.45471E+05 0.00678  2.46945E+05 0.00376  2.55496E+05 0.00594  2.30804E+05 0.00392  2.08324E+05 0.00475  1.78950E+05 0.00496  1.56741E+05 0.00184  1.36141E+05 0.00473  1.23841E+05 0.00242  1.12720E+05 0.00752  1.04450E+05 0.00679  9.60650E+04 0.00703  9.32154E+04 0.00614  7.83896E+04 0.00565  7.65733E+04 0.00467  7.37207E+04 0.00588  6.90152E+04 0.00454  1.26465E+05 0.00791  1.07997E+05 0.00368  6.86790E+04 0.00668  3.99311E+04 0.01073  4.11792E+04 0.00221  3.39752E+04 0.01425  2.67334E+04 0.00441  4.15461E+04 0.00202  8.96706E+03 0.01894  1.13860E+04 0.00961  1.03202E+04 0.00924  5.82714E+03 0.03283  1.03027E+04 0.01834  6.98192E+03 0.01903  5.18210E+03 0.00965  9.28554E+02 0.00871  8.63040E+02 0.07667  9.08316E+02 0.05953  9.28386E+02 0.04817  9.29413E+02 0.01163  9.43064E+02 0.05111  9.41618E+02 0.09167  8.59202E+02 0.02463  1.58940E+03 0.03492  2.49970E+03 0.04561  3.18301E+03 0.03058  8.19965E+03 0.00273  7.41845E+03 0.00889  6.50637E+03 0.01203  3.31462E+03 0.04271  1.97816E+03 0.05311  1.24944E+03 0.02110  1.27400E+03 0.00435  2.01903E+03 0.01364  2.08574E+03 0.00451  3.21717E+03 0.01631  4.09128E+03 0.03207  4.76730E+03 0.01799  2.85764E+03 0.03953  1.95223E+03 0.02949  1.46499E+03 0.00351  1.32362E+03 0.02006  1.19049E+03 0.02169  9.11668E+02 0.02235  6.15713E+02 0.06261  5.54521E+02 0.04187  4.68723E+02 0.09224  3.75264E+02 0.08874  2.08685E+02 0.10838  1.18361E+02 0.21224  4.58603E+01 0.27949 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.67754E+00 0.00321 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.02928E+00 0.00082  4.99859E-02 0.00294 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56041E-01 0.00073  4.71572E-01 0.00396 ];
INF_CAPT                  (idx, [1:   4]) = [  2.50065E-03 0.00066  2.44404E-02 0.00427 ];
INF_ABS                   (idx, [1:   4]) = [  6.60810E-03 0.00023  1.59055E-01 0.00358 ];
INF_FISS                  (idx, [1:   4]) = [  4.10745E-03 0.00042  1.34615E-01 0.00347 ];
INF_NSF                   (idx, [1:   4]) = [  1.00500E-02 0.00041  3.27940E-01 0.00347 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44677E+00 7.4E-06  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 4.0E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.36426E-08 0.00054  1.84884E-06 0.00236 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49431E-01 0.00082  3.13468E-01 0.00324 ];
INF_SCATT1                (idx, [1:   4]) = [  2.02271E-02 0.00526  1.82539E-02 0.06316 ];
INF_SCATT2                (idx, [1:   4]) = [  4.51008E-03 0.00401  1.47657E-03 0.61787 ];
INF_SCATT3                (idx, [1:   4]) = [  8.51235E-04 0.07106  3.01882E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.37431E-04 0.34111  9.23381E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  8.30311E-05 0.61805 -4.56346E-04 0.82797 ];
INF_SCATT6                (idx, [1:   4]) = [  8.39496E-05 0.81912  6.42908E-04 0.73764 ];
INF_SCATT7                (idx, [1:   4]) = [ -6.39095E-05 0.22399 -4.93978E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49433E-01 0.00082  3.13468E-01 0.00324 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.02266E-02 0.00527  1.82539E-02 0.06316 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.51019E-03 0.00402  1.47657E-03 0.61787 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.51185E-04 0.07089  3.01882E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.37351E-04 0.34022  9.23381E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.29720E-05 0.61894 -4.56346E-04 0.82797 ];
INF_SCATTP6               (idx, [1:   4]) = [  8.38753E-05 0.81995  6.42908E-04 0.73764 ];
INF_SCATTP7               (idx, [1:   4]) = [ -6.38821E-05 0.22648 -4.93978E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02671E-01 0.00061  4.34290E-01 0.00295 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64470E+00 0.00061  7.67550E-01 0.00294 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.60623E-03 0.00026  1.59055E-01 0.00358 ];
INF_REMXS                 (idx, [1:   4]) = [  7.20483E-03 0.00250  1.59250E-01 0.00667 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48836E-01 0.00081  5.94950E-04 0.00820  1.14662E-03 0.05763  3.12321E-01 0.00320 ];
INF_S1                    (idx, [1:   8]) = [  2.03666E-02 0.00521 -1.39517E-04 0.03617  3.25980E-04 0.15174  1.79279E-02 0.06236 ];
INF_S2                    (idx, [1:   8]) = [  4.52471E-03 0.00440 -1.46319E-05 0.13343  4.27699E-05 0.62495  1.43380E-03 0.62440 ];
INF_S3                    (idx, [1:   8]) = [  8.61340E-04 0.06983 -1.01048E-05 0.15213 -3.74167E-05 0.32468  3.39299E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.40103E-04 0.33173 -2.67149E-06 0.55330 -5.77636E-05 0.08595  1.50102E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  8.24962E-05 0.63660  5.34892E-07 1.00000 -5.44716E-06 1.00000 -4.50898E-04 0.75238 ];
INF_S6                    (idx, [1:   8]) = [  8.08009E-05 0.85453  3.14872E-06 0.21622 -6.84613E-06 1.00000  6.49755E-04 0.71531 ];
INF_S7                    (idx, [1:   8]) = [ -6.14868E-05 0.24806 -2.42266E-06 0.38711 -1.75458E-05 0.86941 -3.18521E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48838E-01 0.00081  5.94950E-04 0.00820  1.14662E-03 0.05763  3.12321E-01 0.00320 ];
INF_SP1                   (idx, [1:   8]) = [  2.03662E-02 0.00522 -1.39517E-04 0.03617  3.25980E-04 0.15174  1.79279E-02 0.06236 ];
INF_SP2                   (idx, [1:   8]) = [  4.52483E-03 0.00440 -1.46319E-05 0.13343  4.27699E-05 0.62495  1.43380E-03 0.62440 ];
INF_SP3                   (idx, [1:   8]) = [  8.61289E-04 0.06966 -1.01048E-05 0.15213 -3.74167E-05 0.32468  3.39299E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.40022E-04 0.33084 -2.67149E-06 0.55330 -5.77636E-05 0.08595  1.50102E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  8.24371E-05 0.63757  5.34892E-07 1.00000 -5.44716E-06 1.00000 -4.50898E-04 0.75238 ];
INF_SP6                   (idx, [1:   8]) = [  8.07266E-05 0.85542  3.14872E-06 0.21622 -6.84613E-06 1.00000  6.49755E-04 0.71531 ];
INF_SP7                   (idx, [1:   8]) = [ -6.14595E-05 0.25066 -2.42266E-06 0.38711 -1.75458E-05 0.86941 -3.18521E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.13240E-01 0.00313 -2.10979E-01 0.01345 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.18749E-01 0.01164 -1.20828E-01 0.02478 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.06912E-01 0.00997 -1.16070E-01 0.03139 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.97783E-01 0.00711  3.72975E-01 0.04861 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  8.06650E-01 0.00313 -1.58050E+00 0.01330 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.42747E-01 0.01170 -2.76212E+00 0.02468 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.57706E-01 0.00988 -2.87754E+00 0.03163 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.11950E+00 0.00709  8.98159E-01 0.05091 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.23881E-03 0.02944  2.44310E-04 0.15294  1.21000E-03 0.05793  7.11671E-04 0.10348  1.50346E-03 0.07187  2.25127E-03 0.05939  6.83424E-04 0.09740  5.10260E-04 0.08891  1.24403E-04 0.20777 ];
LAMBDA                    (idx, [1:  18]) = [  3.71056E-01 0.04652  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37931E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87305E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29225 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70723E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '8' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.80885E+04 0.02469  8.50869E+04 0.01092  1.97053E+05 0.00311  3.33566E+05 0.00395  3.43335E+05 0.00549  3.11089E+05 0.00310  2.80235E+05 0.00297  2.42091E+05 0.00337  2.10403E+05 0.00038  1.85563E+05 0.00266  1.66813E+05 0.00457  1.53999E+05 0.00541  1.40970E+05 0.00518  1.33363E+05 0.00937  1.27829E+05 0.00454  1.07853E+05 0.00452  1.05445E+05 0.00629  1.00867E+05 0.00267  9.46591E+04 0.00733  1.75264E+05 0.00452  1.51702E+05 0.00459  9.78439E+04 0.00167  5.71236E+04 0.00480  6.00185E+04 0.01444  5.11078E+04 0.01507  3.89800E+04 0.01222  6.26385E+04 0.00370  1.29972E+04 0.00772  1.62223E+04 0.00760  1.52447E+04 0.01769  8.45303E+03 0.02709  1.47471E+04 0.03181  9.92440E+03 0.01092  7.84966E+03 0.00794  1.23080E+03 0.02376  1.32625E+03 0.06799  1.33588E+03 0.05597  1.50120E+03 0.04328  1.45089E+03 0.01431  1.39410E+03 0.05444  1.43650E+03 0.07952  1.23573E+03 0.04617  2.54107E+03 0.03315  4.05431E+03 0.01410  4.60316E+03 0.00926  1.24781E+04 0.01203  1.12094E+04 0.00257  1.04026E+04 0.00970  5.40493E+03 0.00497  3.21411E+03 0.01432  2.01120E+03 0.03584  2.28144E+03 0.02843  3.57130E+03 0.01402  3.63450E+03 0.02952  5.64133E+03 0.02465  6.49495E+03 0.02995  8.67378E+03 0.01615  5.47864E+03 0.01307  3.93697E+03 0.01421  2.92617E+03 0.02434  2.67562E+03 0.04145  2.62046E+03 0.05010  2.10293E+03 0.01494  1.32127E+03 0.02517  1.19239E+03 0.09530  1.01308E+03 0.04086  8.03790E+02 0.06303  5.92540E+02 0.10944  2.49682E+02 0.12876  7.28872E+01 0.11879 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.70020E+00 0.00145 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.15106E+00 0.00030  8.74849E-02 0.00375 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55601E-01 0.00013  4.63662E-01 0.00383 ];
INF_CAPT                  (idx, [1:   4]) = [  2.36716E-03 0.00480  2.31018E-02 0.00300 ];
INF_ABS                   (idx, [1:   4]) = [  6.12614E-03 0.00317  1.50164E-01 0.00354 ];
INF_FISS                  (idx, [1:   4]) = [  3.75898E-03 0.00215  1.27062E-01 0.00364 ];
INF_NSF                   (idx, [1:   4]) = [  9.19497E-03 0.00214  3.09541E-01 0.00364 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44613E+00 9.9E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 5.1E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.58205E-08 0.00315  1.95926E-06 0.00324 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49510E-01 0.00015  3.13865E-01 0.00324 ];
INF_SCATT1                (idx, [1:   4]) = [  2.01410E-02 0.00251  1.69291E-02 0.03659 ];
INF_SCATT2                (idx, [1:   4]) = [  4.41820E-03 0.00969  1.35479E-03 0.44391 ];
INF_SCATT3                (idx, [1:   4]) = [  8.94219E-04 0.03602  1.41884E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.92584E-04 0.13322  6.64416E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  9.59451E-05 0.36844 -4.58357E-04 0.29940 ];
INF_SCATT6                (idx, [1:   4]) = [  1.55430E-04 0.21403 -6.46715E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.22204E-05 0.19155 -6.66517E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49512E-01 0.00016  3.13865E-01 0.00324 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.01409E-02 0.00252  1.69291E-02 0.03659 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.41825E-03 0.00971  1.35479E-03 0.44391 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.94168E-04 0.03578  1.41884E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.92694E-04 0.13379  6.64416E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.57246E-05 0.37013 -4.58357E-04 0.29940 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.55382E-04 0.21386 -6.46715E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.22696E-05 0.19256 -6.66517E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02466E-01 0.00014  4.26820E-01 0.00488 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64637E+00 0.00014  7.81007E-01 0.00486 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.12325E-03 0.00329  1.50164E-01 0.00354 ];
INF_REMXS                 (idx, [1:   4]) = [  6.72652E-03 0.00146  1.50825E-01 0.00503 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48874E-01 0.00015  6.35393E-04 0.01105  1.02822E-03 0.01457  3.12837E-01 0.00326 ];
INF_S1                    (idx, [1:   8]) = [  2.02825E-02 0.00240 -1.41553E-04 0.03804  2.53276E-04 0.02443  1.66758E-02 0.03680 ];
INF_S2                    (idx, [1:   8]) = [  4.43600E-03 0.00913 -1.77956E-05 0.18159 -3.15991E-05 1.00000  1.38639E-03 0.41280 ];
INF_S3                    (idx, [1:   8]) = [  9.03295E-04 0.03700 -9.07615E-06 0.21184 -6.38072E-05 0.09458  2.05692E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.98471E-04 0.14064 -5.88707E-06 0.41480 -2.09190E-05 0.37605  8.73606E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  9.51410E-05 0.39042  8.04067E-07 1.00000 -1.31222E-06 1.00000 -4.57045E-04 0.31325 ];
INF_S6                    (idx, [1:   8]) = [  1.50737E-04 0.21367  4.69368E-06 0.46199 -1.70210E-06 0.83506 -6.29694E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.49428E-05 0.11762 -2.72235E-06 0.61015 -1.79467E-05 0.87761 -4.87051E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48877E-01 0.00016  6.35393E-04 0.01105  1.02822E-03 0.01457  3.12837E-01 0.00326 ];
INF_SP1                   (idx, [1:   8]) = [  2.02824E-02 0.00241 -1.41553E-04 0.03804  2.53276E-04 0.02443  1.66758E-02 0.03680 ];
INF_SP2                   (idx, [1:   8]) = [  4.43604E-03 0.00915 -1.77956E-05 0.18159 -3.15991E-05 1.00000  1.38639E-03 0.41280 ];
INF_SP3                   (idx, [1:   8]) = [  9.03244E-04 0.03676 -9.07615E-06 0.21184 -6.38072E-05 0.09458  2.05692E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.98581E-04 0.14120 -5.88707E-06 0.41480 -2.09190E-05 0.37605  8.73606E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  9.49205E-05 0.39214  8.04067E-07 1.00000 -1.31222E-06 1.00000 -4.57045E-04 0.31325 ];
INF_SP6                   (idx, [1:   8]) = [  1.50689E-04 0.21357  4.69368E-06 0.46199 -1.70210E-06 0.83506 -6.29694E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.49920E-05 0.11878 -2.72235E-06 0.61015 -1.79467E-05 0.87761 -4.87051E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.25549E-01 0.00273 -1.84954E-01 0.01280 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.19004E-01 0.00697 -1.03550E-01 0.01212 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.31044E-01 0.00343 -1.04895E-01 0.02976 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.08667E-01 0.00086  3.38656E-01 0.07423 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.83313E-01 0.00273 -1.80284E+00 0.01281 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.42318E-01 0.00692 -3.22000E+00 0.01205 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.27709E-01 0.00344 -3.18332E+00 0.02916 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.07991E+00 0.00086  9.94794E-01 0.07131 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.13363E-03 0.02646  2.33134E-04 0.13617  1.18835E-03 0.06170  7.53659E-04 0.08110  1.43334E-03 0.05910  2.14468E-03 0.04262  6.06188E-04 0.10025  6.04515E-04 0.08612  1.69769E-04 0.18406 ];
LAMBDA                    (idx, [1:  18]) = [  4.05105E-01 0.04667  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87305E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29229 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70723E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '7' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.13697E+04 0.01795  5.25205E+04 0.01335  1.21411E+05 0.01073  2.06331E+05 0.00385  2.10962E+05 0.00643  1.92801E+05 0.00388  1.71379E+05 0.00478  1.48662E+05 0.00758  1.29666E+05 0.00270  1.13976E+05 0.00706  1.04286E+05 0.00653  9.55123E+04 0.00554  8.83031E+04 0.01169  8.34709E+04 0.00504  8.05672E+04 0.00448  6.74265E+04 0.01091  6.67041E+04 0.01022  6.33784E+04 0.00857  5.97768E+04 0.00270  1.12112E+05 0.00082  9.89171E+04 0.01091  6.41833E+04 0.01083  3.82044E+04 0.00886  4.04152E+04 0.01602  3.44322E+04 0.00652  2.73110E+04 0.00351  4.37945E+04 0.01170  9.06090E+03 0.01274  1.13480E+04 0.01379  1.06870E+04 0.03134  5.99516E+03 0.02073  1.02878E+04 0.00695  6.89237E+03 0.02415  5.37672E+03 0.02614  9.48516E+02 0.01278  9.11717E+02 0.04414  9.68914E+02 0.03920  9.86864E+02 0.06953  9.72761E+02 0.05850  9.14212E+02 0.02886  9.65112E+02 0.00491  8.97866E+02 0.03603  1.82154E+03 0.03374  2.78686E+03 0.02422  3.33020E+03 0.01572  8.84575E+03 0.01508  8.13689E+03 0.00520  7.52962E+03 0.02649  3.89024E+03 0.00527  2.32833E+03 0.04553  1.64169E+03 0.05266  1.64957E+03 0.01940  2.65562E+03 0.01931  2.84819E+03 0.02381  4.47791E+03 0.01567  5.51739E+03 0.02779  7.74298E+03 0.02570  5.00172E+03 0.04002  3.68301E+03 0.01167  2.80212E+03 0.02007  2.61529E+03 0.04859  2.56525E+03 0.01628  2.13922E+03 0.01415  1.55455E+03 0.01905  1.32375E+03 0.04709  1.17145E+03 0.06034  9.30751E+02 0.02060  6.84818E+02 0.10209  3.64066E+02 0.02729  8.24926E+01 0.12760 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.71959E+00 0.00348 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.61065E+00 0.00200  7.33024E-02 0.00712 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55853E-01 0.00178  4.55811E-01 0.00384 ];
INF_CAPT                  (idx, [1:   4]) = [  2.26904E-03 0.00556  2.19472E-02 0.00406 ];
INF_ABS                   (idx, [1:   4]) = [  5.75400E-03 0.00512  1.42748E-01 0.00422 ];
INF_FISS                  (idx, [1:   4]) = [  3.48496E-03 0.00495  1.20800E-01 0.00426 ];
INF_NSF                   (idx, [1:   4]) = [  8.52192E-03 0.00493  2.94286E-01 0.00426 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44534E+00 2.2E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 7.5E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.89233E-08 0.00388  2.12028E-06 0.00159 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50074E-01 0.00170  3.13918E-01 0.00390 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98946E-02 0.00479  1.74326E-02 0.04798 ];
INF_SCATT2                (idx, [1:   4]) = [  4.25664E-03 0.00798  2.15258E-03 0.20968 ];
INF_SCATT3                (idx, [1:   4]) = [  8.56984E-04 0.15016  4.44048E-04 0.44541 ];
INF_SCATT4                (idx, [1:   4]) = [  2.13205E-04 0.36170  5.61166E-04 0.74507 ];
INF_SCATT5                (idx, [1:   4]) = [  1.09628E-04 0.37977 -4.48401E-04 0.59793 ];
INF_SCATT6                (idx, [1:   4]) = [  9.30408E-05 0.27855 -2.64814E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.97669E-05 0.27261 -8.31596E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50076E-01 0.00170  3.13918E-01 0.00390 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98951E-02 0.00480  1.74326E-02 0.04798 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.25736E-03 0.00802  2.15258E-03 0.20968 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.57169E-04 0.14991  4.44048E-04 0.44541 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.13238E-04 0.36210  5.61166E-04 0.74507 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.09573E-04 0.37888 -4.48401E-04 0.59793 ];
INF_SCATTP6               (idx, [1:   4]) = [  9.29318E-05 0.27957 -2.64814E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.97270E-05 0.26986 -8.31596E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02918E-01 0.00135  4.19665E-01 0.00251 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64271E+00 0.00135  7.94295E-01 0.00250 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.75221E-03 0.00507  1.42748E-01 0.00422 ];
INF_REMXS                 (idx, [1:   4]) = [  6.50620E-03 0.00569  1.42756E-01 0.00473 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49347E-01 0.00168  7.27332E-04 0.01604  8.63155E-04 0.03546  3.13055E-01 0.00388 ];
INF_S1                    (idx, [1:   8]) = [  2.00578E-02 0.00456 -1.63179E-04 0.02561  2.39919E-04 0.03977  1.71926E-02 0.04887 ];
INF_S2                    (idx, [1:   8]) = [  4.26920E-03 0.00918 -1.25685E-05 0.45223  3.26879E-05 0.38675  2.11989E-03 0.20941 ];
INF_S3                    (idx, [1:   8]) = [  8.72143E-04 0.14929 -1.51592E-05 0.11367 -3.77015E-05 0.12144  4.81749E-04 0.40383 ];
INF_S4                    (idx, [1:   8]) = [  2.20528E-04 0.35525 -7.32229E-06 0.17488 -3.50025E-05 0.26255  5.96168E-04 0.71181 ];
INF_S5                    (idx, [1:   8]) = [  1.06524E-04 0.39927  3.10426E-06 1.00000 -3.27539E-05 0.26432 -4.15647E-04 0.66074 ];
INF_S6                    (idx, [1:   8]) = [  9.22667E-05 0.32353  7.74094E-07 1.00000  1.30753E-05 0.87650 -2.77890E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.45720E-05 0.53957  5.19495E-06 0.47638  2.57391E-05 0.28147 -1.08899E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49349E-01 0.00168  7.27332E-04 0.01604  8.63155E-04 0.03546  3.13055E-01 0.00388 ];
INF_SP1                   (idx, [1:   8]) = [  2.00583E-02 0.00457 -1.63179E-04 0.02561  2.39919E-04 0.03977  1.71926E-02 0.04887 ];
INF_SP2                   (idx, [1:   8]) = [  4.26993E-03 0.00921 -1.25685E-05 0.45223  3.26879E-05 0.38675  2.11989E-03 0.20941 ];
INF_SP3                   (idx, [1:   8]) = [  8.72328E-04 0.14904 -1.51592E-05 0.11367 -3.77015E-05 0.12144  4.81749E-04 0.40383 ];
INF_SP4                   (idx, [1:   8]) = [  2.20560E-04 0.35563 -7.32229E-06 0.17488 -3.50025E-05 0.26255  5.96168E-04 0.71181 ];
INF_SP5                   (idx, [1:   8]) = [  1.06468E-04 0.39820  3.10426E-06 1.00000 -3.27539E-05 0.26432 -4.15647E-04 0.66074 ];
INF_SP6                   (idx, [1:   8]) = [  9.21577E-05 0.32476  7.74094E-07 1.00000  1.30753E-05 0.87650 -2.77890E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.45320E-05 0.53659  5.19495E-06 0.47638  2.57391E-05 0.28147 -1.08899E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.25640E-01 0.00181 -1.81815E-01 0.01660 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.18021E-01 0.01566 -1.01597E-01 0.01150 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.18544E-01 0.00489 -1.06745E-01 0.02544 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.13657E-01 0.00452  3.68771E-01 0.02976 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.83139E-01 0.00181 -1.83437E+00 0.01649 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.43787E-01 0.01549 -3.28180E+00 0.01159 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.42856E-01 0.00488 -3.12679E+00 0.02560 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.06277E+00 0.00450  9.05464E-01 0.02897 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.83870E-03 0.03274  2.73944E-04 0.16061  1.15506E-03 0.07314  6.61870E-04 0.10023  1.41328E-03 0.08928  2.01309E-03 0.05680  5.65286E-04 0.11382  6.34248E-04 0.08410  1.21919E-04 0.20019 ];
LAMBDA                    (idx, [1:  18]) = [  3.93267E-01 0.04519  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87305E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29229 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70723E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '6' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  6.23694E+03 0.01085  2.87526E+04 0.01421  6.46569E+04 0.00203  1.10913E+05 0.00752  1.13866E+05 0.01044  1.04357E+05 0.01177  9.34548E+04 0.00675  8.12500E+04 0.00762  7.04413E+04 0.00429  6.27602E+04 0.00673  5.68541E+04 0.00322  5.21473E+04 0.00442  4.78863E+04 0.00793  4.58852E+04 0.00672  4.38189E+04 0.00679  3.71149E+04 0.00421  3.65226E+04 0.00233  3.50495E+04 0.00995  3.30745E+04 0.00892  6.26065E+04 0.00721  5.35988E+04 0.00457  3.56199E+04 0.00961  2.11125E+04 0.00570  2.29480E+04 0.01500  2.01892E+04 0.00794  1.57442E+04 0.00903  2.52629E+04 0.00941  5.42770E+03 0.02888  6.48827E+03 0.01296  6.10038E+03 0.03243  3.26454E+03 0.01676  5.76908E+03 0.02471  3.91244E+03 0.03631  3.10416E+03 0.02406  6.21405E+02 0.06227  5.27687E+02 0.09830  5.80195E+02 0.05314  5.71498E+02 0.12600  6.10922E+02 0.09067  5.22118E+02 0.05514  6.09631E+02 0.05540  5.07779E+02 0.10791  9.62782E+02 0.04963  1.56769E+03 0.04522  1.86413E+03 0.01745  4.89895E+03 0.04319  4.70995E+03 0.03390  4.61899E+03 0.01050  2.38406E+03 0.05065  1.46789E+03 0.06962  9.29602E+02 0.03075  1.03051E+03 0.08076  1.67342E+03 0.03301  1.68700E+03 0.01367  2.70955E+03 0.00952  3.47191E+03 0.01857  4.86748E+03 0.02814  3.33580E+03 0.02084  2.45377E+03 0.02694  1.81030E+03 0.03557  1.72229E+03 0.03883  1.75966E+03 0.02622  1.64827E+03 0.03591  1.05388E+03 0.03263  9.39783E+02 0.01027  8.56831E+02 0.02434  7.06844E+02 0.04750  5.41160E+02 0.10783  3.09075E+02 0.06861  1.04342E+02 0.18139 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.72065E+00 0.00424 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.42936E+00 0.00127  4.67706E-02 0.00650 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55505E-01 0.00054  4.41988E-01 0.00206 ];
INF_CAPT                  (idx, [1:   4]) = [  2.10201E-03 0.00964  1.99674E-02 0.00372 ];
INF_ABS                   (idx, [1:   4]) = [  5.20881E-03 0.00501  1.29917E-01 0.00393 ];
INF_FISS                  (idx, [1:   4]) = [  3.10679E-03 0.00237  1.09949E-01 0.00397 ];
INF_NSF                   (idx, [1:   4]) = [  7.59619E-03 0.00239  2.67852E-01 0.00397 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44503E+00 1.3E-05  2.43614E+00 1.3E-08 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 6.3E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.03028E-08 0.00538  2.21285E-06 0.00869 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50251E-01 0.00043  3.11909E-01 0.00145 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98403E-02 0.00412  1.75621E-02 0.01752 ];
INF_SCATT2                (idx, [1:   4]) = [  4.14295E-03 0.02745  1.83987E-03 0.21154 ];
INF_SCATT3                (idx, [1:   4]) = [  8.48841E-04 0.13354 -1.19522E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.85378E-04 0.46863  8.71036E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [ -4.33435E-05 1.00000 -8.03498E-04 0.72805 ];
INF_SCATT6                (idx, [1:   4]) = [  1.78795E-04 0.30916  1.87730E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.60927E-05 1.00000 -2.35656E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50253E-01 0.00043  3.11909E-01 0.00145 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98409E-02 0.00411  1.75621E-02 0.01752 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.14329E-03 0.02747  1.83987E-03 0.21154 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.49085E-04 0.13383 -1.19522E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.85770E-04 0.46815  8.71036E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [ -4.28022E-05 1.00000 -8.03498E-04 0.72805 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.78915E-04 0.30849  1.87730E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.62128E-05 1.00000 -2.35656E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03705E-01 0.00153  4.06958E-01 0.00564 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63636E+00 0.00153  8.19138E-01 0.00566 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.20717E-03 0.00504  1.29917E-01 0.00393 ];
INF_REMXS                 (idx, [1:   4]) = [  6.00037E-03 0.00773  1.31027E-01 0.00383 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49505E-01 0.00045  7.46196E-04 0.01232  9.47741E-04 0.08328  3.10961E-01 0.00152 ];
INF_S1                    (idx, [1:   8]) = [  2.00011E-02 0.00425 -1.60767E-04 0.02809  2.44669E-04 0.30013  1.73174E-02 0.01931 ];
INF_S2                    (idx, [1:   8]) = [  4.16427E-03 0.02688 -2.13236E-05 0.11086  5.09626E-05 1.00000  1.78891E-03 0.20315 ];
INF_S3                    (idx, [1:   8]) = [  8.53617E-04 0.13087 -4.77533E-06 0.94362  3.26926E-06 1.00000 -1.22792E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.91728E-04 0.44864 -6.34985E-06 0.49633  9.36218E-06 1.00000  7.77414E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [ -4.34806E-05 1.00000  1.37090E-07 1.00000 -1.97496E-05 1.00000 -7.83749E-04 0.78655 ];
INF_S6                    (idx, [1:   8]) = [  1.83071E-04 0.29881 -4.27574E-06 0.89397 -5.93453E-05 0.41098  2.47076E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.63547E-05 1.00000 -2.62057E-07 1.00000 -6.41471E-05 0.30176  4.05815E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49507E-01 0.00045  7.46196E-04 0.01232  9.47741E-04 0.08328  3.10961E-01 0.00152 ];
INF_SP1                   (idx, [1:   8]) = [  2.00017E-02 0.00423 -1.60767E-04 0.02809  2.44669E-04 0.30013  1.73174E-02 0.01931 ];
INF_SP2                   (idx, [1:   8]) = [  4.16461E-03 0.02690 -2.13236E-05 0.11086  5.09626E-05 1.00000  1.78891E-03 0.20315 ];
INF_SP3                   (idx, [1:   8]) = [  8.53860E-04 0.13115 -4.77533E-06 0.94362  3.26926E-06 1.00000 -1.22792E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.92119E-04 0.44822 -6.34985E-06 0.49633  9.36218E-06 1.00000  7.77414E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [ -4.29392E-05 1.00000  1.37090E-07 1.00000 -1.97496E-05 1.00000 -7.83749E-04 0.78655 ];
INF_SP6                   (idx, [1:   8]) = [  1.83190E-04 0.29819 -4.27574E-06 0.89397 -5.93453E-05 0.41098  2.47076E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.64748E-05 1.00000 -2.62057E-07 1.00000 -6.41471E-05 0.30176  4.05815E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.21116E-01 0.00356 -1.90449E-01 0.02663 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.98887E-01 0.01283 -1.07212E-01 0.02200 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.01436E-01 0.00532 -1.11275E-01 0.01364 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.20045E-01 0.00251  3.94260E-01 0.05646 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.91567E-01 0.00358 -1.75271E+00 0.02630 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.68372E-01 0.01276 -3.11205E+00 0.02156 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.64795E-01 0.00530 -2.99669E+00 0.01369 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.04153E+00 0.00250  8.50627E-01 0.05373 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.56935E-03 0.04029  2.28551E-04 0.19813  1.15966E-03 0.11684  8.36025E-04 0.12439  1.27830E-03 0.11055  1.76520E-03 0.07404  6.16266E-04 0.13957  5.09162E-04 0.14437  1.76190E-04 0.27912 ];
LAMBDA                    (idx, [1:  18]) = [  3.97225E-01 0.06397  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87307E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29228 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70721E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '5' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.72244E+03 0.02476  2.76854E+04 0.00094  6.27014E+04 0.01041  1.05835E+05 0.00481  1.11057E+05 0.00515  1.00195E+05 0.00358  9.04209E+04 0.00396  7.77644E+04 0.00703  6.82230E+04 0.00313  6.08676E+04 0.00912  5.54999E+04 0.00273  5.12864E+04 0.00283  4.70404E+04 0.00932  4.50185E+04 0.00455  4.33991E+04 0.00730  3.59774E+04 0.00345  3.56288E+04 0.00655  3.37550E+04 0.01484  3.31378E+04 0.00474  6.15582E+04 0.00647  5.41528E+04 0.01152  3.67226E+04 0.00625  2.16982E+04 0.01370  2.33002E+04 0.02650  2.06256E+04 0.01774  1.65088E+04 0.00650  2.66434E+04 0.00426  5.33708E+03 0.03159  7.02135E+03 0.00502  6.17124E+03 0.00500  3.43283E+03 0.01691  6.00290E+03 0.00802  3.84194E+03 0.03161  3.30374E+03 0.02705  5.41423E+02 0.04471  5.63896E+02 0.06168  5.41673E+02 0.02898  6.00997E+02 0.09572  5.32145E+02 0.07296  5.52165E+02 0.05283  5.79305E+02 0.00851  5.78960E+02 0.06355  1.05578E+03 0.03425  1.83604E+03 0.03915  2.18663E+03 0.04917  5.32109E+03 0.00712  5.02484E+03 0.02177  4.71064E+03 0.01139  2.52866E+03 0.01409  1.55922E+03 0.02473  1.12741E+03 0.00879  1.11421E+03 0.04596  1.92326E+03 0.01888  2.06791E+03 0.02658  3.32122E+03 0.03172  4.21921E+03 0.00530  6.08247E+03 0.03102  4.16071E+03 0.02248  3.20161E+03 0.02409  2.55097E+03 0.01381  2.18619E+03 0.01541  2.46940E+03 0.03006  2.04950E+03 0.00833  1.47254E+03 0.02560  1.35778E+03 0.03797  1.25374E+03 0.02977  9.47204E+02 0.05356  8.05695E+02 0.05313  4.49946E+02 0.10481  1.14781E+02 0.11073 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.75697E+00 0.01009 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.40177E+00 0.00104  5.66728E-02 0.00608 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55661E-01 0.00174  4.28527E-01 0.00127 ];
INF_CAPT                  (idx, [1:   4]) = [  1.95638E-03 0.00169  1.83509E-02 0.00150 ];
INF_ABS                   (idx, [1:   4]) = [  4.77529E-03 0.00103  1.18852E-01 0.00126 ];
INF_FISS                  (idx, [1:   4]) = [  2.81891E-03 0.00280  1.00501E-01 0.00154 ];
INF_NSF                   (idx, [1:   4]) = [  6.89065E-03 0.00279  2.44835E-01 0.00154 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44444E+00 1.3E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 1.0E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.25621E-08 0.00670  2.32254E-06 0.00346 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50921E-01 0.00181  3.09339E-01 0.00470 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97352E-02 0.00607  1.87100E-02 0.03998 ];
INF_SCATT2                (idx, [1:   4]) = [  4.07666E-03 0.03655  1.14340E-03 0.75762 ];
INF_SCATT3                (idx, [1:   4]) = [  8.99861E-04 0.05296  3.05739E-05 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  6.42386E-05 0.75119  1.35325E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.81261E-04 0.86732  6.93698E-04 0.21955 ];
INF_SCATT6                (idx, [1:   4]) = [ -5.84471E-05 0.18517  1.03902E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  4.14255E-05 1.00000 -5.65818E-04 0.29414 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50922E-01 0.00181  3.09339E-01 0.00470 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97352E-02 0.00606  1.87100E-02 0.03998 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.07651E-03 0.03653  1.14340E-03 0.75762 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.99783E-04 0.05282  3.05739E-05 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  6.40774E-05 0.75335  1.35325E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.81327E-04 0.86690  6.93698E-04 0.21955 ];
INF_SCATTP6               (idx, [1:   4]) = [ -5.83042E-05 0.18400  1.03902E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.15069E-05 1.00000 -5.65818E-04 0.29414 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04064E-01 0.00235  3.94808E-01 0.00406 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63349E+00 0.00234  8.44320E-01 0.00405 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.77434E-03 0.00103  1.18852E-01 0.00126 ];
INF_REMXS                 (idx, [1:   4]) = [  5.57073E-03 0.01386  1.19969E-01 0.00865 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50090E-01 0.00180  8.30613E-04 0.01514  7.80816E-04 0.10812  3.08558E-01 0.00444 ];
INF_S1                    (idx, [1:   8]) = [  1.99302E-02 0.00532 -1.95008E-04 0.07093  2.02169E-04 0.22051  1.85078E-02 0.03841 ];
INF_S2                    (idx, [1:   8]) = [  4.09551E-03 0.03713 -1.88541E-05 0.17872  6.73784E-05 0.23591  1.07603E-03 0.80836 ];
INF_S3                    (idx, [1:   8]) = [  9.22130E-04 0.05352 -2.22687E-05 0.21041 -9.29635E-07 1.00000  3.15036E-05 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  6.85541E-05 0.67486 -4.31554E-06 0.46134 -1.73378E-05 1.00000  1.52663E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.71039E-04 0.92007  1.02223E-05 0.04496 -1.27790E-05 0.62614  7.06477E-04 0.22554 ];
INF_S6                    (idx, [1:   8]) = [ -5.49947E-05 0.20272 -3.45237E-06 0.36458 -3.04633E-06 1.00000  1.06948E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  4.56484E-05 0.98804 -4.22295E-06 0.63973 -1.62725E-05 1.00000 -5.49546E-04 0.31996 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50091E-01 0.00180  8.30613E-04 0.01514  7.80816E-04 0.10812  3.08558E-01 0.00444 ];
INF_SP1                   (idx, [1:   8]) = [  1.99302E-02 0.00531 -1.95008E-04 0.07093  2.02169E-04 0.22051  1.85078E-02 0.03841 ];
INF_SP2                   (idx, [1:   8]) = [  4.09536E-03 0.03711 -1.88541E-05 0.17872  6.73784E-05 0.23591  1.07603E-03 0.80836 ];
INF_SP3                   (idx, [1:   8]) = [  9.22052E-04 0.05338 -2.22687E-05 0.21041 -9.29635E-07 1.00000  3.15036E-05 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  6.83929E-05 0.67670 -4.31554E-06 0.46134 -1.73378E-05 1.00000  1.52663E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.71105E-04 0.91961  1.02223E-05 0.04496 -1.27790E-05 0.62614  7.06477E-04 0.22554 ];
INF_SP6                   (idx, [1:   8]) = [ -5.48518E-05 0.20145 -3.45237E-06 0.36458 -3.04633E-06 1.00000  1.06948E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  4.57298E-05 0.98699 -4.22295E-06 0.63973 -1.62725E-05 1.00000 -5.49546E-04 0.31996 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.31426E-01 0.00726 -1.89867E-01 0.01963 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.94921E-01 0.01293 -1.11278E-01 0.04175 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.15387E-01 0.01422 -1.07090E-01 0.02196 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.34235E-01 0.00666  3.92964E-01 0.04147 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.72714E-01 0.00729 -1.75697E+00 0.01967 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.73732E-01 0.01282 -3.00636E+00 0.04330 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.47021E-01 0.01402 -3.11571E+00 0.02246 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.97390E-01 0.00666  8.51154E-01 0.04113 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.98407E-03 0.04240  1.84749E-04 0.32081  1.15617E-03 0.10409  6.95544E-04 0.12294  1.35594E-03 0.09318  2.07554E-03 0.07345  6.81817E-04 0.13426  6.65098E-04 0.18039  1.69207E-04 0.24579 ];
LAMBDA                    (idx, [1:  18]) = [  4.20083E-01 0.06487  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87307E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29228 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70721E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '4' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.05989E+04 0.01005  5.27887E+04 0.00960  1.19986E+05 0.00732  2.05756E+05 0.00593  2.11643E+05 0.00228  1.93609E+05 0.00713  1.72730E+05 0.00288  1.50942E+05 0.00381  1.33879E+05 0.00565  1.19776E+05 0.00474  1.08224E+05 0.00399  1.00109E+05 0.00348  9.40270E+04 0.00616  8.84632E+04 0.00587  8.45543E+04 0.00501  7.27324E+04 0.00816  7.11614E+04 0.00205  6.93009E+04 0.00370  6.65068E+04 0.00314  1.24491E+05 0.00252  1.10276E+05 0.00448  7.37030E+04 0.00178  4.49594E+04 0.00577  4.82266E+04 0.00992  4.21875E+04 0.00960  3.39931E+04 0.00645  5.55033E+04 0.01118  1.14303E+04 0.01021  1.38365E+04 0.01951  1.27529E+04 0.01447  7.09803E+03 0.01705  1.24898E+04 0.02001  8.36825E+03 0.03470  6.66149E+03 0.03359  1.40520E+03 0.07043  1.18577E+03 0.02579  1.32421E+03 0.03502  1.32791E+03 0.03827  1.28795E+03 0.01232  1.19853E+03 0.01509  1.19376E+03 0.05627  1.13853E+03 0.03174  2.25556E+03 0.05661  3.41233E+03 0.01543  4.33396E+03 0.01685  1.10624E+04 0.01230  1.07345E+04 0.02150  1.00799E+04 0.01061  5.39335E+03 0.02763  3.44149E+03 0.01382  2.46942E+03 0.00377  2.53121E+03 0.02802  3.90678E+03 0.02547  4.27182E+03 0.00826  6.92783E+03 0.02783  9.18033E+03 0.02515  1.38182E+04 0.01349  9.66611E+03 0.01303  7.34902E+03 0.01557  5.67966E+03 0.01499  5.40664E+03 0.02387  5.67694E+03 0.02012  5.03859E+03 0.01461  3.62456E+03 0.02495  3.45382E+03 0.03663  3.01320E+03 0.01797  2.47005E+03 0.01928  1.94719E+03 0.01782  1.13309E+03 0.02313  3.10632E+02 0.03769 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.76228E+00 0.00384 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.76259E+00 0.00025  1.27464E-01 0.00629 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55616E-01 0.00054  4.16344E-01 0.00443 ];
INF_CAPT                  (idx, [1:   4]) = [  1.84478E-03 0.00417  1.65891E-02 0.00557 ];
INF_ABS                   (idx, [1:   4]) = [  4.39671E-03 0.00185  1.07248E-01 0.00517 ];
INF_FISS                  (idx, [1:   4]) = [  2.55192E-03 0.00083  9.06593E-02 0.00509 ];
INF_NSF                   (idx, [1:   4]) = [  6.23662E-03 0.00082  2.20859E-01 0.00509 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44389E+00 1.2E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 5.7E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.45284E-08 0.00343  2.39328E-06 0.00115 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.51199E-01 0.00040  3.10014E-01 0.00421 ];
INF_SCATT1                (idx, [1:   4]) = [  1.96672E-02 0.00486  1.86768E-02 0.01749 ];
INF_SCATT2                (idx, [1:   4]) = [  3.80484E-03 0.01876  9.45757E-04 0.38565 ];
INF_SCATT3                (idx, [1:   4]) = [  8.12422E-04 0.10712  3.31187E-04 0.18617 ];
INF_SCATT4                (idx, [1:   4]) = [  1.03770E-04 0.47893  2.96955E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  2.26526E-05 1.00000 -5.85358E-04 0.19811 ];
INF_SCATT6                (idx, [1:   4]) = [  2.30855E-05 1.00000 -4.60040E-04 0.59773 ];
INF_SCATT7                (idx, [1:   4]) = [  6.10583E-05 0.16915  1.31843E-04 0.41817 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.51200E-01 0.00040  3.10014E-01 0.00421 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.96670E-02 0.00486  1.86768E-02 0.01749 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.80491E-03 0.01878  9.45757E-04 0.38565 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.12433E-04 0.10706  3.31187E-04 0.18617 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.03698E-04 0.48002  2.96955E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.28415E-05 1.00000 -5.85358E-04 0.19811 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.29957E-05 1.00000 -4.60040E-04 0.59773 ];
INF_SCATTP7               (idx, [1:   4]) = [  6.11849E-05 0.17006  1.31843E-04 0.41817 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04630E-01 0.00114  3.84862E-01 0.00541 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.62896E+00 0.00114  8.66161E-01 0.00542 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.39526E-03 0.00182  1.07248E-01 0.00517 ];
INF_REMXS                 (idx, [1:   4]) = [  5.27473E-03 0.00891  1.06949E-01 0.00512 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50341E-01 0.00036  8.57571E-04 0.01451  6.19334E-04 0.01707  3.09394E-01 0.00424 ];
INF_S1                    (idx, [1:   8]) = [  1.98627E-02 0.00501 -1.95526E-04 0.02004  1.43314E-04 0.14227  1.85335E-02 0.01712 ];
INF_S2                    (idx, [1:   8]) = [  3.82810E-03 0.01874 -2.32647E-05 0.11608 -3.20071E-05 0.36689  9.77764E-04 0.37958 ];
INF_S3                    (idx, [1:   8]) = [  8.23095E-04 0.10445 -1.06726E-05 0.35341 -3.35969E-05 0.26320  3.64784E-04 0.19042 ];
INF_S4                    (idx, [1:   8]) = [  1.09673E-04 0.43534 -5.90312E-06 0.38810  1.72629E-08 1.00000  2.96782E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  2.34547E-05 1.00000 -8.02123E-07 1.00000  6.24831E-06 0.98197 -5.91606E-04 0.19416 ];
INF_S6                    (idx, [1:   8]) = [  2.21152E-05 1.00000  9.70329E-07 1.00000 -1.37676E-05 0.44664 -4.46273E-04 0.61890 ];
INF_S7                    (idx, [1:   8]) = [  5.92887E-05 0.15888  1.76957E-06 0.97670 -1.57684E-05 0.20306  1.47612E-04 0.35181 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50343E-01 0.00036  8.57571E-04 0.01451  6.19334E-04 0.01707  3.09394E-01 0.00424 ];
INF_SP1                   (idx, [1:   8]) = [  1.98625E-02 0.00501 -1.95526E-04 0.02004  1.43314E-04 0.14227  1.85335E-02 0.01712 ];
INF_SP2                   (idx, [1:   8]) = [  3.82817E-03 0.01876 -2.32647E-05 0.11608 -3.20071E-05 0.36689  9.77764E-04 0.37958 ];
INF_SP3                   (idx, [1:   8]) = [  8.23106E-04 0.10439 -1.06726E-05 0.35341 -3.35969E-05 0.26320  3.64784E-04 0.19042 ];
INF_SP4                   (idx, [1:   8]) = [  1.09601E-04 0.43633 -5.90312E-06 0.38810  1.72629E-08 1.00000  2.96782E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  2.36437E-05 1.00000 -8.02123E-07 1.00000  6.24831E-06 0.98197 -5.91606E-04 0.19416 ];
INF_SP6                   (idx, [1:   8]) = [  2.20254E-05 1.00000  9.70329E-07 1.00000 -1.37676E-05 0.44664 -4.46273E-04 0.61890 ];
INF_SP7                   (idx, [1:   8]) = [  5.94154E-05 0.15983  1.76957E-06 0.97670 -1.57684E-05 0.20306  1.47612E-04 0.35181 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.38824E-01 0.00889 -1.99193E-01 0.02614 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.18571E-01 0.01019 -1.13071E-01 0.02125 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.14275E-01 0.01315 -1.16029E-01 0.01652 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.37456E-01 0.00667  4.23800E-01 0.08874 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.59728E-01 0.00897 -1.67565E+00 0.02551 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.42926E-01 0.01020 -2.95066E+00 0.02127 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.48388E-01 0.01331 -2.87440E+00 0.01632 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.87870E-01 0.00669  7.98094E-01 0.08162 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.54966E-03 0.03402  1.59497E-04 0.19020  1.05864E-03 0.08984  7.04023E-04 0.10491  1.25712E-03 0.07770  1.99008E-03 0.05779  6.35380E-04 0.11241  5.64855E-04 0.09727  1.80057E-04 0.17390 ];
LAMBDA                    (idx, [1:  18]) = [  4.38121E-01 0.04750  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87307E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29228 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70721E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '3' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.02307E+03 0.01315  9.50317E+03 0.01623  2.27775E+04 0.00876  3.84676E+04 0.01633  4.00576E+04 0.00336  3.64943E+04 0.01288  3.30013E+04 0.00743  2.94026E+04 0.01118  2.56690E+04 0.00664  2.35086E+04 0.00806  2.13785E+04 0.00784  1.97013E+04 0.00881  1.79684E+04 0.01280  1.75149E+04 0.01447  1.69707E+04 0.00707  1.42668E+04 0.01700  1.39629E+04 0.01153  1.33911E+04 0.02743  1.30169E+04 0.01246  2.47197E+04 0.01061  2.21084E+04 0.01065  1.46048E+04 0.01572  8.95949E+03 0.02324  9.49829E+03 0.00424  8.89156E+03 0.02494  6.99361E+03 0.00862  1.16944E+04 0.00486  2.38989E+03 0.00946  2.90025E+03 0.02722  2.66128E+03 0.04998  1.50726E+03 0.08781  2.54064E+03 0.05150  1.70262E+03 0.01949  1.37934E+03 0.00670  3.24999E+02 0.15910  2.19842E+02 0.10629  2.42826E+02 0.10614  2.68542E+02 0.09198  2.21248E+02 0.06726  2.57769E+02 0.05847  2.75285E+02 0.15767  2.89077E+02 0.08343  5.14707E+02 0.16066  7.19894E+02 0.09959  9.69786E+02 0.06099  2.36723E+03 0.00909  2.35319E+03 0.02235  2.18755E+03 0.04320  1.26902E+03 0.05510  7.96403E+02 0.02247  5.71291E+02 0.03041  5.93004E+02 0.09767  8.75624E+02 0.05057  1.01855E+03 0.05629  1.55674E+03 0.06461  2.17324E+03 0.04699  3.33239E+03 0.02112  2.27723E+03 0.02011  1.72089E+03 0.05218  1.52708E+03 0.01134  1.48411E+03 0.04018  1.42640E+03 0.04902  1.36497E+03 0.05892  9.90156E+02 0.09644  8.62760E+02 0.00286  9.32675E+02 0.05256  7.68530E+02 0.05580  6.39684E+02 0.05100  3.75351E+02 0.05571  9.03468E+01 0.00924 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.79486E+00 0.00664 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  5.38047E-01 0.00241  3.11722E-02 0.01841 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56344E-01 0.00263  4.11208E-01 0.00987 ];
INF_CAPT                  (idx, [1:   4]) = [  1.77115E-03 0.00555  1.55719E-02 0.00936 ];
INF_ABS                   (idx, [1:   4]) = [  4.09836E-03 0.00516  1.00311E-01 0.00907 ];
INF_FISS                  (idx, [1:   4]) = [  2.32721E-03 0.00491  8.47390E-02 0.00902 ];
INF_NSF                   (idx, [1:   4]) = [  5.68603E-03 0.00491  2.06436E-01 0.00902 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44328E+00 1.8E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 2.7E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.72729E-08 0.00962  2.52686E-06 0.00313 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.52278E-01 0.00273  3.11673E-01 0.01056 ];
INF_SCATT1                (idx, [1:   4]) = [  2.00843E-02 0.00971  1.71663E-02 0.07347 ];
INF_SCATT2                (idx, [1:   4]) = [  3.58067E-03 0.00641  3.42076E-03 0.45566 ];
INF_SCATT3                (idx, [1:   4]) = [  5.52196E-04 0.10584  8.88720E-04 0.43201 ];
INF_SCATT4                (idx, [1:   4]) = [  3.14365E-04 0.47161  2.38418E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  2.12020E-04 0.44828  1.24953E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  3.93549E-04 0.22676 -1.09708E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.69901E-04 0.41698 -3.75115E-04 0.60231 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.52279E-01 0.00273  3.11673E-01 0.01056 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.00847E-02 0.00970  1.71663E-02 0.07347 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.58087E-03 0.00639  3.42076E-03 0.45566 ];
INF_SCATTP3               (idx, [1:   4]) = [  5.52133E-04 0.10575  8.88720E-04 0.43201 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.14136E-04 0.47202  2.38418E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.11765E-04 0.44803  1.24953E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.93396E-04 0.22681 -1.09708E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.69908E-04 0.41700 -3.75115E-04 0.60231 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.05668E-01 0.00361  3.81187E-01 0.00616 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.62078E+00 0.00360  8.74529E-01 0.00619 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.09774E-03 0.00512  1.00311E-01 0.00907 ];
INF_REMXS                 (idx, [1:   4]) = [  5.02483E-03 0.01191  1.00236E-01 0.00731 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51319E-01 0.00276  9.59120E-04 0.01003  7.01571E-04 0.11512  3.10972E-01 0.01074 ];
INF_S1                    (idx, [1:   8]) = [  2.03064E-02 0.00963 -2.22089E-04 0.01152  2.09943E-04 0.15712  1.69563E-02 0.07491 ];
INF_S2                    (idx, [1:   8]) = [  3.60035E-03 0.00531 -1.96831E-05 0.21917  1.68032E-06 1.00000  3.41908E-03 0.44530 ];
INF_S3                    (idx, [1:   8]) = [  5.63756E-04 0.08903 -1.15602E-05 0.98284  1.38176E-05 1.00000  8.74902E-04 0.46073 ];
INF_S4                    (idx, [1:   8]) = [  3.23574E-04 0.43535 -9.20884E-06 0.99892 -1.59113E-05 1.00000  2.54329E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  2.04878E-04 0.48707  7.14164E-06 0.86760 -8.28510E-05 0.36642  2.07804E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  3.97108E-04 0.22096 -3.55870E-06 1.00000 -5.02757E-05 0.49425  3.93049E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.75751E-04 0.41708 -5.85064E-06 0.65503 -5.44887E-05 0.20938 -3.20626E-04 0.73343 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51320E-01 0.00276  9.59120E-04 0.01003  7.01571E-04 0.11512  3.10972E-01 0.01074 ];
INF_SP1                   (idx, [1:   8]) = [  2.03068E-02 0.00961 -2.22089E-04 0.01152  2.09943E-04 0.15712  1.69563E-02 0.07491 ];
INF_SP2                   (idx, [1:   8]) = [  3.60055E-03 0.00530 -1.96831E-05 0.21917  1.68032E-06 1.00000  3.41908E-03 0.44530 ];
INF_SP3                   (idx, [1:   8]) = [  5.63693E-04 0.08894 -1.15602E-05 0.98284  1.38176E-05 1.00000  8.74902E-04 0.46073 ];
INF_SP4                   (idx, [1:   8]) = [  3.23345E-04 0.43574 -9.20884E-06 0.99892 -1.59113E-05 1.00000  2.54329E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  2.04623E-04 0.48682  7.14164E-06 0.86760 -8.28510E-05 0.36642  2.07804E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  3.96954E-04 0.22104 -3.55870E-06 1.00000 -5.02757E-05 0.49425  3.93049E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.75759E-04 0.41710 -5.85064E-06 0.65503 -5.44887E-05 0.20938 -3.20626E-04 0.73343 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.18263E-01 0.02533 -2.17418E-01 0.03191 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.77166E-01 0.04040 -1.26222E-01 0.01960 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.45380E-01 0.02220 -1.29185E-01 0.01573 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.53874E-01 0.03122  5.87529E-01 0.21773 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.97953E-01 0.02487 -1.53637E+00 0.03294 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.00869E-01 0.04066 -2.64286E+00 0.01925 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.49178E-01 0.02267 -2.58153E+00 0.01549 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.43811E-01 0.03153  6.15268E-01 0.17888 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  8.24240E-03 0.07104  2.35937E-04 0.33434  1.45908E-03 0.19750  8.82748E-04 0.20522  1.11120E-03 0.14856  2.96828E-03 0.14029  5.65834E-04 0.29118  8.75027E-04 0.20681  1.44290E-04 0.30630 ];
LAMBDA                    (idx, [1:  18]) = [  3.98445E-01 0.09101  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87308E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29227 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70719E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '2' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.51400E+04 0.01025  7.23634E+04 0.00735  1.64107E+05 0.00828  2.89931E+05 0.00176  3.00909E+05 0.00214  2.77556E+05 0.00378  2.51235E+05 0.00102  2.20637E+05 0.00410  1.96369E+05 0.00151  1.76471E+05 0.00543  1.63502E+05 0.00146  1.52320E+05 0.00451  1.41715E+05 0.01116  1.36192E+05 0.00321  1.31808E+05 0.00430  1.11277E+05 0.00159  1.10967E+05 0.00129  1.06543E+05 0.00096  1.01691E+05 0.00938  1.93465E+05 0.00402  1.75104E+05 0.00636  1.18544E+05 0.00538  7.35234E+04 0.00533  8.03095E+04 0.01126  7.15591E+04 0.00300  5.73861E+04 0.00701  9.55462E+04 0.00328  1.96203E+04 0.01120  2.29413E+04 0.00464  2.15936E+04 0.01324  1.22362E+04 0.00789  2.03404E+04 0.01551  1.37187E+04 0.00272  1.15424E+04 0.02377  2.15903E+03 0.07278  2.04808E+03 0.01241  2.17952E+03 0.03412  2.41952E+03 0.02135  2.22445E+03 0.02466  2.20561E+03 0.01974  2.14491E+03 0.04230  2.00668E+03 0.00973  3.86260E+03 0.00646  6.28333E+03 0.02693  7.47302E+03 0.02700  1.91986E+04 0.01095  1.85416E+04 0.01527  1.81576E+04 0.01227  9.95594E+03 0.01400  6.36622E+03 0.01094  4.51116E+03 0.01263  4.88016E+03 0.01531  7.80564E+03 0.01505  8.93233E+03 0.00751  1.41123E+04 0.01197  1.90524E+04 0.00198  3.00215E+04 0.01079  2.16098E+04 0.00368  1.79918E+04 0.01165  1.42116E+04 0.00755  1.39865E+04 0.01868  1.48146E+04 0.00263  1.33723E+04 0.01062  9.61325E+03 0.00889  9.44270E+03 0.01992  8.73849E+03 0.02226  7.54458E+03 0.01778  6.12386E+03 0.02601  3.96347E+03 0.01738  1.23183E+03 0.01827 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.79249E+00 0.00316 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.16041E+00 0.00061  2.84848E-01 0.00210 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56269E-01 0.00072  4.00721E-01 0.00212 ];
INF_CAPT                  (idx, [1:   4]) = [  1.66160E-03 0.00090  1.42817E-02 0.00213 ];
INF_ABS                   (idx, [1:   4]) = [  3.77395E-03 0.00113  9.12690E-02 0.00232 ];
INF_FISS                  (idx, [1:   4]) = [  2.11235E-03 0.00142  7.69873E-02 0.00236 ];
INF_NSF                   (idx, [1:   4]) = [  5.15990E-03 0.00142  1.87552E-01 0.00236 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44273E+00 4.8E-06  2.43614E+00 1.3E-08 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 4.0E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.95175E-08 0.00100  2.63802E-06 0.00085 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.52488E-01 0.00077  3.09257E-01 0.00279 ];
INF_SCATT1                (idx, [1:   4]) = [  1.95141E-02 0.00623  1.81090E-02 0.01898 ];
INF_SCATT2                (idx, [1:   4]) = [  3.69364E-03 0.01445  1.45846E-03 0.06558 ];
INF_SCATT3                (idx, [1:   4]) = [  7.16724E-04 0.09789  4.00510E-04 0.45274 ];
INF_SCATT4                (idx, [1:   4]) = [  7.42577E-05 0.60794  4.51459E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  3.76379E-05 1.00000 -4.11799E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  3.31295E-05 0.90659 -7.47794E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.62974E-05 0.73921 -3.24124E-04 0.36179 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.52489E-01 0.00077  3.09257E-01 0.00279 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.95142E-02 0.00623  1.81090E-02 0.01898 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.69359E-03 0.01446  1.45846E-03 0.06558 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.16651E-04 0.09783  4.00510E-04 0.45274 ];
INF_SCATTP4               (idx, [1:   4]) = [  7.42407E-05 0.60874  4.51459E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  3.76555E-05 1.00000 -4.11799E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.31405E-05 0.90750 -7.47794E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.63038E-05 0.73808 -3.24124E-04 0.36179 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.06746E-01 0.00155  3.71728E-01 0.00093 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.61229E+00 0.00155  8.96714E-01 0.00093 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  3.77371E-03 0.00112  9.12690E-02 0.00232 ];
INF_REMXS                 (idx, [1:   4]) = [  4.77506E-03 0.00133  9.19771E-02 0.00097 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51494E-01 0.00073  9.94616E-04 0.01045  5.12461E-04 0.02348  3.08744E-01 0.00276 ];
INF_S1                    (idx, [1:   8]) = [  1.97430E-02 0.00569 -2.28814E-04 0.04006  1.21847E-04 0.06245  1.79871E-02 0.01921 ];
INF_S2                    (idx, [1:   8]) = [  3.71658E-03 0.01368 -2.29321E-05 0.11048 -1.11298E-05 0.17901  1.46959E-03 0.06573 ];
INF_S3                    (idx, [1:   8]) = [  7.27884E-04 0.10052 -1.11605E-05 0.28052  3.51673E-07 1.00000  4.00158E-04 0.45470 ];
INF_S4                    (idx, [1:   8]) = [  8.18301E-05 0.55608 -7.57242E-06 0.24607  8.20713E-06 0.61534  3.69388E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  4.28603E-05 1.00000 -5.22240E-06 0.37362 -1.53622E-05 0.13220 -2.58177E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  3.22424E-05 0.96115  8.87122E-07 1.00000 -1.61751E-05 0.31201 -5.86043E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.51691E-05 0.76608  1.12829E-06 0.59171 -9.83058E-06 0.34245 -3.14293E-04 0.36518 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51494E-01 0.00073  9.94616E-04 0.01045  5.12461E-04 0.02348  3.08744E-01 0.00276 ];
INF_SP1                   (idx, [1:   8]) = [  1.97431E-02 0.00569 -2.28814E-04 0.04006  1.21847E-04 0.06245  1.79871E-02 0.01921 ];
INF_SP2                   (idx, [1:   8]) = [  3.71653E-03 0.01369 -2.29321E-05 0.11048 -1.11298E-05 0.17901  1.46959E-03 0.06573 ];
INF_SP3                   (idx, [1:   8]) = [  7.27811E-04 0.10046 -1.11605E-05 0.28052  3.51673E-07 1.00000  4.00158E-04 0.45470 ];
INF_SP4                   (idx, [1:   8]) = [  8.18131E-05 0.55681 -7.57242E-06 0.24607  8.20713E-06 0.61534  3.69388E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  4.28779E-05 1.00000 -5.22240E-06 0.37362 -1.53622E-05 0.13220 -2.58177E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  3.22533E-05 0.96208  8.87122E-07 1.00000 -1.61751E-05 0.31201 -5.86043E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.51755E-05 0.76486  1.12829E-06 0.59171 -9.83058E-06 0.34245 -3.14293E-04 0.36518 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.21765E-01 0.00202 -2.43204E-01 0.00445 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.57645E-01 0.00552 -1.38388E-01 0.01244 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.58489E-01 0.00770 -1.40007E-01 0.01151 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.64121E-01 0.00444  4.91503E-01 0.02379 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.90336E-01 0.00202 -1.37065E+00 0.00446 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.28412E-01 0.00549 -2.40943E+00 0.01259 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.27112E-01 0.00765 -2.38145E+00 0.01141 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.15484E-01 0.00446  6.78942E-01 0.02324 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.67890E-03 0.03244  2.04075E-04 0.17926  1.03049E-03 0.07461  7.66290E-04 0.09062  1.36462E-03 0.06907  2.00430E-03 0.04915  5.44710E-04 0.11196  5.99561E-04 0.10049  1.64845E-04 0.17880 ];
LAMBDA                    (idx, [1:  18]) = [  4.21400E-01 0.04987  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87308E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29227 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70719E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'g' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.88620E+04 0.00364  3.70334E+05 0.00076  7.63715E+05 0.00086  1.59882E+06 0.00067  1.81770E+06 0.00110  1.74367E+06 0.00243  1.67131E+06 0.00129  1.58964E+06 0.00162  1.51116E+06 0.00144  1.47042E+06 0.00168  1.44763E+06 0.00226  1.41990E+06 0.00127  1.39863E+06 0.00131  1.38074E+06 0.00161  1.37992E+06 0.00102  1.20609E+06 0.00116  1.20773E+06 0.00156  1.19463E+06 0.00186  1.17868E+06 0.00091  2.30296E+06 0.00157  2.22015E+06 0.00160  1.59907E+06 0.00154  1.03164E+06 0.00192  1.21503E+06 0.00197  1.16493E+06 0.00196  9.74997E+05 0.00145  1.74183E+06 0.00124  3.55104E+05 0.00242  4.33807E+05 0.00257  3.84158E+05 0.00200  2.20775E+05 0.00370  3.74570E+05 0.00093  2.49785E+05 0.00190  2.12920E+05 0.00237  4.13221E+04 0.00217  4.05409E+04 0.00082  4.16379E+04 0.00482  4.21090E+04 0.00229  4.15684E+04 0.00798  4.10612E+04 0.00161  4.19958E+04 0.00416  3.93313E+04 0.00175  7.39857E+04 0.00639  1.16449E+05 0.00456  1.46594E+05 0.00111  3.77435E+05 0.00233  3.78655E+05 0.00206  3.80070E+05 0.00375  2.28863E+05 0.00286  1.54135E+05 0.00337  1.12119E+05 0.00249  1.21330E+05 0.00111  2.03147E+05 0.00374  2.38315E+05 0.00181  4.13636E+05 0.00068  6.32847E+05 0.00085  1.16896E+06 0.00151  9.92117E+05 0.00169  8.72221E+05 0.00054  7.31977E+05 0.00168  7.44788E+05 0.00116  8.30599E+05 0.00146  7.85521E+05 0.00128  5.83030E+05 0.00181  5.88511E+05 0.00086  5.74767E+05 0.00138  5.33507E+05 0.00374  4.54491E+05 0.00442  3.27464E+05 0.00212  1.28207E+05 0.00477 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.99366E+01 0.00109  1.21736E+01 0.00107 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.59752E-01 5.8E-05  5.29173E-01 4.2E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.54686E-05 0.00682  2.94462E-04 0.00066 ];
INF_ABS                   (idx, [1:   4]) = [  1.54686E-05 0.00682  2.94462E-04 0.00066 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  8.72437E-08 0.00023  3.23625E-06 0.00066 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.59736E-01 5.7E-05  5.28878E-01 4.5E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  2.99264E-02 0.00146  2.83186E-02 0.00172 ];
INF_SCATT2                (idx, [1:   4]) = [  2.81596E-03 0.01240  1.83929E-03 0.02500 ];
INF_SCATT3                (idx, [1:   4]) = [  4.63593E-04 0.02569  2.17740E-04 0.11981 ];
INF_SCATT4                (idx, [1:   4]) = [  4.08482E-05 0.10526  9.83196E-05 0.21905 ];
INF_SCATT5                (idx, [1:   4]) = [  1.82138E-05 1.00000 -5.16383E-06 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  4.88852E-05 0.23114  3.72009E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.67923E-05 0.81199  5.24413E-05 0.97585 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.59736E-01 5.7E-05  5.28878E-01 4.5E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.99264E-02 0.00146  2.83186E-02 0.00172 ];
INF_SCATTP2               (idx, [1:   4]) = [  2.81596E-03 0.01240  1.83929E-03 0.02500 ];
INF_SCATTP3               (idx, [1:   4]) = [  4.63593E-04 0.02569  2.17740E-04 0.11981 ];
INF_SCATTP4               (idx, [1:   4]) = [  4.08482E-05 0.10526  9.83196E-05 0.21905 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.82138E-05 1.00000 -5.16383E-06 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.88852E-05 0.23114  3.72009E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.67923E-05 0.81199  5.24413E-05 0.97585 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  3.92036E-01 0.00027  4.99772E-01 0.00013 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  8.50262E-01 0.00027  6.66971E-01 0.00013 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.54686E-05 0.00682  2.94462E-04 0.00066 ];
INF_REMXS                 (idx, [1:   4]) = [  3.37278E-03 0.00029  7.01876E-04 0.01061 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.56379E-01 5.7E-05  3.35683E-03 0.00030  4.06656E-04 0.00103  5.28471E-01 4.6E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.08487E-02 0.00150 -9.22303E-04 0.00302  1.10763E-04 0.01573  2.82078E-02 0.00167 ];
INF_S2                    (idx, [1:   8]) = [  2.88552E-03 0.01256 -6.95516E-05 0.01930  2.10649E-06 0.13083  1.83719E-03 0.02518 ];
INF_S3                    (idx, [1:   8]) = [  4.70909E-04 0.02423 -7.31576E-06 0.22176 -1.34260E-05 0.07752  2.31166E-04 0.11735 ];
INF_S4                    (idx, [1:   8]) = [  4.64916E-05 0.10803 -5.64341E-06 0.23883 -1.07760E-05 0.06926  1.09096E-04 0.20139 ];
INF_S5                    (idx, [1:   8]) = [  2.03899E-05 1.00000 -2.17616E-06 0.52877 -6.67898E-06 0.10874  1.51515E-06 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  4.95637E-05 0.24100 -6.78523E-07 1.00000 -3.42041E-06 0.07943  4.06213E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.85355E-05 0.78652 -1.74325E-06 0.82817 -1.63703E-06 0.29079  5.40783E-05 0.93957 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.56379E-01 5.7E-05  3.35683E-03 0.00030  4.06656E-04 0.00103  5.28471E-01 4.6E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.08487E-02 0.00150 -9.22303E-04 0.00302  1.10763E-04 0.01573  2.82078E-02 0.00167 ];
INF_SP2                   (idx, [1:   8]) = [  2.88552E-03 0.01256 -6.95516E-05 0.01930  2.10649E-06 0.13083  1.83719E-03 0.02518 ];
INF_SP3                   (idx, [1:   8]) = [  4.70909E-04 0.02423 -7.31576E-06 0.22176 -1.34260E-05 0.07752  2.31166E-04 0.11735 ];
INF_SP4                   (idx, [1:   8]) = [  4.64916E-05 0.10803 -5.64341E-06 0.23883 -1.07760E-05 0.06926  1.09096E-04 0.20139 ];
INF_SP5                   (idx, [1:   8]) = [  2.03899E-05 1.00000 -2.17616E-06 0.52877 -6.67898E-06 0.10874  1.51515E-06 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  4.95637E-05 0.24100 -6.78523E-07 1.00000 -3.42041E-06 0.07943  4.06213E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.85355E-05 0.78652 -1.74325E-06 0.82817 -1.63703E-06 0.29079  5.40783E-05 0.93957 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.99902E-01 0.00037  5.47839E-01 0.00161 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.72862E-01 0.00059  5.43643E-01 0.00910 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.73765E-01 0.00082  5.42358E-01 0.00442 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.88393E-01 0.00120  5.57948E-01 0.00729 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.66749E+00 0.00037  6.08455E-01 0.00161 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.92832E+00 0.00060  6.13250E-01 0.00913 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.91831E+00 0.00082  6.14624E-01 0.00443 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.15583E+00 0.00120  5.97491E-01 0.00728 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'hetKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:13:30 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938135 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  9.88330E-01  9.16460E-01  9.82713E-01  9.98332E-01  1.07854E+00  1.03562E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 3.8E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89780E-01 0.00056  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10220E-01 0.00013  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47890E-01 0.00014  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52403E-01 0.00014  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98318E+00 0.00065  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31500E-01 1.1E-05  6.77355E-02 0.00015  7.64996E-04 0.00102  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37595E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34751E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82689E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42312E+01 0.00066  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000202 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00034E+04 0.00081 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.37934E+01 ;
RUNNING_TIME              (idx, 1)        =  7.87308E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.11000E-01  1.11000E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  7.96667E-03  7.96667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  7.75403E+00  7.75403E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  7.87288E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 4.29227 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.70724E+00 0.02934 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70719E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1443.03 ;
MEMSIZE                   (idx, 1)        = 1362.84 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 359.12 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  1.99906E-05 0.00043  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37439E-02 0.00455 ];
U235_FISS                 (idx, [1:   4]) = [  4.39471E-01 0.00077  9.99226E-01 2.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.40430E-04 0.02909  7.74023E-04 0.02908 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63531E-01 0.00115  5.93202E-01 0.00080 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43211E-02 0.00441  5.19507E-02 0.00444 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000202 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.63106E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 827215 8.27422E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1319766 1.32005E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 853221 8.53288E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000202 3.00076E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 4.09782E-08 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42572E-11 0.00047 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07546E+00 0.00047 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39931E-01 0.00047 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75774E-01 0.00049 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15704E-01 0.00040 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99531E-01 0.00043 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50674E+02 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84296E-01 0.00101 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34625E+01 0.00056 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06801E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43419E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.22082E-01 0.00126 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.46993E+00 0.00146 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84541E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12088E-01 0.00020 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50329E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07571E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44461E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07597E+00 0.00061  1.06793E+00 0.00061  7.77388E-03 0.00897 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07598E+00 0.00075 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07573E+00 0.00047 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50319E+00 0.00021 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34189E+01 0.00032 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34261E+01 0.00024 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.97517E-05 0.00436 ];
IMP_EALF                  (idx, [1:   2]) = [  2.95317E-05 0.00321 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.67305E-02 0.00407 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.59423E-02 0.00108 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.17041E-03 0.00738  2.07848E-04 0.04106  9.22962E-04 0.01648  6.26334E-04 0.02066  1.23568E-03 0.01696  1.93726E-03 0.01209  5.65095E-04 0.02283  5.24351E-04 0.02699  1.50878E-04 0.04731 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.14588E-01 0.01236  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.24523E-03 0.01087  2.42039E-04 0.05822  1.11565E-03 0.02770  7.50432E-04 0.03197  1.40049E-03 0.02500  2.25385E-03 0.02106  6.81320E-04 0.03818  6.34973E-04 0.03496  1.66471E-04 0.07268 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.14192E-01 0.01879  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 1.9E-09  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 5.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.65011E-05 0.00335  3.65315E-05 0.00335  3.24759E-05 0.05174 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92736E-05 0.00334  3.93063E-05 0.00335  3.49360E-05 0.05159 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.20349E-03 0.00898  2.60235E-04 0.05619  1.07499E-03 0.02675  7.47928E-04 0.02782  1.46467E-03 0.02337  2.21529E-03 0.01860  6.48867E-04 0.03328  6.21541E-04 0.03613  1.69963E-04 0.07406 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.10213E-01 0.01676  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14306E-05 0.04119  3.14535E-05 0.04119  2.62272E-05 0.12527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.38242E-05 0.04118  3.38487E-05 0.04118  2.82361E-05 0.12534 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.48981E-03 0.05675  2.44991E-04 0.16273  9.41674E-04 0.10653  6.31033E-04 0.12006  1.24043E-03 0.09809  2.10408E-03 0.07596  5.05379E-04 0.11114  6.51145E-04 0.13195  1.71081E-04 0.23686 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.40678E-01 0.05833  1.24667E-02 3.3E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.46193E-03 0.05497  2.49564E-04 0.15624  9.87045E-04 0.10280  6.11273E-04 0.11234  1.23545E-03 0.09675  2.04056E-03 0.07412  5.19736E-04 0.10752  6.42273E-04 0.12937  1.76025E-04 0.22242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.41991E-01 0.05769  1.24667E-02 2.7E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.08944E+02 0.04497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57706E-05 0.00160 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.84876E-05 0.00156 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.35512E-03 0.00449 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05696E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23777E-07 0.00177 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83823E-05 0.00057  1.83847E-05 0.00056  1.80560E-05 0.00680 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86466E-04 0.00193  1.86563E-04 0.00195  1.73731E-04 0.02540 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28178E-01 0.00115  2.27977E-01 0.00117  2.61460E-01 0.01446 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32592E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34751E+01 0.00054  5.42316E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '_' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CAPT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_ABS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP1               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP2               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP3               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP4               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP5               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP6               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP7               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_REMXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP1                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP2                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP3                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP4                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP5                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP6                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP7                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

