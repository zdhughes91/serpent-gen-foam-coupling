
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28660E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97383E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82769 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64855E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   2]) = '40' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  4.84009E+05 0.00350  2.30421E+06 0.00180  5.18984E+06 0.00082  9.36821E+06 0.00077  9.93477E+06 0.00104  9.23502E+06 0.00120  8.43166E+06 0.00029  7.49220E+06 0.00040  6.68751E+06 0.00051  6.04911E+06 0.00040  5.57372E+06 0.00054  5.19510E+06 0.00066  4.83862E+06 0.00033  4.64687E+06 0.00055  4.47017E+06 0.00011  3.80284E+06 0.00011  3.73449E+06 0.00053  3.58281E+06 0.00092  3.41639E+06 0.00051  6.33777E+06 0.00100  5.55820E+06 0.00125  3.65341E+06 0.00109  2.20161E+06 0.00161  2.35980E+06 0.00159  2.09370E+06 0.00138  1.66364E+06 0.00103  2.76795E+06 0.00127  5.61937E+05 0.00207  6.92120E+05 0.00101  6.21085E+05 0.00169  3.53528E+05 0.00191  6.09057E+05 0.00297  4.06159E+05 0.00086  3.34820E+05 0.00178  6.22424E+04 0.00348  6.13286E+04 0.00386  6.28055E+04 0.00333  6.42971E+04 0.00275  6.32426E+04 0.00113  6.20814E+04 0.00372  6.35837E+04 0.00375  5.93506E+04 0.00250  1.11755E+05 0.00233  1.75968E+05 0.00274  2.20189E+05 0.00100  5.58048E+05 0.00117  5.47003E+05 0.00163  5.26288E+05 0.00240  3.04233E+05 0.00292  2.00241E+05 0.00272  1.42718E+05 0.00247  1.52089E+05 0.00350  2.50679E+05 0.00317  2.89940E+05 0.00260  4.89690E+05 0.00167  7.26945E+05 0.00093  1.28973E+06 0.00061  1.06887E+06 0.00150  9.27382E+05 0.00179  7.72396E+05 0.00171  7.82861E+05 0.00211  8.70210E+05 0.00231  8.19898E+05 0.00240  6.08895E+05 0.00210  6.11217E+05 0.00200  5.94886E+05 0.00359  5.50208E+05 0.00326  4.67968E+05 0.00385  3.34348E+05 0.00161  1.31322E+05 0.00478 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.48158E+00 0.00093 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36242E+02 0.00058  1.34625E+01 0.00126 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.38177E-01 0.00023  5.25327E-01 4.3E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.85381E-03 0.00066  2.22584E-03 0.00110 ];
INF_ABS                   (idx, [1:   4]) = [  4.15870E-03 0.00062  1.12027E-02 0.00158 ];
INF_FISS                  (idx, [1:   4]) = [  2.30490E-03 0.00060  8.97686E-03 0.00172 ];
INF_NSF                   (idx, [1:   4]) = [  5.64248E-03 0.00059  2.18689E-02 0.00172 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44804E+00 9.0E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 4.3E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.49734E-08 0.00095  3.12494E-06 0.00057 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.34013E-01 0.00024  5.14140E-01 7.2E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  3.71907E-02 0.00029  3.09223E-02 0.00095 ];
INF_SCATT2                (idx, [1:   4]) = [  9.96916E-03 0.00150  3.20340E-03 0.01061 ];
INF_SCATT3                (idx, [1:   4]) = [  9.56035E-04 0.01123  8.18961E-04 0.02678 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.87754E-04 0.01248  2.61330E-04 0.14216 ];
INF_SCATT5                (idx, [1:   4]) = [  8.35049E-05 0.19408  1.93595E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  4.05454E-04 0.01175  5.46778E-05 0.19924 ];
INF_SCATT7                (idx, [1:   4]) = [  4.20334E-05 0.12059  7.82738E-05 0.54752 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.34015E-01 0.00024  5.14140E-01 7.2E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.71908E-02 0.00029  3.09223E-02 0.00095 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.96922E-03 0.00150  3.20340E-03 0.01061 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.56045E-04 0.01119  8.18961E-04 0.02678 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.87726E-04 0.01248  2.61330E-04 0.14216 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.35026E-05 0.19383  1.93595E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.05456E-04 0.01177  5.46778E-05 0.19924 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.20542E-05 0.12070  7.82738E-05 0.54752 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.51614E-01 0.00037  4.92588E-01 9.7E-05 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.32478E+00 0.00037  6.76698E-01 9.7E-05 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.15679E-03 0.00062  1.12027E-02 0.00158 ];
INF_REMXS                 (idx, [1:   4]) = [  5.81780E-03 0.00026  1.16773E-02 0.00151 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.32359E-01 0.00024  1.65441E-03 0.00104  4.89906E-04 0.01247  5.13650E-01 6.5E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.73694E-02 0.00028 -1.78722E-04 0.00483  1.27226E-04 0.01970  3.07951E-02 0.00099 ];
INF_S2                    (idx, [1:   8]) = [  1.00431E-02 0.00145 -7.39038E-05 0.01192  5.81319E-06 0.35447  3.19759E-03 0.01060 ];
INF_S3                    (idx, [1:   8]) = [  1.03884E-03 0.01068 -8.28092E-05 0.00916 -1.06482E-05 0.15416  8.29609E-04 0.02498 ];
INF_S4                    (idx, [1:   8]) = [ -5.60238E-04 0.01417 -2.75164E-05 0.03023 -9.29004E-06 0.16622  2.70620E-04 0.13158 ];
INF_S5                    (idx, [1:   8]) = [  7.66385E-05 0.21072  6.86647E-06 0.14889 -8.52341E-06 0.12672  2.78830E-05 0.95379 ];
INF_S6                    (idx, [1:   8]) = [  3.98863E-04 0.00965  6.59107E-06 0.14482 -4.56140E-06 0.24527  5.92392E-05 0.20121 ];
INF_S7                    (idx, [1:   8]) = [  4.26147E-05 0.13197 -5.81303E-07 1.00000 -1.06371E-06 0.57638  7.93375E-05 0.53564 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.32361E-01 0.00024  1.65441E-03 0.00104  4.89906E-04 0.01247  5.13650E-01 6.5E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.73695E-02 0.00028 -1.78722E-04 0.00483  1.27226E-04 0.01970  3.07951E-02 0.00099 ];
INF_SP2                   (idx, [1:   8]) = [  1.00431E-02 0.00144 -7.39038E-05 0.01192  5.81319E-06 0.35447  3.19759E-03 0.01060 ];
INF_SP3                   (idx, [1:   8]) = [  1.03885E-03 0.01065 -8.28092E-05 0.00916 -1.06482E-05 0.15416  8.29609E-04 0.02498 ];
INF_SP4                   (idx, [1:   8]) = [ -5.60210E-04 0.01416 -2.75164E-05 0.03023 -9.29004E-06 0.16622  2.70620E-04 0.13158 ];
INF_SP5                   (idx, [1:   8]) = [  7.66361E-05 0.21044  6.86647E-06 0.14889 -8.52341E-06 0.12672  2.78830E-05 0.95379 ];
INF_SP6                   (idx, [1:   8]) = [  3.98865E-04 0.00968  6.59107E-06 0.14482 -4.56140E-06 0.24527  5.92392E-05 0.20121 ];
INF_SP7                   (idx, [1:   8]) = [  4.26355E-05 0.13206 -5.81303E-07 1.00000 -1.06371E-06 0.57638  7.93375E-05 0.53564 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.82333E-01 0.00072  7.82029E-01 0.00240 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.86060E-01 0.00021  9.94035E-01 0.00790 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.85507E-01 0.00028  1.01593E+00 0.00504 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.75676E-01 0.00209  5.41883E-01 0.00919 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.18064E+00 0.00072  4.26247E-01 0.00240 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.16526E+00 0.00021  3.35376E-01 0.00794 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.16751E+00 0.00028  3.28122E-01 0.00506 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.20916E+00 0.00210  6.15243E-01 0.00917 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
LAMBDA                    (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28663E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97387E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82772 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64850E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'F' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.71169E+05 0.00725  1.28763E+06 0.00161  2.94111E+06 0.00148  5.18039E+06 0.00149  5.41554E+06 0.00160  5.00257E+06 0.00154  4.52418E+06 0.00139  3.95826E+06 0.00101  3.47145E+06 0.00068  3.06723E+06 0.00081  2.75515E+06 5.0E-05  2.50872E+06 4.4E-05  2.27959E+06 0.00088  2.14743E+06 0.00112  2.02805E+06 0.00120  1.69785E+06 0.00139  1.64488E+06 0.00021  1.54371E+06 0.00085  1.43817E+06 0.00039  2.55784E+06 0.00102  2.06689E+06 0.00085  1.23268E+06 0.00061  6.81365E+05 0.00110  6.43441E+05 0.00149  4.99684E+05 0.00224  3.61337E+05 0.00105  5.15567E+05 0.00126  1.03561E+05 0.00449  1.30016E+05 0.00431  1.22245E+05 0.00321  6.78621E+04 0.00230  1.20105E+05 0.00492  7.98280E+04 0.00324  6.07398E+04 0.00164  1.02559E+04 0.01116  1.00036E+04 0.00815  1.01345E+04 0.00946  1.06964E+04 0.00639  1.03130E+04 0.00873  1.01897E+04 0.00682  1.04559E+04 0.01041  9.76873E+03 0.00873  1.83183E+04 0.00776  2.83072E+04 0.00799  3.53499E+04 0.00389  8.60630E+04 0.00255  7.76676E+04 0.00575  6.55572E+04 0.00531  3.10036E+04 0.00326  1.76555E+04 0.00684  1.15691E+04 0.00585  1.11218E+04 0.01128  1.67136E+04 0.00220  1.72002E+04 0.00105  2.41041E+04 0.00188  2.60809E+04 0.00693  2.76147E+04 0.00674  1.39820E+04 0.00891  8.91656E+03 0.00485  5.99090E+03 0.00439  5.18464E+03 0.01451  4.81007E+03 0.01587  3.72134E+03 0.00497  2.37463E+03 0.01335  2.14167E+03 0.01109  1.84008E+03 0.01972  1.42152E+03 0.02982  1.03014E+03 0.01110  5.81872E+02 0.01870  1.58570E+02 0.03289 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.59320E+00 0.00129 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  6.26677E+01 0.00121  3.78512E-01 0.00185 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56522E-01 0.00019  4.67659E-01 0.00024 ];
INF_CAPT                  (idx, [1:   4]) = [  2.41838E-03 0.00084  2.36615E-02 0.00064 ];
INF_ABS                   (idx, [1:   4]) = [  6.41942E-03 0.00083  1.52931E-01 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  4.00104E-03 0.00082  1.29269E-01 0.00070 ];
INF_NSF                   (idx, [1:   4]) = [  9.79768E-03 0.00082  3.14918E-01 0.00070 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44878E+00 9.5E-06  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 4.0E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.29958E-08 0.00122  1.57971E-06 0.00075 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50095E-01 0.00020  3.15432E-01 0.00070 ];
INF_SCATT1                (idx, [1:   4]) = [  2.06312E-02 0.00038  1.90176E-02 0.00841 ];
INF_SCATT2                (idx, [1:   4]) = [  4.46199E-03 0.00143  1.28803E-03 0.08322 ];
INF_SCATT3                (idx, [1:   4]) = [  9.95976E-04 0.01157  2.40766E-04 0.67089 ];
INF_SCATT4                (idx, [1:   4]) = [  2.16480E-04 0.00932  4.47177E-04 0.43692 ];
INF_SCATT5                (idx, [1:   4]) = [  1.35599E-04 0.12072 -4.95882E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.02101E-04 0.06503 -3.10844E-04 0.58717 ];
INF_SCATT7                (idx, [1:   4]) = [  3.86810E-05 0.24950  1.25018E-04 0.48823 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50098E-01 0.00020  3.15432E-01 0.00070 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.06314E-02 0.00037  1.90176E-02 0.00841 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.46206E-03 0.00141  1.28803E-03 0.08322 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.95963E-04 0.01164  2.40766E-04 0.67089 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.16516E-04 0.00923  4.47177E-04 0.43692 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.35597E-04 0.12050 -4.95882E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.02120E-04 0.06439 -3.10844E-04 0.58717 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.87129E-05 0.24892  1.25018E-04 0.48823 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02612E-01 0.00031  4.30923E-01 0.00066 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64518E+00 0.00031  7.73533E-01 0.00066 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.41644E-03 0.00083  1.52931E-01 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  6.73389E-03 0.00089  1.54082E-01 0.00086 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49788E-01 0.00020  3.06769E-04 0.00244  1.85476E-03 0.02797  3.13577E-01 0.00075 ];
INF_S1                    (idx, [1:   8]) = [  2.07003E-02 0.00037 -6.90614E-05 0.00195  3.76458E-04 0.04501  1.86411E-02 0.00770 ];
INF_S2                    (idx, [1:   8]) = [  4.47026E-03 0.00130 -8.27488E-06 0.06900  1.50104E-05 1.00000  1.27302E-03 0.09604 ];
INF_S3                    (idx, [1:   8]) = [  1.00120E-03 0.01110 -5.22424E-06 0.07958 -4.05529E-05 0.23712  2.81319E-04 0.54218 ];
INF_S4                    (idx, [1:   8]) = [  2.18417E-04 0.00873 -1.93695E-06 0.10078 -3.03349E-05 0.33389  4.77512E-04 0.40551 ];
INF_S5                    (idx, [1:   8]) = [  1.35243E-04 0.12070  3.55401E-07 0.12875 -2.95817E-05 0.11619 -2.00065E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.01636E-04 0.06540  4.64465E-07 0.29802 -2.45201E-05 0.59316 -2.86324E-04 0.68094 ];
INF_S7                    (idx, [1:   8]) = [  3.89913E-05 0.24846 -3.10261E-07 1.00000 -2.05455E-05 0.61889  1.45564E-04 0.50601 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49791E-01 0.00020  3.06769E-04 0.00244  1.85476E-03 0.02797  3.13577E-01 0.00075 ];
INF_SP1                   (idx, [1:   8]) = [  2.07004E-02 0.00036 -6.90614E-05 0.00195  3.76458E-04 0.04501  1.86411E-02 0.00770 ];
INF_SP2                   (idx, [1:   8]) = [  4.47034E-03 0.00128 -8.27488E-06 0.06900  1.50104E-05 1.00000  1.27302E-03 0.09604 ];
INF_SP3                   (idx, [1:   8]) = [  1.00119E-03 0.01116 -5.22424E-06 0.07958 -4.05529E-05 0.23712  2.81319E-04 0.54218 ];
INF_SP4                   (idx, [1:   8]) = [  2.18453E-04 0.00863 -1.93695E-06 0.10078 -3.03349E-05 0.33389  4.77512E-04 0.40551 ];
INF_SP5                   (idx, [1:   8]) = [  1.35241E-04 0.12049  3.55401E-07 0.12875 -2.95817E-05 0.11619 -2.00065E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.01655E-04 0.06475  4.64465E-07 0.29802 -2.45201E-05 0.59316 -2.86324E-04 0.68094 ];
INF_SP7                   (idx, [1:   8]) = [  3.90231E-05 0.24789 -3.10261E-07 1.00000 -2.05455E-05 0.61889  1.45564E-04 0.50601 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.47577E-01 0.00109 -4.60474E+00 0.05466 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.00324E-01 0.00053 -6.71892E-01 0.01087 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  3.98332E-01 0.00074 -6.70144E-01 0.06077 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.76035E-01 0.00186  4.28736E-01 0.02933 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  9.59023E-01 0.00109 -7.28285E-02 0.05526 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  8.32659E-01 0.00053 -4.96228E-01 0.01083 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  8.36823E-01 0.00074 -5.01045E-01 0.05988 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.20758E+00 0.00187  7.78788E-01 0.02863 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.36841E-03 0.01208  2.62789E-04 0.06307  1.07576E-03 0.03432  7.27128E-04 0.03546  1.44701E-03 0.02831  2.33817E-03 0.01589  6.86356E-04 0.04258  6.50265E-04 0.04337  1.80930E-04 0.08110 ];
LAMBDA                    (idx, [1:  18]) = [  4.21794E-01 0.01780  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28663E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97388E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82772 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64847E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'T' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.70613E+04 0.01252  8.17517E+04 0.00389  1.85877E+05 0.00411  3.34498E+05 0.00527  3.56803E+05 0.00206  3.30503E+05 0.00528  2.98456E+05 0.00174  2.61910E+05 0.00435  2.30864E+05 0.00089  2.03061E+05 0.00201  1.83264E+05 0.00096  1.69798E+05 0.00385  1.51992E+05 0.00170  1.48622E+05 0.00305  1.39215E+05 0.00264  1.17119E+05 0.00199  1.14123E+05 0.00205  1.07452E+05 0.00294  9.99355E+04 0.00233  1.78273E+05 0.00401  1.42513E+05 0.00246  8.52912E+04 0.00172  4.94954E+04 0.00724  4.49336E+04 0.00366  3.66872E+04 0.00697  2.52035E+04 0.00529  3.51963E+04 0.00949  6.37675E+03 0.00750  8.31257E+03 0.00598  7.78542E+03 0.00052  4.35966E+03 0.00859  7.59030E+03 0.01517  5.07106E+03 0.00981  3.94813E+03 0.01199  7.33450E+02 0.01181  6.96037E+02 0.04160  6.97759E+02 0.01409  7.54784E+02 0.05540  7.19464E+02 0.04307  7.00234E+02 0.04442  6.68674E+02 0.02209  6.47488E+02 0.02113  1.21217E+03 0.02332  1.82726E+03 0.01406  2.32552E+03 0.01228  5.79679E+03 0.00660  5.22740E+03 0.01149  4.60396E+03 0.00647  2.32636E+03 0.01320  1.41843E+03 0.01158  1.00647E+03 0.01318  9.62628E+02 0.02653  1.51939E+03 0.03347  1.54042E+03 0.03546  2.18996E+03 0.00844  2.47072E+03 0.01034  2.68448E+03 0.00463  1.53155E+03 0.01796  1.03374E+03 0.03873  7.49975E+02 0.05340  7.02751E+02 0.02817  6.74258E+02 0.00650  6.18085E+02 0.02656  4.25360E+02 0.03912  4.05197E+02 0.03734  3.79963E+02 0.01120  3.52121E+02 0.02479  3.00382E+02 0.01577  2.31779E+02 0.04765  1.03789E+02 0.03213 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.19090E+00 0.00191  3.34653E-02 0.00230 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.16896E-01 0.00054  5.87170E-01 0.00097 ];
INF_CAPT                  (idx, [1:   4]) = [  9.04702E-03 0.00256  2.69897E-02 0.00507 ];
INF_ABS                   (idx, [1:   4]) = [  9.04702E-03 0.00256  2.69897E-02 0.00507 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.32578E-08 0.00159  1.88827E-06 0.00532 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.07867E-01 0.00053  5.60488E-01 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  1.49658E-01 0.00101  2.00169E-01 0.00725 ];
INF_SCATT2                (idx, [1:   4]) = [  6.19509E-02 0.00279  7.99527E-02 0.01502 ];
INF_SCATT3                (idx, [1:   4]) = [  2.59848E-03 0.03618  2.44544E-02 0.04206 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.55072E-03 0.02160  4.08922E-03 0.15389 ];
INF_SCATT5                (idx, [1:   4]) = [  3.06026E-04 0.32324 -2.26060E-03 0.23816 ];
INF_SCATT6                (idx, [1:   4]) = [  3.33032E-03 0.01026 -2.02768E-03 0.28290 ];
INF_SCATT7                (idx, [1:   4]) = [  2.07417E-04 0.09824 -1.10096E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.07870E-01 0.00053  5.60488E-01 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.49659E-01 0.00101  2.00169E-01 0.00725 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.19511E-02 0.00278  7.99527E-02 0.01502 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.59844E-03 0.03616  2.44544E-02 0.04206 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.55059E-03 0.02156  4.08922E-03 0.15389 ];
INF_SCATTP5               (idx, [1:   4]) = [  3.05943E-04 0.32292 -2.26060E-03 0.23816 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.33024E-03 0.01026 -2.02768E-03 0.28290 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.07358E-04 0.09811 -1.10096E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  1.99588E-01 0.00083  3.72829E-01 0.00381 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.67011E+00 0.00083  8.94091E-01 0.00381 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.04326E-03 0.00254  2.69897E-02 0.00507 ];
INF_REMXS                 (idx, [1:   4]) = [  1.17037E-02 0.00230  2.83362E-02 0.02311 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.05192E-01 0.00050  2.67464E-03 0.00582  1.65412E-03 0.06052  5.58834E-01 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  1.48708E-01 0.00097  9.50851E-04 0.00960  4.70584E-04 0.11611  1.99698E-01 0.00749 ];
INF_S2                    (idx, [1:   8]) = [  6.22822E-02 0.00275 -3.31305E-04 0.00771  4.27078E-04 0.19019  7.95256E-02 0.01610 ];
INF_S3                    (idx, [1:   8]) = [  3.09079E-03 0.03180 -4.92310E-04 0.00971  3.14476E-04 0.22365  2.41400E-02 0.04355 ];
INF_S4                    (idx, [1:   8]) = [ -6.40657E-03 0.02229 -1.44146E-04 0.01050  8.41738E-05 0.81688  4.00504E-03 0.16163 ];
INF_S5                    (idx, [1:   8]) = [  2.48268E-04 0.38360  5.77577E-05 0.06816  1.21094E-04 0.30516 -2.38170E-03 0.22317 ];
INF_S6                    (idx, [1:   8]) = [  3.29096E-03 0.01022  3.93515E-05 0.05187  5.89877E-05 0.62590 -2.08666E-03 0.27618 ];
INF_S7                    (idx, [1:   8]) = [  2.09493E-04 0.08776 -2.07595E-06 1.00000 -2.56715E-05 1.00000 -8.44244E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.05196E-01 0.00050  2.67464E-03 0.00582  1.65412E-03 0.06052  5.58834E-01 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  1.48708E-01 0.00098  9.50851E-04 0.00960  4.70584E-04 0.11611  1.99698E-01 0.00749 ];
INF_SP2                   (idx, [1:   8]) = [  6.22824E-02 0.00275 -3.31305E-04 0.00771  4.27078E-04 0.19019  7.95256E-02 0.01610 ];
INF_SP3                   (idx, [1:   8]) = [  3.09074E-03 0.03179 -4.92310E-04 0.00971  3.14476E-04 0.22365  2.41400E-02 0.04355 ];
INF_SP4                   (idx, [1:   8]) = [ -6.40644E-03 0.02224 -1.44146E-04 0.01050  8.41738E-05 0.81688  4.00504E-03 0.16163 ];
INF_SP5                   (idx, [1:   8]) = [  2.48185E-04 0.38323  5.77577E-05 0.06816  1.21094E-04 0.30516 -2.38170E-03 0.22317 ];
INF_SP6                   (idx, [1:   8]) = [  3.29089E-03 0.01022  3.93515E-05 0.05187  5.89877E-05 0.62590 -2.08666E-03 0.27618 ];
INF_SP7                   (idx, [1:   8]) = [  2.09434E-04 0.08765 -2.07595E-06 1.00000 -2.56715E-05 1.00000 -8.44244E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.22917E-01 0.00155  1.40097E+00 0.19873 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.53029E-01 0.00457 -2.95158E+00 1.00000 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.53692E-01 0.00136 -1.07809E+00 1.00000 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.79730E-01 0.00154  5.45741E-01 0.13293 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.49533E+00 0.00155  2.54869E-01 0.16755 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.31742E+00 0.00457  6.57224E-02 0.87056 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.31393E+00 0.00136  6.25223E-02 0.87722 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.85464E+00 0.00154  6.36362E-01 0.15139 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28667E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97393E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82774 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64839E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'C' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.88111E+04 0.00582  1.85966E+05 0.00309  4.28940E+05 0.00139  7.60653E+05 0.00224  8.02457E+05 0.00182  7.43336E+05 0.00218  6.66297E+05 0.00063  5.81910E+05 0.00188  5.09542E+05 0.00116  4.49371E+05 0.00105  4.06354E+05 0.00136  3.77484E+05 0.00146  3.40946E+05 0.00124  3.34677E+05 0.00040  3.15281E+05 0.00089  2.67681E+05 0.00145  2.61181E+05 0.00171  2.51284E+05 0.00208  2.37935E+05 0.00218  4.36753E+05 0.00025  3.72151E+05 0.00095  2.38585E+05 0.00242  1.40392E+05 0.00104  1.43570E+05 0.00116  1.21448E+05 0.00452  9.12109E+04 0.00312  1.39517E+05 0.00408  2.75836E+04 0.00594  3.38826E+04 0.00474  3.05145E+04 0.00760  1.71725E+04 0.00382  3.01635E+04 0.00195  1.99568E+04 0.00415  1.60061E+04 0.00287  2.90570E+03 0.01236  2.85777E+03 0.02336  2.91180E+03 0.00687  3.00482E+03 0.01918  2.88529E+03 0.01111  2.84337E+03 0.00960  2.92810E+03 0.01349  2.73884E+03 0.01013  5.15546E+03 0.00771  7.99309E+03 0.00695  1.00565E+04 0.00182  2.50371E+04 0.00321  2.37298E+04 0.00756  2.18688E+04 0.00263  1.18098E+04 0.00599  7.29606E+03 0.00503  5.05862E+03 0.00288  5.21691E+03 0.01198  8.18826E+03 0.01507  9.06222E+03 0.00613  1.36152E+04 0.00682  1.70538E+04 0.00879  2.30315E+04 0.00372  1.55554E+04 0.01154  1.17513E+04 0.01086  9.12031E+03 0.01142  8.70679E+03 0.01703  9.24236E+03 0.00925  8.24607E+03 0.01212  5.91298E+03 0.00716  5.73889E+03 0.01608  5.44184E+03 0.01191  4.90516E+03 0.00201  4.06189E+03 0.00336  2.92168E+03 0.01278  1.21690E+03 0.02138 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.89215E+00 0.00057  2.38796E-01 0.00306 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.99741E-01 0.00015  7.12388E-01 0.00022 ];
INF_CAPT                  (idx, [1:   4]) = [  1.93792E-03 0.00060  1.41631E-02 0.00117 ];
INF_ABS                   (idx, [1:   4]) = [  1.93792E-03 0.00060  1.41631E-02 0.00117 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.34221E-08 0.00131  2.36027E-06 0.00118 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.97805E-01 0.00015  6.98251E-01 0.00018 ];
INF_SCATT1                (idx, [1:   4]) = [  1.58544E-01 0.00076  2.01536E-01 0.00328 ];
INF_SCATT2                (idx, [1:   4]) = [  6.32891E-02 0.00169  7.67317E-02 0.00807 ];
INF_SCATT3                (idx, [1:   4]) = [  2.35987E-03 0.02413  2.55395E-02 0.01381 ];
INF_SCATT4                (idx, [1:   4]) = [ -7.09964E-03 0.00893  6.42352E-03 0.06331 ];
INF_SCATT5                (idx, [1:   4]) = [  4.48965E-06 1.00000  1.54318E-03 0.03688 ];
INF_SCATT6                (idx, [1:   4]) = [  3.34547E-03 0.00849  6.21629E-04 0.15138 ];
INF_SCATT7                (idx, [1:   4]) = [  2.83748E-04 0.11632  1.32503E-03 0.13540 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.97808E-01 0.00015  6.98251E-01 0.00018 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.58544E-01 0.00077  2.01536E-01 0.00328 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.32891E-02 0.00169  7.67317E-02 0.00807 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.35992E-03 0.02414  2.55395E-02 0.01381 ];
INF_SCATTP4               (idx, [1:   4]) = [ -7.09959E-03 0.00895  6.42352E-03 0.06331 ];
INF_SCATTP5               (idx, [1:   4]) = [  4.39073E-06 1.00000  1.54318E-03 0.03688 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.34538E-03 0.00848  6.21629E-04 0.15138 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.83753E-04 0.11618  1.32503E-03 0.13540 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.71624E-01 0.00061  4.95001E-01 0.00111 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.22719E+00 0.00061  6.73402E-01 0.00111 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.93521E-03 0.00044  1.41631E-02 0.00117 ];
INF_REMXS                 (idx, [1:   4]) = [  6.69692E-03 0.00104  1.54213E-02 0.01132 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.93044E-01 0.00014  4.76076E-03 0.00106  1.28444E-03 0.06805  6.96966E-01 8.5E-05 ];
INF_S1                    (idx, [1:   8]) = [  1.56934E-01 0.00075  1.60947E-03 0.00360  3.33886E-04 0.08179  2.01202E-01 0.00316 ];
INF_S2                    (idx, [1:   8]) = [  6.37955E-02 0.00168 -5.06419E-04 0.00479  2.06308E-04 0.17997  7.65254E-02 0.00848 ];
INF_S3                    (idx, [1:   8]) = [  3.20941E-03 0.01741 -8.49539E-04 0.00492  1.25250E-04 0.11085  2.54143E-02 0.01368 ];
INF_S4                    (idx, [1:   8]) = [ -6.81364E-03 0.00981 -2.86002E-04 0.01248  1.17440E-04 0.08482  6.30608E-03 0.06311 ];
INF_S5                    (idx, [1:   8]) = [ -7.43927E-05 0.57054  7.88824E-05 0.02903  6.86091E-05 0.11362  1.47457E-03 0.03484 ];
INF_S6                    (idx, [1:   8]) = [  3.26611E-03 0.00864  7.93598E-05 0.01846  3.68315E-05 0.20357  5.84797E-04 0.17110 ];
INF_S7                    (idx, [1:   8]) = [  2.79985E-04 0.11677  3.76244E-06 0.12840  3.34116E-05 0.21169  1.29161E-03 0.13375 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.93047E-01 0.00014  4.76076E-03 0.00106  1.28444E-03 0.06805  6.96966E-01 8.5E-05 ];
INF_SP1                   (idx, [1:   8]) = [  1.56935E-01 0.00075  1.60947E-03 0.00360  3.33886E-04 0.08179  2.01202E-01 0.00316 ];
INF_SP2                   (idx, [1:   8]) = [  6.37955E-02 0.00168 -5.06419E-04 0.00479  2.06308E-04 0.17997  7.65254E-02 0.00848 ];
INF_SP3                   (idx, [1:   8]) = [  3.20946E-03 0.01742 -8.49539E-04 0.00492  1.25250E-04 0.11085  2.54143E-02 0.01368 ];
INF_SP4                   (idx, [1:   8]) = [ -6.81359E-03 0.00983 -2.86002E-04 0.01248  1.17440E-04 0.08482  6.30608E-03 0.06311 ];
INF_SP5                   (idx, [1:   8]) = [ -7.44916E-05 0.56945  7.88824E-05 0.02903  6.86091E-05 0.11362  1.47457E-03 0.03484 ];
INF_SP6                   (idx, [1:   8]) = [  3.26602E-03 0.00862  7.93598E-05 0.01846  3.68315E-05 0.20357  5.84797E-04 0.17110 ];
INF_SP7                   (idx, [1:   8]) = [  2.79990E-04 0.11663  3.76244E-06 0.12840  3.34116E-05 0.21169  1.29161E-03 0.13375 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.22442E-01 0.00256 -2.64456E-01 0.01007 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.63218E-01 0.00478 -1.43032E-01 0.00916 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.63083E-01 0.00654 -1.44133E-01 0.00711 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.69892E-01 0.00202  3.88353E-01 0.04439 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.49854E+00 0.00256 -1.26071E+00 0.01016 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.26644E+00 0.00475 -2.33088E+00 0.00919 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.26714E+00 0.00655 -2.31291E+00 0.00706 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.96204E+00 0.00202  8.61666E-01 0.04370 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28668E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97402E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82771 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64825E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '9' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.34374E+04 0.01621  6.26900E+04 0.00705  1.45617E+05 0.00473  2.49847E+05 0.00131  2.54336E+05 0.00188  2.31887E+05 0.00082  2.07761E+05 0.00548  1.79382E+05 0.00543  1.56800E+05 0.00212  1.37891E+05 0.00377  1.24636E+05 0.00091  1.13213E+05 0.00540  1.04370E+05 0.00704  9.85020E+04 0.00161  9.31734E+04 0.00249  7.86794E+04 0.00265  7.65719E+04 0.00165  7.33905E+04 0.00871  6.91099E+04 0.00436  1.26047E+05 0.00426  1.08627E+05 0.00514  6.88885E+04 0.00103  3.97030E+04 0.00661  3.98831E+04 0.00553  3.34309E+04 0.00474  2.55784E+04 0.00164  3.95340E+04 0.00058  8.37090E+03 0.00730  1.05379E+04 0.01105  9.69009E+03 0.02220  5.49833E+03 0.01445  9.64510E+03 0.01051  6.36275E+03 0.00350  4.92848E+03 0.01077  8.22994E+02 0.03245  8.46567E+02 0.03947  8.66790E+02 0.05792  9.21818E+02 0.03953  9.02248E+02 0.02080  8.58503E+02 0.00305  8.76408E+02 0.03965  8.11123E+02 0.00233  1.57684E+03 0.01652  2.42368E+03 0.04280  3.11199E+03 0.00872  7.36941E+03 0.00607  7.20291E+03 0.01151  6.19419E+03 0.00533  3.07069E+03 0.01355  1.82648E+03 0.04847  1.19375E+03 0.03302  1.23445E+03 0.05039  1.94364E+03 0.03723  1.99585E+03 0.01296  3.15862E+03 0.01314  3.73004E+03 0.01526  4.48058E+03 0.01799  2.71404E+03 0.01649  1.83195E+03 0.01957  1.35629E+03 0.03321  1.14082E+03 0.01510  1.09284E+03 0.02594  8.72175E+02 0.01971  5.61007E+02 0.03443  4.93257E+02 0.02190  4.14952E+02 0.02720  3.36922E+02 0.01452  2.15195E+02 0.04211  1.15260E+02 0.03497  2.87277E+01 0.11805 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.65691E+00 0.00373 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.02996E+00 0.00163  4.72130E-02 0.00378 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.57015E-01 0.00017  4.73124E-01 0.00064 ];
INF_CAPT                  (idx, [1:   4]) = [  2.59975E-03 0.00140  2.44599E-02 0.00149 ];
INF_ABS                   (idx, [1:   4]) = [  6.75482E-03 0.00124  1.58925E-01 0.00181 ];
INF_FISS                  (idx, [1:   4]) = [  4.15507E-03 0.00128  1.34466E-01 0.00189 ];
INF_NSF                   (idx, [1:   4]) = [  1.01663E-02 0.00127  3.27577E-01 0.00189 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44672E+00 6.5E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 3.9E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.23012E-08 0.00308  1.83364E-06 0.00228 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50293E-01 0.00027  3.12848E-01 0.00251 ];
INF_SCATT1                (idx, [1:   4]) = [  2.04197E-02 0.00666  1.78716E-02 0.01879 ];
INF_SCATT2                (idx, [1:   4]) = [  4.30299E-03 0.00683  5.98635E-04 1.00000 ];
INF_SCATT3                (idx, [1:   4]) = [  9.31644E-04 0.06380  1.06090E-03 0.50442 ];
INF_SCATT4                (idx, [1:   4]) = [  2.06548E-04 0.21060 -1.36337E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.24269E-04 0.88101 -2.31424E-04 0.80559 ];
INF_SCATT6                (idx, [1:   4]) = [  7.78750E-05 0.47674 -2.73358E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -5.54546E-05 0.57648  1.04499E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50296E-01 0.00027  3.12848E-01 0.00251 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.04197E-02 0.00665  1.78716E-02 0.01879 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.30272E-03 0.00677  5.98635E-04 1.00000 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.31706E-04 0.06381  1.06090E-03 0.50442 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.06370E-04 0.21079 -1.36337E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.24298E-04 0.88083 -2.31424E-04 0.80559 ];
INF_SCATTP6               (idx, [1:   4]) = [  7.76481E-05 0.47782 -2.73358E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -5.55721E-05 0.57321  1.04499E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02821E-01 0.00101  4.35601E-01 0.00260 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64349E+00 0.00101  7.65237E-01 0.00259 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.75207E-03 0.00129  1.58925E-01 0.00181 ];
INF_REMXS                 (idx, [1:   4]) = [  7.26646E-03 0.00217  1.61632E-01 0.00309 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49749E-01 0.00024  5.44659E-04 0.01396  1.35637E-03 0.01391  3.11492E-01 0.00247 ];
INF_S1                    (idx, [1:   8]) = [  2.05482E-02 0.00663 -1.28451E-04 0.01187  2.94933E-04 0.18087  1.75766E-02 0.01717 ];
INF_S2                    (idx, [1:   8]) = [  4.31429E-03 0.00659 -1.13043E-05 0.18487  7.60226E-07 1.00000  5.97875E-04 1.00000 ];
INF_S3                    (idx, [1:   8]) = [  9.39903E-04 0.06762 -8.25860E-06 0.49885 -2.89806E-05 1.00000  1.08988E-03 0.51763 ];
INF_S4                    (idx, [1:   8]) = [  2.08287E-04 0.20743 -1.73851E-06 0.36725 -5.21635E-05 0.54632 -8.41739E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.24022E-04 0.88858  2.47592E-07 1.00000 -4.15723E-05 0.81578 -1.89851E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  7.81174E-05 0.47830 -2.42468E-07 1.00000 -1.84132E-05 0.90872 -8.92266E-06 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -5.54213E-05 0.55324 -3.32507E-08 1.00000 -4.08124E-05 0.21473  1.45311E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49751E-01 0.00024  5.44659E-04 0.01396  1.35637E-03 0.01391  3.11492E-01 0.00247 ];
INF_SP1                   (idx, [1:   8]) = [  2.05481E-02 0.00663 -1.28451E-04 0.01187  2.94933E-04 0.18087  1.75766E-02 0.01717 ];
INF_SP2                   (idx, [1:   8]) = [  4.31402E-03 0.00653 -1.13043E-05 0.18487  7.60226E-07 1.00000  5.97875E-04 1.00000 ];
INF_SP3                   (idx, [1:   8]) = [  9.39965E-04 0.06763 -8.25860E-06 0.49885 -2.89806E-05 1.00000  1.08988E-03 0.51763 ];
INF_SP4                   (idx, [1:   8]) = [  2.08108E-04 0.20762 -1.73851E-06 0.36725 -5.21635E-05 0.54632 -8.41739E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.24050E-04 0.88840  2.47592E-07 1.00000 -4.15723E-05 0.81578 -1.89851E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  7.78905E-05 0.47941 -2.42468E-07 1.00000 -1.84132E-05 0.90872 -8.92266E-06 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -5.55389E-05 0.55007 -3.32507E-08 1.00000 -4.08124E-05 0.21473  1.45311E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.24914E-01 0.00497 -2.06282E-01 0.03633 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.34439E-01 0.00359 -1.15554E-01 0.05061 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.28103E-01 0.00433 -1.13680E-01 0.00974 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.03471E-01 0.00943  3.44440E-01 0.04329 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.84512E-01 0.00500 -1.62023E+00 0.03674 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.23724E-01 0.00360 -2.89944E+00 0.05044 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.31213E-01 0.00433 -2.93276E+00 0.00968 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.09860E+00 0.00952  9.71498E-01 0.04453 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.19058E-03 0.03082  2.99400E-04 0.13124  1.09220E-03 0.06564  6.28208E-04 0.08212  1.41055E-03 0.06556  2.23834E-03 0.05457  6.70898E-04 0.10095  6.94089E-04 0.09592  1.56907E-04 0.17216 ];
LAMBDA                    (idx, [1:  18]) = [  4.19610E-01 0.03671  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28669E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97405E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82770 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64820E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '8' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.72927E+04 0.02929  8.47883E+04 0.00393  1.95903E+05 0.00539  3.36286E+05 0.00108  3.44740E+05 0.00224  3.12900E+05 0.00309  2.80757E+05 0.00306  2.42499E+05 0.00186  2.11724E+05 0.00374  1.88152E+05 0.00169  1.68474E+05 0.00627  1.53770E+05 0.00205  1.41990E+05 0.00280  1.34279E+05 0.00060  1.29100E+05 0.00396  1.08192E+05 0.00451  1.05972E+05 0.00356  1.01186E+05 0.00577  9.52203E+04 0.00645  1.75914E+05 0.00222  1.51130E+05 0.00203  9.70658E+04 0.00400  5.76321E+04 0.00626  5.81987E+04 0.00314  4.95620E+04 0.00488  3.80073E+04 0.00530  5.92609E+04 0.01023  1.27645E+04 0.00569  1.57574E+04 0.00938  1.45366E+04 0.00885  8.06464E+03 0.01375  1.42513E+04 0.00949  9.49684E+03 0.01013  7.45377E+03 0.01776  1.29552E+03 0.03181  1.28485E+03 0.01196  1.33998E+03 0.01570  1.33566E+03 0.01445  1.35721E+03 0.03573  1.32566E+03 0.00494  1.35360E+03 0.01959  1.22859E+03 0.02276  2.33541E+03 0.00565  3.73679E+03 0.01913  4.54980E+03 0.00803  1.15186E+04 0.00374  1.09610E+04 0.00809  9.83612E+03 0.01728  5.06534E+03 0.00596  3.01327E+03 0.02212  2.05876E+03 0.00717  2.05039E+03 0.03685  3.21497E+03 0.01020  3.52011E+03 0.01012  5.30775E+03 0.01217  6.46204E+03 0.02127  8.39590E+03 0.01254  5.36920E+03 0.01590  3.86868E+03 0.01397  2.74233E+03 0.02575  2.55242E+03 0.02520  2.53493E+03 0.02514  2.04679E+03 0.00082  1.33819E+03 0.01909  1.20438E+03 0.02784  1.01150E+03 0.00199  7.59337E+02 0.02551  5.66927E+02 0.02067  2.94924E+02 0.01933  7.33548E+01 0.02876 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.67382E+00 0.00285 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.15574E+00 0.00063  8.42640E-02 0.00630 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56626E-01 0.00041  4.64226E-01 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  2.44395E-03 0.00227  2.31620E-02 0.00081 ];
INF_ABS                   (idx, [1:   4]) = [  6.24218E-03 0.00245  1.50758E-01 0.00114 ];
INF_FISS                  (idx, [1:   4]) = [  3.79823E-03 0.00292  1.27596E-01 0.00120 ];
INF_NSF                   (idx, [1:   4]) = [  9.29047E-03 0.00291  3.10842E-01 0.00120 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44600E+00 1.5E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 6.7E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.46431E-08 0.00392  1.97232E-06 0.00126 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50358E-01 0.00034  3.13834E-01 0.00220 ];
INF_SCATT1                (idx, [1:   4]) = [  2.01190E-02 0.00930  1.91775E-02 0.02570 ];
INF_SCATT2                (idx, [1:   4]) = [  4.38096E-03 0.01904  7.15709E-04 0.24037 ];
INF_SCATT3                (idx, [1:   4]) = [  9.21816E-04 0.10776  1.01367E-03 0.33044 ];
INF_SCATT4                (idx, [1:   4]) = [  2.71819E-04 0.18063  7.73511E-04 0.31588 ];
INF_SCATT5                (idx, [1:   4]) = [  1.10291E-04 0.24324  4.52622E-04 0.70010 ];
INF_SCATT6                (idx, [1:   4]) = [  1.17698E-04 0.33185  5.53340E-04 0.34631 ];
INF_SCATT7                (idx, [1:   4]) = [  1.30419E-05 1.00000  9.74185E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50359E-01 0.00034  3.13834E-01 0.00220 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.01192E-02 0.00930  1.91775E-02 0.02570 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.38117E-03 0.01908  7.15709E-04 0.24037 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.22077E-04 0.10777  1.01367E-03 0.33044 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.71824E-04 0.18035  7.73511E-04 0.31588 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.10410E-04 0.24304  4.52622E-04 0.70010 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.17774E-04 0.33220  5.53340E-04 0.34631 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.31003E-05 1.00000  9.74185E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03476E-01 0.00059  4.26377E-01 0.00066 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63819E+00 0.00059  7.81781E-01 0.00066 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.24041E-03 0.00242  1.50758E-01 0.00114 ];
INF_REMXS                 (idx, [1:   4]) = [  6.89566E-03 0.00384  1.51481E-01 0.00503 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49730E-01 0.00032  6.27310E-04 0.01285  1.08877E-03 0.04582  3.12746E-01 0.00211 ];
INF_S1                    (idx, [1:   8]) = [  2.02694E-02 0.00932 -1.50472E-04 0.02413  2.40993E-04 0.09035  1.89365E-02 0.02534 ];
INF_S2                    (idx, [1:   8]) = [  4.39380E-03 0.01880 -1.28387E-05 0.36216 -3.05971E-05 0.20594  7.46307E-04 0.23602 ];
INF_S3                    (idx, [1:   8]) = [  9.30202E-04 0.10464 -8.38683E-06 0.26878 -6.71193E-05 0.16632  1.08079E-03 0.30255 ];
INF_S4                    (idx, [1:   8]) = [  2.78802E-04 0.17498 -6.98256E-06 0.30974 -4.48505E-05 0.52497  8.18362E-04 0.28724 ];
INF_S5                    (idx, [1:   8]) = [  1.04702E-04 0.25547  5.58895E-06 0.06227 -4.75628E-05 0.15257  5.00184E-04 0.63334 ];
INF_S6                    (idx, [1:   8]) = [  1.17647E-04 0.34409  5.09011E-08 1.00000 -1.26077E-05 0.39755  5.65948E-04 0.34040 ];
INF_S7                    (idx, [1:   8]) = [  1.42702E-05 1.00000 -1.22827E-06 1.00000  1.07496E-05 1.00000  8.66690E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49732E-01 0.00032  6.27310E-04 0.01285  1.08877E-03 0.04582  3.12746E-01 0.00211 ];
INF_SP1                   (idx, [1:   8]) = [  2.02696E-02 0.00932 -1.50472E-04 0.02413  2.40993E-04 0.09035  1.89365E-02 0.02534 ];
INF_SP2                   (idx, [1:   8]) = [  4.39401E-03 0.01884 -1.28387E-05 0.36216 -3.05971E-05 0.20594  7.46307E-04 0.23602 ];
INF_SP3                   (idx, [1:   8]) = [  9.30464E-04 0.10465 -8.38683E-06 0.26878 -6.71193E-05 0.16632  1.08079E-03 0.30255 ];
INF_SP4                   (idx, [1:   8]) = [  2.78807E-04 0.17470 -6.98256E-06 0.30974 -4.48505E-05 0.52497  8.18362E-04 0.28724 ];
INF_SP5                   (idx, [1:   8]) = [  1.04821E-04 0.25525  5.58895E-06 0.06227 -4.75628E-05 0.15257  5.00184E-04 0.63334 ];
INF_SP6                   (idx, [1:   8]) = [  1.17724E-04 0.34444  5.09011E-08 1.00000 -1.26077E-05 0.39755  5.65948E-04 0.34040 ];
INF_SP7                   (idx, [1:   8]) = [  1.43285E-05 1.00000 -1.22827E-06 1.00000  1.07496E-05 1.00000  8.66690E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.28195E-01 0.00253 -1.89094E-01 0.02890 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.26772E-01 0.00125 -1.08896E-01 0.03711 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.28742E-01 0.01059 -1.07540E-01 0.01342 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.10956E-01 0.00661  3.81966E-01 0.01574 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.78471E-01 0.00253 -1.76577E+00 0.02916 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.32787E-01 0.00125 -3.06967E+00 0.03798 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.30570E-01 0.01066 -3.10074E+00 0.01347 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.07206E+00 0.00662  8.73105E-01 0.01551 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.06873E-03 0.02270  2.30199E-04 0.14876  1.06511E-03 0.05565  7.13828E-04 0.07718  1.42074E-03 0.05830  2.25722E-03 0.04696  6.46238E-04 0.08329  6.23071E-04 0.07732  1.12325E-04 0.17065 ];
LAMBDA                    (idx, [1:  18]) = [  3.91400E-01 0.03898  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28670E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97407E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82771 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64817E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '7' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.05958E+04 0.01238  5.21845E+04 0.01044  1.20632E+05 0.00498  2.05124E+05 0.00215  2.11684E+05 0.00581  1.93918E+05 0.00593  1.73239E+05 0.00196  1.49368E+05 0.00294  1.29472E+05 0.00408  1.15751E+05 0.00308  1.04550E+05 0.00480  9.52476E+04 0.00789  8.83631E+04 0.00945  8.42120E+04 0.00443  8.04310E+04 0.00796  6.77680E+04 0.00327  6.65644E+04 0.00275  6.41164E+04 0.00342  6.12789E+04 0.00639  1.12666E+05 0.00376  9.75331E+04 0.00914  6.39591E+04 0.00783  3.84705E+04 0.01011  3.97488E+04 0.00678  3.35720E+04 0.00494  2.63092E+04 0.01033  4.16078E+04 0.00342  8.66156E+03 0.01100  1.06976E+04 0.01745  1.00750E+04 0.00992  5.56069E+03 0.01506  9.85089E+03 0.01675  6.68353E+03 0.01484  5.19149E+03 0.01374  9.19264E+02 0.00854  9.41073E+02 0.00361  9.68537E+02 0.02145  9.22406E+02 0.03395  9.19318E+02 0.02239  8.71549E+02 0.01910  9.35854E+02 0.00825  8.71385E+02 0.02269  1.65501E+03 0.01461  2.68786E+03 0.02463  3.27662E+03 0.02434  8.23240E+03 0.01507  7.91123E+03 0.00505  7.03588E+03 0.00763  3.77685E+03 0.03866  2.38213E+03 0.03917  1.53851E+03 0.04418  1.64472E+03 0.03530  2.51979E+03 0.03115  2.85434E+03 0.00500  4.25851E+03 0.01325  5.53563E+03 0.01372  7.37498E+03 0.01512  4.99654E+03 0.00321  3.58725E+03 0.00999  2.62313E+03 0.00998  2.50677E+03 0.01397  2.47998E+03 0.01055  2.10008E+03 0.00529  1.38277E+03 0.02202  1.23221E+03 0.01816  1.09597E+03 0.01720  9.54179E+02 0.02962  6.50734E+02 0.02110  3.66866E+02 0.00746  9.60382E+01 0.04340 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.69841E+00 0.00588 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.60877E+00 0.00093  7.09181E-02 0.00093 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56704E-01 0.00056  4.55790E-01 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  2.34416E-03 0.00083  2.19491E-02 0.00070 ];
INF_ABS                   (idx, [1:   4]) = [  5.86593E-03 0.00041  1.42815E-01 0.00112 ];
INF_FISS                  (idx, [1:   4]) = [  3.52177E-03 0.00013  1.20866E-01 0.00120 ];
INF_NSF                   (idx, [1:   4]) = [  8.61142E-03 0.00013  2.94446E-01 0.00120 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44520E+00 9.5E-06  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 3.9E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.76641E-08 0.00101  2.11853E-06 0.00160 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50809E-01 0.00041  3.12677E-01 0.00297 ];
INF_SCATT1                (idx, [1:   4]) = [  2.01233E-02 0.00423  1.78258E-02 0.03634 ];
INF_SCATT2                (idx, [1:   4]) = [  4.30460E-03 0.00980  1.26417E-03 0.27338 ];
INF_SCATT3                (idx, [1:   4]) = [  8.10251E-04 0.03431  1.22759E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.65627E-04 0.15560  4.58530E-04 0.38893 ];
INF_SCATT5                (idx, [1:   4]) = [  2.00339E-04 0.24289  4.09058E-04 0.58466 ];
INF_SCATT6                (idx, [1:   4]) = [  4.19652E-05 1.00000 -4.24603E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -3.61889E-06 1.00000  2.41563E-04 0.24075 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50810E-01 0.00041  3.12677E-01 0.00297 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.01231E-02 0.00423  1.78258E-02 0.03634 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.30481E-03 0.00982  1.26417E-03 0.27338 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.10435E-04 0.03444  1.22759E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.65696E-04 0.15607  4.58530E-04 0.38893 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.00391E-04 0.24335  4.09058E-04 0.58466 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.19561E-05 1.00000 -4.24603E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -3.38350E-06 1.00000  2.41563E-04 0.24075 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03576E-01 0.00088  4.19749E-01 0.00109 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63739E+00 0.00088  7.94127E-01 0.00109 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.86427E-03 0.00043  1.42815E-01 0.00112 ];
INF_REMXS                 (idx, [1:   4]) = [  6.59973E-03 0.00695  1.44063E-01 0.00839 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50104E-01 0.00039  7.04689E-04 0.00700  9.49949E-04 0.14477  3.11727E-01 0.00342 ];
INF_S1                    (idx, [1:   8]) = [  2.02794E-02 0.00443 -1.56118E-04 0.03568  1.65928E-04 0.13075  1.76598E-02 0.03588 ];
INF_S2                    (idx, [1:   8]) = [  4.32804E-03 0.01045 -2.34355E-05 0.14077  1.14448E-05 1.00000  1.25273E-03 0.27834 ];
INF_S3                    (idx, [1:   8]) = [  8.13139E-04 0.03354 -2.88849E-06 0.92247 -3.50964E-05 0.80211  1.57856E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.70914E-04 0.13883 -5.28752E-06 0.39856 -5.01686E-05 0.23690  5.08698E-04 0.34887 ];
INF_S5                    (idx, [1:   8]) = [  2.01761E-04 0.26362 -1.42157E-06 1.00000 -2.59135E-05 1.00000  4.34971E-04 0.58279 ];
INF_S6                    (idx, [1:   8]) = [  4.27064E-05 1.00000 -7.41181E-07 1.00000 -1.00425E-05 1.00000 -4.14561E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -1.66781E-06 1.00000 -1.95107E-06 1.00000 -8.65791E-06 0.80212  2.50221E-04 0.20547 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50106E-01 0.00039  7.04689E-04 0.00700  9.49949E-04 0.14477  3.11727E-01 0.00342 ];
INF_SP1                   (idx, [1:   8]) = [  2.02792E-02 0.00444 -1.56118E-04 0.03568  1.65928E-04 0.13075  1.76598E-02 0.03588 ];
INF_SP2                   (idx, [1:   8]) = [  4.32824E-03 0.01047 -2.34355E-05 0.14077  1.14448E-05 1.00000  1.25273E-03 0.27834 ];
INF_SP3                   (idx, [1:   8]) = [  8.13324E-04 0.03368 -2.88849E-06 0.92247 -3.50964E-05 0.80211  1.57856E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.70984E-04 0.13930 -5.28752E-06 0.39856 -5.01686E-05 0.23690  5.08698E-04 0.34887 ];
INF_SP5                   (idx, [1:   8]) = [  2.01813E-04 0.26406 -1.42157E-06 1.00000 -2.59135E-05 1.00000  4.34971E-04 0.58279 ];
INF_SP6                   (idx, [1:   8]) = [  4.26973E-05 1.00000 -7.41181E-07 1.00000 -1.00425E-05 1.00000 -4.14561E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -1.43242E-06 1.00000 -1.95107E-06 1.00000 -8.65791E-06 0.80212  2.50221E-04 0.20547 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.38434E-01 0.00270 -1.83334E-01 0.01071 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.46140E-01 0.00860 -1.04547E-01 0.02628 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.33329E-01 0.00662 -1.03026E-01 0.00579 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.18937E-01 0.01087  3.42901E-01 0.01791 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.60293E-01 0.00271 -1.81859E+00 0.01080 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.10435E-01 0.00857 -3.19288E+00 0.02698 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.25059E-01 0.00659 -3.23563E+00 0.00582 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.04538E+00 0.01084  9.72732E-01 0.01820 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.32514E-03 0.03184  2.35172E-04 0.15344  1.13023E-03 0.07968  6.46361E-04 0.11692  1.40384E-03 0.06775  2.37947E-03 0.04232  7.32603E-04 0.10822  6.21835E-04 0.12548  1.75626E-04 0.17043 ];
LAMBDA                    (idx, [1:  18]) = [  4.12215E-01 0.04188  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28671E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97418E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82765 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64799E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '6' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.87289E+03 0.02785  2.82541E+04 0.00555  6.51380E+04 0.00557  1.10746E+05 0.00360  1.13939E+05 0.00184  1.04060E+05 0.00177  9.31354E+04 0.00623  8.14673E+04 0.00617  7.06069E+04 0.00063  6.29095E+04 0.00410  5.65531E+04 0.00440  5.24925E+04 0.00169  4.80150E+04 0.00250  4.61006E+04 0.00476  4.39743E+04 0.00326  3.74628E+04 0.01122  3.67336E+04 0.00527  3.51292E+04 0.00427  3.31679E+04 0.00222  6.23444E+04 0.01105  5.44232E+04 0.00789  3.60531E+04 0.00249  2.12665E+04 0.00459  2.25738E+04 0.00960  1.94118E+04 0.01774  1.46582E+04 0.01555  2.37868E+04 0.00595  4.99990E+03 0.01312  6.17878E+03 0.00531  5.54986E+03 0.00530  3.25166E+03 0.02541  5.48011E+03 0.02698  3.71678E+03 0.01391  3.04107E+03 0.02154  5.65673E+02 0.03839  4.80360E+02 0.04901  5.40949E+02 0.07372  5.51850E+02 0.01597  5.78533E+02 0.02137  5.19794E+02 0.04962  5.74051E+02 0.04868  5.05664E+02 0.01011  1.01020E+03 0.01472  1.48700E+03 0.02351  1.90668E+03 0.01393  4.91690E+03 0.02172  4.61851E+03 0.02302  4.26191E+03 0.02564  2.29739E+03 0.01912  1.43031E+03 0.01013  9.61945E+02 0.03259  9.54510E+02 0.01285  1.54757E+03 0.02482  1.68389E+03 0.02183  2.56583E+03 0.01702  3.46560E+03 0.01435  4.62938E+03 0.02206  3.20502E+03 0.00631  2.44990E+03 0.00381  1.84035E+03 0.00638  1.72149E+03 0.00935  1.76453E+03 0.00726  1.50073E+03 0.02680  1.00453E+03 0.01101  9.26647E+02 0.02003  8.25674E+02 0.02177  7.20353E+02 0.00689  5.15110E+02 0.03241  2.87089E+02 0.02896  8.19440E+01 0.00938 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.71294E+00 0.00287 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.42639E+00 0.00142  4.52686E-02 0.00325 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56173E-01 0.00055  4.43092E-01 0.00055 ];
INF_CAPT                  (idx, [1:   4]) = [  2.15726E-03 0.00826  2.01019E-02 0.00220 ];
INF_ABS                   (idx, [1:   4]) = [  5.30397E-03 0.00539  1.30660E-01 0.00179 ];
INF_FISS                  (idx, [1:   4]) = [  3.14671E-03 0.00345  1.10558E-01 0.00174 ];
INF_NSF                   (idx, [1:   4]) = [  7.69330E-03 0.00343  2.69334E-01 0.00174 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44487E+00 1.9E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 1.2E-07  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.91546E-08 0.00035  2.21426E-06 0.00143 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50822E-01 0.00051  3.12906E-01 0.00213 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98604E-02 0.00766  1.84053E-02 0.03051 ];
INF_SCATT2                (idx, [1:   4]) = [  4.31362E-03 0.03356  8.72867E-04 0.81668 ];
INF_SCATT3                (idx, [1:   4]) = [  7.55274E-04 0.08873 -3.68965E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  8.15459E-05 0.55988 -2.58259E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  4.85171E-05 0.88613 -5.26972E-04 0.89562 ];
INF_SCATT6                (idx, [1:   4]) = [ -2.93013E-07 1.00000  3.23792E-04 0.66438 ];
INF_SCATT7                (idx, [1:   4]) = [ -4.08461E-07 1.00000 -9.33012E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50823E-01 0.00051  3.12906E-01 0.00213 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98607E-02 0.00765  1.84053E-02 0.03051 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.31364E-03 0.03356  8.72867E-04 0.81668 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.55400E-04 0.08873 -3.68965E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  8.19090E-05 0.55306 -2.58259E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  4.88662E-05 0.87825 -5.26972E-04 0.89562 ];
INF_SCATTP6               (idx, [1:   4]) = [ -1.44297E-07 1.00000  3.23792E-04 0.66438 ];
INF_SCATTP7               (idx, [1:   4]) = [ -3.12200E-07 1.00000 -9.33012E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03942E-01 0.00152  4.08079E-01 0.00116 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63446E+00 0.00152  8.16838E-01 0.00116 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.30327E-03 0.00531  1.30660E-01 0.00179 ];
INF_REMXS                 (idx, [1:   4]) = [  6.08408E-03 0.00400  1.31024E-01 0.00555 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50089E-01 0.00056  7.33483E-04 0.01650  8.38190E-04 0.03758  3.12068E-01 0.00206 ];
INF_S1                    (idx, [1:   8]) = [  2.00386E-02 0.00766 -1.78158E-04 0.02521  1.85461E-04 0.33309  1.82198E-02 0.03100 ];
INF_S2                    (idx, [1:   8]) = [  4.33924E-03 0.03398 -2.56205E-05 0.18583  4.10446E-05 1.00000  8.31822E-04 0.90003 ];
INF_S3                    (idx, [1:   8]) = [  7.59351E-04 0.08665 -4.07698E-06 1.00000 -5.39094E-06 1.00000 -3.63575E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  8.12333E-05 0.61491  3.12604E-07 1.00000 -6.46051E-06 1.00000 -2.51798E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  5.29930E-05 0.73235 -4.47586E-06 1.00000  1.70520E-05 0.53597 -5.44024E-04 0.85601 ];
INF_S6                    (idx, [1:   8]) = [ -1.35294E-06 1.00000  1.05993E-06 0.99911  5.98839E-06 1.00000  3.17803E-04 0.64271 ];
INF_S7                    (idx, [1:   8]) = [  3.97613E-06 1.00000 -4.38460E-06 0.60609 -1.05520E-06 1.00000 -8.27492E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50089E-01 0.00056  7.33483E-04 0.01650  8.38190E-04 0.03758  3.12068E-01 0.00206 ];
INF_SP1                   (idx, [1:   8]) = [  2.00389E-02 0.00766 -1.78158E-04 0.02521  1.85461E-04 0.33309  1.82198E-02 0.03100 ];
INF_SP2                   (idx, [1:   8]) = [  4.33926E-03 0.03397 -2.56205E-05 0.18583  4.10446E-05 1.00000  8.31822E-04 0.90003 ];
INF_SP3                   (idx, [1:   8]) = [  7.59477E-04 0.08666 -4.07698E-06 1.00000 -5.39094E-06 1.00000 -3.63575E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  8.15964E-05 0.60777  3.12604E-07 1.00000 -6.46051E-06 1.00000 -2.51798E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  5.33420E-05 0.72549 -4.47586E-06 1.00000  1.70520E-05 0.53597 -5.44024E-04 0.85601 ];
INF_SP6                   (idx, [1:   8]) = [ -1.20422E-06 1.00000  1.05993E-06 0.99911  5.98839E-06 1.00000  3.17803E-04 0.64271 ];
INF_SP7                   (idx, [1:   8]) = [  4.07240E-06 1.00000 -4.38460E-06 0.60609 -1.05520E-06 1.00000 -8.27492E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.26232E-01 0.00363 -1.94483E-01 0.01244 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.08545E-01 0.01658 -1.10971E-01 0.04911 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.99381E-01 0.01592 -1.13916E-01 0.02820 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.26026E-01 0.00811  4.15942E-01 0.04741 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.82068E-01 0.00364 -1.71447E+00 0.01234 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.55829E-01 0.01675 -3.01776E+00 0.04714 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.67827E-01 0.01572 -2.93067E+00 0.02745 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.02255E+00 0.00817  8.05013E-01 0.04748 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.22978E-03 0.04637  3.54973E-04 0.37624  1.16948E-03 0.12563  7.18762E-04 0.12714  1.50269E-03 0.10307  2.05304E-03 0.07917  7.41641E-04 0.13603  6.13677E-04 0.13846  7.55209E-05 0.26003 ];
LAMBDA                    (idx, [1:  18]) = [  3.70527E-01 0.05430  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28671E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97420E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82765 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64796E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '5' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.72832E+03 0.00214  2.72990E+04 0.00915  6.31484E+04 0.00302  1.07913E+05 0.00509  1.11124E+05 0.00533  1.00922E+05 0.00431  9.03231E+04 0.00425  7.84734E+04 0.00296  6.86761E+04 0.00297  6.09040E+04 0.00478  5.58070E+04 0.00706  5.15353E+04 0.00560  4.72549E+04 0.00283  4.60080E+04 0.00379  4.33809E+04 0.00266  3.69885E+04 0.00096  3.62514E+04 0.00359  3.46830E+04 0.00849  3.30997E+04 0.00511  6.18876E+04 0.00784  5.52339E+04 0.00595  3.60267E+04 0.00615  2.17602E+04 0.01029  2.31960E+04 0.00208  2.03966E+04 0.01337  1.59390E+04 0.00580  2.55707E+04 0.01000  5.23460E+03 0.01876  6.44940E+03 0.00511  6.06322E+03 0.01138  3.33423E+03 0.00955  6.04223E+03 0.02002  4.01247E+03 0.01204  3.24738E+03 0.00372  5.21276E+02 0.02496  5.41280E+02 0.05057  5.60884E+02 0.04574  5.87541E+02 0.04248  5.75753E+02 0.03234  5.19134E+02 0.02889  5.92497E+02 0.00976  5.55888E+02 0.03002  1.08528E+03 0.03276  1.62775E+03 0.02574  2.03279E+03 0.04158  5.08173E+03 0.01628  4.88546E+03 0.01100  4.41050E+03 0.02109  2.49968E+03 0.01216  1.53184E+03 0.02702  9.88148E+02 0.02513  1.02975E+03 0.02744  1.74753E+03 0.00905  1.90881E+03 0.02543  2.93552E+03 0.01301  3.89246E+03 0.00281  5.72366E+03 0.00954  4.01626E+03 0.00143  3.14025E+03 0.01388  2.43019E+03 0.01037  2.32587E+03 0.00421  2.33068E+03 0.02196  2.13059E+03 0.02818  1.43001E+03 0.04474  1.35491E+03 0.00778  1.17589E+03 0.01958  9.79890E+02 0.00722  7.32860E+02 0.02275  4.49045E+02 0.01622  1.24335E+02 0.01009 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.74471E+00 0.00331 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.40845E+00 0.00037  5.41841E-02 0.00025 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56037E-01 0.00054  4.33356E-01 0.00092 ];
INF_CAPT                  (idx, [1:   4]) = [  2.00892E-03 0.00355  1.86717E-02 0.00284 ];
INF_ABS                   (idx, [1:   4]) = [  4.85557E-03 0.00307  1.21225E-01 0.00306 ];
INF_FISS                  (idx, [1:   4]) = [  2.84664E-03 0.00282  1.02553E-01 0.00310 ];
INF_NSF                   (idx, [1:   4]) = [  6.95829E-03 0.00280  2.49834E-01 0.00310 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44438E+00 1.8E-05  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 1.0E-07  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.15391E-08 0.00347  2.34562E-06 0.00253 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.51201E-01 0.00058  3.12152E-01 0.00085 ];
INF_SCATT1                (idx, [1:   4]) = [  1.96935E-02 0.00555  1.80191E-02 0.06455 ];
INF_SCATT2                (idx, [1:   4]) = [  4.10707E-03 0.01050  1.39918E-03 0.12701 ];
INF_SCATT3                (idx, [1:   4]) = [  8.24693E-04 0.07358  9.67560E-04 0.09176 ];
INF_SCATT4                (idx, [1:   4]) = [  1.00173E-04 0.60862 -1.60297E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [ -3.75126E-05 0.38311 -1.05792E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  4.67927E-05 1.00000 -6.53007E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.31641E-05 1.00000  4.11949E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.51203E-01 0.00058  3.12152E-01 0.00085 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.96938E-02 0.00555  1.80191E-02 0.06455 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.10733E-03 0.01041  1.39918E-03 0.12701 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.25094E-04 0.07338  9.67560E-04 0.09176 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.00609E-04 0.60697 -1.60297E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [ -3.70589E-05 0.38032 -1.05792E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.70221E-05 1.00000 -6.53007E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.34977E-05 1.00000  4.11949E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04301E-01 0.00132  4.00178E-01 0.00248 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63158E+00 0.00133  8.32972E-01 0.00249 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.85415E-03 0.00308  1.21225E-01 0.00306 ];
INF_REMXS                 (idx, [1:   4]) = [  5.63011E-03 0.00513  1.22035E-01 0.00429 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50407E-01 0.00061  7.94218E-04 0.01383  8.30651E-04 0.10291  3.11321E-01 0.00113 ];
INF_S1                    (idx, [1:   8]) = [  1.98737E-02 0.00535 -1.80231E-04 0.05503  1.81486E-04 0.25775  1.78376E-02 0.06745 ];
INF_S2                    (idx, [1:   8]) = [  4.13109E-03 0.00896 -2.40260E-05 0.26319  1.34480E-05 0.94849  1.38573E-03 0.12598 ];
INF_S3                    (idx, [1:   8]) = [  8.34125E-04 0.07454 -9.43210E-06 0.46487  1.95442E-06 1.00000  9.65606E-04 0.06555 ];
INF_S4                    (idx, [1:   8]) = [  1.03298E-04 0.57199 -3.12536E-06 0.84457  2.81628E-06 1.00000 -1.63113E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [ -4.11370E-05 0.30026  3.62435E-06 0.60940  1.60044E-05 0.35955 -1.21796E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  4.74238E-05 0.95007 -6.31182E-07 1.00000  9.52779E-06 1.00000 -7.48285E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.26116E-05 1.00000  5.52556E-07 1.00000  1.48399E-05 1.00000  3.97109E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50409E-01 0.00061  7.94218E-04 0.01383  8.30651E-04 0.10291  3.11321E-01 0.00113 ];
INF_SP1                   (idx, [1:   8]) = [  1.98740E-02 0.00535 -1.80231E-04 0.05503  1.81486E-04 0.25775  1.78376E-02 0.06745 ];
INF_SP2                   (idx, [1:   8]) = [  4.13136E-03 0.00887 -2.40260E-05 0.26319  1.34480E-05 0.94849  1.38573E-03 0.12598 ];
INF_SP3                   (idx, [1:   8]) = [  8.34526E-04 0.07432 -9.43210E-06 0.46487  1.95442E-06 1.00000  9.65606E-04 0.06555 ];
INF_SP4                   (idx, [1:   8]) = [  1.03735E-04 0.57059 -3.12536E-06 0.84457  2.81628E-06 1.00000 -1.63113E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [ -4.06833E-05 0.29692  3.62435E-06 0.60940  1.60044E-05 0.35955 -1.21796E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  4.76533E-05 0.94644 -6.31182E-07 1.00000  9.52779E-06 1.00000 -7.48285E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.29452E-05 1.00000  5.52556E-07 1.00000  1.48399E-05 1.00000  3.97109E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.29720E-01 0.00331 -1.80785E-01 0.01828 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.93856E-01 0.01034 -1.04999E-01 0.02795 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.14387E-01 0.01254 -1.06090E-01 0.02284 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.32128E-01 0.00963  4.22467E-01 0.02268 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.75715E-01 0.00330 -1.84503E+00 0.01797 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.75105E-01 0.01029 -3.17959E+00 0.02795 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.48227E-01 0.01269 -3.14530E+00 0.02314 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.00381E+00 0.00955  7.89810E-01 0.02219 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.62759E-03 0.04080  2.58199E-04 0.20979  1.19280E-03 0.09974  5.77638E-04 0.12753  1.40714E-03 0.09770  2.13609E-03 0.07340  9.09627E-04 0.12638  8.56216E-04 0.13456  2.89884E-04 0.36809 ];
LAMBDA                    (idx, [1:  18]) = [  4.89733E-01 0.07286  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28671E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97423E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82763 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64790E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '4' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.11100E+04 0.01080  5.23727E+04 0.00023  1.19367E+05 0.00167  2.04350E+05 0.00564  2.11587E+05 0.00545  1.94773E+05 0.00212  1.74577E+05 0.00054  1.50600E+05 0.00446  1.32924E+05 0.00313  1.19061E+05 0.00163  1.08223E+05 0.00315  1.00000E+05 0.00723  9.35722E+04 0.00489  8.95532E+04 0.00306  8.59410E+04 0.01179  7.23995E+04 0.00393  7.17965E+04 0.00367  6.94983E+04 0.00307  6.54478E+04 0.00344  1.23969E+05 0.00558  1.10011E+05 0.00077  7.34316E+04 0.00678  4.40591E+04 0.00125  4.77016E+04 0.00198  4.20067E+04 0.00476  3.28311E+04 0.00478  5.34765E+04 0.01022  1.09583E+04 0.00856  1.34625E+04 0.02014  1.22708E+04 0.01540  6.89609E+03 0.00595  1.18295E+04 0.00744  8.18097E+03 0.02363  6.39643E+03 0.02616  1.25795E+03 0.01848  1.14123E+03 0.03300  1.19903E+03 0.03684  1.21819E+03 0.00177  1.20010E+03 0.02443  1.16688E+03 0.04471  1.26317E+03 0.02374  1.13176E+03 0.02021  2.21220E+03 0.02636  3.32603E+03 0.02708  4.11092E+03 0.01537  1.04873E+04 0.00692  1.02248E+04 0.00682  9.53758E+03 0.01190  5.29523E+03 0.02243  3.17739E+03 0.02807  2.24625E+03 0.01421  2.40636E+03 0.02444  3.79222E+03 0.02296  4.15386E+03 0.01138  6.89492E+03 0.00924  8.89834E+03 0.00402  1.33190E+04 0.00798  9.59212E+03 0.00616  7.31183E+03 0.01015  5.62263E+03 0.02067  5.39433E+03 0.00914  5.60737E+03 0.00290  4.91101E+03 0.01026  3.50128E+03 0.00828  3.32345E+03 0.01131  2.86560E+03 0.01087  2.51341E+03 0.00813  1.86394E+03 0.00939  1.07926E+03 0.02190  2.99937E+02 0.03338 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.74768E+00 0.00143 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.75485E+00 0.00112  1.23855E-01 0.00176 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56460E-01 0.00022  4.20197E-01 0.00027 ];
INF_CAPT                  (idx, [1:   4]) = [  1.89402E-03 0.00180  1.67966E-02 0.00111 ];
INF_ABS                   (idx, [1:   4]) = [  4.46870E-03 0.00045  1.08540E-01 0.00091 ];
INF_FISS                  (idx, [1:   4]) = [  2.57468E-03 0.00135  9.17433E-02 0.00087 ];
INF_NSF                   (idx, [1:   4]) = [  6.29225E-03 0.00135  2.23499E-01 0.00087 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44390E+00 7.1E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 5.2E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.34090E-08 0.00413  2.40202E-06 0.00065 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.51946E-01 0.00024  3.12609E-01 0.00078 ];
INF_SCATT1                (idx, [1:   4]) = [  1.99294E-02 0.00270  1.79905E-02 0.04130 ];
INF_SCATT2                (idx, [1:   4]) = [  3.88501E-03 0.03519  2.17030E-03 0.09954 ];
INF_SCATT3                (idx, [1:   4]) = [  8.16250E-04 0.07897  4.55966E-04 0.30419 ];
INF_SCATT4                (idx, [1:   4]) = [  3.04927E-05 1.00000  1.58057E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  8.98125E-05 0.16363  1.82378E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  2.81137E-05 1.00000  3.37349E-04 0.43571 ];
INF_SCATT7                (idx, [1:   4]) = [  3.33742E-05 1.00000  2.32817E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.51947E-01 0.00024  3.12609E-01 0.00078 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.99293E-02 0.00269  1.79905E-02 0.04130 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.88548E-03 0.03513  2.17030E-03 0.09954 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.16079E-04 0.07865  4.55966E-04 0.30419 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.05668E-05 1.00000  1.58057E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.95867E-05 0.16389  1.82378E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.81683E-05 1.00000  3.37349E-04 0.43571 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.32908E-05 1.00000  2.32817E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.05024E-01 0.00030  3.89668E-01 0.00199 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.62583E+00 0.00030  8.55435E-01 0.00198 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.46761E-03 0.00045  1.08540E-01 0.00091 ];
INF_REMXS                 (idx, [1:   4]) = [  5.35923E-03 0.00204  1.08340E-01 0.00291 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51100E-01 0.00024  8.45352E-04 0.00366  7.50908E-04 0.06430  3.11858E-01 0.00091 ];
INF_S1                    (idx, [1:   8]) = [  2.01104E-02 0.00257 -1.81006E-04 0.01384  1.80970E-04 0.04787  1.78095E-02 0.04218 ];
INF_S2                    (idx, [1:   8]) = [  3.91105E-03 0.03486 -2.60399E-05 0.01540  4.82789E-05 0.69440  2.12202E-03 0.08776 ];
INF_S3                    (idx, [1:   8]) = [  8.27530E-04 0.07815 -1.12798E-05 0.15850 -1.54498E-05 0.34378  4.71415E-04 0.28586 ];
INF_S4                    (idx, [1:   8]) = [  4.01386E-05 0.83941 -9.64590E-06 0.09217  1.23072E-05 1.00000  1.45750E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  9.17581E-05 0.16884 -1.94564E-06 0.63874 -1.12144E-05 0.93482  1.93593E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  2.95518E-05 1.00000 -1.43818E-06 1.00000  2.18840E-05 1.00000  3.15465E-04 0.39603 ];
INF_S7                    (idx, [1:   8]) = [  3.15961E-05 1.00000  1.77807E-06 1.00000 -3.89412E-06 1.00000  2.36711E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51101E-01 0.00024  8.45352E-04 0.00366  7.50908E-04 0.06430  3.11858E-01 0.00091 ];
INF_SP1                   (idx, [1:   8]) = [  2.01103E-02 0.00256 -1.81006E-04 0.01384  1.80970E-04 0.04787  1.78095E-02 0.04218 ];
INF_SP2                   (idx, [1:   8]) = [  3.91152E-03 0.03480 -2.60399E-05 0.01540  4.82789E-05 0.69440  2.12202E-03 0.08776 ];
INF_SP3                   (idx, [1:   8]) = [  8.27359E-04 0.07782 -1.12798E-05 0.15850 -1.54498E-05 0.34378  4.71415E-04 0.28586 ];
INF_SP4                   (idx, [1:   8]) = [  4.02127E-05 0.84388 -9.64590E-06 0.09217  1.23072E-05 1.00000  1.45750E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  9.15323E-05 0.16903 -1.94564E-06 0.63874 -1.12144E-05 0.93482  1.93593E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  2.96065E-05 1.00000 -1.43818E-06 1.00000  2.18840E-05 1.00000  3.15465E-04 0.39603 ];
INF_SP7                   (idx, [1:   8]) = [  3.15128E-05 1.00000  1.77807E-06 1.00000 -3.89412E-06 1.00000  2.36711E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.39063E-01 0.00840 -1.98432E-01 0.01234 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.15690E-01 0.00361 -1.18020E-01 0.01871 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.17883E-01 0.01856 -1.12228E-01 0.01354 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.37641E-01 0.00927  4.42881E-01 0.05046 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.59299E-01 0.00833 -1.68034E+00 0.01219 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.46399E-01 0.00360 -2.82640E+00 0.01906 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.44087E-01 0.01843 -2.97122E+00 0.01336 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.87409E-01 0.00918  7.56603E-01 0.05186 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.94095E-03 0.03093  2.34721E-04 0.16092  1.03348E-03 0.07625  6.52167E-04 0.11441  1.33810E-03 0.07588  2.15887E-03 0.06163  7.61048E-04 0.10006  5.88713E-04 0.09382  1.73845E-04 0.22594 ];
LAMBDA                    (idx, [1:  18]) = [  4.24777E-01 0.05410  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28672E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97427E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82761 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64785E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '3' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.94023E+03 0.06517  9.44157E+03 0.01937  2.29764E+04 0.01620  3.81888E+04 0.01693  3.98995E+04 0.00558  3.68504E+04 0.00219  3.34706E+04 0.00543  2.93137E+04 0.01159  2.56902E+04 0.00996  2.31135E+04 0.00645  2.13575E+04 0.00257  1.97684E+04 0.00467  1.85397E+04 0.00282  1.76133E+04 0.01199  1.69092E+04 0.01237  1.45345E+04 0.00472  1.42929E+04 0.02325  1.36592E+04 0.00501  1.31545E+04 0.01208  2.46652E+04 0.01894  2.21952E+04 0.01071  1.46863E+04 0.00851  9.00221E+03 0.00481  9.78444E+03 0.00421  8.59838E+03 0.02333  6.73309E+03 0.01646  1.09948E+04 0.01899  2.35288E+03 0.02326  2.80336E+03 0.02489  2.40863E+03 0.01584  1.39376E+03 0.02152  2.55685E+03 0.01354  1.67621E+03 0.02597  1.35516E+03 0.03539  2.84198E+02 0.04876  2.38320E+02 0.02917  2.46155E+02 0.03807  2.26011E+02 0.03513  2.55327E+02 0.07350  2.45251E+02 0.05228  2.47617E+02 0.02114  2.46510E+02 0.02969  4.11622E+02 0.05201  7.00554E+02 0.04957  8.68578E+02 0.04186  2.20264E+03 0.01949  2.15011E+03 0.01847  2.10898E+03 0.01689  1.17615E+03 0.01872  7.62146E+02 0.04634  5.22341E+02 0.07894  5.71536E+02 0.01087  8.46677E+02 0.03517  9.96208E+02 0.03875  1.51809E+03 0.04107  2.09409E+03 0.01246  3.16743E+03 0.01278  2.34491E+03 0.03641  1.88641E+03 0.02068  1.46158E+03 0.04163  1.32626E+03 0.02200  1.45027E+03 0.02037  1.34491E+03 0.00778  9.47792E+02 0.02007  9.34106E+02 0.02268  8.45721E+02 0.04531  7.09534E+02 0.01831  5.77923E+02 0.03455  3.77176E+02 0.00906  1.08623E+02 0.06309 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.75951E+00 0.01177 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  5.38190E-01 0.00331  3.02345E-02 0.00157 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.57515E-01 0.00045  4.12897E-01 0.00097 ];
INF_CAPT                  (idx, [1:   4]) = [  1.80564E-03 0.00664  1.57271E-02 0.00341 ];
INF_ABS                   (idx, [1:   4]) = [  4.15254E-03 0.00509  1.01155E-01 0.00357 ];
INF_FISS                  (idx, [1:   4]) = [  2.34689E-03 0.00407  8.54284E-02 0.00363 ];
INF_NSF                   (idx, [1:   4]) = [  5.73391E-03 0.00405  2.08115E-01 0.00363 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44319E+00 2.5E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 6.4E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.56524E-08 0.00362  2.54173E-06 0.00335 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.53335E-01 0.00048  3.10954E-01 0.00451 ];
INF_SCATT1                (idx, [1:   4]) = [  1.94295E-02 0.00898  1.75774E-02 0.01330 ];
INF_SCATT2                (idx, [1:   4]) = [  4.01291E-03 0.04557  1.94870E-03 0.24887 ];
INF_SCATT3                (idx, [1:   4]) = [  7.70141E-04 0.05805  5.21336E-04 0.33421 ];
INF_SCATT4                (idx, [1:   4]) = [  3.88542E-04 0.39239  6.67489E-04 0.65355 ];
INF_SCATT5                (idx, [1:   4]) = [  4.17028E-05 1.00000 -2.54884E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.56055E-04 0.45186  1.09923E-03 0.35439 ];
INF_SCATT7                (idx, [1:   4]) = [  7.49267E-05 1.00000  7.54805E-04 0.97337 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.53336E-01 0.00048  3.10954E-01 0.00451 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.94300E-02 0.00899  1.75774E-02 0.01330 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.01314E-03 0.04552  1.94870E-03 0.24887 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.70115E-04 0.05807  5.21336E-04 0.33421 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.88336E-04 0.39307  6.67489E-04 0.65355 ];
INF_SCATTP5               (idx, [1:   4]) = [  4.14426E-05 1.00000 -2.54884E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.55864E-04 0.45120  1.09923E-03 0.35439 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.48800E-05 1.00000  7.54805E-04 0.97337 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.07219E-01 0.00267  3.82882E-01 0.00080 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.60863E+00 0.00267  8.70591E-01 0.00080 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.15192E-03 0.00523  1.01155E-01 0.00357 ];
INF_REMXS                 (idx, [1:   4]) = [  5.05916E-03 0.01170  1.02626E-01 0.01220 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.52456E-01 0.00063  8.79159E-04 0.04074  6.83479E-04 0.17946  3.10271E-01 0.00416 ];
INF_S1                    (idx, [1:   8]) = [  1.96400E-02 0.00821 -2.10482E-04 0.06666  2.60223E-04 0.05681  1.73172E-02 0.01340 ];
INF_S2                    (idx, [1:   8]) = [  4.03717E-03 0.04289 -2.42594E-05 0.44179  4.63894E-05 1.00000  1.90231E-03 0.24908 ];
INF_S3                    (idx, [1:   8]) = [  7.85549E-04 0.05145 -1.54079E-05 0.50667  1.25032E-05 1.00000  5.08833E-04 0.27852 ];
INF_S4                    (idx, [1:   8]) = [  3.90838E-04 0.39488 -2.29590E-06 1.00000  2.32404E-05 1.00000  6.44249E-04 0.66765 ];
INF_S5                    (idx, [1:   8]) = [  3.23128E-05 1.00000  9.38997E-06 0.30942 -1.84888E-05 1.00000 -2.36396E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.66083E-04 0.40206 -1.00285E-05 0.40989 -1.91764E-05 1.00000  1.11841E-03 0.36779 ];
INF_S7                    (idx, [1:   8]) = [  7.79849E-05 1.00000 -3.05817E-06 0.69575 -3.89317E-07 1.00000  7.55194E-04 0.98919 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.52456E-01 0.00063  8.79159E-04 0.04074  6.83479E-04 0.17946  3.10271E-01 0.00416 ];
INF_SP1                   (idx, [1:   8]) = [  1.96405E-02 0.00822 -2.10482E-04 0.06666  2.60223E-04 0.05681  1.73172E-02 0.01340 ];
INF_SP2                   (idx, [1:   8]) = [  4.03740E-03 0.04283 -2.42594E-05 0.44179  4.63894E-05 1.00000  1.90231E-03 0.24908 ];
INF_SP3                   (idx, [1:   8]) = [  7.85523E-04 0.05147 -1.54079E-05 0.50667  1.25032E-05 1.00000  5.08833E-04 0.27852 ];
INF_SP4                   (idx, [1:   8]) = [  3.90632E-04 0.39555 -2.29590E-06 1.00000  2.32404E-05 1.00000  6.44249E-04 0.66765 ];
INF_SP5                   (idx, [1:   8]) = [  3.20527E-05 1.00000  9.38997E-06 0.30942 -1.84888E-05 1.00000 -2.36396E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.65892E-04 0.40138 -1.00285E-05 0.40989 -1.91764E-05 1.00000  1.11841E-03 0.36779 ];
INF_SP7                   (idx, [1:   8]) = [  7.79381E-05 1.00000 -3.05817E-06 0.69575 -3.89317E-07 1.00000  7.55194E-04 0.98919 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.37507E-01 0.00557 -2.16667E-01 0.04614 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.96813E-01 0.02198 -1.25943E-01 0.04482 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.77355E-01 0.02038 -1.26791E-01 0.04641 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.64241E-01 0.01024  5.05447E-01 0.04789 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.61940E-01 0.00560 -1.54483E+00 0.04471 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.71605E-01 0.02242 -2.65699E+00 0.04320 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.98881E-01 0.02067 -2.63994E+00 0.04467 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.15334E-01 0.01014  6.62443E-01 0.04671 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.00422E-03 0.06356  2.93138E-04 0.28837  9.75850E-04 0.18385  7.49413E-04 0.19331  1.03578E-03 0.13635  2.30019E-03 0.11867  6.68620E-04 0.25490  7.20518E-04 0.27614  2.60710E-04 0.32665 ];
LAMBDA                    (idx, [1:  18]) = [  4.59857E-01 0.09648  1.24667E-02 5.0E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 1.9E-09  1.63478E+00 0.0E+00  3.55460E+00 7.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28672E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97430E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82759 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64780E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '2' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.47618E+04 0.03429  7.21723E+04 0.00876  1.64599E+05 0.00499  2.89959E+05 0.00234  3.00454E+05 0.00137  2.75864E+05 0.00230  2.50103E+05 0.00363  2.19939E+05 0.00548  1.95610E+05 0.00335  1.76762E+05 0.00369  1.62977E+05 0.00548  1.52637E+05 0.00427  1.43547E+05 0.00105  1.35959E+05 0.00466  1.31369E+05 0.00129  1.11232E+05 0.00393  1.09841E+05 0.00480  1.05992E+05 0.00772  1.02835E+05 0.00682  1.93423E+05 0.00223  1.74581E+05 0.00810  1.17855E+05 0.00124  7.23894E+04 0.00133  7.94561E+04 0.00647  7.13436E+04 0.00385  5.60313E+04 0.00193  9.28280E+04 0.00302  1.86346E+04 0.00650  2.27415E+04 0.00704  2.03676E+04 0.00228  1.17913E+04 0.01019  2.00460E+04 0.00423  1.31857E+04 0.00904  1.10222E+04 0.01355  2.09087E+03 0.04876  2.09047E+03 0.01235  2.15071E+03 0.01849  2.17925E+03 0.02888  2.14819E+03 0.00547  2.04599E+03 0.03320  2.04584E+03 0.02622  1.97680E+03 0.00959  3.64268E+03 0.02112  5.87704E+03 0.01691  7.26786E+03 0.00156  1.83981E+04 0.00665  1.78173E+04 0.01123  1.70834E+04 0.00913  9.98239E+03 0.00244  6.36587E+03 0.01557  4.44826E+03 0.01358  4.68811E+03 0.02199  7.40073E+03 0.00486  8.31669E+03 0.01602  1.36595E+04 0.00793  1.86565E+04 0.00218  2.92322E+04 0.01275  2.19047E+04 0.00775  1.79220E+04 0.00397  1.43098E+04 0.01323  1.36383E+04 0.00795  1.44920E+04 0.00895  1.31853E+04 0.00882  9.44239E+03 0.00738  9.12689E+03 0.00470  8.67659E+03 0.01230  7.63480E+03 0.00879  6.05903E+03 0.00744  3.79722E+03 0.01612  1.21300E+03 0.01126 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.78650E+00 0.00211 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.14498E+00 0.00044  2.79104E-01 0.00375 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.58218E-01 0.00017  4.03998E-01 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  1.70788E-03 0.00347  1.44228E-02 0.00155 ];
INF_ABS                   (idx, [1:   4]) = [  3.84867E-03 0.00259  9.23204E-02 0.00158 ];
INF_FISS                  (idx, [1:   4]) = [  2.14080E-03 0.00192  7.78976E-02 0.00159 ];
INF_NSF                   (idx, [1:   4]) = [  5.22932E-03 0.00191  1.89769E-01 0.00159 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44270E+00 1.2E-05  2.43614E+00 1.3E-08 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 3.3E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.84558E-08 0.00272  2.64767E-06 0.00134 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.54375E-01 0.00014  3.11351E-01 0.00114 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97104E-02 0.00266  1.82019E-02 0.00897 ];
INF_SCATT2                (idx, [1:   4]) = [  3.75514E-03 0.02086  1.54860E-03 0.06072 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33059E-04 0.05823  4.82021E-04 0.15007 ];
INF_SCATT4                (idx, [1:   4]) = [  5.56162E-06 1.00000  7.50508E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.44115E-05 1.00000 -1.45690E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.04157E-04 0.56742 -8.87243E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -6.35095E-06 1.00000 -8.10770E-05 0.25749 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.54376E-01 0.00014  3.11351E-01 0.00114 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97105E-02 0.00266  1.82019E-02 0.00897 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.75504E-03 0.02088  1.54860E-03 0.06072 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33045E-04 0.05831  4.82021E-04 0.15007 ];
INF_SCATTP4               (idx, [1:   4]) = [  5.47576E-06 1.00000  7.50508E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.44040E-05 1.00000 -1.45690E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.04170E-04 0.56685 -8.87243E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -6.30594E-06 1.00000 -8.10770E-05 0.25749 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.08105E-01 0.00037  3.75163E-01 0.00048 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.60176E+00 0.00037  8.88502E-01 0.00048 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  3.84763E-03 0.00263  9.23204E-02 0.00158 ];
INF_REMXS                 (idx, [1:   4]) = [  4.81093E-03 0.00496  9.31712E-02 0.00552 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.53407E-01 0.00015  9.68380E-04 0.01039  5.23569E-04 0.03709  3.10827E-01 0.00116 ];
INF_S1                    (idx, [1:   8]) = [  1.99309E-02 0.00243 -2.20498E-04 0.01776  1.15624E-04 0.13235  1.80862E-02 0.00921 ];
INF_S2                    (idx, [1:   8]) = [  3.78053E-03 0.02020 -2.53919E-05 0.08031 -2.42974E-06 1.00000  1.55103E-03 0.06998 ];
INF_S3                    (idx, [1:   8]) = [  7.48735E-04 0.05776 -1.56760E-05 0.12302 -1.98688E-05 0.46861  5.01890E-04 0.15930 ];
INF_S4                    (idx, [1:   8]) = [  1.21118E-05 1.00000 -6.55017E-06 0.55075 -2.27792E-05 0.39113  9.78300E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [ -1.79332E-05 1.00000  3.52172E-06 0.42647 -3.39945E-06 1.00000 -1.11696E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.08591E-04 0.55214 -4.43343E-06 0.24251 -7.22787E-06 1.00000 -8.14965E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -1.13695E-05 1.00000  5.01851E-06 0.15754 -5.43024E-06 0.99928 -7.56468E-05 0.31732 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.53408E-01 0.00015  9.68380E-04 0.01039  5.23569E-04 0.03709  3.10827E-01 0.00116 ];
INF_SP1                   (idx, [1:   8]) = [  1.99310E-02 0.00243 -2.20498E-04 0.01776  1.15624E-04 0.13235  1.80862E-02 0.00921 ];
INF_SP2                   (idx, [1:   8]) = [  3.78043E-03 0.02022 -2.53919E-05 0.08031 -2.42974E-06 1.00000  1.55103E-03 0.06998 ];
INF_SP3                   (idx, [1:   8]) = [  7.48721E-04 0.05785 -1.56760E-05 0.12302 -1.98688E-05 0.46861  5.01890E-04 0.15930 ];
INF_SP4                   (idx, [1:   8]) = [  1.20259E-05 1.00000 -6.55017E-06 0.55075 -2.27792E-05 0.39113  9.78300E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [ -1.79257E-05 1.00000  3.52172E-06 0.42647 -3.39945E-06 1.00000 -1.11696E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.08604E-04 0.55158 -4.43343E-06 0.24251 -7.22787E-06 1.00000 -8.14965E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -1.13245E-05 1.00000  5.01851E-06 0.15754 -5.43024E-06 0.99928 -7.56468E-05 0.31732 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.28005E-01 0.00890 -2.34768E-01 0.01670 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.69876E-01 0.00845 -1.34068E-01 0.01648 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.64285E-01 0.02388 -1.36755E-01 0.00510 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.66923E-01 0.00634  5.03765E-01 0.03534 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.78929E-01 0.00887 -1.42063E+00 0.01665 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.09507E-01 0.00840 -2.48766E+00 0.01661 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.18751E-01 0.02334 -2.43757E+00 0.00510 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.08529E-01 0.00637  6.63336E-01 0.03526 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.03730E-03 0.03023  1.92712E-04 0.19878  9.94780E-04 0.08161  7.27569E-04 0.11407  1.36064E-03 0.06409  2.23801E-03 0.05790  7.52849E-04 0.08963  5.98410E-04 0.09235  1.72330E-04 0.20552 ];
LAMBDA                    (idx, [1:  18]) = [  4.26087E-01 0.04920  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28672E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97432E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82758 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64777E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'g' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.93385E+04 0.00662  3.72424E+05 0.00533  7.62021E+05 0.00220  1.59967E+06 0.00220  1.82174E+06 0.00215  1.75095E+06 0.00131  1.67713E+06 0.00202  1.59110E+06 0.00184  1.51216E+06 0.00193  1.46953E+06 0.00123  1.44882E+06 0.00149  1.42077E+06 0.00215  1.39887E+06 0.00171  1.38082E+06 0.00106  1.37924E+06 0.00081  1.20616E+06 0.00099  1.20875E+06 0.00069  1.19483E+06 0.00071  1.17816E+06 0.00090  2.30462E+06 0.00086  2.22053E+06 0.00141  1.59984E+06 0.00140  1.03245E+06 0.00159  1.21379E+06 0.00151  1.16285E+06 0.00091  9.73615E+05 0.00109  1.73620E+06 0.00079  3.53458E+05 0.00173  4.32512E+05 0.00153  3.80585E+05 0.00248  2.18967E+05 0.00155  3.72520E+05 0.00261  2.48674E+05 0.00244  2.12095E+05 0.00204  4.07011E+04 0.00480  4.03250E+04 0.00134  4.12837E+04 0.00568  4.20118E+04 0.00476  4.14897E+04 0.00047  4.08964E+04 0.00377  4.17605E+04 0.00534  3.89786E+04 0.00638  7.33184E+04 0.00267  1.16255E+05 0.00129  1.45663E+05 0.00222  3.73785E+05 0.00150  3.75518E+05 0.00197  3.74596E+05 0.00254  2.26372E+05 0.00399  1.53642E+05 0.00205  1.11329E+05 0.00352  1.20389E+05 0.00355  2.01542E+05 0.00338  2.37013E+05 0.00309  4.09973E+05 0.00209  6.29115E+05 0.00095  1.16071E+06 0.00090  9.84048E+05 0.00183  8.63976E+05 0.00178  7.24348E+05 0.00177  7.37868E+05 0.00225  8.23939E+05 0.00258  7.79394E+05 0.00253  5.80690E+05 0.00181  5.84442E+05 0.00201  5.70424E+05 0.00335  5.29003E+05 0.00347  4.51464E+05 0.00404  3.23894E+05 0.00159  1.27834E+05 0.00488 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.99590E+01 0.00045  1.20837E+01 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.59651E-01 0.00013  5.29182E-01 3.4E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.54164E-05 0.00883  2.94533E-04 0.00056 ];
INF_ABS                   (idx, [1:   4]) = [  1.54164E-05 0.00883  2.94533E-04 0.00056 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  8.68915E-08 0.00132  3.23703E-06 0.00056 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.59635E-01 0.00013  5.28884E-01 3.5E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  2.98634E-02 0.00030  2.82282E-02 0.00139 ];
INF_SCATT2                (idx, [1:   4]) = [  2.82512E-03 0.00205  1.70400E-03 0.02309 ];
INF_SCATT3                (idx, [1:   4]) = [  4.32670E-04 0.04055  3.01261E-04 0.07433 ];
INF_SCATT4                (idx, [1:   4]) = [  3.45083E-05 0.65756  1.27020E-04 0.25884 ];
INF_SCATT5                (idx, [1:   4]) = [ -2.98480E-06 1.00000 -4.40237E-06 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  6.93998E-06 1.00000  5.77366E-05 0.08297 ];
INF_SCATT7                (idx, [1:   4]) = [ -9.66366E-06 1.00000  5.16083E-05 0.83700 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.59635E-01 0.00013  5.28884E-01 3.5E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.98634E-02 0.00030  2.82282E-02 0.00139 ];
INF_SCATTP2               (idx, [1:   4]) = [  2.82512E-03 0.00205  1.70400E-03 0.02309 ];
INF_SCATTP3               (idx, [1:   4]) = [  4.32670E-04 0.04055  3.01261E-04 0.07433 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.45083E-05 0.65756  1.27020E-04 0.25884 ];
INF_SCATTP5               (idx, [1:   4]) = [ -2.98480E-06 1.00000 -4.40237E-06 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  6.93998E-06 1.00000  5.77366E-05 0.08297 ];
INF_SCATTP7               (idx, [1:   4]) = [ -9.66366E-06 1.00000  5.16083E-05 0.83700 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  3.91893E-01 0.00018  4.99881E-01 0.00011 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  8.50572E-01 0.00018  6.66825E-01 0.00011 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.54164E-05 0.00883  2.94533E-04 0.00056 ];
INF_REMXS                 (idx, [1:   4]) = [  3.34670E-03 0.00102  7.10444E-04 0.00452 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.56304E-01 0.00013  3.33104E-03 0.00088  4.11787E-04 0.00937  5.28472E-01 2.8E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.07783E-02 0.00026 -9.14878E-04 0.00121  1.11835E-04 0.02267  2.81164E-02 0.00145 ];
INF_S2                    (idx, [1:   8]) = [  2.89400E-03 0.00220 -6.88736E-05 0.03600  1.03747E-07 1.00000  1.70390E-03 0.02278 ];
INF_S3                    (idx, [1:   8]) = [  4.40166E-04 0.04153 -7.49605E-06 0.12309 -1.25712E-05 0.15029  3.13832E-04 0.06896 ];
INF_S4                    (idx, [1:   8]) = [  3.66412E-05 0.69584 -2.13297E-06 1.00000 -1.08412E-05 0.12489  1.37861E-04 0.22949 ];
INF_S5                    (idx, [1:   8]) = [  5.35254E-07 1.00000 -3.52006E-06 0.59488 -9.59132E-06 0.08107  5.18895E-06 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  8.31641E-06 1.00000 -1.37643E-06 1.00000 -5.10669E-06 0.25131  6.28433E-05 0.08738 ];
INF_S7                    (idx, [1:   8]) = [ -7.27079E-06 1.00000 -2.39287E-06 1.00000 -8.78509E-07 0.61248  5.24868E-05 0.81356 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.56304E-01 0.00013  3.33104E-03 0.00088  4.11787E-04 0.00937  5.28472E-01 2.8E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.07783E-02 0.00026 -9.14878E-04 0.00121  1.11835E-04 0.02267  2.81164E-02 0.00145 ];
INF_SP2                   (idx, [1:   8]) = [  2.89400E-03 0.00220 -6.88736E-05 0.03600  1.03747E-07 1.00000  1.70390E-03 0.02278 ];
INF_SP3                   (idx, [1:   8]) = [  4.40166E-04 0.04153 -7.49605E-06 0.12309 -1.25712E-05 0.15029  3.13832E-04 0.06896 ];
INF_SP4                   (idx, [1:   8]) = [  3.66412E-05 0.69584 -2.13297E-06 1.00000 -1.08412E-05 0.12489  1.37861E-04 0.22949 ];
INF_SP5                   (idx, [1:   8]) = [  5.35254E-07 1.00000 -3.52006E-06 0.59488 -9.59132E-06 0.08107  5.18895E-06 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  8.31641E-06 1.00000 -1.37643E-06 1.00000 -5.10669E-06 0.25131  6.28433E-05 0.08738 ];
INF_SP7                   (idx, [1:   8]) = [ -7.27079E-06 1.00000 -2.39287E-06 1.00000 -8.78509E-07 0.61248  5.24868E-05 0.81356 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.01120E-01 0.00065  5.54952E-01 0.00331 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.74747E-01 0.00062  5.51087E-01 0.00384 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.74798E-01 0.00107  5.58283E-01 0.00514 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.87941E-01 0.00322  5.55681E-01 0.01109 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.65738E+00 0.00065  6.00665E-01 0.00331 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.90752E+00 0.00062  6.04883E-01 0.00382 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.90697E+00 0.00107  5.97100E-01 0.00517 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.15767E+00 0.00323  6.00012E-01 0.01104 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  80]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi1500' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:36 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.04269E+00  9.96507E-01  9.97187E-01  9.78710E-01  9.86411E-01  9.98493E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.68515E-02 0.00265  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.83149E-01 4.5E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39687E-01 8.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.40268E-01 8.2E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.81077E+00 0.00048  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30339E-01 1.3E-05  6.72353E-02 0.00017  2.42576E-03 0.00071  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.36266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.33441E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  2.99711E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.57785E+00 0.00241  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 2999868 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  4.99978E+04 0.00107 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.28672E+01 ;
RUNNING_TIME              (idx, 1)        =  5.97435E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  7.56833E-02  7.56833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  6.25000E-03  6.25000E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.89188E+00  5.89188E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  5.97330E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.82756 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.87980E+00 0.00709 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64772E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1279.23 ;
MEMSIZE                   (idx, 1)        = 1188.14 ;
XS_MEMSIZE                (idx, 1)        = 582.59 ;
MAT_MEMSIZE               (idx, 1)        = 260.01 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 91.09 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 460144 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00037E-05 0.00046  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.63601E-02 0.00460 ];
U235_FISS                 (idx, [1:   4]) = [  4.34944E-01 0.00089  9.99266E-01 2.5E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.19674E-04 0.03343  7.34333E-04 0.03338 ];
U235_CAPT                 (idx, [1:   4]) = [  1.65794E-01 0.00154  5.86692E-01 0.00101 ];
U238_CAPT                 (idx, [1:   4]) = [  1.58399E-02 0.00451  5.60540E-02 0.00447 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 2999868 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 7.80666E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 847308 8.47616E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1305118 1.30555E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 847442 8.47617E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 2999868 3.00078E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.83122E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.40931E-11 0.00042 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06314E+00 0.00042 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.34868E-01 0.00042 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.82543E-01 0.00048 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.17411E-01 0.00035 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00018E+00 0.00046 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50246E+02 0.00043 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82589E-01 0.00089 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.33654E+01 0.00049 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05853E+00 0.00075 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47698E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.09886E-01 0.00144 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.62200E+00 0.00144 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85940E-01 0.00032 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12871E-01 0.00021 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.48287E+00 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06390E+00 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44473E+00 4.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06374E+00 0.00076  1.05611E+00 0.00073  7.78796E-03 0.01040 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06296E+00 0.00071 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06341E+00 0.00042 ];
ABS_KINF                  (idx, [1:   2]) = [  1.48243E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.32998E+01 0.00035 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33075E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.35170E-05 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  3.32497E-05 0.00342 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.73178E-02 0.00420 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.73368E-02 0.00113 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.20451E-03 0.00710  2.09985E-04 0.03948  9.38822E-04 0.01736  5.98318E-04 0.02250  1.20900E-03 0.01446  1.95761E-03 0.01277  5.85687E-04 0.02421  5.53520E-04 0.02709  1.51568E-04 0.04564 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.22307E-01 0.01064  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.29827E-03 0.01043  2.57269E-04 0.05893  1.07439E-03 0.02851  7.12334E-04 0.03301  1.42818E-03 0.02475  2.30437E-03 0.01476  6.98853E-04 0.03815  6.47402E-04 0.03769  1.75470E-04 0.08048 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.21274E-01 0.01695  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67995E-05 0.00437  3.68215E-05 0.00434  3.39104E-05 0.04412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.91429E-05 0.00420  3.91662E-05 0.00417  3.60721E-05 0.04417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30511E-03 0.01023  2.61199E-04 0.05897  1.07108E-03 0.02392  7.24973E-04 0.03175  1.44226E-03 0.02330  2.27247E-03 0.01620  6.76972E-04 0.03628  6.76324E-04 0.04405  1.79833E-04 0.05775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.26102E-01 0.01560  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24578E-05 0.04061  3.24702E-05 0.04066  2.87054E-05 0.13583 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.45324E-05 0.04060  3.45454E-05 0.04065  3.05636E-05 0.13629 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.86069E-03 0.05585  2.99698E-04 0.17812  1.07018E-03 0.08945  6.52142E-04 0.11789  1.38361E-03 0.08381  2.12549E-03 0.07417  5.38900E-04 0.12148  6.56047E-04 0.13707  1.34616E-04 0.22826 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.99655E-01 0.04740  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.0E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.77377E-03 0.05458  2.82432E-04 0.17768  1.04804E-03 0.08799  6.54178E-04 0.11081  1.38842E-03 0.07929  2.10066E-03 0.07289  5.18854E-04 0.11113  6.41592E-04 0.12836  1.39598E-04 0.22273 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.03865E-01 0.04589  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.8E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.4E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.13670E+02 0.04278 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62958E-05 0.00173 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86078E-05 0.00149 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.51174E-03 0.00511 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06978E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.20960E-07 0.00194 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86471E-05 0.00061  1.86501E-05 0.00062  1.82064E-05 0.00746 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.91913E-04 0.00197  1.91957E-04 0.00195  1.85029E-04 0.02558 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.19325E-01 0.00119  2.19147E-01 0.00122  2.48379E-01 0.01347 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.30427E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.33441E+01 0.00050  5.40546E+01 0.00071 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '_' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CAPT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_ABS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP1               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP2               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP3               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP4               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP5               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP6               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP7               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_REMXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP1                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP2                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP3                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP4                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP5                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP6                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP7                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

