/***********************************************************
******************* Full Model of Core *********************
************************************************************/
include materials.txt
include fuelElementMaterials.txt
include FuelChannel.txt
include FuelElement.txt
include UnloadedCentralFuelElement.txt
include core.txt
include reflector.txt


% - surfaces
surf outerCoreRad cylz 0 0 42.6 0 132.08
surf outerReflect cylz 0 0 50.8 0 132.08

% - cells
cell core      0 fill 30 -outerCoreRad
cell Reflector 0 fill 31 -outerReflect outerCore
cell outerCell 0 outside outerCoreRad




% - plotting
set pop 10000 100 50
plot 3 5000 5000 1 -50 50 -50 50 % xy plot