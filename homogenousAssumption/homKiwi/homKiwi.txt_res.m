
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33327E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79610E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42315 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76610E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   2]) = '40' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.73687E+05 0.00220  4.59795E+06 0.00055  1.03837E+07 0.00056  1.87291E+07 0.00029  1.98565E+07 0.00031  1.84654E+07 0.00015  1.68546E+07 0.00021  1.49936E+07 0.00025  1.33798E+07 0.00030  1.20990E+07 0.00036  1.11500E+07 0.00042  1.03998E+07 0.00017  9.68506E+06 0.00025  9.29510E+06 0.00041  8.95146E+06 0.00029  7.61745E+06 0.00022  7.46985E+06 0.00042  7.17004E+06 0.00040  6.84333E+06 0.00046  1.27004E+07 0.00033  1.11501E+07 0.00034  7.35175E+06 0.00031  4.43918E+06 0.00039  4.78373E+06 0.00071  4.25986E+06 0.00064  3.39983E+06 0.00082  5.67438E+06 0.00064  1.14956E+06 0.00106  1.41092E+06 0.00069  1.27102E+06 0.00053  7.21037E+05 0.00107  1.24471E+06 0.00082  8.29388E+05 0.00095  6.84901E+05 0.00100  1.26713E+05 0.00234  1.24202E+05 0.00254  1.27206E+05 0.00285  1.30528E+05 0.00105  1.29083E+05 0.00193  1.27159E+05 0.00235  1.29847E+05 0.00143  1.21835E+05 0.00162  2.27568E+05 0.00134  3.59183E+05 0.00140  4.48507E+05 0.00157  1.13751E+06 0.00065  1.11477E+06 0.00097  1.07331E+06 0.00123  6.19301E+05 0.00184  4.05169E+05 0.00043  2.87983E+05 0.00148  3.06297E+05 0.00159  5.07198E+05 0.00114  5.87099E+05 0.00058  9.91837E+05 0.00088  1.47241E+06 0.00085  2.60777E+06 0.00188  2.15893E+06 0.00206  1.87556E+06 0.00210  1.56177E+06 0.00227  1.58057E+06 0.00214  1.75539E+06 0.00249  1.65554E+06 0.00251  1.22736E+06 0.00262  1.23268E+06 0.00260  1.19835E+06 0.00244  1.11067E+06 0.00254  9.43427E+05 0.00218  6.73501E+05 0.00297  2.63519E+05 0.00280 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.49088E+00 0.00021 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36575E+02 0.00014  1.36039E+01 0.00177 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.38237E-01 0.00012  5.25241E-01 3.1E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.82380E-03 0.00043  2.25318E-03 0.00133 ];
INF_ABS                   (idx, [1:   4]) = [  4.11718E-03 0.00034  1.13597E-02 0.00157 ];
INF_FISS                  (idx, [1:   4]) = [  2.29338E-03 0.00033  9.10648E-03 0.00164 ];
INF_NSF                   (idx, [1:   4]) = [  5.61434E-03 0.00033  2.21846E-02 0.00164 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44806E+00 2.0E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.0E-08  2.02270E+02 5.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.56126E-08 0.00044  3.12070E-06 0.00032 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.34119E-01 0.00012  5.13879E-01 6.3E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  3.71951E-02 0.00015  3.10951E-02 0.00148 ];
INF_SCATT2                (idx, [1:   4]) = [  9.99343E-03 0.00067  3.23609E-03 0.00363 ];
INF_SCATT3                (idx, [1:   4]) = [  9.68192E-04 0.00763  7.87511E-04 0.01981 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.84568E-04 0.00299  2.47129E-04 0.03152 ];
INF_SCATT5                (idx, [1:   4]) = [  8.60008E-05 0.03501  9.36707E-05 0.25060 ];
INF_SCATT6                (idx, [1:   4]) = [  4.01884E-04 0.01063  3.46934E-05 0.48343 ];
INF_SCATT7                (idx, [1:   4]) = [  4.98341E-05 0.07120  3.93219E-05 0.19888 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.34121E-01 0.00012  5.13879E-01 6.3E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.71951E-02 0.00015  3.10951E-02 0.00148 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.99344E-03 0.00067  3.23609E-03 0.00363 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.68193E-04 0.00762  7.87511E-04 0.01981 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.84559E-04 0.00299  2.47129E-04 0.03152 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.59964E-05 0.03503  9.36707E-05 0.25060 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.01879E-04 0.01063  3.46934E-05 0.48343 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.98252E-05 0.07111  3.93219E-05 0.19888 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.51865E-01 0.00018  4.92316E-01 0.00011 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.32346E+00 0.00018  6.77072E-01 0.00011 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.11534E-03 0.00034  1.13597E-02 0.00157 ];
INF_REMXS                 (idx, [1:   4]) = [  5.80107E-03 0.00021  1.18465E-02 0.00163 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.32436E-01 0.00012  1.68304E-03 0.00048  4.84288E-04 0.00253  5.13394E-01 6.4E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.73722E-02 0.00014 -1.77121E-04 0.00210  1.28310E-04 0.00612  3.09668E-02 0.00148 ];
INF_S2                    (idx, [1:   8]) = [  1.00700E-02 0.00070 -7.65276E-05 0.00751  5.26411E-06 0.17666  3.23083E-03 0.00369 ];
INF_S3                    (idx, [1:   8]) = [  1.05397E-03 0.00693 -8.57749E-05 0.00291 -1.28778E-05 0.03498  8.00389E-04 0.01988 ];
INF_S4                    (idx, [1:   8]) = [ -5.56009E-04 0.00312 -2.85583E-05 0.01400 -1.00621E-05 0.04703  2.57191E-04 0.02929 ];
INF_S5                    (idx, [1:   8]) = [  7.91544E-05 0.04039  6.84643E-06 0.03378 -7.08646E-06 0.05032  1.00757E-04 0.23238 ];
INF_S6                    (idx, [1:   8]) = [  3.94817E-04 0.01057  7.06702E-06 0.01826 -5.09614E-06 0.11950  3.97895E-05 0.42415 ];
INF_S7                    (idx, [1:   8]) = [  4.97699E-05 0.06886  6.41801E-08 1.00000 -4.00329E-06 0.10873  4.33252E-05 0.18834 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.32438E-01 0.00012  1.68304E-03 0.00048  4.84288E-04 0.00253  5.13394E-01 6.4E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.73722E-02 0.00014 -1.77121E-04 0.00210  1.28310E-04 0.00612  3.09668E-02 0.00148 ];
INF_SP2                   (idx, [1:   8]) = [  1.00700E-02 0.00070 -7.65276E-05 0.00751  5.26411E-06 0.17666  3.23083E-03 0.00369 ];
INF_SP3                   (idx, [1:   8]) = [  1.05397E-03 0.00693 -8.57749E-05 0.00291 -1.28778E-05 0.03498  8.00389E-04 0.01988 ];
INF_SP4                   (idx, [1:   8]) = [ -5.56000E-04 0.00312 -2.85583E-05 0.01400 -1.00621E-05 0.04703  2.57191E-04 0.02929 ];
INF_SP5                   (idx, [1:   8]) = [  7.91500E-05 0.04041  6.84643E-06 0.03378 -7.08646E-06 0.05032  1.00757E-04 0.23238 ];
INF_SP6                   (idx, [1:   8]) = [  3.94812E-04 0.01057  7.06702E-06 0.01826 -5.09614E-06 0.11950  3.97895E-05 0.42415 ];
INF_SP7                   (idx, [1:   8]) = [  4.97610E-05 0.06878  6.41801E-08 1.00000 -4.00329E-06 0.10873  4.33252E-05 0.18834 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.82488E-01 0.00034  7.78515E-01 0.00126 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.85830E-01 0.00051  9.84111E-01 0.00423 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.85923E-01 0.00037  9.94203E-01 0.00561 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.75949E-01 0.00058  5.46068E-01 0.00159 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.17999E+00 0.00034  4.28169E-01 0.00126 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.16620E+00 0.00051  3.38745E-01 0.00422 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.16582E+00 0.00037  3.35329E-01 0.00554 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.20796E+00 0.00058  6.10433E-01 0.00158 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
LAMBDA                    (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33327E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79611E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42315 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76607E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'F' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.43625E+05 0.00324  2.56454E+06 0.00128  5.88108E+06 0.00043  1.03457E+07 0.00078  1.08137E+07 0.00057  1.00022E+07 0.00064  9.04591E+06 0.00067  7.92758E+06 0.00052  6.94272E+06 0.00036  6.13186E+06 0.00037  5.51693E+06 0.00037  5.02412E+06 0.00040  4.56971E+06 0.00036  4.30083E+06 0.00052  4.06703E+06 0.00067  3.40200E+06 0.00059  3.28922E+06 0.00040  3.09184E+06 0.00059  2.88215E+06 0.00085  5.13272E+06 0.00120  4.15640E+06 0.00078  2.49495E+06 0.00066  1.38961E+06 0.00094  1.32970E+06 0.00066  1.04793E+06 0.00065  7.68020E+05 0.00084  1.11290E+06 0.00065  2.20982E+05 0.00262  2.73686E+05 0.00175  2.58352E+05 0.00101  1.42147E+05 0.00319  2.54474E+05 0.00250  1.68689E+05 0.00237  1.29720E+05 0.00210  2.13321E+04 0.00502  2.08449E+04 0.00455  2.13287E+04 0.00427  2.20817E+04 0.00407  2.18405E+04 0.00549  2.16743E+04 0.00242  2.23408E+04 0.00405  2.08100E+04 0.00668  3.86128E+04 0.00426  6.03563E+04 0.00153  7.36438E+04 0.00317  1.80431E+05 0.00270  1.62758E+05 0.00289  1.36690E+05 0.00410  6.63014E+04 0.00159  3.70510E+04 0.00246  2.35342E+04 0.00345  2.28995E+04 0.00353  3.45342E+04 0.00490  3.57678E+04 0.00402  5.06811E+04 0.00395  5.43552E+04 0.00226  5.72982E+04 0.00230  2.92669E+04 0.00424  1.84200E+04 0.00485  1.24377E+04 0.00677  1.04932E+04 0.01097  9.83695E+03 0.00488  7.78488E+03 0.00461  4.95203E+03 0.00470  4.39962E+03 0.00492  3.74838E+03 0.00797  3.00413E+03 0.00468  2.13238E+03 0.00860  1.22187E+03 0.00772  3.33503E+02 0.02852 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.60807E+00 0.00036 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  6.28683E+01 0.00042  3.94914E-01 0.00170 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56673E-01 4.4E-05  4.67259E-01 0.00020 ];
INF_CAPT                  (idx, [1:   4]) = [  2.35409E-03 0.00014  2.35930E-02 0.00073 ];
INF_ABS                   (idx, [1:   4]) = [  6.33475E-03 0.00014  1.52557E-01 0.00060 ];
INF_FISS                  (idx, [1:   4]) = [  3.98066E-03 0.00019  1.28964E-01 0.00058 ];
INF_NSF                   (idx, [1:   4]) = [  9.74781E-03 0.00018  3.14174E-01 0.00058 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44879E+00 2.1E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.3E-08  2.02270E+02 8.2E-09 ];
INF_INVV                  (idx, [1:   4]) = [  2.38088E-08 0.00057  1.57660E-06 0.00044 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50339E-01 3.6E-05  3.14785E-01 0.00025 ];
INF_SCATT1                (idx, [1:   4]) = [  2.05899E-02 0.00034  1.83819E-02 0.00590 ];
INF_SCATT2                (idx, [1:   4]) = [  4.49491E-03 0.00369  1.24498E-03 0.13137 ];
INF_SCATT3                (idx, [1:   4]) = [  1.00981E-03 0.00761  2.68516E-04 0.41492 ];
INF_SCATT4                (idx, [1:   4]) = [  2.39452E-04 0.02695  6.85052E-05 0.87641 ];
INF_SCATT5                (idx, [1:   4]) = [  1.35665E-04 0.04283  2.78035E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  9.30217E-05 0.03773 -1.58501E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.74763E-05 0.13063 -1.05587E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50342E-01 3.6E-05  3.14785E-01 0.00025 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.05899E-02 0.00034  1.83819E-02 0.00590 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.49492E-03 0.00369  1.24498E-03 0.13137 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.00981E-03 0.00761  2.68516E-04 0.41492 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.39468E-04 0.02697  6.85052E-05 0.87641 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.35673E-04 0.04281  2.78035E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  9.30036E-05 0.03767 -1.58501E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.74507E-05 0.13048 -1.05587E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02941E-01 0.00014  4.31283E-01 0.00044 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64251E+00 0.00014  7.72888E-01 0.00044 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.33210E-03 0.00014  1.52557E-01 0.00060 ];
INF_REMXS                 (idx, [1:   4]) = [  6.65451E-03 0.00039  1.54191E-01 0.00089 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50019E-01 3.7E-05  3.20567E-04 0.00146  1.71707E-03 0.01276  3.13068E-01 0.00030 ];
INF_S1                    (idx, [1:   8]) = [  2.06626E-02 0.00034 -7.27891E-05 0.00491  4.18215E-04 0.02312  1.79637E-02 0.00559 ];
INF_S2                    (idx, [1:   8]) = [  4.50342E-03 0.00371 -8.50964E-06 0.01675  1.07632E-06 1.00000  1.24391E-03 0.13396 ];
INF_S3                    (idx, [1:   8]) = [  1.01542E-03 0.00721 -5.61300E-06 0.07186 -5.58107E-05 0.10184  3.24326E-04 0.34730 ];
INF_S4                    (idx, [1:   8]) = [  2.41229E-04 0.02694 -1.77713E-06 0.16014 -4.97006E-05 0.12552  1.18206E-04 0.53436 ];
INF_S5                    (idx, [1:   8]) = [  1.35553E-04 0.04323  1.11827E-07 1.00000 -2.43134E-05 0.19741  5.21170E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  9.24066E-05 0.03790  6.15062E-07 0.33492 -1.34484E-05 0.40957 -2.40171E-06 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.75894E-05 0.12808 -1.13118E-07 1.00000 -1.72770E-05 0.15673  6.71828E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50021E-01 3.7E-05  3.20567E-04 0.00146  1.71707E-03 0.01276  3.13068E-01 0.00030 ];
INF_SP1                   (idx, [1:   8]) = [  2.06627E-02 0.00034 -7.27891E-05 0.00491  4.18215E-04 0.02312  1.79637E-02 0.00559 ];
INF_SP2                   (idx, [1:   8]) = [  4.50343E-03 0.00371 -8.50964E-06 0.01675  1.07632E-06 1.00000  1.24391E-03 0.13396 ];
INF_SP3                   (idx, [1:   8]) = [  1.01542E-03 0.00721 -5.61300E-06 0.07186 -5.58107E-05 0.10184  3.24326E-04 0.34730 ];
INF_SP4                   (idx, [1:   8]) = [  2.41245E-04 0.02696 -1.77713E-06 0.16014 -4.97006E-05 0.12552  1.18206E-04 0.53436 ];
INF_SP5                   (idx, [1:   8]) = [  1.35561E-04 0.04321  1.11827E-07 1.00000 -2.43134E-05 0.19741  5.21170E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  9.23886E-05 0.03784  6.15062E-07 0.33492 -1.34484E-05 0.40957 -2.40171E-06 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.75638E-05 0.12791 -1.13118E-07 1.00000 -1.72770E-05 0.15673  6.71828E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.48031E-01 0.00035 -7.94777E+00 0.18550 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  3.99654E-01 0.00095 -7.27924E-01 0.03226 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.00467E-01 0.00074 -7.21573E-01 0.02610 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.76194E-01 0.00055  4.28668E-01 0.01650 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  9.57769E-01 0.00035 -4.84247E-02 0.15720 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  8.34059E-01 0.00095 -4.60409E-01 0.03355 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  8.32364E-01 0.00074 -4.63533E-01 0.02616 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.20688E+00 0.00055  7.78668E-01 0.01658 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.45067E-03 0.00608  2.45922E-04 0.03570  1.10829E-03 0.01633  7.57801E-04 0.01836  1.49107E-03 0.01487  2.35782E-03 0.01018  6.99179E-04 0.01845  6.23545E-04 0.02041  1.67047E-04 0.04055 ];
LAMBDA                    (idx, [1:  18]) = [  4.07653E-01 0.00939  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33328E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79611E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42313 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76604E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'T' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.41032E+04 0.00809  1.61687E+05 0.00244  3.71811E+05 0.00280  6.66677E+05 0.00155  7.10777E+05 0.00148  6.59777E+05 0.00076  5.99736E+05 0.00122  5.24579E+05 0.00141  4.61706E+05 0.00083  4.06432E+05 0.00129  3.65610E+05 0.00148  3.39710E+05 0.00064  3.05092E+05 0.00148  2.96396E+05 0.00061  2.78088E+05 0.00120  2.34704E+05 0.00133  2.27272E+05 0.00136  2.15703E+05 0.00171  2.00182E+05 0.00139  3.57259E+05 0.00136  2.87791E+05 0.00048  1.72410E+05 0.00207  1.00212E+05 0.00118  9.24616E+04 0.00244  7.62482E+04 0.00266  5.27303E+04 0.00426  7.46667E+04 0.00213  1.37257E+04 0.00534  1.75429E+04 0.00498  1.64257E+04 0.00373  9.06532E+03 0.00134  1.60355E+04 0.00537  1.05254E+04 0.00690  8.21072E+03 0.00701  1.47212E+03 0.02184  1.41882E+03 0.01575  1.45742E+03 0.01079  1.49514E+03 0.01974  1.48778E+03 0.00911  1.43065E+03 0.01566  1.46028E+03 0.01602  1.34123E+03 0.02835  2.51515E+03 0.02309  4.00176E+03 0.01065  4.89123E+03 0.01093  1.19475E+04 0.00518  1.10778E+04 0.00835  9.59901E+03 0.00647  4.94691E+03 0.00943  2.93689E+03 0.01112  1.95057E+03 0.01014  1.95374E+03 0.02462  3.06389E+03 0.00928  3.22141E+03 0.00969  4.50256E+03 0.01195  4.96259E+03 0.00789  5.60813E+03 0.00717  3.16346E+03 0.01396  2.13380E+03 0.01238  1.55856E+03 0.01370  1.36334E+03 0.00937  1.39656E+03 0.01019  1.20853E+03 0.01876  8.67831E+02 0.02142  8.41977E+02 0.02448  7.87006E+02 0.01636  7.36078E+02 0.01133  6.22519E+02 0.02284  4.71425E+02 0.01962  2.11361E+02 0.03691 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.19975E+00 0.00037  3.45897E-02 0.00426 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.17632E-01 0.00013  5.86795E-01 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  9.20208E-03 0.00115  2.68893E-02 0.00163 ];
INF_ABS                   (idx, [1:   4]) = [  9.20208E-03 0.00115  2.68893E-02 0.00163 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.39386E-08 0.00070  1.88079E-06 0.00170 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.08412E-01 0.00011  5.59846E-01 0.00064 ];
INF_SCATT1                (idx, [1:   4]) = [  1.49965E-01 0.00047  1.99326E-01 0.00222 ];
INF_SCATT2                (idx, [1:   4]) = [  6.21082E-02 0.00043  8.02259E-02 0.00461 ];
INF_SCATT3                (idx, [1:   4]) = [  2.65966E-03 0.01346  2.38184E-02 0.01483 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.56723E-03 0.00564  3.50541E-03 0.13171 ];
INF_SCATT5                (idx, [1:   4]) = [  2.10605E-04 0.17933 -1.24024E-03 0.21157 ];
INF_SCATT6                (idx, [1:   4]) = [  3.34397E-03 0.00842 -1.54647E-03 0.13806 ];
INF_SCATT7                (idx, [1:   4]) = [  2.28222E-04 0.11863 -3.34454E-04 0.65359 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.08417E-01 0.00012  5.59846E-01 0.00064 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.49965E-01 0.00047  1.99326E-01 0.00222 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.21082E-02 0.00044  8.02259E-02 0.00461 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.65965E-03 0.01342  2.38184E-02 0.01483 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.56712E-03 0.00565  3.50541E-03 0.13171 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.10663E-04 0.17922 -1.24024E-03 0.21157 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.34413E-03 0.00843 -1.54647E-03 0.13806 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.28296E-04 0.11865 -3.34454E-04 0.65359 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.00113E-01 0.00049  3.73719E-01 0.00138 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.66572E+00 0.00049  8.91944E-01 0.00138 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.19747E-03 0.00115  2.68893E-02 0.00163 ];
INF_REMXS                 (idx, [1:   4]) = [  1.20175E-02 0.00149  2.84769E-02 0.00709 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.05614E-01 0.00012  2.79828E-03 0.00271  1.52791E-03 0.04520  5.58318E-01 0.00067 ];
INF_S1                    (idx, [1:   8]) = [  1.48967E-01 0.00045  9.97975E-04 0.00332  3.42111E-04 0.16840  1.98984E-01 0.00211 ];
INF_S2                    (idx, [1:   8]) = [  6.24538E-02 0.00042 -3.45682E-04 0.00635  2.84808E-04 0.15031  7.99411E-02 0.00433 ];
INF_S3                    (idx, [1:   8]) = [  3.18204E-03 0.01184 -5.22381E-04 0.00384  2.03139E-04 0.11187  2.36153E-02 0.01475 ];
INF_S4                    (idx, [1:   8]) = [ -6.41385E-03 0.00602 -1.53378E-04 0.01343  1.47460E-04 0.22328  3.35795E-03 0.13792 ];
INF_S5                    (idx, [1:   8]) = [  1.48393E-04 0.26231  6.22119E-05 0.03574  8.71521E-05 0.31087 -1.32739E-03 0.19096 ];
INF_S6                    (idx, [1:   8]) = [  3.29961E-03 0.00872  4.43598E-05 0.02861  6.72413E-05 0.28759 -1.61371E-03 0.13106 ];
INF_S7                    (idx, [1:   8]) = [  2.31805E-04 0.11658 -3.58301E-06 0.34731  3.36971E-06 1.00000 -3.37824E-04 0.66133 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.05619E-01 0.00012  2.79828E-03 0.00271  1.52791E-03 0.04520  5.58318E-01 0.00067 ];
INF_SP1                   (idx, [1:   8]) = [  1.48967E-01 0.00045  9.97975E-04 0.00332  3.42111E-04 0.16840  1.98984E-01 0.00211 ];
INF_SP2                   (idx, [1:   8]) = [  6.24538E-02 0.00042 -3.45682E-04 0.00635  2.84808E-04 0.15031  7.99411E-02 0.00433 ];
INF_SP3                   (idx, [1:   8]) = [  3.18203E-03 0.01180 -5.22381E-04 0.00384  2.03139E-04 0.11187  2.36153E-02 0.01475 ];
INF_SP4                   (idx, [1:   8]) = [ -6.41375E-03 0.00603 -1.53378E-04 0.01343  1.47460E-04 0.22328  3.35795E-03 0.13792 ];
INF_SP5                   (idx, [1:   8]) = [  1.48451E-04 0.26213  6.22119E-05 0.03574  8.71521E-05 0.31087 -1.32739E-03 0.19096 ];
INF_SP6                   (idx, [1:   8]) = [  3.29977E-03 0.00874  4.43598E-05 0.02861  6.72413E-05 0.28759 -1.61371E-03 0.13106 ];
INF_SP7                   (idx, [1:   8]) = [  2.31879E-04 0.11659 -3.58301E-06 0.34731  3.36971E-06 1.00000 -3.37824E-04 0.66133 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.23848E-01 0.00197  1.29386E+00 0.05125 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.54318E-01 0.00175  5.08421E+00 0.50120 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.54194E-01 0.00311  1.66114E+00 1.00000 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.80647E-01 0.00267  5.06115E-01 0.02925 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.48913E+00 0.00196  2.61220E-01 0.05402 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.31071E+00 0.00174  4.34654E-02 0.65984 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.31140E+00 0.00310  7.84615E-02 0.39927 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.84528E+00 0.00267  6.61733E-01 0.03228 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33328E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79611E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42313 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76601E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'C' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.85572E+04 0.00361  3.70884E+05 0.00165  8.56776E+05 0.00170  1.51820E+06 0.00061  1.60195E+06 0.00077  1.47997E+06 0.00070  1.33107E+06 0.00052  1.16027E+06 0.00083  1.01836E+06 0.00063  9.00154E+05 0.00044  8.11238E+05 0.00073  7.54685E+05 0.00045  6.83246E+05 0.00070  6.65912E+05 0.00059  6.29096E+05 0.00090  5.36869E+05 0.00043  5.24434E+05 0.00097  5.01181E+05 0.00135  4.75638E+05 0.00112  8.73494E+05 0.00078  7.47609E+05 0.00043  4.80668E+05 0.00089  2.82907E+05 0.00068  2.92915E+05 0.00151  2.49378E+05 0.00093  1.88978E+05 0.00102  2.91032E+05 0.00127  5.75966E+04 0.00335  7.05759E+04 0.00180  6.35678E+04 0.00342  3.59069E+04 0.00233  6.20560E+04 0.00302  4.12912E+04 0.00332  3.33213E+04 0.00293  6.03755E+03 0.00675  5.87673E+03 0.00300  5.99633E+03 0.01140  6.19735E+03 0.00748  6.02639E+03 0.00784  5.99276E+03 0.00638  5.99636E+03 0.00437  5.62985E+03 0.00863  1.06751E+04 0.00887  1.66801E+04 0.00287  2.06673E+04 0.00108  5.14314E+04 0.00387  4.89541E+04 0.00338  4.48335E+04 0.00360  2.44489E+04 0.00229  1.51585E+04 0.00418  1.04554E+04 0.00619  1.07329E+04 0.00810  1.69325E+04 0.00270  1.83157E+04 0.00389  2.82759E+04 0.00444  3.50965E+04 0.00352  4.75470E+04 0.00282  3.16244E+04 0.00428  2.43509E+04 0.00181  1.86141E+04 0.00461  1.76613E+04 0.00226  1.86819E+04 0.00291  1.67071E+04 0.00441  1.19819E+04 0.00397  1.16495E+04 0.00371  1.10065E+04 0.00457  1.01008E+04 0.00376  8.41213E+03 0.00626  6.00042E+03 0.00640  2.46369E+03 0.00626 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.90757E+00 0.00018  2.44980E-01 0.00102 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.00337E-01 0.00011  7.12107E-01 0.00010 ];
INF_CAPT                  (idx, [1:   4]) = [  1.96376E-03 0.00070  1.41303E-02 0.00059 ];
INF_ABS                   (idx, [1:   4]) = [  1.96376E-03 0.00070  1.41303E-02 0.00059 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.42403E-08 0.00046  2.35478E-06 0.00060 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.98376E-01 0.00011  6.98078E-01 0.00011 ];
INF_SCATT1                (idx, [1:   4]) = [  1.58865E-01 0.00021  2.01468E-01 0.00114 ];
INF_SCATT2                (idx, [1:   4]) = [  6.33972E-02 0.00028  7.68494E-02 0.00416 ];
INF_SCATT3                (idx, [1:   4]) = [  2.34448E-03 0.01008  2.50501E-02 0.00590 ];
INF_SCATT4                (idx, [1:   4]) = [ -7.16869E-03 0.00257  6.50043E-03 0.01278 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.14849E-05 1.00000  1.36222E-03 0.06654 ];
INF_SCATT6                (idx, [1:   4]) = [  3.35668E-03 0.00789  6.08116E-04 0.35407 ];
INF_SCATT7                (idx, [1:   4]) = [  3.13920E-04 0.03983  8.02617E-04 0.10118 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.98379E-01 0.00011  6.98078E-01 0.00011 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.58865E-01 0.00021  2.01468E-01 0.00114 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.33973E-02 0.00028  7.68494E-02 0.00416 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.34449E-03 0.01008  2.50501E-02 0.00590 ];
INF_SCATTP4               (idx, [1:   4]) = [ -7.16871E-03 0.00257  6.50043E-03 0.01278 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.15127E-05 1.00000  1.36222E-03 0.06654 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.35672E-03 0.00789  6.08116E-04 0.35407 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.13988E-04 0.03986  8.02617E-04 0.10118 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.71993E-01 0.00027  4.94770E-01 0.00047 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.22552E+00 0.00027  6.73714E-01 0.00047 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.96076E-03 0.00075  1.41303E-02 0.00059 ];
INF_REMXS                 (idx, [1:   4]) = [  6.87243E-03 0.00089  1.52844E-02 0.00296 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.93464E-01 0.00010  4.91197E-03 0.00094  1.25548E-03 0.01333  6.96823E-01 0.00013 ];
INF_S1                    (idx, [1:   8]) = [  1.57206E-01 0.00021  1.65878E-03 0.00171  3.21057E-04 0.05715  2.01146E-01 0.00109 ];
INF_S2                    (idx, [1:   8]) = [  6.39234E-02 0.00029 -5.26189E-04 0.00454  1.99307E-04 0.03578  7.66501E-02 0.00413 ];
INF_S3                    (idx, [1:   8]) = [  3.21952E-03 0.00722 -8.75042E-04 0.00158  1.42216E-04 0.03546  2.49079E-02 0.00584 ];
INF_S4                    (idx, [1:   8]) = [ -6.87402E-03 0.00286 -2.94672E-04 0.00820  9.91169E-05 0.09097  6.40131E-03 0.01288 ];
INF_S5                    (idx, [1:   8]) = [ -8.95699E-05 0.26946  7.80850E-05 0.02768  6.18916E-05 0.10882  1.30032E-03 0.07181 ];
INF_S6                    (idx, [1:   8]) = [  3.27792E-03 0.00779  7.87527E-05 0.02002  3.15573E-05 0.25020  5.76558E-04 0.37243 ];
INF_S7                    (idx, [1:   8]) = [  3.10464E-04 0.03754  3.45584E-06 0.65113  3.71126E-06 1.00000  7.98906E-04 0.10605 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.93467E-01 0.00010  4.91197E-03 0.00094  1.25548E-03 0.01333  6.96823E-01 0.00013 ];
INF_SP1                   (idx, [1:   8]) = [  1.57207E-01 0.00021  1.65878E-03 0.00171  3.21057E-04 0.05715  2.01146E-01 0.00109 ];
INF_SP2                   (idx, [1:   8]) = [  6.39235E-02 0.00028 -5.26189E-04 0.00454  1.99307E-04 0.03578  7.66501E-02 0.00413 ];
INF_SP3                   (idx, [1:   8]) = [  3.21953E-03 0.00721 -8.75042E-04 0.00158  1.42216E-04 0.03546  2.49079E-02 0.00584 ];
INF_SP4                   (idx, [1:   8]) = [ -6.87404E-03 0.00286 -2.94672E-04 0.00820  9.91169E-05 0.09097  6.40131E-03 0.01288 ];
INF_SP5                   (idx, [1:   8]) = [ -8.95977E-05 0.26969  7.80850E-05 0.02768  6.18916E-05 0.10882  1.30032E-03 0.07181 ];
INF_SP6                   (idx, [1:   8]) = [  3.27797E-03 0.00779  7.87527E-05 0.02002  3.15573E-05 0.25020  5.76558E-04 0.37243 ];
INF_SP7                   (idx, [1:   8]) = [  3.10532E-04 0.03758  3.45584E-06 0.65113  3.71126E-06 1.00000  7.98906E-04 0.10605 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.22207E-01 0.00077 -2.63341E-01 0.00537 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.62352E-01 0.00186 -1.44960E-01 0.00539 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.62966E-01 0.00147 -1.45997E-01 0.00501 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.69885E-01 0.00142  4.25082E-01 0.01744 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.50011E+00 0.00077 -1.26596E+00 0.00530 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.27058E+00 0.00186 -2.29981E+00 0.00533 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.26761E+00 0.00147 -2.28344E+00 0.00499 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.96214E+00 0.00142  7.85354E-01 0.01742 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33328E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79612E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42313 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76599E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '9' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.64704E+04 0.01024  1.26248E+05 0.00412  2.90553E+05 0.00162  4.99112E+05 0.00177  5.10315E+05 0.00146  4.65126E+05 0.00175  4.13919E+05 0.00091  3.60231E+05 0.00175  3.12803E+05 0.00115  2.75772E+05 0.00278  2.47699E+05 0.00252  2.26493E+05 0.00199  2.06711E+05 0.00203  1.96378E+05 0.00122  1.87242E+05 0.00145  1.58107E+05 0.00191  1.53957E+05 0.00191  1.46251E+05 0.00222  1.38657E+05 0.00071  2.53239E+05 0.00286  2.15967E+05 0.00067  1.37800E+05 0.00406  8.01783E+04 0.00303  8.19179E+04 0.00227  6.88217E+04 0.00129  5.32695E+04 0.00353  8.22836E+04 0.00523  1.72780E+04 0.00773  2.15872E+04 0.00397  2.03749E+04 0.00627  1.11945E+04 0.00473  1.99373E+04 0.00451  1.34371E+04 0.00368  1.04062E+04 0.00598  1.78406E+03 0.02282  1.73997E+03 0.01428  1.80124E+03 0.01230  1.87791E+03 0.01794  1.84358E+03 0.01417  1.86429E+03 0.00856  1.86644E+03 0.02356  1.75034E+03 0.01579  3.28040E+03 0.01233  5.08449E+03 0.00129  6.37023E+03 0.01249  1.57206E+04 0.00393  1.45818E+04 0.00542  1.28248E+04 0.00496  6.55027E+03 0.01022  3.85714E+03 0.01382  2.54010E+03 0.01406  2.51971E+03 0.00945  3.88465E+03 0.00696  4.19821E+03 0.01181  6.34466E+03 0.00814  7.53560E+03 0.00677  9.26633E+03 0.01069  5.48369E+03 0.00830  3.77728E+03 0.01039  2.67210E+03 0.01791  2.39472E+03 0.00771  2.26225E+03 0.01945  1.80843E+03 0.01654  1.15780E+03 0.01686  1.02102E+03 0.02048  8.66843E+02 0.02046  6.63887E+02 0.00461  4.52729E+02 0.03032  2.46493E+02 0.01652  5.91626E+01 0.02348 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.66914E+00 0.00143 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.03708E+00 0.00080  4.84803E-02 0.00410 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.57007E-01 0.00012  4.73233E-01 0.00068 ];
INF_CAPT                  (idx, [1:   4]) = [  2.51790E-03 0.00163  2.44690E-02 0.00181 ];
INF_ABS                   (idx, [1:   4]) = [  6.63845E-03 0.00108  1.59049E-01 0.00196 ];
INF_FISS                  (idx, [1:   4]) = [  4.12055E-03 0.00078  1.34580E-01 0.00199 ];
INF_NSF                   (idx, [1:   4]) = [  1.00821E-02 0.00078  3.27856E-01 0.00199 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44678E+00 8.2E-06  2.43614E+00 5.8E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 2.7E-08  2.02270E+02 5.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.31861E-08 0.00109  1.83325E-06 0.00171 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50366E-01 9.9E-05  3.14302E-01 0.00118 ];
INF_SCATT1                (idx, [1:   4]) = [  2.02284E-02 0.00179  1.72544E-02 0.02941 ];
INF_SCATT2                (idx, [1:   4]) = [  4.44987E-03 0.00924  1.12184E-03 0.37641 ];
INF_SCATT3                (idx, [1:   4]) = [  9.31507E-04 0.02350  3.00287E-04 0.48872 ];
INF_SCATT4                (idx, [1:   4]) = [  1.59087E-04 0.22942 -1.96398E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  9.92821E-05 0.27486  2.65180E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  9.36060E-05 0.20500 -4.37593E-04 0.46243 ];
INF_SCATT7                (idx, [1:   4]) = [  3.33459E-05 0.49004 -2.45080E-04 0.35780 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50368E-01 9.9E-05  3.14302E-01 0.00118 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.02285E-02 0.00179  1.72544E-02 0.02941 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.44981E-03 0.00923  1.12184E-03 0.37641 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.31626E-04 0.02360  3.00287E-04 0.48872 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.59187E-04 0.22957 -1.96398E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.92107E-05 0.27561  2.65180E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  9.35099E-05 0.20526 -4.37593E-04 0.46243 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.33165E-05 0.48925 -2.45080E-04 0.35780 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03188E-01 0.00050  4.36162E-01 0.00177 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64052E+00 0.00050  7.64254E-01 0.00177 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.63606E-03 0.00111  1.59049E-01 0.00196 ];
INF_REMXS                 (idx, [1:   4]) = [  7.21457E-03 0.00166  1.60145E-01 0.00323 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49792E-01 0.00010  5.73273E-04 0.00316  1.21400E-03 0.02889  3.13088E-01 0.00117 ];
INF_S1                    (idx, [1:   8]) = [  2.03587E-02 0.00177 -1.30301E-04 0.02707  2.72395E-04 0.05110  1.69820E-02 0.03004 ];
INF_S2                    (idx, [1:   8]) = [  4.46621E-03 0.00917 -1.63338E-05 0.12374  2.40075E-05 0.76298  1.09784E-03 0.37921 ];
INF_S3                    (idx, [1:   8]) = [  9.39826E-04 0.02240 -8.31928E-06 0.13687 -7.43821E-06 0.81991  3.07725E-04 0.47587 ];
INF_S4                    (idx, [1:   8]) = [  1.61263E-04 0.22919 -2.17620E-06 0.44122  3.79722E-07 1.00000 -1.96778E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.00139E-04 0.26843 -8.57248E-07 1.00000 -1.19360E-05 0.78753  3.84540E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  9.35232E-05 0.19940  8.28085E-08 1.00000 -3.00205E-05 0.35075 -4.07573E-04 0.49402 ];
INF_S7                    (idx, [1:   8]) = [  3.25343E-05 0.48999  8.11637E-07 0.94498 -3.94634E-05 0.22416 -2.05616E-04 0.40010 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49795E-01 0.00010  5.73273E-04 0.00316  1.21400E-03 0.02889  3.13088E-01 0.00117 ];
INF_SP1                   (idx, [1:   8]) = [  2.03588E-02 0.00176 -1.30301E-04 0.02707  2.72395E-04 0.05110  1.69820E-02 0.03004 ];
INF_SP2                   (idx, [1:   8]) = [  4.46615E-03 0.00916 -1.63338E-05 0.12374  2.40075E-05 0.76298  1.09784E-03 0.37921 ];
INF_SP3                   (idx, [1:   8]) = [  9.39945E-04 0.02249 -8.31928E-06 0.13687 -7.43821E-06 0.81991  3.07725E-04 0.47587 ];
INF_SP4                   (idx, [1:   8]) = [  1.61363E-04 0.22934 -2.17620E-06 0.44122  3.79722E-07 1.00000 -1.96778E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.00068E-04 0.26916 -8.57248E-07 1.00000 -1.19360E-05 0.78753  3.84540E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  9.34271E-05 0.19966  8.28085E-08 1.00000 -3.00205E-05 0.35075 -4.07573E-04 0.49402 ];
INF_SP7                   (idx, [1:   8]) = [  3.25049E-05 0.48920  8.11637E-07 0.94498 -3.94634E-05 0.22416 -2.05616E-04 0.40010 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.23963E-01 0.00304 -2.19851E-01 0.00972 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.32433E-01 0.00559 -1.19909E-01 0.01613 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.22888E-01 0.00715 -1.20351E-01 0.01259 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.04431E-01 0.00352  3.40824E-01 0.08335 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.86269E-01 0.00305 -1.51689E+00 0.00968 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.26154E-01 0.00560 -2.78358E+00 0.01643 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.37648E-01 0.00711 -2.77185E+00 0.01250 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.09501E+00 0.00350  1.00476E+00 0.06410 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.27177E-03 0.01624  2.46757E-04 0.08128  1.09123E-03 0.04628  6.62780E-04 0.04518  1.54008E-03 0.03490  2.28668E-03 0.02867  6.63757E-04 0.04235  6.02570E-04 0.04550  1.77908E-04 0.11793 ];
LAMBDA                    (idx, [1:  18]) = [  4.13620E-01 0.02600  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33329E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79612E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42312 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76596E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '8' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.58415E+04 0.00865  1.70108E+05 0.00182  3.93605E+05 0.00410  6.71189E+05 0.00229  6.89603E+05 0.00129  6.27684E+05 0.00195  5.58362E+05 0.00151  4.84345E+05 0.00192  4.22000E+05 0.00099  3.72366E+05 0.00165  3.35604E+05 0.00243  3.07608E+05 0.00199  2.81823E+05 0.00196  2.68842E+05 0.00083  2.55633E+05 0.00193  2.16956E+05 0.00199  2.11049E+05 0.00225  2.02200E+05 0.00194  1.91417E+05 0.00159  3.52333E+05 0.00125  3.03029E+05 0.00188  1.95729E+05 0.00166  1.16025E+05 0.00168  1.19142E+05 0.00306  1.01291E+05 0.00309  7.92515E+04 0.00347  1.24975E+05 0.00274  2.59957E+04 0.00696  3.23658E+04 0.00240  3.01469E+04 0.00509  1.68946E+04 0.00364  2.95074E+04 0.00540  1.98055E+04 0.00614  1.56309E+04 0.00523  2.72080E+03 0.01828  2.61978E+03 0.02042  2.74958E+03 0.01219  2.76363E+03 0.01374  2.80243E+03 0.01832  2.73339E+03 0.01419  2.82483E+03 0.00738  2.64699E+03 0.00934  4.86356E+03 0.00785  7.76650E+03 0.00721  9.46692E+03 0.00710  2.38911E+04 0.00522  2.25173E+04 0.00815  2.01706E+04 0.00628  1.04438E+04 0.00736  6.28762E+03 0.00951  4.15755E+03 0.00407  4.18306E+03 0.00944  6.46455E+03 0.00720  7.21234E+03 0.00720  1.07815E+04 0.00508  1.32587E+04 0.00418  1.75096E+04 0.00362  1.08639E+04 0.00731  7.79268E+03 0.00760  5.53904E+03 0.00672  5.02232E+03 0.00858  4.88680E+03 0.00662  4.14241E+03 0.00677  2.69062E+03 0.00797  2.37235E+03 0.01133  2.01193E+03 0.00962  1.63733E+03 0.01099  1.11841E+03 0.01371  5.93678E+02 0.02073  1.47783E+02 0.02871 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.68826E+00 0.00104 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.16272E+00 0.00058  8.58950E-02 0.00265 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56572E-01 0.00014  4.63403E-01 0.00022 ];
INF_CAPT                  (idx, [1:   4]) = [  2.38017E-03 0.00126  2.30453E-02 0.00071 ];
INF_ABS                   (idx, [1:   4]) = [  6.15269E-03 0.00096  1.49988E-01 0.00066 ];
INF_FISS                  (idx, [1:   4]) = [  3.77253E-03 0.00080  1.26943E-01 0.00066 ];
INF_NSF                   (idx, [1:   4]) = [  9.22799E-03 0.00079  3.09251E-01 0.00066 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44610E+00 6.3E-06  2.43614E+00 8.2E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 3.9E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.55853E-08 0.00113  1.96315E-06 0.00053 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50417E-01 0.00013  3.13519E-01 0.00116 ];
INF_SCATT1                (idx, [1:   4]) = [  2.01733E-02 0.00143  1.82733E-02 0.01916 ];
INF_SCATT2                (idx, [1:   4]) = [  4.36731E-03 0.00408  1.56007E-03 0.12639 ];
INF_SCATT3                (idx, [1:   4]) = [  9.80115E-04 0.02130  3.58721E-04 0.46454 ];
INF_SCATT4                (idx, [1:   4]) = [  1.89180E-04 0.10858 -2.73542E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  8.92184E-05 0.16513 -2.01465E-04 0.95858 ];
INF_SCATT6                (idx, [1:   4]) = [  1.15931E-04 0.19700 -3.36470E-04 0.48066 ];
INF_SCATT7                (idx, [1:   4]) = [  2.62360E-05 0.93084 -1.29003E-04 0.73062 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50419E-01 0.00013  3.13519E-01 0.00116 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.01732E-02 0.00143  1.82733E-02 0.01916 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.36729E-03 0.00407  1.56007E-03 0.12639 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.80084E-04 0.02131  3.58721E-04 0.46454 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.89117E-04 0.10890 -2.73542E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.91787E-05 0.16532 -2.01465E-04 0.95858 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.15926E-04 0.19695 -3.36470E-04 0.48066 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.61748E-05 0.93260 -1.29003E-04 0.73062 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03250E-01 0.00030  4.26378E-01 0.00077 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64002E+00 0.00030  7.81782E-01 0.00077 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.15013E-03 0.00099  1.49988E-01 0.00066 ];
INF_REMXS                 (idx, [1:   4]) = [  6.78716E-03 0.00164  1.50883E-01 0.00246 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49785E-01 0.00014  6.31641E-04 0.00330  9.99242E-04 0.01574  3.12520E-01 0.00119 ];
INF_S1                    (idx, [1:   8]) = [  2.03192E-02 0.00142 -1.45945E-04 0.00246  2.52217E-04 0.05883  1.80211E-02 0.01970 ];
INF_S2                    (idx, [1:   8]) = [  4.38214E-03 0.00428 -1.48255E-05 0.11419  2.34965E-05 0.40975  1.53657E-03 0.12800 ];
INF_S3                    (idx, [1:   8]) = [  9.91476E-04 0.01976 -1.13614E-05 0.19395 -3.16215E-05 0.37411  3.90343E-04 0.41415 ];
INF_S4                    (idx, [1:   8]) = [  1.91100E-04 0.10886 -1.92004E-06 0.49695 -2.83663E-05 0.19955  1.01214E-06 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  8.99426E-05 0.16194 -7.24214E-07 0.95662 -2.72067E-05 0.21705 -1.74258E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.14528E-04 0.20137  1.40314E-06 0.73167 -2.18188E-05 0.32393 -3.14651E-04 0.49890 ];
INF_S7                    (idx, [1:   8]) = [  2.63200E-05 0.92990 -8.39389E-08 1.00000 -2.32975E-05 0.25209 -1.05705E-04 0.88078 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49787E-01 0.00014  6.31641E-04 0.00330  9.99242E-04 0.01574  3.12520E-01 0.00119 ];
INF_SP1                   (idx, [1:   8]) = [  2.03192E-02 0.00142 -1.45945E-04 0.00246  2.52217E-04 0.05883  1.80211E-02 0.01970 ];
INF_SP2                   (idx, [1:   8]) = [  4.38212E-03 0.00427 -1.48255E-05 0.11419  2.34965E-05 0.40975  1.53657E-03 0.12800 ];
INF_SP3                   (idx, [1:   8]) = [  9.91446E-04 0.01978 -1.13614E-05 0.19395 -3.16215E-05 0.37411  3.90343E-04 0.41415 ];
INF_SP4                   (idx, [1:   8]) = [  1.91037E-04 0.10918 -1.92004E-06 0.49695 -2.83663E-05 0.19955  1.01214E-06 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  8.99029E-05 0.16211 -7.24214E-07 0.95662 -2.72067E-05 0.21705 -1.74258E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.14523E-04 0.20132  1.40314E-06 0.73167 -2.18188E-05 0.32393 -3.14651E-04 0.49890 ];
INF_SP7                   (idx, [1:   8]) = [  2.62588E-05 0.93164 -8.39389E-08 1.00000 -2.32975E-05 0.25209 -1.05705E-04 0.88078 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.29663E-01 0.00182 -1.94376E-01 0.01476 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.32985E-01 0.00556 -1.09229E-01 0.00845 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.30635E-01 0.00497 -1.10519E-01 0.01292 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.10462E-01 0.00175  3.64222E-01 0.03563 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.75815E-01 0.00182 -1.71677E+00 0.01478 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.25505E-01 0.00553 -3.05278E+00 0.00838 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.28256E-01 0.00498 -3.01860E+00 0.01292 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.07368E+00 0.00175  9.21086E-01 0.03603 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.26316E-03 0.01362  2.26907E-04 0.07888  1.06378E-03 0.03788  6.82764E-04 0.04071  1.49903E-03 0.03068  2.30984E-03 0.02231  7.28808E-04 0.04520  5.84892E-04 0.04531  1.67145E-04 0.08726 ];
LAMBDA                    (idx, [1:  18]) = [  4.11479E-01 0.02049  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 7.0E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33329E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79613E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42313 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76594E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '7' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.21456E+04 0.01153  1.05320E+05 0.00585  2.41037E+05 0.00254  4.11494E+05 0.00282  4.22788E+05 0.00174  3.85344E+05 0.00223  3.44004E+05 0.00213  2.97758E+05 0.00071  2.60717E+05 0.00247  2.30064E+05 0.00198  2.07739E+05 0.00238  1.90869E+05 0.00217  1.75611E+05 0.00256  1.66890E+05 0.00252  1.60291E+05 0.00355  1.35872E+05 0.00296  1.33323E+05 0.00257  1.28000E+05 0.00112  1.21736E+05 0.00266  2.25051E+05 0.00075  1.96932E+05 0.00204  1.29158E+05 0.00288  7.68801E+04 0.00194  8.07395E+04 0.00151  6.91551E+04 0.00297  5.40708E+04 0.00410  8.71262E+04 0.00345  1.81338E+04 0.00561  2.24135E+04 0.00503  2.05965E+04 0.00688  1.14803E+04 0.00643  2.04371E+04 0.00342  1.35535E+04 0.00659  1.08143E+04 0.00739  1.88600E+03 0.01791  1.82987E+03 0.01052  1.94160E+03 0.01186  1.96471E+03 0.01372  1.99107E+03 0.01398  1.95638E+03 0.00730  1.96561E+03 0.02110  1.79181E+03 0.01308  3.39196E+03 0.01700  5.34749E+03 0.00459  6.80882E+03 0.00809  1.67474E+04 0.00447  1.58515E+04 0.00440  1.47902E+04 0.00895  7.69329E+03 0.00457  4.71171E+03 0.00859  3.18660E+03 0.01640  3.23170E+03 0.01018  5.16119E+03 0.00950  5.72031E+03 0.01149  8.71249E+03 0.00939  1.10003E+04 0.01003  1.51491E+04 0.00657  9.68028E+03 0.00735  7.35935E+03 0.00401  5.45027E+03 0.00511  5.04570E+03 0.00836  5.03960E+03 0.00896  4.25521E+03 0.00888  2.85782E+03 0.01105  2.60244E+03 0.00706  2.30712E+03 0.01099  1.80684E+03 0.01012  1.34769E+03 0.01511  7.28526E+02 0.01188  1.82784E+02 0.02116 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.71124E+00 0.00191 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.61234E+00 0.00078  7.19293E-02 0.00154 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56668E-01 0.00021  4.55801E-01 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  2.27715E-03 0.00099  2.19377E-02 0.00121 ];
INF_ABS                   (idx, [1:   4]) = [  5.76725E-03 0.00099  1.42845E-01 0.00126 ];
INF_FISS                  (idx, [1:   4]) = [  3.49010E-03 0.00111  1.20907E-01 0.00127 ];
INF_NSF                   (idx, [1:   4]) = [  8.53443E-03 0.00110  2.94547E-01 0.00127 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44533E+00 8.4E-06  2.43614E+00 8.2E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 4.5E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.85442E-08 0.00086  2.11814E-06 0.00109 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50892E-01 0.00022  3.12246E-01 0.00167 ];
INF_SCATT1                (idx, [1:   4]) = [  2.00264E-02 0.00346  1.78683E-02 0.02181 ];
INF_SCATT2                (idx, [1:   4]) = [  4.26403E-03 0.00522  1.92298E-03 0.15991 ];
INF_SCATT3                (idx, [1:   4]) = [  8.50652E-04 0.04105  4.58423E-04 0.50311 ];
INF_SCATT4                (idx, [1:   4]) = [  1.57114E-04 0.15177  4.26737E-04 0.44249 ];
INF_SCATT5                (idx, [1:   4]) = [  1.12612E-04 0.24744  2.69208E-04 0.33953 ];
INF_SCATT6                (idx, [1:   4]) = [  8.58304E-05 0.25801  2.64637E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.37836E-05 1.00000 -1.09677E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50894E-01 0.00022  3.12246E-01 0.00167 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.00264E-02 0.00346  1.78683E-02 0.02181 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.26399E-03 0.00523  1.92298E-03 0.15991 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.50711E-04 0.04101  4.58423E-04 0.50311 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.57172E-04 0.15187  4.26737E-04 0.44249 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.12506E-04 0.24749  2.69208E-04 0.33953 ];
INF_SCATTP6               (idx, [1:   4]) = [  8.57761E-05 0.25808  2.64637E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.38270E-05 1.00000 -1.09677E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03734E-01 0.00082  4.19511E-01 0.00136 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63613E+00 0.00082  7.94583E-01 0.00135 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.76543E-03 0.00097  1.42845E-01 0.00126 ];
INF_REMXS                 (idx, [1:   4]) = [  6.48464E-03 0.00211  1.44476E-01 0.00339 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50184E-01 0.00021  7.08761E-04 0.00703  9.20084E-04 0.05485  3.11325E-01 0.00163 ];
INF_S1                    (idx, [1:   8]) = [  2.01922E-02 0.00327 -1.65892E-04 0.02374  2.07700E-04 0.08150  1.76606E-02 0.02196 ];
INF_S2                    (idx, [1:   8]) = [  4.28104E-03 0.00553 -1.70095E-05 0.11837 -5.99635E-06 1.00000  1.92898E-03 0.15854 ];
INF_S3                    (idx, [1:   8]) = [  8.62450E-04 0.03954 -1.17981E-05 0.08438 -2.13624E-05 0.55040  4.79785E-04 0.48558 ];
INF_S4                    (idx, [1:   8]) = [  1.60653E-04 0.15332 -3.53921E-06 0.31508 -2.44941E-05 0.43944  4.51231E-04 0.42362 ];
INF_S5                    (idx, [1:   8]) = [  1.11794E-04 0.25094  8.17680E-07 1.00000 -1.68359E-05 0.66421  2.86044E-04 0.29154 ];
INF_S6                    (idx, [1:   8]) = [  8.46001E-05 0.25729  1.23038E-06 0.78403 -9.59704E-06 0.93332  3.60608E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.30733E-05 1.00000  7.10260E-07 1.00000 -8.42813E-06 0.52607 -2.53956E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50185E-01 0.00021  7.08761E-04 0.00703  9.20084E-04 0.05485  3.11325E-01 0.00163 ];
INF_SP1                   (idx, [1:   8]) = [  2.01923E-02 0.00327 -1.65892E-04 0.02374  2.07700E-04 0.08150  1.76606E-02 0.02196 ];
INF_SP2                   (idx, [1:   8]) = [  4.28100E-03 0.00554 -1.70095E-05 0.11837 -5.99635E-06 1.00000  1.92898E-03 0.15854 ];
INF_SP3                   (idx, [1:   8]) = [  8.62509E-04 0.03949 -1.17981E-05 0.08438 -2.13624E-05 0.55040  4.79785E-04 0.48558 ];
INF_SP4                   (idx, [1:   8]) = [  1.60711E-04 0.15342 -3.53921E-06 0.31508 -2.44941E-05 0.43944  4.51231E-04 0.42362 ];
INF_SP5                   (idx, [1:   8]) = [  1.11689E-04 0.25100  8.17680E-07 1.00000 -1.68359E-05 0.66421  2.86044E-04 0.29154 ];
INF_SP6                   (idx, [1:   8]) = [  8.45457E-05 0.25736  1.23038E-06 0.78403 -9.59704E-06 0.93332  3.60608E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.31167E-05 1.00000  7.10260E-07 1.00000 -8.42813E-06 0.52607 -2.53956E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.34194E-01 0.00519 -1.81291E-01 0.01758 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.27120E-01 0.00725 -1.03466E-01 0.01267 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.31097E-01 0.00897 -1.05537E-01 0.00992 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.19598E-01 0.00255  3.95603E-01 0.06494 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.67810E-01 0.00520 -1.84164E+00 0.01843 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.32535E-01 0.00733 -3.22435E+00 0.01306 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.27885E-01 0.00900 -3.16000E+00 0.00983 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.04301E+00 0.00255  8.59420E-01 0.06067 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.87718E-03 0.01615  2.38467E-04 0.09372  1.04901E-03 0.04059  6.62865E-04 0.05808  1.36669E-03 0.03471  2.21493E-03 0.03030  6.30069E-04 0.05705  5.65026E-04 0.05624  1.50123E-04 0.09684 ];
LAMBDA                    (idx, [1:  18]) = [  4.04607E-01 0.02509  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33329E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79613E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42312 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76591E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '6' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.22969E+04 0.01556  5.71646E+04 0.00400  1.30965E+05 0.00368  2.23615E+05 0.00397  2.29542E+05 0.00211  2.08186E+05 0.00255  1.86322E+05 0.00309  1.61600E+05 0.00152  1.41021E+05 0.00140  1.24964E+05 0.00281  1.13848E+05 0.00062  1.04324E+05 0.00180  9.63614E+04 0.00326  9.18068E+04 0.00251  8.82677E+04 0.00329  7.50484E+04 0.00394  7.33920E+04 0.00540  7.03609E+04 0.00209  6.69587E+04 0.00466  1.24677E+05 0.00348  1.09446E+05 0.00178  7.18837E+04 0.00324  4.29321E+04 0.00423  4.52561E+04 0.00337  3.93519E+04 0.00425  3.12715E+04 0.00174  4.98167E+04 0.00311  1.02881E+04 0.00939  1.27941E+04 0.00530  1.16588E+04 0.00932  6.55490E+03 0.00999  1.13235E+04 0.00645  7.68998E+03 0.01111  6.09635E+03 0.00913  1.08639E+03 0.02964  1.07508E+03 0.01282  1.08299E+03 0.01770  1.09892E+03 0.02195  1.14240E+03 0.03143  1.11187E+03 0.01279  1.13817E+03 0.02248  1.03967E+03 0.01722  1.96288E+03 0.00947  3.04709E+03 0.01226  3.85835E+03 0.00700  9.63543E+03 0.00979  9.31594E+03 0.01029  8.44698E+03 0.00809  4.61583E+03 0.00713  2.83704E+03 0.01195  1.83296E+03 0.01390  1.93657E+03 0.01359  3.17727E+03 0.00620  3.46688E+03 0.01198  5.42112E+03 0.00588  6.98445E+03 0.00513  9.59326E+03 0.00885  6.49369E+03 0.00709  4.93739E+03 0.00822  3.73518E+03 0.00911  3.43273E+03 0.01161  3.51221E+03 0.01542  2.99371E+03 0.01022  2.05135E+03 0.00428  1.89002E+03 0.01122  1.65291E+03 0.01752  1.38384E+03 0.02248  1.00508E+03 0.01108  5.85243E+02 0.02080  1.66070E+02 0.02864 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.72516E+00 0.00213 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.43205E+00 0.00081  4.57296E-02 0.00114 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55991E-01 9.8E-05  4.42875E-01 0.00015 ];
INF_CAPT                  (idx, [1:   4]) = [  2.09958E-03 0.00075  2.00788E-02 0.00064 ];
INF_ABS                   (idx, [1:   4]) = [  5.21169E-03 0.00051  1.30480E-01 0.00052 ];
INF_FISS                  (idx, [1:   4]) = [  3.11211E-03 0.00039  1.10401E-01 0.00051 ];
INF_NSF                   (idx, [1:   4]) = [  7.60919E-03 0.00039  2.68952E-01 0.00051 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44503E+00 4.4E-06  2.43614E+00 8.2E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 3.2E-08  2.02270E+02 8.2E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.97453E-08 0.00112  2.21094E-06 0.00044 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50775E-01 8.9E-05  3.12110E-01 0.00185 ];
INF_SCATT1                (idx, [1:   4]) = [  1.99155E-02 0.00363  1.73157E-02 0.02214 ];
INF_SCATT2                (idx, [1:   4]) = [  4.15512E-03 0.01259  8.37447E-04 0.46311 ];
INF_SCATT3                (idx, [1:   4]) = [  9.04892E-04 0.06386 -1.92021E-04 0.47884 ];
INF_SCATT4                (idx, [1:   4]) = [  1.81043E-04 0.28842  5.93771E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  5.29862E-05 0.54505 -1.96917E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  8.46973E-05 0.42847  6.51280E-08 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  4.36941E-06 1.00000  1.50778E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50777E-01 9.0E-05  3.12110E-01 0.00185 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.99156E-02 0.00363  1.73157E-02 0.02214 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.15508E-03 0.01263  8.37447E-04 0.46311 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.04796E-04 0.06380 -1.92021E-04 0.47884 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.80946E-04 0.28834  5.93771E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  5.29976E-05 0.54355 -1.96917E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  8.47104E-05 0.42792  6.51280E-08 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.32433E-06 1.00000  1.50778E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03517E-01 0.00075  4.09000E-01 0.00103 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63787E+00 0.00075  8.15000E-01 0.00103 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.20982E-03 0.00051  1.30480E-01 0.00052 ];
INF_REMXS                 (idx, [1:   4]) = [  5.95763E-03 0.00281  1.31603E-01 0.00424 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50033E-01 0.00011  7.42233E-04 0.00965  8.38320E-04 0.03290  3.11272E-01 0.00189 ];
INF_S1                    (idx, [1:   8]) = [  2.00839E-02 0.00356 -1.68408E-04 0.02137  1.84125E-04 0.17962  1.71315E-02 0.02166 ];
INF_S2                    (idx, [1:   8]) = [  4.17658E-03 0.01235 -2.14585E-05 0.16348 -2.08850E-05 0.36645  8.58332E-04 0.45097 ];
INF_S3                    (idx, [1:   8]) = [  9.15027E-04 0.06527 -1.01351E-05 0.29317 -2.86792E-05 0.29019 -1.63342E-04 0.54309 ];
INF_S4                    (idx, [1:   8]) = [  1.81838E-04 0.29192 -7.95652E-07 1.00000 -2.01911E-05 0.34701  7.95682E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  5.48756E-05 0.52391 -1.88947E-06 0.74331 -2.92328E-05 0.34895 -1.67684E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  8.49505E-05 0.41722 -2.53205E-07 1.00000  2.27793E-06 1.00000 -2.21280E-06 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  6.81088E-06 1.00000 -2.44147E-06 0.45459 -1.73338E-06 1.00000  1.52512E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50035E-01 0.00011  7.42233E-04 0.00965  8.38320E-04 0.03290  3.11272E-01 0.00189 ];
INF_SP1                   (idx, [1:   8]) = [  2.00841E-02 0.00356 -1.68408E-04 0.02137  1.84125E-04 0.17962  1.71315E-02 0.02166 ];
INF_SP2                   (idx, [1:   8]) = [  4.17654E-03 0.01238 -2.14585E-05 0.16348 -2.08850E-05 0.36645  8.58332E-04 0.45097 ];
INF_SP3                   (idx, [1:   8]) = [  9.14931E-04 0.06521 -1.01351E-05 0.29317 -2.86792E-05 0.29019 -1.63342E-04 0.54309 ];
INF_SP4                   (idx, [1:   8]) = [  1.81742E-04 0.29186 -7.95652E-07 1.00000 -2.01911E-05 0.34701  7.95682E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  5.48870E-05 0.52243 -1.88947E-06 0.74331 -2.92328E-05 0.34895 -1.67684E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  8.49636E-05 0.41668 -2.53205E-07 1.00000  2.27793E-06 1.00000 -2.21280E-06 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  6.76580E-06 1.00000 -2.44147E-06 0.45459 -1.73338E-06 1.00000  1.52512E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.23951E-01 0.00447 -1.91765E-01 0.00700 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.02224E-01 0.00865 -1.11380E-01 0.01029 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.01240E-01 0.00775 -1.09917E-01 0.01308 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.23732E-01 0.00559  4.12502E-01 0.03775 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.86333E-01 0.00446 -1.73867E+00 0.00706 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.63968E-01 0.00885 -2.99434E+00 0.01025 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.65214E-01 0.00764 -3.03517E+00 0.01295 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.02982E+00 0.00561  8.13500E-01 0.03543 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.78386E-03 0.02492  1.96964E-04 0.12120  1.08914E-03 0.05765  5.86792E-04 0.06222  1.43065E-03 0.04998  2.17687E-03 0.03969  6.24021E-04 0.07259  5.32007E-04 0.07492  1.47412E-04 0.15652 ];
LAMBDA                    (idx, [1:  18]) = [  3.99005E-01 0.03188  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 7.0E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33329E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79614E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42311 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76589E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '5' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.13563E+04 0.01224  5.48958E+04 0.00644  1.25433E+05 0.00298  2.14778E+05 0.00213  2.22236E+05 0.00201  2.01990E+05 0.00223  1.79946E+05 0.00228  1.57018E+05 0.00117  1.37534E+05 0.00221  1.22145E+05 0.00274  1.11494E+05 0.00327  1.02867E+05 0.00087  9.50122E+04 0.00179  9.07746E+04 0.00595  8.72727E+04 0.00402  7.39244E+04 0.00230  7.26643E+04 0.00206  7.00620E+04 0.00208  6.67537E+04 0.00285  1.24248E+05 0.00217  1.10629E+05 0.00186  7.30290E+04 0.00322  4.41444E+04 0.00380  4.69703E+04 0.00308  4.09864E+04 0.00387  3.24202E+04 0.00403  5.24286E+04 0.00251  1.08389E+04 0.00738  1.33773E+04 0.00557  1.22964E+04 0.00346  6.91742E+03 0.01016  1.19331E+04 0.00749  7.97853E+03 0.00531  6.40589E+03 0.01100  1.15071E+03 0.02089  1.10951E+03 0.01778  1.13832E+03 0.01558  1.17342E+03 0.02038  1.16592E+03 0.01880  1.16061E+03 0.02252  1.20272E+03 0.01453  1.13555E+03 0.01947  2.06343E+03 0.01997  3.29764E+03 0.00832  4.01891E+03 0.00658  1.04060E+04 0.00764  1.01085E+04 0.00761  9.15865E+03 0.00773  4.98384E+03 0.01094  3.07044E+03 0.01833  2.08792E+03 0.01295  2.24146E+03 0.00742  3.53515E+03 0.01915  3.91379E+03 0.01074  6.26265E+03 0.00784  8.15797E+03 0.00844  1.17342E+04 0.00494  8.09590E+03 0.01059  6.43282E+03 0.00775  4.77868E+03 0.00678  4.58151E+03 0.00950  4.67970E+03 0.01229  4.08141E+03 0.00421  2.81909E+03 0.01214  2.65683E+03 0.01104  2.46296E+03 0.00779  2.00288E+03 0.01465  1.43839E+03 0.00862  8.25505E+02 0.02001  2.29798E+02 0.02036 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.74250E+00 0.00205 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.41076E+00 0.00058  5.51648E-02 0.00332 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56161E-01 0.00019  4.31889E-01 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  1.96996E-03 0.00182  1.84787E-02 0.00137 ];
INF_ABS                   (idx, [1:   4]) = [  4.78716E-03 0.00117  1.19895E-01 0.00137 ];
INF_FISS                  (idx, [1:   4]) = [  2.81720E-03 0.00118  1.01417E-01 0.00138 ];
INF_NSF                   (idx, [1:   4]) = [  6.88645E-03 0.00118  2.47065E-01 0.00138 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44443E+00 4.5E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 2.0E-08  2.02270E+02 5.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.19911E-08 0.00191  2.32189E-06 0.00121 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.51367E-01 0.00018  3.11596E-01 0.00106 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98115E-02 0.00535  1.78811E-02 0.01166 ];
INF_SCATT2                (idx, [1:   4]) = [  4.02587E-03 0.01013  1.86435E-03 0.15848 ];
INF_SCATT3                (idx, [1:   4]) = [  7.95711E-04 0.06261  4.63712E-04 0.67834 ];
INF_SCATT4                (idx, [1:   4]) = [  4.03124E-05 0.78955  2.30009E-04 0.62920 ];
INF_SCATT5                (idx, [1:   4]) = [  4.86539E-05 0.75809  2.28028E-04 0.93570 ];
INF_SCATT6                (idx, [1:   4]) = [  2.07436E-05 1.00000 -6.76575E-06 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  5.18443E-05 0.43057 -5.67907E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.51368E-01 0.00018  3.11596E-01 0.00106 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98117E-02 0.00534  1.78811E-02 0.01166 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.02587E-03 0.01014  1.86435E-03 0.15848 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.95700E-04 0.06258  4.63712E-04 0.67834 ];
INF_SCATTP4               (idx, [1:   4]) = [  4.01062E-05 0.79430  2.30009E-04 0.62920 ];
INF_SCATTP5               (idx, [1:   4]) = [  4.86443E-05 0.75900  2.28028E-04 0.93570 ];
INF_SCATTP6               (idx, [1:   4]) = [  2.07739E-05 1.00000 -6.76575E-06 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  5.17764E-05 0.43128 -5.67907E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04454E-01 0.00076  3.99327E-01 0.00088 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63036E+00 0.00076  8.34741E-01 0.00088 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.78621E-03 0.00119  1.19895E-01 0.00137 ];
INF_REMXS                 (idx, [1:   4]) = [  5.61347E-03 0.00366  1.21057E-01 0.00244 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50547E-01 0.00017  8.19817E-04 0.00531  7.64042E-04 0.03673  3.10832E-01 0.00110 ];
INF_S1                    (idx, [1:   8]) = [  1.99939E-02 0.00514 -1.82414E-04 0.02152  1.99311E-04 0.13396  1.76818E-02 0.01225 ];
INF_S2                    (idx, [1:   8]) = [  4.05231E-03 0.01047 -2.64431E-05 0.11350  1.89846E-05 1.00000  1.84537E-03 0.16236 ];
INF_S3                    (idx, [1:   8]) = [  8.10248E-04 0.06184 -1.45365E-05 0.07571 -3.16429E-05 0.57634  4.95355E-04 0.61621 ];
INF_S4                    (idx, [1:   8]) = [  4.44220E-05 0.76797 -4.10957E-06 0.77601 -1.11857E-05 1.00000  2.41195E-04 0.63242 ];
INF_S5                    (idx, [1:   8]) = [  4.72653E-05 0.77456  1.38857E-06 1.00000 -7.46941E-07 1.00000  2.28775E-04 0.94945 ];
INF_S6                    (idx, [1:   8]) = [  2.01214E-05 1.00000  6.22221E-07 1.00000 -6.97937E-06 1.00000  2.13625E-07 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  5.30708E-05 0.42494 -1.22652E-06 1.00000 -1.73238E-05 0.37633 -3.94669E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50548E-01 0.00017  8.19817E-04 0.00531  7.64042E-04 0.03673  3.10832E-01 0.00110 ];
INF_SP1                   (idx, [1:   8]) = [  1.99941E-02 0.00514 -1.82414E-04 0.02152  1.99311E-04 0.13396  1.76818E-02 0.01225 ];
INF_SP2                   (idx, [1:   8]) = [  4.05232E-03 0.01048 -2.64431E-05 0.11350  1.89846E-05 1.00000  1.84537E-03 0.16236 ];
INF_SP3                   (idx, [1:   8]) = [  8.10237E-04 0.06181 -1.45365E-05 0.07571 -3.16429E-05 0.57634  4.95355E-04 0.61621 ];
INF_SP4                   (idx, [1:   8]) = [  4.42158E-05 0.77215 -4.10957E-06 0.77601 -1.11857E-05 1.00000  2.41195E-04 0.63242 ];
INF_SP5                   (idx, [1:   8]) = [  4.72557E-05 0.77550  1.38857E-06 1.00000 -7.46941E-07 1.00000  2.28775E-04 0.94945 ];
INF_SP6                   (idx, [1:   8]) = [  2.01516E-05 1.00000  6.22221E-07 1.00000 -6.97937E-06 1.00000  2.13625E-07 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  5.30029E-05 0.42568 -1.22652E-06 1.00000 -1.73238E-05 0.37633 -3.94669E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.33773E-01 0.00372 -1.85749E-01 0.01429 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.08987E-01 0.00736 -1.07584E-01 0.00974 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.03714E-01 0.00464 -1.07329E-01 0.00765 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.37244E-01 0.00662  4.14593E-01 0.05500 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.68505E-01 0.00373 -1.79635E+00 0.01418 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.55076E-01 0.00746 -3.09983E+00 0.00969 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.61821E-01 0.00462 -3.10661E+00 0.00747 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.88618E-01 0.00655  8.17385E-01 0.06027 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.80660E-03 0.02351  2.03576E-04 0.13564  1.10377E-03 0.05671  6.36125E-04 0.07406  1.35623E-03 0.04756  2.14944E-03 0.03947  6.53352E-04 0.07029  5.49876E-04 0.07554  1.54228E-04 0.13701 ];
LAMBDA                    (idx, [1:  18]) = [  4.10247E-01 0.03493  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33330E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79614E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42311 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76587E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '4' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.19621E+04 0.00786  1.03463E+05 0.00232  2.39008E+05 0.00093  4.12706E+05 0.00225  4.25540E+05 0.00155  3.88300E+05 0.00142  3.48067E+05 0.00161  3.03742E+05 0.00169  2.66491E+05 0.00230  2.38741E+05 0.00151  2.16689E+05 0.00224  2.01533E+05 0.00121  1.86085E+05 0.00235  1.78042E+05 0.00253  1.71667E+05 0.00320  1.45799E+05 0.00164  1.42775E+05 0.00155  1.38482E+05 0.00100  1.32611E+05 0.00117  2.48384E+05 0.00300  2.20202E+05 0.00201  1.47415E+05 0.00172  8.95453E+04 0.00253  9.66933E+04 0.00252  8.49985E+04 0.00445  6.73143E+04 0.00198  1.09839E+05 0.00205  2.23649E+04 0.00507  2.77538E+04 0.00462  2.50721E+04 0.00521  1.41389E+04 0.00668  2.45102E+04 0.00475  1.63550E+04 0.00778  1.32722E+04 0.00512  2.37157E+03 0.01076  2.40910E+03 0.01608  2.38001E+03 0.01700  2.50434E+03 0.01807  2.45281E+03 0.01194  2.43639E+03 0.01379  2.46069E+03 0.01326  2.33328E+03 0.01381  4.26370E+03 0.01255  6.78629E+03 0.00790  8.47220E+03 0.00796  2.15249E+04 0.00436  2.08799E+04 0.00672  1.97136E+04 0.00804  1.06391E+04 0.00752  6.76198E+03 0.00812  4.60427E+03 0.00926  4.84269E+03 0.01074  7.78561E+03 0.00974  8.67470E+03 0.00449  1.37839E+04 0.00925  1.81978E+04 0.00651  2.69326E+04 0.00502  1.88805E+04 0.00227  1.50188E+04 0.00452  1.14583E+04 0.00324  1.07844E+04 0.00665  1.11847E+04 0.00534  9.86305E+03 0.00681  6.85977E+03 0.00840  6.61256E+03 0.00642  5.99584E+03 0.00787  5.09931E+03 0.01187  3.84536E+03 0.00731  2.28970E+03 0.00718  6.32252E+02 0.01939 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.75057E+00 0.00131 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.76472E+00 0.00061  1.25659E-01 0.00264 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56459E-01 0.00016  4.20167E-01 0.00018 ];
INF_CAPT                  (idx, [1:   4]) = [  1.85538E-03 0.00098  1.67978E-02 0.00067 ];
INF_ABS                   (idx, [1:   4]) = [  4.40873E-03 0.00069  1.08519E-01 0.00065 ];
INF_FISS                  (idx, [1:   4]) = [  2.55336E-03 0.00053  9.17216E-02 0.00064 ];
INF_NSF                   (idx, [1:   4]) = [  6.24018E-03 0.00053  2.23447E-01 0.00064 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44391E+00 3.0E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 2.9E-08  2.02270E+02 5.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.40119E-08 0.00119  2.39942E-06 0.00056 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.52028E-01 0.00012  3.11564E-01 0.00060 ];
INF_SCATT1                (idx, [1:   4]) = [  1.98101E-02 0.00246  1.79730E-02 0.01306 ];
INF_SCATT2                (idx, [1:   4]) = [  4.01518E-03 0.00805  1.73011E-03 0.10049 ];
INF_SCATT3                (idx, [1:   4]) = [  7.54699E-04 0.03582  4.21420E-04 0.61742 ];
INF_SCATT4                (idx, [1:   4]) = [  1.19293E-04 0.20151  5.28804E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  6.25079E-05 0.52579  1.15353E-04 0.55824 ];
INF_SCATT6                (idx, [1:   4]) = [  5.53789E-05 0.34836  1.00572E-04 0.90267 ];
INF_SCATT7                (idx, [1:   4]) = [  6.59545E-05 0.38379 -4.29274E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.52029E-01 0.00012  3.11564E-01 0.00060 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.98102E-02 0.00247  1.79730E-02 0.01306 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.01514E-03 0.00805  1.73011E-03 0.10049 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.54662E-04 0.03581  4.21420E-04 0.61742 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.19336E-04 0.20143  5.28804E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  6.24842E-05 0.52673  1.15353E-04 0.55824 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.53724E-05 0.34809  1.00572E-04 0.90267 ];
INF_SCATTP7               (idx, [1:   4]) = [  6.59335E-05 0.38400 -4.29274E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.05231E-01 0.00046  3.89375E-01 0.00064 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.62419E+00 0.00046  8.56074E-01 0.00064 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.40750E-03 0.00070  1.08519E-01 0.00065 ];
INF_REMXS                 (idx, [1:   4]) = [  5.27917E-03 0.00309  1.09237E-01 0.00212 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51180E-01 0.00012  8.47796E-04 0.01091  6.34479E-04 0.02110  3.10930E-01 0.00060 ];
INF_S1                    (idx, [1:   8]) = [  2.00042E-02 0.00265 -1.94041E-04 0.02725  1.52908E-04 0.04856  1.78200E-02 0.01290 ];
INF_S2                    (idx, [1:   8]) = [  4.04055E-03 0.00772 -2.53772E-05 0.08036 -1.35168E-05 0.66960  1.74362E-03 0.10046 ];
INF_S3                    (idx, [1:   8]) = [  7.66427E-04 0.03642 -1.17275E-05 0.10133 -3.72975E-05 0.22265  4.58718E-04 0.57666 ];
INF_S4                    (idx, [1:   8]) = [  1.22595E-04 0.18847 -3.30164E-06 0.52773 -2.05734E-05 0.48007  7.34538E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  6.43527E-05 0.51006 -1.84481E-06 0.76367 -1.45805E-06 1.00000  1.16811E-04 0.54874 ];
INF_S6                    (idx, [1:   8]) = [  5.50597E-05 0.36842  3.19134E-07 1.00000 -1.17641E-06 1.00000  1.01748E-04 0.87530 ];
INF_S7                    (idx, [1:   8]) = [  6.65304E-05 0.37725 -5.75974E-07 1.00000 -1.02860E-05 0.56724  5.99322E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51181E-01 0.00012  8.47796E-04 0.01091  6.34479E-04 0.02110  3.10930E-01 0.00060 ];
INF_SP1                   (idx, [1:   8]) = [  2.00042E-02 0.00265 -1.94041E-04 0.02725  1.52908E-04 0.04856  1.78200E-02 0.01290 ];
INF_SP2                   (idx, [1:   8]) = [  4.04052E-03 0.00773 -2.53772E-05 0.08036 -1.35168E-05 0.66960  1.74362E-03 0.10046 ];
INF_SP3                   (idx, [1:   8]) = [  7.66389E-04 0.03640 -1.17275E-05 0.10133 -3.72975E-05 0.22265  4.58718E-04 0.57666 ];
INF_SP4                   (idx, [1:   8]) = [  1.22638E-04 0.18840 -3.30164E-06 0.52773 -2.05734E-05 0.48007  7.34538E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  6.43290E-05 0.51096 -1.84481E-06 0.76367 -1.45805E-06 1.00000  1.16811E-04 0.54874 ];
INF_SP6                   (idx, [1:   8]) = [  5.50532E-05 0.36814  3.19134E-07 1.00000 -1.17641E-06 1.00000  1.01748E-04 0.87530 ];
INF_SP7                   (idx, [1:   8]) = [  6.65094E-05 0.37745 -5.75974E-07 1.00000 -1.02860E-05 0.56724  5.99322E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.46765E-01 0.00351 -1.98702E-01 0.00457 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.22374E-01 0.00568 -1.15058E-01 0.00599 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.24816E-01 0.00492 -1.13818E-01 0.00876 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.45441E-01 0.00375  4.20513E-01 0.01941 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.46151E-01 0.00350 -1.67773E+00 0.00453 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.38215E-01 0.00564 -2.89761E+00 0.00595 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.35221E-01 0.00496 -2.92975E+00 0.00856 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.65018E-01 0.00374  7.94171E-01 0.01932 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.98986E-03 0.01704  2.37301E-04 0.08654  1.07296E-03 0.04396  7.14425E-04 0.05210  1.36340E-03 0.03800  2.24409E-03 0.03346  7.10760E-04 0.04548  5.17566E-04 0.05477  1.29355E-04 0.11467 ];
LAMBDA                    (idx, [1:  18]) = [  3.86302E-01 0.02433  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33330E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79615E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42311 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76584E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '3' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  4.16099E+03 0.01133  1.93709E+04 0.01566  4.39373E+04 0.00499  7.72543E+04 0.00342  8.04906E+04 0.00566  7.32243E+04 0.00391  6.63829E+04 0.00361  5.82647E+04 0.00408  5.16209E+04 0.00457  4.65268E+04 0.00673  4.24616E+04 0.00682  3.98714E+04 0.00452  3.66532E+04 0.00446  3.52257E+04 0.00532  3.42013E+04 0.00493  2.89016E+04 0.00369  2.83668E+04 0.00457  2.72798E+04 0.00310  2.62293E+04 0.00566  4.91282E+04 0.00236  4.41025E+04 0.00370  2.95804E+04 0.00178  1.80539E+04 0.00775  1.97541E+04 0.00869  1.77281E+04 0.00717  1.39687E+04 0.00651  2.30778E+04 0.00527  4.63324E+03 0.01432  5.83027E+03 0.01125  5.15096E+03 0.01032  2.94966E+03 0.01571  5.03778E+03 0.00811  3.27974E+03 0.00883  2.77353E+03 0.00917  5.06048E+02 0.04581  5.09880E+02 0.02471  4.96546E+02 0.03365  5.17518E+02 0.03538  5.21474E+02 0.02729  5.23195E+02 0.03397  5.06575E+02 0.02775  4.96751E+02 0.04191  9.42735E+02 0.02311  1.45028E+03 0.02100  1.79600E+03 0.02043  4.58668E+03 0.01698  4.47732E+03 0.01256  4.18981E+03 0.00984  2.35499E+03 0.01317  1.50908E+03 0.01980  1.03589E+03 0.02984  1.08888E+03 0.02706  1.76143E+03 0.02009  2.04626E+03 0.02000  3.19941E+03 0.01006  4.30107E+03 0.01355  6.45186E+03 0.00336  4.68374E+03 0.01147  3.64894E+03 0.01051  2.91733E+03 0.01670  2.87083E+03 0.01653  3.06619E+03 0.01548  2.69517E+03 0.00938  1.93909E+03 0.01098  1.82150E+03 0.02635  1.65695E+03 0.01205  1.48988E+03 0.01616  1.15607E+03 0.01485  7.07003E+02 0.02320  2.32093E+02 0.03155 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.77454E+00 0.00473 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  5.39113E-01 0.00081  3.06475E-02 0.00361 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.57602E-01 0.00031  4.12705E-01 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  1.77379E-03 0.00362  1.57018E-02 0.00169 ];
INF_ABS                   (idx, [1:   4]) = [  4.11535E-03 0.00270  1.01000E-01 0.00178 ];
INF_FISS                  (idx, [1:   4]) = [  2.34156E-03 0.00237  8.52982E-02 0.00180 ];
INF_NSF                   (idx, [1:   4]) = [  5.72097E-03 0.00236  2.07798E-01 0.00180 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44323E+00 1.0E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 6.8E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.66431E-08 0.00110  2.53711E-06 0.00156 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.53499E-01 0.00030  3.11027E-01 0.00177 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97526E-02 0.00355  1.83348E-02 0.02763 ];
INF_SCATT2                (idx, [1:   4]) = [  3.86831E-03 0.02597  1.60050E-03 0.27229 ];
INF_SCATT3                (idx, [1:   4]) = [  5.86072E-04 0.13452  4.41048E-04 0.76972 ];
INF_SCATT4                (idx, [1:   4]) = [  1.16088E-04 0.46801  2.68414E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.59865E-04 0.22015  1.50496E-04 0.98379 ];
INF_SCATT6                (idx, [1:   4]) = [  3.14865E-05 1.00000 -4.15361E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -3.33998E-05 1.00000 -3.29841E-04 0.46556 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.53500E-01 0.00029  3.11027E-01 0.00177 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97529E-02 0.00355  1.83348E-02 0.02763 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.86829E-03 0.02595  1.60050E-03 0.27229 ];
INF_SCATTP3               (idx, [1:   4]) = [  5.86027E-04 0.13462  4.41048E-04 0.76972 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.16036E-04 0.46808  2.68414E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.59817E-04 0.22026  1.50496E-04 0.98379 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.12534E-05 1.00000 -4.15361E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -3.34818E-05 1.00000 -3.29841E-04 0.46556 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.06900E-01 0.00072  3.82254E-01 0.00153 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.61109E+00 0.00072  8.72032E-01 0.00152 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.11427E-03 0.00275  1.01000E-01 0.00178 ];
INF_REMXS                 (idx, [1:   4]) = [  5.04129E-03 0.00468  1.02183E-01 0.00456 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.52561E-01 0.00031  9.38561E-04 0.00634  5.05599E-04 0.08277  3.10521E-01 0.00172 ];
INF_S1                    (idx, [1:   8]) = [  1.99665E-02 0.00365 -2.13876E-04 0.03716  9.63534E-05 0.24749  1.82384E-02 0.02743 ];
INF_S2                    (idx, [1:   8]) = [  3.89760E-03 0.02527 -2.92830E-05 0.29656 -5.17686E-06 1.00000  1.60568E-03 0.27189 ];
INF_S3                    (idx, [1:   8]) = [  5.98682E-04 0.13378 -1.26098E-05 0.43698 -2.97933E-05 0.22517  4.70841E-04 0.72902 ];
INF_S4                    (idx, [1:   8]) = [  1.23826E-04 0.42719 -7.73808E-06 0.23746 -3.57210E-06 1.00000  2.71986E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.55034E-04 0.22169  4.83140E-06 0.32815  2.70798E-05 0.29897  1.23416E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  3.13370E-05 1.00000  1.49532E-07 1.00000  1.34906E-05 0.42893 -5.50267E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -2.77978E-05 1.00000 -5.60200E-06 0.53852  8.28861E-07 1.00000 -3.30670E-04 0.47233 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.52562E-01 0.00031  9.38561E-04 0.00634  5.05599E-04 0.08277  3.10521E-01 0.00172 ];
INF_SP1                   (idx, [1:   8]) = [  1.99668E-02 0.00365 -2.13876E-04 0.03716  9.63534E-05 0.24749  1.82384E-02 0.02743 ];
INF_SP2                   (idx, [1:   8]) = [  3.89757E-03 0.02526 -2.92830E-05 0.29656 -5.17686E-06 1.00000  1.60568E-03 0.27189 ];
INF_SP3                   (idx, [1:   8]) = [  5.98637E-04 0.13387 -1.26098E-05 0.43698 -2.97933E-05 0.22517  4.70841E-04 0.72902 ];
INF_SP4                   (idx, [1:   8]) = [  1.23775E-04 0.42723 -7.73808E-06 0.23746 -3.57210E-06 1.00000  2.71986E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.54986E-04 0.22182  4.83140E-06 0.32815  2.70798E-05 0.29897  1.23416E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  3.11038E-05 1.00000  1.49532E-07 1.00000  1.34906E-05 0.42893 -5.50267E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -2.78798E-05 1.00000 -5.60200E-06 0.53852  8.28861E-07 1.00000 -3.30670E-04 0.47233 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.29479E-01 0.00615 -2.18265E-01 0.01512 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.74850E-01 0.01162 -1.22533E-01 0.01638 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.84422E-01 0.01182 -1.27434E-01 0.01281 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.55713E-01 0.01235  4.57383E-01 0.08711 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.76280E-01 0.00615 -1.52898E+00 0.01540 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.02449E-01 0.01158 -2.72400E+00 0.01634 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.88580E-01 0.01167 -2.61787E+00 0.01276 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.37813E-01 0.01255  7.54937E-01 0.08007 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.49001E-03 0.03576  1.93830E-04 0.19864  1.13538E-03 0.08612  6.52187E-04 0.10610  1.34517E-03 0.07953  1.93651E-03 0.05215  5.08314E-04 0.12817  5.63855E-04 0.13216  1.54767E-04 0.24031 ];
LAMBDA                    (idx, [1:  18]) = [  3.87197E-01 0.05038  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.9E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33330E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79615E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42310 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76581E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '2' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.02500E+04 0.00678  1.43130E+05 0.00537  3.30773E+05 0.00194  5.78710E+05 0.00114  6.01880E+05 0.00133  5.53205E+05 0.00159  4.99805E+05 0.00089  4.39753E+05 0.00247  3.92025E+05 0.00251  3.55229E+05 0.00145  3.27556E+05 0.00132  3.05493E+05 0.00164  2.84864E+05 0.00280  2.72628E+05 0.00240  2.62862E+05 0.00174  2.23336E+05 0.00208  2.20451E+05 0.00178  2.12261E+05 0.00192  2.05314E+05 0.00215  3.86564E+05 0.00223  3.48903E+05 0.00111  2.37188E+05 0.00194  1.45854E+05 0.00258  1.60146E+05 0.00241  1.43509E+05 0.00142  1.15016E+05 0.00271  1.90024E+05 0.00096  3.87137E+04 0.00458  4.70181E+04 0.00420  4.20523E+04 0.00570  2.38914E+04 0.00643  4.14278E+04 0.00326  2.75725E+04 0.00468  2.28239E+04 0.00726  4.22374E+03 0.00930  4.08539E+03 0.01344  4.27838E+03 0.01164  4.30607E+03 0.00881  4.23196E+03 0.01028  4.22360E+03 0.00938  4.30401E+03 0.01054  4.09318E+03 0.01232  7.65081E+03 0.01003  1.21302E+04 0.01047  1.50279E+04 0.00649  3.77440E+04 0.00433  3.69034E+04 0.00437  3.52208E+04 0.00318  1.99342E+04 0.00700  1.28359E+04 0.00527  8.90135E+03 0.00790  9.48504E+03 0.00317  1.53777E+04 0.00585  1.73728E+04 0.00640  2.81502E+04 0.00673  3.81450E+04 0.00148  5.92824E+04 0.00182  4.38980E+04 0.00333  3.58458E+04 0.00596  2.85195E+04 0.00528  2.78419E+04 0.00325  2.95263E+04 0.00214  2.69026E+04 0.00217  1.91652E+04 0.00376  1.85205E+04 0.00433  1.73821E+04 0.00552  1.52913E+04 0.00583  1.22561E+04 0.00556  7.79477E+03 0.00304  2.48695E+03 0.01280 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.79378E+00 0.00094 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.15788E+00 0.00090  2.83493E-01 0.00114 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.58299E-01 8.5E-05  4.03801E-01 0.00019 ];
INF_CAPT                  (idx, [1:   4]) = [  1.68488E-03 0.00052  1.44010E-02 0.00068 ];
INF_ABS                   (idx, [1:   4]) = [  3.81875E-03 0.00041  9.21622E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  2.13388E-03 0.00051  7.77612E-02 0.00076 ];
INF_NSF                   (idx, [1:   4]) = [  5.21243E-03 0.00051  1.89437E-01 0.00076 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44271E+00 2.7E-06  2.43614E+00 5.8E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 1.2E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.92986E-08 0.00106  2.64161E-06 0.00068 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.54475E-01 9.3E-05  3.11655E-01 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  1.96217E-02 0.00294  1.81399E-02 0.00428 ];
INF_SCATT2                (idx, [1:   4]) = [  3.69775E-03 0.00650  1.54527E-03 0.09737 ];
INF_SCATT3                (idx, [1:   4]) = [  6.67719E-04 0.04477  2.87048E-04 0.35825 ];
INF_SCATT4                (idx, [1:   4]) = [  8.13302E-05 0.20967  2.22571E-04 0.25439 ];
INF_SCATT5                (idx, [1:   4]) = [  5.15346E-05 0.45055  9.29720E-05 0.62761 ];
INF_SCATT6                (idx, [1:   4]) = [  4.55606E-05 0.62148  5.85370E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.21917E-05 0.44840  6.80832E-05 0.85519 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.54476E-01 9.2E-05  3.11655E-01 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.96217E-02 0.00294  1.81399E-02 0.00428 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.69773E-03 0.00650  1.54527E-03 0.09737 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.67709E-04 0.04473  2.87048E-04 0.35825 ];
INF_SCATTP4               (idx, [1:   4]) = [  8.13502E-05 0.20980  2.22571E-04 0.25439 ];
INF_SCATTP5               (idx, [1:   4]) = [  5.14593E-05 0.45147  9.29720E-05 0.62761 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.55206E-05 0.62146  5.85370E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.21674E-05 0.44872  6.80832E-05 0.85519 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.08280E-01 0.00039  3.74905E-01 0.00016 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.60041E+00 0.00039  8.89114E-01 0.00016 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  3.81781E-03 0.00042  9.21622E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  4.82257E-03 0.00169  9.26882E-02 0.00126 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.53477E-01 0.00011  9.97929E-04 0.00555  5.41503E-04 0.01157  3.11113E-01 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  1.98490E-02 0.00288 -2.27250E-04 0.01017  1.37474E-04 0.06596  1.80024E-02 0.00454 ];
INF_S2                    (idx, [1:   8]) = [  3.72424E-03 0.00652 -2.64928E-05 0.03731 -7.46170E-06 0.55788  1.55273E-03 0.09624 ];
INF_S3                    (idx, [1:   8]) = [  6.85235E-04 0.04315 -1.75158E-05 0.07499 -1.82973E-05 0.18057  3.05346E-04 0.32749 ];
INF_S4                    (idx, [1:   8]) = [  8.64403E-05 0.21199 -5.11004E-06 0.39098 -1.16472E-05 0.32272  2.34218E-04 0.24573 ];
INF_S5                    (idx, [1:   8]) = [  5.38428E-05 0.41983 -2.30823E-06 0.47254 -9.35318E-06 0.25313  1.02325E-04 0.57214 ];
INF_S6                    (idx, [1:   8]) = [  4.40550E-05 0.64288  1.50563E-06 0.59425 -7.24665E-06 0.67028  6.57837E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.93655E-05 0.50334  2.82622E-06 0.21245 -7.89747E-07 1.00000  6.88730E-05 0.83110 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.53478E-01 0.00011  9.97929E-04 0.00555  5.41503E-04 0.01157  3.11113E-01 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  1.98489E-02 0.00288 -2.27250E-04 0.01017  1.37474E-04 0.06596  1.80024E-02 0.00454 ];
INF_SP2                   (idx, [1:   8]) = [  3.72422E-03 0.00651 -2.64928E-05 0.03731 -7.46170E-06 0.55788  1.55273E-03 0.09624 ];
INF_SP3                   (idx, [1:   8]) = [  6.85224E-04 0.04311 -1.75158E-05 0.07499 -1.82973E-05 0.18057  3.05346E-04 0.32749 ];
INF_SP4                   (idx, [1:   8]) = [  8.64603E-05 0.21210 -5.11004E-06 0.39098 -1.16472E-05 0.32272  2.34218E-04 0.24573 ];
INF_SP5                   (idx, [1:   8]) = [  5.37675E-05 0.42066 -2.30823E-06 0.47254 -9.35318E-06 0.25313  1.02325E-04 0.57214 ];
INF_SP6                   (idx, [1:   8]) = [  4.40149E-05 0.64287  1.50563E-06 0.59425 -7.24665E-06 0.67028  6.57837E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.93412E-05 0.50371  2.82622E-06 0.21245 -7.89747E-07 1.00000  6.88730E-05 0.83110 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.26539E-01 0.00169 -2.41274E-01 0.00555 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.65209E-01 0.00284 -1.37256E-01 0.00633 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.65502E-01 0.00420 -1.37251E-01 0.00637 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.65592E-01 0.00257  4.67995E-01 0.01304 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.81494E-01 0.00169 -1.38177E+00 0.00558 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.16553E-01 0.00284 -2.42904E+00 0.00622 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.16136E-01 0.00419 -2.42913E+00 0.00627 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.11793E-01 0.00258  7.12861E-01 0.01295 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.98226E-03 0.01639  2.39979E-04 0.08811  1.10175E-03 0.03766  6.24051E-04 0.04969  1.42928E-03 0.03413  2.20566E-03 0.02606  6.61087E-04 0.05004  5.66806E-04 0.04599  1.53643E-04 0.09168 ];
LAMBDA                    (idx, [1:  18]) = [  4.05744E-01 0.02409  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33330E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79616E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42309 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76580E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'g' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.58900E+05 0.00249  7.47422E+05 0.00287  1.52873E+06 0.00114  3.20681E+06 0.00117  3.64550E+06 0.00101  3.50543E+06 0.00096  3.35505E+06 0.00078  3.18177E+06 0.00075  3.02826E+06 0.00060  2.94453E+06 0.00066  2.89812E+06 0.00080  2.84263E+06 0.00074  2.80075E+06 0.00103  2.76506E+06 0.00092  2.76138E+06 0.00095  2.41218E+06 0.00128  2.41824E+06 0.00097  2.39046E+06 0.00105  2.35847E+06 0.00079  4.61451E+06 0.00083  4.44432E+06 0.00106  3.20392E+06 0.00101  2.06559E+06 0.00096  2.43134E+06 0.00113  2.33137E+06 0.00090  1.95148E+06 0.00101  3.48809E+06 0.00082  7.11190E+05 0.00110  8.68601E+05 0.00084  7.67687E+05 0.00118  4.41211E+05 0.00084  7.50315E+05 0.00112  5.00683E+05 0.00084  4.26669E+05 0.00131  8.23699E+04 0.00239  8.09034E+04 0.00353  8.27931E+04 0.00264  8.47861E+04 0.00177  8.38033E+04 0.00242  8.22658E+04 0.00245  8.40239E+04 0.00125  7.89659E+04 0.00200  1.47748E+05 0.00123  2.33835E+05 0.00163  2.94259E+05 0.00213  7.55339E+05 0.00103  7.59144E+05 0.00082  7.59343E+05 0.00101  4.57277E+05 0.00217  3.08726E+05 0.00083  2.24107E+05 0.00170  2.41586E+05 0.00196  4.06139E+05 0.00123  4.77845E+05 0.00073  8.26680E+05 0.00140  1.27154E+06 0.00098  2.34274E+06 0.00211  1.98758E+06 0.00225  1.74642E+06 0.00218  1.46452E+06 0.00230  1.48947E+06 0.00224  1.66173E+06 0.00259  1.57345E+06 0.00256  1.17026E+06 0.00276  1.17853E+06 0.00267  1.14870E+06 0.00248  1.06762E+06 0.00257  9.09778E+05 0.00229  6.52134E+05 0.00301  2.56407E+05 0.00288 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.00151E+01 0.00070  1.21897E+01 0.00193 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.59665E-01 8.9E-05  5.29161E-01 1.7E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.54041E-05 0.00373  2.94370E-04 0.00030 ];
INF_ABS                   (idx, [1:   4]) = [  1.54041E-05 0.00373  2.94370E-04 0.00030 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  8.72037E-08 0.00022  3.23525E-06 0.00030 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.59650E-01 8.9E-05  5.28867E-01 1.7E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  2.98997E-02 0.00040  2.84082E-02 0.00155 ];
INF_SCATT2                (idx, [1:   4]) = [  2.82842E-03 0.00623  1.70450E-03 0.00730 ];
INF_SCATT3                (idx, [1:   4]) = [  4.53226E-04 0.03835  2.79317E-04 0.05691 ];
INF_SCATT4                (idx, [1:   4]) = [  2.54918E-05 0.38639  1.23883E-04 0.06630 ];
INF_SCATT5                (idx, [1:   4]) = [  2.05720E-05 0.37218  7.50648E-05 0.30417 ];
INF_SCATT6                (idx, [1:   4]) = [  5.42491E-06 1.00000  3.31510E-05 0.54603 ];
INF_SCATT7                (idx, [1:   4]) = [  9.27397E-06 0.83013  2.97928E-05 0.21582 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.59650E-01 8.9E-05  5.28867E-01 1.7E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.98997E-02 0.00040  2.84082E-02 0.00155 ];
INF_SCATTP2               (idx, [1:   4]) = [  2.82842E-03 0.00623  1.70450E-03 0.00730 ];
INF_SCATTP3               (idx, [1:   4]) = [  4.53226E-04 0.03835  2.79317E-04 0.05691 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.54918E-05 0.38639  1.23883E-04 0.06630 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.05720E-05 0.37218  7.50648E-05 0.30417 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.42491E-06 1.00000  3.31510E-05 0.54603 ];
INF_SCATTP7               (idx, [1:   4]) = [  9.27397E-06 0.83013  2.97928E-05 0.21582 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  3.91870E-01 0.00021  4.99685E-01 0.00010 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  8.50622E-01 0.00021  6.67087E-01 0.00010 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.54041E-05 0.00373  2.94370E-04 0.00030 ];
INF_REMXS                 (idx, [1:   4]) = [  3.36862E-03 0.00027  7.05799E-04 0.00283 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.56296E-01 8.8E-05  3.35344E-03 0.00025  4.12033E-04 0.00414  5.28455E-01 1.5E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.08194E-02 0.00039 -9.19658E-04 0.00078  1.11822E-04 0.00466  2.82964E-02 0.00156 ];
INF_S2                    (idx, [1:   8]) = [  2.89938E-03 0.00644 -7.09614E-05 0.01865  1.06140E-06 0.95351  1.70344E-03 0.00691 ];
INF_S3                    (idx, [1:   8]) = [  4.59427E-04 0.03736 -6.20044E-06 0.15880 -1.44917E-05 0.02874  2.93809E-04 0.05457 ];
INF_S4                    (idx, [1:   8]) = [  2.94507E-05 0.32395 -3.95884E-06 0.19023 -1.10875E-05 0.04737  1.34970E-04 0.05946 ];
INF_S5                    (idx, [1:   8]) = [  2.28800E-05 0.33525 -2.30808E-06 0.35177 -8.06529E-06 0.05523  8.31301E-05 0.27427 ];
INF_S6                    (idx, [1:   8]) = [  6.84178E-06 0.95312 -1.41687E-06 0.21790 -5.61122E-06 0.12106  3.87622E-05 0.47178 ];
INF_S7                    (idx, [1:   8]) = [  9.47948E-06 0.79256 -2.05503E-07 1.00000 -3.40667E-06 0.12036  3.31994E-05 0.19561 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.56296E-01 8.8E-05  3.35344E-03 0.00025  4.12033E-04 0.00414  5.28455E-01 1.5E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.08194E-02 0.00039 -9.19658E-04 0.00078  1.11822E-04 0.00466  2.82964E-02 0.00156 ];
INF_SP2                   (idx, [1:   8]) = [  2.89938E-03 0.00644 -7.09614E-05 0.01865  1.06140E-06 0.95351  1.70344E-03 0.00691 ];
INF_SP3                   (idx, [1:   8]) = [  4.59427E-04 0.03736 -6.20044E-06 0.15880 -1.44917E-05 0.02874  2.93809E-04 0.05457 ];
INF_SP4                   (idx, [1:   8]) = [  2.94507E-05 0.32395 -3.95884E-06 0.19023 -1.10875E-05 0.04737  1.34970E-04 0.05946 ];
INF_SP5                   (idx, [1:   8]) = [  2.28800E-05 0.33525 -2.30808E-06 0.35177 -8.06529E-06 0.05523  8.31301E-05 0.27427 ];
INF_SP6                   (idx, [1:   8]) = [  6.84178E-06 0.95312 -1.41687E-06 0.21790 -5.61122E-06 0.12106  3.87622E-05 0.47178 ];
INF_SP7                   (idx, [1:   8]) = [  9.47948E-06 0.79256 -2.05503E-07 1.00000 -3.40667E-06 0.12036  3.31994E-05 0.19561 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.01011E-01 0.00082  5.53271E-01 0.00165 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.74674E-01 0.00076  5.47365E-01 0.00210 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.74534E-01 0.00053  5.51574E-01 0.00354 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.88184E-01 0.00176  5.61100E-01 0.00289 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.65829E+00 0.00081  6.02485E-01 0.00165 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.90832E+00 0.00076  6.08991E-01 0.00208 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.90985E+00 0.00053  6.04369E-01 0.00354 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.15669E+00 0.00175  5.94096E-01 0.00288 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'homKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:21:07 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589784 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.01614E+00  1.00208E+00  9.93724E-01  9.90832E-01  1.00840E+00  9.89303E-01  9.94172E-01  1.00535E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74600E-02 0.00150  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82540E-01 2.7E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39107E-01 5.5E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39695E-01 5.5E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80715E+00 0.00028  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30385E-01 5.4E-06  6.71934E-02 7.5E-05  2.42169E-03 0.00042  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38355E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35524E+01 0.00025  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01630E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63618E+00 0.00137  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999772 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99981E+04 0.00048 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.33330E+02 ;
RUNNING_TIME              (idx, 1)        =  1.79616E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.04000E-02  6.04000E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.81667E-03  4.81667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  1.78958E+01  1.78958E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  1.79603E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.42308 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.45996E+00 0.00224 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76579E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1759.72 ;
MEMSIZE                   (idx, 1)        = 1666.21 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 302.05 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.50 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99907E-06 0.00021  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39069E-02 0.00232 ];
U235_FISS                 (idx, [1:   4]) = [  4.36777E-01 0.00039  9.99260E-01 1.3E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.23457E-04 0.01695  7.39971E-04 0.01691 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63364E-01 0.00076  5.83643E-01 0.00048 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43514E-02 0.00227  5.12731E-02 0.00223 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999772 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.00268E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3358068 3.35914E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5244177 5.24570E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3397527 3.39816E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999772 1.20030E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 -2.24076E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41654E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06856E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.37097E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79749E-01 0.00027 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16846E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99907E-01 0.00021 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50718E+02 0.00023 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83154E-01 0.00055 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35592E+01 0.00028 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05903E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.48569E-01 0.00017 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15654E-01 0.00067 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.53974E+00 0.00072 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85855E-01 0.00018 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12153E-01 0.00011 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49085E+00 0.00028 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06867E+00 0.00034 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44468E+00 1.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 9.2E-09 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06875E+00 0.00034  1.06090E+00 0.00034  7.77130E-03 0.00479 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06867E+00 0.00039 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06884E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49118E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33620E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33611E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.14867E-05 0.00215 ];
IMP_EALF                  (idx, [1:   2]) = [  3.15091E-05 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.66546E-02 0.00230 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.67632E-02 0.00050 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.21090E-03 0.00341  2.05715E-04 0.02031  9.45900E-04 0.00930  6.05504E-04 0.00957  1.24161E-03 0.00831  1.96725E-03 0.00702  5.84396E-04 0.01114  5.19394E-04 0.01125  1.41133E-04 0.02463 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.08383E-01 0.00587  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.31828E-03 0.00545  2.41235E-04 0.03172  1.10018E-03 0.01408  7.26657E-04 0.01693  1.47327E-03 0.01295  2.31820E-03 0.00951  6.90042E-04 0.01739  6.05282E-04 0.01711  1.63417E-04 0.03750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.06254E-01 0.00820  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.69050E-05 0.00211  3.69227E-05 0.00211  3.45326E-05 0.02357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94412E-05 0.00203  3.94601E-05 0.00203  3.69069E-05 0.02357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.24852E-03 0.00481  2.38407E-04 0.02723  1.10093E-03 0.01320  7.14434E-04 0.01643  1.44142E-03 0.01346  2.30240E-03 0.00969  6.81542E-04 0.01663  6.04769E-04 0.01841  1.64610E-04 0.03599 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.08294E-01 0.00922  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.39892E-05 0.01992  3.39880E-05 0.01992  3.35995E-05 0.06692 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.63246E-05 0.01990  3.63235E-05 0.01991  3.58922E-05 0.06681 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.88651E-03 0.02729  2.42871E-04 0.09417  1.06302E-03 0.04622  7.19584E-04 0.05515  1.34991E-03 0.04397  2.17594E-03 0.03776  6.43294E-04 0.05784  5.33940E-04 0.06303  1.57953E-04 0.12793 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  3.95321E-01 0.02579  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.90122E-03 0.02704  2.40983E-04 0.09442  1.08429E-03 0.04550  7.12623E-04 0.05213  1.33158E-03 0.04159  2.18699E-03 0.03558  6.39476E-04 0.05390  5.42614E-04 0.06117  1.62665E-04 0.12799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  3.98869E-01 0.02576  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.2E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.03029E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.63505E-05 0.00116 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.88491E-05 0.00111 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.32537E-03 0.00311 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.01575E+02 0.00361 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23195E-07 0.00100 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86352E-05 0.00026  1.86372E-05 0.00026  1.83418E-05 0.00325 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89748E-04 0.00112  1.89793E-04 0.00112  1.83372E-04 0.01128 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23885E-01 0.00055  2.23696E-01 0.00056  2.54496E-01 0.00850 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32897E+01 0.00782 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35524E+01 0.00025  5.43160E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '_' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CAPT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_ABS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP1               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP2               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP3               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP4               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP5               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP6               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP7               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_REMXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP1                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP2                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP3                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP4                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP5                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP6                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP7                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

