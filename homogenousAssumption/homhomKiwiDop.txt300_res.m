
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30603E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05867E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80617 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58770E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   2]) = '40' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  4.84398E+05 0.00479  2.30342E+06 0.00116  5.19550E+06 0.00063  9.38490E+06 0.00069  9.93688E+06 0.00023  9.23811E+06 0.00021  8.43236E+06 0.00044  7.49964E+06 0.00029  6.68372E+06 0.00033  6.05107E+06 0.00024  5.57691E+06 0.00043  5.19652E+06 0.00077  4.83927E+06 0.00052  4.64571E+06 0.00016  4.47400E+06 0.00086  3.80639E+06 0.00023  3.73575E+06 0.00014  3.58563E+06 0.00013  3.42177E+06 0.00067  6.35098E+06 0.00022  5.57209E+06 0.00031  3.67032E+06 0.00044  2.21781E+06 0.00035  2.38819E+06 0.00037  2.12852E+06 0.00128  1.70104E+06 0.00059  2.83991E+06 0.00129  5.75620E+05 0.00148  7.05528E+05 0.00089  6.34211E+05 0.00184  3.61325E+05 0.00298  6.22464E+05 0.00168  4.15000E+05 0.00139  3.41596E+05 0.00104  6.32184E+04 0.00349  6.18929E+04 0.00282  6.37698E+04 0.00636  6.53673E+04 0.00173  6.40796E+04 0.00123  6.32756E+04 0.00409  6.53007E+04 0.00280  6.07622E+04 0.00749  1.13621E+05 0.00142  1.79763E+05 0.00221  2.23416E+05 0.00122  5.67637E+05 0.00044  5.55307E+05 0.00089  5.35981E+05 0.00101  3.09439E+05 0.00056  2.02599E+05 0.00280  1.44273E+05 0.00180  1.53618E+05 0.00055  2.52868E+05 0.00236  2.92888E+05 0.00150  4.93052E+05 0.00172  7.32997E+05 0.00196  1.30072E+06 0.00242  1.07278E+06 0.00265  9.31817E+05 0.00252  7.76608E+05 0.00361  7.87566E+05 0.00165  8.73905E+05 0.00203  8.23931E+05 0.00415  6.10757E+05 0.00285  6.14527E+05 0.00277  5.95917E+05 0.00108  5.51199E+05 0.00102  4.71084E+05 0.00271  3.35770E+05 0.00306  1.31382E+05 0.00539 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.48986E+00 0.00011 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36625E+02 6.0E-05  1.35526E+01 0.00149 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.38161E-01 0.00010  5.25234E-01 3.1E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.82466E-03 0.00057  2.25792E-03 0.00294 ];
INF_ABS                   (idx, [1:   4]) = [  4.11782E-03 0.00052  1.13781E-02 0.00321 ];
INF_FISS                  (idx, [1:   4]) = [  2.29315E-03 0.00048  9.12021E-03 0.00328 ];
INF_NSF                   (idx, [1:   4]) = [  5.61380E-03 0.00048  2.22181E-02 0.00328 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44807E+00 3.1E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.6E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.55873E-08 0.00037  3.12019E-06 0.00042 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.34042E-01 0.00011  5.13831E-01 0.00011 ];
INF_SCATT1                (idx, [1:   4]) = [  3.72181E-02 0.00036  3.09713E-02 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.99360E-03 0.00159  3.27799E-03 0.00290 ];
INF_SCATT3                (idx, [1:   4]) = [  9.75179E-04 0.00212  7.92226E-04 0.04584 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.95630E-04 0.01326  2.11154E-04 0.12921 ];
INF_SCATT5                (idx, [1:   4]) = [  8.36913E-05 0.08384  8.80590E-05 0.26271 ];
INF_SCATT6                (idx, [1:   4]) = [  4.01081E-04 0.00733  1.84626E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  5.18011E-05 0.07750  1.66708E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.34044E-01 0.00011  5.13831E-01 0.00011 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.72181E-02 0.00036  3.09713E-02 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.99359E-03 0.00159  3.27799E-03 0.00290 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.75170E-04 0.00213  7.92226E-04 0.04584 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.95642E-04 0.01329  2.11154E-04 0.12921 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.36763E-05 0.08387  8.80590E-05 0.26271 ];
INF_SCATTP6               (idx, [1:   4]) = [  4.01060E-04 0.00724  1.84626E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  5.18099E-05 0.07758  1.66708E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.51765E-01 0.00021  4.92420E-01 7.6E-06 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.32398E+00 0.00021  6.76929E-01 7.6E-06 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.11585E-03 0.00050  1.13781E-02 0.00321 ];
INF_REMXS                 (idx, [1:   4]) = [  5.80003E-03 0.00013  1.18866E-02 0.00363 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.32361E-01 0.00010  1.68188E-03 0.00085  4.83545E-04 0.00630  5.13348E-01 0.00011 ];
INF_S1                    (idx, [1:   8]) = [  3.73950E-02 0.00035 -1.76878E-04 0.00892  1.24711E-04 0.00686  3.08466E-02 0.00063 ];
INF_S2                    (idx, [1:   8]) = [  1.00687E-02 0.00146 -7.50704E-05 0.02324  5.47005E-06 0.21517  3.27252E-03 0.00295 ];
INF_S3                    (idx, [1:   8]) = [  1.06153E-03 0.00151 -8.63479E-05 0.00790 -1.22205E-05 0.09278  8.04447E-04 0.04637 ];
INF_S4                    (idx, [1:   8]) = [ -5.66046E-04 0.01560 -2.95844E-05 0.03549 -9.99764E-06 0.05635  2.21152E-04 0.12098 ];
INF_S5                    (idx, [1:   8]) = [  7.67834E-05 0.09699  6.90786E-06 0.07735 -6.43618E-06 0.09543  9.44952E-05 0.24032 ];
INF_S6                    (idx, [1:   8]) = [  3.93537E-04 0.00573  7.54493E-06 0.14685 -5.70462E-06 0.21084  2.41672E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  5.15900E-05 0.07452  2.11079E-07 0.90727 -3.67813E-06 0.06073  2.03490E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.32363E-01 0.00010  1.68188E-03 0.00085  4.83545E-04 0.00630  5.13348E-01 0.00011 ];
INF_SP1                   (idx, [1:   8]) = [  3.73950E-02 0.00035 -1.76878E-04 0.00892  1.24711E-04 0.00686  3.08466E-02 0.00063 ];
INF_SP2                   (idx, [1:   8]) = [  1.00687E-02 0.00146 -7.50704E-05 0.02324  5.47005E-06 0.21517  3.27252E-03 0.00295 ];
INF_SP3                   (idx, [1:   8]) = [  1.06152E-03 0.00152 -8.63479E-05 0.00790 -1.22205E-05 0.09278  8.04447E-04 0.04637 ];
INF_SP4                   (idx, [1:   8]) = [ -5.66058E-04 0.01564 -2.95844E-05 0.03549 -9.99764E-06 0.05635  2.21152E-04 0.12098 ];
INF_SP5                   (idx, [1:   8]) = [  7.67685E-05 0.09703  6.90786E-06 0.07735 -6.43618E-06 0.09543  9.44952E-05 0.24032 ];
INF_SP6                   (idx, [1:   8]) = [  3.93515E-04 0.00563  7.54493E-06 0.14685 -5.70462E-06 0.21084  2.41672E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  5.15988E-05 0.07460  2.11079E-07 0.90727 -3.67813E-06 0.06073  2.03490E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.82568E-01 0.00076  7.74073E-01 0.00154 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.86078E-01 0.00080  9.88559E-01 0.01066 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.85702E-01 0.00095  9.71105E-01 0.00514 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.76151E-01 0.00079  5.45300E-01 0.00653 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.17966E+00 0.00076  4.30625E-01 0.00153 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.16519E+00 0.00080  3.37269E-01 0.01077 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.16672E+00 0.00095  3.43270E-01 0.00512 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.20707E+00 0.00079  6.11336E-01 0.00649 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
LAMBDA                    (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30604E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05868E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80617 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58767E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'F' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.70386E+05 0.00538  1.28352E+06 0.00106  2.94481E+06 0.00137  5.18654E+06 0.00081  5.41585E+06 0.00016  5.00719E+06 0.00041  4.52482E+06 0.00014  3.96746E+06 0.00081  3.46945E+06 0.00046  3.06926E+06 0.00068  2.75891E+06 0.00122  2.51126E+06 0.00152  2.28213E+06 0.00153  2.14788E+06 0.00053  2.02944E+06 0.00141  1.69809E+06 0.00194  1.64524E+06 0.00087  1.54923E+06 0.00162  1.44161E+06 0.00228  2.56790E+06 0.00105  2.07637E+06 0.00247  1.24533E+06 0.00158  6.93948E+05 0.00276  6.63797E+05 0.00053  5.24407E+05 0.00185  3.85923E+05 0.00086  5.58475E+05 0.00264  1.11472E+05 0.00291  1.37506E+05 0.00216  1.29364E+05 0.00602  7.11559E+04 0.00946  1.27749E+05 0.00576  8.50215E+04 0.00509  6.44690E+04 0.00107  1.07298E+04 0.00267  1.03175E+04 0.00523  1.08767E+04 0.00868  1.11670E+04 0.01032  1.09417E+04 0.00183  1.08843E+04 0.01513  1.12514E+04 0.01497  1.02909E+04 0.00145  1.93090E+04 0.00664  3.02195E+04 0.00504  3.68182E+04 0.00393  8.99577E+04 0.00355  8.07971E+04 0.00648  6.79831E+04 0.00147  3.28894E+04 0.00865  1.82988E+04 0.00495  1.18384E+04 0.00937  1.15025E+04 0.00408  1.72371E+04 0.01226  1.77979E+04 0.00675  2.54191E+04 0.00349  2.69442E+04 0.00812  2.87538E+04 0.00570  1.45645E+04 0.00444  9.24799E+03 0.00242  6.18146E+03 0.00526  5.22361E+03 0.00063  4.85454E+03 0.00872  3.86327E+03 0.00964  2.54792E+03 0.00244  2.21476E+03 0.01784  1.88308E+03 0.00935  1.49532E+03 0.01477  1.06868E+03 0.01424  6.20844E+02 0.03721  1.64275E+02 0.01535 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.60574E+00 0.00063 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  6.29162E+01 0.00058  3.93439E-01 0.00147 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56622E-01 7.3E-05  4.67496E-01 0.00027 ];
INF_CAPT                  (idx, [1:   4]) = [  2.35552E-03 0.00033  2.36327E-02 0.00078 ];
INF_ABS                   (idx, [1:   4]) = [  6.33542E-03 0.00023  1.52788E-01 0.00079 ];
INF_FISS                  (idx, [1:   4]) = [  3.97990E-03 0.00028  1.29155E-01 0.00079 ];
INF_NSF                   (idx, [1:   4]) = [  9.74597E-03 0.00029  3.14640E-01 0.00079 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44880E+00 2.3E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.6E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  2.38276E-08 0.00073  1.57835E-06 0.00065 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50282E-01 8.0E-05  3.14240E-01 0.00086 ];
INF_SCATT1                (idx, [1:   4]) = [  2.06179E-02 0.00024  1.89651E-02 0.01966 ];
INF_SCATT2                (idx, [1:   4]) = [  4.50004E-03 0.00326  1.37975E-03 0.15023 ];
INF_SCATT3                (idx, [1:   4]) = [  9.97324E-04 0.01170  1.42470E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  2.14255E-04 0.01829  1.47134E-04 0.61970 ];
INF_SCATT5                (idx, [1:   4]) = [  1.49920E-04 0.12837 -1.93146E-04 0.60548 ];
INF_SCATT6                (idx, [1:   4]) = [  8.56922E-05 0.05625  1.60665E-04 0.14252 ];
INF_SCATT7                (idx, [1:   4]) = [  3.87176E-05 0.15366 -4.34768E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50285E-01 8.1E-05  3.14240E-01 0.00086 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.06179E-02 0.00024  1.89651E-02 0.01966 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.49997E-03 0.00325  1.37975E-03 0.15023 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.97263E-04 0.01170  1.42470E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.14193E-04 0.01818  1.47134E-04 0.61970 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.49880E-04 0.12851 -1.93146E-04 0.60548 ];
INF_SCATTP6               (idx, [1:   4]) = [  8.56898E-05 0.05664  1.60665E-04 0.14252 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.87309E-05 0.15331 -4.34768E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02883E-01 0.00011  4.30749E-01 0.00104 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64298E+00 0.00011  7.73847E-01 0.00104 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.33256E-03 0.00022  1.52788E-01 0.00079 ];
INF_REMXS                 (idx, [1:   4]) = [  6.65847E-03 0.00045  1.54953E-01 0.00185 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49963E-01 8.6E-05  3.19171E-04 0.00425  1.69723E-03 0.01970  3.12543E-01 0.00077 ];
INF_S1                    (idx, [1:   8]) = [  2.06906E-02 0.00026 -7.27831E-05 0.00671  4.08111E-04 0.01929  1.85570E-02 0.02051 ];
INF_S2                    (idx, [1:   8]) = [  4.50875E-03 0.00323 -8.71034E-06 0.06430 -2.71007E-06 1.00000  1.38246E-03 0.14634 ];
INF_S3                    (idx, [1:   8]) = [  1.00300E-03 0.01188 -5.67321E-06 0.04459 -6.97362E-05 0.15203  2.12207E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  2.16144E-04 0.01870 -1.88927E-06 0.34469 -5.16903E-05 0.25602  1.98825E-04 0.39297 ];
INF_S5                    (idx, [1:   8]) = [  1.49234E-04 0.13022  6.85801E-07 0.50662 -2.06161E-05 0.19907 -1.72530E-04 0.69722 ];
INF_S6                    (idx, [1:   8]) = [  8.56866E-05 0.05995  5.56650E-09 1.00000 -1.41759E-05 0.53079  1.74841E-04 0.10778 ];
INF_S7                    (idx, [1:   8]) = [  3.78268E-05 0.15348  8.90754E-07 0.17676 -4.68246E-06 0.84772 -3.87943E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49966E-01 8.6E-05  3.19171E-04 0.00425  1.69723E-03 0.01970  3.12543E-01 0.00077 ];
INF_SP1                   (idx, [1:   8]) = [  2.06907E-02 0.00026 -7.27831E-05 0.00671  4.08111E-04 0.01929  1.85570E-02 0.02051 ];
INF_SP2                   (idx, [1:   8]) = [  4.50868E-03 0.00322 -8.71034E-06 0.06430 -2.71007E-06 1.00000  1.38246E-03 0.14634 ];
INF_SP3                   (idx, [1:   8]) = [  1.00294E-03 0.01188 -5.67321E-06 0.04459 -6.97362E-05 0.15203  2.12207E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  2.16083E-04 0.01858 -1.88927E-06 0.34469 -5.16903E-05 0.25602  1.98825E-04 0.39297 ];
INF_SP5                   (idx, [1:   8]) = [  1.49194E-04 0.13036  6.85801E-07 0.50662 -2.06161E-05 0.19907 -1.72530E-04 0.69722 ];
INF_SP6                   (idx, [1:   8]) = [  8.56842E-05 0.06036  5.56650E-09 1.00000 -1.41759E-05 0.53079  1.74841E-04 0.10778 ];
INF_SP7                   (idx, [1:   8]) = [  3.78401E-05 0.15314  8.90754E-07 0.17676 -4.68246E-06 0.84772 -3.87943E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.47967E-01 0.00177 -9.40687E+00 0.37929 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  3.98976E-01 0.00302 -7.48415E-01 0.00809 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.00088E-01 0.00195 -7.33719E-01 0.03246 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.76578E-01 0.00158  4.38626E-01 0.06299 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  9.57951E-01 0.00176 -4.47823E-02 0.27526 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  8.35487E-01 0.00301 -4.45445E-01 0.00815 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  8.33156E-01 0.00195 -4.55295E-01 0.03345 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.20521E+00 0.00159  7.66393E-01 0.06678 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.53698E-03 0.01316  2.55441E-04 0.05945  1.07299E-03 0.03740  8.08853E-04 0.03535  1.52559E-03 0.03175  2.39245E-03 0.02268  6.80995E-04 0.03965  6.25076E-04 0.03920  1.75592E-04 0.07797 ];
LAMBDA                    (idx, [1:  18]) = [  4.07638E-01 0.01962  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 5.9E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30606E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05872E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80619 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58762E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'T' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.66939E+04 0.00544  8.08518E+04 0.00500  1.85324E+05 0.00091  3.33173E+05 0.00244  3.54493E+05 0.00062  3.30523E+05 0.00104  2.99446E+05 0.00475  2.62805E+05 0.00087  2.30528E+05 0.00241  2.02604E+05 0.00223  1.83590E+05 0.00151  1.70086E+05 0.00195  1.52202E+05 0.00192  1.48225E+05 0.00050  1.38732E+05 0.00314  1.17150E+05 0.00087  1.13690E+05 0.00442  1.07157E+05 0.00465  1.00600E+05 0.00314  1.78901E+05 0.00187  1.43907E+05 0.00267  8.59788E+04 0.00035  5.02335E+04 0.00262  4.59546E+04 0.00436  3.83159E+04 0.00289  2.65817E+04 0.00419  3.76724E+04 0.00417  6.84123E+03 0.00890  8.87011E+03 0.00843  8.29124E+03 0.00756  4.66586E+03 0.01784  7.96570E+03 0.01169  5.31556E+03 0.00528  4.35355E+03 0.02171  7.08191E+02 0.07002  7.25488E+02 0.00854  7.51198E+02 0.01748  7.36715E+02 0.03681  7.49085E+02 0.02859  7.24554E+02 0.03488  7.48192E+02 0.01925  7.07695E+02 0.04815  1.29929E+03 0.02278  2.02544E+03 0.00994  2.51622E+03 0.01550  6.01031E+03 0.00413  5.43470E+03 0.00443  4.77039E+03 0.01225  2.51052E+03 0.03717  1.46678E+03 0.03554  1.02661E+03 0.02948  1.00555E+03 0.03860  1.56601E+03 0.01441  1.58055E+03 0.03979  2.32994E+03 0.02445  2.53422E+03 0.01165  2.96704E+03 0.00820  1.53505E+03 0.00141  1.05840E+03 0.00598  7.76502E+02 0.03410  7.09053E+02 0.01468  7.25537E+02 0.01502  5.86585E+02 0.02639  4.47984E+02 0.01370  4.12423E+02 0.03574  3.98077E+02 0.05945  3.70296E+02 0.02726  3.25779E+02 0.00876  2.51168E+02 0.06292  1.17122E+02 0.04935 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.19993E+00 0.00021  3.49105E-02 0.00428 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.17738E-01 0.00019  5.87545E-01 0.00086 ];
INF_CAPT                  (idx, [1:   4]) = [  9.19907E-03 0.00156  2.70492E-02 0.00409 ];
INF_ABS                   (idx, [1:   4]) = [  9.19907E-03 0.00156  2.70492E-02 0.00409 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.40944E-08 0.00160  1.89258E-06 0.00426 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.08548E-01 0.00022  5.59769E-01 0.00115 ];
INF_SCATT1                (idx, [1:   4]) = [  1.49982E-01 7.4E-05  2.00000E-01 0.00488 ];
INF_SCATT2                (idx, [1:   4]) = [  6.21533E-02 0.00182  7.93840E-02 0.00400 ];
INF_SCATT3                (idx, [1:   4]) = [  2.70545E-03 0.03226  2.49232E-02 0.01602 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.61835E-03 0.00330  4.63914E-03 0.05604 ];
INF_SCATT5                (idx, [1:   4]) = [  1.99854E-04 0.19050 -1.12199E-03 0.28351 ];
INF_SCATT6                (idx, [1:   4]) = [  3.41433E-03 0.01785 -1.81473E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.39014E-04 0.13710 -7.44696E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.08553E-01 0.00022  5.59769E-01 0.00115 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.49982E-01 7.4E-05  2.00000E-01 0.00488 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.21537E-02 0.00182  7.93840E-02 0.00400 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.70566E-03 0.03223  2.49232E-02 0.01602 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.61828E-03 0.00334  4.63914E-03 0.05604 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.99680E-04 0.18911 -1.12199E-03 0.28351 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.41437E-03 0.01779 -1.81473E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.38878E-04 0.13692 -7.44696E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.00212E-01 0.00014  3.73123E-01 0.00277 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.66490E+00 0.00014  8.93374E-01 0.00278 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.19406E-03 0.00158  2.70492E-02 0.00409 ];
INF_REMXS                 (idx, [1:   4]) = [  1.20435E-02 0.00039  2.92665E-02 0.00415 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.05695E-01 0.00019  2.85285E-03 0.00555  1.49038E-03 0.03951  5.58278E-01 0.00110 ];
INF_S1                    (idx, [1:   8]) = [  1.48970E-01 7.6E-05  1.01197E-03 0.00292  3.41315E-04 0.33991  1.99659E-01 0.00440 ];
INF_S2                    (idx, [1:   8]) = [  6.25106E-02 0.00184 -3.57309E-04 0.01366  2.71871E-04 0.21850  7.91121E-02 0.00458 ];
INF_S3                    (idx, [1:   8]) = [  3.23658E-03 0.02705 -5.31139E-04 0.00712  2.08056E-04 0.26814  2.47152E-02 0.01498 ];
INF_S4                    (idx, [1:   8]) = [ -6.46029E-03 0.00374 -1.58057E-04 0.03124  1.53804E-04 0.31752  4.48534E-03 0.06190 ];
INF_S5                    (idx, [1:   8]) = [  1.43687E-04 0.26550  5.61670E-05 0.05987  1.37785E-04 0.51797 -1.25977E-03 0.20922 ];
INF_S6                    (idx, [1:   8]) = [  3.37072E-03 0.01855  4.36051E-05 0.06236  5.97850E-05 0.61956 -2.41259E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.40194E-04 0.16151 -1.17970E-06 1.00000  2.65485E-06 1.00000 -7.71244E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.05700E-01 0.00019  2.85285E-03 0.00555  1.49038E-03 0.03951  5.58278E-01 0.00110 ];
INF_SP1                   (idx, [1:   8]) = [  1.48970E-01 7.6E-05  1.01197E-03 0.00292  3.41315E-04 0.33991  1.99659E-01 0.00440 ];
INF_SP2                   (idx, [1:   8]) = [  6.25110E-02 0.00183 -3.57309E-04 0.01366  2.71871E-04 0.21850  7.91121E-02 0.00458 ];
INF_SP3                   (idx, [1:   8]) = [  3.23680E-03 0.02703 -5.31139E-04 0.00712  2.08056E-04 0.26814  2.47152E-02 0.01498 ];
INF_SP4                   (idx, [1:   8]) = [ -6.46023E-03 0.00378 -1.58057E-04 0.03124  1.53804E-04 0.31752  4.48534E-03 0.06190 ];
INF_SP5                   (idx, [1:   8]) = [  1.43513E-04 0.26344  5.61670E-05 0.05987  1.37785E-04 0.51797 -1.25977E-03 0.20922 ];
INF_SP6                   (idx, [1:   8]) = [  3.37076E-03 0.01848  4.36051E-05 0.06236  5.97850E-05 0.61956 -2.41259E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.40058E-04 0.16137 -1.17970E-06 1.00000  2.65485E-06 1.00000 -7.71244E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.24691E-01 0.00060  1.37108E+00 0.04814 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.55429E-01 0.00588  4.31578E+00 0.08002 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.54695E-01 0.00566  4.36584E+00 0.25181 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.81496E-01 0.00420  6.02088E-01 0.10349 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.48352E+00 0.00060  2.44266E-01 0.04890 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.30508E+00 0.00585  7.83237E-02 0.08679 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.30884E+00 0.00566  8.93949E-02 0.29682 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.83665E+00 0.00419  5.65080E-01 0.09828 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30606E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05875E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80617 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58757E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'C' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.94586E+04 0.00589  1.87114E+05 0.00376  4.27919E+05 0.00263  7.62771E+05 0.00163  8.03068E+05 0.00163  7.40801E+05 0.00109  6.67812E+05 0.00168  5.79873E+05 0.00048  5.09755E+05 0.00165  4.48882E+05 0.00205  4.06040E+05 0.00109  3.76744E+05 0.00132  3.41288E+05 0.00038  3.32940E+05 0.00121  3.14175E+05 0.00323  2.67753E+05 0.00102  2.61586E+05 0.00260  2.51096E+05 0.00132  2.38385E+05 0.00275  4.36017E+05 0.00257  3.73400E+05 0.00175  2.40937E+05 0.00138  1.42510E+05 0.00424  1.46208E+05 0.00243  1.24570E+05 0.00160  9.48621E+04 0.00371  1.45980E+05 0.00194  2.86193E+04 0.00469  3.50002E+04 0.00344  3.18283E+04 0.00709  1.81273E+04 0.00919  3.14045E+04 0.00235  2.07077E+04 0.00903  1.66052E+04 0.00491  3.09835E+03 0.01826  2.97806E+03 0.00744  2.98183E+03 0.01031  3.10040E+03 0.01214  2.99864E+03 0.02184  2.92388E+03 0.01009  3.07587E+03 0.01179  2.84375E+03 0.00516  5.29930E+03 0.00725  8.28850E+03 0.01244  1.04340E+04 0.00178  2.56233E+04 0.00314  2.45636E+04 0.00425  2.24629E+04 0.00310  1.22509E+04 0.00458  7.63509E+03 0.01251  5.20644E+03 0.00302  5.39093E+03 0.00771  8.50127E+03 0.00679  9.26930E+03 0.00651  1.38938E+04 0.00429  1.73381E+04 0.00425  2.36896E+04 0.00444  1.56638E+04 0.00628  1.19502E+04 0.00703  9.29379E+03 0.00368  8.89765E+03 0.01186  9.40597E+03 0.00862  8.19234E+03 0.00063  6.07731E+03 0.00129  5.81498E+03 0.01476  5.53907E+03 0.00471  5.01193E+03 0.00921  4.16904E+03 0.00387  3.07488E+03 0.00714  1.25681E+03 0.01587 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.91907E+00 0.00066  2.44579E-01 0.00153 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.00051E-01 0.00012  7.12293E-01 0.00015 ];
INF_CAPT                  (idx, [1:   4]) = [  1.96525E-03 0.00107  1.41394E-02 0.00070 ];
INF_ABS                   (idx, [1:   4]) = [  1.96525E-03 0.00107  1.41394E-02 0.00070 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.42502E-08 0.00147  2.35631E-06 0.00070 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.98092E-01 0.00012  6.98123E-01 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  1.58681E-01 0.00039  2.00362E-01 0.00160 ];
INF_SCATT2                (idx, [1:   4]) = [  6.32867E-02 0.00049  7.63879E-02 0.00679 ];
INF_SCATT3                (idx, [1:   4]) = [  2.42074E-03 0.01088  2.53978E-02 0.01107 ];
INF_SCATT4                (idx, [1:   4]) = [ -7.19012E-03 0.00706  6.46022E-03 0.00102 ];
INF_SCATT5                (idx, [1:   4]) = [ -2.03369E-05 1.00000  1.52747E-03 0.15171 ];
INF_SCATT6                (idx, [1:   4]) = [  3.27996E-03 0.00909  4.91760E-04 0.39312 ];
INF_SCATT7                (idx, [1:   4]) = [  3.19426E-04 0.14927  6.56030E-04 0.30558 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.98096E-01 0.00012  6.98123E-01 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.58682E-01 0.00039  2.00362E-01 0.00160 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.32866E-02 0.00049  7.63879E-02 0.00679 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.42078E-03 0.01089  2.53978E-02 0.01107 ];
INF_SCATTP4               (idx, [1:   4]) = [ -7.19003E-03 0.00705  6.46022E-03 0.00102 ];
INF_SCATTP5               (idx, [1:   4]) = [ -2.02811E-05 1.00000  1.52747E-03 0.15171 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.27978E-03 0.00913  4.91760E-04 0.39312 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.19641E-04 0.14937  6.56030E-04 0.30558 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.71923E-01 0.00018  4.95804E-01 0.00053 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.22584E+00 0.00018  6.72309E-01 0.00053 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.96150E-03 0.00111  1.41394E-02 0.00070 ];
INF_REMXS                 (idx, [1:   4]) = [  6.86986E-03 0.00167  1.54715E-02 0.01753 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.93181E-01 0.00014  4.91084E-03 0.00252  1.30202E-03 0.02628  6.96821E-01 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  1.57028E-01 0.00042  1.65362E-03 0.00289  3.05979E-04 0.09663  2.00056E-01 0.00151 ];
INF_S2                    (idx, [1:   8]) = [  6.38121E-02 0.00043 -5.25353E-04 0.00705  1.82152E-04 0.07279  7.62057E-02 0.00665 ];
INF_S3                    (idx, [1:   8]) = [  3.29696E-03 0.00776 -8.76217E-04 0.00163  1.29096E-04 0.13192  2.52687E-02 0.01087 ];
INF_S4                    (idx, [1:   8]) = [ -6.88994E-03 0.00686 -3.00187E-04 0.01814  1.15187E-04 0.12140  6.34504E-03 0.00217 ];
INF_S5                    (idx, [1:   8]) = [ -1.02423E-04 0.42651  8.20863E-05 0.05508  7.31872E-05 0.05151  1.45428E-03 0.15729 ];
INF_S6                    (idx, [1:   8]) = [  3.19623E-03 0.01001  8.37334E-05 0.02600  3.33136E-05 0.24373  4.58446E-04 0.40503 ];
INF_S7                    (idx, [1:   8]) = [  3.09521E-04 0.15544  9.90457E-06 0.32268  1.33712E-05 0.41245  6.42658E-04 0.30497 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.93185E-01 0.00014  4.91084E-03 0.00252  1.30202E-03 0.02628  6.96821E-01 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  1.57028E-01 0.00042  1.65362E-03 0.00289  3.05979E-04 0.09663  2.00056E-01 0.00151 ];
INF_SP2                   (idx, [1:   8]) = [  6.38120E-02 0.00043 -5.25353E-04 0.00705  1.82152E-04 0.07279  7.62057E-02 0.00665 ];
INF_SP3                   (idx, [1:   8]) = [  3.29700E-03 0.00776 -8.76217E-04 0.00163  1.29096E-04 0.13192  2.52687E-02 0.01087 ];
INF_SP4                   (idx, [1:   8]) = [ -6.88984E-03 0.00686 -3.00187E-04 0.01814  1.15187E-04 0.12140  6.34504E-03 0.00217 ];
INF_SP5                   (idx, [1:   8]) = [ -1.02367E-04 0.42754  8.20863E-05 0.05508  7.31872E-05 0.05151  1.45428E-03 0.15729 ];
INF_SP6                   (idx, [1:   8]) = [  3.19604E-03 0.01005  8.37334E-05 0.02600  3.33136E-05 0.24373  4.58446E-04 0.40503 ];
INF_SP7                   (idx, [1:   8]) = [  3.09737E-04 0.15556  9.90457E-06 0.32268  1.33712E-05 0.41245  6.42658E-04 0.30497 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.22960E-01 0.00240 -2.66642E-01 0.02240 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.64423E-01 0.00474 -1.45277E-01 0.01563 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.63747E-01 0.00510 -1.46426E-01 0.00809 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.70017E-01 0.00129  4.13988E-01 0.08805 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.49505E+00 0.00240 -1.25134E+00 0.02191 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.26066E+00 0.00474 -2.29559E+00 0.01556 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.26390E+00 0.00511 -2.27677E+00 0.00811 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.96059E+00 0.00129  8.18327E-01 0.09156 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30607E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05878E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80615 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58751E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '9' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.33657E+04 0.01159  6.31104E+04 0.00149  1.44579E+05 0.00236  2.48883E+05 0.00258  2.56842E+05 0.00358  2.32556E+05 0.00289  2.06918E+05 0.00427  1.79908E+05 0.00206  1.55427E+05 0.00063  1.38199E+05 0.00035  1.23773E+05 0.00486  1.12977E+05 0.00409  1.03432E+05 0.00170  9.84272E+04 0.00376  9.42158E+04 0.00079  7.89874E+04 0.00431  7.70827E+04 0.00446  7.29595E+04 0.00440  6.90381E+04 0.00888  1.26558E+05 0.00322  1.07417E+05 0.00666  6.90248E+04 0.00186  4.00514E+04 0.00211  4.07613E+04 0.00229  3.42142E+04 0.00286  2.66239E+04 0.00579  4.14116E+04 0.00921  8.73593E+03 0.02238  1.08585E+04 0.01856  1.01199E+04 0.01103  5.79946E+03 0.00352  9.86015E+03 0.00768  6.49235E+03 0.00789  5.27042E+03 0.00696  9.15545E+02 0.04080  8.78264E+02 0.02369  8.93217E+02 0.02555  9.35903E+02 0.02818  9.19869E+02 0.02223  9.75435E+02 0.01853  9.31106E+02 0.03976  9.02626E+02 0.02344  1.60762E+03 0.02095  2.52474E+03 0.02806  3.13090E+03 0.01544  7.72138E+03 0.01184  7.35734E+03 0.00867  6.35416E+03 0.01576  3.32065E+03 0.00929  1.92430E+03 0.02150  1.31521E+03 0.02233  1.32349E+03 0.03875  1.92559E+03 0.01271  2.10257E+03 0.00786  3.22503E+03 0.01816  3.74767E+03 0.01049  4.57009E+03 0.01080  2.79334E+03 0.01881  1.95475E+03 0.01872  1.27819E+03 0.03003  1.12785E+03 0.00712  1.11105E+03 0.02127  8.96468E+02 0.01488  5.77759E+02 0.01130  5.15393E+02 0.03646  4.41596E+02 0.00078  3.20228E+02 0.05097  2.35247E+02 0.04226  1.19025E+02 0.04647  3.18273E+01 0.07102 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.67742E+00 0.00283 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.03658E+00 0.00100  4.85746E-02 0.00418 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56966E-01 0.00033  4.72989E-01 0.00144 ];
INF_CAPT                  (idx, [1:   4]) = [  2.51029E-03 0.00385  2.44531E-02 0.00352 ];
INF_ABS                   (idx, [1:   4]) = [  6.62462E-03 0.00197  1.58813E-01 0.00412 ];
INF_FISS                  (idx, [1:   4]) = [  4.11433E-03 0.00093  1.34360E-01 0.00423 ];
INF_NSF                   (idx, [1:   4]) = [  1.00670E-02 0.00092  3.27320E-01 0.00423 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44681E+00 1.1E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 5.5E-08  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.31497E-08 0.00220  1.82968E-06 0.00374 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50379E-01 0.00043  3.14272E-01 0.00063 ];
INF_SCATT1                (idx, [1:   4]) = [  2.02561E-02 0.00429  1.82587E-02 0.06869 ];
INF_SCATT2                (idx, [1:   4]) = [  4.46039E-03 0.01164  2.49123E-03 0.30323 ];
INF_SCATT3                (idx, [1:   4]) = [  1.01281E-03 0.07587  5.42197E-04 0.75186 ];
INF_SCATT4                (idx, [1:   4]) = [  2.48589E-04 0.20841 -7.56143E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  7.08393E-05 0.62662  1.93428E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.30684E-04 0.41757  6.09286E-04 0.98295 ];
INF_SCATT7                (idx, [1:   4]) = [  1.08317E-05 1.00000 -5.81192E-04 0.51575 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50382E-01 0.00043  3.14272E-01 0.00063 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.02565E-02 0.00430  1.82587E-02 0.06869 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.46096E-03 0.01163  2.49123E-03 0.30323 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.01296E-03 0.07556  5.42197E-04 0.75186 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.48610E-04 0.20769 -7.56143E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.06768E-05 0.62803  1.93428E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.30376E-04 0.41989  6.09286E-04 0.98295 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.06056E-05 1.00000 -5.81192E-04 0.51575 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03024E-01 0.00102  4.34712E-01 0.00412 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64185E+00 0.00102  7.66817E-01 0.00413 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.62186E-03 0.00200  1.58813E-01 0.00412 ];
INF_REMXS                 (idx, [1:   4]) = [  7.14506E-03 0.00394  1.59910E-01 0.00414 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49821E-01 0.00045  5.58146E-04 0.00986  1.19335E-03 0.10154  3.13079E-01 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.03753E-02 0.00443 -1.19212E-04 0.03904  2.95699E-04 0.21569  1.79630E-02 0.06674 ];
INF_S2                    (idx, [1:   8]) = [  4.47370E-03 0.01147 -1.33048E-05 0.09111  2.51078E-05 0.30217  2.46612E-03 0.30642 ];
INF_S3                    (idx, [1:   8]) = [  1.02384E-03 0.07213 -1.10321E-05 0.34142 -4.52697E-05 0.27245  5.87466E-04 0.69693 ];
INF_S4                    (idx, [1:   8]) = [  2.51197E-04 0.20016 -2.60858E-06 0.67615 -5.56585E-05 0.63903 -1.99558E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  7.17182E-05 0.62574 -8.78938E-07 1.00000 -1.88923E-05 1.00000  2.12320E-04 0.83896 ];
INF_S6                    (idx, [1:   8]) = [  1.34024E-04 0.42059 -3.34046E-06 1.00000  3.20408E-05 0.57894  5.77245E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  7.39918E-06 1.00000  3.43250E-06 0.25187  1.01285E-05 1.00000 -5.91321E-04 0.53298 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49824E-01 0.00045  5.58146E-04 0.00986  1.19335E-03 0.10154  3.13079E-01 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.03757E-02 0.00444 -1.19212E-04 0.03904  2.95699E-04 0.21569  1.79630E-02 0.06674 ];
INF_SP2                   (idx, [1:   8]) = [  4.47426E-03 0.01146 -1.33048E-05 0.09111  2.51078E-05 0.30217  2.46612E-03 0.30642 ];
INF_SP3                   (idx, [1:   8]) = [  1.02400E-03 0.07181 -1.10321E-05 0.34142 -4.52697E-05 0.27245  5.87466E-04 0.69693 ];
INF_SP4                   (idx, [1:   8]) = [  2.51219E-04 0.19946 -2.60858E-06 0.67615 -5.56585E-05 0.63903 -1.99558E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  7.15557E-05 0.62703 -8.78938E-07 1.00000 -1.88923E-05 1.00000  2.12320E-04 0.83896 ];
INF_SP6                   (idx, [1:   8]) = [  1.33717E-04 0.42289 -3.34046E-06 1.00000  3.20408E-05 0.57894  5.77245E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  7.17312E-06 1.00000  3.43250E-06 0.25187  1.01285E-05 1.00000 -5.91321E-04 0.53298 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.26917E-01 0.00527 -2.11586E-01 0.02639 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.24528E-01 0.00867 -1.16518E-01 0.01543 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.34571E-01 0.00586 -1.26798E-01 0.02942 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.07703E-01 0.00342  4.54541E-01 0.14931 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.80835E-01 0.00528 -1.57755E+00 0.02579 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.35587E-01 0.00865 -2.86216E+00 0.01552 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.23596E-01 0.00589 -2.63353E+00 0.03017 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.08332E+00 0.00343  7.63044E-01 0.13044 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.25295E-03 0.02795  2.05116E-04 0.16493  1.13639E-03 0.07998  8.58677E-04 0.08161  1.39999E-03 0.06871  2.25333E-03 0.04549  6.36292E-04 0.10334  6.00985E-04 0.11001  1.62174E-04 0.17391 ];
LAMBDA                    (idx, [1:  18]) = [  4.02840E-01 0.04651  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30610E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05882E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80619 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58746E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '8' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.75651E+04 0.02024  8.49369E+04 0.00821  1.96877E+05 0.00288  3.36140E+05 0.00129  3.43991E+05 0.00420  3.14640E+05 0.00261  2.79133E+05 0.00492  2.42048E+05 0.00255  2.09755E+05 0.00176  1.86605E+05 0.00249  1.67939E+05 0.00420  1.53336E+05 0.00215  1.40555E+05 0.00369  1.33347E+05 0.00201  1.28526E+05 0.00214  1.08358E+05 0.00558  1.05731E+05 0.00561  1.00368E+05 0.00072  9.56688E+04 0.00523  1.76760E+05 0.00379  1.52032E+05 0.00300  9.78008E+04 0.00407  5.75745E+04 0.00268  5.97565E+04 0.00614  5.06729E+04 0.00283  3.90325E+04 0.00610  6.19214E+04 0.00322  1.31904E+04 0.00355  1.62780E+04 0.00462  1.47421E+04 0.01251  8.31056E+03 0.01505  1.45567E+04 0.00608  9.67832E+03 0.00896  7.85350E+03 0.00884  1.37051E+03 0.01782  1.28309E+03 0.00513  1.41973E+03 0.03793  1.41102E+03 0.02523  1.38365E+03 0.00595  1.36423E+03 0.01825  1.39488E+03 0.02534  1.33096E+03 0.02049  2.50519E+03 0.02067  3.98807E+03 0.01744  4.84840E+03 0.02367  1.18405E+04 0.01324  1.11526E+04 0.01213  1.01926E+04 0.00696  5.25228E+03 0.00117  3.12467E+03 0.00871  2.07228E+03 0.02416  2.11232E+03 0.03007  3.34398E+03 0.01109  3.57754E+03 0.01980  5.44415E+03 0.01887  6.60375E+03 0.01296  8.70052E+03 0.01007  5.31759E+03 0.00917  3.87151E+03 0.01361  2.79763E+03 0.00384  2.48753E+03 0.00192  2.47175E+03 0.00699  2.01895E+03 0.03504  1.34841E+03 0.02038  1.19558E+03 0.01311  1.02216E+03 0.02749  7.56969E+02 0.03629  5.57207E+02 0.03131  2.93954E+02 0.00402  7.67337E+01 0.01758 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.69470E+00 0.00278 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.16032E+00 0.00040  8.58030E-02 0.00445 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56523E-01 0.00044  4.63133E-01 0.00072 ];
INF_CAPT                  (idx, [1:   4]) = [  2.37989E-03 0.00187  2.30160E-02 0.00287 ];
INF_ABS                   (idx, [1:   4]) = [  6.14507E-03 0.00192  1.49730E-01 0.00217 ];
INF_FISS                  (idx, [1:   4]) = [  3.76518E-03 0.00218  1.26714E-01 0.00205 ];
INF_NSF                   (idx, [1:   4]) = [  9.21003E-03 0.00216  3.08692E-01 0.00205 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44611E+00 1.9E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 7.8E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.55018E-08 0.00224  1.95956E-06 0.00134 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50392E-01 0.00047  3.14708E-01 0.00126 ];
INF_SCATT1                (idx, [1:   4]) = [  2.02620E-02 0.00305  1.97586E-02 0.02152 ];
INF_SCATT2                (idx, [1:   4]) = [  4.31978E-03 0.00561  2.88240E-03 0.16185 ];
INF_SCATT3                (idx, [1:   4]) = [  9.63855E-04 0.03621  4.43898E-04 0.80045 ];
INF_SCATT4                (idx, [1:   4]) = [  2.31791E-04 0.22037 -1.85079E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  9.61239E-05 0.34309  1.46956E-04 0.98997 ];
INF_SCATT6                (idx, [1:   4]) = [  9.74356E-05 0.20163 -1.81044E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.41701E-05 0.15938  4.27115E-04 0.36717 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50393E-01 0.00047  3.14708E-01 0.00126 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.02619E-02 0.00304  1.97586E-02 0.02152 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.31983E-03 0.00559  2.88240E-03 0.16185 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.63892E-04 0.03629  4.43898E-04 0.80045 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.31839E-04 0.22025 -1.85079E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.61549E-05 0.34337  1.46956E-04 0.98997 ];
INF_SCATTP6               (idx, [1:   4]) = [  9.74030E-05 0.20246 -1.81044E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.42670E-05 0.15844  4.27115E-04 0.36717 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02987E-01 0.00029  4.24485E-01 0.00161 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64214E+00 0.00029  7.85270E-01 0.00161 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.14355E-03 0.00193  1.49730E-01 0.00217 ];
INF_REMXS                 (idx, [1:   4]) = [  6.76254E-03 0.00275  1.49532E-01 0.00239 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49761E-01 0.00050  6.30802E-04 0.01069  1.10693E-03 0.03959  3.13601E-01 0.00118 ];
INF_S1                    (idx, [1:   8]) = [  2.04091E-02 0.00293 -1.47095E-04 0.02357  2.60701E-04 0.14197  1.94979E-02 0.02176 ];
INF_S2                    (idx, [1:   8]) = [  4.33076E-03 0.00627 -1.09868E-05 0.29081 -4.17770E-05 1.00000  2.92418E-03 0.15466 ];
INF_S3                    (idx, [1:   8]) = [  9.75935E-04 0.03805 -1.20797E-05 0.19023 -5.54579E-05 0.37010  4.99355E-04 0.70884 ];
INF_S4                    (idx, [1:   8]) = [  2.35167E-04 0.22857 -3.37582E-06 0.79878 -3.05908E-05 0.35965 -1.54488E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  9.49509E-05 0.35537  1.17302E-06 1.00000 -3.26125E-06 1.00000  1.50217E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  9.84264E-05 0.19517 -9.90810E-07 1.00000 -2.27090E-05 0.61406 -1.58335E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  2.13600E-05 0.12362  2.81014E-06 0.51500 -1.12126E-05 0.85328  4.38327E-04 0.36907 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49762E-01 0.00050  6.30802E-04 0.01069  1.10693E-03 0.03959  3.13601E-01 0.00118 ];
INF_SP1                   (idx, [1:   8]) = [  2.04089E-02 0.00292 -1.47095E-04 0.02357  2.60701E-04 0.14197  1.94979E-02 0.02176 ];
INF_SP2                   (idx, [1:   8]) = [  4.33082E-03 0.00625 -1.09868E-05 0.29081 -4.17770E-05 1.00000  2.92418E-03 0.15466 ];
INF_SP3                   (idx, [1:   8]) = [  9.75971E-04 0.03813 -1.20797E-05 0.19023 -5.54579E-05 0.37010  4.99355E-04 0.70884 ];
INF_SP4                   (idx, [1:   8]) = [  2.35215E-04 0.22845 -3.37582E-06 0.79878 -3.05908E-05 0.35965 -1.54488E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  9.49819E-05 0.35552  1.17302E-06 1.00000 -3.26125E-06 1.00000  1.50217E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  9.83938E-05 0.19594 -9.90810E-07 1.00000 -2.27090E-05 0.61406 -1.58335E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  2.14568E-05 0.12290  2.81014E-06 0.51500 -1.12126E-05 0.85328  4.38327E-04 0.36907 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.31459E-01 0.00541 -1.89068E-01 0.01215 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.38486E-01 0.00655 -1.06223E-01 0.00613 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.34120E-01 0.00135 -1.09355E-01 0.00714 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.10214E-01 0.00897  3.73652E-01 0.04654 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.72618E-01 0.00539 -1.76355E+00 0.01209 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.19072E-01 0.00659 -3.13830E+00 0.00615 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.24082E-01 0.00135 -3.04848E+00 0.00715 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.07470E+00 0.00889  8.96128E-01 0.04835 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.42356E-03 0.02915  2.62582E-04 0.12470  1.14466E-03 0.06909  8.03656E-04 0.08831  1.38676E-03 0.06884  2.32753E-03 0.05067  7.39459E-04 0.08832  5.87099E-04 0.08495  1.71816E-04 0.18716 ];
LAMBDA                    (idx, [1:  18]) = [  4.03749E-01 0.04073  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30611E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05885E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80619 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58741E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '7' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.12022E+04 0.03544  5.27431E+04 0.01321  1.20566E+05 0.00789  2.06699E+05 0.00202  2.11533E+05 0.00382  1.91551E+05 0.00684  1.71906E+05 0.00170  1.49594E+05 0.00331  1.30300E+05 0.00168  1.14900E+05 0.00351  1.04592E+05 0.00461  9.56735E+04 0.00134  8.80067E+04 0.00405  8.39267E+04 0.00201  8.08967E+04 0.00471  6.80604E+04 0.00690  6.67698E+04 0.00378  6.33488E+04 0.00571  6.00271E+04 0.00386  1.12814E+05 0.00131  9.79321E+04 0.00239  6.39456E+04 0.00456  3.84060E+04 0.00683  4.04193E+04 0.00941  3.45957E+04 0.00863  2.68650E+04 0.01097  4.33004E+04 0.00729  9.01484E+03 0.00969  1.10581E+04 0.01599  1.03948E+04 0.00855  5.89774E+03 0.01320  1.01097E+04 0.00393  6.72214E+03 0.00359  5.24424E+03 0.01829  9.32285E+02 0.02883  9.02610E+02 0.05486  9.33894E+02 0.03691  9.18837E+02 0.01769  1.04732E+03 0.03288  9.64331E+02 0.01094  9.46049E+02 0.03538  9.64446E+02 0.04434  1.69201E+03 0.00822  2.69806E+03 0.00609  3.27660E+03 0.01494  8.51783E+03 0.01132  7.92008E+03 0.01400  7.37649E+03 0.00549  4.01451E+03 0.01108  2.42112E+03 0.02069  1.56652E+03 0.01930  1.60601E+03 0.01074  2.51998E+03 0.02972  2.90772E+03 0.02985  4.34298E+03 0.01484  5.56960E+03 0.00662  7.57122E+03 0.01216  4.91712E+03 0.01208  3.62007E+03 0.00543  2.72458E+03 0.02429  2.46457E+03 0.01362  2.48301E+03 0.01222  2.08499E+03 0.01057  1.43462E+03 0.01183  1.32492E+03 0.01351  1.14448E+03 0.01979  9.27806E+02 0.01433  6.70161E+02 0.02453  3.69401E+02 0.02294  9.83432E+01 0.03493 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.71135E+00 0.00263 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.61312E+00 0.00170  7.20889E-02 0.00450 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56576E-01 0.00025  4.55663E-01 0.00083 ];
INF_CAPT                  (idx, [1:   4]) = [  2.26777E-03 0.00634  2.19188E-02 0.00254 ];
INF_ABS                   (idx, [1:   4]) = [  5.74270E-03 0.00432  1.42706E-01 0.00250 ];
INF_FISS                  (idx, [1:   4]) = [  3.47493E-03 0.00300  1.20787E-01 0.00250 ];
INF_NSF                   (idx, [1:   4]) = [  8.49761E-03 0.00300  2.94254E-01 0.00250 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44540E+00 5.7E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 2.9E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.84121E-08 0.00138  2.11630E-06 0.00207 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50807E-01 0.00018  3.12826E-01 0.00156 ];
INF_SCATT1                (idx, [1:   4]) = [  1.99929E-02 0.00298  1.78515E-02 0.04387 ];
INF_SCATT2                (idx, [1:   4]) = [  4.20245E-03 0.02537  1.33896E-03 0.17802 ];
INF_SCATT3                (idx, [1:   4]) = [  9.05125E-04 0.06499  9.58173E-05 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  2.87288E-04 0.13781  3.55268E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63867E-04 0.20740 -3.03382E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.18768E-04 0.14752 -5.56767E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  2.02196E-06 1.00000 -3.56794E-04 0.28843 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50808E-01 0.00019  3.12826E-01 0.00156 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.99932E-02 0.00295  1.78515E-02 0.04387 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.20275E-03 0.02539  1.33896E-03 0.17802 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.04988E-04 0.06468  9.58173E-05 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.87337E-04 0.13834  3.55268E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63897E-04 0.20731 -3.03382E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.18993E-04 0.14736 -5.56767E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.96579E-06 1.00000 -3.56794E-04 0.28843 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03611E-01 0.00089  4.19274E-01 0.00187 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63711E+00 0.00089  7.95031E-01 0.00187 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.74130E-03 0.00423  1.42706E-01 0.00250 ];
INF_REMXS                 (idx, [1:   4]) = [  6.49810E-03 0.00472  1.43743E-01 0.00569 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50078E-01 0.00014  7.28206E-04 0.01610  9.06691E-04 0.07095  3.11920E-01 0.00161 ];
INF_S1                    (idx, [1:   8]) = [  2.01664E-02 0.00299 -1.73458E-04 0.00477  2.05654E-04 0.10641  1.76458E-02 0.04436 ];
INF_S2                    (idx, [1:   8]) = [  4.21593E-03 0.02600 -1.34797E-05 0.28571 -3.54178E-05 0.80098  1.37438E-03 0.15599 ];
INF_S3                    (idx, [1:   8]) = [  9.17533E-04 0.06469 -1.24074E-05 0.09688 -4.99523E-05 0.56347  1.45770E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  2.94563E-04 0.12935 -7.27493E-06 0.20712 -1.66190E-05 1.00000  3.71887E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.61809E-04 0.20500  2.05765E-06 1.00000 -3.39961E-05 0.42931 -2.69386E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.19064E-04 0.14292 -2.96008E-07 1.00000 -7.36086E-06 1.00000 -5.49406E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.83350E-06 1.00000 -1.81154E-06 1.00000 -1.04502E-06 1.00000 -3.55749E-04 0.29950 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50080E-01 0.00014  7.28206E-04 0.01610  9.06691E-04 0.07095  3.11920E-01 0.00161 ];
INF_SP1                   (idx, [1:   8]) = [  2.01667E-02 0.00296 -1.73458E-04 0.00477  2.05654E-04 0.10641  1.76458E-02 0.04436 ];
INF_SP2                   (idx, [1:   8]) = [  4.21623E-03 0.02603 -1.34797E-05 0.28571 -3.54178E-05 0.80098  1.37438E-03 0.15599 ];
INF_SP3                   (idx, [1:   8]) = [  9.17395E-04 0.06438 -1.24074E-05 0.09688 -4.99523E-05 0.56347  1.45770E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  2.94612E-04 0.12987 -7.27493E-06 0.20712 -1.66190E-05 1.00000  3.71887E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.61840E-04 0.20495  2.05765E-06 1.00000 -3.39961E-05 0.42931 -2.69386E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.19289E-04 0.14288 -2.96008E-07 1.00000 -7.36086E-06 1.00000 -5.49406E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.77733E-06 1.00000 -1.81154E-06 1.00000 -1.04502E-06 1.00000 -3.55749E-04 0.29950 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.35241E-01 0.00300 -1.85144E-01 0.02626 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.23468E-01 0.00723 -1.09319E-01 0.04352 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.37754E-01 0.00349 -1.04013E-01 0.02829 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.20287E-01 0.00890  3.90608E-01 0.07229 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.65874E-01 0.00300 -1.80283E+00 0.02561 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.36846E-01 0.00728 -3.06084E+00 0.04386 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.19877E-01 0.00349 -3.20980E+00 0.02800 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.04090E+00 0.00883  8.62154E-01 0.07062 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.30421E-03 0.03533  2.94666E-04 0.20291  1.14847E-03 0.07636  7.89340E-04 0.10072  1.59916E-03 0.09076  2.04343E-03 0.06017  6.43692E-04 0.09970  5.76203E-04 0.10566  2.09246E-04 0.20030 ];
LAMBDA                    (idx, [1:  18]) = [  4.16405E-01 0.05771  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30612E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05888E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80618 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58736E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '6' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  6.00373E+03 0.02935  2.87022E+04 0.01318  6.54626E+04 0.00982  1.10989E+05 0.00325  1.13534E+05 0.00623  1.04320E+05 0.00361  9.30594E+04 0.00440  8.05015E+04 0.00612  7.10950E+04 0.00594  6.29517E+04 0.00508  5.66683E+04 0.00411  5.23752E+04 0.00747  4.78286E+04 0.00207  4.58617E+04 0.00882  4.40427E+04 0.01216  3.77427E+04 0.00636  3.68435E+04 0.00231  3.47434E+04 0.00541  3.32789E+04 0.01173  6.27017E+04 0.00747  5.48780E+04 0.00306  3.59900E+04 0.00762  2.13838E+04 0.00955  2.27890E+04 0.00054  1.97811E+04 0.00862  1.53316E+04 0.00304  2.49303E+04 0.01061  5.06036E+03 0.01942  6.31130E+03 0.01167  5.92209E+03 0.00937  3.24826E+03 0.00414  5.84873E+03 0.00762  3.95676E+03 0.03289  2.98471E+03 0.02343  5.23710E+02 0.02993  5.17890E+02 0.01697  5.57700E+02 0.03634  5.55220E+02 0.02216  5.93610E+02 0.00707  5.82195E+02 0.04599  5.35807E+02 0.02204  5.21905E+02 0.03317  9.83602E+02 0.02267  1.56864E+03 0.01900  1.92939E+03 0.02043  4.84789E+03 0.02249  4.65692E+03 0.01264  4.38906E+03 0.00938  2.32197E+03 0.02099  1.43268E+03 0.02155  9.68885E+02 0.02616  9.68977E+02 0.01637  1.62284E+03 0.01324  1.66535E+03 0.02937  2.62862E+03 0.00738  3.39370E+03 0.01706  4.78530E+03 0.00620  3.09522E+03 0.00827  2.39574E+03 0.00868  1.83073E+03 0.02541  1.75803E+03 0.02474  1.75794E+03 0.00548  1.52425E+03 0.04227  1.01808E+03 0.01961  9.00022E+02 0.02073  8.62170E+02 0.01528  7.03946E+02 0.02783  5.02572E+02 0.01346  2.91237E+02 0.03401  8.05829E+01 0.04733 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.72600E+00 0.00401 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.43101E+00 0.00093  4.55603E-02 0.00484 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56186E-01 0.00059  4.42713E-01 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  2.10359E-03 0.00194  2.00502E-02 0.00123 ];
INF_ABS                   (idx, [1:   4]) = [  5.21986E-03 0.00237  1.30322E-01 0.00100 ];
INF_FISS                  (idx, [1:   4]) = [  3.11627E-03 0.00268  1.10272E-01 0.00096 ];
INF_NSF                   (idx, [1:   4]) = [  7.61922E-03 0.00267  2.68637E-01 0.00096 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44498E+00 1.7E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 1.0E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.98814E-08 0.00110  2.20735E-06 0.00085 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50984E-01 0.00064  3.11771E-01 0.00213 ];
INF_SCATT1                (idx, [1:   4]) = [  1.99045E-02 0.01218  1.90463E-02 0.06808 ];
INF_SCATT2                (idx, [1:   4]) = [  4.33452E-03 0.02774  1.81663E-03 0.28131 ];
INF_SCATT3                (idx, [1:   4]) = [  8.07272E-04 0.05400  7.55321E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.66372E-04 0.15582  1.42095E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.60848E-04 0.36543  2.68243E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  6.09649E-05 0.55500 -3.58528E-04 0.52840 ];
INF_SCATT7                (idx, [1:   4]) = [  1.23249E-05 0.53637  1.32647E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50985E-01 0.00064  3.11771E-01 0.00213 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.99043E-02 0.01218  1.90463E-02 0.06808 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.33426E-03 0.02772  1.81663E-03 0.28131 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.07240E-04 0.05407  7.55321E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.66439E-04 0.15547  1.42095E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.60805E-04 0.36626  2.68243E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  6.08561E-05 0.55479 -3.58528E-04 0.52840 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.25274E-05 0.52230  1.32647E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04043E-01 0.00211  4.06800E-01 0.00282 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63366E+00 0.00212  8.19417E-01 0.00282 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.21846E-03 0.00242  1.30322E-01 0.00100 ];
INF_REMXS                 (idx, [1:   4]) = [  5.94213E-03 0.00700  1.31806E-01 0.00376 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50244E-01 0.00070  7.39699E-04 0.01903  8.64257E-04 0.04140  3.10906E-01 0.00202 ];
INF_S1                    (idx, [1:   8]) = [  2.00729E-02 0.01175 -1.68456E-04 0.04236  2.58432E-04 0.08365  1.87878E-02 0.06811 ];
INF_S2                    (idx, [1:   8]) = [  4.35452E-03 0.02691 -2.00054E-05 0.17007  9.79935E-05 0.17792  1.71863E-03 0.29370 ];
INF_S3                    (idx, [1:   8]) = [  8.12798E-04 0.05039 -5.52639E-06 0.90469  3.12278E-05 1.00000  7.24093E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.71952E-04 0.15427 -5.57938E-06 0.73318  6.94226E-06 1.00000  1.35153E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.62680E-04 0.33335 -1.83190E-06 1.00000 -2.71153E-05 0.57501  2.95358E-04 0.98170 ];
INF_S6                    (idx, [1:   8]) = [  5.76146E-05 0.53545  3.35024E-06 1.00000  4.37054E-06 1.00000 -3.62898E-04 0.44398 ];
INF_S7                    (idx, [1:   8]) = [  1.30609E-05 0.77049 -7.35942E-07 1.00000 -3.83054E-05 0.50526  3.96319E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50246E-01 0.00070  7.39699E-04 0.01903  8.64257E-04 0.04140  3.10906E-01 0.00202 ];
INF_SP1                   (idx, [1:   8]) = [  2.00728E-02 0.01174 -1.68456E-04 0.04236  2.58432E-04 0.08365  1.87878E-02 0.06811 ];
INF_SP2                   (idx, [1:   8]) = [  4.35427E-03 0.02689 -2.00054E-05 0.17007  9.79935E-05 0.17792  1.71863E-03 0.29370 ];
INF_SP3                   (idx, [1:   8]) = [  8.12766E-04 0.05047 -5.52639E-06 0.90469  3.12278E-05 1.00000  7.24093E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.72018E-04 0.15399 -5.57938E-06 0.73318  6.94226E-06 1.00000  1.35153E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.62637E-04 0.33417 -1.83190E-06 1.00000 -2.71153E-05 0.57501  2.95358E-04 0.98170 ];
INF_SP6                   (idx, [1:   8]) = [  5.75058E-05 0.53535  3.35024E-06 1.00000  4.37054E-06 1.00000 -3.62898E-04 0.44398 ];
INF_SP7                   (idx, [1:   8]) = [  1.32633E-05 0.75561 -7.35942E-07 1.00000 -3.83054E-05 0.50526  3.96319E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.18275E-01 0.00300 -1.92440E-01 0.02249 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.00754E-01 0.01119 -1.13950E-01 0.01233 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.88254E-01 0.01682 -1.10367E-01 0.02904 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.19963E-01 0.00698  4.46497E-01 0.04657 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.96938E-01 0.00300 -1.73386E+00 0.02207 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.65829E-01 0.01115 -2.92615E+00 0.01233 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.83095E-01 0.01702 -3.02525E+00 0.02861 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.04189E+00 0.00694  7.49813E-01 0.04674 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.10288E-03 0.04081  1.81643E-04 0.18724  1.16562E-03 0.10361  6.75468E-04 0.14607  1.61740E-03 0.09473  2.05548E-03 0.07838  6.71297E-04 0.12185  5.98467E-04 0.14004  1.37516E-04 0.21866 ];
LAMBDA                    (idx, [1:  18]) = [  3.88965E-01 0.05557  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30613E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05892E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80618 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58730E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '5' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.72974E+03 0.03518  2.74713E+04 0.01392  6.30457E+04 0.00219  1.08494E+05 0.00760  1.10608E+05 0.00208  1.00730E+05 0.00181  8.98971E+04 0.00598  7.76912E+04 0.00660  6.83277E+04 0.00798  6.11128E+04 0.00434  5.53591E+04 0.00701  5.11999E+04 0.00610  4.76852E+04 0.00267  4.51948E+04 0.00557  4.38755E+04 0.00191  3.68389E+04 0.01180  3.64287E+04 0.00645  3.48330E+04 0.00149  3.32107E+04 0.01171  6.20257E+04 0.00402  5.51978E+04 0.00132  3.61640E+04 0.00491  2.23497E+04 0.00631  2.37072E+04 0.00559  2.05331E+04 0.00642  1.61774E+04 0.00943  2.65408E+04 0.01045  5.48801E+03 0.01632  6.69281E+03 0.00957  6.26113E+03 0.00517  3.42676E+03 0.02709  5.97046E+03 0.02479  4.03037E+03 0.01861  3.21391E+03 0.00407  5.89379E+02 0.04279  5.43395E+02 0.03888  6.03887E+02 0.02380  6.01506E+02 0.04962  5.94838E+02 0.02984  5.36184E+02 0.01512  5.96630E+02 0.05906  5.79837E+02 0.01952  1.04257E+03 0.01177  1.63485E+03 0.04137  2.09277E+03 0.01728  5.16621E+03 0.00675  5.00115E+03 0.00567  4.70237E+03 0.02216  2.51964E+03 0.01155  1.55668E+03 0.04114  1.02441E+03 0.02133  1.15307E+03 0.05355  1.80162E+03 0.04368  1.87964E+03 0.02199  3.05449E+03 0.00668  4.06854E+03 0.02526  5.89717E+03 0.00422  4.03030E+03 0.00961  3.08001E+03 0.01722  2.35972E+03 0.01721  2.31050E+03 0.03051  2.32945E+03 0.00800  2.00263E+03 0.01941  1.37765E+03 0.00895  1.35983E+03 0.02378  1.19293E+03 0.03576  9.83937E+02 0.00933  7.49137E+02 0.01824  4.31702E+02 0.01887  1.21590E+02 0.08499 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.72956E+00 0.00433 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.41027E+00 0.00146  5.49947E-02 0.00110 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56088E-01 0.00054  4.31953E-01 0.00093 ];
INF_CAPT                  (idx, [1:   4]) = [  1.97842E-03 0.00250  1.84900E-02 0.00372 ];
INF_ABS                   (idx, [1:   4]) = [  4.80834E-03 0.00287  1.19936E-01 0.00311 ];
INF_FISS                  (idx, [1:   4]) = [  2.82992E-03 0.00319  1.01446E-01 0.00301 ];
INF_NSF                   (idx, [1:   4]) = [  6.91753E-03 0.00319  2.47137E-01 0.00301 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44442E+00 1.2E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 1.1E-07  2.02270E+02 9.1E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.22023E-08 0.00541  2.32185E-06 0.00208 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.51216E-01 0.00048  3.11382E-01 0.00251 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97644E-02 0.00270  1.73205E-02 0.02185 ];
INF_SCATT2                (idx, [1:   4]) = [  3.77775E-03 0.02666  1.68181E-03 0.70392 ];
INF_SCATT3                (idx, [1:   4]) = [  8.49053E-04 0.03513  7.54406E-04 0.31333 ];
INF_SCATT4                (idx, [1:   4]) = [  2.31583E-04 0.09589  1.32701E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.02563E-04 0.32475 -5.72920E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  3.74415E-05 1.00000 -9.93693E-04 0.50227 ];
INF_SCATT7                (idx, [1:   4]) = [  6.18363E-05 0.40080 -1.56415E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.51217E-01 0.00048  3.11382E-01 0.00251 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97643E-02 0.00270  1.73205E-02 0.02185 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.77804E-03 0.02672  1.68181E-03 0.70392 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.49340E-04 0.03506  7.54406E-04 0.31333 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.31249E-04 0.09595  1.32701E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.02620E-04 0.32416 -5.72920E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.74295E-05 1.00000 -9.93693E-04 0.50227 ];
INF_SCATTP7               (idx, [1:   4]) = [  6.16029E-05 0.40253 -1.56415E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04139E-01 0.00066  3.99707E-01 0.00126 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63287E+00 0.00066  8.33947E-01 0.00126 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.80716E-03 0.00296  1.19936E-01 0.00311 ];
INF_REMXS                 (idx, [1:   4]) = [  5.68308E-03 0.00413  1.21287E-01 0.00918 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50405E-01 0.00053  8.11467E-04 0.01610  7.15982E-04 0.06239  3.10666E-01 0.00263 ];
INF_S1                    (idx, [1:   8]) = [  1.99557E-02 0.00244 -1.91372E-04 0.02759  1.44341E-04 0.14140  1.71761E-02 0.02091 ];
INF_S2                    (idx, [1:   8]) = [  3.79520E-03 0.02604 -1.74433E-05 0.16444 -1.47329E-05 0.93149  1.69654E-03 0.70586 ];
INF_S3                    (idx, [1:   8]) = [  8.64298E-04 0.02816 -1.52458E-05 0.36019 -2.62598E-06 1.00000  7.57032E-04 0.29632 ];
INF_S4                    (idx, [1:   8]) = [  2.32864E-04 0.09183 -1.28172E-06 1.00000 -2.71557E-05 0.56651  1.59857E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.06756E-04 0.31478 -4.19334E-06 0.08557 -3.10297E-05 0.47544 -2.62623E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  3.26245E-05 1.00000  4.81700E-06 0.58981 -5.93064E-06 1.00000 -9.87763E-04 0.51025 ];
INF_S7                    (idx, [1:   8]) = [  6.14654E-05 0.38046  3.70901E-07 1.00000  1.85608E-05 0.89828 -1.74976E-04 0.84179 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50406E-01 0.00053  8.11467E-04 0.01610  7.15982E-04 0.06239  3.10666E-01 0.00263 ];
INF_SP1                   (idx, [1:   8]) = [  1.99557E-02 0.00244 -1.91372E-04 0.02759  1.44341E-04 0.14140  1.71761E-02 0.02091 ];
INF_SP2                   (idx, [1:   8]) = [  3.79548E-03 0.02610 -1.74433E-05 0.16444 -1.47329E-05 0.93149  1.69654E-03 0.70586 ];
INF_SP3                   (idx, [1:   8]) = [  8.64586E-04 0.02809 -1.52458E-05 0.36019 -2.62598E-06 1.00000  7.57032E-04 0.29632 ];
INF_SP4                   (idx, [1:   8]) = [  2.32531E-04 0.09196 -1.28172E-06 1.00000 -2.71557E-05 0.56651  1.59857E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.06814E-04 0.31422 -4.19334E-06 0.08557 -3.10297E-05 0.47544 -2.62623E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  3.26125E-05 1.00000  4.81700E-06 0.58981 -5.93064E-06 1.00000 -9.87763E-04 0.51025 ];
INF_SP7                   (idx, [1:   8]) = [  6.12320E-05 0.38218  3.70901E-07 1.00000  1.85608E-05 0.89828 -1.74976E-04 0.84179 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.25953E-01 0.01759 -1.89095E-01 0.02394 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.92096E-01 0.02768 -1.08686E-01 0.01714 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.07018E-01 0.02738 -1.09224E-01 0.02399 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.29310E-01 0.00545  4.06307E-01 0.07726 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.83051E-01 0.01787 -1.76479E+00 0.02375 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.78422E-01 0.02792 -3.06877E+00 0.01743 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.58454E-01 0.02815 -3.05544E+00 0.02454 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.01228E+00 0.00547  8.29845E-01 0.07381 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.22476E-03 0.04099  2.27163E-04 0.20587  1.08282E-03 0.09743  6.18730E-04 0.11873  1.83017E-03 0.10798  1.94557E-03 0.07495  6.43197E-04 0.12765  5.82580E-04 0.13402  2.94522E-04 0.22561 ];
LAMBDA                    (idx, [1:  18]) = [  4.39831E-01 0.06660  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30613E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05895E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80616 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58725E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '4' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.06931E+04 0.01433  5.20775E+04 0.00337  1.19583E+05 0.01089  2.05677E+05 0.00573  2.12032E+05 0.00262  1.93974E+05 0.00283  1.73791E+05 0.00321  1.51382E+05 0.00136  1.33575E+05 0.00235  1.18862E+05 0.00632  1.08458E+05 0.00333  9.98723E+04 0.00151  9.35367E+04 0.00143  8.94403E+04 0.00346  8.60774E+04 0.00430  7.35452E+04 0.00597  7.11693E+04 0.00551  6.90111E+04 0.00312  6.60203E+04 0.00369  1.23719E+05 0.00044  1.10407E+05 0.00292  7.42181E+04 0.00251  4.44468E+04 0.00104  4.76225E+04 0.00551  4.22364E+04 0.01206  3.37600E+04 0.00423  5.49732E+04 0.00402  1.10919E+04 0.00914  1.36619E+04 0.01501  1.25093E+04 0.01065  7.10099E+03 0.00432  1.25013E+04 0.00623  7.97918E+03 0.01056  6.58077E+03 0.01716  1.18596E+03 0.04368  1.17970E+03 0.00530  1.19727E+03 0.02535  1.24704E+03 0.02843  1.26624E+03 0.00226  1.33061E+03 0.06330  1.29443E+03 0.01129  1.16511E+03 0.02998  2.09898E+03 0.03282  3.47400E+03 0.01300  4.17858E+03 0.01656  1.09331E+04 0.01673  1.04521E+04 0.01545  9.94302E+03 0.01014  5.51993E+03 0.02789  3.46605E+03 0.01890  2.35995E+03 0.00325  2.45538E+03 0.03123  3.84894E+03 0.00733  4.36675E+03 0.01464  7.10150E+03 0.00646  9.19144E+03 0.01146  1.35205E+04 0.00742  9.41927E+03 0.00848  7.35670E+03 0.00749  5.61516E+03 0.01873  5.30315E+03 0.01867  5.67801E+03 0.01906  4.86789E+03 0.00252  3.47494E+03 0.00933  3.24070E+03 0.00703  2.92819E+03 0.02295  2.49753E+03 0.01545  1.91728E+03 0.00966  1.14269E+03 0.02672  3.37142E+02 0.03813 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.74513E+00 0.00033 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.76247E+00 0.00086  1.26019E-01 0.00327 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56495E-01 0.00042  4.19727E-01 0.00064 ];
INF_CAPT                  (idx, [1:   4]) = [  1.85688E-03 0.00243  1.67443E-02 0.00254 ];
INF_ABS                   (idx, [1:   4]) = [  4.40857E-03 0.00199  1.08101E-01 0.00222 ];
INF_FISS                  (idx, [1:   4]) = [  2.55169E-03 0.00175  9.13567E-02 0.00216 ];
INF_NSF                   (idx, [1:   4]) = [  6.23608E-03 0.00173  2.22557E-01 0.00216 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44391E+00 2.4E-05  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 1.3E-07  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.40789E-08 0.00247  2.39061E-06 0.00169 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.52086E-01 0.00039  3.10490E-01 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  1.96696E-02 0.00599  1.73456E-02 0.04468 ];
INF_SCATT2                (idx, [1:   4]) = [  4.03202E-03 0.01161  1.30316E-03 0.32782 ];
INF_SCATT3                (idx, [1:   4]) = [  8.22902E-04 0.05848  5.84361E-04 0.29855 ];
INF_SCATT4                (idx, [1:   4]) = [  6.80328E-05 0.67016  3.39568E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.69421E-05 1.00000 -5.32147E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.21749E-04 0.27844  3.70965E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.55856E-05 0.72939  6.26610E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.52087E-01 0.00040  3.10490E-01 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.96697E-02 0.00601  1.73456E-02 0.04468 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.03196E-03 0.01162  1.30316E-03 0.32782 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.22960E-04 0.05843  5.84361E-04 0.29855 ];
INF_SCATTP4               (idx, [1:   4]) = [  6.83437E-05 0.66757  3.39568E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.71620E-05 1.00000 -5.32147E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.21619E-04 0.27881  3.70965E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.54968E-05 0.73064  6.26610E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.05370E-01 0.00011  3.89485E-01 0.00199 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.62309E+00 0.00011  8.55839E-01 0.00199 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.40712E-03 0.00193  1.08101E-01 0.00222 ];
INF_REMXS                 (idx, [1:   4]) = [  5.26713E-03 0.00121  1.09884E-01 0.00097 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51228E-01 0.00045  8.57722E-04 0.01740  6.47887E-04 0.07938  3.09842E-01 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  1.98628E-02 0.00616 -1.93145E-04 0.03129  1.38300E-04 0.16423  1.72073E-02 0.04633 ];
INF_S2                    (idx, [1:   8]) = [  4.04679E-03 0.01057 -1.47764E-05 0.39308 -2.71206E-05 0.33509  1.33028E-03 0.31488 ];
INF_S3                    (idx, [1:   8]) = [  8.42840E-04 0.05932 -1.99379E-05 0.18137 -2.11013E-05 0.61109  6.05463E-04 0.26766 ];
INF_S4                    (idx, [1:   8]) = [  7.74611E-05 0.58794 -9.42825E-06 0.56520 -1.38443E-05 0.79394  3.53413E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.48466E-05 1.00000  2.09549E-06 0.84062 -1.72386E-05 0.92815 -3.59761E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.20254E-04 0.28623  1.49536E-06 0.34790  2.02850E-06 1.00000  3.68936E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.48676E-05 0.75660  7.17946E-07 1.00000  1.42448E-05 1.00000  4.84162E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51230E-01 0.00045  8.57722E-04 0.01740  6.47887E-04 0.07938  3.09842E-01 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  1.98628E-02 0.00617 -1.93145E-04 0.03129  1.38300E-04 0.16423  1.72073E-02 0.04633 ];
INF_SP2                   (idx, [1:   8]) = [  4.04674E-03 0.01058 -1.47764E-05 0.39308 -2.71206E-05 0.33509  1.33028E-03 0.31488 ];
INF_SP3                   (idx, [1:   8]) = [  8.42898E-04 0.05928 -1.99379E-05 0.18137 -2.11013E-05 0.61109  6.05463E-04 0.26766 ];
INF_SP4                   (idx, [1:   8]) = [  7.77720E-05 0.58634 -9.42825E-06 0.56520 -1.38443E-05 0.79394  3.53413E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.50665E-05 1.00000  2.09549E-06 0.84062 -1.72386E-05 0.92815 -3.59761E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.20124E-04 0.28661  1.49536E-06 0.34790  2.02850E-06 1.00000  3.68936E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.47789E-05 0.75797  7.17946E-07 1.00000  1.42448E-05 1.00000  4.84162E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.46231E-01 0.00630 -1.95997E-01 0.00329 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.24450E-01 0.00279 -1.14072E-01 0.00814 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.24072E-01 0.00846 -1.14611E-01 0.00898 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.43986E-01 0.01432  4.60255E-01 0.05845 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.47056E-01 0.00628 -1.70074E+00 0.00329 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.35597E-01 0.00278 -2.92251E+00 0.00821 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.36137E-01 0.00852 -2.90885E+00 0.00899 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.69434E-01 0.01447  7.29144E-01 0.05767 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.99331E-03 0.03205  2.52171E-04 0.16127  1.03630E-03 0.09678  6.75210E-04 0.10221  1.52573E-03 0.07560  2.15138E-03 0.05990  6.20664E-04 0.11258  5.61157E-04 0.12625  1.70701E-04 0.19172 ];
LAMBDA                    (idx, [1:  18]) = [  4.04995E-01 0.05040  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30614E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05897E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80615 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58722E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '3' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.16256E+03 0.05151  9.58156E+03 0.00930  2.26867E+04 0.01547  3.86501E+04 0.01418  4.04662E+04 0.01167  3.67633E+04 0.00336  3.31709E+04 0.01246  2.86557E+04 0.01237  2.57365E+04 0.00471  2.31020E+04 0.01404  2.11647E+04 0.00188  1.97868E+04 0.00241  1.84392E+04 0.01005  1.75952E+04 0.00512  1.68987E+04 0.00772  1.43231E+04 0.01198  1.41295E+04 0.00504  1.38475E+04 0.00718  1.35179E+04 0.00983  2.46260E+04 0.00729  2.23286E+04 0.00114  1.49943E+04 0.00742  9.03905E+03 0.00888  9.83543E+03 0.00963  8.94359E+03 0.01598  7.00190E+03 0.02111  1.13883E+04 0.01126  2.28341E+03 0.01800  2.82852E+03 0.01631  2.50189E+03 0.02243  1.47811E+03 0.03557  2.38268E+03 0.01020  1.64884E+03 0.03672  1.39494E+03 0.03703  2.51703E+02 0.05641  2.54802E+02 0.04006  2.71760E+02 0.06351  2.39182E+02 0.02621  2.85129E+02 0.07577  2.50961E+02 0.12077  2.55003E+02 0.06264  2.47786E+02 0.02710  4.65679E+02 0.02892  7.03830E+02 0.04213  9.40674E+02 0.02199  2.36233E+03 0.01928  2.20450E+03 0.02656  2.15463E+03 0.03344  1.18142E+03 0.01526  7.41676E+02 0.00997  4.92691E+02 0.05053  5.29702E+02 0.06059  8.39861E+02 0.01153  1.02781E+03 0.04730  1.56801E+03 0.03438  2.09304E+03 0.02715  3.21485E+03 0.01618  2.31798E+03 0.02390  1.90511E+03 0.00519  1.43353E+03 0.02418  1.38665E+03 0.01685  1.54831E+03 0.05673  1.33430E+03 0.01564  9.56004E+02 0.00933  8.99372E+02 0.00574  8.73932E+02 0.05421  7.28380E+02 0.03258  5.71218E+02 0.00957  3.76358E+02 0.02631  1.08441E+02 0.09839 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.77797E+00 0.00926 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  5.39947E-01 0.00204  3.04916E-02 0.01144 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.57395E-01 0.00040  4.12900E-01 0.00064 ];
INF_CAPT                  (idx, [1:   4]) = [  1.78083E-03 0.00680  1.57194E-02 0.00227 ];
INF_ABS                   (idx, [1:   4]) = [  4.13159E-03 0.00305  1.01181E-01 0.00210 ];
INF_FISS                  (idx, [1:   4]) = [  2.35076E-03 0.00121  8.54613E-02 0.00207 ];
INF_NSF                   (idx, [1:   4]) = [  5.74351E-03 0.00122  2.08195E-01 0.00207 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44326E+00 8.0E-06  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 3.3E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.64644E-08 0.00378  2.54191E-06 0.00180 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.53286E-01 0.00065  3.11321E-01 0.00208 ];
INF_SCATT1                (idx, [1:   4]) = [  1.93864E-02 0.01129  1.74879E-02 0.04268 ];
INF_SCATT2                (idx, [1:   4]) = [  3.81776E-03 0.01537  1.52191E-03 0.68834 ];
INF_SCATT3                (idx, [1:   4]) = [  6.88026E-04 0.23783  3.40895E-04 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.57742E-06 1.00000  6.93412E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  5.51133E-06 1.00000 -2.73916E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [ -6.19980E-05 1.00000  8.36619E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  6.16804E-05 0.46320  3.84868E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.53287E-01 0.00065  3.11321E-01 0.00208 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.93871E-02 0.01132  1.74879E-02 0.04268 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.81784E-03 0.01549  1.52191E-03 0.68834 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.88032E-04 0.23798  3.40895E-04 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.44460E-06 1.00000  6.93412E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  5.51046E-06 1.00000 -2.73916E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [ -6.23281E-05 1.00000  8.36619E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  6.12606E-05 0.46377  3.84868E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.07195E-01 0.00205  3.82700E-01 0.00209 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.60880E+00 0.00204  8.71011E-01 0.00209 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.13034E-03 0.00312  1.01181E-01 0.00210 ];
INF_REMXS                 (idx, [1:   4]) = [  5.05716E-03 0.01115  1.02137E-01 0.00370 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.52338E-01 0.00061  9.48348E-04 0.01744  5.58747E-04 0.03646  3.10762E-01 0.00202 ];
INF_S1                    (idx, [1:   8]) = [  1.96108E-02 0.01103 -2.24328E-04 0.04212  8.35572E-05 0.43816  1.74043E-02 0.04085 ];
INF_S2                    (idx, [1:   8]) = [  3.83518E-03 0.01601 -1.74200E-05 0.15741 -6.01106E-06 1.00000  1.52792E-03 0.67504 ];
INF_S3                    (idx, [1:   8]) = [  7.10025E-04 0.21931 -2.19983E-05 0.77928 -1.68142E-05 0.58473  3.57709E-04 1.00000 ];
INF_S4                    (idx, [1:   8]) = [ -3.27974E-06 1.00000 -2.29768E-06 1.00000 -3.74252E-05 0.28085  7.30837E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.29635E-05 1.00000 -7.45213E-06 1.00000 -3.21377E-05 0.19470 -2.41778E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [ -7.58754E-05 1.00000  1.38774E-05 0.21600 -5.08842E-05 0.44568  1.34546E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  6.12929E-05 0.37135  3.87456E-07 1.00000  8.94847E-06 1.00000  3.75919E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.52339E-01 0.00061  9.48348E-04 0.01744  5.58747E-04 0.03646  3.10762E-01 0.00202 ];
INF_SP1                   (idx, [1:   8]) = [  1.96114E-02 0.01105 -2.24328E-04 0.04212  8.35572E-05 0.43816  1.74043E-02 0.04085 ];
INF_SP2                   (idx, [1:   8]) = [  3.83526E-03 0.01613 -1.74200E-05 0.15741 -6.01106E-06 1.00000  1.52792E-03 0.67504 ];
INF_SP3                   (idx, [1:   8]) = [  7.10030E-04 0.21942 -2.19983E-05 0.77928 -1.68142E-05 0.58473  3.57709E-04 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [ -3.14693E-06 1.00000 -2.29768E-06 1.00000 -3.74252E-05 0.28085  7.30837E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.29626E-05 1.00000 -7.45213E-06 1.00000 -3.21377E-05 0.19470 -2.41778E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [ -7.62055E-05 1.00000  1.38774E-05 0.21600 -5.08842E-05 0.44568  1.34546E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  6.08731E-05 0.37065  3.87456E-07 1.00000  8.94847E-06 1.00000  3.75919E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.26816E-01 0.00297 -2.12216E-01 0.00807 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.78055E-01 0.02392 -1.23460E-01 0.00837 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.74525E-01 0.02079 -1.28705E-01 0.02872 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.54050E-01 0.01333  5.77632E-01 0.06555 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.80991E-01 0.00296 -1.57093E+00 0.00804 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.98075E-01 0.02409 -2.70030E+00 0.00833 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.03076E-01 0.02116 -2.59430E+00 0.02949 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.41823E-01 0.01339  5.81817E-01 0.06230 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.84613E-03 0.07064  1.41819E-04 0.26827  9.10732E-04 0.19859  5.66544E-04 0.21364  1.52915E-03 0.19499  2.09902E-03 0.09575  7.02494E-04 0.22659  7.57662E-04 0.21400  1.38708E-04 0.39188 ];
LAMBDA                    (idx, [1:  18]) = [  4.48725E-01 0.10854  1.24667E-02 5.0E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 1.9E-09  1.63478E+00 0.0E+00  3.55460E+00 6.8E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30614E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05900E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80614 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58717E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '2' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.50788E+04 0.03083  7.29069E+04 0.00424  1.64714E+05 0.00401  2.90907E+05 0.00219  3.01707E+05 0.00120  2.76260E+05 0.00034  2.50604E+05 0.00259  2.19935E+05 0.00613  1.95802E+05 0.00557  1.76897E+05 0.00314  1.64786E+05 0.00433  1.52281E+05 0.00138  1.42283E+05 0.00167  1.36739E+05 0.00083  1.31025E+05 0.00422  1.11700E+05 0.00224  1.10512E+05 0.00167  1.06273E+05 0.00235  1.02055E+05 0.00168  1.93549E+05 0.00252  1.74591E+05 0.00332  1.18867E+05 0.00216  7.27948E+04 0.00562  8.02129E+04 0.00624  7.16252E+04 0.00395  5.69896E+04 0.00606  9.55730E+04 0.00849  1.96303E+04 0.00764  2.35753E+04 0.00340  2.09017E+04 0.00431  1.21747E+04 0.00255  2.05872E+04 0.00342  1.36665E+04 0.01184  1.14129E+04 0.00604  2.11778E+03 0.01314  2.17050E+03 0.03188  2.16940E+03 0.00986  2.22119E+03 0.01592  2.11116E+03 0.04931  2.16452E+03 0.00980  2.25240E+03 0.03349  2.03296E+03 0.00484  3.85561E+03 0.00507  6.04437E+03 0.01913  7.56028E+03 0.01159  1.90468E+04 0.00817  1.86503E+04 0.00353  1.78404E+04 0.00912  1.00398E+04 0.01925  6.52096E+03 0.00147  4.43210E+03 0.01422  4.63597E+03 0.00636  7.59871E+03 0.01418  8.62511E+03 0.02491  1.38917E+04 0.00493  1.87882E+04 0.00155  2.99192E+04 0.00190  2.18933E+04 0.00289  1.78724E+04 0.01136  1.42704E+04 0.00566  1.37525E+04 0.00119  1.47123E+04 0.01078  1.34482E+04 0.00285  9.58779E+03 0.00206  9.29429E+03 0.00444  8.60263E+03 0.01930  7.54700E+03 0.01871  5.98388E+03 0.02006  3.88658E+03 0.01868  1.24452E+03 0.02963 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.79707E+00 0.00097 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.16286E+00 0.00122  2.83072E-01 0.00325 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.58236E-01 0.00030  4.03548E-01 0.00050 ];
INF_CAPT                  (idx, [1:   4]) = [  1.68793E-03 0.00147  1.43612E-02 0.00214 ];
INF_ABS                   (idx, [1:   4]) = [  3.82473E-03 0.00094  9.19354E-02 0.00191 ];
INF_FISS                  (idx, [1:   4]) = [  2.13680E-03 0.00053  7.75741E-02 0.00187 ];
INF_NSF                   (idx, [1:   4]) = [  5.21959E-03 0.00052  1.88981E-01 0.00187 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44271E+00 1.4E-05  2.43614E+00 9.1E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 6.0E-08  2.02270E+02 1.3E-08 ];
INF_INVV                  (idx, [1:   4]) = [  4.93889E-08 0.00092  2.63644E-06 0.00151 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.54425E-01 0.00034  3.11708E-01 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  1.96734E-02 0.00103  1.80902E-02 0.00470 ];
INF_SCATT2                (idx, [1:   4]) = [  3.71510E-03 0.03098  1.35889E-03 0.05974 ];
INF_SCATT3                (idx, [1:   4]) = [  6.93014E-04 0.07924  5.68551E-04 0.77683 ];
INF_SCATT4                (idx, [1:   4]) = [  3.53092E-05 0.69644  3.40001E-04 0.88737 ];
INF_SCATT5                (idx, [1:   4]) = [  4.81769E-05 1.00000 -3.35446E-04 0.81801 ];
INF_SCATT6                (idx, [1:   4]) = [  1.02765E-04 0.42438 -1.23159E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.70994E-05 0.59462  7.71939E-05 0.92310 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.54426E-01 0.00034  3.11708E-01 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.96735E-02 0.00103  1.80902E-02 0.00470 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.71504E-03 0.03094  1.35889E-03 0.05974 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.93144E-04 0.07920  5.68551E-04 0.77683 ];
INF_SCATTP4               (idx, [1:   4]) = [  3.53459E-05 0.69579  3.40001E-04 0.88737 ];
INF_SCATTP5               (idx, [1:   4]) = [  4.82420E-05 1.00000 -3.35446E-04 0.81801 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.02783E-04 0.42469 -1.23159E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.70367E-05 0.59346  7.71939E-05 0.92310 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.07947E-01 0.00086  3.74676E-01 0.00054 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.60297E+00 0.00086  8.89659E-01 0.00054 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  3.82385E-03 0.00089  9.19354E-02 0.00191 ];
INF_REMXS                 (idx, [1:   4]) = [  4.82333E-03 0.00320  9.23773E-02 0.00376 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.53413E-01 0.00036  1.01217E-03 0.00651  5.36448E-04 0.07614  3.11171E-01 0.00072 ];
INF_S1                    (idx, [1:   8]) = [  1.98995E-02 0.00112 -2.26038E-04 0.01019  1.61798E-04 0.11534  1.79284E-02 0.00491 ];
INF_S2                    (idx, [1:   8]) = [  3.74157E-03 0.03130 -2.64764E-05 0.09970  1.52313E-05 0.77069  1.34366E-03 0.05197 ];
INF_S3                    (idx, [1:   8]) = [  7.08385E-04 0.07901 -1.53708E-05 0.12647 -1.98492E-05 0.55650  5.88400E-04 0.76513 ];
INF_S4                    (idx, [1:   8]) = [  4.25650E-05 0.56079 -7.25580E-06 0.09986 -5.94006E-06 0.79041  3.45941E-04 0.87585 ];
INF_S5                    (idx, [1:   8]) = [  4.86262E-05 1.00000 -4.49302E-07 1.00000 -2.46455E-06 1.00000 -3.32981E-04 0.80829 ];
INF_S6                    (idx, [1:   8]) = [  1.02372E-04 0.43966  3.92922E-07 1.00000 -8.43829E-06 0.36948 -1.14720E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.65146E-05 0.59797  5.84851E-07 0.87343 -8.78341E-06 0.73844  8.59773E-05 0.77571 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.53414E-01 0.00036  1.01217E-03 0.00651  5.36448E-04 0.07614  3.11171E-01 0.00072 ];
INF_SP1                   (idx, [1:   8]) = [  1.98995E-02 0.00113 -2.26038E-04 0.01019  1.61798E-04 0.11534  1.79284E-02 0.00491 ];
INF_SP2                   (idx, [1:   8]) = [  3.74152E-03 0.03126 -2.64764E-05 0.09970  1.52313E-05 0.77069  1.34366E-03 0.05197 ];
INF_SP3                   (idx, [1:   8]) = [  7.08514E-04 0.07898 -1.53708E-05 0.12647 -1.98492E-05 0.55650  5.88400E-04 0.76513 ];
INF_SP4                   (idx, [1:   8]) = [  4.26017E-05 0.56036 -7.25580E-06 0.09986 -5.94006E-06 0.79041  3.45941E-04 0.87585 ];
INF_SP5                   (idx, [1:   8]) = [  4.86913E-05 1.00000 -4.49302E-07 1.00000 -2.46455E-06 1.00000 -3.32981E-04 0.80829 ];
INF_SP6                   (idx, [1:   8]) = [  1.02390E-04 0.43997  3.92922E-07 1.00000 -8.43829E-06 0.36948 -1.14720E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.64518E-05 0.59680  5.84851E-07 0.87343 -8.78341E-06 0.73844  8.59773E-05 0.77571 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.28765E-01 0.00501 -2.38975E-01 0.01461 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.70132E-01 0.01607 -1.37369E-01 0.01011 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.63105E-01 0.01013 -1.38520E-01 0.00304 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.69154E-01 0.00812  5.17932E-01 0.05341 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.77465E-01 0.00499 -1.39544E+00 0.01454 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.09382E-01 0.01584 -2.42705E+00 0.01003 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.19928E-01 0.01023 -2.40644E+00 0.00303 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.03085E-01 0.00806  6.47159E-01 0.05175 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.60944E-03 0.03254  1.94894E-04 0.15821  1.07212E-03 0.07055  6.43801E-04 0.10335  1.43470E-03 0.05968  1.95232E-03 0.06349  6.44129E-04 0.09850  4.77778E-04 0.10906  1.89700E-04 0.16213 ];
LAMBDA                    (idx, [1:  18]) = [  4.15735E-01 0.04609  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30615E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05902E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80614 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58714E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'g' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.88045E+04 0.00698  3.73409E+05 0.00423  7.65392E+05 0.00294  1.60472E+06 0.00191  1.82187E+06 0.00062  1.75123E+06 0.00054  1.67881E+06 0.00134  1.59148E+06 0.00194  1.51189E+06 0.00178  1.47261E+06 0.00131  1.44797E+06 0.00124  1.42133E+06 0.00023  1.40049E+06 0.00037  1.38306E+06 0.00116  1.38197E+06 0.00122  1.20697E+06 0.00159  1.20922E+06 0.00157  1.19483E+06 0.00106  1.17969E+06 0.00122  2.30573E+06 0.00112  2.22089E+06 0.00143  1.59797E+06 0.00076  1.03146E+06 0.00120  1.21362E+06 0.00083  1.16394E+06 0.00080  9.75847E+05 0.00051  1.74375E+06 0.00120  3.55288E+05 0.00062  4.34217E+05 0.00169  3.82577E+05 0.00195  2.20596E+05 0.00238  3.74661E+05 0.00130  2.50536E+05 0.00220  2.12826E+05 0.00125  4.09105E+04 0.00627  4.02582E+04 0.00376  4.12299E+04 0.00451  4.23396E+04 0.00230  4.12952E+04 0.00246  4.06828E+04 0.00146  4.21381E+04 0.00298  3.92844E+04 0.00957  7.36633E+04 0.00193  1.16915E+05 0.00259  1.46080E+05 0.00121  3.76555E+05 0.00065  3.78030E+05 0.00176  3.78667E+05 0.00122  2.28109E+05 0.00075  1.54314E+05 0.00251  1.12158E+05 0.00335  1.21137E+05 0.00039  2.02380E+05 0.00171  2.38425E+05 0.00090  4.10668E+05 0.00268  6.33329E+05 0.00184  1.16787E+06 0.00287  9.87635E+05 0.00269  8.67793E+05 0.00268  7.28236E+05 0.00385  7.42354E+05 0.00179  8.27064E+05 0.00205  7.83283E+05 0.00442  5.82041E+05 0.00299  5.87475E+05 0.00310  5.71119E+05 0.00166  5.29944E+05 0.00141  4.54415E+05 0.00280  3.24963E+05 0.00352  1.27769E+05 0.00574 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.00058E+01 0.00061  1.21406E+01 0.00181 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.59638E-01 0.00015  5.29162E-01 2.4E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.52352E-05 0.00787  2.94354E-04 0.00041 ];
INF_ABS                   (idx, [1:   4]) = [  1.52352E-05 0.00787  2.94354E-04 0.00041 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  8.71025E-08 0.00106  3.23507E-06 0.00041 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.59623E-01 0.00015  5.28865E-01 2.2E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  2.99604E-02 0.00160  2.82522E-02 0.00104 ];
INF_SCATT2                (idx, [1:   4]) = [  2.84380E-03 0.01507  1.74698E-03 0.01021 ];
INF_SCATT3                (idx, [1:   4]) = [  4.58793E-04 0.03462  2.63675E-04 0.08817 ];
INF_SCATT4                (idx, [1:   4]) = [  2.44759E-05 1.00000  7.38405E-05 0.30589 ];
INF_SCATT5                (idx, [1:   4]) = [ -1.02165E-05 1.00000  8.54848E-05 0.26295 ];
INF_SCATT6                (idx, [1:   4]) = [  1.36916E-05 0.50448  1.17959E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -2.86672E-06 1.00000  4.19868E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.59623E-01 0.00015  5.28865E-01 2.2E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.99604E-02 0.00160  2.82522E-02 0.00104 ];
INF_SCATTP2               (idx, [1:   4]) = [  2.84380E-03 0.01507  1.74698E-03 0.01021 ];
INF_SCATTP3               (idx, [1:   4]) = [  4.58793E-04 0.03462  2.63675E-04 0.08817 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.44759E-05 1.00000  7.38405E-05 0.30589 ];
INF_SCATTP5               (idx, [1:   4]) = [ -1.02165E-05 1.00000  8.54848E-05 0.26295 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.36916E-05 0.50448  1.17959E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -2.86672E-06 1.00000  4.19868E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  3.91788E-01 0.00036  4.99840E-01 3.0E-05 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  8.50800E-01 0.00036  6.66880E-01 3.0E-05 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.52352E-05 0.00787  2.94354E-04 0.00041 ];
INF_REMXS                 (idx, [1:   4]) = [  3.36039E-03 0.00071  7.07187E-04 0.00351 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.56277E-01 0.00015  3.34519E-03 0.00071  4.09872E-04 0.00859  5.28455E-01 2.7E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.08803E-02 0.00156 -9.19898E-04 0.00489  1.07564E-04 0.00988  2.81446E-02 0.00108 ];
INF_S2                    (idx, [1:   8]) = [  2.91053E-03 0.01345 -6.67262E-05 0.06893  1.78943E-06 0.20794  1.74519E-03 0.01041 ];
INF_S3                    (idx, [1:   8]) = [  4.64821E-04 0.03081 -6.02819E-06 0.33994 -1.31436E-05 0.06023  2.76819E-04 0.08647 ];
INF_S4                    (idx, [1:   8]) = [  2.86880E-05 1.00000 -4.21212E-06 0.31467 -1.12667E-05 0.06496  8.51072E-05 0.25738 ];
INF_S5                    (idx, [1:   8]) = [ -6.48157E-06 1.00000 -3.73497E-06 0.03723 -7.56089E-06 0.09323  9.30457E-05 0.23485 ];
INF_S6                    (idx, [1:   8]) = [  1.35596E-05 0.28401  1.32001E-07 1.00000 -6.40820E-06 0.20153  1.82041E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  7.11131E-07 1.00000 -3.57785E-06 0.56324 -4.03427E-06 0.16241  8.23295E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.56277E-01 0.00015  3.34519E-03 0.00071  4.09872E-04 0.00859  5.28455E-01 2.7E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.08803E-02 0.00156 -9.19898E-04 0.00489  1.07564E-04 0.00988  2.81446E-02 0.00108 ];
INF_SP2                   (idx, [1:   8]) = [  2.91053E-03 0.01345 -6.67262E-05 0.06893  1.78943E-06 0.20794  1.74519E-03 0.01041 ];
INF_SP3                   (idx, [1:   8]) = [  4.64821E-04 0.03081 -6.02819E-06 0.33994 -1.31436E-05 0.06023  2.76819E-04 0.08647 ];
INF_SP4                   (idx, [1:   8]) = [  2.86880E-05 1.00000 -4.21212E-06 0.31467 -1.12667E-05 0.06496  8.51072E-05 0.25738 ];
INF_SP5                   (idx, [1:   8]) = [ -6.48157E-06 1.00000 -3.73497E-06 0.03723 -7.56089E-06 0.09323  9.30457E-05 0.23485 ];
INF_SP6                   (idx, [1:   8]) = [  1.35596E-05 0.28401  1.32001E-07 1.00000 -6.40820E-06 0.20153  1.82041E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  7.11131E-07 1.00000 -3.57785E-06 0.56324 -4.03427E-06 0.16241  8.23295E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.00841E-01 0.00100  5.49736E-01 0.00091 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.74822E-01 0.00187  5.49065E-01 0.00736 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.74152E-01 0.00103  5.44161E-01 0.00437 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.87776E-01 0.00185  5.56216E-01 0.00471 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.65969E+00 0.00100  6.06353E-01 0.00091 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.90672E+00 0.00188  6.07159E-01 0.00742 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.91404E+00 0.00103  6.12587E-01 0.00436 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.15832E+00 0.00186  5.99314E-01 0.00469 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  14]) = 'homKiwiDop.txt' ;
WORKING_DIRECTORY         (idx, [1:  79]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/homKiwi300' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Thu Dec  7 08:05:38 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Thu Dec  7 08:11:41 2023' ;

% Run parameters:

POP                       (idx, 1)        = 50000 ;
CYCLES                    (idx, 1)        = 60 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701957938158 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 6 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   6]) = [  1.05169E+00  9.75045E-01  9.64080E-01  1.00630E+00  1.01351E+00  9.89388E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.74378E-02 0.00275  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.82562E-01 4.9E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  6.39077E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  6.39665E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.80571E+00 0.00058  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.30374E-01 1.2E-05  6.72111E-02 0.00016  2.41498E-03 0.00073  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.38027E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.35196E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  3.01485E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  1.63357E+00 0.00252  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 60 ;
SIMULATED_HISTORIES       (idx, 1)        = 3000094 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  5.00016E+04 0.00078 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30615E+01 ;
RUNNING_TIME              (idx, 1)        =  6.05902E+00 ;
INIT_TIME                 (idx, [1:   2]) = [  1.08750E-01  1.08750E-01 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  8.91667E-03  8.91667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  5.94098E+00  5.94098E+00  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  6.05827E+00  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 3.80614 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  3.91498E+00 0.01040 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.58714E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1385.93 ;
MEMSIZE                   (idx, 1)        = 1305.74 ;
XS_MEMSIZE                (idx, 1)        = 658.17 ;
MAT_MEMSIZE               (idx, 1)        = 302.02 ;
RES_MEMSIZE               (idx, 1)        = 11.31 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 334.23 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 80.19 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.00024E-05 0.00035  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.39711E-02 0.00436 ];
U235_FISS                 (idx, [1:   4]) = [  4.36893E-01 0.00067  9.99251E-01 2.4E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.27477E-04 0.03217  7.48978E-04 0.03216 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63362E-01 0.00142  5.83489E-01 0.00092 ];
U238_CAPT                 (idx, [1:   4]) = [  1.43934E-02 0.00447  5.14133E-02 0.00462 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 3000094 3.00000E+06 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 8.06774E+02 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 839560 8.39819E+05 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 1311178 1.31150E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 849356 8.49483E+05 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 3000094 3.00081E+06 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 2.46335E-07 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.41589E-11 0.00040 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.06808E+00 0.00040 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.36899E-01 0.00040 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.79906E-01 0.00043 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.16805E-01 0.00033 ];
TOT_SRCRATE               (idx, [1:   2]) = [  1.00012E+00 0.00035 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50717E+02 0.00040 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83195E-01 0.00085 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.35388E+01 0.00050 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.05806E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.47947E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.15561E-01 0.00128 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.54550E+00 0.00119 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.85848E-01 0.00029 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.12185E-01 0.00017 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.49090E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06873E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44469E+00 4.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 2.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06849E+00 0.00060  1.06096E+00 0.00060  7.77196E-03 0.00969 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06797E+00 0.00064 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06835E+00 0.00040 ];
ABS_KINF                  (idx, [1:   2]) = [  1.49059E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.33556E+01 0.00033 ];
IMP_ALF                   (idx, [1:   2]) = [  1.33568E+01 0.00026 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.16953E-05 0.00438 ];
IMP_EALF                  (idx, [1:   2]) = [  3.16505E-05 0.00347 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.68476E-02 0.00442 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.69247E-02 0.00105 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.24915E-03 0.00737  2.09739E-04 0.03772  9.29293E-04 0.02175  6.18213E-04 0.01784  1.23365E-03 0.01589  1.98742E-03 0.01225  5.79546E-04 0.02409  5.36792E-04 0.02053  1.54495E-04 0.04461 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.18851E-01 0.01102  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.8E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.39688E-03 0.01341  2.47580E-04 0.05530  1.08248E-03 0.03310  7.83117E-04 0.03521  1.51708E-03 0.02995  2.30722E-03 0.02106  6.74855E-04 0.03441  6.06208E-04 0.03788  1.78347E-04 0.06611 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.08575E-01 0.01739  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 3.3E-09  1.63478E+00 0.0E+00  3.55460E+00 6.2E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67098E-05 0.00385  3.67099E-05 0.00386  3.67316E-05 0.04441 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.92233E-05 0.00380  3.92234E-05 0.00381  3.92452E-05 0.04435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.27168E-03 0.00968  2.42919E-04 0.05297  1.04915E-03 0.02599  7.28907E-04 0.02798  1.46123E-03 0.02236  2.31609E-03 0.01744  6.84023E-04 0.03281  6.12130E-04 0.03278  1.77229E-04 0.06963 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.15263E-01 0.01512  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 4.2E-09  1.63478E+00 0.0E+00  3.55460E+00 6.5E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26682E-05 0.04091  3.26923E-05 0.04093  2.89145E-05 0.12687 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.49187E-05 0.04094  3.49445E-05 0.04096  3.09123E-05 0.12700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.40395E-03 0.06042  1.78428E-04 0.21397  9.93086E-04 0.11731  6.37121E-04 0.11407  1.15825E-03 0.08328  2.13883E-03 0.07928  6.35994E-04 0.11097  5.04587E-04 0.13386  1.57658E-04 0.24120 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.13669E-01 0.04503  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.5E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 1.9E-09  3.55460E+00 0.0E+00 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.38562E-03 0.05968  1.78853E-04 0.20071  9.76036E-04 0.10690  6.16442E-04 0.11677  1.18389E-03 0.08272  2.18145E-03 0.07941  6.12280E-04 0.11129  4.76188E-04 0.12750  1.60481E-04 0.22746 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.08996E-01 0.04504  1.24667E-02 4.2E-09  2.82917E-02 0.0E+00  4.25244E-02 0.0E+00  1.33042E-01 6.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 6.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96958E+02 0.04690 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61577E-05 0.00196 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86334E-05 0.00188 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.16448E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98142E+02 0.00472 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.22081E-07 0.00201 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.86208E-05 0.00060  1.86236E-05 0.00060  1.82279E-05 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.89143E-04 0.00218  1.89200E-04 0.00221  1.81446E-04 0.02317 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.23807E-01 0.00100  2.23634E-01 0.00100  2.52320E-01 0.01959 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.31008E+01 0.01659 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.35196E+01 0.00045  5.42562E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '_' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CAPT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_ABS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP1               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP2               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP3               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP4               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP5               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP6               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP7               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_REMXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP1                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP2                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP3                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP4                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP5                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP6                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP7                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

