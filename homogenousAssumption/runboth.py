
# ----- input
runKeff = False               # to run base cases to find keff (@ 300K)
fuelDoppler = True           # to run doppler broadened cases (300K -> 1500K)
propellantDoppler = False     # to run propellant doppler broadened case
# ----- 

import os
import subprocess
import time
import matplotlib.pyplot as plt

def extractValue(startline,path):
    """
    extract a value from serpent output file
        start (str) - what the line starts with that you want the d for
                        ex. "ANA_KEFF" or "BETA_EFF"
        path (str) - the path to the file that was run
    """
    with open(path+'_res.m','r') as file:
        for line in file:
            if line.startswith(startline):
                parts = line.split('=')
                if len(parts) > 1:
                    stringVals = parts[1].split()[1:-1]
                    keff = [float(value) for value in stringVals ]
                    return keff
                

sss2path = './../../../Serpent2/src/src/sss2'
execList = ['hetKiwi.txt','homKiwi.txt']
hetArgs = ['hetKiwi.txt','-omp','8']
homArgs = ['homKiwi.txt','-omp','8']

if fuelDoppler:
                                                                
    sss2path = './../../../Serpent2/src/src/sss2'
    #serpentFile = 'coreSupport.txt'

    import sys
    sys.path.append('../tools')  # Add the directory to the Python path
    from dopplerFeedback import dopplerFeedback

    inletPropDensity = 10 # kg/m^3
    outletPropDensity = 0.2 # kg/m^3
    #outputFile = serpentFile + '_res.m'   
    initT, dopplerT = 300, 1500
    
    het = dopplerFeedback(folderName='hetKiwi',
                        execName='hetKiwiDop.txt',
                        materialFiles=['fuelElementMaterials.txt','materials.txt'],
                        initT=initT,
                        dopplerT=dopplerT,
                        numCores=6
                        )

    basefolder, doppfolder = het.setupCases() # building two cases for runs

    # broadening the heterogenous doppler folder
    het.controlHIdentifiers(doppfolder+'/'+'fuelElementMaterials.txt','92235.03','92235.15') # 
    het.controlHIdentifiers(doppfolder+'/'+'fuelElementMaterials.txt','92238.03','92238.15')  

    hetbaseP, hetdoppP = het.startProcesses(sss2path) # start serpent for het cases

    hom = dopplerFeedback(folderName='homKiwi',
                    execName='homKiwiDop.txt',
                    materialFiles=['homogenizedFuels','homogenizedUnloaded'],
                    initT=initT,
                    dopplerT=dopplerT,
                    numCores=6
                    )


    hombasefolder, homdoppfolder = hom.setupCases()

    # broadening the homogenized doppler folder
    hom.controlHIdentifiers(homdoppfolder+'/'+'homogenizedFuels','92235.03','92235.15')
    hom.controlHIdentifiers(homdoppfolder+'/'+'homogenizedFuels','92238.03','92238.15')

    hombaseP, homdoppP = hom.startProcesses(sss2path)

    hetbaseP.wait()
    hetdoppP.wait()
    hombaseP.wait()
    homdoppP.wait()

    het.clean(casename='het')
    hom.clean(casename='hom')


    dataHet = {'base':{'keff':het.extractValue("ANA_KEFF",'het'+het.execName+str(het.initT)),
                       'beta':het.extractValue("BETA_EFF",'het'+het.execName+str(het.initT))
                    },
               'dopp':{'keff':het.extractValue("ANA_KEFF",'het'+het.execName+str(het.dopplerT)),
                       'beta':het.extractValue("BETA_EFF",'het'+het.execName+str(het.dopplerT))
                    }
              }
    dataHom = {'base':{'keff':hom.extractValue("ANA_KEFF",'hom'+hom.execName+str(hom.initT)),
                       'beta':hom.extractValue("BETA_EFF",'hom'+hom.execName+str(hom.initT))
                },
               'dopp':{'keff':hom.extractValue("ANA_KEFF",'hom'+hom.execName+str(hom.dopplerT)),
                       'beta':hom.extractValue("BETA_EFF",'hom'+hom.execName+str(hom.dopplerT))
                }
              }
    
    print('-'*20+'Heterogenous'+'-'*20)
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f} '.format(initT,dataHet['base']['keff'][0],dataHet['base']['keff'][1]))
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f}'.format(dopplerT,dataHet['dopp']['keff'][0],dataHet['dopp']['keff'][1]))
    #print('| alphaFuelDoppler = {:0.4e} +/- {:0.4e} pcm/K '.format(dataHet['alpha'][0],dataHet['alpha'][1]))
    print('Reactivity change = {:0.4e} $ '.format((dataHet['dopp']['keff'][0]-dataHet['base']['keff'][0])/dataHet['base']['beta'][0]))
    print('-'*54)

    print('-'*22+'Homogenous'+'-'*22)
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f} '.format(initT,dataHom['base']['keff'][0],dataHom['base']['keff'][1]))
    print('| Keff_{:0.0f}K fuel = {:0.6f} +/- {:0.6f}'.format(dopplerT,dataHom['dopp']['keff'][0],dataHom['dopp']['keff'][1]))
    #print('| alphaFuelDoppler = {:0.4e} +/- {:0.4e} pcm/K '.format(dataHom['alpha'][0],dataHom['alpha'][1]))
    print('Reactivity change = {:0.4e} $ '.format((dataHom['dopp']['keff'][0]-dataHom['base']['keff'][0])/dataHom['base']['beta'][0]))
    print('-'*54)



if runKeff:
    start = time.time()

    os.chdir('hetKiwi')
    with open('hetKiwi.log','w') as logHet:
        hetProc = subprocess.Popen([sss2path] + hetArgs, stdout=logHet, stderr=logHet)

    os.chdir('../homKiwi')

    with open('homKiwi.log','w') as logHom:
        homProc = subprocess.Popen([sss2path] + homArgs, stdout=logHom, stderr=logHom)

    os.chdir('..')

    hetProc.wait()
    homProc.wait()
    print('Time to solve = {:0.4f} min'.format((time.time()-start)/60))

d = {
    'het':{},
    'hom':{}
}

d['het']['keff'] = extractValue("ANA_KEFF",'hetKiwi/hetKiwi.txt')
d['hom']['keff'] = extractValue("ANA_KEFF",'homKiwi/homKiwi.txt')

print('het_keff = {:0.6f} +/- {:0.6f}'.format(d['het']['keff'][0],d['het']['keff'][1]))
print('hom_keff = {:0.6f} +/- {:0.6f}'.format(d['hom']['keff'][0],d['hom']['keff'][1]))

# ------------------------------
categories = ['Heterogeneous', 'Homogeneous']
data_values = [d['het']['keff'][0], d['hom']['keff'][0]]  # Replace with your actual data

# Create a figure and axis
fig, ax = plt.subplots()
# Plot a bar chart
bars = ax.bar(categories, data_values, yerr=[d['het']['keff'][1], d['hom']['keff'][1]], capsize=5, color=['blue', 'orange'])
# Add labels and title
ax.set_xlabel('Data Types')
ax.set_ylabel('K-effective')
ax.set_ylim(1.068,1.077)
ax.set_title('Heterogenous vs Homogeneous Keff')
# Add values on top of the bars
for bar in bars:
    yval = bar.get_height()
    ax.text(bar.get_x() + bar.get_width()/2, yval, round(yval, 6), ha='left', va='bottom')
# Show the plot
plt.show()
# ------------------------------
