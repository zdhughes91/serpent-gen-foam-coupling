#    0 |      0.128
# Material
#         ID             =        1
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.128 g/cm3
#         Temperature    =        2222
#         Density        =        1.858 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.06406889128094725 [wo]
#         U238           =        0.004822389666307854 [wo]
#         C0             =        0.9311087190527448 [wo]

#     1 |      0.148
# Material
#         ID             =        2
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.148 g/cm3
#         Temperature    =        2222
#         Density        =        1.878 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.07329073482428115 [wo]
#         U238           =        0.005516506922257717 [wo]
#         C0             =        0.9211927582534611 [wo]

#     2 |      0.170
# Material
#         ID             =        3
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.17 g/cm3
#         Temperature    =        2222
#         Density        =        1.9 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.08321052631578948 [wo]
#         U238           =        0.0062631578947368385 [wo]
#         C0             =        0.9105263157894737 [wo]

#     3 |      0.195
# Material
#         ID             =        4
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.195 g/cm3
#         Temperature    =        2222
#         Density        =        1.925 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.09420779220779221 [wo]
#         U238           =        0.007090909090909086 [wo]
#         C0             =        0.8987012987012987 [wo]

#     4 |      0.224
# Material
#         ID             =        5
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.224 g/cm3
#         Temperature    =        2222
#         Density        =        1.954 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.10661207778915047 [wo]
#         U238           =        0.008024564994882287 [wo]
#         C0             =        0.8853633572159673 [wo]

#     5 |      0.258
# Material
#         ID             =        6
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.258 g/cm3
#         Temperature    =        2222
#         Density        =        1.988 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.12069416498993966 [wo]
#         U238           =        0.009084507042253515 [wo]
#         C0             =        0.8702213279678068 [wo]

#     6 |      0.297
# Material
#         ID             =        7
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.297 g/cm3
#         Temperature    =        2222
#         Density        =        2.027 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.13626541687222496 [wo]
#         U238           =        0.010256536753823377 [wo]
#         C0             =        0.8534780463739516 [wo]

#     7 |      0.341
# Material
#         ID             =        8
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.341 g/cm3
#         Temperature    =        2222
#         Density        =        2.071 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.15312892322549493 [wo]
#         U238           =        0.011525832930951222 [wo]
#         C0             =        0.8353452438435538 [wo]

#     8 |      0.392
# Material
#         ID             =        9
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.392 g/cm3
#         Temperature    =        2222
#         Density        =        2.122 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.1718001885014138 [wo]
#         U238           =        0.012931196983977373 [wo]
#         C0             =        0.8152686145146089 [wo]

#     9 |      0.450
# Material
#         ID             =        10
#         Name           =        Fuel (UC2) e=93.000 %; rhoU = 0.45 g/cm3
#         Temperature    =        2222
#         Density        =        2.18 [g/cm3]
#         S(a,b) Tables
#         Nuclides
#         U235           =        0.1919724770642202 [wo]
#         U238           =        0.014449541284403658 [wo]
#         C0             =        0.793577981651376 [wo]
def getCircularArea(innerRadius,outerRadius):
    return (3.14159*(outerRadius**2 - innerRadius**2))

totHexArea = 3.1764 # cm using 0.95758 =r in hexSolver
propRadius = 0.122 # cm from fuelChannel.txt
cladRadius = 0.127 # ""
numChannels = 19

propFrac = numChannels * getCircularArea(0,propRadius) / totHexArea
cladFrac = numChannels * getCircularArea(propRadius,cladRadius) / totHexArea
fuelFrac = 1 - cladFrac - propFrac


import sys
sys.path.append('../tools')  # Add the directory to the Python path
import homogenize as h

u235wpct = [0.06406889128094725,0.07329073482428115,0.08321052631578948,
            0.09420779220779221,0.10661207778915047,0.12069416498993966,
            0.13626541687222496,0.15312892322549493,0.1718001885014138,
            0.1919724770642202]

u238wpct = [0.004822389666307854,0.005516506922257717,0.0062631578947368385,
            0.007090909090909086,0.008024564994882287,0.009084507042253515,
            0.010256536753823377,0.011525832930951222,0.012931196983977373,
            0.014449541284403658]

cwpct = [0.9311087190527448,0.9211927582534611,0.9105263157894737,
         0.8987012987012987,0.8853633572159673,0.8702213279678068,
         0.8534780463739516,0.8353452438435538,0.8152686145146089,
         0.793577981651376]

rho = [1.858,1.878,1.9,1.925,1.954,1.988,2.027,2.071,2.122,2.18]

uDict = {
        # fuel material
        'UC':{'volFrac':fuelFrac,
                'rho':1.858,
                'u235':{'molarMass':235.0,
                        'enrichment':0.93,
                        'id':'92235.03c'
                        },
                'u238':{'molarMass':238.0,
                        'enrichment':0.07,
                        'id':'92238.03c'
                        },
                'C':{'molarMass':12.011,
                    'enrichment':1.0,
                    'id':'6000.03c'
                        }
                },
            # propellant material
            'Prop':{'volFrac':propFrac,
                    'rho':0.0009245,
                    'H':{'molarMass':2.016,
                        'enrichment':1.0,
                        'id':'1001.03c'
                        }
                    },
            # cladding material
            'Clad':{'volFrac':cladFrac,
                    'rho':7.82,
                    'Nb':{'molarMass':92.906,
                        'enrichment':1.0,
                        'id':'41093.03c'
                        },
                    'C':{'molarMass':12.011,
                        'enrichment':1.0,
                        'id':'6000.03c'
                        }
                    },
            
            'totalVolume':415.127 # cc
            }

fuelTypesList = ['13','15','17','19','22','26','30','34','39','45']
uniNames = ['type'+num for num in fuelTypesList]
# =================================================================================================
for fuelType in range(len(rho)): # creating + printing all hmgnized fuel types
    u = h.homogenizeUni(uDict)
    u.d['UC']['rho'] = rho[fuelType]
    u.homogenizeDensity()
    u.getMaterialWeightPercent()

    # assinging Thomas' values to dict
    u.d['UC']['u235']['wPct'] = u235wpct[fuelType]
    u.d['UC']['u238']['wPct'] = u238wpct[fuelType]
    u.d['UC']['C']['wPct'] = cwpct[fuelType]

    u.d['UC']['u235']['mass'] = u.d['UC']['mass'] * u.d['UC']['u235']['wPct']
    u.d['UC']['u238']['mass'] = u.d['UC']['mass'] * u.d['UC']['u238']['wPct']
    u.d['UC']['C']['mass'] = u.d['UC']['mass'] * u.d['UC']['C']['wPct']
    #u.d['Prop']['H']['mass'] = u.d['Prop']['mass'] 
    #ud.['NbC'] 
    
    # need to initialize self.isotopes()
#     for mat in u.d: # looping thru materials
#         if not isinstance(u.d[mat],dict): continue
#         for iso in u.d[mat]: # looping thru elements/isotops
#             if not isinstance(u.d[mat][iso],dict): continue 
#             u.isotopes[iso] = {'wPct':0.0}
    # print(fuelTypesList[fuelType])
    # print('rho',u.d['UC']['rho'])
    # print('5',u.d['UC']['u235']['wPct'])
    # print('8',u.d['UC']['u238']['wPct'])
    # print('c',u.d['UC']['C']['wPct'])
    # print('nb',u.d['Clad']['Nb']['wPct'])
    # print('nb',u.d['Clad']['C']['wPct'])
    u.getUniverseWeightPercent()
    
    #u.writeOut('homogenizedFuels',uniNames[fuelType])
# =================================================================================================
# now i need to homogenize both of the unloaded elements
#  Density        =        8.19 [g/cm3]
#         S(a,b) Tables
#         S(a,b)         =        ('c_Fe56', 1.0)
#         Nuclides
#         Ni58           =        0.35278795280709696 [wo]
#         Ni60           =        0.1405738052209552 [wo]
#         Ni61           =        0.006212635197807729 [wo]
#         Ni62           =        0.02013281709330764 [wo]
#         Ni64           =        0.005292789680832517 [wo]
#         Cr50           =        0.007930004583164795 [wo]
#         Cr52           =        0.1590287884675572 [wo]
#         Cr53           =        0.01837981495548285 [wo]
#         Cr54           =        0.004661391993795167 [wo]
#         Fe54           =        0.007417134392992267 [wo]
#         Fe56           =        0.12074022851058916 [wo]
#         Fe57           =        0.002838292284645737 [wo]
#         Fe58           =        0.0003843448117728501 [wo]
#         Mo100          =        0.003043806490411952 [wo]
#         Mo92           =        0.004209567414118365 [wo]
#         Mo94           =        0.002697395514189008 [wo]
#         Mo95           =        0.0047101393167091445 [wo]
#         Mo96           =        0.00499960090540818 [wo]
#         Mo97           =        0.0029032786522591304 [wo]
#         Mo98           =        0.00743621170690422 [wo]
#         Nb93           =        0.05         [wo]
#         Ta180          =        5.971800015785824e-06 [wo]
#         Ta181          =        0.04999402819998422 [wo]
#         Co59           =        0.005        [wo]
#         Mn55           =        0.001        [wo]
#         Cu63           =        0.0006847919520523901 [wo]
#         Cu65           =        0.00031520804794761 [wo]
#         Al27           =        0.005        [wo]
#         Ti46           =        0.0007920095215776056 [wo]
#         Ti47           =        0.000729778232319522 [wo]
#         Ti48           =        0.007384505225582347 [wo]
#         Ti49           =        0.0005532190538169196 [wo]
#         Ti50           =        0.0005404879667036064 [wo]
#         Si28           =        0.0009187351728283676 [wo]
#         Si29           =        4.831750292786396e-05 [wo]
#         Si30           =        3.2947324243768426e-05 [wo]
#         C0             =        0.0005       [wo]
#         S32            =        4.738430540413793e-05 [wo]
#         S33            =        3.8494096887701767e-07 [wo]
#         S34            =        2.2225762004380115e-06 [wo]
#         S36            =        8.177426547043516e-09 [wo]
#         P31            =        5e-05        [wo]
#         B10            =        3.671099175014652e-06 [wo]
#         B11            =        1.6328900824985346e-05 [wo]

rPropel = 0.47879 # all from UnloadedCentralFuelElement.txt
rClad = 0.48387
rTieRod = 0.53467
rZrH = 0.83947
rPyroC = 0.86487
#totHexArea = 3.143
# solving volume fractions (height is same for all)
propelFrac = getCircularArea(0,rPropel) / totHexArea
cladFrac = getCircularArea(rPropel,rClad) / totHexArea
tieRodFrac = getCircularArea(rClad,rTieRod) / totHexArea
zrhFrac = getCircularArea(rTieRod,rZrH) / totHexArea
pyroFrac = getCircularArea(rZrH,rPyroC) / totHexArea
graphFrac = 1 - pyroFrac - zrhFrac - tieRodFrac - cladFrac - propelFrac

#print(graphFrac,pyroFrac,zrhFrac,tieRodFrac,cladFrac,propelFrac)

uDict = {
        # fuel material
        'Prop':{'volFrac':propelFrac,
                'rho':0.0009245,
                'H1':{'molarMass':2.016,
                        'enrichment':1.0,
                        'id':'1001.03c'
                        }
                },
        # cladding material
        'Clad':{'volFrac':cladFrac,
                'rho':7.82,
                'Nb':{'molarMass':92.906,
                'enrichment':1.0,
                'id':'41093.03c'
                },
                'C':{'molarMass':12.011,
                'enrichment':1.0,
                'id':'6000.03c'
                }
                },
        'tieRod':{'volFrac':tieRodFrac,
                  'rho':8.19,
                  'Ni58':{'molarMass':1.0,'enrichment':1.0,'id':'28058.03c'}, #wrong molar mass bc im rewriting
                  'Ni60':{'molarMass':1.0,'enrichment':1.0,'id':'28060.03c'},
                  'Ni62':{'molarMass':1.0,'enrichment':1.0,'id':'28062.03c'},
                  'Cr52':{'molarMass':1.0,'enrichment':1.0,'id':'24052.03c'},
                  'Cr53':{'molarMass':1.0,'enrichment':1.0,'id':'24053.03c'},
                  'Fe56':{'molarMass':1.0,'enrichment':1.0,'id':'23000.03c'},
                  'Nb':{'molarMass':1.0,'enrichment':1.0,'id':'41093.03c'},
                  'Ta':{'molarMass':1.0,'enrichment':1.0,'id':'73181.03c'}
                  },
        'ZrH':{'volFrac':zrhFrac,
               'rho':5.9,
               'Zr90':{'molarMass':90.0,
                     'enrichment':0.25725,
                     'id':'40090.03c'
                     },
               'Zr91':{'molarMass':91.0,
                     'enrichment':0.0561,
                     'id':'40091.03c'
                     },
                'Zr92':{'molarMass':92.0,
                     'enrichment':0.08575,
                     'id':'40092.03c'
                     },
                'Zr94':{'molarMass':94.0,
                     'enrichment':0.0869,
                     'id':'40094.03c'
                     },
                'Zr96':{'molarMass':96.0,
                     'enrichment':0.014,
                     'id':'40096.03c'
                     },
                'H1':{'molarMass':1.008,
                     'enrichment':0.49992213,
                     'id':'1001.03c'
                     },
                'H2':{'molarMass':2.014,
                     'enrichment':0.00007787,
                     'id':'1002.03c'
                     }
                },
        'pyroC':{'volFrac':pyroFrac,
                 'rho':1.41,
                'C':{'molarMass':12.011,
                     'enrichment':1.0,
                     'id':'6000.03c'
                    }
                },
        'graph':{'volFrac':graphFrac,
                 'rho':2.16,
                 'C':{'molarMass':12.011,
                      'enrichment':1.0,
                      'id':'6000.03c'
                     }
                },
            
            'totalVolume':415.127 # cc
            }
        # 'ZrH':{'volFrac':zrhFrac,
        #        'rho':5.9,
        #        'Zr90':{'molarMass':90.0,
        #              'enrichment':0.5145,
        #              'id':'40090.03c'
        #              },
        #        'Zr91':{'molarMass':91.0,
        #              'enrichment':0.1122,
        #              'id':'40091.03c'
        #              },
        #         'Zr92':{'molarMass':92.0,
        #              'enrichment':0.1715,
        #              'id':'40092.03c'
        #              },
        #         'Zr94':{'molarMass':94.0,
        #              'enrichment':0.1738,
        #              'id':'40094.03c'
        #              },
        #         'Zr96':{'molarMass':96.0,
        #              'enrichment':0.028,
        #              'id':'40096.03c'
        #              },
        #         'H1':{'molarMass':1.001,
        #              'enrichment':0.99984426,
        #              'id':'1001.03c'
        #              },
        #         'H2':{'molarMass':2.014,
        #              'enrichment':0.00015574,
        #              'id':'1002.03c'
        #              }
unloadHomoUni = h.homogenizeUni(uDict)
unloadHomoUni.homogenizeDensity()
unloadHomoUni.getMaterialWeightPercent()
def printWPcts(mat):
    print(mat)
    for iso in unloadHomoUni.d[mat]:
        if not isinstance(unloadHomoUni.d[mat][iso],dict): continue 
        print(iso,unloadHomoUni.d[mat][iso]['wPct'])

# printWPcts('Clad')
# printWPcts('ZrH')

# now you have to input w%s of all isotopes in tie rod by hand (pain)
unloadHomoUni.d['tieRod']['Ni58']['wPct'] = 0.3527879
unloadHomoUni.d['tieRod']['Ni58']['molarMass'] = 58.0
unloadHomoUni.d['tieRod']['Ni58']['id'] = '28058.03c'

unloadHomoUni.d['tieRod']['Ni60']['wPct'] = 0.14057
unloadHomoUni.d['tieRod']['Ni60']['molarMass'] = 60.0
unloadHomoUni.d['tieRod']['Ni60']['id'] = '28060.03c'

unloadHomoUni.d['tieRod']['Ni62']['wPct'] = 0.020132
unloadHomoUni.d['tieRod']['Ni62']['molarMass'] = 62.0
unloadHomoUni.d['tieRod']['Ni62']['id'] = '28062.03c'

unloadHomoUni.d['tieRod']['Cr52']['wPct'] = 0.159028
unloadHomoUni.d['tieRod']['Cr52']['molarMass'] = 52.0
unloadHomoUni.d['tieRod']['Cr52']['id'] = '24052.03c'

unloadHomoUni.d['tieRod']['Cr53']['wPct'] = 0.0183798
unloadHomoUni.d['tieRod']['Cr53']['molarMass'] = 53.0
unloadHomoUni.d['tieRod']['Cr53']['id'] = '24053.03c'

unloadHomoUni.d['tieRod']['Fe56']['wPct'] = 0.12074
unloadHomoUni.d['tieRod']['Fe56']['molarMass'] = 56.0
unloadHomoUni.d['tieRod']['Fe56']['id'] = '23000.03c'

unloadHomoUni.d['tieRod']['Nb']['wPct'] = 0.05
unloadHomoUni.d['tieRod']['Nb']['molarMass'] = 92.906
unloadHomoUni.d['tieRod']['Nb']['id'] = '41093.03c'

unloadHomoUni.d['tieRod']['Ta']['wPct'] = 0.051
unloadHomoUni.d['tieRod']['Ta']['molarMass'] = 181.0
unloadHomoUni.d['tieRod']['Ta']['id'] = '73181.03c'


# below two lines solve and write the homogenized unloaded universe
#unloadHomoUni.solve()
sum = 0
for iso in unloadHomoUni.d['tieRod']:
    if not isinstance(unloadHomoUni.d['tieRod'][iso],dict): continue
    sum += unloadHomoUni.d['tieRod'][iso]['wPct']
for iso in unloadHomoUni.d['tieRod']:
    if not isinstance(unloadHomoUni.d['tieRod'][iso],dict): continue # normalizing
    unloadHomoUni.d['tieRod'][iso]['wPct'] = unloadHomoUni.d['tieRod'][iso]['wPct'] / sum
    #print(iso+'is ',unloadHomoUni.d['tieRod'][iso]['wPct'])
for iso in unloadHomoUni.d['tieRod']:
    if not isinstance(unloadHomoUni.d['tieRod'][iso],dict): continue
    unloadHomoUni.d['tieRod'][iso]['mass'] = unloadHomoUni.d['tieRod'][iso]['wPct'] * \
                                                unloadHomoUni.d['tieRod']['mass']

# printWPcts('tieRod')
# sum = 0
# for iso in unloadHomoUni.d['ZrH']:
#     if not isinstance(unloadHomoUni.d['tieRod'][iso],dict): continue
#     sum += unloadHomoUni.d['ZrH'][iso]['enrichment']
# for iso in unloadHomoUni.d['ZrH']:
#     if not isinstance(unloadHomoUni.d['tieRod'][iso],dict): continue # normalizing
#     unloadHomoUni.d['ZrH'][iso]['wPct'] = 

#print('ZrH',unloadHomoUni.d['ZrH'])
# unloadHomoUni.getUniverseWeightPercent()
# unloadHomoUni.writeOut('homKiwi/homogenizedUnloaded','unloaded')

# up until this point , it seems to have been done correctly (keff~=1.025) --------------------------

# now updating the graphite to include Ta for the other unloadedUni
unloadHomoUni.d['graph']['Ta'] = {}
unloadHomoUni.d['graph']['Ta']['wPct'] = 0.9378
unloadHomoUni.d['graph']['Ta']['molarMass'] = 181.0
unloadHomoUni.d['graph']['Ta']['enrichment'] = 1.0
unloadHomoUni.d['graph']['Ta']['id'] = '73181.03c'

unloadHomoUni.d['graph']['C']['wPct'] = 1-0.9378

# sum = 0
# for iso in unloadHomoUni.d['graph']:
#     if not isinstance(unloadHomoUni.d['tieRod'][iso],dict): continue
#     sum += unloadHomoUni.d['graph'][iso]['wPct']
# for iso in unloadHomoUni.d['graph']:
#     if not isinstance(unloadHomoUni.d['tieRod'][iso],dict): continue # normalizing
#     unloadHomoUni.d['tieRod'][iso]['wPct'] = unloadHomoUni.d['tieRod'][iso]['wPct'] / sum
for iso in unloadHomoUni.d['graph']:
    if not isinstance(unloadHomoUni.d['graph'][iso],dict): continue
    unloadHomoUni.d['graph'][iso]['mass'] = unloadHomoUni.d['graph'][iso]['wPct'] * \
                                                unloadHomoUni.d['graph']['mass']

# printWPcts('graph')
#unloadHomoUni.solve()
# unloadHomoUni.getUniverseWeightPercent()
# unloadHomoUni.writeOut('homKiwi/homogenizedUnloaded','unloadedTa')
