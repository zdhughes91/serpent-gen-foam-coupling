
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86429E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84754E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   2]) = '40' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  9.71352E+05 0.00370  4.61819E+06 0.00067  1.04056E+07 0.00074  1.87612E+07 0.00038  1.98810E+07 0.00056  1.84949E+07 0.00040  1.68735E+07 0.00035  1.50147E+07 0.00034  1.33843E+07 0.00018  1.21151E+07 0.00021  1.11536E+07 0.00038  1.03929E+07 0.00031  9.67294E+06 0.00061  9.27740E+06 0.00036  8.92755E+06 0.00046  7.59270E+06 0.00039  7.44647E+06 0.00060  7.14416E+06 0.00060  6.82245E+06 0.00029  1.26521E+07 0.00043  1.11157E+07 0.00045  7.33261E+06 0.00050  4.43229E+06 0.00060  4.78032E+06 0.00034  4.25866E+06 0.00034  3.40347E+06 0.00073  5.70143E+06 0.00051  1.15895E+06 0.00110  1.42493E+06 0.00115  1.28375E+06 0.00036  7.29580E+05 0.00088  1.25873E+06 0.00070  8.38184E+05 0.00077  6.91540E+05 0.00090  1.27515E+05 0.00116  1.24984E+05 0.00193  1.29401E+05 0.00266  1.31845E+05 0.00222  1.30170E+05 0.00299  1.28341E+05 0.00277  1.31258E+05 0.00115  1.22678E+05 0.00305  2.30915E+05 0.00103  3.62773E+05 0.00174  4.52691E+05 0.00119  1.14605E+06 0.00104  1.12229E+06 0.00092  1.08232E+06 0.00105  6.22720E+05 0.00057  4.08120E+05 0.00076  2.90241E+05 0.00142  3.08887E+05 0.00086  5.10332E+05 0.00087  5.89039E+05 0.00121  9.95749E+05 0.00069  1.47519E+06 0.00063  2.61326E+06 0.00081  2.15929E+06 0.00092  1.87636E+06 0.00086  1.56256E+06 0.00099  1.58116E+06 0.00072  1.75619E+06 0.00080  1.65824E+06 0.00104  1.22571E+06 0.00141  1.23319E+06 0.00102  1.19973E+06 0.00109  1.11078E+06 0.00105  9.43978E+05 0.00073  6.73555E+05 0.00094  2.63693E+05 0.00118 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.50227E+00 0.00040 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36614E+02 0.00021  1.36312E+01 0.00055 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  3.37609E-01 7.4E-05  5.25498E-01 7.0E-05 ];
INF_CAPT                  (idx, [1:   4]) = [  1.78766E-03 0.00069  2.31315E-03 0.00062 ];
INF_ABS                   (idx, [1:   4]) = [  4.07515E-03 0.00059  1.16411E-02 0.00062 ];
INF_FISS                  (idx, [1:   4]) = [  2.28749E-03 0.00054  9.32793E-03 0.00062 ];
INF_NSF                   (idx, [1:   4]) = [  5.59994E-03 0.00053  2.27241E-02 0.00062 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44807E+00 2.6E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.7E-08  2.02270E+02 5.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.58309E-08 0.00016  3.11814E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  3.33532E-01 7.7E-05  5.13853E-01 8.3E-05 ];
INF_SCATT1                (idx, [1:   4]) = [  3.73513E-02 0.00018  3.13735E-02 0.00078 ];
INF_SCATT2                (idx, [1:   4]) = [  1.00475E-02 0.00060  3.41515E-03 0.00631 ];
INF_SCATT3                (idx, [1:   4]) = [  9.70877E-04 0.00919  8.42890E-04 0.01180 ];
INF_SCATT4                (idx, [1:   4]) = [ -5.95344E-04 0.00726  2.47876E-04 0.07417 ];
INF_SCATT5                (idx, [1:   4]) = [  7.35006E-05 0.04717  7.85910E-05 0.25661 ];
INF_SCATT6                (idx, [1:   4]) = [  3.99459E-04 0.01439  5.14935E-05 0.36945 ];
INF_SCATT7                (idx, [1:   4]) = [  4.02217E-05 0.08261  1.94791E-05 0.38060 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  3.33534E-01 7.7E-05  5.13853E-01 8.3E-05 ];
INF_SCATTP1               (idx, [1:   4]) = [  3.73513E-02 0.00019  3.13735E-02 0.00078 ];
INF_SCATTP2               (idx, [1:   4]) = [  1.00476E-02 0.00060  3.41515E-03 0.00631 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.70838E-04 0.00919  8.42890E-04 0.01180 ];
INF_SCATTP4               (idx, [1:   4]) = [ -5.95337E-04 0.00726  2.47876E-04 0.07417 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.35016E-05 0.04731  7.85910E-05 0.25661 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.99452E-04 0.01439  5.14935E-05 0.36945 ];
INF_SCATTP7               (idx, [1:   4]) = [  4.02429E-05 0.08235  1.94791E-05 0.38060 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.51116E-01 0.00014  4.92247E-01 0.00011 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.32741E+00 0.00014  6.77167E-01 0.00011 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.07328E-03 0.00059  1.16411E-02 0.00062 ];
INF_REMXS                 (idx, [1:   4]) = [  5.79009E-03 0.00032  1.21280E-02 0.00074 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  3.31819E-01 7.7E-05  1.71316E-03 0.00032  4.83320E-04 0.00462  5.13370E-01 8.5E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.75195E-02 0.00018 -1.68184E-04 0.00379  1.28709E-04 0.00599  3.12448E-02 0.00078 ];
INF_S2                    (idx, [1:   8]) = [  1.01263E-02 0.00059 -7.87964E-05 0.00475  6.36548E-06 0.15474  3.40878E-03 0.00620 ];
INF_S3                    (idx, [1:   8]) = [  1.06247E-03 0.00842 -9.15945E-05 0.00256 -1.31956E-05 0.05260  8.56085E-04 0.01175 ];
INF_S4                    (idx, [1:   8]) = [ -5.65645E-04 0.00755 -2.96981E-05 0.01018 -9.29681E-06 0.04993  2.57173E-04 0.07107 ];
INF_S5                    (idx, [1:   8]) = [  6.60507E-05 0.05230  7.44989E-06 0.05215 -6.41679E-06 0.05344  8.50078E-05 0.23761 ];
INF_S6                    (idx, [1:   8]) = [  3.92222E-04 0.01450  7.23651E-06 0.02578 -4.69117E-06 0.09101  5.61847E-05 0.34543 ];
INF_S7                    (idx, [1:   8]) = [  4.05194E-05 0.08583 -2.97676E-07 0.88945 -2.97364E-06 0.20605  2.24527E-05 0.33106 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  3.31821E-01 7.7E-05  1.71316E-03 0.00032  4.83320E-04 0.00462  5.13370E-01 8.5E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.75195E-02 0.00018 -1.68184E-04 0.00379  1.28709E-04 0.00599  3.12448E-02 0.00078 ];
INF_SP2                   (idx, [1:   8]) = [  1.01264E-02 0.00059 -7.87964E-05 0.00475  6.36548E-06 0.15474  3.40878E-03 0.00620 ];
INF_SP3                   (idx, [1:   8]) = [  1.06243E-03 0.00842 -9.15945E-05 0.00256 -1.31956E-05 0.05260  8.56085E-04 0.01175 ];
INF_SP4                   (idx, [1:   8]) = [ -5.65639E-04 0.00756 -2.96981E-05 0.01018 -9.29681E-06 0.04993  2.57173E-04 0.07107 ];
INF_SP5                   (idx, [1:   8]) = [  6.60517E-05 0.05245  7.44989E-06 0.05215 -6.41679E-06 0.05344  8.50078E-05 0.23761 ];
INF_SP6                   (idx, [1:   8]) = [  3.92216E-04 0.01450  7.23651E-06 0.02578 -4.69117E-06 0.09101  5.61847E-05 0.34543 ];
INF_SP7                   (idx, [1:   8]) = [  4.05406E-05 0.08557 -2.97676E-07 0.88945 -2.97364E-06 0.20605  2.24527E-05 0.33106 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.79972E-01 0.00026  7.75373E-01 0.00225 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.83898E-01 0.00059  9.89445E-01 0.00497 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.83911E-01 0.00053  9.83541E-01 0.00727 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.72425E-01 0.00034  5.43089E-01 0.00310 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.19060E+00 0.00026  4.29911E-01 0.00224 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.17413E+00 0.00059  3.36931E-01 0.00499 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.17408E+00 0.00053  3.39000E-01 0.00723 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.22358E+00 0.00034  6.13803E-01 0.00310 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
LAMBDA                    (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86429E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84754E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'F' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  5.40730E+05 0.00384  2.57875E+06 0.00081  5.90336E+06 0.00084  1.03805E+07 0.00051  1.08465E+07 0.00084  1.00204E+07 0.00093  9.06029E+06 0.00063  7.94232E+06 0.00072  6.94879E+06 0.00032  6.14515E+06 0.00061  5.52037E+06 0.00092  5.02286E+06 0.00074  4.56141E+06 0.00112  4.28403E+06 0.00098  4.04754E+06 0.00108  3.38287E+06 0.00090  3.27101E+06 0.00132  3.07580E+06 0.00125  2.86539E+06 0.00083  5.10003E+06 0.00145  4.12823E+06 0.00068  2.48552E+06 0.00081  1.38725E+06 0.00166  1.32724E+06 0.00113  1.05093E+06 0.00149  7.71822E+05 0.00162  1.13062E+06 0.00210  2.27094E+05 0.00359  2.84357E+05 0.00377  2.68369E+05 0.00302  1.48015E+05 0.00277  2.65434E+05 0.00269  1.75685E+05 0.00331  1.34738E+05 0.00263  2.19773E+04 0.00838  2.13575E+04 0.01046  2.25987E+04 0.00951  2.30034E+04 0.00658  2.31071E+04 0.00506  2.24700E+04 0.00837  2.31212E+04 0.00825  2.14875E+04 0.00710  4.06094E+04 0.00581  6.24888E+04 0.00577  7.69882E+04 0.00438  1.86682E+05 0.00118  1.69244E+05 0.00339  1.42168E+05 0.00353  6.84040E+04 0.00483  3.86835E+04 0.00617  2.46768E+04 0.00781  2.37381E+04 0.00713  3.61109E+04 0.00547  3.70735E+04 0.00532  5.33600E+04 0.00290  5.71359E+04 0.00494  6.13729E+04 0.00497  3.11078E+04 0.00959  2.03011E+04 0.00833  1.32507E+04 0.01121  1.14514E+04 0.00311  1.07365E+04 0.01204  8.70873E+03 0.01002  5.42721E+03 0.01456  4.79334E+03 0.01666  4.16104E+03 0.00850  3.35279E+03 0.01884  2.43087E+03 0.02397  1.26838E+03 0.02987  3.82389E+02 0.08572 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.61181E+00 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  6.29142E+01 0.00039  4.14667E-01 0.00099 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55589E-01 0.00017  4.66795E-01 0.00061 ];
INF_CAPT                  (idx, [1:   4]) = [  2.34232E-03 0.00032  2.36879E-02 0.00063 ];
INF_ABS                   (idx, [1:   4]) = [  6.31341E-03 0.00032  1.53267E-01 0.00053 ];
INF_FISS                  (idx, [1:   4]) = [  3.97110E-03 0.00034  1.29579E-01 0.00052 ];
INF_NSF                   (idx, [1:   4]) = [  9.72440E-03 0.00034  3.15672E-01 0.00052 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44879E+00 3.2E-06  2.43614E+00 5.8E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02275E+02 1.9E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.41622E-08 0.00060  1.58970E-06 0.00102 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49272E-01 0.00017  3.13618E-01 0.00100 ];
INF_SCATT1                (idx, [1:   4]) = [  2.05435E-02 0.00076  1.82829E-02 0.00837 ];
INF_SCATT2                (idx, [1:   4]) = [  4.48687E-03 0.00158  1.36932E-03 0.04075 ];
INF_SCATT3                (idx, [1:   4]) = [  1.00468E-03 0.00255  3.19837E-04 0.29273 ];
INF_SCATT4                (idx, [1:   4]) = [  2.35788E-04 0.00920  5.84983E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.22970E-04 0.03911  4.34139E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  9.36223E-05 0.03589  5.36684E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.87418E-05 0.31045 -6.40788E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49274E-01 0.00017  3.13618E-01 0.00100 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.05434E-02 0.00076  1.82829E-02 0.00837 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.48691E-03 0.00158  1.36932E-03 0.04075 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.00462E-03 0.00254  3.19837E-04 0.29273 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.35801E-04 0.00916  5.84983E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.22983E-04 0.03916  4.34139E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  9.36137E-05 0.03588  5.36684E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.87691E-05 0.30922 -6.40788E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02047E-01 0.00024  4.30682E-01 0.00072 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64978E+00 0.00024  7.73968E-01 0.00071 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.31065E-03 0.00032  1.53267E-01 0.00053 ];
INF_REMXS                 (idx, [1:   4]) = [  6.64977E-03 0.00030  1.54870E-01 0.00082 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48939E-01 0.00017  3.32530E-04 0.00176  1.69352E-03 0.01246  3.11925E-01 0.00100 ];
INF_S1                    (idx, [1:   8]) = [  2.06190E-02 0.00077 -7.55755E-05 0.00519  4.26805E-04 0.01500  1.78561E-02 0.00862 ];
INF_S2                    (idx, [1:   8]) = [  4.49591E-03 0.00156 -9.04140E-06 0.02961  4.61403E-06 1.00000  1.36471E-03 0.03748 ];
INF_S3                    (idx, [1:   8]) = [  1.01071E-03 0.00255 -6.02536E-06 0.03579 -5.01095E-05 0.18169  3.69947E-04 0.23718 ];
INF_S4                    (idx, [1:   8]) = [  2.37696E-04 0.00898 -1.90813E-06 0.10774 -3.94158E-05 0.23072  9.79141E-05 0.67679 ];
INF_S5                    (idx, [1:   8]) = [  1.22522E-04 0.03936  4.47353E-07 0.33380 -2.33275E-05 0.12799  6.67414E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  9.30169E-05 0.03714  6.05468E-07 0.20332 -2.18167E-05 0.23140  7.54851E-05 0.88418 ];
INF_S7                    (idx, [1:   8]) = [  1.88918E-05 0.30782 -1.50006E-07 1.00000 -1.18247E-05 0.40803  5.41683E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48942E-01 0.00017  3.32530E-04 0.00176  1.69352E-03 0.01246  3.11925E-01 0.00100 ];
INF_SP1                   (idx, [1:   8]) = [  2.06190E-02 0.00077 -7.55755E-05 0.00519  4.26805E-04 0.01500  1.78561E-02 0.00862 ];
INF_SP2                   (idx, [1:   8]) = [  4.49595E-03 0.00156 -9.04140E-06 0.02961  4.61403E-06 1.00000  1.36471E-03 0.03748 ];
INF_SP3                   (idx, [1:   8]) = [  1.01065E-03 0.00254 -6.02536E-06 0.03579 -5.01095E-05 0.18169  3.69947E-04 0.23718 ];
INF_SP4                   (idx, [1:   8]) = [  2.37709E-04 0.00893 -1.90813E-06 0.10774 -3.94158E-05 0.23072  9.79141E-05 0.67679 ];
INF_SP5                   (idx, [1:   8]) = [  1.22535E-04 0.03941  4.47353E-07 0.33380 -2.33275E-05 0.12799  6.67414E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  9.30083E-05 0.03712  6.05468E-07 0.20332 -2.18167E-05 0.23140  7.54851E-05 0.88418 ];
INF_SP7                   (idx, [1:   8]) = [  1.89191E-05 0.30661 -1.50006E-07 1.00000 -1.18247E-05 0.40803  5.41683E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  3.44583E-01 0.00040 -4.02360E+01 0.79567 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  3.98439E-01 0.00066 -7.43728E-01 0.02410 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  3.97484E-01 0.00073 -7.55071E-01 0.03819 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.71699E-01 0.00055  4.22114E-01 0.01502 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  9.67353E-01 0.00040 -3.45486E-02 0.19918 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  8.36601E-01 0.00066 -4.49495E-01 0.02408 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  8.38610E-01 0.00073 -4.44701E-01 0.03826 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.22685E+00 0.00055  7.90551E-01 0.01476 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.40288E-03 0.00548  2.51509E-04 0.03148  1.08779E-03 0.01474  7.48306E-04 0.01781  1.47458E-03 0.01439  2.33827E-03 0.01078  6.98762E-04 0.01955  6.29667E-04 0.02354  1.74009E-04 0.03922 ];
LAMBDA                    (idx, [1:  18]) = [  4.13130E-01 0.01133  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86429E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59059 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84753E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'T' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.43813E+04 0.00934  1.62028E+05 0.00535  3.73088E+05 0.00284  6.69229E+05 0.00218  7.10294E+05 0.00183  6.63198E+05 0.00178  6.00214E+05 0.00239  5.27548E+05 0.00238  4.60433E+05 0.00151  4.07109E+05 0.00102  3.65138E+05 0.00116  3.38664E+05 0.00313  3.03118E+05 0.00212  2.96082E+05 0.00280  2.75193E+05 0.00231  2.33904E+05 0.00292  2.26021E+05 0.00220  2.14094E+05 0.00203  2.01046E+05 0.00340  3.57365E+05 0.00247  2.90116E+05 0.00159  1.75108E+05 0.00324  1.00815E+05 0.00247  9.58803E+04 0.00337  7.77758E+04 0.00364  5.58846E+04 0.00330  7.90502E+04 0.00434  1.46188E+04 0.01012  1.87105E+04 0.00833  1.73391E+04 0.00741  9.68265E+03 0.02222  1.69214E+04 0.01059  1.10322E+04 0.00692  8.95224E+03 0.01130  1.57427E+03 0.03065  1.49045E+03 0.02470  1.56917E+03 0.04082  1.57300E+03 0.02161  1.52300E+03 0.01405  1.43249E+03 0.01398  1.48329E+03 0.02112  1.39962E+03 0.03468  2.80299E+03 0.03283  4.20376E+03 0.01177  5.11926E+03 0.01085  1.28064E+04 0.01310  1.14859E+04 0.01135  1.03021E+04 0.01160  5.38900E+03 0.02111  3.14434E+03 0.03057  2.06012E+03 0.01449  2.08004E+03 0.02563  3.23094E+03 0.02058  3.24334E+03 0.02197  4.89640E+03 0.02237  5.44654E+03 0.01503  6.18374E+03 0.02099  3.44365E+03 0.01768  2.47614E+03 0.02125  1.77631E+03 0.01576  1.54013E+03 0.02018  1.63667E+03 0.02226  1.33574E+03 0.03594  1.02828E+03 0.02524  1.03435E+03 0.02612  8.99247E+02 0.02377  8.63236E+02 0.03101  7.82732E+02 0.04992  5.67716E+02 0.04428  2.36164E+02 0.06152 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.21347E+00 0.00058  3.75410E-02 0.00458 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.20230E-01 0.00035  6.32049E-01 0.00192 ];
INF_CAPT                  (idx, [1:   4]) = [  8.63872E-03 0.00107  2.50753E-02 0.00377 ];
INF_ABS                   (idx, [1:   4]) = [  8.63872E-03 0.00107  2.50753E-02 0.00377 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  2.47531E-08 0.00171  1.92314E-06 0.00247 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.11594E-01 0.00037  6.06735E-01 0.00202 ];
INF_SCATT1                (idx, [1:   4]) = [  1.52138E-01 0.00043  2.17114E-01 0.00305 ];
INF_SCATT2                (idx, [1:   4]) = [  6.29268E-02 0.00058  8.75224E-02 0.00384 ];
INF_SCATT3                (idx, [1:   4]) = [  2.69676E-03 0.01882  2.58046E-02 0.01861 ];
INF_SCATT4                (idx, [1:   4]) = [ -6.69029E-03 0.00718  4.23718E-03 0.07236 ];
INF_SCATT5                (idx, [1:   4]) = [  2.16170E-04 0.19744 -1.96007E-04 0.69798 ];
INF_SCATT6                (idx, [1:   4]) = [  3.41248E-03 0.01132 -5.68998E-04 0.45073 ];
INF_SCATT7                (idx, [1:   4]) = [  2.54543E-04 0.09266  3.82134E-04 0.86183 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.11599E-01 0.00037  6.06735E-01 0.00202 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.52138E-01 0.00043  2.17114E-01 0.00305 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.29268E-02 0.00058  8.75224E-02 0.00384 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.69676E-03 0.01881  2.58046E-02 0.01861 ];
INF_SCATTP4               (idx, [1:   4]) = [ -6.69013E-03 0.00718  4.23718E-03 0.07236 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.16011E-04 0.19759 -1.96007E-04 0.69798 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.41232E-03 0.01133 -5.68998E-04 0.45073 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.54608E-04 0.09264  3.82134E-04 0.86183 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.00471E-01 0.00074  3.95765E-01 0.00446 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.66275E+00 0.00073  8.42335E-01 0.00450 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.63425E-03 0.00107  2.50753E-02 0.00377 ];
INF_REMXS                 (idx, [1:   4]) = [  1.17046E-02 0.00131  2.68687E-02 0.00922 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.08526E-01 0.00037  3.06871E-03 0.00248  1.55469E-03 0.02665  6.05181E-01 0.00206 ];
INF_S1                    (idx, [1:   8]) = [  1.51045E-01 0.00043  1.09257E-03 0.00547  3.51588E-04 0.08805  2.16763E-01 0.00313 ];
INF_S2                    (idx, [1:   8]) = [  6.33054E-02 0.00057 -3.78600E-04 0.00670  2.92197E-04 0.08943  8.72302E-02 0.00376 ];
INF_S3                    (idx, [1:   8]) = [  3.26491E-03 0.01570 -5.68150E-04 0.00382  2.54471E-04 0.03590  2.55501E-02 0.01905 ];
INF_S4                    (idx, [1:   8]) = [ -6.52385E-03 0.00738 -1.66437E-04 0.01998  1.81394E-04 0.11677  4.05579E-03 0.07378 ];
INF_S5                    (idx, [1:   8]) = [  1.52057E-04 0.27444  6.41132E-05 0.02818  1.09035E-04 0.15035 -3.05041E-04 0.44847 ];
INF_S6                    (idx, [1:   8]) = [  3.36760E-03 0.01138  4.48846E-05 0.03060  7.18321E-05 0.24577 -6.40830E-04 0.39349 ];
INF_S7                    (idx, [1:   8]) = [  2.55941E-04 0.09354 -1.39834E-06 1.00000 -1.20780E-05 1.00000  3.94212E-04 0.81095 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.08530E-01 0.00037  3.06871E-03 0.00248  1.55469E-03 0.02665  6.05181E-01 0.00206 ];
INF_SP1                   (idx, [1:   8]) = [  1.51045E-01 0.00043  1.09257E-03 0.00547  3.51588E-04 0.08805  2.16763E-01 0.00313 ];
INF_SP2                   (idx, [1:   8]) = [  6.33054E-02 0.00057 -3.78600E-04 0.00670  2.92197E-04 0.08943  8.72302E-02 0.00376 ];
INF_SP3                   (idx, [1:   8]) = [  3.26491E-03 0.01570 -5.68150E-04 0.00382  2.54471E-04 0.03590  2.55501E-02 0.01905 ];
INF_SP4                   (idx, [1:   8]) = [ -6.52369E-03 0.00738 -1.66437E-04 0.01998  1.81394E-04 0.11677  4.05579E-03 0.07378 ];
INF_SP5                   (idx, [1:   8]) = [  1.51898E-04 0.27472  6.41132E-05 0.02818  1.09035E-04 0.15035 -3.05041E-04 0.44847 ];
INF_SP6                   (idx, [1:   8]) = [  3.36744E-03 0.01139  4.48846E-05 0.03060  7.18321E-05 0.24577 -6.40830E-04 0.39349 ];
INF_SP7                   (idx, [1:   8]) = [  2.56007E-04 0.09350 -1.39834E-06 1.00000 -1.20780E-05 1.00000  3.94212E-04 0.81095 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.25399E-01 0.00126  1.54170E+00 0.08697 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.54899E-01 0.00231  4.38537E-01 1.00000 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.57652E-01 0.00324  3.02715E+01 0.63577 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.81656E-01 0.00313  5.38913E-01 0.04873 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.47887E+00 0.00126  2.22889E-01 0.06947 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.30774E+00 0.00231 -3.10191E-03 1.00000 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.29380E+00 0.00325  4.61451E-02 0.33393 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.83506E+00 0.00312  6.25622E-01 0.04673 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59061 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84753E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'C' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.85601E+04 0.00352  3.73446E+05 0.00138  8.59391E+05 0.00157  1.52426E+06 0.00099  1.60256E+06 0.00090  1.48328E+06 0.00125  1.33200E+06 0.00082  1.15882E+06 0.00075  1.01580E+06 0.00105  8.97744E+05 0.00032  8.09394E+05 0.00104  7.49646E+05 0.00177  6.79394E+05 0.00143  6.64098E+05 0.00097  6.24389E+05 0.00092  5.33748E+05 0.00171  5.22657E+05 0.00121  4.98808E+05 0.00081  4.74126E+05 0.00189  8.69677E+05 0.00158  7.44894E+05 0.00103  4.78943E+05 0.00131  2.83304E+05 0.00144  2.93522E+05 0.00214  2.49779E+05 0.00134  1.89546E+05 0.00390  2.94628E+05 0.00349  5.87439E+04 0.00454  7.21225E+04 0.00272  6.53771E+04 0.00355  3.65094E+04 0.00463  6.34833E+04 0.00279  4.21272E+04 0.00471  3.39019E+04 0.00619  6.03211E+03 0.01117  5.89427E+03 0.01896  6.30360E+03 0.01390  6.36577E+03 0.00851  6.23650E+03 0.01529  6.09105E+03 0.01002  6.27335E+03 0.00786  5.94409E+03 0.00906  1.10571E+04 0.00640  1.70634E+04 0.00587  2.08736E+04 0.00500  5.26486E+04 0.00474  4.99069E+04 0.00531  4.58301E+04 0.00240  2.46753E+04 0.01092  1.54677E+04 0.01036  1.05972E+04 0.00820  1.09268E+04 0.00444  1.76354E+04 0.00841  1.90205E+04 0.00950  2.87790E+04 0.00451  3.58109E+04 0.00560  4.86438E+04 0.00383  3.25024E+04 0.00617  2.52050E+04 0.00574  1.92368E+04 0.00755  1.82411E+04 0.00911  1.95903E+04 0.01058  1.70120E+04 0.00849  1.22439E+04 0.00881  1.19807E+04 0.01316  1.15172E+04 0.00748  1.03677E+04 0.00588  8.64853E+03 0.01145  6.10341E+03 0.01137  2.54490E+03 0.01044 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  9.90466E+00 0.00039  2.51241E-01 0.00220 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.00043E-01 0.00013  7.28763E-01 0.00112 ];
INF_CAPT                  (idx, [1:   4]) = [  1.79695E-03 0.00117  1.48581E-02 0.00255 ];
INF_ABS                   (idx, [1:   4]) = [  1.79695E-03 0.00117  1.48581E-02 0.00255 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  3.46316E-08 0.00091  2.35884E-06 0.00123 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.98245E-01 0.00014  7.13812E-01 0.00109 ];
INF_SCATT1                (idx, [1:   4]) = [  1.60416E-01 0.00038  2.11445E-01 0.00184 ];
INF_SCATT2                (idx, [1:   4]) = [  6.40014E-02 0.00031  8.14643E-02 0.00280 ];
INF_SCATT3                (idx, [1:   4]) = [  2.39094E-03 0.01733  2.70031E-02 0.00299 ];
INF_SCATT4                (idx, [1:   4]) = [ -7.26550E-03 0.00291  7.50752E-03 0.02662 ];
INF_SCATT5                (idx, [1:   4]) = [ -5.12143E-05 0.28728  1.88662E-03 0.02116 ];
INF_SCATT6                (idx, [1:   4]) = [  3.34654E-03 0.00709  7.21104E-04 0.18250 ];
INF_SCATT7                (idx, [1:   4]) = [  2.82887E-04 0.09359  9.42002E-04 0.13112 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.98248E-01 0.00014  7.13812E-01 0.00109 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.60416E-01 0.00038  2.11445E-01 0.00184 ];
INF_SCATTP2               (idx, [1:   4]) = [  6.40014E-02 0.00031  8.14643E-02 0.00280 ];
INF_SCATTP3               (idx, [1:   4]) = [  2.39089E-03 0.01734  2.70031E-02 0.00299 ];
INF_SCATTP4               (idx, [1:   4]) = [ -7.26545E-03 0.00291  7.50752E-03 0.02662 ];
INF_SCATTP5               (idx, [1:   4]) = [ -5.11874E-05 0.28694  1.88662E-03 0.02116 ];
INF_SCATTP6               (idx, [1:   4]) = [  3.34657E-03 0.00708  7.21104E-04 0.18250 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.82899E-04 0.09370  9.42002E-04 0.13112 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.70594E-01 0.00033  4.99094E-01 0.00132 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.23186E+00 0.00033  6.67883E-01 0.00132 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.79396E-03 0.00117  1.48581E-02 0.00255 ];
INF_REMXS                 (idx, [1:   4]) = [  6.93917E-03 0.00168  1.62182E-02 0.00450 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.93104E-01 0.00013  5.14103E-03 0.00196  1.26713E-03 0.02126  7.12545E-01 0.00108 ];
INF_S1                    (idx, [1:   8]) = [  1.58663E-01 0.00037  1.75287E-03 0.00277  3.15894E-04 0.02127  2.11129E-01 0.00184 ];
INF_S2                    (idx, [1:   8]) = [  6.45498E-02 0.00033 -5.48381E-04 0.00474  2.00769E-04 0.03332  8.12636E-02 0.00286 ];
INF_S3                    (idx, [1:   8]) = [  3.31092E-03 0.01226 -9.19979E-04 0.00320  1.37683E-04 0.06572  2.68654E-02 0.00292 ];
INF_S4                    (idx, [1:   8]) = [ -6.95223E-03 0.00315 -3.13264E-04 0.00664  1.02687E-04 0.04661  7.40483E-03 0.02657 ];
INF_S5                    (idx, [1:   8]) = [ -1.31810E-04 0.11347  8.05962E-05 0.01485  5.40028E-05 0.14242  1.83262E-03 0.01916 ];
INF_S6                    (idx, [1:   8]) = [  3.26482E-03 0.00725  8.17131E-05 0.02401  2.39343E-05 0.23485  6.97170E-04 0.18921 ];
INF_S7                    (idx, [1:   8]) = [  2.78314E-04 0.09071  4.57355E-06 0.38440  9.78947E-06 0.72852  9.32213E-04 0.13195 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.93107E-01 0.00013  5.14103E-03 0.00196  1.26713E-03 0.02126  7.12545E-01 0.00108 ];
INF_SP1                   (idx, [1:   8]) = [  1.58663E-01 0.00037  1.75287E-03 0.00277  3.15894E-04 0.02127  2.11129E-01 0.00184 ];
INF_SP2                   (idx, [1:   8]) = [  6.45498E-02 0.00033 -5.48381E-04 0.00474  2.00769E-04 0.03332  8.12636E-02 0.00286 ];
INF_SP3                   (idx, [1:   8]) = [  3.31087E-03 0.01227 -9.19979E-04 0.00320  1.37683E-04 0.06572  2.68654E-02 0.00292 ];
INF_SP4                   (idx, [1:   8]) = [ -6.95218E-03 0.00315 -3.13264E-04 0.00664  1.02687E-04 0.04661  7.40483E-03 0.02657 ];
INF_SP5                   (idx, [1:   8]) = [ -1.31784E-04 0.11331  8.05962E-05 0.01485  5.40028E-05 0.14242  1.83262E-03 0.01916 ];
INF_SP6                   (idx, [1:   8]) = [  3.26486E-03 0.00724  8.17131E-05 0.02401  2.39343E-05 0.23485  6.97170E-04 0.18921 ];
INF_SP7                   (idx, [1:   8]) = [  2.78326E-04 0.09082  4.57355E-06 0.38440  9.78947E-06 0.72852  9.32213E-04 0.13195 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.20049E-01 0.00176 -2.75125E-01 0.00793 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.60588E-01 0.00166 -1.49279E-01 0.00741 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.61298E-01 0.00173 -1.49507E-01 0.00846 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  1.67540E-01 0.00232  4.03506E-01 0.02160 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.51484E+00 0.00176 -1.21195E+00 0.00800 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.27918E+00 0.00166 -2.23357E+00 0.00745 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.27570E+00 0.00174 -2.23034E+00 0.00834 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.98963E+00 0.00232  8.28050E-01 0.02191 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84753E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '9' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.63148E+04 0.00926  1.26825E+05 0.00441  2.91969E+05 0.00284  4.96706E+05 0.00244  5.09440E+05 0.00145  4.66778E+05 0.00219  4.15616E+05 0.00282  3.60516E+05 0.00163  3.13023E+05 0.00284  2.75778E+05 0.00238  2.47918E+05 0.00277  2.25273E+05 0.00146  2.07177E+05 0.00206  1.94861E+05 0.00113  1.86703E+05 0.00334  1.57214E+05 0.00233  1.54127E+05 0.00202  1.46180E+05 0.00277  1.38269E+05 0.00281  2.53119E+05 0.00327  2.15968E+05 0.00416  1.36957E+05 0.00373  7.98882E+04 0.00375  8.09899E+04 0.00463  6.83022E+04 0.00558  5.24401E+04 0.00412  8.35518E+04 0.00612  1.75179E+04 0.00431  2.21500E+04 0.00898  2.01737E+04 0.00351  1.15585E+04 0.01362  1.99387E+04 0.00791  1.35485E+04 0.01442  1.06369E+04 0.01336  1.80379E+03 0.02566  1.84941E+03 0.02517  1.86902E+03 0.01700  1.79331E+03 0.01911  1.76764E+03 0.04510  1.81955E+03 0.03033  1.86338E+03 0.03087  1.72732E+03 0.01754  3.24488E+03 0.00880  5.08167E+03 0.02331  6.33376E+03 0.02077  1.57037E+04 0.01753  1.47481E+04 0.00865  1.31575E+04 0.01168  6.69732E+03 0.01867  3.89182E+03 0.01291  2.59255E+03 0.01103  2.51266E+03 0.01219  3.93147E+03 0.01448  4.22613E+03 0.01679  6.57444E+03 0.00519  7.57586E+03 0.01462  9.37996E+03 0.01610  5.62906E+03 0.01801  3.93832E+03 0.01894  2.78111E+03 0.00888  2.45399E+03 0.02074  2.33899E+03 0.02105  1.84402E+03 0.01645  1.20860E+03 0.03422  1.03741E+03 0.03118  8.73381E+02 0.01707  7.17221E+02 0.03904  4.80096E+02 0.05265  3.19424E+02 0.05581  6.91509E+01 0.13858 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.67118E+00 0.00197 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.03612E+00 0.00070  4.94890E-02 0.00531 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56057E-01 0.00034  4.73727E-01 0.00288 ];
INF_CAPT                  (idx, [1:   4]) = [  2.49725E-03 0.00129  2.45608E-02 0.00293 ];
INF_ABS                   (idx, [1:   4]) = [  6.59721E-03 0.00100  1.59577E-01 0.00291 ];
INF_FISS                  (idx, [1:   4]) = [  4.09996E-03 0.00091  1.35016E-01 0.00292 ];
INF_NSF                   (idx, [1:   4]) = [  1.00318E-02 0.00091  3.28917E-01 0.00292 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44681E+00 5.5E-06  2.43614E+00 8.2E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 1.3E-08  2.02270E+02 8.2E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.32710E-08 0.00285  1.84443E-06 0.00200 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49437E-01 0.00037  3.14583E-01 0.00347 ];
INF_SCATT1                (idx, [1:   4]) = [  2.02857E-02 0.00254  1.82342E-02 0.03084 ];
INF_SCATT2                (idx, [1:   4]) = [  4.42470E-03 0.00766  2.03349E-03 0.25039 ];
INF_SCATT3                (idx, [1:   4]) = [  1.01655E-03 0.03642 -6.67795E-05 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  2.48650E-04 0.08259  9.09103E-06 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.19732E-04 0.15738  3.51882E-04 0.81187 ];
INF_SCATT6                (idx, [1:   4]) = [  9.33046E-05 0.16341  1.93449E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  3.81805E-05 0.35645  2.09481E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49440E-01 0.00037  3.14583E-01 0.00347 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.02858E-02 0.00254  1.82342E-02 0.03084 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.42464E-03 0.00767  2.03349E-03 0.25039 ];
INF_SCATTP3               (idx, [1:   4]) = [  1.01640E-03 0.03637 -6.67795E-05 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.48499E-04 0.08234  9.09103E-06 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.19702E-04 0.15747  3.51882E-04 0.81187 ];
INF_SCATTP6               (idx, [1:   4]) = [  9.34345E-05 0.16341  1.93449E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.82509E-05 0.35648  2.09481E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02269E-01 0.00077  4.35144E-01 0.00224 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64797E+00 0.00077  7.66049E-01 0.00226 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.59441E-03 0.00100  1.59577E-01 0.00291 ];
INF_REMXS                 (idx, [1:   4]) = [  7.19478E-03 0.00215  1.60436E-01 0.00524 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48862E-01 0.00037  5.74864E-04 0.00415  1.29300E-03 0.04743  3.13290E-01 0.00337 ];
INF_S1                    (idx, [1:   8]) = [  2.04189E-02 0.00255 -1.33273E-04 0.01306  3.47623E-04 0.08717  1.78866E-02 0.02998 ];
INF_S2                    (idx, [1:   8]) = [  4.43722E-03 0.00781 -1.25196E-05 0.08741  1.55780E-05 0.49811  2.01791E-03 0.25170 ];
INF_S3                    (idx, [1:   8]) = [  1.02740E-03 0.03618 -1.08553E-05 0.07382 -3.35635E-05 0.58216 -3.32160E-05 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  2.51264E-04 0.08293 -2.61458E-06 0.55200 -3.50877E-05 0.24099  4.41787E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.19333E-04 0.15466  3.98612E-07 1.00000 -2.12671E-05 0.42631  3.73149E-04 0.76144 ];
INF_S6                    (idx, [1:   8]) = [  9.41925E-05 0.15179 -8.87955E-07 1.00000 -1.73818E-05 0.70893  3.67267E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  3.72405E-05 0.37637  9.39971E-07 1.00000 -2.34659E-05 0.55139  4.44140E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48865E-01 0.00037  5.74864E-04 0.00415  1.29300E-03 0.04743  3.13290E-01 0.00337 ];
INF_SP1                   (idx, [1:   8]) = [  2.04191E-02 0.00254 -1.33273E-04 0.01306  3.47623E-04 0.08717  1.78866E-02 0.02998 ];
INF_SP2                   (idx, [1:   8]) = [  4.43716E-03 0.00783 -1.25196E-05 0.08741  1.55780E-05 0.49811  2.01791E-03 0.25170 ];
INF_SP3                   (idx, [1:   8]) = [  1.02726E-03 0.03613 -1.08553E-05 0.07382 -3.35635E-05 0.58216 -3.32160E-05 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  2.51114E-04 0.08267 -2.61458E-06 0.55200 -3.50877E-05 0.24099  4.41787E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.19303E-04 0.15478  3.98612E-07 1.00000 -2.12671E-05 0.42631  3.73149E-04 0.76144 ];
INF_SP6                   (idx, [1:   8]) = [  9.43224E-05 0.15178 -8.87955E-07 1.00000 -1.73818E-05 0.70893  3.67267E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  3.73109E-05 0.37619  9.39971E-07 1.00000 -2.34659E-05 0.55139  4.44140E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.18189E-01 0.00276 -2.14149E-01 0.00260 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.20271E-01 0.00426 -1.20437E-01 0.01691 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.21503E-01 0.00525 -1.18120E-01 0.01564 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.99955E-01 0.00319  3.60714E-01 0.02700 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.97118E-01 0.00276 -1.55660E+00 0.00260 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.40750E-01 0.00427 -2.77161E+00 0.01667 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.39267E-01 0.00532 -2.82536E+00 0.01533 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.11134E+00 0.00318  9.27184E-01 0.02470 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.32859E-03 0.01530  2.63477E-04 0.09206  1.08332E-03 0.03447  6.79762E-04 0.04636  1.55134E-03 0.03212  2.20036E-03 0.02799  7.17113E-04 0.05170  6.33984E-04 0.05486  1.99238E-04 0.09637 ];
LAMBDA                    (idx, [1:  18]) = [  4.27759E-01 0.02658  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84753E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '8' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.63963E+04 0.00457  1.70734E+05 0.00398  3.92750E+05 0.00297  6.70458E+05 0.00098  6.87649E+05 0.00237  6.27799E+05 0.00101  5.58159E+05 0.00270  4.83710E+05 0.00141  4.21613E+05 0.00076  3.72639E+05 0.00287  3.35969E+05 0.00189  3.07886E+05 0.00090  2.81658E+05 0.00266  2.68344E+05 0.00293  2.55635E+05 0.00121  2.15361E+05 0.00287  2.11553E+05 0.00300  2.01113E+05 0.00347  1.91325E+05 0.00182  3.51053E+05 0.00244  3.02703E+05 0.00163  1.95984E+05 0.00570  1.15911E+05 0.00129  1.18876E+05 0.00354  1.01031E+05 0.00294  7.92178E+04 0.00350  1.25827E+05 0.00427  2.65625E+04 0.00528  3.27008E+04 0.00902  3.02373E+04 0.01009  1.69697E+04 0.00532  3.01138E+04 0.00602  1.96894E+04 0.00592  1.58261E+04 0.00537  2.73128E+03 0.01653  2.68246E+03 0.01683  2.79552E+03 0.01868  2.84168E+03 0.01263  2.88863E+03 0.02951  2.87328E+03 0.01062  2.92352E+03 0.01718  2.70741E+03 0.01962  5.03069E+03 0.01797  8.06592E+03 0.00886  9.79915E+03 0.01048  2.42485E+04 0.00905  2.28927E+04 0.00589  2.05693E+04 0.00605  1.04484E+04 0.01339  6.44050E+03 0.01047  4.15908E+03 0.02181  4.30107E+03 0.01488  6.57686E+03 0.01404  7.14323E+03 0.01231  1.12546E+04 0.01248  1.36671E+04 0.00926  1.77147E+04 0.00361  1.10770E+04 0.00810  7.82389E+03 0.01057  5.65876E+03 0.00570  5.02399E+03 0.01517  5.06799E+03 0.01597  4.17404E+03 0.01526  2.68198E+03 0.02517  2.49623E+03 0.02258  2.07510E+03 0.03958  1.64910E+03 0.02438  1.17366E+03 0.00938  5.96519E+02 0.03467  1.59239E+02 0.08758 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.69833E+00 0.00159 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.16149E+00 0.00107  8.74118E-02 0.00161 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55715E-01 0.00025  4.62160E-01 0.00136 ];
INF_CAPT                  (idx, [1:   4]) = [  2.36182E-03 0.00130  2.30301E-02 0.00215 ];
INF_ABS                   (idx, [1:   4]) = [  6.11925E-03 0.00097  1.49880E-01 0.00228 ];
INF_FISS                  (idx, [1:   4]) = [  3.75743E-03 0.00086  1.26850E-01 0.00231 ];
INF_NSF                   (idx, [1:   4]) = [  9.19108E-03 0.00085  3.09025E-01 0.00231 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44611E+00 6.6E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 2.9E-08  2.02270E+02 8.2E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.58728E-08 0.00097  1.96466E-06 0.00213 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49622E-01 0.00024  3.12526E-01 0.00213 ];
INF_SCATT1                (idx, [1:   4]) = [  2.01247E-02 0.00168  1.82528E-02 0.01310 ];
INF_SCATT2                (idx, [1:   4]) = [  4.30045E-03 0.00541  1.37788E-03 0.14674 ];
INF_SCATT3                (idx, [1:   4]) = [  9.30856E-04 0.01610  2.32218E-04 0.73843 ];
INF_SCATT4                (idx, [1:   4]) = [  1.55899E-04 0.15937 -1.64178E-04 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.10587E-04 0.15509 -3.26873E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  7.89106E-05 0.36790  1.14802E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  1.75499E-05 0.78930  1.94112E-04 0.92672 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49625E-01 0.00024  3.12526E-01 0.00213 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.01248E-02 0.00168  1.82528E-02 0.01310 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.30039E-03 0.00541  1.37788E-03 0.14674 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.30685E-04 0.01605  2.32218E-04 0.73843 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.55855E-04 0.15975 -1.64178E-04 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.10682E-04 0.15486 -3.26873E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  7.88711E-05 0.36762  1.14802E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  1.76076E-05 0.78894  1.94112E-04 0.92672 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02469E-01 0.00048  4.23873E-01 0.00191 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64634E+00 0.00048  7.86413E-01 0.00191 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  6.11714E-03 0.00096  1.49880E-01 0.00228 ];
INF_REMXS                 (idx, [1:   4]) = [  6.73506E-03 0.00202  1.50670E-01 0.00187 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.48980E-01 0.00023  6.42131E-04 0.00806  1.03631E-03 0.02451  3.11490E-01 0.00211 ];
INF_S1                    (idx, [1:   8]) = [  2.02692E-02 0.00170 -1.44449E-04 0.01445  2.82515E-04 0.03040  1.79703E-02 0.01359 ];
INF_S2                    (idx, [1:   8]) = [  4.31863E-03 0.00552 -1.81811E-05 0.10273  1.58385E-05 0.50428  1.36204E-03 0.14530 ];
INF_S3                    (idx, [1:   8]) = [  9.41995E-04 0.01653 -1.11385E-05 0.09608 -2.87290E-05 0.41932  2.60947E-04 0.67830 ];
INF_S4                    (idx, [1:   8]) = [  1.59357E-04 0.15771 -3.45855E-06 0.28330 -1.63365E-05 0.83345 -1.47842E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.09313E-04 0.14336  1.27320E-06 1.00000 -1.35121E-05 0.56210 -1.91752E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  7.73118E-05 0.36996  1.59879E-06 0.55950 -7.48060E-06 1.00000  1.89608E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  1.74671E-05 0.76750  8.28325E-08 1.00000 -7.00307E-06 0.65880  2.01115E-04 0.88483 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.48982E-01 0.00023  6.42131E-04 0.00806  1.03631E-03 0.02451  3.11490E-01 0.00211 ];
INF_SP1                   (idx, [1:   8]) = [  2.02692E-02 0.00170 -1.44449E-04 0.01445  2.82515E-04 0.03040  1.79703E-02 0.01359 ];
INF_SP2                   (idx, [1:   8]) = [  4.31857E-03 0.00552 -1.81811E-05 0.10273  1.58385E-05 0.50428  1.36204E-03 0.14530 ];
INF_SP3                   (idx, [1:   8]) = [  9.41823E-04 0.01648 -1.11385E-05 0.09608 -2.87290E-05 0.41932  2.60947E-04 0.67830 ];
INF_SP4                   (idx, [1:   8]) = [  1.59313E-04 0.15809 -3.45855E-06 0.28330 -1.63365E-05 0.83345 -1.47842E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.09408E-04 0.14317  1.27320E-06 1.00000 -1.35121E-05 0.56210 -1.91752E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  7.72723E-05 0.36967  1.59879E-06 0.55950 -7.48060E-06 1.00000  1.89608E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  1.75248E-05 0.76728  8.28325E-08 1.00000 -7.00307E-06 0.65880  2.01115E-04 0.88483 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.24803E-01 0.00275 -1.90123E-01 0.01069 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.28584E-01 0.00366 -1.07555E-01 0.01212 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.31134E-01 0.00516 -1.07486E-01 0.00530 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.04213E-01 0.00350  3.55458E-01 0.02400 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.84707E-01 0.00273 -1.75426E+00 0.01074 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.30658E-01 0.00363 -3.10148E+00 0.01210 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.27672E-01 0.00515 -3.10163E+00 0.00531 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.09579E+00 0.00351  9.40326E-01 0.02279 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.12303E-03 0.01222  2.59995E-04 0.07314  1.00432E-03 0.03620  7.10766E-04 0.04240  1.40940E-03 0.03088  2.31364E-03 0.02353  6.68173E-04 0.04016  5.94274E-04 0.04381  1.62465E-04 0.09775 ];
LAMBDA                    (idx, [1:  18]) = [  4.09135E-01 0.02165  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 7.0E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84752E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '7' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.17437E+04 0.00793  1.05443E+05 0.00293  2.42031E+05 0.00191  4.10990E+05 0.00234  4.21138E+05 0.00183  3.84644E+05 0.00221  3.43020E+05 0.00241  2.98259E+05 0.00185  2.59964E+05 0.00157  2.29114E+05 0.00279  2.07703E+05 0.00400  1.91577E+05 0.00458  1.76334E+05 0.00373  1.68435E+05 0.00115  1.60881E+05 0.00461  1.36350E+05 0.00396  1.32513E+05 0.00254  1.26489E+05 0.00347  1.21905E+05 0.00479  2.24271E+05 0.00286  1.97032E+05 0.00178  1.28323E+05 0.00326  7.66432E+04 0.00496  8.03091E+04 0.00411  6.90641E+04 0.00699  5.47002E+04 0.00339  8.70517E+04 0.00509  1.85123E+04 0.00730  2.27639E+04 0.01160  2.04924E+04 0.00640  1.17179E+04 0.00575  2.06973E+04 0.00272  1.35555E+04 0.01459  1.07802E+04 0.00946  1.91408E+03 0.01543  1.96739E+03 0.03249  1.98858E+03 0.02649  1.94599E+03 0.02455  1.98681E+03 0.02028  2.00111E+03 0.02501  1.99654E+03 0.02200  1.79830E+03 0.03429  3.55459E+03 0.02445  5.38508E+03 0.01634  6.85095E+03 0.02222  1.71274E+04 0.00983  1.64258E+04 0.00934  1.48033E+04 0.00940  7.97634E+03 0.00554  4.78474E+03 0.01684  3.17672E+03 0.02981  3.27517E+03 0.03048  5.21665E+03 0.01135  5.76647E+03 0.01137  8.95998E+03 0.01136  1.12372E+04 0.00336  1.53562E+04 0.00486  9.85218E+03 0.00978  7.34743E+03 0.00776  5.51323E+03 0.01349  5.12964E+03 0.02050  5.03709E+03 0.00603  4.30528E+03 0.01946  2.83831E+03 0.02342  2.58199E+03 0.01075  2.37265E+03 0.03431  1.88363E+03 0.02998  1.38019E+03 0.04569  7.75360E+02 0.01243  1.78801E+02 0.06643 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.71732E+00 0.00134 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.61146E+00 0.00076  7.30866E-02 0.00174 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55746E-01 0.00058  4.54417E-01 0.00173 ];
INF_CAPT                  (idx, [1:   4]) = [  2.26669E-03 0.00149  2.18492E-02 0.00263 ];
INF_ABS                   (idx, [1:   4]) = [  5.74529E-03 0.00138  1.42221E-01 0.00258 ];
INF_FISS                  (idx, [1:   4]) = [  3.47860E-03 0.00136  1.20371E-01 0.00257 ];
INF_NSF                   (idx, [1:   4]) = [  8.50622E-03 0.00135  2.93241E-01 0.00257 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44530E+00 8.8E-06  2.43614E+00 5.8E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 4.5E-08  2.02270E+02 8.2E-09 ];
INF_INVV                  (idx, [1:   4]) = [  3.87852E-08 0.00196  2.11562E-06 0.00220 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.49995E-01 0.00055  3.12114E-01 0.00133 ];
INF_SCATT1                (idx, [1:   4]) = [  2.00052E-02 0.00120  1.79293E-02 0.01925 ];
INF_SCATT2                (idx, [1:   4]) = [  4.22283E-03 0.01014  1.74481E-03 0.13538 ];
INF_SCATT3                (idx, [1:   4]) = [  9.58039E-04 0.03092  4.93164E-04 0.38702 ];
INF_SCATT4                (idx, [1:   4]) = [  1.44050E-04 0.30913  7.82574E-05 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  1.02207E-04 0.20472  1.13542E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.12150E-04 0.21460  2.24658E-04 0.75182 ];
INF_SCATT7                (idx, [1:   4]) = [  3.22981E-05 0.36148 -2.02498E-04 0.77240 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.49997E-01 0.00055  3.12114E-01 0.00133 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.00052E-02 0.00120  1.79293E-02 0.01925 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.22272E-03 0.01015  1.74481E-03 0.13538 ];
INF_SCATTP3               (idx, [1:   4]) = [  9.58103E-04 0.03084  4.93164E-04 0.38702 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.44061E-04 0.30906  7.82574E-05 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.02115E-04 0.20494  1.13542E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.12082E-04 0.21496  2.24658E-04 0.75182 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.23577E-05 0.36221 -2.02498E-04 0.77240 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.02969E-01 0.00103  4.17948E-01 0.00190 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.64230E+00 0.00103  7.97563E-01 0.00190 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.74357E-03 0.00137  1.42221E-01 0.00258 ];
INF_REMXS                 (idx, [1:   4]) = [  6.46975E-03 0.00215  1.43200E-01 0.00341 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49276E-01 0.00056  7.18442E-04 0.00826  8.96483E-04 0.03199  3.11217E-01 0.00129 ];
INF_S1                    (idx, [1:   8]) = [  2.01703E-02 0.00120 -1.65144E-04 0.01491  1.95337E-04 0.12328  1.77339E-02 0.01866 ];
INF_S2                    (idx, [1:   8]) = [  4.24057E-03 0.01008 -1.77398E-05 0.03681  7.67274E-07 1.00000  1.74404E-03 0.12589 ];
INF_S3                    (idx, [1:   8]) = [  9.67980E-04 0.03050 -9.94137E-06 0.17881 -1.71125E-05 0.71237  5.10277E-04 0.38853 ];
INF_S4                    (idx, [1:   8]) = [  1.49131E-04 0.30218 -5.08152E-06 0.33839 -3.58615E-05 0.15874  1.14119E-04 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  1.00847E-04 0.20920  1.36011E-06 1.00000 -3.24706E-05 0.13881  4.38248E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.11488E-04 0.22111  6.62313E-07 1.00000 -2.45101E-05 0.41515  2.49168E-04 0.68609 ];
INF_S7                    (idx, [1:   8]) = [  3.43729E-05 0.33375 -2.07488E-06 0.32494 -1.80242E-06 1.00000 -2.00696E-04 0.79871 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49278E-01 0.00056  7.18442E-04 0.00826  8.96483E-04 0.03199  3.11217E-01 0.00129 ];
INF_SP1                   (idx, [1:   8]) = [  2.01703E-02 0.00120 -1.65144E-04 0.01491  1.95337E-04 0.12328  1.77339E-02 0.01866 ];
INF_SP2                   (idx, [1:   8]) = [  4.24046E-03 0.01008 -1.77398E-05 0.03681  7.67274E-07 1.00000  1.74404E-03 0.12589 ];
INF_SP3                   (idx, [1:   8]) = [  9.68044E-04 0.03042 -9.94137E-06 0.17881 -1.71125E-05 0.71237  5.10277E-04 0.38853 ];
INF_SP4                   (idx, [1:   8]) = [  1.49142E-04 0.30212 -5.08152E-06 0.33839 -3.58615E-05 0.15874  1.14119E-04 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  1.00754E-04 0.20940  1.36011E-06 1.00000 -3.24706E-05 0.13881  4.38248E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.11419E-04 0.22148  6.62313E-07 1.00000 -2.45101E-05 0.41515  2.49168E-04 0.68609 ];
INF_SP7                   (idx, [1:   8]) = [  3.44325E-05 0.33450 -2.07488E-06 0.32494 -1.80242E-06 1.00000 -2.00696E-04 0.79871 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.27901E-01 0.00216 -1.82411E-01 0.00628 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.18054E-01 0.00380 -1.04210E-01 0.00727 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.25241E-01 0.00445 -1.03276E-01 0.00966 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.14815E-01 0.00302  3.56861E-01 0.04812 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.79014E-01 0.00217 -1.82774E+00 0.00631 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.43480E-01 0.00380 -3.19951E+00 0.00714 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.34692E-01 0.00446 -3.22908E+00 0.00955 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.05887E+00 0.00302  9.45369E-01 0.04975 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.33415E-03 0.01673  2.62195E-04 0.07853  1.09295E-03 0.04647  7.18528E-04 0.05307  1.46711E-03 0.03537  2.30219E-03 0.02814  6.58876E-04 0.04910  6.48534E-04 0.05440  1.83775E-04 0.12013 ];
LAMBDA                    (idx, [1:  18]) = [  4.18956E-01 0.02551  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84752E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '6' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.18322E+04 0.01701  5.63421E+04 0.00579  1.29869E+05 0.00369  2.21938E+05 0.00328  2.27488E+05 0.00214  2.08725E+05 0.00193  1.85875E+05 0.00383  1.62494E+05 0.00401  1.41190E+05 0.00364  1.25891E+05 0.00284  1.13852E+05 0.00121  1.04857E+05 0.00661  9.71051E+04 0.00504  9.17757E+04 0.00230  8.80568E+04 0.00234  7.47855E+04 0.00341  7.24780E+04 0.00409  7.02284E+04 0.00523  6.70314E+04 0.00560  1.23780E+05 0.00223  1.09536E+05 0.00311  7.17221E+04 0.00375  4.28876E+04 0.00283  4.48116E+04 0.00158  3.92479E+04 0.00672  3.07352E+04 0.00555  5.03588E+04 0.00568  1.03385E+04 0.00907  1.28534E+04 0.00829  1.17223E+04 0.01272  6.57469E+03 0.02512  1.16440E+04 0.01684  7.87630E+03 0.00936  6.24403E+03 0.00950  1.06942E+03 0.03720  1.04357E+03 0.01715  1.15474E+03 0.03728  1.18049E+03 0.01928  1.20646E+03 0.03964  1.20310E+03 0.03385  1.17974E+03 0.02435  1.09355E+03 0.02924  2.00008E+03 0.01757  3.17745E+03 0.02152  3.75512E+03 0.01913  9.91665E+03 0.01092  9.26350E+03 0.01448  8.51738E+03 0.01149  4.58371E+03 0.01348  2.81426E+03 0.02970  1.91496E+03 0.01268  2.05059E+03 0.02091  3.06631E+03 0.02895  3.54615E+03 0.02860  5.38665E+03 0.01849  6.89533E+03 0.01595  9.80261E+03 0.00938  6.73884E+03 0.00779  5.04247E+03 0.01858  3.82363E+03 0.02376  3.60450E+03 0.02084  3.63827E+03 0.00761  3.16185E+03 0.02450  2.05072E+03 0.01966  1.95500E+03 0.01293  1.67762E+03 0.01981  1.44038E+03 0.02132  1.04591E+03 0.02255  5.99207E+02 0.04246  1.78306E+02 0.09500 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.73305E+00 0.00359 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.43005E+00 0.00124  4.63988E-02 0.00400 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55240E-01 0.00033  4.41208E-01 0.00452 ];
INF_CAPT                  (idx, [1:   4]) = [  2.08880E-03 0.00230  2.00553E-02 0.00454 ];
INF_ABS                   (idx, [1:   4]) = [  5.19753E-03 0.00152  1.30301E-01 0.00440 ];
INF_FISS                  (idx, [1:   4]) = [  3.10873E-03 0.00149  1.10246E-01 0.00438 ];
INF_NSF                   (idx, [1:   4]) = [  7.60062E-03 0.00149  2.68573E-01 0.00438 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44493E+00 7.0E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02274E+02 2.0E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.01426E-08 0.00107  2.22866E-06 0.00259 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50054E-01 0.00027  3.10830E-01 0.00401 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97613E-02 0.00409  1.86624E-02 0.02160 ];
INF_SCATT2                (idx, [1:   4]) = [  4.16644E-03 0.01915  1.38435E-03 0.18293 ];
INF_SCATT3                (idx, [1:   4]) = [  8.20066E-04 0.05039  4.27871E-04 0.83756 ];
INF_SCATT4                (idx, [1:   4]) = [  1.60443E-04 0.15150  2.44393E-04 0.91026 ];
INF_SCATT5                (idx, [1:   4]) = [  9.05361E-05 0.28762  1.17987E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  1.16451E-04 0.19121 -8.52040E-05 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [ -4.51518E-05 0.39273  1.05329E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50056E-01 0.00027  3.10830E-01 0.00401 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97613E-02 0.00409  1.86624E-02 0.02160 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.16648E-03 0.01916  1.38435E-03 0.18293 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.20096E-04 0.05045  4.27871E-04 0.83756 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.60412E-04 0.15186  2.44393E-04 0.91026 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.06948E-05 0.28688  1.17987E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  1.16422E-04 0.19136 -8.52040E-05 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [ -4.50983E-05 0.39190  1.05329E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03402E-01 0.00121  4.06419E-01 0.00487 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63880E+00 0.00121  8.20269E-01 0.00487 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  5.19607E-03 0.00154  1.30301E-01 0.00440 ];
INF_REMXS                 (idx, [1:   4]) = [  5.92578E-03 0.00665  1.31196E-01 0.00618 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49314E-01 0.00026  7.40261E-04 0.00971  8.17878E-04 0.04224  3.10013E-01 0.00398 ];
INF_S1                    (idx, [1:   8]) = [  1.99289E-02 0.00411 -1.67612E-04 0.02075  1.87553E-04 0.12696  1.84748E-02 0.02114 ];
INF_S2                    (idx, [1:   8]) = [  4.18700E-03 0.01882 -2.05588E-05 0.13697  2.54515E-06 1.00000  1.38180E-03 0.18550 ];
INF_S3                    (idx, [1:   8]) = [  8.30268E-04 0.04699 -1.02021E-05 0.28892 -2.94860E-05 0.65316  4.57357E-04 0.76650 ];
INF_S4                    (idx, [1:   8]) = [  1.66218E-04 0.14910 -5.77450E-06 0.14891 -1.90637E-05 0.15340  2.63457E-04 0.83777 ];
INF_S5                    (idx, [1:   8]) = [  8.85792E-05 0.28843  1.95694E-06 0.62354 -5.52918E-06 1.00000  1.23516E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  1.17574E-04 0.19095 -1.12277E-06 1.00000 -5.76404E-06 1.00000 -7.94400E-05 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -4.51177E-05 0.39324 -3.41001E-08 1.00000 -1.25049E-05 0.73730  1.17834E-04 0.91184 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49315E-01 0.00026  7.40261E-04 0.00971  8.17878E-04 0.04224  3.10013E-01 0.00398 ];
INF_SP1                   (idx, [1:   8]) = [  1.99289E-02 0.00411 -1.67612E-04 0.02075  1.87553E-04 0.12696  1.84748E-02 0.02114 ];
INF_SP2                   (idx, [1:   8]) = [  4.18703E-03 0.01883 -2.05588E-05 0.13697  2.54515E-06 1.00000  1.38180E-03 0.18550 ];
INF_SP3                   (idx, [1:   8]) = [  8.30299E-04 0.04704 -1.02021E-05 0.28892 -2.94860E-05 0.65316  4.57357E-04 0.76650 ];
INF_SP4                   (idx, [1:   8]) = [  1.66187E-04 0.14946 -5.77450E-06 0.14891 -1.90637E-05 0.15340  2.63457E-04 0.83777 ];
INF_SP5                   (idx, [1:   8]) = [  8.87378E-05 0.28765  1.95694E-06 0.62354 -5.52918E-06 1.00000  1.23516E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  1.17545E-04 0.19111 -1.12277E-06 1.00000 -5.76404E-06 1.00000 -7.94400E-05 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -4.50642E-05 0.39242 -3.41001E-08 1.00000 -1.25049E-05 0.73730  1.17834E-04 0.91184 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.18938E-01 0.00375 -1.94109E-01 0.01120 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.90613E-01 0.00935 -1.10333E-01 0.01235 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.99214E-01 0.00619 -1.10424E-01 0.01352 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.20684E-01 0.00527  3.77754E-01 0.04043 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.95719E-01 0.00375 -1.71832E+00 0.01114 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.79721E-01 0.00941 -3.02353E+00 0.01264 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.67843E-01 0.00614 -3.02142E+00 0.01346 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.03959E+00 0.00534  8.89976E-01 0.04215 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.16411E-03 0.02328  2.49269E-04 0.12692  9.71020E-04 0.05618  6.90504E-04 0.07384  1.37356E-03 0.05552  2.32734E-03 0.03926  7.09313E-04 0.07927  6.72510E-04 0.07195  1.70594E-04 0.12766 ];
LAMBDA                    (idx, [1:  18]) = [  4.29004E-01 0.03018  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 7.0E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.3E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84752E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '5' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.15206E+04 0.01623  5.48202E+04 0.00534  1.24713E+05 0.00204  2.14634E+05 0.00377  2.21340E+05 0.00317  2.01896E+05 0.00313  1.80663E+05 0.00180  1.56620E+05 0.00324  1.37328E+05 0.00450  1.21904E+05 0.00287  1.10470E+05 0.00352  1.02178E+05 0.00299  9.45894E+04 0.00614  9.04052E+04 0.00327  8.68787E+04 0.00521  7.33475E+04 0.00534  7.24206E+04 0.00313  6.90383E+04 0.00503  6.61565E+04 0.00408  1.24258E+05 0.00331  1.09775E+05 0.00184  7.31461E+04 0.00672  4.39143E+04 0.00529  4.71206E+04 0.00614  4.06130E+04 0.00674  3.26312E+04 0.00257  5.28836E+04 0.00335  1.08927E+04 0.01071  1.35365E+04 0.01076  1.23410E+04 0.00611  7.07547E+03 0.00795  1.19972E+04 0.01203  8.13213E+03 0.01498  6.57349E+03 0.00710  1.17891E+03 0.03379  1.15230E+03 0.03411  1.16817E+03 0.02380  1.19973E+03 0.03228  1.19307E+03 0.04909  1.25380E+03 0.05684  1.22964E+03 0.02005  1.09854E+03 0.03786  2.12124E+03 0.02024  3.30847E+03 0.01830  4.16703E+03 0.01176  1.05288E+04 0.00810  9.95280E+03 0.01141  9.23070E+03 0.00661  5.05216E+03 0.01862  3.15471E+03 0.01953  2.18230E+03 0.01766  2.24274E+03 0.02253  3.63056E+03 0.01638  4.04188E+03 0.01625  6.23208E+03 0.01337  8.18606E+03 0.00823  1.18617E+04 0.00701  8.10920E+03 0.00813  6.49120E+03 0.00695  4.90794E+03 0.01461  4.65230E+03 0.02327  4.87617E+03 0.00922  4.24832E+03 0.01370  2.86867E+03 0.02119  2.70218E+03 0.01234  2.52267E+03 0.01975  2.02204E+03 0.03125  1.53152E+03 0.02943  8.94639E+02 0.03041  2.36053E+02 0.05419 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.74842E+00 0.00286 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.40770E+00 0.00125  5.59148E-02 0.00245 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55133E-01 0.00057  4.30772E-01 0.00305 ];
INF_CAPT                  (idx, [1:   4]) = [  1.96047E-03 0.00300  1.84467E-02 0.00263 ];
INF_ABS                   (idx, [1:   4]) = [  4.77071E-03 0.00178  1.19577E-01 0.00261 ];
INF_FISS                  (idx, [1:   4]) = [  2.81024E-03 0.00142  1.01131E-01 0.00261 ];
INF_NSF                   (idx, [1:   4]) = [  6.86942E-03 0.00142  2.46368E-01 0.00261 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44443E+00 5.7E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 2.3E-08  2.02270E+02 8.2E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.24132E-08 0.00187  2.33486E-06 0.00310 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.50377E-01 0.00054  3.10654E-01 0.00342 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97756E-02 0.00093  1.79682E-02 0.01565 ];
INF_SCATT2                (idx, [1:   4]) = [  4.15171E-03 0.01405  1.63442E-03 0.20520 ];
INF_SCATT3                (idx, [1:   4]) = [  8.20098E-04 0.04049 -9.85884E-06 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  1.68010E-04 0.27276 -4.08441E-04 0.57285 ];
INF_SCATT5                (idx, [1:   4]) = [  8.41419E-05 0.35406  1.59614E-04 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  7.36803E-05 0.26987 -2.42864E-04 0.92498 ];
INF_SCATT7                (idx, [1:   4]) = [ -7.98622E-05 0.42793  1.40140E-04 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.50378E-01 0.00054  3.10654E-01 0.00342 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97758E-02 0.00093  1.79682E-02 0.01565 ];
INF_SCATTP2               (idx, [1:   4]) = [  4.15162E-03 0.01405  1.63442E-03 0.20520 ];
INF_SCATTP3               (idx, [1:   4]) = [  8.20179E-04 0.04045 -9.85884E-06 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.67950E-04 0.27306 -4.08441E-04 0.57285 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.40410E-05 0.35446  1.59614E-04 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  7.37299E-05 0.26938 -2.42864E-04 0.92498 ];
INF_SCATTP7               (idx, [1:   4]) = [ -7.98532E-05 0.42794  1.40140E-04 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.03554E-01 0.00091  3.98765E-01 0.00371 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.63757E+00 0.00091  8.35971E-01 0.00373 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.76964E-03 0.00182  1.19577E-01 0.00261 ];
INF_REMXS                 (idx, [1:   4]) = [  5.56105E-03 0.00303  1.20841E-01 0.00355 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.49572E-01 0.00055  8.04948E-04 0.00986  7.22881E-04 0.06955  3.09931E-01 0.00344 ];
INF_S1                    (idx, [1:   8]) = [  1.99629E-02 0.00108 -1.87363E-04 0.02489  2.15550E-04 0.08201  1.77527E-02 0.01529 ];
INF_S2                    (idx, [1:   8]) = [  4.16849E-03 0.01400 -1.67742E-05 0.16135  3.85893E-05 0.36045  1.59583E-03 0.20570 ];
INF_S3                    (idx, [1:   8]) = [  8.34639E-04 0.03666 -1.45411E-05 0.28275 -4.82282E-05 0.31835  3.83694E-05 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  1.69969E-04 0.28419 -1.95893E-06 1.00000 -3.12233E-05 0.28804 -3.77217E-04 0.61476 ];
INF_S5                    (idx, [1:   8]) = [  8.31250E-05 0.37263  1.01688E-06 1.00000 -1.86526E-05 0.54702  1.78267E-04 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  7.21722E-05 0.29372  1.50805E-06 1.00000 -1.63484E-05 0.60390 -2.26516E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [ -7.81632E-05 0.42060 -1.69905E-06 0.90188 -9.89397E-06 0.53730  1.50034E-04 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.49573E-01 0.00055  8.04948E-04 0.00986  7.22881E-04 0.06955  3.09931E-01 0.00344 ];
INF_SP1                   (idx, [1:   8]) = [  1.99631E-02 0.00108 -1.87363E-04 0.02489  2.15550E-04 0.08201  1.77527E-02 0.01529 ];
INF_SP2                   (idx, [1:   8]) = [  4.16840E-03 0.01400 -1.67742E-05 0.16135  3.85893E-05 0.36045  1.59583E-03 0.20570 ];
INF_SP3                   (idx, [1:   8]) = [  8.34720E-04 0.03662 -1.45411E-05 0.28275 -4.82282E-05 0.31835  3.83694E-05 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  1.69909E-04 0.28446 -1.95893E-06 1.00000 -3.12233E-05 0.28804 -3.77217E-04 0.61476 ];
INF_SP5                   (idx, [1:   8]) = [  8.30242E-05 0.37307  1.01688E-06 1.00000 -1.86526E-05 0.54702  1.78267E-04 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  7.22218E-05 0.29313  1.50805E-06 1.00000 -1.63484E-05 0.60390 -2.26516E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [ -7.81541E-05 0.42061 -1.69905E-06 0.90188 -9.89397E-06 0.53730  1.50034E-04 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.26559E-01 0.00536 -1.85010E-01 0.01186 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.99747E-01 0.01014 -1.06109E-01 0.01023 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.07444E-01 0.00826 -1.07648E-01 0.00650 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.26813E-01 0.00716  4.05586E-01 0.04687 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.81558E-01 0.00532 -1.80292E+00 0.01140 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.67350E-01 0.01020 -3.14309E+00 0.01039 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.57114E-01 0.00834 -3.09716E+00 0.00649 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.02021E+00 0.00718  8.31473E-01 0.04962 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.00418E-03 0.02183  2.70244E-04 0.11029  1.02701E-03 0.05668  6.70246E-04 0.06450  1.42549E-03 0.04779  2.31791E-03 0.03800  6.16219E-04 0.06841  5.22470E-04 0.07506  1.54594E-04 0.13045 ];
LAMBDA                    (idx, [1:  18]) = [  3.93880E-01 0.03322  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59059 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84751E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '4' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  2.20307E+04 0.00377  1.03515E+05 0.00514  2.38040E+05 0.00241  4.11263E+05 0.00232  4.24704E+05 0.00106  3.87612E+05 0.00165  3.46853E+05 0.00248  3.03585E+05 0.00316  2.66675E+05 0.00206  2.38676E+05 0.00203  2.17696E+05 0.00326  2.00865E+05 0.00306  1.86816E+05 0.00349  1.78148E+05 0.00216  1.70287E+05 0.00239  1.45245E+05 0.00399  1.42706E+05 0.00341  1.38031E+05 0.00405  1.31321E+05 0.00408  2.46912E+05 0.00134  2.20269E+05 0.00304  1.47267E+05 0.00189  8.93517E+04 0.00354  9.67848E+04 0.00450  8.53083E+04 0.00392  6.78225E+04 0.00495  1.10557E+05 0.00477  2.26302E+04 0.00574  2.80225E+04 0.00441  2.52553E+04 0.00677  1.44321E+04 0.00395  2.49237E+04 0.00691  1.63031E+04 0.00580  1.32836E+04 0.01072  2.37396E+03 0.01045  2.47209E+03 0.01671  2.55977E+03 0.02261  2.61779E+03 0.01377  2.49880E+03 0.03086  2.52149E+03 0.01596  2.51505E+03 0.01945  2.37823E+03 0.01845  4.58313E+03 0.01085  6.97216E+03 0.00772  8.81026E+03 0.01038  2.19634E+04 0.00924  2.10796E+04 0.00266  1.98565E+04 0.00399  1.10465E+04 0.00618  6.79555E+03 0.00992  4.80031E+03 0.01263  4.88302E+03 0.01255  7.85284E+03 0.01017  9.00778E+03 0.00721  1.41196E+04 0.00739  1.86164E+04 0.01027  2.70649E+04 0.00689  1.91454E+04 0.00454  1.51423E+04 0.00745  1.15674E+04 0.00729  1.07138E+04 0.00904  1.14030E+04 0.00854  9.78929E+03 0.01447  7.00986E+03 0.01184  6.66271E+03 0.01011  5.94607E+03 0.01303  5.11909E+03 0.01273  3.88528E+03 0.02472  2.21719E+03 0.02146  6.04465E+02 0.03977 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.75773E+00 0.00138 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  2.76271E+00 0.00062  1.27164E-01 0.00206 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.55633E-01 0.00027  4.17314E-01 0.00211 ];
INF_CAPT                  (idx, [1:   4]) = [  1.84368E-03 0.00140  1.66432E-02 0.00201 ];
INF_ABS                   (idx, [1:   4]) = [  4.39610E-03 0.00071  1.07430E-01 0.00192 ];
INF_FISS                  (idx, [1:   4]) = [  2.55242E-03 0.00053  9.07867E-02 0.00190 ];
INF_NSF                   (idx, [1:   4]) = [  6.23778E-03 0.00053  2.21169E-01 0.00190 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44387E+00 8.1E-06  2.43614E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 3.7E-08  2.02270E+02 5.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.44840E-08 0.00180  2.38982E-06 0.00152 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.51246E-01 0.00025  3.09662E-01 0.00258 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97246E-02 0.00170  1.78530E-02 0.01270 ];
INF_SCATT2                (idx, [1:   4]) = [  3.94193E-03 0.00859  1.12590E-03 0.10942 ];
INF_SCATT3                (idx, [1:   4]) = [  7.78297E-04 0.04274  3.81688E-04 0.49056 ];
INF_SCATT4                (idx, [1:   4]) = [  1.25186E-04 0.23188  1.68966E-04 0.49463 ];
INF_SCATT5                (idx, [1:   4]) = [  7.05806E-05 0.36399  1.49567E-04 0.84247 ];
INF_SCATT6                (idx, [1:   4]) = [  5.39849E-05 0.30297  1.78564E-04 0.87082 ];
INF_SCATT7                (idx, [1:   4]) = [  2.76526E-05 1.00000 -7.91847E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.51247E-01 0.00025  3.09662E-01 0.00258 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97247E-02 0.00170  1.78530E-02 0.01270 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.94197E-03 0.00858  1.12590E-03 0.10942 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.78263E-04 0.04272  3.81688E-04 0.49056 ];
INF_SCATTP4               (idx, [1:   4]) = [  1.25088E-04 0.23232  1.68966E-04 0.49463 ];
INF_SCATTP5               (idx, [1:   4]) = [  7.04957E-05 0.36459  1.49567E-04 0.84247 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.39939E-05 0.30288  1.78564E-04 0.87082 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.76310E-05 1.00000 -7.91847E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.04597E-01 0.00074  3.86600E-01 0.00206 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.62922E+00 0.00074  8.62236E-01 0.00206 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.39489E-03 0.00071  1.07430E-01 0.00192 ];
INF_REMXS                 (idx, [1:   4]) = [  5.25642E-03 0.00339  1.08355E-01 0.00197 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.50376E-01 0.00027  8.69812E-04 0.00524  7.03158E-04 0.05148  3.08958E-01 0.00255 ];
INF_S1                    (idx, [1:   8]) = [  1.99192E-02 0.00175 -1.94585E-04 0.01241  1.89566E-04 0.11151  1.76635E-02 0.01195 ];
INF_S2                    (idx, [1:   8]) = [  3.96901E-03 0.00838 -2.70797E-05 0.07522  1.91504E-05 0.44787  1.10675E-03 0.11012 ];
INF_S3                    (idx, [1:   8]) = [  7.92834E-04 0.04079 -1.45368E-05 0.16889 -1.12673E-05 0.68348  3.92955E-04 0.48035 ];
INF_S4                    (idx, [1:   8]) = [  1.28110E-04 0.22698 -2.92417E-06 0.46428 -1.85303E-05 0.32920  1.87496E-04 0.46506 ];
INF_S5                    (idx, [1:   8]) = [  7.07782E-05 0.35837 -1.97625E-07 1.00000 -6.17119E-06 1.00000  1.55738E-04 0.84018 ];
INF_S6                    (idx, [1:   8]) = [  5.29177E-05 0.29826  1.06716E-06 1.00000 -4.89603E-06 1.00000  1.83460E-04 0.81969 ];
INF_S7                    (idx, [1:   8]) = [  2.64510E-05 1.00000  1.20164E-06 0.93936 -1.24229E-05 0.29141 -6.67618E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.50378E-01 0.00027  8.69812E-04 0.00524  7.03158E-04 0.05148  3.08958E-01 0.00255 ];
INF_SP1                   (idx, [1:   8]) = [  1.99193E-02 0.00175 -1.94585E-04 0.01241  1.89566E-04 0.11151  1.76635E-02 0.01195 ];
INF_SP2                   (idx, [1:   8]) = [  3.96905E-03 0.00837 -2.70797E-05 0.07522  1.91504E-05 0.44787  1.10675E-03 0.11012 ];
INF_SP3                   (idx, [1:   8]) = [  7.92800E-04 0.04078 -1.45368E-05 0.16889 -1.12673E-05 0.68348  3.92955E-04 0.48035 ];
INF_SP4                   (idx, [1:   8]) = [  1.28012E-04 0.22742 -2.92417E-06 0.46428 -1.85303E-05 0.32920  1.87496E-04 0.46506 ];
INF_SP5                   (idx, [1:   8]) = [  7.06934E-05 0.35896 -1.97625E-07 1.00000 -6.17119E-06 1.00000  1.55738E-04 0.84018 ];
INF_SP6                   (idx, [1:   8]) = [  5.29267E-05 0.29818  1.06716E-06 1.00000 -4.89603E-06 1.00000  1.83460E-04 0.81969 ];
INF_SP7                   (idx, [1:   8]) = [  2.64293E-05 1.00000  1.20164E-06 0.93936 -1.24229E-05 0.29141 -6.67618E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.34100E-01 0.00445 -1.99181E-01 0.00633 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  5.09034E-01 0.00692 -1.13902E-01 0.00344 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  5.08927E-01 0.00874 -1.15126E-01 0.00721 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.35486E-01 0.00332  4.18745E-01 0.03655 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.67949E-01 0.00449 -1.67386E+00 0.00631 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  6.54994E-01 0.00699 -2.92667E+00 0.00341 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  6.55217E-01 0.00853 -2.89613E+00 0.00724 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.93638E-01 0.00333  8.01225E-01 0.03554 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.02610E-03 0.01686  2.54081E-04 0.09673  1.11531E-03 0.04484  6.88375E-04 0.05585  1.40637E-03 0.03669  2.26443E-03 0.02789  5.82809E-04 0.04951  5.37709E-04 0.05652  1.77013E-04 0.11681 ];
LAMBDA                    (idx, [1:  18]) = [  4.03424E-01 0.02838  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.6E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59059 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84751E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '3' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  4.17083E+03 0.03135  1.92941E+04 0.00972  4.47110E+04 0.00464  7.71275E+04 0.00617  7.99994E+04 0.00431  7.35929E+04 0.00448  6.66012E+04 0.00863  5.81316E+04 0.00665  5.12681E+04 0.00346  4.61642E+04 0.00662  4.27576E+04 0.00633  3.96696E+04 0.00647  3.68192E+04 0.00400  3.49422E+04 0.00461  3.40435E+04 0.00607  2.85919E+04 0.00456  2.80818E+04 0.00513  2.74513E+04 0.00517  2.62184E+04 0.00375  4.87920E+04 0.00793  4.42154E+04 0.00330  2.97401E+04 0.00695  1.80072E+04 0.00583  1.98835E+04 0.00451  1.75111E+04 0.00467  1.41205E+04 0.00736  2.33033E+04 0.00512  4.84325E+03 0.02106  5.82640E+03 0.02393  5.20472E+03 0.01018  3.05891E+03 0.00829  5.08839E+03 0.01291  3.35654E+03 0.01374  2.86600E+03 0.02697  5.21813E+02 0.07275  5.04499E+02 0.08530  4.79984E+02 0.05819  5.69822E+02 0.05138  5.00365E+02 0.06086  5.02017E+02 0.04602  4.84691E+02 0.07287  5.17893E+02 0.05403  9.38849E+02 0.04509  1.46766E+03 0.04589  1.91178E+03 0.01344  4.57569E+03 0.01319  4.46056E+03 0.01261  4.23852E+03 0.01783  2.47163E+03 0.02106  1.46501E+03 0.03325  1.04668E+03 0.04622  1.08843E+03 0.03691  1.84242E+03 0.02783  2.01415E+03 0.01858  3.21894E+03 0.02247  4.10746E+03 0.02183  6.58821E+03 0.01735  4.68476E+03 0.00653  3.83492E+03 0.02334  3.00206E+03 0.02535  2.77130E+03 0.02276  3.02236E+03 0.02267  2.77396E+03 0.01463  1.97269E+03 0.03059  1.88315E+03 0.01835  1.69942E+03 0.02650  1.50047E+03 0.02727  1.19622E+03 0.02942  7.10885E+02 0.04630  2.01958E+02 0.03718 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.77616E+00 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  5.39211E-01 0.00105  3.08980E-02 0.00604 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56298E-01 0.00117  4.11506E-01 0.00441 ];
INF_CAPT                  (idx, [1:   4]) = [  1.76104E-03 0.00444  1.56852E-02 0.00501 ];
INF_ABS                   (idx, [1:   4]) = [  4.08647E-03 0.00345  1.00818E-01 0.00535 ];
INF_FISS                  (idx, [1:   4]) = [  2.32543E-03 0.00280  8.51328E-02 0.00542 ];
INF_NSF                   (idx, [1:   4]) = [  5.68165E-03 0.00279  2.07395E-01 0.00542 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44327E+00 1.1E-05  2.43614E+00 5.8E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 6.2E-08  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  4.70754E-08 0.00365  2.53795E-06 0.00233 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.52177E-01 0.00110  3.10821E-01 0.00419 ];
INF_SCATT1                (idx, [1:   4]) = [  1.97214E-02 0.00692  1.81735E-02 0.02525 ];
INF_SCATT2                (idx, [1:   4]) = [  3.84122E-03 0.00948  1.49990E-03 0.30950 ];
INF_SCATT3                (idx, [1:   4]) = [  7.44359E-04 0.06340 -1.83999E-05 1.00000 ];
INF_SCATT4                (idx, [1:   4]) = [  9.07643E-05 0.44024  5.63031E-04 0.53911 ];
INF_SCATT5                (idx, [1:   4]) = [  2.05125E-06 1.00000  3.01712E-04 0.85495 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08993E-05 1.00000  1.37928E-04 1.00000 ];
INF_SCATT7                (idx, [1:   4]) = [  9.81519E-06 1.00000  2.46631E-04 0.69001 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.52178E-01 0.00110  3.10821E-01 0.00419 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.97210E-02 0.00692  1.81735E-02 0.02525 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.84108E-03 0.00951  1.49990E-03 0.30950 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.44497E-04 0.06332 -1.83999E-05 1.00000 ];
INF_SCATTP4               (idx, [1:   4]) = [  9.07342E-05 0.44066  5.63031E-04 0.53911 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.79843E-06 1.00000  3.01712E-04 0.85495 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10888E-05 1.00000  1.37928E-04 1.00000 ];
INF_SCATTP7               (idx, [1:   4]) = [  9.96788E-06 1.00000  2.46631E-04 0.69001 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.05756E-01 0.00169  3.80309E-01 0.00400 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.62006E+00 0.00169  8.76551E-01 0.00402 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  4.08492E-03 0.00343  1.00818E-01 0.00535 ];
INF_REMXS                 (idx, [1:   4]) = [  5.06690E-03 0.00501  1.01360E-01 0.00615 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51231E-01 0.00112  9.45068E-04 0.01335  6.74954E-04 0.04162  3.10146E-01 0.00416 ];
INF_S1                    (idx, [1:   8]) = [  1.99275E-02 0.00682 -2.06127E-04 0.03759  1.68115E-04 0.11251  1.80054E-02 0.02535 ];
INF_S2                    (idx, [1:   8]) = [  3.86918E-03 0.01030 -2.79597E-05 0.32304 -3.28536E-05 0.51751  1.53276E-03 0.30001 ];
INF_S3                    (idx, [1:   8]) = [  7.58651E-04 0.06298 -1.42922E-05 0.26842 -1.57382E-05 0.90594 -2.66177E-06 1.00000 ];
INF_S4                    (idx, [1:   8]) = [  9.60972E-05 0.41457 -5.33295E-06 1.00000 -5.28208E-06 1.00000  5.68313E-04 0.51488 ];
INF_S5                    (idx, [1:   8]) = [  8.75153E-06 1.00000 -6.70028E-06 0.46913 -1.40162E-05 0.43060  3.15728E-04 0.80874 ];
INF_S6                    (idx, [1:   8]) = [  4.88639E-05 1.00000  2.03537E-06 1.00000 -6.84702E-06 1.00000  1.44775E-04 1.00000 ];
INF_S7                    (idx, [1:   8]) = [  6.10697E-06 1.00000  3.70822E-06 1.00000  5.39644E-06 1.00000  2.41234E-04 0.69776 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51233E-01 0.00112  9.45068E-04 0.01335  6.74954E-04 0.04162  3.10146E-01 0.00416 ];
INF_SP1                   (idx, [1:   8]) = [  1.99271E-02 0.00683 -2.06127E-04 0.03759  1.68115E-04 0.11251  1.80054E-02 0.02535 ];
INF_SP2                   (idx, [1:   8]) = [  3.86904E-03 0.01033 -2.79597E-05 0.32304 -3.28536E-05 0.51751  1.53276E-03 0.30001 ];
INF_SP3                   (idx, [1:   8]) = [  7.58789E-04 0.06291 -1.42922E-05 0.26842 -1.57382E-05 0.90594 -2.66177E-06 1.00000 ];
INF_SP4                   (idx, [1:   8]) = [  9.60672E-05 0.41504 -5.33295E-06 1.00000 -5.28208E-06 1.00000  5.68313E-04 0.51488 ];
INF_SP5                   (idx, [1:   8]) = [  8.49871E-06 1.00000 -6.70028E-06 0.46913 -1.40162E-05 0.43060  3.15728E-04 0.80874 ];
INF_SP6                   (idx, [1:   8]) = [  4.90535E-05 1.00000  2.03537E-06 1.00000 -6.84702E-06 1.00000  1.44775E-04 1.00000 ];
INF_SP7                   (idx, [1:   8]) = [  6.25966E-06 1.00000  3.70822E-06 1.00000  5.39644E-06 1.00000  2.41234E-04 0.69776 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.20892E-01 0.00391 -2.21285E-01 0.01862 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.71856E-01 0.00845 -1.25908E-01 0.01187 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.67964E-01 0.01400 -1.26502E-01 0.01542 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.48559E-01 0.00431  4.47394E-01 0.06152 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.92030E-01 0.00394 -1.50904E+00 0.01912 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.06679E-01 0.00833 -2.64935E+00 0.01216 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.13005E-01 0.01403 -2.63820E+00 0.01569 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.56407E-01 0.00432  7.60442E-01 0.06598 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  7.25937E-03 0.03313  2.27305E-04 0.17617  1.12280E-03 0.08523  8.34526E-04 0.11330  1.46799E-03 0.07624  2.17378E-03 0.06584  6.92358E-04 0.11348  5.69407E-04 0.10776  1.71205E-04 0.19284 ];
LAMBDA                    (idx, [1:  18]) = [  3.92398E-01 0.04487  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 7.0E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.5E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59059 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84751E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '2' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  3.00264E+04 0.00690  1.44537E+05 0.00289  3.29806E+05 0.00334  5.77420E+05 0.00102  6.02924E+05 0.00191  5.52957E+05 0.00174  4.99945E+05 0.00107  4.41384E+05 0.00090  3.92037E+05 0.00236  3.55199E+05 0.00238  3.26755E+05 0.00198  3.04800E+05 0.00186  2.85199E+05 0.00243  2.71634E+05 0.00065  2.62680E+05 0.00234  2.23452E+05 0.00356  2.19416E+05 0.00340  2.11806E+05 0.00310  2.04648E+05 0.00314  3.85325E+05 0.00272  3.48055E+05 0.00228  2.35844E+05 0.00335  1.45732E+05 0.00327  1.60393E+05 0.00279  1.42315E+05 0.00433  1.14133E+05 0.00286  1.90132E+05 0.00280  3.86299E+04 0.00979  4.78053E+04 0.00566  4.26017E+04 0.00424  2.41576E+04 0.00926  4.16220E+04 0.00419  2.75946E+04 0.00420  2.29883E+04 0.00892  4.16258E+03 0.02699  4.17617E+03 0.02553  4.31425E+03 0.02902  4.40308E+03 0.03366  4.29284E+03 0.01283  4.25619E+03 0.01680  4.47819E+03 0.02298  3.99931E+03 0.01766  7.75907E+03 0.01505  1.22152E+04 0.01339  1.51627E+04 0.00462  3.78032E+04 0.00522  3.67151E+04 0.00542  3.55464E+04 0.00814  2.00886E+04 0.00481  1.27649E+04 0.00993  8.99705E+03 0.01318  9.51867E+03 0.01323  1.53585E+04 0.00799  1.72106E+04 0.00902  2.85330E+04 0.00711  3.80769E+04 0.00664  5.99120E+04 0.00313  4.43891E+04 0.00792  3.59498E+04 0.01013  2.87170E+04 0.00610  2.77430E+04 0.00663  2.94401E+04 0.00558  2.69315E+04 0.01128  1.90906E+04 0.00739  1.87897E+04 0.00892  1.74513E+04 0.00907  1.53655E+04 0.00592  1.22636E+04 0.01074  7.86318E+03 0.00879  2.55743E+03 0.02604 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.79092E+00 0.00176 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  4.15546E+00 0.00074  2.84634E-01 0.00282 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  2.56405E-01 0.00043  4.00234E-01 0.00181 ];
INF_CAPT                  (idx, [1:   4]) = [  1.66786E-03 0.00094  1.42697E-02 0.00190 ];
INF_ABS                   (idx, [1:   4]) = [  3.78330E-03 0.00072  9.12389E-02 0.00203 ];
INF_FISS                  (idx, [1:   4]) = [  2.11544E-03 0.00087  7.69692E-02 0.00205 ];
INF_NSF                   (idx, [1:   4]) = [  5.16741E-03 0.00087  1.87508E-01 0.00205 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.44271E+00 5.3E-06  2.43614E+00 5.8E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.02273E+02 2.6E-08  2.02270E+02 8.2E-09 ];
INF_INVV                  (idx, [1:   4]) = [  4.94442E-08 0.00160  2.64296E-06 0.00062 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  2.52603E-01 0.00044  3.08913E-01 0.00187 ];
INF_SCATT1                (idx, [1:   4]) = [  1.95375E-02 0.00112  1.77903E-02 0.00895 ];
INF_SCATT2                (idx, [1:   4]) = [  3.71001E-03 0.01009  1.36249E-03 0.07591 ];
INF_SCATT3                (idx, [1:   4]) = [  6.78889E-04 0.06112  3.69888E-04 0.28558 ];
INF_SCATT4                (idx, [1:   4]) = [  6.98044E-05 0.25827  9.98709E-06 1.00000 ];
INF_SCATT5                (idx, [1:   4]) = [  5.37050E-05 0.33233  4.95167E-05 1.00000 ];
INF_SCATT6                (idx, [1:   4]) = [  6.36333E-05 0.38246  2.26487E-04 0.35932 ];
INF_SCATT7                (idx, [1:   4]) = [  2.35845E-05 0.69725 -5.10948E-05 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  2.52604E-01 0.00044  3.08913E-01 0.00187 ];
INF_SCATTP1               (idx, [1:   4]) = [  1.95375E-02 0.00112  1.77903E-02 0.00895 ];
INF_SCATTP2               (idx, [1:   4]) = [  3.70997E-03 0.01008  1.36249E-03 0.07591 ];
INF_SCATTP3               (idx, [1:   4]) = [  6.78879E-04 0.06106  3.69888E-04 0.28558 ];
INF_SCATTP4               (idx, [1:   4]) = [  6.97887E-05 0.25864  9.98709E-06 1.00000 ];
INF_SCATTP5               (idx, [1:   4]) = [  5.36922E-05 0.33254  4.95167E-05 1.00000 ];
INF_SCATTP6               (idx, [1:   4]) = [  6.35802E-05 0.38308  2.26487E-04 0.35932 ];
INF_SCATTP7               (idx, [1:   4]) = [  2.35994E-05 0.69649 -5.10948E-05 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.06700E-01 0.00062  3.71827E-01 0.00168 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.61265E+00 0.00062  8.96487E-01 0.00168 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  3.78236E-03 0.00072  9.12389E-02 0.00203 ];
INF_REMXS                 (idx, [1:   4]) = [  4.79388E-03 0.00199  9.18189E-02 0.00220 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  2.51612E-01 0.00043  9.91202E-04 0.00330  4.97134E-04 0.01682  3.08415E-01 0.00189 ];
INF_S1                    (idx, [1:   8]) = [  1.97662E-02 0.00108 -2.28692E-04 0.00778  1.17501E-04 0.05017  1.76728E-02 0.00913 ];
INF_S2                    (idx, [1:   8]) = [  3.73367E-03 0.01000 -2.36651E-05 0.09702  2.48899E-07 1.00000  1.36224E-03 0.07927 ];
INF_S3                    (idx, [1:   8]) = [  6.95297E-04 0.05747 -1.64079E-05 0.11851 -1.64488E-05 0.14860  3.86337E-04 0.27534 ];
INF_S4                    (idx, [1:   8]) = [  7.61752E-05 0.23677 -6.37072E-06 0.12952 -1.06558E-05 0.34702  2.06429E-05 1.00000 ];
INF_S5                    (idx, [1:   8]) = [  5.32699E-05 0.33172  4.35108E-07 1.00000 -6.04862E-06 0.65651  5.55653E-05 1.00000 ];
INF_S6                    (idx, [1:   8]) = [  6.31701E-05 0.37941  4.63169E-07 1.00000 -4.43569E-06 0.76949  2.30923E-04 0.35827 ];
INF_S7                    (idx, [1:   8]) = [  2.23896E-05 0.75478  1.19492E-06 0.67645 -4.94204E-06 0.55209 -4.61527E-05 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  2.51613E-01 0.00043  9.91202E-04 0.00330  4.97134E-04 0.01682  3.08415E-01 0.00189 ];
INF_SP1                   (idx, [1:   8]) = [  1.97662E-02 0.00108 -2.28692E-04 0.00778  1.17501E-04 0.05017  1.76728E-02 0.00913 ];
INF_SP2                   (idx, [1:   8]) = [  3.73364E-03 0.01000 -2.36651E-05 0.09702  2.48899E-07 1.00000  1.36224E-03 0.07927 ];
INF_SP3                   (idx, [1:   8]) = [  6.95287E-04 0.05741 -1.64079E-05 0.11851 -1.64488E-05 0.14860  3.86337E-04 0.27534 ];
INF_SP4                   (idx, [1:   8]) = [  7.61595E-05 0.23711 -6.37072E-06 0.12952 -1.06558E-05 0.34702  2.06429E-05 1.00000 ];
INF_SP5                   (idx, [1:   8]) = [  5.32571E-05 0.33196  4.35108E-07 1.00000 -6.04862E-06 0.65651  5.55653E-05 1.00000 ];
INF_SP6                   (idx, [1:   8]) = [  6.31170E-05 0.38002  4.63169E-07 1.00000 -4.43569E-06 0.76949  2.30923E-04 0.35827 ];
INF_SP7                   (idx, [1:   8]) = [  2.24045E-05 0.75396  1.19492E-06 0.67645 -4.94204E-06 0.55209 -4.61527E-05 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  4.21377E-01 0.00260 -2.44057E-01 0.00333 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  4.57450E-01 0.00211 -1.38990E-01 0.00541 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  4.62201E-01 0.00597 -1.37574E-01 0.00768 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  3.61070E-01 0.00361  4.60530E-01 0.01510 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  7.91083E-01 0.00259 -1.36588E+00 0.00337 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  7.28693E-01 0.00211 -2.39861E+00 0.00547 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  7.21314E-01 0.00596 -2.42365E+00 0.00771 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  9.23242E-01 0.00360  7.24630E-01 0.01511 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  6.75490E-03 0.01340  2.42163E-04 0.07795  1.05784E-03 0.03685  6.19883E-04 0.04751  1.33230E-03 0.03148  2.16491E-03 0.02629  6.28298E-04 0.05281  5.37769E-04 0.04933  1.71738E-04 0.07771 ];
LAMBDA                    (idx, [1:  18]) = [  4.11157E-01 0.02090  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.5E-09  1.33042E-01 3.7E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.1E-09 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45606E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59059 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84751E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = 'g' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.58519E+05 0.00588  7.44332E+05 0.00190  1.52068E+06 0.00067  3.19320E+06 0.00097  3.63588E+06 0.00061  3.50231E+06 0.00074  3.35351E+06 0.00053  3.18138E+06 0.00047  3.02868E+06 0.00042  2.94666E+06 0.00078  2.89810E+06 0.00044  2.84367E+06 0.00059  2.79897E+06 0.00066  2.76764E+06 0.00056  2.76621E+06 0.00064  2.41337E+06 0.00070  2.41810E+06 0.00084  2.38845E+06 0.00064  2.35712E+06 0.00101  4.60772E+06 0.00097  4.43937E+06 0.00097  3.19575E+06 0.00095  2.06128E+06 0.00108  2.42770E+06 0.00100  2.32759E+06 0.00073  1.94831E+06 0.00090  3.48539E+06 0.00066  7.10839E+05 0.00059  8.66733E+05 0.00053  7.66991E+05 0.00098  4.41146E+05 0.00132  7.49141E+05 0.00126  5.00843E+05 0.00043  4.26018E+05 0.00073  8.24199E+04 0.00141  8.06269E+04 0.00362  8.28439E+04 0.00215  8.45966E+04 0.00218  8.32237E+04 0.00156  8.21530E+04 0.00076  8.39322E+04 0.00256  7.87449E+04 0.00278  1.47631E+05 0.00074  2.33991E+05 0.00196  2.93710E+05 0.00151  7.54035E+05 0.00120  7.58005E+05 0.00103  7.59845E+05 0.00113  4.56805E+05 0.00086  3.09310E+05 0.00063  2.24469E+05 0.00127  2.42688E+05 0.00119  4.06506E+05 0.00080  4.77432E+05 0.00149  8.25466E+05 0.00080  1.26957E+06 0.00078  2.34075E+06 0.00083  1.98345E+06 0.00080  1.74339E+06 0.00083  1.46273E+06 0.00088  1.48823E+06 0.00082  1.65979E+06 0.00074  1.57430E+06 0.00105  1.16753E+06 0.00129  1.17750E+06 0.00120  1.14875E+06 0.00110  1.06669E+06 0.00099  9.09306E+05 0.00078  6.51741E+05 0.00073  2.56376E+05 0.00123 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.99795E+01 0.00076  1.21802E+01 0.00058 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  4.59749E-01 6.8E-05  5.29166E-01 8.1E-06 ];
INF_CAPT                  (idx, [1:   4]) = [  1.54493E-05 0.00363  2.94391E-04 0.00018 ];
INF_ABS                   (idx, [1:   4]) = [  1.54493E-05 0.00363  2.94391E-04 0.00018 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  8.71797E-08 0.00023  3.23547E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  4.59734E-01 6.8E-05  5.28871E-01 8.8E-06 ];
INF_SCATT1                (idx, [1:   4]) = [  2.99041E-02 0.00044  2.83601E-02 0.00115 ];
INF_SCATT2                (idx, [1:   4]) = [  2.78405E-03 0.00441  1.73763E-03 0.01585 ];
INF_SCATT3                (idx, [1:   4]) = [  4.43859E-04 0.03441  2.77423E-04 0.03917 ];
INF_SCATT4                (idx, [1:   4]) = [  2.67122E-05 0.42058  1.05479E-04 0.16151 ];
INF_SCATT5                (idx, [1:   4]) = [  3.62266E-06 1.00000  4.21293E-05 0.55370 ];
INF_SCATT6                (idx, [1:   4]) = [ -1.02489E-05 0.97092  3.57518E-05 0.61905 ];
INF_SCATT7                (idx, [1:   4]) = [  3.50448E-06 1.00000  1.56392E-06 1.00000 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  4.59734E-01 6.8E-05  5.28871E-01 8.8E-06 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.99041E-02 0.00044  2.83601E-02 0.00115 ];
INF_SCATTP2               (idx, [1:   4]) = [  2.78405E-03 0.00441  1.73763E-03 0.01585 ];
INF_SCATTP3               (idx, [1:   4]) = [  4.43859E-04 0.03441  2.77423E-04 0.03917 ];
INF_SCATTP4               (idx, [1:   4]) = [  2.67122E-05 0.42058  1.05479E-04 0.16151 ];
INF_SCATTP5               (idx, [1:   4]) = [  3.62266E-06 1.00000  4.21293E-05 0.55370 ];
INF_SCATTP6               (idx, [1:   4]) = [ -1.02489E-05 0.97092  3.57518E-05 0.61905 ];
INF_SCATTP7               (idx, [1:   4]) = [  3.50448E-06 1.00000  1.56392E-06 1.00000 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  3.92048E-01 0.00018  4.99731E-01 7.3E-05 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  8.50236E-01 0.00018  6.67025E-01 7.3E-05 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.54493E-05 0.00363  2.94391E-04 0.00018 ];
INF_REMXS                 (idx, [1:   4]) = [  3.36971E-03 0.00069  7.03446E-04 0.00252 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  4.56379E-01 7.1E-05  3.35449E-03 0.00069  4.08195E-04 0.00365  5.28462E-01 1.0E-05 ];
INF_S1                    (idx, [1:   8]) = [  3.08234E-02 0.00041 -9.19227E-04 0.00111  1.10747E-04 0.00711  2.82494E-02 0.00115 ];
INF_S2                    (idx, [1:   8]) = [  2.85343E-03 0.00440 -6.93829E-05 0.01448  1.46733E-06 0.80579  1.73616E-03 0.01560 ];
INF_S3                    (idx, [1:   8]) = [  4.53161E-04 0.03291 -9.30207E-06 0.12456 -1.53928E-05 0.02449  2.92816E-04 0.03782 ];
INF_S4                    (idx, [1:   8]) = [  2.79544E-05 0.41222 -1.24220E-06 0.64208 -1.06375E-05 0.03103  1.16117E-04 0.14699 ];
INF_S5                    (idx, [1:   8]) = [  5.91096E-06 1.00000 -2.28830E-06 0.25390 -7.14120E-06 0.03574  4.92706E-05 0.47033 ];
INF_S6                    (idx, [1:   8]) = [ -8.80220E-06 1.00000 -1.44666E-06 0.28401 -4.68473E-06 0.09252  4.04365E-05 0.55691 ];
INF_S7                    (idx, [1:   8]) = [  5.39346E-06 1.00000 -1.88898E-06 0.36542 -2.63286E-06 0.19175  4.19678E-06 1.00000 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  4.56379E-01 7.1E-05  3.35449E-03 0.00069  4.08195E-04 0.00365  5.28462E-01 1.0E-05 ];
INF_SP1                   (idx, [1:   8]) = [  3.08234E-02 0.00041 -9.19227E-04 0.00111  1.10747E-04 0.00711  2.82494E-02 0.00115 ];
INF_SP2                   (idx, [1:   8]) = [  2.85343E-03 0.00440 -6.93829E-05 0.01448  1.46733E-06 0.80579  1.73616E-03 0.01560 ];
INF_SP3                   (idx, [1:   8]) = [  4.53161E-04 0.03291 -9.30207E-06 0.12456 -1.53928E-05 0.02449  2.92816E-04 0.03782 ];
INF_SP4                   (idx, [1:   8]) = [  2.79544E-05 0.41222 -1.24220E-06 0.64208 -1.06375E-05 0.03103  1.16117E-04 0.14699 ];
INF_SP5                   (idx, [1:   8]) = [  5.91096E-06 1.00000 -2.28830E-06 0.25390 -7.14120E-06 0.03574  4.92706E-05 0.47033 ];
INF_SP6                   (idx, [1:   8]) = [ -8.80220E-06 1.00000 -1.44666E-06 0.28401 -4.68473E-06 0.09252  4.04365E-05 0.55691 ];
INF_SP7                   (idx, [1:   8]) = [  5.39346E-06 1.00000 -1.88898E-06 0.36542 -2.63286E-06 0.19175  4.19678E-06 1.00000 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  1.99825E-01 0.00025  5.49942E-01 0.00130 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  1.73255E-01 0.00076  5.46150E-01 0.00245 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  1.73108E-01 0.00058  5.44093E-01 0.00307 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.88641E-01 0.00068  5.59910E-01 0.00342 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.66813E+00 0.00025  6.06129E-01 0.00130 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.92395E+00 0.00076  6.10351E-01 0.00245 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.92559E+00 0.00058  6.12669E-01 0.00306 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.15484E+00 0.00068  5.95368E-01 0.00340 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1:  13]) = 'Serpent 2.2.1' ;
COMPILE_DATE              (idx, [1:  20]) = 'Mar 29 2023 13:58:32' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1:   8]) = 'Untitled' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  11]) = 'hetKiwi.txt' ;
WORKING_DIRECTORY         (idx, [1:  76]) = '/home/grads/z/zhughes/serpent-gen-foam-coupling/homogenousAssumption/hetKiwi' ;
HOSTNAME                  (idx, [1:  27]) = 'nuen-pf4q5fgc.engr.tamu.edu' ;
CPU_TYPE                  (idx, [1:  35]) = '13th Gen Intel(R) Core(TM) i7-1360P' ;
CPU_MHZ                   (idx, 1)        = 19982.0 ;
START_DATE                (idx, [1:  24]) = 'Wed Dec  6 10:03:09 2023' ;
COMPLETE_DATE             (idx, [1:  24]) = 'Wed Dec  6 10:27:43 2023' ;

% Run parameters:

POP                       (idx, 1)        = 100000 ;
CYCLES                    (idx, 1)        = 120 ;
SKIP                      (idx, 1)        = 10 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1701878589783 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000 ;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ] ;
B1_IMPLICIT_LEAKAGE       (idx, 1)        = 0 ;
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;

CRIT_SPEC_MODE            (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 8 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   8]) = [  1.06096E+00  1.00748E+00  1.04059E+00  9.65456E-01  9.91199E-01  1.00561E+00  1.00344E+00  9.25270E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;
OMP_SHARED_QUEUE_LIM      (idx, 1)        = 0 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1:  58]) = '/home/grads/z/zhughes/Serpent2/src/src/sss_jeff311u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:   3]) = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:   3]) = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:   2]) = [  9.00000E-01  9.00000E-01 ] ;
ST_FRAC                   (idx, [1:   4]) = [  1.89732E-01 0.00022  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  8.10268E-01 5.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  3.47901E-01 6.2E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  3.52412E-01 6.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  4.98140E+00 0.00034  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CELL_SEARCH_FRAC          (idx, [1:  10]) = [  9.31503E-01 5.1E-06  6.77301E-02 7.1E-05  7.66816E-04 0.00053  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  5.37674E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  5.34829E+01 0.00023  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.82792E+01 0.00021  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.42252E+01 0.00031  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 120 ;
SIMULATED_HISTORIES       (idx, 1)        = 11999594 ;
MEAN_POP_SIZE             (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
MEAN_POP_WGT              (idx, [1:   2]) = [  9.99966E+04 0.00055 ] ;
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.86430E+02 ;
RUNNING_TIME              (idx, 1)        =  2.45607E+01 ;
INIT_TIME                 (idx, [1:   2]) = [  6.03833E-02  6.03833E-02 ] ;
PROCESS_TIME              (idx, [1:   2]) = [  4.06667E-03  4.06667E-03 ] ;
TRANSPORT_CYCLE_TIME      (idx, [1:   3]) = [  2.44961E+01  2.44961E+01  0.00000E+00 ] ;
MPI_OVERHEAD_TIME         (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
ESTIMATED_RUNNING_TIME    (idx, [1:   2]) = [  2.45602E+01  0.00000E+00 ] ;
CPU_USAGE                 (idx, 1)        = 7.59060 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  7.65370E+00 0.00311 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.84750E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 31749.58 ;
ALLOC_MEMSIZE             (idx, 1)        = 1816.82 ;
MEMSIZE                   (idx, 1)        = 1723.31 ;
XS_MEMSIZE                (idx, 1)        = 684.35 ;
MAT_MEMSIZE               (idx, 1)        = 359.14 ;
RES_MEMSIZE               (idx, 1)        = 11.74 ;
IFC_MEMSIZE               (idx, 1)        = 0.00 ;
MISC_MEMSIZE              (idx, 1)        = 668.08 ;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00 ;
UNUSED_MEMSIZE            (idx, 1)        = 93.51 ;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 88 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 534567 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 7 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 18 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 18 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 564 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Energy deposition:

EDEP_MODE                 (idx, 1)        = 0 ;
EDEP_DELAYED              (idx, 1)        = 1 ;
EDEP_KEFF_CORR            (idx, 1)        = 1 ;
EDEP_LOCAL_EGD            (idx, 1)        = 0 ;
EDEP_COMP                 (idx, [1:   9]) = [ 0 0 0 0 0 0 0 0 0 ] ;
EDEP_CAPT_E               (idx, 1)        =  0.00000E+00 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, [1:   2]) = [  0.00000E+00  0.00000E+00 ] ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  9.99993E-06 0.00023  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  2.37040E-02 0.00233 ];
U235_FISS                 (idx, [1:   4]) = [  4.39594E-01 0.00043  9.99239E-01 1.2E-05 ];
U238_FISS                 (idx, [1:   4]) = [  3.34930E-04 0.01599  7.61270E-04 0.01596 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63448E-01 0.00066  5.92781E-01 0.00047 ];
U238_CAPT                 (idx, [1:   4]) = [  1.42985E-02 0.00231  5.18575E-02 0.00231 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC      (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_FISS     (idx, [1:   2]) = [ 11999594 1.20000E+07 ] ;
BALA_SRC_NEUTRON_NXN      (idx, [1:   2]) = [ 0 3.07185E+03 ] ;
BALA_SRC_NEUTRON_VR       (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_SRC_NEUTRON_TOT      (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_LOSS_NEUTRON_CAPT    (idx, [1:   2]) = [ 3307706 3.30881E+06 ] ;
BALA_LOSS_NEUTRON_FISS    (idx, [1:   2]) = [ 5277489 5.27919E+06 ] ;
BALA_LOSS_NEUTRON_LEAK    (idx, [1:   2]) = [ 3414399 3.41507E+06 ] ;
BALA_LOSS_NEUTRON_CUT     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_ERR     (idx, [1:   2]) = [ 0 0.00000E+00 ] ;
BALA_LOSS_NEUTRON_TOT     (idx, [1:   2]) = [ 11999594 1.20031E+07 ] ;

BALA_NEUTRON_DIFF         (idx, [1:   2]) = [ 0 1.30385E-06 ] ;

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.42481E-11 0.00025 ];
TOT_POWDENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.07478E+00 0.00025 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.39652E-01 0.00025 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  2.75761E-01 0.00028 ];
TOT_ABSRATE               (idx, [1:   2]) = [  7.15413E-01 0.00022 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99993E-01 0.00023 ];
TOT_FLUX                  (idx, [1:   2]) = [  1.50754E+02 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84587E-01 0.00054 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  5.34945E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  0.00000E+00 ;
TOT_FMASS                 (idx, 1)        =  0.00000E+00 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  2.06897E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  9.43314E-01 0.00018 ];
SIX_FF_P                  (idx, [1:   2]) = [  2.21873E-01 0.00061 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  3.47168E+00 0.00065 ];
SIX_FF_LF                 (idx, [1:   2]) = [  7.84479E-01 0.00017 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.11956E-01 1.0E-04 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.50326E+00 0.00030 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07545E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.44462E+00 2.1E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02274E+02 1.1E-08 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07546E+00 0.00037  1.06760E+00 0.00035  7.85251E-03 0.00513 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07480E+00 0.00041 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07505E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.50286E+00 0.00011 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.34231E+01 0.00016 ];
IMP_ALF                   (idx, [1:   2]) = [  1.34222E+01 0.00013 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  2.96201E-05 0.00213 ];
IMP_EALF                  (idx, [1:   2]) = [  2.96427E-05 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  9.56457E-02 0.00191 ];
IMP_AFGE                  (idx, [1:   2]) = [  9.60144E-02 0.00056 ];

% Forward-weighted delayed neutron parameters:

PRECURSOR_GROUPS          (idx, 1)        = 8 ;
FWD_ANA_BETA_ZERO         (idx, [1:  18]) = [  6.19701E-03 0.00316  2.12214E-04 0.02069  9.32270E-04 0.00871  6.03108E-04 0.01181  1.24314E-03 0.00810  1.95618E-03 0.00583  5.75214E-04 0.01149  5.29829E-04 0.01106  1.45057E-04 0.02175 ];
FWD_ANA_LAMBDA            (idx, [1:  18]) = [  4.12626E-01 0.00550  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  18]) = [  7.30326E-03 0.00484  2.52643E-04 0.02883  1.07836E-03 0.01379  7.28104E-04 0.01592  1.45729E-03 0.01289  2.31190E-03 0.00915  6.84640E-04 0.01732  6.16116E-04 0.01891  1.74199E-04 0.03540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  18]) = [  4.13031E-01 0.00975  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.7E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using Nauchi's method:

IFP_CHAIN_LENGTH          (idx, 1)        = 15 ;
ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.67074E-05 0.00175  3.67396E-05 0.00175  3.23918E-05 0.02412 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.94768E-05 0.00172  3.95114E-05 0.00173  3.48255E-05 0.02401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  18]) = [  7.30834E-03 0.00522  2.56277E-04 0.02750  1.09392E-03 0.01343  7.24251E-04 0.01710  1.46691E-03 0.01287  2.30459E-03 0.00939  6.85706E-04 0.01667  6.12519E-04 0.01809  1.64175E-04 0.03686 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  18]) = [  4.06861E-01 0.00831  1.24667E-02 0.0E+00  2.82917E-02 1.3E-09  4.25244E-02 6.6E-09  1.33042E-01 4.0E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 4.9E-09 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.40314E-05 0.01969  3.40741E-05 0.01970  2.81157E-05 0.05863 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.65992E-05 0.01968  3.66451E-05 0.01969  3.02356E-05 0.05859 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  18]) = [  6.99011E-03 0.02656  2.72235E-04 0.09362  1.03109E-03 0.05136  7.15367E-04 0.05715  1.40770E-03 0.04094  2.18721E-03 0.03841  6.15540E-04 0.06010  6.06935E-04 0.06302  1.54033E-04 0.13562 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  18]) = [  4.06891E-01 0.03027  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 3.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  18]) = [  6.98748E-03 0.02656  2.61792E-04 0.09162  1.03864E-03 0.04747  7.09594E-04 0.05445  1.40819E-03 0.04020  2.19984E-03 0.03751  6.15317E-04 0.05838  5.99649E-04 0.06141  1.54460E-04 0.13110 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  18]) = [  4.05045E-01 0.02857  1.24667E-02 0.0E+00  2.82917E-02 0.0E+00  4.25244E-02 6.3E-09  1.33042E-01 4.2E-09  2.92467E-01 0.0E+00  6.66488E-01 0.0E+00  1.63478E+00 0.0E+00  3.55460E+00 5.6E-09 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05690E+02 0.01907 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.62085E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.89400E-05 0.00094 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33604E-03 0.00238 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02625E+02 0.00249 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  3.23664E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  1.83987E-05 0.00030  1.84013E-05 0.00031  1.80256E-05 0.00410 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  1.86518E-04 0.00090  1.86593E-04 0.00092  1.75907E-04 0.01332 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  2.28083E-01 0.00053  2.27887E-01 0.00052  2.59814E-01 0.00725 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.32394E+01 0.00678 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  5.34829E+01 0.00023  5.42613E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:   1]) = '_' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  2.00000E+01  6.06550E+00  3.67900E+00  2.23100E+00  1.35300E+00  8.21000E-01  5.00000E-01  3.02500E-01  1.83000E-01  1.11000E-01  6.74300E-02  4.08500E-02  2.47800E-02  1.50300E-02  9.11800E-03  5.50000E-03  3.51910E-03  2.23945E-03  1.42510E-03  9.06898E-04  3.67262E-04  1.48728E-04  7.55014E-05  4.80520E-05  2.77000E-05  1.59680E-05  9.87700E-06  4.00000E-06  3.30000E-06  2.60000E-06  2.10000E-06  1.85500E-06  1.50000E-06  1.30000E-06  1.15000E-06  1.12300E-06  1.09700E-06  1.07100E-06  1.04500E-06  1.02000E-06  9.96000E-07  9.72000E-07  9.50000E-07  9.10000E-07  8.50000E-07  7.80000E-07  6.25000E-07  5.00000E-07  4.00000E-07  3.50000E-07  3.20000E-07  3.00000E-07  2.80000E-07  2.50000E-07  2.20000E-07  1.80000E-07  1.40000E-07  1.00000E-07  8.00000E-08  6.70000E-08  5.80000E-08  5.00000E-08  4.20000E-08  3.50000E-08  3.00000E-08  2.50000E-08  2.00000E-08  1.50000E-08  1.00000E-08  5.00000E-09  1.00000E-11 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CAPT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_ABS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_FISS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NSF                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_NUBAR                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATT7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP1               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP2               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP3               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP4               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP5               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP6               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SCATTP7               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_REMXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MACRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison universe-averaged densities:

I135_ADENS                (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
XE135_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM147_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM148M_ADENS              (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
PM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
SM149_ADENS               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Poison decay constants:

PM147_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148_LAMBDA              (idx, 1)        =  0.00000E+00 ;
PM148M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
PM149_LAMBDA              (idx, 1)        =  0.00000E+00 ;
I135_LAMBDA               (idx, 1)        =  0.00000E+00 ;
XE135_LAMBDA              (idx, 1)        =  0.00000E+00 ;
XE135M_LAMBDA             (idx, 1)        =  0.00000E+00 ;
I135_BR                   (idx, 1)        =  0.00000E+00 ;

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_S7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP1                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP2                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP3                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP4                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP5                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP6                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SP7                   (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
LAMBDA                    (idx, [1:  18]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

